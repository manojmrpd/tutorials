# JVM Memory Management and Garbage Collection

1. **Introduction**
2. **Java Basics**
   - [What is the Difference Between JDK, JVM, JRE, JIT Compiler](#what-is-the-difference-between-jdk-jvm-jre-jit-compiler)
   - [What is the Difference Between Path vs Classpath](#what-is-the-difference-between-path-vs-classpath)

3. **JVM Architecture**
   - [Explain JVM Architecture in Detail](#explain-jvm-architecture-in-detail)

4. **Class Loading Subsystem**
   - [What is Class Loader Subsystem in Java](#what-is-class-loader-subsystem-in-java)
   - [What is Bootstrap Class Loaders](#what-is-bootstrap-class-loaders)
   - [What is Extension Class Loaders](#what-is-extension-class-loaders)
   - [What is System Class Loaders](#what-is-system-class-loaders)
   - [What is the Delegation Model of Class Loader Subsystem](#what-is-the-delegation-model-of-class-loader-subsystem)

5. **JVM Memory Model**
   - [What is Code Segment](#what-is-code-segment)
   - [What is Data Segment](#what-is-data-segment)
   - [What is Method Area](#what-is-method-area)
   - [How Are Temporary Variables Stored Inside Method Block](#how-are-temporary-variables-stored-inside-method-block)
   - [What is Stack Area](#what-is-stack-area)
   - [How Method Invocation/Execution Block Stores in Stack Area](#how-method-invocationexecution-block-stores-in-stack-area)
   - [Explain LIFO Principle of Stack Area](#explain-lifo-principle-of-stack-area)
   - [What is Heap Area](#what-is-heap-area)

6. **Heap Structure**
   - [What is Young Generation](#what-is-young-generation)
   - [What is Eden Space](#what-is-eden-space)
   - [What is Survivor Spaces (S0, S1)](#what-is-survivor-spaces-s0-s1)
   - [What is Minor GC](#what-is-minor-gc)
   - [What is Old or Tenured Generation](#what-is-old-or-tenured-generation)
   - [What is Major GC](#what-is-major-gc)
   - [What is Age Threshold](#what-is-age-threshold)
   - [What is the Difference Between PermGen vs Metaspace](#what-is-the-difference-between-permgen-vs-metaspace)

7. **References in Java**
   - [What is Heap Object References](#what-is-heap-object-references)
   - [What is Strong References](#what-is-strong-references)
   - [What is Weak References](#what-is-weak-references)
   - [What is Soft References](#what-is-soft-references)
   - [What is Phantom References](#what-is-phantom-references)

8. **JVM Runtime Details**
   - [What is PC Registers](#what-is-pc-registers)
   - [What is Java Native Interface (JNI)](#what-is-java-native-interface-jni)

9. **Garbage Collection**
   - [What is Garbage Collection](#what-is-garbage-collection)
   - [How Garbage Collection Works in Java](#how-garbage-collection-works-in-java)
   - [When an Object is Eligible for Garbage Collection](#when-an-object-is-eligible-for-garbage-collection)
   - [How to Prevent Object from Garbage Collection](#how-to-prevent-object-from-garbage-collection)
   - [Explain Mark and Sweep Algorithm](#explain-mark-and-sweep-algorithm)
   - [Explain Mark and Compact Algorithm](#explain-mark-and-compact-algorithm)
   - [What are Garbage Collection Types](#what-are-garbage-collection-types)

10. **Garbage Collection Algorithms**
    - [What is Sequential GC](#what-is-sequential-gc)
    - [What is Parallel GC](#what-is-parallel-gc)
    - [What is G1 Garbage Collector](#what-is-g1-garbage-collector)
    - [What is CMS Garbage Collector](#what-is-cms-garbage-collector)

11. **Memory Allocation**
    - [What is Contiguous vs Non-Contiguous Memory Allocation](#what-is-contiguous-vs-non-contiguous-memory-allocation)

12. **Memory Management Best Practices**
    - [How to Prevent Memory Leaks and OutOfMemoryError](#how-to-prevent-memory-leaks-and-outofmemoryerror)

# Java Object oriented programming principles

 **1. Introduction**

 **2. Basics of Class and Object**
- [What is a Class in Java](#what-is-a-class-in-java)
- [What is an Object in Java](#what-is-an-object-in-java)
- [How to Create an Object in Java](#how-to-create-an-object-in-java)
- [What is the Difference Between Class and Object in Java](#what-is-the-difference-between-class-and-object-in-java)

 **3. Static and Instance Concepts**
- [What is the `static` Keyword](#what-is-the-static-keyword)
- [What is a Static Method and Instance Method](#what-is-a-static-method-and-instance-method)
- [What is a Static Variable and Instance Variable](#what-is-a-static-variable-and-instance-variable)
- [What is a Default Method](#what-is-a-default-method)

 **4. Object-Oriented Principles**
- [What are Object-Oriented Principles in Java](#what-are-object-oriented-principles-in-java)
  - [What is Abstraction](#what-is-abstraction)
  - [What is Encapsulation](#what-is-encapsulation)
  - [What is Inheritance](#what-is-inheritance)
  - [Why Multiple Inheritance is Not Possible in Java](#why-multiple-inheritance-is-not-possible-in-java)
  - [What is Polymorphism](#what-is-polymorphism)
    - [What is Compile-Time and Runtime Polymorphism in Java](#what-is-compile-time-and-runtime-polymorphism-in-java)
  - [What is Dynamic Binding](#what-is-dynamic-binding)
  - [What is Aggregation, Composition, and Association in Java](#what-is-aggregation-composition-and-association-in-java)
  - [What is `is-A` and `has-A` Relationship in Java](#what-is-is-a-and-has-a-relationship-in-java)

 **5. Method Concepts**
- [What is Method Overriding and Method Overloading](#what-is-method-overriding-and-method-overloading)

 **6. Constructors in Java**
- [What is a Constructor](#what-is-a-constructor)
- [What is the Difference Between Default Constructor and Parameterized Constructor](#what-is-the-difference-between-default-constructor-and-parameterized-constructor)

 **7. Keywords in Java**
- [What is `this` Keyword](#what-is-this-keyword)
- [What is `super` Keyword](#what-is-super-keyword)
- [What is the Difference Between `this` and `super` Keyword](#what-is-the-difference-between-this-and-super-keyword)
- [What is `instanceof` Keyword](#what-is-instanceof-keyword)
- [What is `synchronized` Keyword](#what-is-synchronized-keyword)
- [What is `volatile` Keyword](#what-is-synchronized-keyword)
- [What is `transient` Keyword](#what-is-transient-keyword)
- [What is the `final` Keyword](#what-is-the-final-keyword)
- [What is the Difference Between `final`, `finally`, and `finalize()` Keyword in Java](#what-is-the-difference-between-final-finally-and-finalize-keyword-in-java)

 **8. Abstract Classes and Interfaces**
- [What is an Abstract Class](#what-is-an-abstract-class)
- [What is an Interface](#what-is-an-interface)
- [What is the Difference Between Abstract Class and Interface](#what-is-the-difference-between-abstract-class-and-interface)

 **9. Nested and Inner Classes**
- [What is a Nested Class in Java](#what-is-a-nested-class-in-java)
- [What is an Anonymous Inner Class in Java](#what-is-an-anonymous-inner-class-in-java)
- [What is a Nested Interface in Java](#what-is-a-nested-interface-in-java)

 **10. Specialized Interfaces**
- [What is a Marker Interface in Java](#what-is-a-marker-interface-in-java)
- [What is a Functional Interface in Java](#what-is-a-functional-interface-in-java)

 **11. Wrapper Classes and Autoboxing**
- [What is a Wrapper Class](#what-is-a-wrapper-class)
- [What is Autoboxing and Unboxing in Java](#what-is-autoboxing-and-unboxing-in-java)

 **12. Access Modifiers and Packages**
- [What are Access Modifiers in Java](#what-are-access-modifiers-in-java)
- [What is a Package in Java](#what-is-a-package-in-java)

 **13. Mutable and Immutable Concepts**
- [What is a Mutable Class vs Immutable Class in Java](#what-is-a-mutable-class-vs-immutable-class-in-java)
- [How to Make a Class Immutable in Java](#how-to-make-a-class-immutable-in-java)

 **14. Comparison Concepts**
- [What is the Difference Between `equals()` and `==` Operator in Java](#what-is-the-difference-between-equals-and-operator-in-java)
- [What is the Difference Between String, StringBuilder, and StringBuffer in Java](#what-is-the-difference-between-string-stringbuilder-and-stringbuffer-in-java)


 **15. Parameter Passing in Java**
- [What is the Difference Between Pass by Reference and Pass by Value](#what-is-the-difference-between-pass-by-reference-and-pass-by-value)
- [Is Java Pass by Reference or Pass by Value](#is-java-pass-by-reference-or-pass-by-value)

 **16. Enumerations**
- [What is an Enum in Java](#what-is-an-enum-in-java)

 **17. Serialization and Deserialization**
- [What is the Difference Between Serialization and Deserialization](#what-is-the-difference-between-serialization-and-deserialization)
- [What is the Difference Between SerialUUID and RandomUUID](#what-is-the-difference-between-serialuuid-and-randomuuid)
- [What is Externalization in Java](#what-is-externalization-in-java)


 **18. Runnable, Clonable, and Callable**
- [What is the Difference Between Runnable, Clonable, and Callable in Java](#what-is-the-difference-between-runnable-clonable-and-callable-in-java)
- [What is the Difference Between Deep Cloning and Shallow Cloning in Java](#what-is-the-difference-between-deep-cloning-and-shallow-cloaning-in-java)

 **19. Java Keywords**
- [What are Java Keywords](#what-are-java-keywords)

# Java Multithreading and Concurrency

1. **Introduction**
   - [What is Multithreading in Java?](#what-is-multithreading-in-java)  
   - [Why is Multithreading important in modern applications?](#why-is-multithreading-important-in-modern-applications)

2. **Core Concepts**
   - [What is the difference between a Process and a Thread in Java?](#what-is-the-difference-between-a-process-and-a-thread-in-java)  
   - [What is the role of Threads in JVM Architecture?](#what-is-the-role-of-threads-in-jvm-architecture)  
   - [How does Multithreading differ from Multitasking?](#how-does-multithreading-differ-from-multitasking)

3. **Creating Threads**
   - [What are the different ways to create a Thread in Java?](#what-are-the-different-ways-to-create-a-thread-in-java)  
   - [What are the Thread Life Cycle States?](#what-are-the-thread-life-cycle-states)

4. **Thread Communication and Control**
   - [What is Inter-Thread Communication?](#what-is-inter-thread-communication)  
   - [What does `Thread.sleep()` do?](#what-does-threadsleep-do)  
   - [How does `Thread.join()` work?](#how-does-threadjoin-work)  
   - [What is `Thread.interrupt()` and when is it used?](#what-is-threadinterrupt-and-when-is-it-used)  
   - [What is `Thread.yield()`?](#what-is-threadyield)  
   - [What are the differences between `wait()`, `notify()`, and `notifyAll()`?](#what-are-the-differences-between-wait-notify-and-notifyall)  
   - [Why are `suspend()`, `stop()`, and `resume()` deprecated in the latest Java versions?](#why-are-suspend-stop-and-resume-deprecated-in-the-latest-java-versions)

5. **Synchronization and Concurrency**
   - [What is Synchronization in Java?](#what-is-synchronization-in-java)  
   - [What are Synchronized Blocks?](#what-are-synchronized-blocks)  
   - [What are Synchronized Methods?](#what-are-synchronized-methods)  
   - [What is the purpose of the `volatile` keyword?](#what-is-the-purpose-of-the-volatile-keyword)  
   - [What is a Monitor Lock?](#what-is-a-monitor-lock)

6. **Common Thread Issues**
   - [What is a Deadlock?](#what-is-a-deadlock)  
   - [What is a Livelock?](#what-is-a-livelock)  
   - [What is Starvation?](#what-is-starvation)  
   - [What is Context Switching in Threads?](#what-is-context-switching-in-threads)  
   - [What are the causes of Thread Leaks in Java?](#what-are-the-causes-of-thread-leaks-in-java)  
   - [How can Thread Leaks be prevented?](#how-can-thread-leaks-be-prevented)  
   - [What causes Race Conditions in Threads?](#what-causes-race-conditions-in-threads)  
   - [How can Race Conditions in Threads be prevented?](#how-can-race-conditions-in-threads-be-prevented)

7. **Thread Utilities and Advanced Concepts**
   - [What is Thread Priority?](#what-is-thread-priority)  
   - [What is a Daemon Thread?](#what-is-a-daemon-thread)  
   - [What is the Producer-Consumer Problem?](#what-is-the-producer-consumer-problem)  
   - [What is a CountDownLatch?](#what-is-a-countdownlatch)  
   - [What is a CyclicBarrier?](#what-is-a-cyclicbarrier)  
   - [What is a Phaser?](#what-is-a-phaser)  
   - [What is an Exchanger?](#what-is-an-exchanger)  
   - [What is a Semaphore?](#what-is-a-semaphore)  
   - [What is a ReentrantLock?](#what-is-a-reentrantlock)  
   - [What is a ReadWriteLock?](#what-is-a-readwritelock)  
   - [What is a Condition?](#what-is-a-condition)

8. **Atomic Operations**
   - [What are Atomic Variables in Java?](#what-are-atomic-variables-in-java)  
   - [What are the differences between `AtomicInteger`, `AtomicLong`, and `AtomicBoolean`?](#what-are-the-differences-between-atomicinteger-atomiclong-and-atomicboolean)  
   - [What is the difference between `AtomicReference` and `AtomicArrayReference`?](#what-is-the-difference-between-atomicreference-and-atomicarrayreference)  
   - [What is the Compare and Swap (CAS) Algorithm?](#what-is-the-compare-and-swap-cas-algorithm)  
   - [How do Atomic Variables differ from `volatile`?](#how-do-atomic-variables-differ-from-volatile)

9. **Thread Pooling and Executors**
   - [What is a Thread Pool?](#what-is-a-thread-pool)  
   - [What is the Executor Framework?](#what-is-the-executor-framework)  
   - [What is a Fixed Thread Pool?](#what-is-a-fixed-thread-pool)  
   - [What is a Single Thread Pool?](#what-is-a-single-thread-pool)  
   - [What is a Cached Thread Pool?](#what-is-a-cached-thread-pool)  
   - [What is a Scheduled Executor Service?](#what-is-a-scheduled-executor-service)  
   - [What is the difference between `shutdown()`, `awaitTermination()`, and `shutdownNow()`?](#what-is-the-difference-between-shutdown-awaittermination-and-shutdownnow)  
   - [What is a ThreadPoolExecutor?](#what-is-a-threadpoolexecutor)

10. **Futures and Async Programming**
    - [What is the difference between `Callable` and `Future`?](#what-is-the-difference-between-callable-and-future)  
    - [What is a FutureTask?](#what-is-a-futuretask)  
    - [What is a CompletableFuture?](#what-is-a-completablefuture)  
    - [What does `supplyAsync()` do?](#what-does-supplyasync-do)  
    - [What is the difference between `thenApply()` and `thenCompose()`?](#what-is-the-difference-between-thenapply-and-thencompose)

11. **Fork-Join Framework**
    - [What is a ForkJoinPool?](#what-is-a-forkjoinpool)  
    - [What is a WorkStealingPoolExecutor?](#what-is-a-workstealingpoolexecutor)

12. **Modern Thread Concepts**
    - [What is the difference between Virtual Threads and Platform Threads?](#what-is-the-difference-between-virtual-threads-and-platform-threads)  
    - [What is Thread Local?](#what-is-thread-local)

# Java Collection Framework

1. **Introduction**
   - [What is Collection Framework in Java?](#what-is-collection-framework-in-java)
   - [What is the difference between Array and Collection?](#what-is-the-difference-between-array-and-collection)

2. **Queue and Related Interfaces**
   - [What is Queue?](#what-is-queue)  
   - [What is PriorityQueue?](#what-is-priorityqueue)  
   - [What is PriorityBlockingQueue?](#what-is-priorityblockingqueue)  
   - [What is Deque?](#what-is-deque)  
   - [What is the difference between Queue and Deque?](#what-is-the-difference-between-queue-and-deque)
   - [What is ArrayDeque?](#what-is-arraydeque)  
   - [What is ConcurrentLinkedQueue?](#what-is-concurrentlinkedqueue)

3. **List and Related Classes**
   - [What is List?](#what-is-list)  
   - [What is ArrayList?](#what-is-arraylist)  
   - [What is LinkedList?](#what-is-linkedlist)  
   - [What is Vector?](#what-is-vector)  
   - [What is Stack?](#what-is-stack)  
   - [What is the difference between ArrayList and LinkedList?](#what-is-the-difference-between-arraylist-and-linkedlist)  
   - [What is the difference between CopyOnWriteArrayList and CopyOnWriteArraySet?](#what-is-the-difference-between-copyonwritearraylist-and-copyonwritearrayset)

4. **Set and Related Interfaces**
   - [What is Set?](#what-is-set)  
   - [What is HashSet?](#what-is-hashset)  
   - [What is LinkedHashSet?](#what-is-linkedhashset)  
   - [What is the difference between SortedSet, NavigableSet, and TreeSet?](#what-is-the-difference-between-sortedset-navigableset-and-treeset)  
   - [What is ConcurrentSkipListSet?](#what-is-concurrentskiplistset)

5. **Map and Related Classes**
   - [What is Map?](#what-is-map)  
   - [What is Hashtable?](#what-is-hashtable)  
   - [What is HashMap?](#what-is-hashmap)  
   - [What is LinkedHashMap?](#what-is-linkedhashmap)  
   - [What is the difference between SortedMap, NavigableMap, and TreeMap?](#what-is-the-difference-between-sortedmap-navigablemap-and-treemap)  
   - [What is ConcurrentHashMap?](#what-is-concurrenthashmap)  
   - [What is ConcurrentSkipListMap?](#what-is-concurrentskiplistmap)  
   - [What is the difference between EnumMap, IdentityHashMap, and WeakHashMap?](#what-is-the-difference-between-enummap-identityhashmap-and-weakhashmap)

6. **Internal Workings**
   - [Explain the internal working of LinkedList.](#explain-the-internal-working-of-linkedlist)  
   - [Explain the internal working of HashMap.](#explain-the-internal-working-of-hashmap)  
   - [Explain the internal working of ConcurrentHashMap.](#explain-the-internal-working-of-concurrenthashmap)  
   - [Explain the internal working of TreeMap.](#explain-the-internal-working-of-treemap)

7. **Core Concepts and Principles**
   - [What is a Resizable Array?](#what-is-a-resizable-array)  
   - [What is the Skip List Data Structure?](#what-is-the-skip-list-data-structure)  
   - [What is the difference between Single and Doubly LinkedList?](#what-is-the-difference-between-single-and-doubly-linkedlist)  
   - [What is Hashing/Re-hashing?](#what-is-hashingre-hashing)  
   - [What is Initial Capacity vs Load Factor?](#what-is-initial-capacity-vs-load-factor)  
   - [What is a Hash Collision - Chaining vs Open Addressing?](#what-is-a-hash-collision---chaining-vs-open-addressing)  
   - [What is Treefication / Treefy Threshold?](#what-is-treefication--treefy-threshold)  
   - [What is a Red-Black Tree / Self-balanced Binary Search Tree?](#what-is-a-red-black-tree--self-balanced-binary-search-tree)  
   - [What is the Compare and Swap (CAS) Algorithm?](#what-is-the-compare-and-swap-cas-algorithm)  
   - [What is Segment Locking in ConcurrentHashMap?](#what-is-segment-locking-in-concurrenthashmap)

8. **Ordering and Iteration**
   - [What is FIFO vs LIFO principle?](#what-is-fifo-vs-lifo-principle)  
   - [What is the Access Order of LinkedHashMap?](#what-is-the-access-order-of-linkedhashmap)  
   - [What is the difference between Iterator, ListIterator, and Enumeration?](#what-is-the-difference-between-iterator-listiterator-and-enumeration)  
   - [What is the difference between Comparator and Comparable?](#what-is-the-difference-between-comparator-and-comparable)

9. **Performance and Exceptions**
   - [Explain the Time Complexity (Big-O Notation) of all collection methods.](#explain-the-time-complexity-big-o-notation-of-all-collection-methods)  
   - [Explain Sorting, Searching, and Ordering of all collection classes.](#explain-sorting-searching-and-ordering-of-all-collection-classes)  
   - [Explain Null and Duplicate Behavior of all Collection classes.](#explain-null-and-duplicate-behavior-of-all-collection-classes)  
   - [What is the difference between Fail-fast and Fail-safe Iterator?](#what-is-the-difference-between-fail-fast-and-fail-safe-iterator)  
   - [How to avoid ConcurrentModificationException?](#how-to-avoid-concurrentmodificationexception)  
   - [How to avoid Thread Race Conditions?](#how-to-avoid-thread-race-conditions)

10. **Synchronized and Concurrent Collections**
    - [What is the difference between Synchronized and Concurrent Collections?](#what-is-the-difference-between-synchronized-and-concurrent-collections)

11. **Miscellaneous**
    - [What is the difference between Collection and Collections?](#what-is-the-difference-between-collection-and-collections)  
    - [What is the difference between hashCode() and equals() method contract?](#what-is-the-difference-between-hashcode-and-equals-method-contract)

# Java-8 Features

#### 1. Introduction
- [What are Java 8 Features?](#what-are-java-8-features)

#### 2. Lambda Expressions and Functional Interfaces
- [What is Lambda Expressions?](#what-is-lambda-expressions)
- [What is Functional Interfaces?](#what-is-functional-interfaces)
- [What are Pre-defined Functional Interfaces in Java?](#what-are-pre-defined-functional-interfaces-in-java)
- [What is the difference between Consumer vs BiConsumer?](#what-is-the-difference-between-consumer-vs-biconsumer)
- [What is Supplier?](#what-is-supplier)
- [What is the difference between Function vs BiFunction?](#what-is-the-difference-between-function-vs-bifunction)
- [What is the difference between UnaryOperator vs BinaryOperator?](#what-is-the-difference-between-unaryoperator-vs-binaryoperator)

#### 3. Method References
- [What is Method References?](#what-is-method-references)
- [What is Static Method Reference?](#what-is-static-method-reference)
- [What is Instance Method Reference of a Particular Object?](#what-is-instance-method-reference-of-a-particular-object)
- [What is Instance Method Reference of an Arbitrary Object of a Particular Type?](#what-is-instance-method-reference-of-an-arbitrary-object-of-a-particular-type)
- [What is Constructor Reference?](#what-is-constructor-reference)

#### 4. Optional API
- [What is Optional Interface?](#what-is-optional-interface)
- [What is Optional.of()?](#what-is-optionalof)
- [What is Optional.ofNullable()?](#what-is-optionalofnullable)
- [What is Optional.empty()?](#what-is-optionalempty)
- [What is Optional.get()?](#what-is-optionalget)
- [What is Optional.isPresent()?](#what-is-optionalispresent)
- [What is Optional.isEmpty()?](#what-is-optionalisempty)
- [What is Optional.filter()?](#what-is-optionalfilter)
- [What is Optional.map()?](#what-is-optionalmap)
- [What is Optional.flatMap()?](#what-is-optionalflatmap)
- [What is Optional.ifPresent()?](#what-is-optionalifpresent)
- [What is Optional.ifPresentOrElse()?](#what-is-optionalifpresentorelse)
- [What is Optional.orElse()?](#what-is-optionalorelse)
- [What is Optional.orElseGet()?](#what-is-optionalorelseget)
- [What is Optional.orElseThrow()?](#what-is-optionalorelsethrow)

#### 5. Streams API
- [What is Streams API?](#what-is-streams-api)
- [What is Intermediate Operations in Java Stream API?](#what-is-intermediate-operations-in-java-stream-api)
- [What is Terminal Operations in Java Stream API?](#what-is-terminal-operations-in-java-stream-api)

#### 6. Intermediate Operations
- [What is filter()?](#what-is-filter)
- [What is map(), mapToInt(), mapToLong(), mapToDouble()?](#what-is-map-maptoint-maptolong-maptodouble)
- [What is flatMap(), flatMapToInt(), flatMapToLong(), flatMapToDouble()?](#what-is-flatmap-flatmaptoint-flatmaptolong-flatmaptodouble)
- [What is distinct()?](#what-is-distinct)
- [What is sorted()?](#what-is-sorted)
- [What is skip()?](#what-is-skip)
- [What is limit()?](#what-is-limit)
- [What is peek()?](#what-is-peek)

#### 7. Terminal Operations
- [What is forEach()?](#what-is-foreach)
- [What is reduce()?](#what-is-reduce)
- [What is sum()?](#what-is-sum)
- [What is min()?](#what-is-min)
- [What is max()?](#what-is-max)
- [What is average()?](#what-is-average)
- [What is count()?](#what-is-count)
- [What is findFirst()?](#what-is-findfirst)
- [What is findAny()?](#what-is-findany)
- [What is allMatch()?](#what-is-allmatch)
- [What is noneMatch()?](#what-is-nonematch)
- [What is anyMatch()?](#what-is-anymatch)
- [What is Terminal Operations - collect()?](#what-is-terminal-operations---collect)

#### 8. Collectors
- [What is Collectors.toArray()?](#what-is-collectorstotoarray)
- [What is Collectors.toCollection()?](#what-is-collectorstocollection)
- [What is Collectors.toList()?](#what-is-collectorstolist)
- [What is Collectors.toSet()?](#what-is-collectorstoset)
- [What is Collectors.toMap()?](#what-is-collectorstomap)
- [What is Collectors.toUnmodifiableList()?](#what-is-collectorstounmodifiablelist)
- [What is Collectors.toUnmodifiableSet()?](#what-is-collectorstounmodifiableset)
- [What is Collectors.toUnmodifiableMap()?](#what-is-collectorstounmodifiablemap)
- [What is Collectors.groupingBy()?](#what-is-collectorsgroupingby)
- [What is Collectors.groupingByConcurrent()?](#what-is-collectorsgroupingbyconcurrent)
- [What is Collectors.partitioningBy()?](#what-is-collectorspartitioningby)
- [What is Collectors.mapping()?](#what-is-collectorsmapping)
- [What is Collectors.flatMapping()?](#what-is-collectorsflatmapping)
- [What is Collectors.averaging()?](#what-is-collectorsaveraging)
- [What is Collectors.summing()?](#what-is-collectorssumming)
- [What is Collectors.minBy()?](#what-is-collectorsminby)
- [What is Collectors.maxBy()?](#what-is-collectorsmaxby)
- [What is Collectors.counting()?](#what-is-collectorscounting)
- [What is Collectors.joining()?](#what-is-collectorsjoining)
- [What is Collectors.collectingAndThen()?](#what-is-collectorscollectingandthen)

#### 9. Comparator
- [What is Comparator?](#what-is-comparator)
- [What is Comparator.comparing()?](#what-is-comparatorcomparing)
- [What is Comparator.comparingInt()?](#what-is-comparatorcomparingint)
- [What is Comparator.comparingLong()?](#what-is-comparatorcomparinglong)
- [What is Comparator.comparingDouble()?](#what-is-comparatorcomparingdouble)
- [What is Comparator.naturalOrder()?](#what-is-comparatornaturalorder)
- [What is Comparator.reverseOrder()?](#what-is-comparatorreverseorder)
- [What is Comparator.thenComparing()?](#what-is-comparatorthencomparing)

#### 10. Parallel Streams
- [What is the difference between Parallel Stream vs Sequential Stream?](#what-is-the-difference-between-parallel-stream-vs-sequential-stream)
- [Explain Internal Working of Parallel Stream?](#explain-internal-working-of-parallel-stream)
- [What is Task Splitting using SplitIterator?](#what-is-task-splitting-using-splititerator)
- [What is Task Submission using ForkJoinPool?](#what-is-task-submission-using-forkjoinpool)

# Java Design Patterns
#### 1. Creational Design Pattern
1. [What is Singleton Design Pattern](#what-is-singleton-design-pattern)
2. [What is Prototype Design Pattern](#what-is-prototype-design-pattern)
3. [What is Factory Design Pattern](#what-is-factory-design-pattern)
4. [What is Abstract Factory Design Pattern](#what-is-abstract-factory-design-pattern)
5. [What is Builder Design Pattern](#what-is-builder-design-pattern)

#### 2. Structural Design Pattern
1. [What is Adapter Design Pattern](#what-is-adapter-design-pattern)
2. [What is Decorator Design Pattern](#what-is-decorator-design-pattern)
3. [What is Proxy Design Pattern](#what-is-proxy-design-pattern)
4. [What is Bridge Design Pattern](#what-is-bridge-design-pattern)
5. [What is Composite Design Pattern](#what-is-composite-design-pattern)
6. [What is Flyweight Design Pattern](#what-is-flyweight-design-pattern)

#### 3. Behavioural Design Pattern
1. [What is Chain of Responsibility Design Pattern](#what-is-chain-of-Responsibility-design-pattern)
2. [What is Command Design Pattern](#what-is-command-design-pattern)
3. [What is Interpreter Design Pattern](#what-is-interpreter-design-pattern)
4. [What is Mediator Design Pattern](#what-is-mediator-design-pattern)
5. [What is Memento Design Pattern](#what-is-memento-design-pattern)
6. [What is Observer Design Pattern](#what-is-observer-design-pattern)
7. [What is State Design Pattern](#what-is-state-design-pattern)
8. [What is Strategy Design Pattern](#what-is-strategy-design-pattern)
9. [What is Template Design Pattern](#what-is-template-design-pattern)
10. [What is Visitor Design Pattern](#what-is-visitor-design-pattern)

#### 4. SOLID Principles
1. [What is Java SOLID Principles?](#what-is-java-solid-principles)
2. [What is Single Responsibility Principle (SRP)?](#what-is-single-responsibility-principle-srp)
3. [What is Open-Closed Principle (OCP)?](#what-is-open-closed-principle-ocp)
4. [What is Liskov Substitution Principle (LSP)?](#what-is-liskov-substitution-principle-lsp)
5. [What is Interface Segregation Principle (ISP)?](#what-is-interface-segregation-principle-isp)
6. [What is Dependency Inversion Principle (DIP)?](#what-is-dependency-inversion-principle-dip)