# JVM Memory Management and Garbage Collection

## Table of Contents
1. [JVM Architecture](#jvm-architecture)
    - [Code Segment](#code-segment)
    - [Data Segment](#data-segment)
    - [Heap Area](#heap-area)
    - [Stack Area](#stack-area)
    - [Register](#register)
    - [Counter](#counter)
2. [Stack Memory](#stack-memory)
    - [How Method Invocation/Execution Block Stores in Stack Area](#how-method-invocationexecution-block-stores-in-stack-area)
        - [Temporary variables inside method block](#temporary-variables-inside-method-block)
        - [Heap Object references](#heap-object-references)
    - [LIFO principle of STACK](#lifo-principle-of-stack)
    - [Each Thread has its own Stack Area](#each-thread-has-its-own-stack-area)
    - [Heap Object References Types](#heap-object-reference-types)
      - [Strong References](#strong-references)
      - [Weak References](#weak-references)
      - [Soft References](#soft-references)
      - [Phantom References](#phantom-references)
3. [Heap Memory](#heap-memory)
    - [Young Generation](#young-generation)
      - [Eden gen](#eden-gen)
      - [Survivals - S0, S1](#survivals---s0-s1)
      - [Minor gc()](#minor-gc)
    - [Old or Tenured generation](#old-or-tenured-generation)
      - [Major gc()](#major-gc)
      - [Age threshold](#age-threshold)
   - [Permgen vs Metaspace](#permgen-vs-metaspace)
7. [Garbage Collection](#garbage-collection)
    - [Mark and Sweep algorithm](#mark-and-sweep-algorithm)
    - [Mark and Compact algorithm](#mark-and-compact-algorithm)
    - [Garbage Collection Types](#garbage-collection-types)
       - [Sequential GC](#sequential-gc)
       - [Parallel GC](#parallel-gc)
       - [G1 Garbage Collector](#g1-garbage-collector)
8. [Class Loader Sub-system](#class-loader-sub-system)
    - [Bootstrap class loaders](#bootstrap-class-loaders)
    - [Extension class loaders](#extension-class-loaders)
    - [System class loaders](#system-class-loaders)
    - [Delegation model of class loader subsystem](#delegation-model-of-class-loader-subsystem)
    - [Path vs Classpath](#path-vs-classpath)
9. [How to Prevent Memory Leaks and OutOfMemoryError](#how-to-prevent-memory-leaks-and-outofmemoryerror)
10. [JDK, JVM, JRE, JIT Compiler](#jdk-jvm-jre-jit-compiler)
11. [PC Registers and Java Native Interface (JNI)](#pc-registers-and-java-native-interface-jni)

# Java Collection Framework

## Table of Contents

1. [Array vs Collection](#array-vs-collection)
2. [Queue](#queue)
   - [PriorityQueue](#priorityqueue)
   - [PriorityBlockingQueue](#priorityblockingqueue)
3. [Deque](#deque)
   - [ArrayDeque](#arraydeque)
   - [ConcurrentLinkedQueue](#concurrentlinkedqueue)
4. [List](#list)
   - [ArrayList](#arraylist)
   - [LinkedList](#linkedlist)
   - [Vector](#vector)
   - [Stack](#stack)
   - [CopyOnWriteArrayList vs CopyOnWriteArraySet](#copyonwritearraylist-vs-copyonwritearrayset)
5. [Set](#set)
   - [HashSet](#hashset)
   - [LinkedHashSet](#linkedhashset)
   - [SortedSet vs NavigableSet vs TreeSet](#sortedset-vs-navigableset-vs-treeset)
   - [ConcurrentSkipListSet](#concurrentskiplistset)
6. [Map](#map)
   - [Hashtable](#hashtable)
   - [HashMap - Map.Entry<K,V> & Node structure](#hashmap---mapentrykv--node-structure)
   - [LinkedHashMap](#linkedhashmap)
   - [SortedMap vs NavigableMap vs TreeMap](#sortedmap-vs-navigablemap-vs-treemap)
   - [ConcurrentHashMap](#concurrenthashmap)
   - [ConcurrentSkipListMap](#concurrentskiplistmap)
   - [EnumMap vs IdentityHashMap vs WeakHashMap](#enummap-vs-identityhashmap-vs-weakhashmap)
7. [Data Structure Concepts](#data-structure-concepts)
   - [Internal working of HashMap](#internal-working-of-hashmap)
   - [Resizable Array](#resizable-array)
   - [Single vs Doubly LinkedList](#single-vs-doubly-linkedlist)
   - [Hashing/Re-hashing](#hashingre-hashing)
   - [Initial Capacity vs Load Factor](#initial-capacity-vs-load-factor)
   - [Hash collision - Chaining vs Open addressing](#hash-collision---chaining-vs-open-addressing)
   - [Treefication / Treefy Threshold](#treefication--treefy-threshold)
   - [Red-black tree / Self-balanced binary search tree](#red-black-tree--self-balanced-binary-search-tree)
   - [Compare and Swap (CAS) algorithm](#compare-and-swap-cas-algorithm)
   - [Segment locking in ConcurrentHashMap](#segment-locking-in-concurrenthashmap)
   - [FIFO vs LIFO principles](#fifo-vs-lifo-principles)
   - [Access Order of LinkedHashMap](#access-order-of-linkedhashmap)
8. [Iterator vs ListIterator vs Enumeration](#iterator-vs-listiterator-vs-enumeration)
9. [Comparator vs Comparable](#comparator-vs-comparable)
10. [Time-complexity (BigO-Notation) of all collection methods](#time-complexity-bigo-notation-of-all-collection-methods)
11. [Sorting vs Searching vs Ordering](#sorting-vs-searching-vs-ordering)
12. [Allow Null and Duplicates?](#allow-null-and-duplicates)
13. [Fail-fast vs Fail-safe Iterator](#fail-fast-vs-fail-safe-iterator)
14. [How to avoid ConcurrentModificationException](#how-to-avoid-concurrentmodificationexception)
15. [How to avoid Thread race condition](#how-to-avoid-thread-race-condition)
16. [Synchronized vs Concurrent Collections](#synchronized-vs-concurrent-collections)
17. [Collection vs Collections](#collection-vs-collections)
18. [hashCode() and equals() method contract](#hashcode-and-equals-method-contract)

# Java 8 Features
## Table of Contents

1. [Lambda Expressions](#lambda-expressions)
   - [Using implements](#using-implements)
   - [Using anonymous class](#using-anonymous-class)
   - [Using lambda expression](#using-lambda-expression)
2. [Functional Interfaces](#functional-interfaces)
   - [Single abstract vs default vs static methods](#single-abstract-vs-default-vs-static-methods)
   - [Consumer vs BiConsumer](#consumer-vs-biconsumer)
   - [Supplier](#supplier)
   - [Predicate vs BiPredicate](#predicate-vs-bipredicate)
   - [Function vs BiFunction](#function-vs-bifunction)
   - [UnaryOperator vs BinaryOperator](#unaryoperator-vs-binaryoperator)
3. [Method References](#method-references)
   - [Static Method Reference](#static-method-reference)
   - [Instance Method Reference of a Particular Object](#instance-method-reference-of-a-particular-object)
   - [Instance Method Reference of an Arbitrary Object of a Particular Type](#instance-method-reference-of-an-arbitrary-object-of-a-particular-type)
   - [Constructor Reference](#constructor-reference)
4. [Optional Interface](#optional-interface)
   - [Optional.of()](#optionalof)
   - [Optional.ofNullable()](#optionalofnullable)
   - [Optional.empty()](#optionalempty)
   - [Optional.get()](#optionalget)
   - [Optional.isPresent()](#optionalispresent)
   - [Optional.isEmpty()](#optionalisempty)
   - [Optional.filter()](#optionalfilter)
   - [Optional.map()](#optionalmap)
   - [Optional.flatMap()](#optionalflatmap)
   - [Optional.ifPresent()](#optionalifpresent)
   - [Optional.ifPresentOrElse()](#optionalifpresentorelse)
   - [Optional.orElse()](#optionalorelse)
   - [Optional.orElseGet()](#optionalorElseget)
   - [Optional.orElseThrow()](#optionalorelsethrow)
5. [Streams API](#streams-api)
   - [Intermediate Operations](#intermediate-operations)
      - [filter()](#filter)
      - [map(), mapToInt(), mapToLong(), mapToDouble()](#map-maptoint-maptolong-maptodouble)
      - [flatMap(), flatMapToInt(), flatMapToLong(), flatMapToDouble()](#flatmap-flatmaptoint-flatmaptolong-flatmaptodouble)
      - [distinct()](#distinct)
      - [sorted()](#sorted)
      - [skip()](#skip)
      - [limit()](#limit)
      - [peek()](#peek)
   - [Terminal Operations](#terminal-operations)
      - [forEach()](#foreach)
      - [reduce()](#reduce)
      - [sum()](#sum)
      - [min()](#min)
      - [max()](#max)
      - [average()](#average)
      - [count()](#count)
      - [findFirst()](#findfirst)
      - [findAny()](#findany)
      - [allMatch()](#allmatch)
      - [noneMatch()](#nonematch)
      - [anyMatch()](#anymatch)
   - [Terminal Operations - collect()](#terminal-operations---collect)
      - [Collectors.toArray()](#collectorstoarray)
      - [Collectors.toCollection()](#collectorstocollection)
      - [Collectors.toList()](#collectorstolist)
      - [Collectors.toSet()](#collectorstoset)
      - [Collectors.toMap()](#collectorstomap)
      - [Collectors.toUnmodifiableList()](#collectorstounmodifiablelist)
      - [Collectors.toUnmodifiableSet()](#collectorstounmodifiableset)
      - [Collectors.toUnmodifiableMap()](#collectorstounmodifiablemap)
      - [Collectors.groupingBy()](#collectorsgroupingby)
      - [Collectors.groupingByConcurrent()](#collectorsgroupingbyconcurrent)
      - [Collectors.partitioningBy()](#collectorspartitioningby)
      - [Collectors.mapping()](#collectorsmapping)
      - [Collectors.flatMapping()](#collectorsflatmapping)
      - [Collectors.averaging()](#collectorsaveraging)
      - [Collectors.summing()](#collectorssumming)
      - [Collectors.minBy()](#collectorsminby)
      - [Collectors.maxBy()](#collectorsmaxby)
      - [Collectors.counting()](#collectorscounting)
      - [Collectors.joining()](#collectorsjoining)
      - [Collectors.collectingAndThen()](#collectorscollectingandthen)
   - [Comparator](#comparator)
      - [Comparator.comparing()](#comparatorcomparing)
      - [Comparator.comparingInt()](#comparatorcomparingint)
      - [Comparator.comparingLong()](#comparatorcomparinglong)
      - [Comparator.comparingDouble()](#comparatorcomparingdouble)
      - [Comparator.naturalOrder()](#comparatornaturalorder)
      - [Comparator.reverseOrder()](#comparatorreverseorder)
      - [Comparator.thenComparing()](#comparatorthencomparing)
6. [Parallel Stream vs Sequential Stream](#parallel-stream-vs-sequential-stream)
7. [Parallel Stream Internal Working](#parallel-stream-internal-working)
   - [Task Splitting using SplitIterator](#task-splitting-using-splititerator)
   - [Task Submission using ForkJoinPool](#task-submission-using-forkjoinpool)


# Java Multi Threading and Concurrency
## Table of Contents

1. [Process vs Thread](#process-vs-thread)
2. [Threads Role in JVM Architecture](#threads-role-in-jvm-architecture)
   - [Code Segment](#code-segment)
   - [Data Segment](#data-segment)
   - [Heap](#heap)
   - [Stack](#stack)
   - [Register](#register)
   - [Counter](#counter)
3. [Multithreading vs Multitasking](#multithreading-vs-multitasking)
4. [Ways to Create Thread](#ways-to-create-thread)
   - [Extends Thread Class](#extends-thread-class)
   - [Implements Runnable Interface](#implements-runnable-interface)
   - [ExecutorService](#executorservice)
5. [Thread Life Cycle States](#thread-life-cycle-states)
   - [New](#new)
   - [Runnable](#runnable)
   - [Running](#running)
   - [Blocked](#blocked)
   - [Waiting](#waiting)
   - [Timed Waiting](#timed-waiting)
   - [Terminated](#terminated)
6. [Inter Thread Communication](#inter-thread-communication)
   - [Thread.sleep()](#threadsleep)
   - [Thread.join()](#threadjoin)
   - [Thread.interrupt()](#threadinterrupt)
   - [Thread.yield()](#threadyield)
   - [wait(), notify() and notifyAll()](#wait-notify-and-notifyall)
   - [Why Thread - suspend(), stop(), resume() is Deprecated](#why-thread---suspend-stop-resume-is-deprecated)
   - [Producer-Consumer Problem](#producer-consumer-problem)
7. [Thread Priority](#thread-priority)
   - [MIN_PRIORITY](#min_priority)
   - [MAX_PRIORITY](#max_priority)
8. [Daemon Thread](#daemon-thread)
9. [What is Context Switching in Threads?](#what-is-context-switching-in-threads)
10. [Thread Leakages](#thread-leakages)
    - [What are Causes of Thread Leakages?](#what-are-causes-of-thread-leakages)
    - [How to Prevent Thread Leakages?](#how-to-prevent-thread-leakages)
11. [Thread Race Condition](#thread-race-condition)
    - [What are Causes of Thread Race Condition?](#what-are-causes-of-thread-race-condition)
    - [How to Prevent Race Conditions in Threads?](#how-to-prevent-race-conditions-in-threads)
12. [Thread Synchronization to Prevent Race Condition](#thread-synchronization-to-prevent-race-condition)
    - [Synchronized Blocks](#synchronized-blocks)
    - [Synchronized Methods](#synchronized-methods)
    - [Volatile](#volatile)
    - [Locks](#locks)
13. [Improper Handling of Synchronization can Lead to](#improper-handling-of-synchronization-can-lead-to)
    - [Deadlock](#deadlock)
    - [Livelock](#livelock)
    - [Starvation](#starvation)
14. [Lock-based Concurrency](#lock-based-concurrency)
    - [CountDownLatch](#countdownlatch)
    - [CyclicBarrier](#cyclicbarrier)
    - [Phaser](#phaser)
    - [Exchanger](#exchanger)
    - [Semaphore](#semaphore)
    - [ReentrantLock](#reentrantlock)
    - [ReadWriteLock](#readwritelock)
    - [Condition](#condition)
15. [Lock-Free Concurrency](#lock-free-concurrency)
    - [AtomicInteger, AtomicLong, AtomicBoolean](#atomicinteger-atomiclong-atomicboolean)
    - [AtomicReference vs AtomicArrayReference](#atomicreference-vs-atomicarrayreference)
    - [Compare and Swap (CAS) Algorithm](#compare-and-swap-cas-algorithm)
    - [Atomic Variable vs Volatile](#atomic-variable-vs-volatile)
16. [Thread Pool](#thread-pool)
    - [Executor Framework](#executor-framework)
        - [Fixed Thread Pool](#fixed-thread-pool)
        - [Single Thread Pool](#single-thread-pool)
        - [Cached Thread Pool](#cached-thread-pool)
        - [Scheduled Executor Service](#scheduled-executor-service)
        - [Shutdown() vs AwaitTermination() vs shutdownNow()](#shutdown-vs-awaittermination-vs-shutdownnow)
    - [ThreadPoolExecutor](#threadpoolexecutor)
17. [Callable and Future](#callable-and-future)
    - [FutureTask](#futuretask)
    - [CompletableFuture](#completablefuture)
       - [supplyAsync()](#supplyasync)
       - [thenApply() vs thenCompose()](#thenapply-vs-thencompose)
       - [Combining Futures](#combining-futures)
       - [Running Multiple Futures in Parallel](#running-multiple-futures-in-parallel)
       - [Handling Errors](#handling-errors)
    - [ForkJoinPool](#forkjoinpool)
    - [WorkStealingPoolExecutor](#workstealingpoolexecutor)
18. [Virtual Thread vs Platform Thread](#virtual-thread-vs-platform-thread)
19. [Thread Local](#thread-local)