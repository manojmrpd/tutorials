# Java Object oriented programming principles

### **1. Introduction**

### **2. Basics of Class and Object**
- [What is a Class in Java](#what-is-a-class-in-java)
- [What is an Object in Java](#what-is-an-object-in-java)
- [How to Create an Object in Java](#how-to-create-an-object-in-java)
- [What is the Difference Between Class and Object in Java](#what-is-the-difference-between-class-and-object-in-java)

### **3. Static and Instance Concepts**
- [What is the `static` Keyword](#what-is-the-static-keyword)
- [What is a Static Method and Instance Method](#what-is-a-static-method-and-instance-method)
- [What is a Static Variable and Instance Variable](#what-is-a-static-variable-and-instance-variable)
- [What is a Default Method](#what-is-a-default-method)

### **4. Object-Oriented Principles**
- [What are Object-Oriented Principles in Java](#what-are-object-oriented-principles-in-java)
  - [What is Abstraction](#what-is-abstraction)
  - [What is Encapsulation](#what-is-encapsulation)
  - [What is Inheritance](#what-is-inheritance)
  - [Why Multiple Inheritance is Not Possible in Java](#why-multiple-inheritance-is-not-possible-in-java)
  - [What is Polymorphism](#what-is-polymorphism)
    - [What is Compile-Time and Runtime Polymorphism in Java](#what-is-compile-time-and-runtime-polymorphism-in-java)
  - [What is Dynamic Binding](#what-is-dynamic-binding)
  - [What is Aggregation, Composition, and Association in Java](#what-is-aggregation-composition-and-association-in-java)
  - [What is `is-A` and `has-A` Relationship in Java](#what-is-is-a-and-has-a-relationship-in-java)

### **5. Method Concepts**
- [What is Method Overriding and Method Overloading](#what-is-method-overriding-and-method-overloading)

### **6. Constructors in Java**
- [What is a Constructor](#what-is-a-constructor)
- [What is the Difference Between Default Constructor and Parameterized Constructor](#what-is-the-difference-between-default-constructor-and-parameterized-constructor)

### **7. Keywords in Java**
- [What is `this` Keyword](#what-is-this-keyword)
- [What is `super` Keyword](#what-is-super-keyword)
- [What is the Difference Between `this` and `super` Keyword](#what-is-the-difference-between-this-and-super-keyword)
- [What is `instanceof` Keyword](#what-is-instanceof-keyword)
- [What is `synchronized` Keyword](#what-is-synchronized-keyword)
- [What is `volatile` Keyword](#what-is-synchronized-keyword)
- [What is `transient` Keyword](#what-is-transient-keyword)
- [What is the `final` Keyword](#what-is-the-final-keyword)
- [What is the Difference Between `final`, `finally`, and `finalize()` Keyword in Java](#what-is-the-difference-between-final-finally-and-finalize-keyword-in-java)

### **8. Abstract Classes and Interfaces**
- [What is an Abstract Class](#what-is-an-abstract-class)
- [What is an Interface](#what-is-an-interface)
- [What is the Difference Between Abstract Class and Interface](#what-is-the-difference-between-abstract-class-and-interface)

### **9. Nested and Inner Classes**
- [What is a Nested Class in Java](#what-is-a-nested-class-in-java)
- [What is an Anonymous Inner Class in Java](#what-is-an-anonymous-inner-class-in-java)
- [What is a Nested Interface in Java](#what-is-a-nested-interface-in-java)

### **10. Specialized Interfaces**
- [What is a Marker Interface in Java](#what-is-a-marker-interface-in-java)
- [What is a Functional Interface in Java](#what-is-a-functional-interface-in-java)

### **11. Wrapper Classes and Autoboxing**
- [What is a Wrapper Class](#what-is-a-wrapper-class)
- [What is Autoboxing and Unboxing in Java](#what-is-autoboxing-and-unboxing-in-java)

### **12. Access Modifiers and Packages**
- [What are Access Modifiers in Java](#what-are-access-modifiers-in-java)
- [What is a Package in Java](#what-is-a-package-in-java)

### **13. Mutable and Immutable Concepts**
- [What is a Mutable Class vs Immutable Class in Java](#what-is-a-mutable-class-vs-immutable-class-in-java)
- [How to Make a Class Immutable in Java](#how-to-make-a-class-immutable-in-java)

### **14. Comparison Concepts**
- [What is the Difference Between `equals()` and `==` Operator in Java](#what-is-the-difference-between-equals-and-operator-in-java)
- [What is the Difference Between String, StringBuilder, and StringBuffer in Java](#what-is-the-difference-between-string-stringbuilder-and-stringbuffer-in-java)


### **15. Parameter Passing in Java**
- [What is the Difference Between Pass by Reference and Pass by Value](#what-is-the-difference-between-pass-by-reference-and-pass-by-value)
- [Is Java Pass by Reference or Pass by Value](#is-java-pass-by-reference-or-pass-by-value)

### **16. Enumerations**
- [What is an Enum in Java](#what-is-an-enum-in-java)

### **17. Serialization and Deserialization**
- [What is the Difference Between Serialization and Deserialization](#what-is-the-difference-between-serialization-and-deserialization)
- [What is the Difference Between SerialUUID and RandomUUID](#what-is-the-difference-between-serialuuid-and-randomuuid)
- [What is Externalization in Java](#what-is-externalization-in-java)


### **18. Runnable, Clonable, and Callable**
- [What is the Difference Between Runnable, Clonable, and Callable in Java](#what-is-the-difference-between-runnable-clonable-and-callable-in-java)
- [What is the Difference Between Deep Cloning and Shallow Cloning in Java](#what-is-the-difference-between-deep-cloning-and-shallow-cloaning-in-java)

### **19. Java Keywords**
- [What are Java Keywords](#what-are-java-keywords)

## What is a Class in Java
### **What is a Class in Java?**

A **class** in Java is a blueprint or template for creating objects. It defines a set of properties (fields) and behaviors (methods) that the objects created from the class will have. Classes serve as the fundamental building blocks of object-oriented programming in Java.

---

### **Key Concepts of a Class**
1. **Blueprint for Objects**: A class is not an object itself but a prototype from which objects are created.
2. **Fields/Attributes**: Variables within a class that hold data about the object.
3. **Methods**: Functions defined inside the class to perform operations on the attributes or carry out actions.
4. **Constructors**: Special methods used to initialize objects of the class.
5. **Access Modifiers**: Keywords (e.g., `public`, `private`, `protected`) to define the visibility of class members.

---

### **Structure of a Class**
Here is the general structure:

```java
class ClassName {
    // Fields/Attributes
    DataType field1;
    DataType field2;

    // Constructor
    ClassName() {
        // Initialization code
    }

    // Methods
    ReturnType methodName(parameters) {
        // Method logic
    }

    // Other methods, if any
}
```

---

### **Example of a Simple Java Class**
Here’s an example of a class named `Car`:

#### Code:
```java
// Defining the class
class Car {
    // Fields/Attributes
    String brand;
    String model;
    int year;

    // Constructor
    public Car(String brand, String model, int year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    // Method to display car details
    public void displayDetails() {
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
    }
}

// Main class to test the Car class
public class Main {
    public static void main(String[] args) {
        // Creating an object of Car
        Car myCar = new Car("Toyota", "Corolla", 2020);

        // Using the object's method
        myCar.displayDetails();
    }
}
```

---

### **Explanation of the Example**
1. **Fields**: `brand`, `model`, and `year` are attributes of the `Car` class.
2. **Constructor**: The constructor initializes the attributes of a `Car` object when it is created.
3. **Method**: `displayDetails()` prints the details of the car.
4. **Object Creation**: In the `main` method, an object `myCar` is created and its attributes are initialized using the constructor.

---

### **Access Modifiers**
Java classes and their members can have different levels of access:
1. **Public**: Accessible from anywhere.
2. **Private**: Accessible only within the class.
3. **Protected**: Accessible within the same package and subclasses.
4. **Default (No Modifier)**: Accessible only within the same package.

---

### **Features of Classes in Java**
- **Encapsulation**: Bundling of data (fields) and methods in a class.
- **Abstraction**: Hiding implementation details using abstract classes or interfaces.
- **Inheritance**: Creating a new class from an existing class using the `extends` keyword.
- **Polymorphism**: Allowing methods to perform differently based on the object.

---

### **Advanced Example with Multiple Features**
```java
// Parent class
class Animal {
    String name;

    public Animal(String name) {
        this.name = name;
    }

    public void makeSound() {
        System.out.println("Animal makes a sound");
    }
}

// Subclass
class Dog extends Animal {
    public Dog(String name) {
        super(name); // Call parent class constructor
    }

    @Override
    public void makeSound() {
        System.out.println(name + " barks!");
    }
}

// Main class
public class Main {
    public static void main(String[] args) {
        Animal myAnimal = new Animal("Generic Animal");
        myAnimal.makeSound();

        Dog myDog = new Dog("Buddy");
        myDog.makeSound();
    }
}
```

---

### **Output of Advanced Example**
```
Animal makes a sound
Buddy barks!
```

### **Key Concepts Illustrated**
1. **Inheritance**: `Dog` inherits from `Animal`.
2. **Method Overriding**: `makeSound()` is overridden in the `Dog` class.
3. **Super Keyword**: Used to call the parent class constructor.

---

By combining fields, methods, constructors, and other features, Java classes provide a powerful way to model real-world objects and behaviors in software.

## What is an Object in Java
### **What is an Object in Java?**

An **object** in Java is an instance of a class. It is a real-world entity that has:
1. **State**: Represented by the fields or attributes of the class.
2. **Behavior**: Represented by the methods of the class.
3. **Identity**: A unique reference in memory to distinguish it from other objects.

An object is created using the `new` keyword, which instantiates the class and allocates memory for the object.

---

### **Key Characteristics of an Object**
1. **Encapsulation**: Objects combine data (fields) and behavior (methods).
2. **Reusability**: Objects can be reused by creating multiple instances of a class.
3. **Instance-Specific**: Each object can hold its own set of attribute values, independent of other objects.

---

### **How to Create an Object**
To create an object in Java, follow these steps:
1. **Declare** a reference variable: The variable holds the reference to the object.
2. **Instantiate** the object: Use the `new` keyword to allocate memory for the object.
3. **Initialize** the object: Call the constructor of the class to initialize its fields.

#### Syntax:
```java
ClassName objectName = new ClassName(arguments);
```

---

### **Example of Object Creation**

```java
class Car {
    String brand;
    String model;
    int year;

    // Constructor
    public Car(String brand, String model, int year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    // Method to display car details
    public void displayDetails() {
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating an object of Car class
        Car car1 = new Car("Toyota", "Corolla", 2020);

        // Accessing object's methods
        car1.displayDetails();
    }
}
```

---

### **Explanation of the Example**
1. **Class as Blueprint**: `Car` serves as the blueprint for creating objects.
2. **Object Creation**: `Car car1 = new Car("Toyota", "Corolla", 2020);`
   - `Car`: Class name.
   - `car1`: Reference variable that holds the object.
   - `new Car(...)`: Allocates memory and calls the constructor to initialize the object.
3. **Object Method**: `car1.displayDetails()` accesses the behavior defined in the `Car` class.

---

### **Components of an Object**
#### 1. **State**
The object's state is determined by the values of its attributes (fields). For example:
- For `Car car1`, the state includes `brand = "Toyota"`, `model = "Corolla"`, and `year = 2020`.

#### 2. **Behavior**
The object's behavior is defined by the methods of the class. For example:
- The `displayDetails()` method in the `Car` class prints the state of the object.

#### 3. **Identity**
Every object has a unique reference in memory. This reference is held by the reference variable (e.g., `car1`).

---

### **Multiple Objects Example**

```java
class Student {
    String name;
    int age;

    // Constructor
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Method to display student details
    public void displayInfo() {
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating multiple objects
        Student student1 = new Student("Alice", 20);
        Student student2 = new Student("Bob", 22);

        // Accessing methods for each object
        student1.displayInfo();
        student2.displayInfo();
    }
}
```

---

### **Output**
```
Name: Alice
Age: 20
Name: Bob
Age: 22
```

#### Explanation:
Each object (`student1` and `student2`) holds its own state and behaves independently, even though both are created from the same `Student` class.

---

### **Objects in Memory**
When an object is created:
1. Memory is allocated in the **heap**.
2. The reference variable (e.g., `car1`) is stored in the **stack** and points to the object's memory location.

---

### **Features of Objects in Java**

1. **Encapsulation**: Data and behavior are encapsulated within the object.
2. **Polymorphism**: An object can take multiple forms when interacting with parent classes or interfaces.
3. **Inheritance**: Objects of a subclass can inherit fields and methods from the parent class.
4. **Dynamic Binding**: At runtime, the JVM determines the method implementation to call based on the object's actual type.

---

### **Advanced Example with Inheritance and Polymorphism**

```java
class Animal {
    String name;

    public Animal(String name) {
        this.name = name;
    }

    public void makeSound() {
        System.out.println(name + " makes a sound.");
    }
}

class Dog extends Animal {
    public Dog(String name) {
        super(name);
    }

    @Override
    public void makeSound() {
        System.out.println(name + " barks.");
    }
}

public class Main {
    public static void main(String[] args) {
        // Parent class reference for an Animal object
        Animal animal = new Animal("Generic Animal");
        animal.makeSound();

        // Parent class reference for a Dog object (polymorphism)
        Animal dog = new Dog("Buddy");
        dog.makeSound();
    }
}
```

---

### **Output**
```
Generic Animal makes a sound.
Buddy barks.
```

### **Key Concepts Demonstrated**
1. **Inheritance**: `Dog` inherits from `Animal`.
2. **Polymorphism**: The `makeSound()` method behaves differently based on the actual object type (`Animal` or `Dog`).
3. **Dynamic Method Dispatch**: At runtime, the JVM determines which version of `makeSound()` to execute.

---

### **Summary**
An **object** is the core of Java programming, representing a tangible or conceptual entity. It is created from a class and encapsulates state and behavior. Objects allow you to:
- Model real-world systems.
- Reuse and modularize code.
- Implement core object-oriented principles like inheritance, polymorphism, and encapsulation.

## How to Create an Object in Java
### **How to Create an Object in Java?**

In Java, an **object** is created from a class. There are several ways to create objects depending on the requirements. Here's a detailed explanation of different methods to create an object in Java with examples.

---

### **1. Using the `new` Keyword**
This is the most common way to create an object. The `new` keyword instantiates a class by allocating memory for a new object and calling the constructor of the class.

#### Syntax:
```java
ClassName objectName = new ClassName(arguments);
```

#### Example:
```java
class Car {
    String brand;
    String model;

    // Constructor
    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    public void displayDetails() {
        System.out.println("Brand: " + brand + ", Model: " + model);
    }
}

public class Main {
    public static void main(String[] args) {
        // Create object using new keyword
        Car car1 = new Car("Toyota", "Corolla");
        car1.displayDetails();
    }
}
```

#### Output:
```
Brand: Toyota, Model: Corolla
```

---

### **2. Using Reflection (Class.forName)**
Java Reflection API allows you to create an object of a class dynamically during runtime using `Class.forName`.

#### Example:
```java
class Car {
    public void displayDetails() {
        System.out.println("This is a car.");
    }
}

public class Main {
    public static void main(String[] args) {
        try {
            // Create object using reflection
            Class<?> carClass = Class.forName("Car");
            Car car = (Car) carClass.getDeclaredConstructor().newInstance();
            car.displayDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

#### Output:
```
This is a car.
```

---

### **3. Using the Clone Method**
If a class implements the `Cloneable` interface, you can create a copy of an object using the `clone()` method.

#### Example:
```java
class Car implements Cloneable {
    String brand;
    String model;

    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    // Overriding the clone method
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

public class Main {
    public static void main(String[] args) {
        try {
            // Create an object
            Car car1 = new Car("Toyota", "Corolla");

            // Create a clone of car1
            Car car2 = (Car) car1.clone();

            System.out.println("Car1 Brand: " + car1.brand + ", Model: " + car1.model);
            System.out.println("Car2 Brand: " + car2.brand + ", Model: " + car2.model);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
```

#### Output:
```
Car1 Brand: Toyota, Model: Corolla
Car2 Brand: Toyota, Model: Corolla
```

---

### **4. Using Deserialization**
Deserialization is the process of converting a serialized object (stored in a file or database) back into an object in memory.

#### Example:
```java
import java.io.*;

class Car implements Serializable {
    String brand;
    String model;

    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }
}

public class Main {
    public static void main(String[] args) {
        try {
            // Serialize the object
            Car car1 = new Car("Toyota", "Corolla");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("car.ser"));
            oos.writeObject(car1);
            oos.close();

            // Deserialize the object
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("car.ser"));
            Car car2 = (Car) ois.readObject();
            ois.close();

            System.out.println("Deserialized Car Brand: " + car2.brand + ", Model: " + car2.model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

#### Output:
```
Deserialized Car Brand: Toyota, Model: Corolla
```

---

### **5. Using Factory Methods**
Factory methods are static methods that return an instance of the class. This is commonly used in design patterns.

#### Example:
```java
class Car {
    String brand;
    String model;

    private Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    // Factory method
    public static Car createCar(String brand, String model) {
        return new Car(brand, model);
    }

    public void displayDetails() {
        System.out.println("Brand: " + brand + ", Model: " + model);
    }
}

public class Main {
    public static void main(String[] args) {
        // Create object using factory method
        Car car = Car.createCar("Honda", "Civic");
        car.displayDetails();
    }
}
```

#### Output:
```
Brand: Honda, Model: Civic
```

---

### **6. Using Anonymous Inner Classes**
You can create an anonymous object of an abstract class or interface by providing implementation inline.

#### Example:
```java
abstract class Animal {
    abstract void sound();
}

public class Main {
    public static void main(String[] args) {
        // Create an anonymous object
        Animal animal = new Animal() {
            @Override
            void sound() {
                System.out.println("Animal makes a sound.");
            }
        };
        animal.sound();
    }
}
```

#### Output:
```
Animal makes a sound.
```

---

### **7. Using Enum**
Enums can be used to create predefined objects of a specific type.

#### Example:
```java
enum CarType {
    SUV, SEDAN, HATCHBACK;

    public void displayType() {
        System.out.println("Car Type: " + this);
    }
}

public class Main {
    public static void main(String[] args) {
        // Access enum object
        CarType car = CarType.SUV;
        car.displayType();
    }
}
```

#### Output:
```
Car Type: SUV
```

---

### **Summary of Ways to Create an Object**

| Method                    | Description                                                                 |
|---------------------------|-----------------------------------------------------------------------------|
| **Using `new` keyword**   | Most common method, directly calls the constructor of the class.           |
| **Reflection (`Class.forName`)** | Dynamically creates objects at runtime.                               |
| **Clone Method**          | Creates a copy of an existing object using the `clone()` method.           |
| **Deserialization**       | Recreates an object from its serialized form (file, database, etc.).       |
| **Factory Methods**       | Static methods return objects, commonly used in design patterns.           |
| **Anonymous Classes**     | Creates objects of abstract classes or interfaces inline.                  |
| **Enum**                  | Predefined instances for a specific type (like a set of constants).        |

Each method has its use case depending on the complexity and requirements of the application.

## What is the Difference Between Class and Object in Java
### **Difference Between Class and Object in Java**

| **Aspect**           | **Class**                                                                                 | **Object**                                                                                           |
|-----------------------|-------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| **Definition**        | A class is a blueprint or template for creating objects.                                  | An object is an instance of a class that has a state and behavior.                                  |
| **Purpose**           | It defines the structure (attributes) and behavior (methods) that its objects will have.  | It represents a specific entity created based on the class.                                         |
| **Declaration**       | Defined using the `class` keyword.                                                       | Created using the `new` keyword or other object creation methods.                                   |
| **Memory Allocation** | Memory is allocated only when objects are created, not when a class is defined.           | Memory is allocated in the heap when an object is instantiated.                                     |
| **Existence**         | A class is abstract—it does not represent a real entity by itself.                        | An object is concrete—it represents a real entity with actual data and behavior.                    |
| **Components**        | Includes fields, methods, constructors, and nested classes.                              | Includes its own copy of the fields defined in the class and can access methods defined in the class.|
| **Example**           | ```java<br>class Car { String brand; void start() {} }```                                | ```java<br>Car myCar = new Car(); myCar.brand = "Toyota"; myCar.start();```                         |

---

### **Detailed Explanation**

#### **1. Class**
- **What is it?**
  - A class is a logical construct that defines the structure and behavior of objects.
  - It acts as a **template** or **blueprint** for objects.

- **Example**:
  ```java
  class Car {
      String brand;
      String model;

      void start() {
          System.out.println("Car is starting...");
      }
  }
  ```

#### **2. Object**
- **What is it?**
  - An object is a **physical instance** of a class.
  - It contains its own set of data (state) defined by the fields in the class and can perform actions (behavior) defined by the methods.

- **Example**:
  ```java
  Car myCar = new Car(); // Create an object
  myCar.brand = "Toyota"; // Set state
  myCar.model = "Corolla";
  myCar.start(); // Call behavior
  ```

#### **Relationship Between Class and Object**
- A class **cannot function** without objects since objects bring the class to life.
- Objects **cannot exist** without a class since they are based on the structure and behavior defined by the class.

---

### **Analogy**
- **Class**: Think of a **blueprint** of a house. It defines how the house will look, its rooms, and its features.
- **Object**: The **actual house** built based on the blueprint. Each house (object) has its own unique state (color, furniture) but follows the same structure defined by the blueprint (class).

---

### **Code Example: Class vs. Object**

```java
// Define a class (blueprint)
class Car {
    String brand;   // Field
    String model;   // Field
    int year;       // Field

    void displayDetails() { // Method
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
    }
}

public class Main {
    public static void main(String[] args) {
        // Create an object (instance of the class)
        Car myCar = new Car();
        
        // Set object's state
        myCar.brand = "Toyota";
        myCar.model = "Corolla";
        myCar.year = 2020;
        
        // Access object's behavior
        myCar.displayDetails();
    }
}
```

#### **Output:**
```
Brand: Toyota
Model: Corolla
Year: 2020
```

---

### **Key Takeaways**
1. **Class** is the design; **object** is the implementation.
2. You can create multiple objects from a single class, each with its own state.
3. Without a class, objects cannot exist; without objects, a class has no purpose.

## What is the static Keyword
### **The `static` Keyword in Java**

The `static` keyword in Java is used to define class-level members. It means that the member (field, method, block, or nested class) belongs to the class itself rather than to any specific instance of the class. Static members are shared among all objects of the class and can be accessed without creating an instance of the class.

---

### **Key Features of the `static` Keyword**
1. **Class-Level Scope**: Static members belong to the class, not to individual objects.
2. **Shared Memory**: Static members are stored in a common memory area, so all instances share the same value.
3. **Direct Access**: Static members can be accessed directly using the class name without creating an object.
4. **Performance**: Since static members are loaded once during class loading, they can improve performance when used wisely.

---

### **Where Can You Use the `static` Keyword?**
1. **Static Variables**
2. **Static Methods**
3. **Static Blocks**
4. **Static Nested Classes**

---

### **1. Static Variables**
Static variables are class-level variables that are shared by all objects of the class.

#### Example:
```java
class Counter {
    static int count = 0; // Static variable

    Counter() {
        count++; // Increment static variable
    }

    void displayCount() {
        System.out.println("Count: " + count);
    }
}

public class Main {
    public static void main(String[] args) {
        Counter c1 = new Counter();
        Counter c2 = new Counter();
        Counter c3 = new Counter();

        // Display count using one of the objects
        c3.displayCount(); // Output: Count: 3
    }
}
```

#### Explanation:
- The `count` variable is declared as `static`, so it is shared among all instances of the `Counter` class.
- Every time a new `Counter` object is created, the `count` variable is incremented.

---

### **2. Static Methods**
Static methods can be called without creating an object of the class. They can only access static variables or other static methods directly.

#### Example:
```java
class MathUtils {
    static int square(int x) {
        return x * x; // Static method
    }
}

public class Main {
    public static void main(String[] args) {
        // Accessing static method directly using class name
        int result = MathUtils.square(5);
        System.out.println("Square: " + result); // Output: Square: 25
    }
}
```

#### Explanation:
- The `square` method is declared as `static`, so it can be accessed directly using the class name (`MathUtils.square`).
- Static methods cannot access non-static members directly.

---

### **3. Static Blocks**
A static block is used to initialize static variables or execute code that needs to run once when the class is loaded.

#### Example:
```java
class StaticDemo {
    static int value;

    // Static block
    static {
        value = 42;
        System.out.println("Static block executed.");
    }
}

public class Main {
    public static void main(String[] args) {
        System.out.println("Value: " + StaticDemo.value); // Output: Static block executed. Value: 42
    }
}
```

#### Explanation:
- The static block is executed only once, when the class is loaded into memory.
- It is typically used for one-time initialization of static members.

---

### **4. Static Nested Classes**
A static nested class is a class declared inside another class with the `static` keyword. It can be accessed without creating an instance of the outer class.

#### Example:
```java
class Outer {
    static class Nested {
        void display() {
            System.out.println("This is a static nested class.");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        // Accessing the static nested class
        Outer.Nested nested = new Outer.Nested();
        nested.display(); // Output: This is a static nested class.
    }
}
```

#### Explanation:
- The `Nested` class is static, so it can be accessed without creating an instance of the `Outer` class.

---

### **Rules and Restrictions of `static` Keyword**
1. **Accessing Non-Static Members**:
   - Static methods and blocks cannot access non-static variables or methods directly.
   - To access non-static members, you must create an instance of the class.

   **Example:**
   ```java
   class Demo {
       int nonStaticVar = 10;

       static void show() {
           // This will cause an error:
           // System.out.println(nonStaticVar); 
           // Solution:
           Demo obj = new Demo();
           System.out.println(obj.nonStaticVar);
       }
   }
   ```

2. **Overriding and Inheritance**:
   - Static methods cannot be overridden, but they can be hidden by declaring a static method with the same name in the subclass.
   - Static variables and methods are not tied to instances, so they follow class-level inheritance.

---

### **Advantages of Using `static`**
1. **Memory Efficiency**: Static variables are stored in a shared memory space, reducing redundancy.
2. **Direct Access**: Static methods can be accessed without creating an object.
3. **Shared Data**: Static variables allow sharing of data between all instances of a class.

---

### **Disadvantages of Using `static`**
1. **Limited Scope**: Static methods cannot access non-static members directly.
2. **Overuse Can Be Harmful**: Using too many static variables or methods can lead to tightly coupled code and difficulty in testing.

---

### **Example: Using All Static Features**
```java
class Utility {
    static int value;

    // Static block
    static {
        value = 100;
        System.out.println("Static block executed. Value initialized to " + value);
    }

    // Static method
    static int getValue() {
        return value;
    }

    // Static nested class
    static class Helper {
        void printMessage() {
            System.out.println("Static nested class accessed.");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        // Accessing static variable and method
        System.out.println("Value: " + Utility.getValue());

        // Accessing static nested class
        Utility.Helper helper = new Utility.Helper();
        helper.printMessage();
    }
}
```

#### **Output**:
```
Static block executed. Value initialized to 100
Value: 100
Static nested class accessed.
```

---

### **Summary**

| Feature            | Description                                                                                                                                           |
|--------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Static Variables** | Shared by all instances of the class, belong to the class itself.                                                                                     |
| **Static Methods**  | Can be called without creating an object, used for operations that don't depend on instance variables.                                                |
| **Static Blocks**   | Executed once when the class is loaded, typically used for one-time initialization.                                                                   |
| **Static Nested Classes** | Inner classes that can be accessed without creating an object of the outer class, useful for logically grouping related classes together.         |

The `static` keyword is a powerful tool in Java that, when used appropriately, can improve memory efficiency, simplify access to shared data, and facilitate utility methods.

## What is a Static Method and Instance Method
### **Static Method and Instance Method in Java**

Java methods are classified into two types based on how they are associated with a class or object:

1. **Static Method**: Associated with the class itself.
2. **Instance Method**: Associated with an object of the class.

Let’s dive deeper into the differences, features, and use cases for both.

---

### **1. Static Method**

A **static method** is a method that belongs to the **class** rather than any specific instance of the class. It is declared using the `static` keyword. Static methods can be called directly using the class name without creating an object.

#### **Characteristics of Static Methods**
- Belong to the class, not any specific object.
- Can be accessed using the class name.
- Can only access **static variables** or call **static methods** directly.
- Cannot use the `this` or `super` keywords (since they are tied to instances).
- Commonly used for utility or helper methods that perform generic tasks.

#### **Syntax**
```java
class ClassName {
    static void methodName() {
        // method body
    }
}
```

#### **Example of a Static Method**
```java
class Calculator {
    // Static method to calculate the square of a number
    static int square(int number) {
        return number * number;
    }
}

public class Main {
    public static void main(String[] args) {
        // Calling static method using class name
        int result = Calculator.square(5);
        System.out.println("Square: " + result); // Output: Square: 25
    }
}
```

#### **Explanation**
- The `square` method is static, so it can be accessed directly using the class name `Calculator.square(5)`.

---

### **2. Instance Method**

An **instance method** is a method that belongs to an **object** of the class. It is implicitly tied to the object and can access both **instance variables** and **static variables**.

#### **Characteristics of Instance Methods**
- Belong to an object of the class.
- Can be accessed only after creating an instance of the class.
- Can access both instance and static variables and methods.
- Can use `this` to refer to the current object.
- Often used to define behaviors that depend on the specific state of an object.

#### **Syntax**
```java
class ClassName {
    void methodName() {
        // method body
    }
}
```

#### **Example of an Instance Method**
```java
class Person {
    String name;

    // Instance method to display the person's name
    void displayName() {
        System.out.println("Name: " + name);
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating an object
        Person person = new Person();
        person.name = "Alice";

        // Calling the instance method
        person.displayName(); // Output: Name: Alice
    }
}
```

#### **Explanation**
- The `displayName` method is an instance method, so it must be accessed using an object (`person.displayName()`).

---

### **Differences Between Static Method and Instance Method**

| **Aspect**               | **Static Method**                                                                                   | **Instance Method**                                                                                     |
|--------------------------|-----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| **Belongs To**            | Class                                                                                            | Object                                                                                                 |
| **Access**                | Can be called using the class name.                                                               | Can only be called using an object of the class.                                                       |
| **Access to Static Members** | Can directly access only static variables and static methods.                                     | Can access both static and instance variables/methods.                                                 |
| **Access to Instance Members** | Cannot directly access instance variables/methods (requires an object).                          | Can directly access instance variables/methods.                                                        |
| **Usage**                 | Typically used for utility, helper, or global functionality that doesn't depend on object state.   | Used for behaviors or operations that depend on the object's state.                                    |
| **Keywords**              | Cannot use `this` or `super`.                                                                     | Can use `this` and `super` to refer to the current object or parent class.                             |

---

### **Example: Static vs Instance Methods**

```java
class Demo {
    int instanceVar = 10; // Instance variable
    static int staticVar = 20; // Static variable

    // Static method
    static void staticMethod() {
        System.out.println("Static Method");
        // Accessing static variable
        System.out.println("Static Variable: " + staticVar);
        // Cannot directly access instance variables
        // System.out.println("Instance Variable: " + instanceVar); // Error
    }

    // Instance method
    void instanceMethod() {
        System.out.println("Instance Method");
        // Accessing instance variable
        System.out.println("Instance Variable: " + instanceVar);
        // Accessing static variable
        System.out.println("Static Variable: " + staticVar);
    }
}

public class Main {
    public static void main(String[] args) {
        // Calling static method
        Demo.staticMethod();

        // Creating an object to call instance method
        Demo demo = new Demo();
        demo.instanceMethod();
    }
}
```

#### **Output**
```
Static Method
Static Variable: 20
Instance Method
Instance Variable: 10
Static Variable: 20
```

#### **Explanation**
1. The `staticMethod` is called using the class name `Demo.staticMethod()`. It can access the static variable but not the instance variable directly.
2. The `instanceMethod` is called using the object `demo.instanceMethod()`. It can access both instance and static variables.

---

### **Advantages of Static Methods**
1. No need to create an object for usage, saving memory.
2. Useful for utility or helper methods (e.g., `Math.max`, `Math.sqrt`).
3. Shared behavior across all instances.

---

### **Advantages of Instance Methods**
1. Tied to the object, allowing them to work with the object's unique state.
2. More flexible, as they can access both static and instance variables.
3. Useful for object-specific behavior.

---

### **When to Use Static vs Instance Methods**
- Use **static methods** when:
  - The method doesn’t rely on instance variables.
  - The method provides utility functionality (e.g., mathematical calculations, helper functions).
- Use **instance methods** when:
  - The method depends on the object's state or instance variables.
  - You want to modify or interact with the specific instance of the class.

---

### **Real-World Analogy**
- **Static Method**: A "global" service like a calculator. You can use its functionality without needing to "personalize" it.
- **Instance Method**: A "personalized" service like your bank account, where the behavior depends on your account balance and details.

By understanding static and instance methods, you can design classes that are efficient, modular, and easy to maintain!

## What is a Static Variable and Instance Variable
### **Static Variable and Instance Variable in Java**

In Java, variables declared in a class can be either **static** or **instance** variables, depending on how they are defined and used. Understanding the differences between these types of variables is crucial for efficient and effective object-oriented programming.

---

### **1. Static Variable**

A **static variable** is a variable that belongs to the **class** rather than to any specific object. It is shared among all objects of the class and retains its value for the lifetime of the program.

#### **Characteristics of Static Variables**
- Declared using the `static` keyword.
- Shared across all instances of the class.
- Stored in a common memory area (Class Area/Method Area).
- Initialized only once, at the time of class loading.
- Can be accessed using the class name or an instance of the class.
- Its value persists between method calls or object creations.

#### **Syntax**
```java
class ClassName {
    static DataType variableName;
}
```

#### **Example of a Static Variable**
```java
class Counter {
    static int count = 0; // Static variable

    Counter() {
        count++; // Increment static variable
    }

    void displayCount() {
        System.out.println("Count: " + count);
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating multiple objects
        Counter c1 = new Counter();
        Counter c2 = new Counter();
        Counter c3 = new Counter();

        // Accessing the shared static variable
        c1.displayCount(); // Output: Count: 3
    }
}
```

#### **Explanation**
- The `count` variable is static, so it is shared among all objects of the `Counter` class.
- Each time a `Counter` object is created, the `count` variable is incremented, and the updated value is reflected across all instances.

---

### **2. Instance Variable**

An **instance variable** is a variable that belongs to a specific **object** of the class. Each object maintains its own copy of the instance variable, and the value can differ between objects.

#### **Characteristics of Instance Variables**
- Declared without the `static` keyword.
- Unique to each instance of the class.
- Stored in the heap memory.
- Initialized when the object is created.
- Its scope is limited to the object that owns it.

#### **Syntax**
```java
class ClassName {
    DataType variableName; // Instance variable
}
```

#### **Example of an Instance Variable**
```java
class Person {
    String name; // Instance variable

    Person(String name) {
        this.name = name; // Assign value to instance variable
    }

    void displayName() {
        System.out.println("Name: " + name);
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating multiple objects
        Person p1 = new Person("Alice");
        Person p2 = new Person("Bob");

        // Accessing instance variables through objects
        p1.displayName(); // Output: Name: Alice
        p2.displayName(); // Output: Name: Bob
    }
}
```

#### **Explanation**
- Each `Person` object has its own `name` instance variable.
- The `p1` object holds the name `"Alice"`, while the `p2` object holds `"Bob"`. The values are independent.

---

### **Differences Between Static and Instance Variables**

| **Aspect**             | **Static Variable**                                                  | **Instance Variable**                                              |
|------------------------|----------------------------------------------------------------------|----------------------------------------------------------------------|
| **Belongs To**          | Class (shared among all objects).                                   | Object (each object has its own copy).                             |
| **Keyword**             | Declared using the `static` keyword.                                | Declared without the `static` keyword.                             |
| **Memory Allocation**   | Allocated once, in the method area (Class Area).                    | Allocated separately for each object, in the heap.                 |
| **Initialization**      | Initialized at the time of class loading.                          | Initialized when an object is created.                             |
| **Access**              | Can be accessed using the class name or object reference.           | Can only be accessed through an object.                            |
| **Shared or Unique**    | Shared among all instances.                                         | Unique to each instance of the class.                              |

---

### **Examples Showing Both Static and Instance Variables**

#### **Example 1: Static and Instance Variables Together**
```java
class Student {
    static String schoolName = "Greenwood High"; // Static variable
    String studentName; // Instance variable

    Student(String name) {
        this.studentName = name;
    }

    void displayInfo() {
        System.out.println("Student Name: " + studentName);
        System.out.println("School Name: " + schoolName);
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating objects
        Student s1 = new Student("Alice");
        Student s2 = new Student("Bob");

        // Displaying information
        s1.displayInfo();
        s2.displayInfo();

        // Modifying static variable
        Student.schoolName = "Bright Future Academy";

        // Displaying updated information
        s1.displayInfo();
        s2.displayInfo();
    }
}
```

#### **Output**
```
Student Name: Alice
School Name: Greenwood High
Student Name: Bob
School Name: Greenwood High
Student Name: Alice
School Name: Bright Future Academy
Student Name: Bob
School Name: Bright Future Academy
```

#### **Explanation**
1. `schoolName` is a static variable, so it is shared between all `Student` objects.
2. Modifying `schoolName` changes it for all objects.
3. `studentName` is an instance variable, so each object has its own unique value.

---

#### **Example 2: Accessing Static Variables Without Creating an Object**
```java
class Utility {
    static String toolName = "Calculator"; // Static variable

    static void displayTool() {
        System.out.println("Tool: " + toolName);
    }
}

public class Main {
    public static void main(String[] args) {
        // Accessing static variable directly using the class name
        System.out.println(Utility.toolName); // Output: Calculator

        // Calling static method
        Utility.displayTool(); // Output: Tool: Calculator
    }
}
```

---

### **Key Points to Remember**

1. **Static Variables**:
   - Shared among all objects of the class.
   - Can be accessed without creating an instance.
   - Useful for storing common properties or constants.

2. **Instance Variables**:
   - Unique to each object.
   - Require an instance of the class to access.
   - Useful for storing object-specific data.

---

### **Real-World Analogy**

1. **Static Variable**: 
   - Like a common "notice board" shared by all students in a school. The content is visible and accessible to everyone, and any update affects all students.

2. **Instance Variable**:
   - Like a "student ID card." Each student has their own card, and the information on it is unique to the student.

By understanding static and instance variables, you can design efficient and scalable Java programs that leverage both shared and unique data effectively!

## What is a Default Method
### **What is a Default Method in Java?**

A **default method** in Java is a method that is defined in an interface with a **default implementation**. It was introduced in **Java 8** to provide backward compatibility in interfaces without forcing the implementing classes to override the method. 

Before Java 8, all methods in an interface were abstract by default. With default methods, interfaces can now include methods with bodies.

---

### **Characteristics of Default Methods**

1. **Defined in an Interface**: Default methods are written within an interface.
2. **Keyword**: Declared using the `default` keyword.
3. **Default Implementation**: Provides a concrete implementation so that implementing classes don’t need to override them unless they want to provide a specific implementation.
4. **Optional Override**: Implementing classes can override the default method if needed.
5. **Backward Compatibility**: Enables interface updates without breaking existing implementations.

---

### **Syntax of Default Method**
```java
interface InterfaceName {
    default void methodName() {
        // default implementation
    }
}
```

---

### **Example of a Default Method**

#### Code Example
```java
interface Vehicle {
    // Abstract method
    void start();

    // Default method
    default void stop() {
        System.out.println("The vehicle is stopping.");
    }
}

class Car implements Vehicle {
    @Override
    public void start() {
        System.out.println("The car is starting.");
    }

    // Override the default method (optional)
    @Override
    public void stop() {
        System.out.println("The car is stopping.");
    }
}

class Bike implements Vehicle {
    @Override
    public void start() {
        System.out.println("The bike is starting.");
    }
    // Using default implementation of stop()
}

public class Main {
    public static void main(String[] args) {
        Vehicle car = new Car();
        car.start();
        car.stop(); // Custom implementation in Car

        Vehicle bike = new Bike();
        bike.start();
        bike.stop(); // Default implementation from Vehicle
    }
}
```

#### **Output**
```
The car is starting.
The car is stopping.
The bike is starting.
The vehicle is stopping.
```

---

### **Explanation**

1. The `Vehicle` interface has:
   - An abstract method `start()` that must be implemented by all classes.
   - A default method `stop()` that provides a default implementation.
2. The `Car` class overrides both `start()` and `stop()`, providing its own implementation.
3. The `Bike` class only overrides `start()` and uses the default implementation of `stop()`.

---

### **Advantages of Default Methods**

1. **Backward Compatibility**: Allows you to add new methods to an interface without affecting existing implementations.
   - Before Java 8, adding a new method to an interface required all implementing classes to define the method, potentially breaking existing code.
2. **Code Reusability**: Provides reusable methods that all implementing classes can share without duplication.
3. **Optional Override**: Implementing classes can use the default implementation or override it to provide specific behavior.

---

### **Use Cases of Default Methods**

1. **Enhancing Interfaces**:
   - If a new method needs to be added to an interface, it can be done as a default method to avoid breaking older implementations.
2. **Multiple Implementations**:
   - Allows you to provide shared functionality in interfaces while still allowing classes to customize behavior.

---

### **Example: Enhancing Interfaces with Default Methods**

#### Code Example
```java
interface Payment {
    void makePayment(double amount);

    // Default method
    default void sendReceipt() {
        System.out.println("Receipt sent to the customer.");
    }
}

class CreditCardPayment implements Payment {
    @Override
    public void makePayment(double amount) {
        System.out.println("Paid " + amount + " using Credit Card.");
    }

    // Using default sendReceipt implementation
}

class PayPalPayment implements Payment {
    @Override
    public void makePayment(double amount) {
        System.out.println("Paid " + amount + " using PayPal.");
    }

    // Custom implementation of sendReceipt
    @Override
    public void sendReceipt() {
        System.out.println("PayPal receipt sent to the customer.");
    }
}

public class Main {
    public static void main(String[] args) {
        Payment creditCard = new CreditCardPayment();
        creditCard.makePayment(100.0);
        creditCard.sendReceipt(); // Default implementation

        Payment payPal = new PayPalPayment();
        payPal.makePayment(200.0);
        payPal.sendReceipt(); // Custom implementation
    }
}
```

#### **Output**
```
Paid 100.0 using Credit Card.
Receipt sent to the customer.
Paid 200.0 using PayPal.
PayPal receipt sent to the customer.
```

---

### **Rules for Default Methods**

1. **Cannot Override `Object` Methods**: Default methods cannot override methods from the `Object` class (e.g., `toString()`, `equals()`, etc.).
   - **Example**:
     ```java
     interface Example {
         // This will cause a compile-time error
         default String toString() {
             return "Default toString";
         }
     }
     ```
2. **Conflict Resolution**:
   - If a class implements multiple interfaces with the same default method, the implementing class must override the method explicitly.

---

### **Example: Conflict Resolution**

#### Code Example
```java
interface InterfaceA {
    default void show() {
        System.out.println("Default method from InterfaceA");
    }
}

interface InterfaceB {
    default void show() {
        System.out.println("Default method from InterfaceB");
    }
}

class MyClass implements InterfaceA, InterfaceB {
    @Override
    public void show() {
        // Resolving the conflict
        System.out.println("Default method overridden in MyClass");
    }
}

public class Main {
    public static void main(String[] args) {
        MyClass obj = new MyClass();
        obj.show();
    }
}
```

#### **Output**
```
Default method overridden in MyClass
```

#### **Explanation**
- Since `InterfaceA` and `InterfaceB` both have a `show()` default method, the `MyClass` must override it to resolve the ambiguity.

---

### **Key Points to Remember**

1. **Default Methods and Abstract Classes**:
   - Default methods allow interfaces to have a limited form of concrete methods, bridging the gap between interfaces and abstract classes.
2. **Static Methods in Interfaces**:
   - Java 8 also introduced **static methods in interfaces**. These are different from default methods as they cannot be overridden or accessed by implementing classes.

---

### **Conclusion**

Default methods in Java provide flexibility by allowing interfaces to include methods with default implementations. This feature improves code reusability, supports backward compatibility, and allows more robust interface design. By understanding when and how to use default methods, you can design cleaner and more maintainable Java programs.

## What are Object-Oriented Principles in Java
### **Object-Oriented Principles in Java**

Object-Oriented Programming (OOP) is a programming paradigm based on the concept of "objects," which contain data (fields) and behavior (methods). Java, as an object-oriented language, supports **four core principles** of OOP:

1. **Encapsulation**
2. **Inheritance**
3. **Polymorphism**
4. **Abstraction**

Let’s explore each principle in detail with examples and explanations.

---

### **1. Encapsulation**

Encapsulation is the practice of bundling data (variables) and methods (functions) that operate on the data into a single unit, usually a class. It restricts direct access to certain components of an object to ensure controlled modification.

#### **Key Features**
- Protects the internal state of an object from unintended modification.
- Access to the data is provided via **getter** and **setter** methods.
- Achieved using **access modifiers** (`private`, `public`, `protected`).

#### **Example**
```java
class BankAccount {
    private double balance; // Encapsulated data

    // Constructor
    public BankAccount(double initialBalance) {
        this.balance = initialBalance;
    }

    // Getter method
    public double getBalance() {
        return balance;
    }

    // Setter method
    public void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
        } else {
            System.out.println("Deposit amount must be positive.");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        BankAccount account = new BankAccount(500);
        System.out.println("Initial Balance: " + account.getBalance());

        account.deposit(100); // Valid access through setter
        System.out.println("Updated Balance: " + account.getBalance());

        // Direct access to balance is not allowed
        // account.balance = 1000; // This will cause an error
    }
}
```

#### **Output**
```
Initial Balance: 500.0
Updated Balance: 600.0
```

---

### **2. Inheritance**

Inheritance allows a class (child or subclass) to acquire the properties and behaviors of another class (parent or superclass). It promotes code reuse and establishes a relationship between classes.

#### **Key Features**
- Achieved using the `extends` keyword.
- The parent class can provide common functionality to multiple child classes.
- The child class can override methods from the parent class.

#### **Example**
```java
// Parent class
class Animal {
    void eat() {
        System.out.println("This animal eats food.");
    }
}

// Child class
class Dog extends Animal {
    void bark() {
        System.out.println("The dog barks.");
    }
}

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.eat(); // Inherited from Animal
        dog.bark(); // Defined in Dog
    }
}
```

#### **Output**
```
This animal eats food.
The dog barks.
```

---

### **3. Polymorphism**

Polymorphism means "many forms." In Java, it allows a single entity (e.g., method or object) to take multiple forms. Polymorphism is achieved through **method overloading** (compile-time polymorphism) and **method overriding** (runtime polymorphism).

#### **Key Features**
- **Method Overloading**: Multiple methods with the same name but different parameters.
- **Method Overriding**: A subclass provides a specific implementation of a method from the parent class.
- Promotes flexibility and scalability in code.

---

#### **Example of Method Overloading**
```java
class Calculator {
    // Method to add two numbers
    int add(int a, int b) {
        return a + b;
    }

    // Overloaded method to add three numbers
    int add(int a, int b, int c) {
        return a + b + c;
    }
}

public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        System.out.println("Sum (2 args): " + calc.add(10, 20));
        System.out.println("Sum (3 args): " + calc.add(10, 20, 30));
    }
}
```

#### **Output**
```
Sum (2 args): 30
Sum (3 args): 60
```

---

#### **Example of Method Overriding**
```java
class Animal {
    void makeSound() {
        System.out.println("This animal makes a sound.");
    }
}

class Dog extends Animal {
    @Override
    void makeSound() {
        System.out.println("The dog barks.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal animal = new Dog(); // Polymorphic reference
        animal.makeSound(); // Calls the overridden method in Dog
    }
}
```

#### **Output**
```
The dog barks.
```

---

### **4. Abstraction**

Abstraction is the process of hiding implementation details and showing only the essential features of an object. It is achieved through **abstract classes** and **interfaces**.

#### **Key Features**
- Focuses on **what** an object does rather than **how** it does it.
- Abstract classes can have both abstract (no implementation) and concrete methods.
- Interfaces allow full abstraction, where all methods are implicitly abstract (prior to Java 8).

---

#### **Example of Abstract Class**
```java
abstract class Shape {
    // Abstract method
    abstract void draw();

    // Concrete method
    void displayShape() {
        System.out.println("This is a shape.");
    }
}

class Circle extends Shape {
    @Override
    void draw() {
        System.out.println("Drawing a circle.");
    }
}

public class Main {
    public static void main(String[] args) {
        Shape shape = new Circle();
        shape.displayShape();
        shape.draw();
    }
}
```

#### **Output**
```
This is a shape.
Drawing a circle.
```

---

#### **Example of Interface**
```java
interface Vehicle {
    void start(); // Abstract method
}

class Car implements Vehicle {
    @Override
    public void start() {
        System.out.println("The car is starting.");
    }
}

public class Main {
    public static void main(String[] args) {
        Vehicle car = new Car();
        car.start();
    }
}
```

#### **Output**
```
The car is starting.
```

---

### **Key Differences Between OOP Principles**

| Principle      | Description                                                                                      | Example                                                                                          |
|----------------|--------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| **Encapsulation** | Bundles data and methods together, restricts direct access to the data.                           | Use `private` variables with `getter` and `setter` methods.                                     |
| **Inheritance**   | Enables one class to inherit fields and methods from another class.                             | `class Dog extends Animal`                                                                      |
| **Polymorphism**  | Allows methods to behave differently based on the object.                                        | Method Overloading and Overriding                                                               |
| **Abstraction**   | Hides implementation details and exposes only the required functionality.                        | Use `abstract class` or `interface`.                                                           |

---

### **Advantages of Object-Oriented Principles**

1. **Modularity**: Code is organized into objects, making it easy to understand and maintain.
2. **Reusability**: Inheritance and polymorphism promote code reuse.
3. **Flexibility**: Polymorphism allows dynamic method calls.
4. **Scalability**: Encapsulation ensures that changes in one part of the code do not affect other parts.
5. **Security**: Encapsulation restricts direct access to critical data.

---

By adhering to object-oriented principles, Java developers can create scalable, reusable, and maintainable software systems. These principles are the foundation of effective object-oriented design.

## What is Abstraction
### **What is Abstraction in Java?**

**Abstraction** is one of the core principles of Object-Oriented Programming (OOP). It is the process of **hiding implementation details** and exposing only the essential features of an object. The goal is to reduce complexity and increase efficiency by focusing on **what** an object does rather than **how** it does it.

---

### **Key Characteristics of Abstraction**
1. **Hides Complexity**: Implementation details are hidden from the user; only relevant information is exposed.
2. **Focus on Essentials**: Users interact with the interface or abstract class without worrying about the underlying logic.
3. **Implemented Using**:
   - **Abstract Classes**
   - **Interfaces**

---

### **Why Use Abstraction?**
1. **Modularity**: Makes the code modular and easy to manage.
2. **Security**: Protects sensitive details of the implementation.
3. **Flexibility**: Allows changes to the implementation without affecting the user code.
4. **Reusability**: Encourages the reuse of abstract definitions across multiple classes.

---

### **How Abstraction Works in Java**

#### **1. Using Abstract Classes**
An **abstract class** is a class that cannot be instantiated and may contain:
- Abstract methods (methods without a body).
- Concrete methods (methods with a body).

#### **Syntax**
```java
abstract class ClassName {
    // Abstract method
    abstract void abstractMethod();

    // Concrete method
    void concreteMethod() {
        System.out.println("This is a concrete method.");
    }
}
```

---

#### **Example of Abstraction Using Abstract Classes**

```java
abstract class Animal {
    // Abstract method
    abstract void sound();

    // Concrete method
    void eat() {
        System.out.println("This animal eats food.");
    }
}

class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("The dog barks.");
    }
}

class Cat extends Animal {
    @Override
    void sound() {
        System.out.println("The cat meows.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog();
        dog.sound(); // Output: The dog barks.
        dog.eat();   // Output: This animal eats food.

        Animal cat = new Cat();
        cat.sound(); // Output: The cat meows.
        cat.eat();   // Output: This animal eats food.
    }
}
```

#### **Explanation**
- The `Animal` class defines the abstract method `sound()` and the concrete method `eat()`.
- Both `Dog` and `Cat` classes provide their own implementations of the `sound()` method while reusing the `eat()` method from the `Animal` class.
- This demonstrates **abstraction**, as the user interacts with the `Animal` class without knowing the exact implementation in `Dog` or `Cat`.

---

#### **2. Using Interfaces**
An **interface** in Java is a collection of abstract methods (and optionally default and static methods from Java 8 onward). Classes that implement the interface must provide concrete implementations for the abstract methods.

#### **Syntax**
```java
interface InterfaceName {
    void method1(); // Abstract method
}
```

---

#### **Example of Abstraction Using Interfaces**

```java
interface Vehicle {
    void start(); // Abstract method
    void stop();  // Abstract method
}

class Car implements Vehicle {
    @Override
    public void start() {
        System.out.println("The car is starting.");
    }

    @Override
    public void stop() {
        System.out.println("The car is stopping.");
    }
}

class Bike implements Vehicle {
    @Override
    public void start() {
        System.out.println("The bike is starting.");
    }

    @Override
    public void stop() {
        System.out.println("The bike is stopping.");
    }
}

public class Main {
    public static void main(String[] args) {
        Vehicle car = new Car();
        car.start(); // Output: The car is starting.
        car.stop();  // Output: The car is stopping.

        Vehicle bike = new Bike();
        bike.start(); // Output: The bike is starting.
        bike.stop();  // Output: The bike is stopping.
    }
}
```

#### **Explanation**
- The `Vehicle` interface defines two abstract methods: `start()` and `stop()`.
- The `Car` and `Bike` classes implement the interface and provide their own behavior for these methods.
- The user interacts with the `Vehicle` interface, not knowing the specific implementation provided by `Car` or `Bike`.

---

### **Abstract Classes vs Interfaces**

| **Aspect**                | **Abstract Class**                                              | **Interface**                                                  |
|---------------------------|---------------------------------------------------------------|---------------------------------------------------------------|
| **Inheritance**            | Can extend only one abstract class.                          | A class can implement multiple interfaces.                    |
| **Access Modifiers**       | Can have `public`, `protected`, and `private` members.        | Methods are `public` and `abstract` by default (except static and default methods). |
| **Constructors**           | Can have constructors.                                       | Cannot have constructors.                                     |
| **Fields**                 | Can have instance variables.                                 | Fields are `public`, `static`, and `final` by default.        |
| **Methods**                | Can have both abstract and concrete methods.                 | All methods are abstract (before Java 8) or default/static (Java 8 onward). |

---

### **Real-World Analogy**

1. **Abstract Class**:
   - Think of `Animal` as a general blueprint for living creatures. It defines general characteristics like eating, sleeping, and making sounds, but it doesn't specify the exact sound.
   - Specific animals (`Dog`, `Cat`) provide their own implementations of `sound()`.

2. **Interface**:
   - Think of a `Vehicle` interface like a contract. Any class that implements `Vehicle` must define methods like `start()` and `stop()`, but the exact implementation will vary depending on whether it’s a `Car`, `Bike`, or `Bus`.

---

### **Advantages of Abstraction**

1. **Reduces Complexity**: Hides unnecessary details from the user, focusing only on relevant functionality.
2. **Improves Code Reusability**: Common functionality can be defined in abstract classes or interfaces and reused across different classes.
3. **Facilitates Maintenance**: Implementation details can be changed without affecting the user-facing code.
4. **Promotes Scalability**: New functionalities can be added easily without disrupting existing code.

---

### **Disadvantages of Abstraction**

1. **Initial Complexity**: Designing abstract classes or interfaces requires careful planning.
2. **Performance Overhead**: Abstract layers may introduce minor performance overhead.
3. **Restricted to Object-Oriented Design**: Only applicable in OOP.

---

### **Summary**

- **Abstraction** is a technique to simplify the design and implementation of complex systems.
- It focuses on **what** an object does rather than **how** it does it.
- Achieved using:
  - **Abstract Classes**: Used when there’s shared code among subclasses.
  - **Interfaces**: Used for defining contracts for unrelated classes.
- Helps in creating scalable, reusable, and maintainable code by hiding unnecessary details from the user.

By applying abstraction effectively, developers can design robust and modular software systems.

## What is Encapsulation
### **What is Encapsulation in Java?**

**Encapsulation** is one of the fundamental principles of Object-Oriented Programming (OOP). It refers to the bundling of data (variables) and methods (functions) that operate on that data into a single unit, typically a **class**. Encapsulation restricts direct access to some of an object’s components, which means the internal representation of an object is hidden from the outside.

The primary goal of encapsulation is to ensure that:
- The internal state of an object is hidden from the outside world.
- Control over how the data is accessed or modified is achieved using **getter** and **setter** methods.

---

### **Key Features of Encapsulation**
1. **Data Hiding**: Restricts access to the internal state of the object using access modifiers (`private`, `protected`, etc.).
2. **Controlled Access**: Provides access to the data only through public methods (getters and setters).
3. **Improves Security**: Protects the object from unintended or harmful changes.
4. **Improves Maintainability**: Changes to internal implementation do not affect external code.

---

### **How to Achieve Encapsulation in Java**

Encapsulation is achieved in Java by:
1. Declaring the **fields (variables)** of a class as `private`.
2. Providing **public getter and setter methods** to access and update the values of private fields.

#### **Syntax**
```java
class ClassName {
    // Private fields
    private DataType fieldName;

    // Getter method
    public DataType getFieldName() {
        return fieldName;
    }

    // Setter method
    public void setFieldName(DataType value) {
        fieldName = value;
    }
}
```

---

### **Example of Encapsulation**

#### Code Example:
```java
class BankAccount {
    // Private fields
    private String accountHolderName;
    private double balance;

    // Constructor
    public BankAccount(String accountHolderName, double initialBalance) {
        this.accountHolderName = accountHolderName;
        this.balance = initialBalance;
    }

    // Getter for accountHolderName
    public String getAccountHolderName() {
        return accountHolderName;
    }

    // Setter for accountHolderName
    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    // Getter for balance
    public double getBalance() {
        return balance;
    }

    // Method to deposit money
    public void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
        } else {
            System.out.println("Invalid deposit amount!");
        }
    }

    // Method to withdraw money
    public void withdraw(double amount) {
        if (amount > 0 && amount <= balance) {
            balance -= amount;
        } else {
            System.out.println("Invalid withdrawal amount!");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        // Create a BankAccount object
        BankAccount account = new BankAccount("Alice", 5000);

        // Access and modify data using methods
        System.out.println("Account Holder: " + account.getAccountHolderName());
        System.out.println("Balance: " + account.getBalance());

        account.deposit(2000); // Deposit money
        System.out.println("Updated Balance: " + account.getBalance());

        account.withdraw(1000); // Withdraw money
        System.out.println("Balance after Withdrawal: " + account.getBalance());

        // Direct access to balance is restricted
        // account.balance = 10000; // This will cause an error
    }
}
```

#### **Output**
```
Account Holder: Alice
Balance: 5000.0
Updated Balance: 7000.0
Balance after Withdrawal: 6000.0
```

---

### **Explanation**
1. **Data Hiding**:
   - The fields `accountHolderName` and `balance` are declared as `private`, restricting direct access from outside the class.
2. **Controlled Access**:
   - The `getAccountHolderName`, `getBalance`, `deposit`, and `withdraw` methods provide controlled access to the private fields.
3. **Security**:
   - The deposit and withdrawal logic ensures that only valid operations are allowed.

---

### **Advantages of Encapsulation**
1. **Data Security**:
   - By restricting direct access, encapsulation protects the data from being corrupted or mishandled.
2. **Modular Design**:
   - Makes it easier to change or extend the implementation without affecting external code.
3. **Code Maintainability**:
   - Changes to internal logic can be made without modifying the interface (methods).
4. **Flexibility**:
   - Allows validation or additional logic to be implemented within getter and setter methods.

---

### **Real-World Analogy**

Think of a **bank ATM**:
- You interact with the ATM interface (like getter and setter methods) to withdraw or deposit money.
- You don’t directly access the bank’s database or internal mechanisms (data hiding).
- The ATM ensures only valid transactions are processed, protecting the bank’s data (security).

---

### **Encapsulation vs Data Hiding**

| **Aspect**         | **Encapsulation**                                    | **Data Hiding**                                    |
|--------------------|-----------------------------------------------------|--------------------------------------------------|
| **Definition**      | Encapsulation combines data and methods in a single unit. | Data hiding restricts direct access to the data. |
| **Purpose**         | Focuses on creating a modular, maintainable design. | Focuses on securing sensitive data.             |
| **Implementation**  | Achieved using private fields and public methods.  | Achieved using private access modifiers.         |

---

### **Encapsulation and Access Modifiers**

Access modifiers control the visibility of class members. Commonly used modifiers for encapsulation:
1. `private`: Restricts access to within the class.
2. `public`: Allows access from outside the class.
3. `protected` and default (package-private) modifiers are also useful for encapsulation in some cases.

---

### **Best Practices for Encapsulation**
1. Declare variables as `private` to ensure they cannot be accessed directly.
2. Use meaningful names for getter and setter methods.
3. Add validation logic in setter methods if necessary.
4. Avoid exposing internal data structures directly. Return copies if needed.

---

### **Another Example: Employee Class**

#### Code Example:
```java
class Employee {
    private String name;
    private double salary;

    // Constructor
    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    // Getter for name
    public String getName() {
        return name;
    }

    // Setter for name
    public void setName(String name) {
        this.name = name;
    }

    // Getter for salary
    public double getSalary() {
        return salary;
    }

    // Setter for salary with validation
    public void setSalary(double salary) {
        if (salary > 0) {
            this.salary = salary;
        } else {
            System.out.println("Salary must be positive!");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Employee emp = new Employee("John Doe", 50000);

        // Access data through getters
        System.out.println("Employee Name: " + emp.getName());
        System.out.println("Employee Salary: " + emp.getSalary());

        // Modify data through setters
        emp.setSalary(60000);
        System.out.println("Updated Salary: " + emp.getSalary());

        emp.setSalary(-5000); // Invalid value
    }
}
```

#### **Output**
```
Employee Name: John Doe
Employee Salary: 50000.0
Updated Salary: 60000.0
Salary must be positive!
```

---

### **Conclusion**

**Encapsulation** is a fundamental principle of OOP that ensures data security, maintainability, and modular design by restricting direct access to fields and controlling it through public methods. This approach not only enhances code quality but also ensures flexibility in changing implementation details without affecting the external behavior of the class. By using encapsulation effectively, developers can create robust and scalable Java applications.

## What is Inheritance
### **What is Inheritance in Java?**

**Inheritance** is one of the fundamental principles of Object-Oriented Programming (OOP). It is the mechanism by which one class (**child class**) acquires the properties and behaviors (fields and methods) of another class (**parent class**). Inheritance promotes **code reuse**, **modularity**, and **extensibility**, allowing a subclass to extend and enhance the functionality of a superclass.

---

### **Key Features of Inheritance**
1. **Code Reusability**:
   - Common functionality in a superclass can be reused in multiple subclasses.
2. **Extensibility**:
   - Subclasses can extend the functionality of the parent class by adding new methods or overriding existing ones.
3. **Hierarchy**:
   - Inheritance creates a parent-child hierarchy among classes.
4. **Polymorphism**:
   - Inheritance is essential for runtime polymorphism, allowing a subclass to define its version of a parent class method.

---

### **Syntax of Inheritance**
Inheritance is implemented using the `extends` keyword in Java.

#### **Syntax**
```java
class ParentClass {
    // fields and methods
}

class ChildClass extends ParentClass {
    // additional fields and methods
}
```

---

### **Types of Inheritance in Java**

1. **Single Inheritance**:
   - A class inherits from a single parent class.

2. **Multilevel Inheritance**:
   - A class inherits from another class, which itself is inherited from another class.

3. **Hierarchical Inheritance**:
   - Multiple child classes inherit from a single parent class.

4. **Hybrid Inheritance**:
   - A combination of two or more types of inheritance. However, Java does not support multiple inheritance directly to avoid ambiguity (can be achieved using interfaces).

---

### **Examples of Inheritance**

#### **1. Single Inheritance**

```java
// Parent class
class Animal {
    void eat() {
        System.out.println("This animal eats food.");
    }
}

// Child class
class Dog extends Animal {
    void bark() {
        System.out.println("The dog barks.");
    }
}

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.eat(); // Inherited from Animal
        dog.bark(); // Defined in Dog
    }
}
```

#### **Output**
```
This animal eats food.
The dog barks.
```

#### **Explanation**:
- The `Dog` class inherits the `eat()` method from the `Animal` class.
- The `Dog` class has its own method, `bark()`.

---

#### **2. Multilevel Inheritance**

```java
// Grandparent class
class Vehicle {
    void start() {
        System.out.println("Vehicle is starting.");
    }
}

// Parent class
class Car extends Vehicle {
    void drive() {
        System.out.println("Car is driving.");
    }
}

// Child class
class ElectricCar extends Car {
    void charge() {
        System.out.println("Electric car is charging.");
    }
}

public class Main {
    public static void main(String[] args) {
        ElectricCar tesla = new ElectricCar();
        tesla.start(); // Inherited from Vehicle
        tesla.drive(); // Inherited from Car
        tesla.charge(); // Defined in ElectricCar
    }
}
```

#### **Output**
```
Vehicle is starting.
Car is driving.
Electric car is charging.
```

#### **Explanation**:
- The `ElectricCar` class inherits the `start()` method from `Vehicle` and the `drive()` method from `Car`.

---

#### **3. Hierarchical Inheritance**

```java
// Parent class
class Shape {
    void display() {
        System.out.println("This is a shape.");
    }
}

// Child class 1
class Circle extends Shape {
    void draw() {
        System.out.println("Drawing a circle.");
    }
}

// Child class 2
class Rectangle extends Shape {
    void draw() {
        System.out.println("Drawing a rectangle.");
    }
}

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.display(); // Inherited from Shape
        circle.draw(); // Defined in Circle

        Rectangle rectangle = new Rectangle();
        rectangle.display(); // Inherited from Shape
        rectangle.draw(); // Defined in Rectangle
    }
}
```

#### **Output**
```
This is a shape.
Drawing a circle.
This is a shape.
Drawing a rectangle.
```

#### **Explanation**:
- Both `Circle` and `Rectangle` inherit the `display()` method from the `Shape` class but define their own `draw()` methods.

---

### **Method Overriding in Inheritance**

Method overriding allows a subclass to provide a specific implementation of a method already defined in its parent class. It is a key feature of polymorphism.

#### **Example**
```java
class Animal {
    void sound() {
        System.out.println("Animals make sound.");
    }
}

class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("Dogs bark.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal animal = new Dog(); // Polymorphic reference
        animal.sound(); // Calls overridden method in Dog
    }
}
```

#### **Output**
```
Dogs bark.
```

#### **Explanation**:
- The `sound()` method in `Animal` is overridden in the `Dog` class.
- The `animal` reference, although of type `Animal`, calls the overridden method in `Dog`.

---

### **Access Modifiers and Inheritance**

1. **`public`**: Accessible in all classes.
2. **`protected`**: Accessible within the same package and in subclasses.
3. **`default`**: Accessible only within the same package.
4. **`private`**: Not accessible in child classes.

---

### **Advantages of Inheritance**

1. **Code Reusability**:
   - Common code is written in the parent class and reused in child classes.
2. **Extensibility**:
   - Existing functionality can be extended without modifying the original code.
3. **Maintainability**:
   - Changes in the parent class reflect automatically in child classes.
4. **Polymorphism**:
   - Enables dynamic method invocation, making the code more flexible.

---

### **Disadvantages of Inheritance**

1. **Tight Coupling**:
   - Changes in the parent class can affect child classes.
2. **Code Complexity**:
   - Overusing inheritance can lead to complex hierarchies.
3. **Reduced Performance**:
   - Method calls in deep inheritance hierarchies may introduce minor overhead.

---

### **Real-World Analogy**

Think of a **family hierarchy**:
- A **grandparent** has common properties like "last name."
- A **parent** inherits this property and adds their own, like "profession."
- A **child** inherits both and can add more specific properties, like "school."

---

### **Best Practices for Using Inheritance**

1. Use inheritance only when there is a clear **"is-a" relationship**.
   - E.g., A `Car` is a `Vehicle`.
2. Avoid deep inheritance hierarchies as they can make code difficult to understand.
3. Prefer **composition** over inheritance when appropriate (e.g., **"has-a" relationship**).

---

### **Conclusion**

**Inheritance** is a powerful feature in Java that enables code reuse, modularity, and scalability. By allowing classes to inherit fields and methods from other classes, it simplifies the design and development of complex systems. However, it should be used judiciously to avoid pitfalls like tight coupling and complex hierarchies.

## Why Multiple Inheritance is Not Possible in Java
### **Why is Multiple Inheritance Not Possible in Java?**

In Java, **multiple inheritance** (where a class can inherit from more than one class) is **not allowed** using **classes**. This restriction exists to prevent **ambiguity** and **complexity** caused by the "Diamond Problem." However, Java supports multiple inheritance using **interfaces**.

---

### **What is Multiple Inheritance?**

**Multiple inheritance** allows a class to inherit from more than one parent class. For example:

```java
class Parent1 {
    void display() {
        System.out.println("Display from Parent1");
    }
}

class Parent2 {
    void display() {
        System.out.println("Display from Parent2");
    }
}

// This is not allowed in Java
class Child extends Parent1, Parent2 { 
    // Error: Java does not support multiple inheritance with classes
}
```

The above example will fail because Java does not allow a class to inherit from multiple parent classes.

---

### **The Diamond Problem in Multiple Inheritance**

#### **What is the Diamond Problem?**
The Diamond Problem arises when:
1. A class inherits from two parent classes, which have a method with the same signature.
2. The compiler cannot decide which method to inherit in the child class.

#### **Example of the Diamond Problem**

```java
class Parent1 {
    void display() {
        System.out.println("Display from Parent1");
    }
}

class Parent2 {
    void display() {
        System.out.println("Display from Parent2");
    }
}

// This is hypothetical and not allowed in Java
class Child extends Parent1, Parent2 {
    // Compiler confusion: Which `display()` to inherit?
}

public class Main {
    public static void main(String[] args) {
        Child obj = new Child();
        obj.display(); // Which method should be called?
    }
}
```

#### **Ambiguity in the Diamond Problem**
- If `Child` inherits `display()` from both `Parent1` and `Parent2`, the compiler cannot determine which version of `display()` to invoke.
- To avoid such ambiguity, Java prohibits multiple inheritance with classes.

---

### **How Java Avoids Multiple Inheritance**
1. **No Multiple Inheritance with Classes**:
   - Java enforces single inheritance with classes to eliminate ambiguity.
2. **Multiple Inheritance with Interfaces**:
   - Java allows a class to implement multiple interfaces, as interfaces do not provide method implementations (prior to Java 8) or resolve conflicts explicitly (from Java 8 onward).

---

### **Multiple Inheritance Using Interfaces**

Since interfaces only contain method signatures (prior to Java 8), there is no ambiguity about which implementation to inherit.

#### **Example: Multiple Inheritance Using Interfaces**

```java
interface Interface1 {
    void display();
}

interface Interface2 {
    void display();
}

class Child implements Interface1, Interface2 {
    @Override
    public void display() {
        System.out.println("Display from Child");
    }
}

public class Main {
    public static void main(String[] args) {
        Child obj = new Child();
        obj.display(); // Output: Display from Child
    }
}
```

#### **Explanation**
- `Child` implements both `Interface1` and `Interface2`.
- Since interfaces only declare methods (no implementation), the child class provides the implementation, avoiding ambiguity.

---

### **Multiple Inheritance with Default Methods in Interfaces (Java 8)**

From Java 8 onwards, interfaces can have **default methods** (methods with a body). If a class implements multiple interfaces with the same default method, the class must override it to resolve ambiguity.

#### **Example: Resolving Ambiguity with Default Methods**
```java
interface Interface1 {
    default void display() {
        System.out.println("Display from Interface1");
    }
}

interface Interface2 {
    default void display() {
        System.out.println("Display from Interface2");
    }
}

class Child implements Interface1, Interface2 {
    @Override
    public void display() {
        System.out.println("Display from Child (resolving ambiguity)");
    }
}

public class Main {
    public static void main(String[] args) {
        Child obj = new Child();
        obj.display(); // Output: Display from Child (resolving ambiguity)
    }
}
```

#### **Explanation**
- Both `Interface1` and `Interface2` define a `display()` default method.
- The `Child` class explicitly overrides the `display()` method to resolve the conflict.

---

### **Key Reasons Java Disallows Multiple Inheritance with Classes**

1. **Ambiguity (Diamond Problem)**:
   - When two parent classes have methods with the same signature, the compiler cannot decide which method to inherit in the child class.

2. **Increased Complexity**:
   - Handling method resolution and memory management becomes complex with multiple inheritance.

3. **Maintainability**:
   - Code becomes harder to read and maintain if multiple inheritance is allowed, especially in large projects.

4. **Java’s Simplicity**:
   - Java’s single inheritance model simplifies the language and avoids pitfalls associated with multiple inheritance.

---

### **Advantages of Using Interfaces for Multiple Inheritance**

1. **No Ambiguity**:
   - Interfaces allow classes to inherit multiple behaviors without ambiguity since they don't enforce method implementations (prior to Java 8).

2. **Flexibility**:
   - A class can implement multiple interfaces and thus have diverse behaviors.

3. **Explicit Conflict Resolution**:
   - With default methods (Java 8), interfaces allow explicit conflict resolution.

---

### **Comparison: Multiple Inheritance with Classes vs Interfaces**

| **Aspect**                 | **Classes**                                   | **Interfaces**                                 |
|----------------------------|-----------------------------------------------|-----------------------------------------------|
| **Multiple Inheritance**    | Not supported due to ambiguity and complexity. | Supported with conflict resolution mechanisms. |
| **Ambiguity (Diamond Problem)** | Can occur with classes.                     | Avoided by explicitly resolving conflicts.     |
| **Implementation**          | Classes can have fully implemented methods.    | Interfaces may contain only abstract methods (prior to Java 8) or default methods (Java 8+). |
| **Ease of Use**             | Simpler but restricted to single inheritance. | More flexible for designing complex systems.   |

---

### **Conclusion**

Java does not support multiple inheritance with classes to avoid ambiguity and complexity, particularly the **Diamond Problem**. However, Java provides a flexible alternative with **interfaces**, allowing a class to inherit behaviors from multiple sources. With the introduction of **default methods** in Java 8, interfaces provide even greater functionality for multiple inheritance scenarios while maintaining simplicity and clarity.

## What is Polymorphism
### **What is Polymorphism in Java?**

**Polymorphism** is one of the core principles of Object-Oriented Programming (OOP). The term **polymorphism** comes from the Greek words *poly* (meaning "many") and *morph* (meaning "forms"). In Java, polymorphism refers to the ability of an object to take on **many forms**. 

Polymorphism allows the same method or function to behave differently based on the object or context. It provides flexibility and reusability in code.

---

### **Types of Polymorphism in Java**

Java supports two types of polymorphism:

1. **Compile-Time Polymorphism (Static Polymorphism)**:
   - Achieved through **method overloading**.
   - Determined at compile-time.

2. **Run-Time Polymorphism (Dynamic Polymorphism)**:
   - Achieved through **method overriding**.
   - Determined at runtime.

---

### **1. Compile-Time Polymorphism (Method Overloading)**

**Method Overloading** occurs when two or more methods in the same class have the **same name** but different **parameters** (number, type, or order). The method to be called is resolved at compile time.

#### **Example of Method Overloading**

```java
class Calculator {
    // Method to add two integers
    int add(int a, int b) {
        return a + b;
    }

    // Overloaded method to add three integers
    int add(int a, int b, int c) {
        return a + b + c;
    }

    // Overloaded method to add two doubles
    double add(double a, double b) {
        return a + b;
    }
}

public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        // Calling overloaded methods
        System.out.println("Sum of two integers: " + calc.add(10, 20));
        System.out.println("Sum of three integers: " + calc.add(10, 20, 30));
        System.out.println("Sum of two doubles: " + calc.add(5.5, 4.5));
    }
}
```

#### **Output**
```
Sum of two integers: 30
Sum of three integers: 60
Sum of two doubles: 10.0
```

#### **Key Points**
- The method name is the same (`add`), but the parameter list is different.
- The compiler determines which method to call based on the arguments passed.

---

### **2. Run-Time Polymorphism (Method Overriding)**

**Method Overriding** occurs when a subclass provides a specific implementation of a method that is already defined in its parent class. The method to be executed is determined at runtime based on the object's actual type.

#### **Example of Method Overriding**

```java
// Parent class
class Animal {
    void sound() {
        System.out.println("This animal makes a sound.");
    }
}

// Subclass
class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("The dog barks.");
    }
}

// Subclass
class Cat extends Animal {
    @Override
    void sound() {
        System.out.println("The cat meows.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myAnimal;

        // Assign Dog to the parent reference
        myAnimal = new Dog();
        myAnimal.sound(); // Calls Dog's sound method

        // Assign Cat to the parent reference
        myAnimal = new Cat();
        myAnimal.sound(); // Calls Cat's sound method
    }
}
```

#### **Output**
```
The dog barks.
The cat meows.
```

#### **Key Points**
- The `sound()` method is defined in the `Animal` class and overridden in the `Dog` and `Cat` classes.
- At runtime, the JVM determines which `sound()` method to invoke based on the actual type of the object.

---

### **Difference Between Method Overloading and Method Overriding**

| **Aspect**             | **Method Overloading**                                       | **Method Overriding**                                       |
|------------------------|-------------------------------------------------------------|------------------------------------------------------------|
| **Definition**          | Same method name with different parameter lists.             | Same method name and parameters in both parent and child classes. |
| **Type**                | Compile-time polymorphism.                                   | Runtime polymorphism.                                       |
| **Inheritance**         | Not dependent on inheritance.                               | Requires inheritance.                                       |
| **Method Resolution**   | Resolved at compile time.                                    | Resolved at runtime.                                        |
| **Example Use Case**    | Methods performing similar tasks with varying parameters.    | Providing specific behavior for a subclass.                |

---

### **Polymorphism and Dynamic Method Dispatch**

Dynamic method dispatch is a mechanism by which a call to an overridden method is resolved at runtime. It is the core of runtime polymorphism in Java.

#### **Example of Dynamic Method Dispatch**
```java
class Shape {
    void draw() {
        System.out.println("Drawing a generic shape.");
    }
}

class Circle extends Shape {
    @Override
    void draw() {
        System.out.println("Drawing a circle.");
    }
}

class Rectangle extends Shape {
    @Override
    void draw() {
        System.out.println("Drawing a rectangle.");
    }
}

public class Main {
    public static void main(String[] args) {
        Shape shape;

        shape = new Circle();
        shape.draw(); // Calls Circle's draw method

        shape = new Rectangle();
        shape.draw(); // Calls Rectangle's draw method
    }
}
```

#### **Output**
```
Drawing a circle.
Drawing a rectangle.
```

---

### **Polymorphism in Interfaces**

Polymorphism can also be achieved using **interfaces**. A single interface can be implemented by multiple classes, each providing its specific behavior.

#### **Example of Polymorphism with Interfaces**
```java
interface Vehicle {
    void start();
}

class Car implements Vehicle {
    @Override
    public void start() {
        System.out.println("The car is starting.");
    }
}

class Bike implements Vehicle {
    @Override
    public void start() {
        System.out.println("The bike is starting.");
    }
}

public class Main {
    public static void main(String[] args) {
        Vehicle vehicle;

        vehicle = new Car();
        vehicle.start(); // Calls Car's start method

        vehicle = new Bike();
        vehicle.start(); // Calls Bike's start method
    }
}
```

#### **Output**
```
The car is starting.
The bike is starting.
```

---

### **Advantages of Polymorphism**

1. **Code Reusability**:
   - Write common code once in the superclass and reuse it in subclasses.
2. **Flexibility**:
   - The same interface can be used to interact with different types of objects.
3. **Extensibility**:
   - New functionality can be added easily without modifying existing code.
4. **Dynamic Behavior**:
   - Method overriding allows behavior to be determined at runtime.

---

### **Disadvantages of Polymorphism**

1. **Complexity**:
   - Can make the code harder to understand for beginners.
2. **Performance Overhead**:
   - Runtime polymorphism (method overriding) introduces a slight overhead due to dynamic method dispatch.

---

### **Real-World Analogy**

1. **Compile-Time Polymorphism**:
   - Think of a **calculator** that can perform addition for integers, floating-point numbers, and matrices. The method name (`add`) remains the same, but the input type varies.

2. **Run-Time Polymorphism**:
   - Think of a **remote control**. The same remote can operate different devices like a TV, AC, or DVD player. The functionality depends on the device it is controlling at runtime.

---

### **Conclusion**

Polymorphism is a powerful concept in Java that enhances flexibility, code reusability, and extensibility. By using compile-time polymorphism (method overloading) and runtime polymorphism (method overriding), you can design scalable and modular Java applications. Understanding polymorphism and its types is essential for mastering object-oriented programming.

## What is Compile-Time and Runtime Polymorphism in Java
### **Compile-Time and Runtime Polymorphism in Java**

Polymorphism is one of the core principles of Object-Oriented Programming (OOP) that allows a single interface or method to behave differently in different contexts. Java supports **two types of polymorphism**:

1. **Compile-Time Polymorphism** (Static Polymorphism)
2. **Runtime Polymorphism** (Dynamic Polymorphism)

Both types of polymorphism are crucial for writing flexible, reusable, and extensible code.

---

### **1. Compile-Time Polymorphism (Static Polymorphism)**

**Compile-time polymorphism** is achieved when the method to be called is resolved at compile time. This is done through **method overloading**, where multiple methods have the same name but differ in their parameter list (number, type, or order of parameters).

#### **Key Features**
- Determined during compile time.
- Achieved through **method overloading**.
- Increases code readability and reusability.

#### **Example: Method Overloading**
```java
class Calculator {
    // Method to add two integers
    int add(int a, int b) {
        return a + b;
    }

    // Overloaded method to add three integers
    int add(int a, int b, int c) {
        return a + b + c;
    }

    // Overloaded method to add two doubles
    double add(double a, double b) {
        return a + b;
    }
}

public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        // Compile-time resolution of method calls
        System.out.println("Sum of two integers: " + calc.add(10, 20)); // Calls add(int, int)
        System.out.println("Sum of three integers: " + calc.add(10, 20, 30)); // Calls add(int, int, int)
        System.out.println("Sum of two doubles: " + calc.add(5.5, 4.5)); // Calls add(double, double)
    }
}
```

#### **Output**
```
Sum of two integers: 30
Sum of three integers: 60
Sum of two doubles: 10.0
```

#### **How it Works**
- At compile time, the compiler selects the appropriate method based on the arguments passed.
- The methods have the same name (`add`) but differ in their parameter lists.

---

### **2. Runtime Polymorphism (Dynamic Polymorphism)**

**Runtime polymorphism** is achieved when the method to be executed is determined at runtime based on the **object's actual type**, not its reference type. This is done through **method overriding**, where a subclass provides a specific implementation of a method defined in its parent class.

#### **Key Features**
- Determined during runtime.
- Achieved through **method overriding**.
- Enables dynamic method dispatch.
- Makes use of **inheritance** and **polymorphic references**.

#### **Example: Method Overriding**
```java
// Parent class
class Animal {
    void sound() {
        System.out.println("This animal makes a sound.");
    }
}

// Subclass
class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("The dog barks.");
    }
}

// Subclass
class Cat extends Animal {
    @Override
    void sound() {
        System.out.println("The cat meows.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myAnimal;

        // Polymorphic reference to Dog
        myAnimal = new Dog();
        myAnimal.sound(); // Calls Dog's sound method

        // Polymorphic reference to Cat
        myAnimal = new Cat();
        myAnimal.sound(); // Calls Cat's sound method
    }
}
```

#### **Output**
```
The dog barks.
The cat meows.
```

#### **How it Works**
1. The method `sound()` is defined in the `Animal` class and overridden in `Dog` and `Cat`.
2. At runtime, the JVM determines the actual type of the object and calls the corresponding overridden method.

---

### **Key Differences Between Compile-Time and Runtime Polymorphism**

| **Aspect**                  | **Compile-Time Polymorphism**                                   | **Runtime Polymorphism**                                   |
|-----------------------------|---------------------------------------------------------------|-----------------------------------------------------------|
| **Resolution Time**          | Resolved at compile time.                                     | Resolved at runtime.                                       |
| **Achieved Through**         | Method overloading.                                           | Method overriding.                                         |
| **Inheritance Requirement**  | No inheritance is required.                                   | Requires inheritance and method overriding.               |
| **Flexibility**              | Less flexible; determined at compile time.                   | More flexible; determined dynamically at runtime.          |
| **Example**                  | Methods with the same name but different parameter lists.     | Subclasses provide specific implementations of parent methods. |
| **Performance**              | Faster due to compile-time resolution.                       | Slightly slower due to runtime method dispatch.            |

---

### **Polymorphism in Action**

#### **Dynamic Method Dispatch**
Dynamic method dispatch is the mechanism that enables runtime polymorphism. When a superclass reference points to a subclass object, the overridden method of the actual object type is called.

#### **Example**
```java
class Shape {
    void draw() {
        System.out.println("Drawing a generic shape.");
    }
}

class Circle extends Shape {
    @Override
    void draw() {
        System.out.println("Drawing a circle.");
    }
}

class Rectangle extends Shape {
    @Override
    void draw() {
        System.out.println("Drawing a rectangle.");
    }
}

public class Main {
    public static void main(String[] args) {
        Shape shape;

        shape = new Circle();
        shape.draw(); // Output: Drawing a circle.

        shape = new Rectangle();
        shape.draw(); // Output: Drawing a rectangle.
    }
}
```

#### **How it Works**
- The reference variable `shape` of type `Shape` can point to objects of `Circle` or `Rectangle`.
- The method `draw()` is invoked based on the actual type of the object, not the reference type.

---

### **Use Cases of Polymorphism**

1. **Compile-Time Polymorphism**:
   - Simplifies code for tasks requiring methods with the same functionality but different input types.
   - Common in utility classes (e.g., `Math.max` for integers, floats, etc.).

2. **Runtime Polymorphism**:
   - Enables flexible and reusable code.
   - Allows behavior to be dynamically determined at runtime.
   - Used in frameworks and APIs (e.g., using interfaces or abstract classes).

---

### **Advantages of Polymorphism**

1. **Code Reusability**:
   - Common methods in a superclass can be reused in multiple subclasses.
2. **Extensibility**:
   - New behaviors can be added without modifying existing code.
3. **Flexibility**:
   - Methods behave differently based on the object's actual type.
4. **Simplified Code**:
   - Eliminates the need for complex `if-else` or `switch` constructs.

---

### **Disadvantages of Polymorphism**

1. **Performance Overhead**:
   - Runtime polymorphism involves dynamic method dispatch, which can impact performance.
2. **Code Complexity**:
   - Overuse of polymorphism can make code harder to understand and debug.
3. **Compile-Time Errors**:
   - Incorrect method calls in compile-time polymorphism may result in errors if not carefully handled.

---

### **Real-World Analogy**

1. **Compile-Time Polymorphism**:
   - Think of a **calculator** that can perform addition for integers, floating-point numbers, and matrices. The method name (`add`) is the same, but the behavior depends on the input type.

2. **Runtime Polymorphism**:
   - Think of a **remote control** that can operate a TV, an AC, or a music system. The same remote (reference) is used, but the action (method) depends on the connected device (object type).

---

### **Conclusion**

**Compile-Time Polymorphism** (method overloading) and **Runtime Polymorphism** (method overriding) are two essential types of polymorphism in Java. Together, they enable flexibility, scalability, and reusability in Java applications. Compile-time polymorphism simplifies method calls based on parameters, while runtime polymorphism provides dynamic behavior and allows methods to be overridden for specific use cases. Understanding and applying these concepts effectively is key to mastering object-oriented programming in Java.

## What is Dynamic Binding
### **What is Dynamic Binding in Java?**

**Dynamic Binding**, also known as **late binding**, is a mechanism in Java where the method to be called is determined at **runtime** rather than at compile time. It applies specifically to **method overriding** in **runtime polymorphism**. 

When a method is invoked on an object through a reference variable, the JVM decides at runtime which method implementation to execute based on the actual type of the object, not the reference type.

---

### **Key Characteristics of Dynamic Binding**
1. **Occurs at Runtime**:
   - The method call is resolved when the program is running, not during compilation.
   
2. **Supports Runtime Polymorphism**:
   - Enables overriding of methods in subclasses to provide specific behavior.

3. **Works with Non-Static Methods**:
   - Only applicable to instance methods (non-static methods). Static methods are resolved at compile time (static binding).

4. **Relies on Inheritance**:
   - Requires a parent-child relationship between classes for overriding methods.

---

### **How Dynamic Binding Works**

When a method is called on a reference variable, the JVM:
1. Checks the **actual type** of the object (the object being referenced).
2. Invokes the overridden method in the subclass if it exists, ignoring the reference type.

#### **Example**
```java
class Parent {
    void display() {
        System.out.println("Display method in Parent class.");
    }
}

class Child extends Parent {
    @Override
    void display() {
        System.out.println("Display method in Child class.");
    }
}

public class Main {
    public static void main(String[] args) {
        Parent obj = new Child(); // Parent reference, Child object
        obj.display(); // Dynamic binding happens here
    }
}
```

#### **Output**
```
Display method in Child class.
```

#### **Explanation**
- The reference variable `obj` is of type `Parent`, but it points to an object of type `Child`.
- At runtime, the JVM checks the actual type of `obj` (which is `Child`) and calls the `display()` method in the `Child` class.

---

### **Dynamic Binding vs Static Binding**

| **Aspect**              | **Dynamic Binding**                              | **Static Binding**                              |
|-------------------------|-------------------------------------------------|------------------------------------------------|
| **Binding Time**         | Happens at runtime.                            | Happens at compile time.                       |
| **Applicable To**        | Instance methods (non-static methods).          | Static methods, final methods, and variables.  |
| **Flexibility**          | Allows method overriding and runtime polymorphism. | No runtime polymorphism; fixed behavior.       |
| **Performance**          | Slightly slower due to runtime decision-making.  | Faster as method resolution is done at compile time. |

---

### **Example Comparing Dynamic and Static Binding**

```java
class Parent {
    void display() { // Instance method (dynamic binding)
        System.out.println("Display in Parent class.");
    }

    static void staticMethod() { // Static method (static binding)
        System.out.println("Static method in Parent class.");
    }
}

class Child extends Parent {
    @Override
    void display() {
        System.out.println("Display in Child class.");
    }

    static void staticMethod() {
        System.out.println("Static method in Child class.");
    }
}

public class Main {
    public static void main(String[] args) {
        Parent obj = new Child();

        // Dynamic Binding
        obj.display(); // Calls the overridden method in Child class

        // Static Binding
        obj.staticMethod(); // Calls the static method in Parent class
    }
}
```

#### **Output**
```
Display in Child class.
Static method in Parent class.
```

#### **Explanation**
- **Dynamic Binding**:
  - `display()` is an instance method, so the actual object type (`Child`) is used to determine which method to call.
- **Static Binding**:
  - `staticMethod()` is a static method, and static methods are resolved based on the reference type (`Parent`).

---

### **Use Case of Dynamic Binding**

Dynamic binding is widely used in **runtime polymorphism** to enable flexibility and scalability in object-oriented design. A common use case is when a **parent reference** is used to invoke methods on a **child object**, allowing behavior to be determined dynamically.

#### **Example: Animal Hierarchy**
```java
class Animal {
    void makeSound() {
        System.out.println("Animal makes a sound.");
    }
}

class Dog extends Animal {
    @Override
    void makeSound() {
        System.out.println("Dog barks.");
    }
}

class Cat extends Animal {
    @Override
    void makeSound() {
        System.out.println("Cat meows.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal animal;

        // Dynamic binding to Dog
        animal = new Dog();
        animal.makeSound(); // Output: Dog barks.

        // Dynamic binding to Cat
        animal = new Cat();
        animal.makeSound(); // Output: Cat meows.
    }
}
```

---

### **Advantages of Dynamic Binding**
1. **Flexibility**:
   - Enables runtime decision-making, allowing the program to adapt to different object types.
2. **Code Reusability**:
   - Parent classes can define common methods, and child classes can override them to provide specific behavior.
3. **Promotes Polymorphism**:
   - Dynamic binding is essential for implementing runtime polymorphism, a key feature of OOP.

---

### **Disadvantages of Dynamic Binding**
1. **Performance Overhead**:
   - Method resolution at runtime is slower than at compile time.
2. **Potential Complexity**:
   - Understanding and debugging dynamically bound methods can be challenging in large projects.

---

### **Real-World Analogy**

Consider a **smartphone**:
- A smartphone (parent class) can make calls, send messages, and access apps.
- Different brands (child classes) like Samsung and iPhone implement these functionalities in their own way (method overriding).
- When you use the generic smartphone interface (parent reference), the specific brand’s behavior is determined dynamically (runtime binding).

---

### **Key Points to Remember**
1. **Dynamic Binding** applies only to instance methods.
2. It enables **runtime polymorphism**, allowing method behavior to vary based on the object's actual type.
3. It is essential for designing flexible and extensible systems.
4. Methods resolved at compile time, such as static methods, final methods, or private methods, use **static binding**, not dynamic binding.

---

### **Conclusion**

Dynamic binding is a powerful feature in Java that supports runtime polymorphism and method overriding. It enables the JVM to determine the method to invoke based on the actual object type at runtime, ensuring flexibility and reusability in object-oriented programming. By leveraging dynamic binding effectively, developers can design robust and scalable applications.

## What is Aggregation, Composition, and Association in Java
### **Aggregation, Composition, and Association in Java**

In Java, **association**, **aggregation**, and **composition** are concepts that describe the relationships between classes in an object-oriented programming (OOP) paradigm. These relationships define how objects of one class interact with objects of another class and the level of dependency between them.

---

### **1. Association**

**Association** represents a relationship between two classes where the objects of one class are related to the objects of another class. It can be:
- **One-to-One**: One object is associated with one other object.
- **One-to-Many**: One object is associated with many objects.
- **Many-to-Many**: Many objects are associated with many objects.

#### **Key Features**
- Represents a general "uses-a" or "has-a" relationship.
- Objects can exist independently of each other.

#### **Example of Association**

```java
class Student {
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

class Library {
    private String libraryName;

    public Library(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibraryName() {
        return libraryName;
    }
}

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Alice");
        Library library = new Library("Central Library");

        System.out.println(student.getName() + " is associated with " + library.getLibraryName());
    }
}
```

#### **Output**
```
Alice is associated with Central Library
```

---

### **2. Aggregation**

**Aggregation** is a specialized form of association where one class contains a reference to another class, representing a **"has-a"** relationship. In aggregation:
- Objects can exist independently of each other.
- The contained object (part) can exist without the container (whole).

#### **Key Features**
- Represents a **weak relationship** between objects.
- Implies ownership, but the lifecycle of the contained object is not dependent on the container.

#### **Example of Aggregation**

```java
class Address {
    private String city;
    private String state;

    public Address(String city, String state) {
        this.city = city;
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
}

class Employee {
    private String name;
    private Address address; // Aggregation

    public Employee(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public void displayInfo() {
        System.out.println("Name: " + name);
        System.out.println("City: " + address.getCity());
        System.out.println("State: " + address.getState());
    }
}

public class Main {
    public static void main(String[] args) {
        Address address = new Address("New York", "NY");
        Employee employee = new Employee("Bob", address);

        employee.displayInfo();
    }
}
```

#### **Output**
```
Name: Bob
City: New York
State: NY
```

#### **Explanation**
- The `Employee` class contains a reference to the `Address` class, representing a "has-a" relationship.
- The `Address` object can exist independently of the `Employee` object.

---

### **3. Composition**

**Composition** is a stronger form of association where the contained object (part) is strongly dependent on the container (whole). If the container object is destroyed, the contained objects are also destroyed.

#### **Key Features**
- Represents a **strong relationship** between objects.
- The lifecycle of the part is tied to the lifecycle of the whole.

#### **Example of Composition**

```java
class Engine {
    public void start() {
        System.out.println("Engine is starting...");
    }
}

class Car {
    private Engine engine; // Composition

    public Car() {
        engine = new Engine(); // Engine created within Car
    }

    public void startCar() {
        engine.start();
        System.out.println("Car is starting...");
    }
}

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.startCar();
    }
}
```

#### **Output**
```
Engine is starting...
Car is starting...
```

#### **Explanation**
- The `Car` class has an `Engine` object, which is created and managed internally by the `Car` class.
- The `Engine` object cannot exist independently of the `Car` object.

---

### **Key Differences Between Association, Aggregation, and Composition**

| **Aspect**          | **Association**                                | **Aggregation**                              | **Composition**                                |
|---------------------|-----------------------------------------------|---------------------------------------------|-----------------------------------------------|
| **Definition**       | Represents a general relationship between classes. | Represents a weak "has-a" relationship.     | Represents a strong "has-a" relationship.     |
| **Dependency**       | Objects are loosely connected.                | The contained object can exist independently. | The contained object cannot exist independently. |
| **Lifecycle**        | Independent lifecycles for objects.           | Independent lifecycle for contained objects. | Dependent lifecycle for contained objects.    |
| **Strength**         | Weak relationship.                            | Stronger than association but weaker than composition. | Strong relationship.                          |
| **Example**          | Student and Library.                         | Employee and Address.                       | Car and Engine.                               |

---

### **Real-World Examples**

1. **Association**:
   - A teacher is associated with multiple students.
   - Both teacher and student can exist independently.

2. **Aggregation**:
   - A university has multiple departments.
   - Departments can exist independently of the university.

3. **Composition**:
   - A house has rooms.
   - Rooms cannot exist without the house.

---

### **Code Examples of All Relationships Together**

```java
class Teacher {
    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

class Department {
    private String deptName;

    public Department(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptName() {
        return deptName;
    }
}

class School {
    private Department department; // Aggregation
    private Principal principal;   // Composition

    public School(String deptName) {
        department = new Department(deptName); // Aggregated object
        principal = new Principal("Mr. Smith"); // Composed object
    }

    public void displayInfo() {
        System.out.println("Principal: " + principal.getName());
        System.out.println("Department: " + department.getDeptName());
    }

    private class Principal { // Inner class for Composition
        private String name;

        public Principal(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}

public class Main {
    public static void main(String[] args) {
        // Association Example
        Teacher teacher = new Teacher("Mrs. Johnson");
        System.out.println("Teacher: " + teacher.getName());

        // Aggregation and Composition Example
        School school = new School("Science");
        school.displayInfo();
    }
}
```

#### **Output**
```
Teacher: Mrs. Johnson
Principal: Mr. Smith
Department: Science
```

---

### **Summary**

| **Relationship Type** | **Definition**                                                                                          | **Object Independence**                  | **Lifecycle Dependency**                  |
|------------------------|-------------------------------------------------------------------------------------------------------|------------------------------------------|-------------------------------------------|
| **Association**        | General relationship between two classes.                                                             | Both objects can exist independently.    | No dependency.                            |
| **Aggregation**        | A weak "has-a" relationship where the container references the part.                                   | Contained objects can exist independently. | No dependency.                            |
| **Composition**        | A strong "has-a" relationship where the contained object depends entirely on the container.            | Contained objects cannot exist independently. | Dependent on the container.              |

By understanding these relationships, you can design robust and modular Java applications, promoting reusability, maintainability, and scalability.

## What is is-A and has-A Relationship in Java
### **What is IS-A and HAS-A Relationship in Java?**

In Java, **IS-A** and **HAS-A** relationships describe the two fundamental types of relationships between classes and objects in object-oriented programming. Understanding these relationships is key to designing modular, reusable, and maintainable systems.

---

### **1. IS-A Relationship**

The **IS-A relationship** represents **inheritance** or **generalization**. If one class is a subclass (child) of another class (parent), it is said to have an IS-A relationship with the parent class.

#### **Key Features**
- Achieved using the `extends` keyword for classes or the `implements` keyword for interfaces.
- Represents a "kind of" relationship.
- Enables code reuse and polymorphism.

#### **Example of IS-A Relationship**

```java
class Vehicle {
    void start() {
        System.out.println("Vehicle is starting...");
    }
}

class Car extends Vehicle { // IS-A relationship: Car IS-A Vehicle
    void drive() {
        System.out.println("Car is driving...");
    }
}

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.start(); // Inherited from Vehicle
        myCar.drive(); // Defined in Car
    }
}
```

#### **Output**
```
Vehicle is starting...
Car is driving...
```

#### **Explanation**
- The `Car` class is a subclass of the `Vehicle` class, which establishes an IS-A relationship (`Car IS-A Vehicle`).
- The `Car` class inherits the `start()` method from the `Vehicle` class.

---

#### **IS-A Relationship with Interfaces**

```java
interface Animal {
    void eat();
}

class Dog implements Animal { // IS-A relationship: Dog IS-A Animal
    @Override
    public void eat() {
        System.out.println("Dog is eating...");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myDog = new Dog(); // Polymorphism
        myDog.eat();
    }
}
```

#### **Output**
```
Dog is eating...
```

#### **Explanation**
- The `Dog` class implements the `Animal` interface, establishing an IS-A relationship (`Dog IS-A Animal`).
- Polymorphism is achieved as `myDog` is an `Animal` reference pointing to a `Dog` object.

---

### **2. HAS-A Relationship**

The **HAS-A relationship** represents **composition** or **aggregation**. It signifies that one class contains a reference to another class, establishing a "has-a" or "part-of" relationship.

#### **Key Features**
- Represents a "has-a" relationship.
- Achieved by including one class as a field in another class.
- Can be:
  - **Composition**: The contained object cannot exist without the container.
  - **Aggregation**: The contained object can exist independently.

---

#### **Example of HAS-A Relationship (Aggregation)**

```java
class Engine {
    void start() {
        System.out.println("Engine is starting...");
    }
}

class Car {
    private Engine engine; // HAS-A relationship: Car HAS-A Engine

    public Car(Engine engine) {
        this.engine = engine;
    }

    void startCar() {
        engine.start();
        System.out.println("Car is starting...");
    }
}

public class Main {
    public static void main(String[] args) {
        Engine myEngine = new Engine(); // Engine exists independently
        Car myCar = new Car(myEngine); // Aggregation
        myCar.startCar();
    }
}
```

#### **Output**
```
Engine is starting...
Car is starting...
```

#### **Explanation**
- The `Car` class contains a reference to the `Engine` class, establishing a HAS-A relationship (`Car HAS-A Engine`).
- The `Engine` object is created independently and passed to the `Car` object.

---

#### **Example of HAS-A Relationship (Composition)**

```java
class Battery {
    void charge() {
        System.out.println("Battery is charging...");
    }
}

class Phone {
    private Battery battery; // HAS-A relationship: Phone HAS-A Battery

    public Phone() {
        battery = new Battery(); // Composition
    }

    void usePhone() {
        battery.charge();
        System.out.println("Using the phone...");
    }
}

public class Main {
    public static void main(String[] args) {
        Phone myPhone = new Phone(); // Battery is created internally
        myPhone.usePhone();
    }
}
```

#### **Output**
```
Battery is charging...
Using the phone...
```

#### **Explanation**
- The `Phone` class contains a `Battery` object, establishing a HAS-A relationship (`Phone HAS-A Battery`).
- The `Battery` object is created internally, meaning it cannot exist without the `Phone` object (composition).

---

### **Differences Between IS-A and HAS-A Relationships**

| **Aspect**          | **IS-A Relationship**                              | **HAS-A Relationship**                            |
|---------------------|----------------------------------------------------|--------------------------------------------------|
| **Definition**       | Represents an inheritance relationship.            | Represents a composition or aggregation relationship. |
| **Keyword**          | Achieved using `extends` or `implements`.          | Achieved by including an object as a field.      |
| **Type of Relationship** | "Kind of" relationship (e.g., Car IS-A Vehicle). | "Has-a" relationship (e.g., Car HAS-A Engine).   |
| **Dependency**       | Subclass is dependent on the parent class.          | Objects may or may not depend on each other.     |
| **Reuse**            | Reuses the methods of the parent class.             | Reuses an independent class within another class. |

---

### **When to Use IS-A and HAS-A Relationships**

1. **IS-A Relationship (Inheritance)**:
   - Use when one class is a specialized version of another class.
   - Follow the "is-a" principle to ensure proper inheritance.
   - Example: `Dog IS-A Animal`, `Car IS-A Vehicle`.

2. **HAS-A Relationship (Composition or Aggregation)**:
   - Use when one class is a part or component of another class.
   - Prefer **composition** when the contained object cannot exist independently.
   - Example: `Car HAS-A Engine`, `Phone HAS-A Battery`.

---

### **Real-World Analogy**

1. **IS-A Relationship**:
   - A **Car IS-A Vehicle**. It inherits common properties like the ability to move, but it can also define specific properties like a gear system.

2. **HAS-A Relationship**:
   - A **Car HAS-A Engine**. The engine is a component of the car, but it can be replaced or exist independently in aggregation.

---

### **Code Example with Both Relationships**

```java
class Vehicle {
    void start() {
        System.out.println("Vehicle is starting...");
    }
}

class Engine {
    void start() {
        System.out.println("Engine is starting...");
    }
}

class Car extends Vehicle { // IS-A Relationship
    private Engine engine; // HAS-A Relationship

    public Car() {
        engine = new Engine(); // Composition
    }

    void startCar() {
        engine.start(); // HAS-A usage
        super.start(); // IS-A usage
        System.out.println("Car is ready to drive!");
    }
}

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.startCar();
    }
}
```

#### **Output**
```
Engine is starting...
Vehicle is starting...
Car is ready to drive!
```

---

### **Summary**

| **Relationship Type** | **Definition**                                                                                          | **Examples**                                    |
|-----------------------|-------------------------------------------------------------------------------------------------------|------------------------------------------------|
| **IS-A**              | Represents inheritance or generalization (one class is a specialized version of another).             | `Car IS-A Vehicle`, `Dog IS-A Animal`.         |
| **HAS-A**             | Represents composition or aggregation (one class contains another as a component or reference).        | `Car HAS-A Engine`, `Phone HAS-A Battery`.     |

By understanding IS-A and HAS-A relationships, you can design well-structured, modular, and reusable Java programs.

## What is Method Overriding and Method Overloading
### **What is Method Overriding and Method Overloading in Java?**

Method overriding and method overloading are two important concepts in Java that enable **polymorphism**, one of the core principles of Object-Oriented Programming (OOP). While both involve methods with the same name, they differ in their purpose and implementation.

---

### **1. Method Overriding**

**Method overriding** occurs when a **subclass provides a specific implementation** for a method that is already defined in its **superclass**. The overriding method in the subclass has the **same name**, **return type**, and **parameters** as the method in the superclass.

#### **Key Features of Method Overriding**
1. **Purpose**:
   - To provide specific behavior in a subclass for a method defined in the parent class.
2. **Inheritance Required**:
   - Requires a parent-child relationship (inheritance).
3. **Dynamic Polymorphism**:
   - Achieved during runtime (runtime polymorphism).
4. **Annotations**:
   - Use `@Override` to explicitly declare an overriding method (optional but recommended).
5. **Rules**:
   - The method name, parameters, and return type must match the parent method.
   - The overriding method cannot have a stricter access modifier.
   - Only instance methods can be overridden; static and final methods cannot.

---

#### **Example of Method Overriding**

```java
// Parent class
class Animal {
    void sound() {
        System.out.println("This animal makes a sound.");
    }
}

// Subclass
class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("The dog barks.");
    }
}

// Subclass
class Cat extends Animal {
    @Override
    void sound() {
        System.out.println("The cat meows.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myAnimal;

        // Dynamic binding at runtime
        myAnimal = new Dog();
        myAnimal.sound(); // Output: The dog barks.

        myAnimal = new Cat();
        myAnimal.sound(); // Output: The cat meows.
    }
}
```

#### **Output**
```
The dog barks.
The cat meows.
```

#### **Explanation**
- The `sound()` method in the `Animal` class is overridden in the `Dog` and `Cat` classes.
- At runtime, the JVM determines which `sound()` method to call based on the actual object type.

---

### **2. Method Overloading**

**Method overloading** occurs when a **class has multiple methods with the same name but different parameter lists** (number, type, or order of parameters). The method to be called is resolved at **compile time**, hence it is also called **compile-time polymorphism**.

#### **Key Features of Method Overloading**
1. **Purpose**:
   - To define methods that perform similar tasks but with different input arguments.
2. **Inheritance Not Required**:
   - Can be achieved within a single class or across parent and child classes.
3. **Compile-Time Polymorphism**:
   - The method to be called is determined during compilation.
4. **Rules**:
   - Methods must have the same name but differ in parameter lists (number, type, or order of parameters).
   - Return type and access modifiers can vary, but they do not affect method overloading.

---

#### **Example of Method Overloading**

```java
class Calculator {
    // Method to add two integers
    int add(int a, int b) {
        return a + b;
    }

    // Overloaded method to add three integers
    int add(int a, int b, int c) {
        return a + b + c;
    }

    // Overloaded method to add two doubles
    double add(double a, double b) {
        return a + b;
    }
}

public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        // Compile-time resolution of overloaded methods
        System.out.println("Sum of two integers: " + calc.add(10, 20)); // Calls add(int, int)
        System.out.println("Sum of three integers: " + calc.add(10, 20, 30)); // Calls add(int, int, int)
        System.out.println("Sum of two doubles: " + calc.add(5.5, 4.5)); // Calls add(double, double)
    }
}
```

#### **Output**
```
Sum of two integers: 30
Sum of three integers: 60
Sum of two doubles: 10.0
```

#### **Explanation**
- The `add` method is overloaded with different parameter lists.
- The compiler selects the appropriate method based on the arguments passed.

---

### **Comparison: Method Overriding vs Method Overloading**

| **Aspect**             | **Method Overriding**                                  | **Method Overloading**                                  |
|------------------------|-------------------------------------------------------|-------------------------------------------------------|
| **Definition**          | Subclass provides a specific implementation for a method in the parent class. | Same method name with different parameter lists in the same or different classes. |
| **Polymorphism**        | Runtime polymorphism (dynamic method dispatch).       | Compile-time polymorphism.                           |
| **Inheritance**         | Requires inheritance (parent-child relationship).     | Does not require inheritance.                       |
| **Purpose**             | To modify or enhance the behavior of a parent class method. | To define methods with similar functionality but different input arguments. |
| **Binding Time**        | Determined at runtime.                                | Determined at compile time.                          |
| **Rules**               | - Same name, parameters, and return type.<br>- Access modifier cannot be stricter.<br>- Instance methods only. | - Same name but different parameter lists.<br>- Return type can vary.<br>- Static and instance methods are allowed. |
| **Annotations**         | `@Override` is recommended but not mandatory.         | No annotations are required.                         |
| **Example**             | Overriding `toString()` method in Java.               | Overloading constructors in a class.                |

---

### **Code Example with Both Overriding and Overloading**

```java
class Animal {
    // Overloaded methods
    void sound() {
        System.out.println("Animal makes a sound.");
    }

    void sound(String type) {
        System.out.println("Animal makes a " + type + " sound.");
    }
}

class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("Dog barks.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myAnimal = new Dog();

        // Method overriding (runtime polymorphism)
        myAnimal.sound(); // Output: Dog barks.

        // Method overloading (compile-time polymorphism)
        myAnimal.sound("growling"); // Output: Animal makes a growling sound.
    }
}
```

#### **Output**
```
Dog barks.
Animal makes a growling sound.
```

#### **Explanation**
- `sound()` is overridden in the `Dog` class (runtime polymorphism).
- `sound(String type)` is an overloaded version of `sound()` (compile-time polymorphism).

---

### **Advantages of Method Overriding**
1. Enables **runtime polymorphism**.
2. Promotes code reuse and extensibility.
3. Allows specific behavior for subclasses while maintaining a common interface.

### **Advantages of Method Overloading**
1. Simplifies code by allowing similar methods to handle different types of data or arguments.
2. Reduces code duplication by using a single method name for similar tasks.

---

### **Conclusion**

Both **method overriding** and **method overloading** are key techniques for achieving polymorphism in Java:
- **Overriding** allows subclasses to redefine methods from their parent class, supporting runtime polymorphism.
- **Overloading** enables methods with the same name to handle different parameter lists, supporting compile-time polymorphism.

Understanding these concepts helps in designing modular, flexible, and reusable Java applications.

## What is a Constructor
### **What is a Constructor in Java?**

A **constructor** in Java is a special type of method that is used to **initialize objects** of a class. It is called automatically when an object of the class is created. The main purpose of a constructor is to initialize the instance variables of a class.

---

### **Key Characteristics of a Constructor**
1. **Same Name as the Class**:
   - The name of the constructor must match the name of the class.
2. **No Return Type**:
   - A constructor does not have a return type (not even `void`).
3. **Called Automatically**:
   - A constructor is invoked automatically when an object is created.
4. **Overloading**:
   - A class can have multiple constructors with different parameter lists (constructor overloading).
5. **Default Constructor**:
   - If no constructor is explicitly defined, Java provides a default constructor with no arguments.

---

### **Types of Constructors in Java**

#### **1. Default Constructor**
A default constructor is a constructor that takes no arguments. If no constructor is explicitly defined in the class, Java automatically provides a default constructor.

#### **Example: Default Constructor**
```java
class Person {
    String name;

    // Default constructor
    public Person() {
        name = "Unknown"; // Assign default value
    }

    void displayInfo() {
        System.out.println("Name: " + name);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person(); // Default constructor is called
        person.displayInfo(); // Output: Name: Unknown
    }
}
```

#### **Output**
```
Name: Unknown
```

---

#### **2. Parameterized Constructor**
A parameterized constructor is a constructor that takes arguments. It is used to initialize objects with specific values.

#### **Example: Parameterized Constructor**
```java
class Person {
    String name;

    // Parameterized constructor
    public Person(String name) {
        this.name = name; // Initialize instance variable
    }

    void displayInfo() {
        System.out.println("Name: " + name);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Alice"); // Parameterized constructor is called
        person.displayInfo(); // Output: Name: Alice
    }
}
```

#### **Output**
```
Name: Alice
```

---

#### **3. Constructor Overloading**
Constructor overloading is when a class has multiple constructors with different parameter lists. It allows objects to be initialized in different ways.

#### **Example: Constructor Overloading**
```java
class Person {
    String name;
    int age;

    // Default constructor
    public Person() {
        name = "Unknown";
        age = 0;
    }

    // Parameterized constructor with one argument
    public Person(String name) {
        this.name = name;
        age = 0;
    }

    // Parameterized constructor with two arguments
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void displayInfo() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person p1 = new Person(); // Default constructor
        Person p2 = new Person("Alice"); // Constructor with one argument
        Person p3 = new Person("Bob", 25); // Constructor with two arguments

        p1.displayInfo(); // Output: Name: Unknown, Age: 0
        p2.displayInfo(); // Output: Name: Alice, Age: 0
        p3.displayInfo(); // Output: Name: Bob, Age: 25
    }
}
```

#### **Output**
```
Name: Unknown, Age: 0
Name: Alice, Age: 0
Name: Bob, Age: 25
```

---

#### **4. Copy Constructor**
A copy constructor creates a new object by copying the state of another object. Java does not provide a built-in copy constructor, but you can define one manually.

#### **Example: Copy Constructor**
```java
class Person {
    String name;
    int age;

    // Parameterized constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Copy constructor
    public Person(Person other) {
        this.name = other.name;
        this.age = other.age;
    }

    void displayInfo() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person original = new Person("Alice", 25); // Original object
        Person copy = new Person(original); // Copy constructor

        original.displayInfo(); // Output: Name: Alice, Age: 25
        copy.displayInfo(); // Output: Name: Alice, Age: 25
    }
}
```

#### **Output**
```
Name: Alice, Age: 25
Name: Alice, Age: 25
```

---

### **Constructor vs Method**

| **Aspect**               | **Constructor**                                                  | **Method**                                                        |
|--------------------------|-------------------------------------------------------------------|-------------------------------------------------------------------|
| **Purpose**               | Used to initialize an object.                                    | Used to define behavior or perform actions.                      |
| **Name**                  | Must have the same name as the class.                            | Can have any name.                                               |
| **Return Type**           | Does not have a return type (not even `void`).                   | Must have a return type (or `void` for no return).               |
| **Invocation**            | Called automatically when an object is created.                 | Called explicitly using the object reference.                    |
| **Overloading**           | Can be overloaded.                                              | Can be overloaded.                                               |
| **Inheritance**           | Not inherited by subclasses.                                    | Can be inherited by subclasses.                                  |

---

### **Default Constructor vs No-Argument Constructor**

- **Default Constructor**:
  - Provided by Java automatically if no constructors are explicitly defined.
  - Initializes fields with default values (e.g., `0` for integers, `null` for objects).

- **No-Argument Constructor**:
  - Explicitly defined by the programmer.
  - Can include specific initialization logic.

#### **Example**
```java
class Example {
    int value;

    // Default constructor (provided by Java)
    // No special logic included
}

class CustomExample {
    int value;

    // No-argument constructor
    public CustomExample() {
        value = 10; // Custom initialization
    }
}

public class Main {
    public static void main(String[] args) {
        Example e = new Example(); // Default constructor
        CustomExample ce = new CustomExample(); // No-argument constructor

        System.out.println("Default value: " + e.value); // Output: Default value: 0
        System.out.println("Custom value: " + ce.value); // Output: Custom value: 10
    }
}
```

---

### **Key Points to Remember**
1. If no constructors are explicitly defined, Java provides a **default constructor**.
2. Constructors can be **overloaded** to allow flexible object initialization.
3. Constructors cannot be `abstract`, `static`, `final`, or `synchronized`.
4. Constructors are not inherited but can be invoked from a subclass using `super()`.

---

### **Conclusion**

Constructors are essential for initializing objects in Java. By understanding the types of constructors—default, parameterized, overloaded, and copy constructors—you can design flexible and robust Java applications. Proper use of constructors ensures that objects are initialized correctly and consistently, improving code readability and maintainability.

## What is the Difference Between Default Constructor and Parameterized Constructor
### **Difference Between Default Constructor and Parameterized Constructor in Java**

In Java, constructors are used to initialize objects. The **default constructor** and **parameterized constructor** are two common types of constructors, differing in how they initialize objects.

---

### **1. Default Constructor**

A **default constructor** is a constructor with **no parameters**. It initializes objects with default values or values explicitly defined within the constructor.

#### **Characteristics of a Default Constructor**
- **No Parameters**: Takes no arguments.
- **Automatically Provided by Java**:
  - If no constructors are explicitly defined, Java provides a default constructor that initializes instance variables to their default values (e.g., `0` for numeric types, `null` for objects).
- **Explicit Definition**: Developers can explicitly define a default constructor to include custom initialization logic.

---

#### **Example: Default Constructor**

```java
class Person {
    String name;  // Defaults to null
    int age;      // Defaults to 0

    // Default constructor
    public Person() {
        name = "Unknown";  // Custom initialization
        age = 0;
    }

    void displayInfo() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person(); // Default constructor is called
        person.displayInfo(); // Output: Name: Unknown, Age: 0
    }
}
```

#### **Output**
```
Name: Unknown, Age: 0
```

---

### **2. Parameterized Constructor**

A **parameterized constructor** is a constructor that accepts **arguments** to initialize an object with specific values.

#### **Characteristics of a Parameterized Constructor**
- **Takes Parameters**: The constructor accepts arguments, which are used to set the values of instance variables.
- **Custom Initialization**: Enables initializing objects with user-defined values at the time of object creation.
- **Overloading**: Can coexist with other constructors, such as default constructors (constructor overloading).

---

#### **Example: Parameterized Constructor**

```java
class Person {
    String name;
    int age;

    // Parameterized constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void displayInfo() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Alice", 25); // Parameterized constructor is called
        person.displayInfo(); // Output: Name: Alice, Age: 25
    }
}
```

#### **Output**
```
Name: Alice, Age: 25
```

---

### **Key Differences Between Default Constructor and Parameterized Constructor**

| **Aspect**                 | **Default Constructor**                                                   | **Parameterized Constructor**                                           |
|----------------------------|---------------------------------------------------------------------------|-------------------------------------------------------------------------|
| **Definition**              | A constructor with no parameters.                                        | A constructor with parameters to initialize objects with specific values. |
| **Arguments**               | Does not accept any arguments.                                           | Accepts one or more arguments.                                         |
| **Purpose**                 | Initializes objects with default or predefined values.                   | Initializes objects with user-defined values.                          |
| **Provided by Java**        | Automatically provided if no constructor is defined.                    | Not provided by Java; must be explicitly defined.                      |
| **Custom Initialization**   | Limited to hardcoded default values.                                     | Allows flexible initialization with dynamic values.                    |
| **Use Case**                | Suitable when objects share common or default attributes.                | Suitable when objects require unique or specific attributes.           |
| **Example**                 | `Person()` initializes fields with fixed default values.                | `Person(String name, int age)` initializes fields with user input.     |

---

### **Using Both Default and Parameterized Constructors**

A class can have both default and parameterized constructors (constructor overloading). This allows flexibility in object initialization.

#### **Example**
```java
class Person {
    String name;
    int age;

    // Default constructor
    public Person() {
        name = "Unknown";
        age = 0;
    }

    // Parameterized constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void displayInfo() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        // Using default constructor
        Person p1 = new Person();
        p1.displayInfo(); // Output: Name: Unknown, Age: 0

        // Using parameterized constructor
        Person p2 = new Person("Alice", 25);
        p2.displayInfo(); // Output: Name: Alice, Age: 25
    }
}
```

#### **Output**
```
Name: Unknown, Age: 0
Name: Alice, Age: 25
```

---

### **Advantages of Default Constructor**

1. **Simplicity**:
   - Automatically initializes objects with default values.
2. **Ease of Use**:
   - Useful when all objects share common default attributes.

---

### **Advantages of Parameterized Constructor**

1. **Custom Initialization**:
   - Allows dynamic initialization of objects with user-defined values.
2. **Flexibility**:
   - Suitable for scenarios where different objects require different attributes.

---

### **Real-World Analogy**

1. **Default Constructor**:
   - A vending machine dispensing a default snack if no selection is made.

2. **Parameterized Constructor**:
   - A vending machine dispensing a specific snack based on the user's selection.

---

### **Key Points to Remember**

1. If no constructor is explicitly defined, Java provides a **default constructor** that initializes fields to their default values.
2. A **parameterized constructor** must be explicitly defined to initialize objects with specific values.
3. Default and parameterized constructors can coexist in the same class (constructor overloading).
4. Using `this` keyword in parameterized constructors allows calling other constructors or differentiating between local and instance variables.

---

### **Conclusion**

The **default constructor** and **parameterized constructor** are crucial for object initialization in Java. The default constructor is simple and initializes objects with fixed default values, while the parameterized constructor provides flexibility for dynamic and specific object initialization. By combining these constructors, developers can create versatile and reusable classes that cater to a wide range of use cases.

## What is this Keyword
### **What is the `this` Keyword in Java?**

The **`this`** keyword in Java is a reference variable that refers to the **current instance of the class**. It is used to eliminate ambiguity between instance variables and parameters or to explicitly invoke instance methods, constructors, or the current object itself.

The `this` keyword is especially useful in scenarios where a method or constructor parameter has the same name as an instance variable, creating ambiguity.

---

### **Uses of the `this` Keyword**

#### 1. **To Refer to Current Class Instance Variables**
   - The `this` keyword is used to differentiate between **instance variables** and **method or constructor parameters** when they share the same name.

#### Example
```java
class Person {
    String name;
    int age;

    // Constructor with parameters having the same name as instance variables
    public Person(String name, int age) {
        this.name = name; // Refers to instance variable
        this.age = age;   // Refers to instance variable
    }

    void display() {
        System.out.println("Name: " + this.name + ", Age: " + this.age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Alice", 25);
        person.display(); // Output: Name: Alice, Age: 25
    }
}
```

#### **Explanation**
- Without `this.name` and `this.age`, the parameters (`name`, `age`) would shadow the instance variables, leading to incorrect assignment.

---

#### 2. **To Call Another Constructor in the Same Class**
   - The `this()` construct is used to invoke another constructor from the same class, enabling constructor chaining.

#### Example
```java
class Person {
    String name;
    int age;

    // Default constructor
    public Person() {
        this("Unknown", 0); // Calls the parameterized constructor
    }

    // Parameterized constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void display() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person(); // Default constructor
        Person person2 = new Person("Alice", 25); // Parameterized constructor

        person1.display(); // Output: Name: Unknown, Age: 0
        person2.display(); // Output: Name: Alice, Age: 25
    }
}
```

#### **Explanation**
- The `this("Unknown", 0)` in the default constructor calls the parameterized constructor, reusing the initialization logic.

---

#### 3. **To Invoke Current Class Methods**
   - The `this` keyword can explicitly invoke methods of the current object.

#### Example
```java
class Calculator {
    void displaySum(int a, int b) {
        System.out.println("Sum: " + (a + b));
    }

    void calculate() {
        this.displaySum(10, 20); // Calls the displaySum method
    }
}

public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        calc.calculate(); // Output: Sum: 30
    }
}
```

#### **Explanation**
- The `this.displaySum(10, 20)` explicitly calls the `displaySum` method on the current object.

---

#### 4. **To Pass Current Object as an Argument**
   - The `this` keyword can be used to pass the current object as an argument to a method or constructor.

#### Example: Passing to a Method
```java
class Printer {
    void printDetails(Person person) {
        System.out.println("Name: " + person.name + ", Age: " + person.age);
    }
}

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void display(Printer printer) {
        printer.printDetails(this); // Passes the current object
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Alice", 25);
        Printer printer = new Printer();
        person.display(printer); // Output: Name: Alice, Age: 25
    }
}
```

---

#### 5. **To Return the Current Class Instance**
   - The `this` keyword can be used to return the current object from a method.

#### Example
```java
class Person {
    String name;
    int age;

    public Person setName(String name) {
        this.name = name;
        return this; // Returns the current object
    }

    public Person setAge(int age) {
        this.age = age;
        return this; // Returns the current object
    }

    void display() {
        System.out.println("Name: " + name + ", Age: " + age);
    }
}

public class Main {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("Alice").setAge(25); // Chained method calls
        person.display(); // Output: Name: Alice, Age: 25
    }
}
```

#### **Explanation**
- Returning `this` allows method chaining, making the code more concise and readable.

---

### **Key Rules for Using `this` Keyword**

1. **Cannot Be Used in Static Contexts**:
   - The `this` keyword is associated with instance objects and cannot be used in static methods or static contexts.
   
   #### Example:
   ```java
   class Demo {
       static void display() {
           // System.out.println(this); // Error: Cannot use `this` in a static method
       }
   }
   ```

2. **Access Modifier Compatibility**:
   - The `this` keyword can access private, protected, and public members of the current object.

3. **Always Refers to the Current Object**:
   - Regardless of how many objects exist, `this` always refers to the object on which the method or constructor is called.

---

### **Comparison: `this` Keyword in Different Use Cases**

| **Use Case**                          | **Example**                                      | **Purpose**                                    |
|---------------------------------------|------------------------------------------------|------------------------------------------------|
| **Access Instance Variables**         | `this.name = name;`                             | Resolves ambiguity when instance variables are shadowed by parameters. |
| **Call Current Class Constructor**    | `this(arg1, arg2);`                             | Enables constructor chaining.                 |
| **Invoke Current Class Methods**      | `this.methodName();`                            | Explicitly calls another method of the current object. |
| **Pass Current Object as Argument**   | `printer.printDetails(this);`                   | Passes the current object to another method or constructor. |
| **Return Current Object**             | `return this;`                                  | Enables method chaining.                      |

---

### **Real-World Analogy**

- Imagine you are writing a note to yourself and sign it "me." The `this` keyword is like the "me" in your note. It explicitly refers to yourself in that context, eliminating any confusion about whom the note is addressing.

---

### **Advantages of Using `this` Keyword**

1. **Avoids Ambiguity**:
   - Resolves naming conflicts between instance variables and parameters.
2. **Enables Constructor Chaining**:
   - Simplifies initialization logic by reusing constructors.
3. **Improves Readability**:
   - Clearly indicates that the current object's method or variable is being accessed.
4. **Supports Advanced Features**:
   - Facilitates method chaining, passing the current object, and enabling clean and concise code.

---

### **Conclusion**

The `this` keyword in Java is a versatile and powerful tool for managing and referencing the current object within a class. By resolving ambiguity, enabling constructor chaining, and facilitating advanced programming patterns, `this` simplifies object-oriented programming and improves code clarity. Understanding its use cases is essential for writing clean, efficient, and maintainable Java applications.

## What is super Keyword
### **What is the `super` Keyword in Java?**

The **`super`** keyword in Java is a reference variable used in inheritance to refer to the **immediate parent class** of the current class. It allows a subclass to access members (methods, constructors, and variables) of its parent class that might be hidden or overridden.

---

### **Key Features of the `super` Keyword**

1. **Access Parent Class Members**:
   - Used to access fields and methods of the parent class when they are hidden by the subclass.

2. **Call Parent Class Constructor**:
   - Used to invoke the constructor of the parent class.

3. **Avoid Overriding Methods**:
   - Used to call the parent class method when the method is overridden in the child class.

4. **Always Refers to Immediate Parent**:
   - The `super` keyword is restricted to the direct parent class and cannot access members of grandparent classes directly.

---

### **Uses of the `super` Keyword**

#### **1. Access Parent Class Variables**

The `super` keyword can be used to access parent class fields when they are hidden by fields of the same name in the child class.

#### Example
```java
class Animal {
    String name = "Animal";
}

class Dog extends Animal {
    String name = "Dog";

    void displayNames() {
        System.out.println("Name in Child Class: " + name); // Accessing child class variable
        System.out.println("Name in Parent Class: " + super.name); // Accessing parent class variable
    }
}

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.displayNames();
    }
}
```

#### **Output**
```
Name in Child Class: Dog
Name in Parent Class: Animal
```

#### **Explanation**
- The `name` field in the `Dog` class hides the `name` field in the `Animal` class.
- The `super.name` explicitly refers to the parent class's `name` field.

---

#### **2. Call Parent Class Methods**

The `super` keyword can be used to call a parent class method that has been overridden in the child class.

#### Example
```java
class Animal {
    void sound() {
        System.out.println("This is an animal sound.");
    }
}

class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("The dog barks.");
    }

    void displaySound() {
        super.sound(); // Calls the parent class method
        sound();       // Calls the child class method
    }
}

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.displaySound();
    }
}
```

#### **Output**
```
This is an animal sound.
The dog barks.
```

#### **Explanation**
- The `sound()` method is overridden in the `Dog` class.
- The `super.sound()` explicitly calls the parent class's version of the `sound()` method.

---

#### **3. Call Parent Class Constructor**

The `super()` keyword is used to call the **constructor** of the parent class. This is particularly useful for initializing the parent class's fields when creating a subclass object.

#### Example
```java
class Animal {
    String name;

    // Parent class constructor
    public Animal(String name) {
        this.name = name;
        System.out.println("Animal Constructor: " + name);
    }
}

class Dog extends Animal {
    public Dog(String name) {
        super(name); // Calls the parent class constructor
        System.out.println("Dog Constructor: " + name);
    }
}

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Buddy");
    }
}
```

#### **Output**
```
Animal Constructor: Buddy
Dog Constructor: Buddy
```

#### **Explanation**
- The `super(name)` in the `Dog` constructor calls the `Animal` constructor, ensuring the parent class is properly initialized.

---

### **Rules for Using `super`**

1. **Must Be the First Statement**:
   - The `super()` constructor call must be the first statement in a subclass constructor.

   #### Example:
   ```java
   class Animal {
       public Animal() {
           System.out.println("Animal Constructor");
       }
   }

   class Dog extends Animal {
       public Dog() {
           super(); // Must be the first statement
           System.out.println("Dog Constructor");
       }
   }
   ```

2. **Only Refers to Immediate Parent**:
   - The `super` keyword always refers to the immediate parent class and cannot access members of a grandparent class directly.

3. **Optional for Default Constructor**:
   - If the parent class has a default constructor, the compiler automatically inserts a `super()` call if no explicit call is made.

---

### **Difference Between `this` and `super`**

| **Aspect**                  | **`this` Keyword**                                       | **`super` Keyword**                                       |
|-----------------------------|---------------------------------------------------------|----------------------------------------------------------|
| **Definition**               | Refers to the current instance of the class.            | Refers to the immediate parent class of the current class.|
| **Access**                   | Accesses current class's fields, methods, or constructors. | Accesses parent class's fields, methods, or constructors. |
| **Constructor Chaining**     | Used to call another constructor in the same class.     | Used to call the parent class constructor.               |
| **Method Invocation**        | Calls current class methods explicitly.                 | Calls parent class methods explicitly.                   |
| **Context**                  | Applicable within the same class.                       | Applicable in inheritance hierarchy.                     |

---

### **Example: Combining `this` and `super`**

```java
class Animal {
    String name;

    public Animal(String name) {
        this.name = name;
    }

    void display() {
        System.out.println("Animal: " + name);
    }
}

class Dog extends Animal {
    String breed;

    public Dog(String name, String breed) {
        super(name); // Calls parent class constructor
        this.breed = breed; // Refers to current class instance variable
    }

    void display() {
        super.display(); // Calls parent class method
        System.out.println("Breed: " + this.breed);
    }
}

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Buddy", "Golden Retriever");
        dog.display();
    }
}
```

#### **Output**
```
Animal: Buddy
Breed: Golden Retriever
```

#### **Explanation**
- The `super(name)` calls the `Animal` class constructor to initialize `name`.
- The `super.display()` invokes the `Animal` class `display()` method.

---

### **Real-World Analogy**

1. **`this`**:
   - Imagine you are working on your tasks. You refer to your desk (current instance) for all your personal resources.

2. **`super`**:
   - When you need additional tools from your supervisor's desk (parent class), you explicitly reference their desk using `super`.

---

### **Key Points to Remember**

1. **super() in Constructor Chaining**:
   - Must be the first statement in the subclass constructor.
2. **Accessing Parent Members**:
   - Use `super` to access hidden fields or overridden methods in the parent class.
3. **Combining `this` and `super`**:
   - `this` refers to the current object; `super` refers to the parent class.

---

### **Conclusion**

The **`super`** keyword in Java is a powerful tool for managing inheritance. It provides access to the parent class's fields, methods, and constructors, ensuring proper initialization and avoiding ambiguity in method or variable access. Mastering the `super` keyword is essential for understanding and effectively using inheritance in Java applications.

## What is the Difference Between this and super Keyword
### **Difference Between `this` and `super` Keywords in Java**

The **`this`** and **`super`** keywords are reference variables used in Java to refer to the **current class** and **parent class** objects, respectively. Both are extensively used in inheritance and object-oriented programming to eliminate ambiguity and improve code readability.

---

### **Overview**

| **Aspect**                  | **`this` Keyword**                                       | **`super` Keyword**                                       |
|-----------------------------|---------------------------------------------------------|----------------------------------------------------------|
| **Definition**               | Refers to the current instance of the class.            | Refers to the immediate parent class of the current class.|
| **Access**                   | Accesses current class's fields, methods, or constructors. | Accesses parent class's fields, methods, or constructors. |
| **Constructor Chaining**     | Used to call another constructor in the same class.     | Used to call the parent class constructor.               |
| **Method Invocation**        | Calls current class methods explicitly.                 | Calls parent class methods explicitly.                   |
| **Scope**                    | Applicable within the same class.                       | Applicable in inheritance hierarchy.                     |

---

### **Key Differences**

#### **1. Context**

- **`this`**: Refers to the current object of the class where it is used.
- **`super`**: Refers to the parent class of the current object.

#### **Example**
```java
class Parent {
    int age = 50;
}

class Child extends Parent {
    int age = 25;

    void showAge() {
        System.out.println("Current class age (this): " + this.age);  // Current class variable
        System.out.println("Parent class age (super): " + super.age); // Parent class variable
    }
}

public class Main {
    public static void main(String[] args) {
        Child child = new Child();
        child.showAge();
    }
}
```

#### **Output**
```
Current class age (this): 25
Parent class age (super): 50
```

---

#### **2. Variable Access**

- **`this`**: Used to access current class variables, especially when they are shadowed by method or constructor parameters.
- **`super`**: Used to access parent class variables when they are hidden by the child class variables.

---

#### **3. Constructor Invocation**

- **`this()`**: Used for constructor chaining within the same class.
- **`super()`**: Used to call the parent class constructor.

#### **Example: Constructor Invocation**
```java
class Parent {
    Parent(String message) {
        System.out.println("Parent Constructor: " + message);
    }
}

class Child extends Parent {
    Child() {
        super("Hello from Parent!"); // Calls Parent's constructor
        System.out.println("Child Constructor");
    }
}

public class Main {
    public static void main(String[] args) {
        Child child = new Child();
    }
}
```

#### **Output**
```
Parent Constructor: Hello from Parent!
Child Constructor
```

---

#### **4. Method Invocation**

- **`this`**: Used to explicitly call a method of the current class.
- **`super`**: Used to call a method of the parent class, especially if it is overridden in the child class.

#### **Example: Method Invocation**
```java
class Parent {
    void display() {
        System.out.println("Display method in Parent class.");
    }
}

class Child extends Parent {
    @Override
    void display() {
        System.out.println("Display method in Child class.");
    }

    void show() {
        super.display(); // Calls Parent's display method
        this.display();  // Calls Child's display method
    }
}

public class Main {
    public static void main(String[] args) {
        Child child = new Child();
        child.show();
    }
}
```

#### **Output**
```
Display method in Parent class.
Display method in Child class.
```

---

#### **5. Object Reference**

- **`this`**: Refers to the current object instance.
- **`super`**: Refers to the current object as an instance of the parent class.

---

#### **6. Static Context**

- **`this`**: Cannot be used in static methods or static contexts.
- **`super`**: Cannot be used in static methods or static contexts.

#### Example:
```java
class Example {
    static void staticMethod() {
        // System.out.println(this); // Error: Cannot use `this` in a static context
        // System.out.println(super); // Error: Cannot use `super` in a static context
    }
}
```

---

### **Combined Example: `this` vs `super`**

```java
class Parent {
    String name = "Parent Class";

    Parent() {
        System.out.println("Parent Constructor");
    }

    void show() {
        System.out.println("Method in Parent");
    }
}

class Child extends Parent {
    String name = "Child Class";

    Child() {
        super(); // Calls Parent constructor
        System.out.println("Child Constructor");
    }

    void displayNames() {
        System.out.println("Name from current class (this): " + this.name);
        System.out.println("Name from parent class (super): " + super.name);
    }

    @Override
    void show() {
        System.out.println("Method in Child");
    }

    void displayMethods() {
        super.show(); // Calls Parent's show method
        this.show();  // Calls Child's show method
    }
}

public class Main {
    public static void main(String[] args) {
        Child child = new Child();

        System.out.println();
        child.displayNames();

        System.out.println();
        child.displayMethods();
    }
}
```

#### **Output**
```
Parent Constructor
Child Constructor

Name from current class (this): Child Class
Name from parent class (super): Parent Class

Method in Parent
Method in Child
```

#### **Explanation**
1. **Constructor Invocation**:
   - `super()` is used to call the parent constructor from the child constructor.
2. **Variable Access**:
   - `this.name` accesses the `name` field of the current class.
   - `super.name` accesses the `name` field of the parent class.
3. **Method Invocation**:
   - `super.show()` calls the `show()` method in the parent class.
   - `this.show()` calls the overridden `show()` method in the child class.

---

### **Key Points to Remember**

1. **Access Scope**:
   - `this` refers to the current class instance.
   - `super` refers to the immediate parent class.

2. **Constructor Chaining**:
   - Use `this()` to chain constructors within the same class.
   - Use `super()` to invoke the parent class constructor.

3. **Method and Variable Access**:
   - Use `this` to access current class methods and variables.
   - Use `super` to access parent class methods and variables.

4. **Static Context**:
   - Neither `this` nor `super` can be used in static contexts.

---

### **Conclusion**

The **`this`** and **`super`** keywords in Java are essential tools for managing inheritance and eliminating ambiguity. While `this` refers to the current object, `super` allows access to the immediate parent class's members. By understanding their differences and use cases, you can write cleaner and more maintainable Java code, particularly when working with inheritance.

## What is instanceof Keyword
### **What is the `instanceof` Keyword in Java?**

The **`instanceof`** keyword in Java is a binary operator used to test whether an object is an **instance** of a specific class, subclass, or implements a specific interface. It is a **type-checking operator** that evaluates to `true` if the object belongs to the specified type or inherits from it; otherwise, it evaluates to `false`.

---

### **Syntax**

```java
object instanceof ClassName
```

- **`object`**: The reference variable that you want to check.
- **`ClassName`**: The class, subclass, or interface to check the type against.

---

### **Key Features of `instanceof`**

1. **Checks Compatibility**:
   - Ensures that an object can be safely cast to a specific type.
   
2. **Avoids Runtime Exceptions**:
   - Prevents `ClassCastException` during explicit typecasting.
   
3. **Supports Inheritance**:
   - Works with parent and child classes in an inheritance hierarchy.
   
4. **Works with Interfaces**:
   - Tests if an object implements a specific interface.

5. **Null Safety**:
   - Returns `false` if the object is `null`.

---

### **How `instanceof` Works**

- If the object is of the specified type or inherits from the specified type, the result is `true`.
- If the object does not belong to the specified type or is `null`, the result is `false`.

---

### **Examples**

#### **1. Checking Class Type**

```java
class Animal {
}

class Dog extends Animal {
}

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal();
        Dog dog = new Dog();

        System.out.println(animal instanceof Animal); // true
        System.out.println(dog instanceof Animal);    // true
        System.out.println(dog instanceof Dog);       // true
        System.out.println(animal instanceof Dog);    // false
    }
}
```

#### **Output**
```
true
true
true
false
```

#### **Explanation**
- `animal instanceof Animal`: `animal` is an instance of the `Animal` class, so the result is `true`.
- `dog instanceof Animal`: `dog` is a subclass of `Animal`, so the result is `true`.
- `dog instanceof Dog`: `dog` is an instance of the `Dog` class, so the result is `true`.
- `animal instanceof Dog`: `animal` is not an instance of the `Dog` class, so the result is `false`.

---

#### **2. Checking Interface Implementation**

```java
interface Pet {
}

class Cat implements Pet {
}

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat();

        System.out.println(cat instanceof Pet); // true
        System.out.println(cat instanceof Cat); // true
    }
}
```

#### **Output**
```
true
true
```

#### **Explanation**
- `cat instanceof Pet`: The `Cat` class implements the `Pet` interface, so the result is `true`.
- `cat instanceof Cat`: `cat` is an instance of the `Cat` class, so the result is `true`.

---

#### **3. Null Safety**

```java
class Animal {
}

public class Main {
    public static void main(String[] args) {
        Animal animal = null;

        System.out.println(animal instanceof Animal); // false
    }
}
```

#### **Output**
```
false
```

#### **Explanation**
- If the object is `null`, the `instanceof` operator always returns `false`.

---

#### **4. Avoiding ClassCastException**

The `instanceof` operator is commonly used before typecasting to avoid runtime exceptions.

```java
class Animal {
}

class Dog extends Animal {
}

public class Main {
    public static void main(String[] args) {
        Animal animal = new Dog();

        if (animal instanceof Dog) {
            Dog dog = (Dog) animal; // Safe typecasting
            System.out.println("Successfully cast to Dog.");
        }
    }
}
```

#### **Output**
```
Successfully cast to Dog.
```

---

### **Rules for Using `instanceof`**

1. **Parent-Child Relationship**:
   - The `instanceof` operator works with parent and child classes.
   
2. **Interfaces**:
   - Checks if an object implements a specific interface.
   
3. **Primitive Types**:
   - The `instanceof` operator cannot be used with primitive types (e.g., `int`, `double`).

4. **Compile-Time Type Checking**:
   - The operator must have a valid type relationship at compile time. Otherwise, it results in a compilation error.

   #### Invalid Example
   ```java
   String str = "Hello";
   System.out.println(str instanceof Integer); // Compile-time error
   ```

---

### **Real-World Example: Polymorphism**

#### Example: Dynamic Method Dispatch
```java
class Animal {
    void makeSound() {
        System.out.println("Animal makes a sound.");
    }
}

class Dog extends Animal {
    @Override
    void makeSound() {
        System.out.println("Dog barks.");
    }
}

class Cat extends Animal {
    @Override
    void makeSound() {
        System.out.println("Cat meows.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal animal = new Dog(); // Polymorphic reference

        if (animal instanceof Dog) {
            animal.makeSound(); // Output: Dog barks.
        }

        animal = new Cat(); // Reassign to Cat
        if (animal instanceof Cat) {
            animal.makeSound(); // Output: Cat meows.
        }
    }
}
```

#### **Output**
```
Dog barks.
Cat meows.
```

---

### **Advantages of `instanceof`**

1. **Safe Typecasting**:
   - Prevents runtime errors by ensuring an object is compatible with the target type.
   
2. **Supports Polymorphism**:
   - Allows the program to dynamically identify object types during runtime.

3. **Null-Safe**:
   - Returns `false` for `null` references, avoiding `NullPointerException`.

---

### **Disadvantages of `instanceof`**

1. **Breaks Encapsulation**:
   - Excessive use of `instanceof` can indicate poor design, as it may lead to type-checking logic spread across the code.
   
2. **Performance Overhead**:
   - Type checking during runtime may add a slight performance overhead.

3. **Alternative Design**:
   - Polymorphism should ideally replace `instanceof` checks where possible.

---

### **Best Practices**

1. **Minimize Usage**:
   - Use `instanceof` only when necessary. Favor polymorphism to avoid explicit type checks.
   
2. **Use with Casting**:
   - Always combine `instanceof` with typecasting to ensure safe operations.
   
3. **Avoid in Non-Polymorphic Situations**:
   - Don't use `instanceof` when the object's type is guaranteed.

---

### **Conclusion**

The **`instanceof`** keyword in Java is a powerful operator for type-checking at runtime. It ensures safe casting, supports polymorphism, and avoids runtime exceptions like `ClassCastException`. However, its overuse can indicate poor design and should be carefully managed in favor of more robust object-oriented programming techniques. Understanding `instanceof` and its use cases is essential for writing safe and efficient Java code.

## What is synchronized Keyword
### **What is the `synchronized` Keyword in Java?**

The **`synchronized`** keyword in Java is a modifier used to control **access to critical sections of code or shared resources** by multiple threads. It ensures that only one thread can execute a block of code or a method at a time, providing **thread safety** and avoiding **race conditions**.

In multithreading, when multiple threads access shared resources simultaneously, inconsistencies can occur. The `synchronized` keyword helps to avoid these issues by implementing a lock mechanism.

---

### **Key Features of the `synchronized` Keyword**

1. **Thread Safety**:
   - Ensures that only one thread executes the synchronized block or method at a time.
   
2. **Locking Mechanism**:
   - Acquires a lock on an object or class before executing a synchronized block/method and releases the lock once execution is complete.
   
3. **Types of Synchronization**:
   - **Method-level Synchronization**: Synchronizes the entire method.
   - **Block-level Synchronization**: Synchronizes a specific block of code inside a method.

4. **Locks**:
   - Instance methods acquire the **object lock**.
   - Static methods acquire the **class lock**.

5. **Prevents Data Corruption**:
   - Prevents threads from modifying shared resources simultaneously.

---

### **How to Use the `synchronized` Keyword**

#### **1. Synchronized Instance Methods**

A synchronized instance method locks the **current object** (`this`) so that no other thread can access other synchronized methods of the same object until the lock is released.

#### Example: Synchronized Instance Method

```java
class Counter {
    private int count = 0;

    // Synchronized method
    public synchronized void increment() {
        count++;
    }

    public int getCount() {
        return count;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        Runnable task = () -> {
            for (int i = 0; i < 1000; i++) {
                counter.increment();
            }
        };

        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println("Final Count: " + counter.getCount()); // Output: 2000
    }
}
```

#### **Explanation**
- The `increment()` method is synchronized, ensuring that only one thread can modify the `count` variable at a time.
- Without synchronization, race conditions could occur, resulting in an incorrect `count`.

---

#### **2. Synchronized Static Methods**

A synchronized static method locks the **class object** (`ClassName.class`). This ensures that only one thread can execute any synchronized static method of the class.

#### Example: Synchronized Static Method

```java
class Counter {
    private static int count = 0;

    // Synchronized static method
    public static synchronized void increment() {
        count++;
    }

    public static int getCount() {
        return count;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            for (int i = 0; i < 1000; i++) {
                Counter.increment();
            }
        };

        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println("Final Count: " + Counter.getCount()); // Output: 2000
    }
}
```

#### **Explanation**
- The `increment()` method is synchronized and static, ensuring that it locks the class object.
- This guarantees thread safety for shared static variables.

---

#### **3. Synchronized Block**

A synchronized block locks only a specific block of code rather than the entire method. It provides more fine-grained control over synchronization, improving performance by limiting the scope of the lock.

#### Example: Synchronized Block

```java
class Counter {
    private int count = 0;

    public void increment() {
        synchronized (this) { // Synchronizing a specific block
            count++;
        }
    }

    public int getCount() {
        return count;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        Runnable task = () -> {
            for (int i = 0; i < 1000; i++) {
                counter.increment();
            }
        };

        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println("Final Count: " + counter.getCount()); // Output: 2000
    }
}
```

#### **Explanation**
- The synchronized block only locks the critical section, allowing other threads to execute non-critical code simultaneously.
- Improves performance compared to synchronizing the entire method.

---

#### **4. Synchronizing on a Different Object**

You can synchronize on a specific object other than `this` or the class object. This is useful when working with shared resources.

#### Example

```java
class Counter {
    private int count = 0;
    private final Object lock = new Object();

    public void increment() {
        synchronized (lock) { // Locking on a custom object
            count++;
        }
    }

    public int getCount() {
        return count;
    }
}
```

---

### **Comparison of Synchronization Types**

| **Type**                  | **Scope of Lock**             | **Use Case**                                      |
|---------------------------|-------------------------------|-------------------------------------------------|
| **Synchronized Method**   | Entire method.                | When the entire method is critical for thread safety. |
| **Synchronized Block**    | Specific block of code.       | When only part of the method needs synchronization. |
| **Static Synchronization**| Class object.                 | When protecting static variables or methods.     |

---

### **Advantages of `synchronized`**

1. **Thread Safety**:
   - Prevents data inconsistency and corruption by allowing only one thread to access critical code.
2. **Simple to Use**:
   - Provides an easy way to implement thread synchronization.
3. **Prevents Race Conditions**:
   - Ensures that shared resources are accessed safely.

---

### **Disadvantages of `synchronized`**

1. **Performance Overhead**:
   - Synchronization introduces delays as threads have to wait for locks.
2. **Risk of Deadlock**:
   - Poorly designed synchronization can lead to deadlock, where two or more threads wait indefinitely for each other’s locks.
3. **Limited Scope**:
   - Does not address higher-level concurrency issues like thread starvation.

---

### **Real-World Analogy**

Think of a **printer** shared by multiple employees in an office:
- Only one person can use the printer at a time (thread safety).
- The person locks the printer while printing (acquiring a lock).
- Once printing is done, the lock is released for others to use.

---

### **Best Practices**

1. **Minimize the Scope of Synchronization**:
   - Use synchronized blocks instead of synchronizing entire methods to reduce performance overhead.
2. **Avoid Deadlocks**:
   - Be cautious when using nested synchronized blocks or multiple locks.
3. **Prefer Alternatives for High Concurrency**:
   - Use Java's `java.util.concurrent` package (e.g., `ReentrantLock`, `ConcurrentHashMap`) for advanced concurrency needs.

---

### **Conclusion**

The **`synchronized`** keyword in Java is a powerful tool for ensuring thread safety in multithreaded applications. It allows only one thread to execute a critical section of code at a time, preventing race conditions and data corruption. By understanding its uses, types, and limitations, developers can effectively manage shared resources and build robust multithreaded applications. However, for complex scenarios, alternatives like `java.util.concurrent` classes may be more efficient.

## What is volatile Keyword
### **What is the `volatile` Keyword in Java?**

The **`volatile`** keyword in Java is a modifier that is used with variables to ensure **visibility** and **ordering** of changes to that variable across multiple threads. A `volatile` variable is stored in the **main memory** instead of the thread's local cache, ensuring that all threads see the most up-to-date value of the variable.

---

### **Key Features of the `volatile` Keyword**

1. **Visibility**:
   - Ensures that changes made to a `volatile` variable by one thread are visible to all other threads immediately.
   
2. **Atomic Read and Write**:
   - Guarantees atomicity for single read/write operations on the `volatile` variable. However, compound actions (e.g., `i++`) are not atomic.

3. **Ordering**:
   - Prevents instruction reordering by the compiler or CPU, ensuring proper execution order concerning `volatile` variables.

4. **No Locking**:
   - Unlike `synchronized`, `volatile` does not block threads or introduce contention.

---

### **When to Use `volatile`**

1. **When a Variable is Shared Across Threads**:
   - Use `volatile` for variables that multiple threads read and write, but no thread modifies the variable using compound actions (e.g., increment).

2. **When Visibility is Critical**:
   - Use `volatile` to ensure that the latest value of a variable is visible to all threads.

3. **When Ordering is Important**:
   - Use `volatile` to prevent instruction reordering issues in multithreaded programs.

---

### **Syntax**

```java
volatile data_type variable_name;
```

Example:
```java
private volatile boolean flag;
```

---

### **How `volatile` Works**

#### **1. Visibility Example**
Without `volatile`, changes made by one thread may not be visible to other threads due to caching.

```java
class Worker extends Thread {
    private volatile boolean running = true;

    public void run() {
        while (running) {
            // Thread keeps running until 'running' is false
        }
        System.out.println("Thread stopped.");
    }

    public void stopRunning() {
        running = false; // Changes to 'running' will be visible to all threads
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Worker worker = new Worker();
        worker.start();

        Thread.sleep(1000);
        worker.stopRunning(); // Safely stops the thread
    }
}
```

#### **Output**
```
Thread stopped.
```

#### **Explanation**
- The `volatile` keyword ensures that the change to the `running` variable by the `stopRunning()` method is visible to the `run()` method in the `Worker` thread.

---

#### **2. Instruction Reordering Example**

The `volatile` keyword prevents instruction reordering, ensuring proper program behavior.

```java
class ReorderingExample {
    private volatile boolean flag = false;
    private int value = 0;

    public void writer() {
        value = 42;    // Step 1
        flag = true;   // Step 2 (volatile write ensures Step 1 happens first)
    }

    public void reader() {
        if (flag) {    // Step 3
            System.out.println(value); // Step 4 (always prints 42 due to volatile)
        }
    }
}
```

#### **Explanation**
- Without `volatile`, the compiler or CPU might reorder instructions, potentially executing `flag = true` before `value = 42`. The `volatile` keyword prevents this.

---

### **Limitations of `volatile`**

1. **No Atomicity for Compound Actions**:
   - `volatile` does not make compound actions (e.g., `i++`) atomic.

   Example:
   ```java
   class Counter {
       private volatile int count = 0;

       public void increment() {
           count++; // Not atomic: read, increment, write are separate steps
       }
   }
   ```

   Solution: Use `synchronized` or `AtomicInteger` for compound operations.

2. **Not a Replacement for Synchronization**:
   - `volatile` only ensures visibility and ordering, not mutual exclusion.

3. **Limited Use Cases**:
   - Suitable for simple state flags or single-variable updates, but not for complex data structures or compound operations.

---

### **Difference Between `volatile` and `synchronized`**

| **Aspect**                 | **`volatile`**                                  | **`synchronized`**                              |
|----------------------------|-----------------------------------------------|-----------------------------------------------|
| **Purpose**                 | Ensures visibility and prevents instruction reordering. | Ensures mutual exclusion and thread safety.   |
| **Atomicity**               | Does not guarantee atomicity for compound actions. | Guarantees atomicity for synchronized blocks. |
| **Locking**                 | No locking mechanism, so threads are not blocked. | Uses locking, causing threads to block.       |
| **Performance**             | Faster due to no locking overhead.            | Slower due to locking and context switching.  |
| **Use Case**                | Best for simple flags or variables shared across threads. | Best for complex operations or critical sections. |

---

### **Practical Use Cases of `volatile`**

1. **Stop a Thread Safely**:
   - Use `volatile` for a shared flag that signals a thread to stop execution.

2. **Double-Checked Locking for Singleton Pattern**:
   - Use `volatile` to ensure proper initialization of a singleton instance.

#### Example: Double-Checked Locking
```java
class Singleton {
    private static volatile Singleton instance;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton(); // Volatile ensures visibility
                }
            }
        }
        return instance;
    }
}
```

3. **Shared Status Flags**:
   - Use `volatile` for variables like `boolean isRunning` or `boolean isUpdated`.

---

### **Common Pitfalls**

1. **Misuse for Atomicity**:
   - Using `volatile` alone for incrementing or updating shared variables can lead to race conditions.

2. **Overhead in Frequent Updates**:
   - Declaring frequently updated variables as `volatile` can degrade performance due to constant writes to main memory.

3. **Incorrect Use for Complex Synchronization**:
   - `volatile` cannot replace locks for operations involving multiple variables or steps.

---

### **Advantages of `volatile`**

1. **Improved Performance**:
   - Avoids the overhead of locks.
   
2. **Simplifies Code**:
   - Easy to implement for simple visibility requirements.

3. **No Deadlocks**:
   - Does not use locks, so deadlocks are impossible.

---

### **Disadvantages of `volatile`**

1. **Limited Scope**:
   - Suitable only for simple use cases like flags or single-variable updates.
   
2. **No Mutual Exclusion**:
   - Does not prevent simultaneous access by multiple threads.

3. **Not Atomic**:
   - Cannot be used for compound operations like `i++`.

---

### **Conclusion**

The **`volatile`** keyword in Java is a lightweight mechanism for ensuring **visibility** and **ordering** of shared variables in multithreaded programs. It is ideal for simple cases where only visibility is required, such as stopping threads or updating state flags. However, for more complex synchronization needs, `synchronized` blocks or classes from the `java.util.concurrent` package are better suited. Understanding when and how to use `volatile` is essential for writing efficient and thread-safe Java applications.

## What is transient Keyword
### **What is the `transient` Keyword in Java?**

The **`transient`** keyword in Java is a modifier used with fields of a class to indicate that the field should not be serialized when the object is converted into a byte stream during **serialization**. Serialization is the process of saving the state of an object so it can be stored or transmitted and later reconstructed.

When a field is marked as `transient`, its value is not included in the serialized representation of the object, and its value is set to the default value (e.g., `null` for objects, `0` for integers) when the object is deserialized.

---

### **Key Characteristics of the `transient` Keyword**

1. **Used in Serialization**:
   - Fields marked as `transient` are skipped during serialization.

2. **Applicable Only to Fields**:
   - The `transient` modifier can only be applied to instance variables (fields). It cannot be applied to methods, local variables, or classes.

3. **Default Values on Deserialization**:
   - Transient fields are initialized to their default values during deserialization.

4. **Access Modifiers**:
   - Can be used with any access modifier (`private`, `protected`, `public`).

---

### **Syntax**

```java
transient data_type field_name;
```

Example:
```java
private transient String password;
```

---

### **Why Use the `transient` Keyword?**

1. **Sensitive Data**:
   - Exclude sensitive information like passwords or security tokens during serialization to avoid exposing them.

2. **Non-Serializable Fields**:
   - Skip fields that cannot or should not be serialized (e.g., database connections, file streams).

3. **Optimizing Serialization**:
   - Exclude unnecessary fields from the serialized output to reduce the size.

---

### **How the `transient` Keyword Works**

#### Example: Using `transient` in Serialization

```java
import java.io.*;

class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private String username;
    private transient String password; // Marked as transient

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Username: " + username + ", Password: " + password;
    }
}

public class Main {
    public static void main(String[] args) {
        User user = new User("Alice", "12345");

        // Serialize the object
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("user.ser"))) {
            oos.writeObject(user);
            System.out.println("Object serialized: " + user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Deserialize the object
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("user.ser"))) {
            User deserializedUser = (User) ois.readObject();
            System.out.println("Object deserialized: " + deserializedUser);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```

#### **Output**
```
Object serialized: Username: Alice, Password: 12345
Object deserialized: Username: Alice, Password: null
```

#### **Explanation**
- The `password` field is marked as `transient`, so its value (`12345`) is excluded during serialization.
- After deserialization, the `password` field is set to its default value (`null`).

---

### **Behavior of `transient` with Different Data Types**

| **Data Type** | **Default Value After Deserialization** |
|---------------|-----------------------------------------|
| `int`         | `0`                                     |
| `float`       | `0.0f`                                  |
| `boolean`     | `false`                                 |
| `char`        | `'\u0000'`                              |
| `Object`      | `null`                                  |

---

### **When to Use the `transient` Keyword**

1. **Sensitive Information**:
   - Passwords, credit card details, or any confidential information should not be serialized.

2. **Non-Serializable Objects**:
   - Fields like database connections, file handles, or threads cannot be serialized. Use `transient` to skip them during serialization.

3. **Large Data**:
   - Avoid serializing large fields (e.g., cached data or large collections) to reduce the serialized file size.

4. **Derived Data**:
   - Fields that can be derived or recalculated after deserialization.

---

### **Custom Serialization for Transient Fields**

Even though `transient` fields are excluded during serialization, their values can be restored using **custom serialization** by overriding the `writeObject()` and `readObject()` methods in the class.

#### Example: Custom Serialization for Transient Fields

```java
import java.io.*;

class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private String username;
    private transient String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject(); // Serialize non-transient fields
        oos.writeObject(password); // Manually serialize the transient field
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject(); // Deserialize non-transient fields
        password = (String) ois.readObject(); // Manually deserialize the transient field
    }

    @Override
    public String toString() {
        return "Username: " + username + ", Password: " + password;
    }
}

public class Main {
    public static void main(String[] args) {
        User user = new User("Alice", "12345");

        // Serialize the object
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("user.ser"))) {
            oos.writeObject(user);
            System.out.println("Object serialized: " + user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Deserialize the object
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("user.ser"))) {
            User deserializedUser = (User) ois.readObject();
            System.out.println("Object deserialized: " + deserializedUser);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```

#### **Output**
```
Object serialized: Username: Alice, Password: 12345
Object deserialized: Username: Alice, Password: 12345
```

#### **Explanation**
- The `writeObject()` and `readObject()` methods handle the manual serialization and deserialization of the `transient` field `password`.

---

### **Advantages of the `transient` Keyword**

1. **Improved Security**:
   - Ensures that sensitive data is not serialized.

2. **Reduces Serialized File Size**:
   - Excludes unnecessary fields, reducing storage and transmission costs.

3. **Handles Non-Serializable Fields**:
   - Skips fields that cannot be serialized, avoiding runtime exceptions.

---

### **Disadvantages of the `transient` Keyword**

1. **Manual Restoration**:
   - Requires custom serialization to restore transient fields.

2. **Limited to Fields**:
   - Only applicable to fields, not methods or classes.

3. **Requires Careful Use**:
   - Misuse can lead to incomplete object states after deserialization.

---

### **Common Use Cases**

1. **Passwords and Tokens**:
   - Exclude sensitive information from being stored in serialized files.

2. **Cache Data**:
   - Skip fields that store temporary or derived data.

3. **Non-Serializable Fields**:
   - Fields like `Thread`, `Socket`, or `Database Connection` should be marked `transient`.

---

### **Difference Between `transient` and `static`**

| **Aspect**           | **`transient`**                                  | **`static`**                                  |
|-----------------------|-------------------------------------------------|----------------------------------------------|
| **Purpose**           | Prevents field from being serialized.           | Excludes field from being serialized because it belongs to the class, not an instance. |
| **Scope**             | Applies to instance variables only.             | Applies to class-level variables only.       |
| **Serialization**     | Affects serialization explicitly.               | Not serialized by default.                   |

---

### **Conclusion**

The **`transient`** keyword is a valuable tool in Java for controlling what fields are included in serialization. It provides a simple and effective way to ensure sensitive or unnecessary fields are excluded from the serialized representation of an object. While it simplifies serialization in many cases, understanding its limitations and combining it with custom serialization when necessary ensures robust and secure handling of objects in Java.

## What is the final Keyword
### **What is the `final` Keyword in Java?**

The **`final`** keyword in Java is a modifier that can be applied to **variables**, **methods**, and **classes**. It is used to impose restrictions in the code:
1. **Final Variable**: The value cannot be changed after initialization (acts as a constant).
2. **Final Method**: The method cannot be overridden by subclasses.
3. **Final Class**: The class cannot be extended (inherited).

---

### **Syntax**

```java
final data_type variable_name = value; // Final variable
final return_type method_name() { ... } // Final method
final class ClassName { ... } // Final class
```

---

### **1. Final Variables**

A `final` variable is a constant, meaning its value cannot be changed once it is initialized. It must be initialized either at the time of declaration or in the constructor.

#### **Characteristics**
- **Immutable Value**: Once assigned, the value cannot be changed.
- **Static and Final**: Often used together for constants.
- **Blank Final Variable**: A `final` variable that is not initialized at the time of declaration but is initialized later (e.g., in a constructor).

#### **Example: Final Variable**
```java
class Example {
    final int MAX_VALUE = 100; // Initialized at declaration

    final int MIN_VALUE; // Blank final variable

    Example(int minValue) {
        this.MIN_VALUE = minValue; // Initialized in constructor
    }

    void display() {
        System.out.println("MAX_VALUE: " + MAX_VALUE);
        System.out.println("MIN_VALUE: " + MIN_VALUE);
    }
}

public class Main {
    public static void main(String[] args) {
        Example obj = new Example(10);
        obj.display();

        // obj.MAX_VALUE = 200; // Error: Cannot assign a value to final variable
    }
}
```

#### **Output**
```
MAX_VALUE: 100
MIN_VALUE: 10
```

#### **Explanation**
- `MAX_VALUE` is initialized during declaration and cannot be modified.
- `MIN_VALUE` is a blank final variable and is initialized in the constructor.

---

### **2. Final Methods**

A `final` method cannot be overridden by subclasses. This is used to prevent changing the behavior of a method in derived classes.

#### **Characteristics**
- Prevents method overriding.
- Useful for methods that provide essential or non-modifiable behavior.

#### **Example: Final Method**
```java
class Parent {
    final void display() {
        System.out.println("This is a final method in Parent class.");
    }
}

class Child extends Parent {
    // void display() { ... } // Error: Cannot override the final method
}

public class Main {
    public static void main(String[] args) {
        Child obj = new Child();
        obj.display();
    }
}
```

#### **Output**
```
This is a final method in Parent class.
```

#### **Explanation**
- The `display()` method in the `Parent` class is declared as `final`, preventing the `Child` class from overriding it.

---

### **3. Final Classes**

A `final` class cannot be extended (inherited) by any other class. This is used to prevent inheritance when a class is designed to provide specific functionality that should not be modified or extended.

#### **Characteristics**
- Prevents inheritance.
- Ensures the class's implementation remains unaltered.

#### **Example: Final Class**
```java
final class FinalClass {
    void display() {
        System.out.println("This is a final class.");
    }
}

// class ExtendedClass extends FinalClass { ... } // Error: Cannot inherit from a final class

public class Main {
    public static void main(String[] args) {
        FinalClass obj = new FinalClass();
        obj.display();
    }
}
```

#### **Output**
```
This is a final class.
```

#### **Explanation**
- The `FinalClass` is declared as `final`, preventing any class from inheriting it.

---

### **Key Differences: Final Variable, Final Method, and Final Class**

| **Aspect**               | **Final Variable**                         | **Final Method**                           | **Final Class**                            |
|--------------------------|--------------------------------------------|-------------------------------------------|-------------------------------------------|
| **Purpose**               | Prevents reassignment of a variable.       | Prevents overriding of a method.           | Prevents inheritance of a class.           |
| **Scope**                 | Applies to variables.                     | Applies to methods.                        | Applies to classes.                        |
| **Usage**                 | Used for constants.                       | Used to lock the behavior of a method.     | Used to lock the behavior of a class.      |
| **Restrictions**          | Value cannot be reassigned.               | Cannot be overridden in subclasses.        | Cannot be extended (inherited).            |

---

### **Static Final Variables (Constants)**

The combination of `static` and `final` is used to create constants that belong to the class rather than an instance.

#### Example
```java
class Constants {
    static final double PI = 3.14159;
    static final String APP_NAME = "My Application";
}

public class Main {
    public static void main(String[] args) {
        System.out.println("PI: " + Constants.PI);
        System.out.println("Application Name: " + Constants.APP_NAME);

        // Constants.PI = 3.14; // Error: Cannot assign a value to final variable
    }
}
```

#### **Output**
```
PI: 3.14159
Application Name: My Application
```

#### **Explanation**
- `PI` and `APP_NAME` are constants that can be accessed without creating an instance of the `Constants` class.

---

### **Final Keyword and Inheritance**

1. **Final Method in Parent Class**:
   - Prevents the child class from overriding the method.

2. **Final Class**:
   - Prevents any class from inheriting it.

#### Example
```java
class Parent {
    final void display() {
        System.out.println("Final method in Parent.");
    }
}

final class FinalParent {
    void show() {
        System.out.println("This is a final class.");
    }
}

// class Child extends FinalParent { ... } // Error: Cannot inherit from final class
```

---

### **Final Keyword and Abstract Classes**

1. **Final Methods in Abstract Classes**:
   - A final method in an abstract class can be inherited but not overridden.

2. **Final Variables in Abstract Classes**:
   - Can be declared and must be initialized during declaration.

#### Example
```java
abstract class AbstractClass {
    final void finalMethod() {
        System.out.println("Final method in Abstract Class.");
    }
}

class ConcreteClass extends AbstractClass {
    // void finalMethod() { ... } // Error: Cannot override final method
}
```

---

### **Final and Performance**

1. **Performance Optimization**:
   - The JVM may optimize code involving `final` methods, variables, and classes, as their behavior is predictable.
   
2. **Compiler Optimizations**:
   - Final variables allow the compiler to make better optimizations because their values remain constant.

---

### **Common Use Cases**

1. **Defining Constants**:
   - Use `final` with `static` for application-wide constants.

2. **Preventing Method Overriding**:
   - Use `final` methods in classes that provide critical or sensitive functionality.

3. **Preventing Inheritance**:
   - Use `final` classes for security or utility purposes (e.g., `String` class in Java is `final`).

---

### **Advantages of the Final Keyword**

1. **Immutability**:
   - Final variables ensure constants, making the code predictable and safe.

2. **Enhanced Security**:
   - Prevents accidental modification of critical classes or methods.

3. **Performance Improvements**:
   - Allows the JVM to optimize final methods and classes.

4. **Improved Code Design**:
   - Encourages immutability and predictable behavior in object-oriented programming.

---

### **Disadvantages of the Final Keyword**

1. **Reduced Flexibility**:
   - Final classes and methods limit extensibility and inheritance.

2. **Overuse Can Be Restrictive**:
   - Excessive use of `final` can make the code less adaptable to changes.

---

### **Conclusion**

The **`final`** keyword in Java is a versatile and powerful modifier that enforces immutability and restricts modifications in variables, methods, and classes. It plays a crucial role in ensuring **security**, **consistency**, and **predictability** in object-oriented programming. Proper understanding and use of `final` can help create robust and maintainable applications while avoiding misuse that could limit flexibility.

## What is the Difference Between final, finally, and finalize() Keyword in Java
### **Difference Between `final`, `finally`, and `finalize()` in Java**

The **`final`**, **`finally`**, and **`finalize()`** keywords in Java are distinct and serve completely different purposes. Despite their similar names, they are not related to each other. Below is a detailed comparison of their purposes, uses, and examples.

---

### **1. `final` Keyword**

The **`final`** keyword is a modifier used to restrict modification in Java. It can be applied to variables, methods, or classes.

#### **Purpose**
- To impose restrictions on variables, methods, and classes.

#### **Key Features**
| **Aspect**          | **Description**                                                                                       |
|---------------------|-----------------------------------------------------------------------------------------------------|
| **Final Variables** | Value cannot be changed after initialization (constant).                                              |
| **Final Methods**   | Cannot be overridden by subclasses.                                                                  |
| **Final Classes**   | Cannot be inherited by any class.                                                                    |

#### **Example**
```java
class FinalExample {
    final int MAX_VALUE = 100; // Final variable

    final void display() { // Final method
        System.out.println("This is a final method.");
    }
}

final class FinalClass { // Final class
    void show() {
        System.out.println("This is a final class.");
    }
}
```

---

### **2. `finally` Keyword**

The **`finally`** block is used in exception handling and ensures that a block of code always executes, regardless of whether an exception is thrown or caught. It is typically used to release resources like closing files, streams, or database connections.

#### **Purpose**
- To ensure a block of code executes whether or not an exception occurs.

#### **Key Features**
| **Aspect**       | **Description**                                                                                          |
|------------------|--------------------------------------------------------------------------------------------------------|
| **Always Executes** | The `finally` block executes after the `try` block, whether an exception is thrown or not.            |
| **Used With `try-catch`** | It is optional but must follow a `try` block if used.                                              |

#### **Example**
```java
class FinallyExample {
    public static void main(String[] args) {
        try {
            int result = 10 / 2; // No exception here
            System.out.println("Result: " + result);
        } catch (ArithmeticException e) {
            System.out.println("Exception caught: " + e.getMessage());
        } finally {
            System.out.println("This is the finally block.");
        }
    }
}
```

#### **Output**
```
Result: 5
This is the finally block.
```

---

### **3. `finalize()` Method**

The **`finalize()`** method is a protected method defined in the `Object` class. It is called by the **garbage collector** before an object is destroyed to perform cleanup activities such as closing resources.

#### **Purpose**
- To perform cleanup actions before an object is garbage collected.

#### **Key Features**
| **Aspect**             | **Description**                                                                                     |
|------------------------|---------------------------------------------------------------------------------------------------|
| **Garbage Collection** | Called by the garbage collector before the object is destroyed.                                   |
| **Defined in `Object`**| It is a method in the `Object` class and can be overridden.                                       |
| **Rarely Used**        | It is not recommended to rely on `finalize()` for resource cleanup (use `try-with-resources` instead). |

#### **Example**
```java
class FinalizeExample {
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object is being garbage collected.");
    }

    public static void main(String[] args) {
        FinalizeExample obj = new FinalizeExample();
        obj = null; // Make the object eligible for garbage collection
        System.gc(); // Request garbage collection
    }
}
```

#### **Output**
```
Object is being garbage collected.
```

#### **Explanation**
- The `finalize()` method is called before the object is garbage collected.
- Note that garbage collection timing is unpredictable.

---

### **Detailed Comparison Table**

| **Aspect**          | **`final`**                                      | **`finally`**                                   | **`finalize()`**                                  |
|---------------------|--------------------------------------------------|------------------------------------------------|-------------------------------------------------|
| **Purpose**          | Restricts modification of variables, methods, or classes. | Ensures a block of code executes in exception handling. | Performs cleanup before an object is garbage collected. |
| **Where Used**       | Applied to variables, methods, and classes.      | Used with `try-catch` blocks.                   | Method of the `Object` class.                    |
| **Keyword/Method**   | Keyword                                          | Keyword                                        | Method (`protected void finalize()`).            |
| **Execution Timing** | During program execution.                        | Always executes after `try-catch`.             | Called by garbage collector before object destruction. |
| **Example**          | `final int x = 10;`                              | `try { ... } finally { ... }`                  | `protected void finalize() { ... }`             |

---

### **When to Use Each**

1. **`final`**:
   - Use for constants (`final int MAX_VALUE = 100;`).
   - Use to prevent method overriding (`final void display();`).
   - Use to prevent inheritance (`final class FinalClass`).

2. **`finally`**:
   - Use to ensure resources are released (`finally { close resources }`).
   - Always include a `finally` block if you need to execute cleanup code.

3. **`finalize()`**:
   - Use only for debugging or learning purposes.
   - Avoid relying on it for resource cleanup; instead, use `try-with-resources` or explicitly manage resources.

---

### **Conclusion**

The **`final`**, **`finally`**, and **`finalize()`** keywords serve entirely different purposes in Java:

1. **`final`** is used for making variables immutable, preventing method overriding, or stopping inheritance.
2. **`finally`** ensures a block of code executes during exception handling, regardless of the outcome.
3. **`finalize()`** is a cleanup method called by the garbage collector before an object is destroyed.

Understanding the differences between these keywords is essential for writing robust and maintainable Java programs.

## What is an Abstract Class
### **What is an Abstract Class in Java?**

An **abstract class** in Java is a class that is **declared with the `abstract` keyword**. It is used to provide a base class with **common attributes and behaviors** while allowing subclasses to provide their specific implementations for certain methods. Abstract classes are part of Java's mechanism for implementing **abstraction**, one of the core principles of object-oriented programming.

---

### **Key Characteristics of an Abstract Class**

1. **Cannot be Instantiated**:
   - An abstract class **cannot be instantiated directly**; it must be extended by a subclass.

2. **Abstract Methods**:
   - An abstract class can contain **abstract methods**, which are declared without implementation.
   - Subclasses must provide implementations for these methods.

3. **Concrete Methods**:
   - An abstract class can also contain **concrete methods** (methods with implementations), allowing shared behavior across all subclasses.

4. **Can Have Fields**:
   - Abstract classes can define fields (variables), constants, and constructors.

5. **Allows Inheritance**:
   - Abstract classes are meant to be **extended** by subclasses, which must provide implementations for abstract methods.

---

### **Syntax of an Abstract Class**

```java
abstract class ClassName {
    // Abstract method (no body)
    abstract void abstractMethod();

    // Concrete method (with body)
    void concreteMethod() {
        System.out.println("This is a concrete method.");
    }
}
```

---

### **Examples**

#### **1. Abstract Class with Abstract and Concrete Methods**

```java
// Abstract class
abstract class Animal {
    String name;

    // Constructor
    Animal(String name) {
        this.name = name;
    }

    // Abstract method
    abstract void sound();

    // Concrete method
    void sleep() {
        System.out.println(name + " is sleeping.");
    }
}

// Subclass
class Dog extends Animal {
    Dog(String name) {
        super(name);
    }

    // Implement abstract method
    @Override
    void sound() {
        System.out.println(name + " says: Woof Woof!");
    }
}

// Subclass
class Cat extends Animal {
    Cat(String name) {
        super(name);
    }

    // Implement abstract method
    @Override
    void sound() {
        System.out.println(name + " says: Meow!");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog("Buddy");
        dog.sound();
        dog.sleep();

        Animal cat = new Cat("Kitty");
        cat.sound();
        cat.sleep();
    }
}
```

#### **Output**
```
Buddy says: Woof Woof!
Buddy is sleeping.
Kitty says: Meow!
Kitty is sleeping.
```

---

#### **2. Abstract Class with No Abstract Methods**

It is possible to have an abstract class without any abstract methods. Such classes cannot be instantiated but can still act as a base class.

```java
abstract class Vehicle {
    void start() {
        System.out.println("Vehicle is starting.");
    }
}

class Car extends Vehicle {
    void drive() {
        System.out.println("Car is driving.");
    }
}

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.start(); // Calls concrete method from abstract class
        car.drive(); // Calls method from subclass
    }
}
```

#### **Output**
```
Vehicle is starting.
Car is driving.
```

---

### **When to Use an Abstract Class**

1. **Common Base Class**:
   - Use abstract classes when you need a base class to define common behaviors or fields shared across multiple subclasses.

2. **Partial Implementation**:
   - When you want to enforce that certain methods must be implemented by subclasses while providing default implementations for others.

3. **Code Reusability**:
   - To avoid code duplication, shared functionality can be implemented in the abstract class.

---

### **Abstract Class vs Interface**

| **Aspect**             | **Abstract Class**                                       | **Interface**                                          |
|------------------------|---------------------------------------------------------|-------------------------------------------------------|
| **Keyword**             | Declared using the `abstract` keyword.                 | Declared using the `interface` keyword.              |
| **Inheritance Type**    | Supports single inheritance.                           | Supports multiple inheritance.                       |
| **Methods**             | Can have both abstract and concrete methods.           | All methods are implicitly abstract (Java 7 and earlier) or default/static (Java 8+). |
| **Fields**              | Can have instance variables, constants, and static fields. | Only constants (implicitly `public static final`).    |
| **Constructor**         | Can have constructors.                                 | Cannot have constructors.                            |
| **Usage**               | Used when classes share common behaviors and fields.   | Used to define contracts that classes must implement. |

---

#### **Example: Abstract Class vs Interface**

```java
// Abstract class
abstract class Shape {
    String color;

    Shape(String color) {
        this.color = color;
    }

    abstract void draw();

    void displayColor() {
        System.out.println("Color: " + color);
    }
}

// Interface
interface Drawable {
    void draw();
}

// Subclass extending abstract class and implementing interface
class Circle extends Shape implements Drawable {
    Circle(String color) {
        super(color);
    }

    @Override
    void draw() {
        System.out.println("Drawing a Circle.");
    }
}

public class Main {
    public static void main(String[] args) {
        Shape circle = new Circle("Red");
        circle.draw();
        circle.displayColor();
    }
}
```

#### **Output**
```
Drawing a Circle.
Color: Red
```

---

### **Advantages of Abstract Classes**

1. **Code Reusability**:
   - Shared logic is implemented once in the abstract class and reused in subclasses.

2. **Enforces Design**:
   - Subclasses must implement abstract methods, ensuring consistent behavior.

3. **Combines Flexibility and Default Behavior**:
   - Allows both flexibility (abstract methods) and default implementations (concrete methods).

---

### **Disadvantages of Abstract Classes**

1. **Single Inheritance Limitation**:
   - A class can extend only one abstract class, limiting flexibility compared to interfaces.

2. **Less Flexible Than Interfaces**:
   - Interfaces allow multiple inheritance, making them more suitable for defining multiple behaviors.

3. **Cannot Be Instantiated**:
   - An abstract class cannot create objects directly, which might be restrictive in some use cases.

---

### **Real-World Analogy**

Consider a **Vehicle**:
- The **abstract class** `Vehicle` defines common attributes like `speed` and behaviors like `start()` and `stop()`.
- Subclasses like `Car`, `Bike`, or `Bus` implement specific behaviors (e.g., `horn()` or `numberOfWheels`).

---

### **Key Points to Remember**

1. An abstract class **cannot be instantiated**.
2. It can contain both **abstract methods** (without a body) and **concrete methods** (with a body).
3. A subclass **must implement all abstract methods** of its parent abstract class unless it is also abstract.
4. Abstract classes can **extend other abstract classes** or concrete classes.
5. Use abstract classes when creating a **base class** with shared fields or methods that subclasses can inherit or override.

---

### **Conclusion**

An **abstract class** in Java provides a blueprint for creating related classes with shared behavior and structure while still allowing for customization in subclasses. By combining abstract methods (enforcing implementation) and concrete methods (providing default behavior), abstract classes strike a balance between flexibility and structure in object-oriented design. Understanding abstract classes is crucial for designing maintainable and reusable Java applications.

## What is an Interface
### **What is an Interface in Java?**

An **interface** in Java is a blueprint for a class that defines a contract of methods that a class must implement. It is a reference type, similar to a class, that contains **abstract methods**, **default methods**, **static methods**, and **constants**. Interfaces are part of Java's mechanism for achieving **abstraction** and **multiple inheritance**.

---

### **Key Characteristics of an Interface**

1. **Defines Behavior**:
   - Specifies a set of methods that implementing classes must provide.

2. **Abstract Methods**:
   - Before Java 8, interfaces could only contain abstract methods. Since Java 8, they can include **default** and **static methods**.

3. **Multiple Inheritance**:
   - A class can implement multiple interfaces, allowing multiple inheritance.

4. **Cannot Be Instantiated**:
   - Like abstract classes, interfaces cannot be instantiated directly.

5. **Implicit Modifiers**:
   - All methods in an interface are implicitly `public` and `abstract` (before Java 8).
   - All fields are implicitly `public`, `static`, and `final`.

---

### **Syntax**

```java
interface InterfaceName {
    // Abstract method (implicitly public and abstract)
    void methodName();

    // Default method
    default void defaultMethod() {
        System.out.println("This is a default method.");
    }

    // Static method
    static void staticMethod() {
        System.out.println("This is a static method.");
    }
}
```

---

### **Examples**

#### **1. Simple Interface Example**

```java
interface Animal {
    void sound(); // Abstract method
    void eat();   // Abstract method
}

class Dog implements Animal {
    @Override
    public void sound() {
        System.out.println("Dog barks.");
    }

    @Override
    public void eat() {
        System.out.println("Dog eats bones.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myDog = new Dog();
        myDog.sound();
        myDog.eat();
    }
}
```

#### **Output**
```
Dog barks.
Dog eats bones.
```

#### **Explanation**
- The `Dog` class implements the `Animal` interface, providing implementations for the `sound()` and `eat()` methods.

---

#### **2. Interface with Default and Static Methods**

```java
interface Vehicle {
    void start(); // Abstract method

    // Default method
    default void stop() {
        System.out.println("Vehicle is stopping.");
    }

    // Static method
    static void maintenance() {
        System.out.println("Vehicle maintenance required.");
    }
}

class Car implements Vehicle {
    @Override
    public void start() {
        System.out.println("Car is starting.");
    }
}

public class Main {
    public static void main(String[] args) {
        Vehicle car = new Car();
        car.start(); // Calls overridden method
        car.stop();  // Calls default method

        Vehicle.maintenance(); // Calls static method directly using the interface
    }
}
```

#### **Output**
```
Car is starting.
Vehicle is stopping.
Vehicle maintenance required.
```

#### **Explanation**
- The `stop()` method is a default method in the interface, so the `Car` class can use it without overriding.
- The `maintenance()` method is a static method and can be called using the interface name.

---

#### **3. Multiple Inheritance with Interfaces**

```java
interface Printer {
    void print();
}

interface Scanner {
    void scan();
}

class MultiFunctionPrinter implements Printer, Scanner {
    @Override
    public void print() {
        System.out.println("Printing document.");
    }

    @Override
    public void scan() {
        System.out.println("Scanning document.");
    }
}

public class Main {
    public static void main(String[] args) {
        MultiFunctionPrinter mfp = new MultiFunctionPrinter();
        mfp.print();
        mfp.scan();
    }
}
```

#### **Output**
```
Printing document.
Scanning document.
```

#### **Explanation**
- The `MultiFunctionPrinter` class implements both the `Printer` and `Scanner` interfaces, demonstrating multiple inheritance.

---

### **Differences Between Abstract Classes and Interfaces**

| **Aspect**                  | **Abstract Class**                                   | **Interface**                                     |
|-----------------------------|-----------------------------------------------------|-------------------------------------------------|
| **Purpose**                  | Provides a base class with shared fields and methods. | Defines a contract for classes to implement.    |
| **Inheritance**              | Supports single inheritance.                        | Supports multiple inheritance.                  |
| **Methods**                  | Can contain both abstract and concrete methods.      | Before Java 8: Only abstract methods.<br>Java 8+: Can include default and static methods. |
| **Fields**                   | Can have instance variables, constants, and static fields. | Can only have constants (`public static final`). |
| **Constructors**             | Can have constructors.                              | Cannot have constructors.                       |
| **Performance**              | Slightly faster as it uses direct inheritance.       | Slower due to additional indirection.           |

---

### **Key Features of Interfaces in Java Versions**

1. **Before Java 8**:
   - Interfaces could only contain **abstract methods** and **constants**.

2. **Java 8**:
   - Introduced **default methods** and **static methods** in interfaces.

3. **Java 9**:
   - Added **private methods** in interfaces, allowing code reuse within the interface.

---

### **Advantages of Interfaces**

1. **Achieves Abstraction**:
   - Interfaces define a blueprint for classes, promoting abstraction.

2. **Supports Multiple Inheritance**:
   - A class can implement multiple interfaces, combining functionality from different sources.

3. **Ensures Consistency**:
   - Enforces that implementing classes follow the defined contract.

4. **Promotes Loose Coupling**:
   - Interfaces provide a way to decouple code by defining behaviors independently of their implementations.

---

### **Disadvantages of Interfaces**

1. **No State**:
   - Interfaces cannot hold state (e.g., instance variables) as they cannot have instance fields.

2. **Complexity**:
   - Implementing multiple interfaces can make code complex and harder to maintain.

3. **Limited to Behavior**:
   - Interfaces cannot provide implementation for shared fields or non-behavioral functionalities.

---

### **Best Practices**

1. **Use Descriptive Names**:
   - Interface names should clearly indicate their purpose (e.g., `Runnable`, `Serializable`).

2. **Favor Interfaces Over Abstract Classes**:
   - Use interfaces to define contracts and ensure flexibility.

3. **Keep Methods Minimal**:
   - Avoid adding too many methods to a single interface. Use multiple small interfaces if needed.

4. **Leverage Default Methods**:
   - Use default methods for backward compatibility or to provide common functionality.

---

### **Conclusion**

An **interface** in Java is a key element for achieving **abstraction** and **multiple inheritance**, making it a cornerstone of Java's object-oriented programming model. By defining contracts that classes must adhere to, interfaces promote consistency and flexibility while decoupling the implementation from the definition. With the enhancements introduced in Java 8 and later, interfaces have become more versatile, combining the simplicity of abstraction with the power of default and static methods.

## What is the Difference Between Abstract Class and Interface
### **Difference Between Abstract Class and Interface in Java**

**Abstract classes** and **interfaces** are foundational constructs in Java used to implement **abstraction**. While both allow defining methods without implementation, they serve different purposes and have distinct features. Understanding their differences helps in deciding when to use one over the other.

---

### **Definition**

- **Abstract Class**: A class that can have both abstract methods (without implementation) and concrete methods (with implementation). It serves as a base class for subclasses.
- **Interface**: A blueprint for a class that specifies a contract of methods that implementing classes must provide. It allows defining behavior without specifying implementation.

---

### **Key Differences**

| **Aspect**                  | **Abstract Class**                                   | **Interface**                                     |
|-----------------------------|-----------------------------------------------------|-------------------------------------------------|
| **Purpose**                  | Provides a base class for shared behavior and fields. | Defines a contract for behavior across unrelated classes. |
| **Inheritance**              | Supports **single inheritance** (a class can extend only one abstract class). | Supports **multiple inheritance** (a class can implement multiple interfaces). |
| **Methods**                  | Can contain both **abstract** and **concrete** methods. | Before Java 8: Only abstract methods.<br>Java 8+: Can include **default** and **static** methods. |
| **Fields**                   | Can have **instance variables**, **constants**, and **static fields**. | Can only have **constants** (implicitly `public static final`). |
| **Constructors**             | Can have constructors for initializing fields.      | Cannot have constructors.                       |
| **Access Modifiers**         | Methods can have any access modifier (`public`, `protected`, `private`). | Methods are **public** by default (abstract methods). |
| **Type of Relationship**     | Defines a **"is-a" relationship** (inheritance).    | Defines a **"can-do" relationship** (behavioral contract). |
| **Performance**              | Slightly faster as it uses direct inheritance.       | Slightly slower due to method resolution at runtime for multiple interfaces. |

---

### **Examples**

#### **Abstract Class Example**

```java
abstract class Animal {
    String name;

    // Constructor
    Animal(String name) {
        this.name = name;
    }

    // Abstract method (must be implemented by subclasses)
    abstract void sound();

    // Concrete method (common to all subclasses)
    void sleep() {
        System.out.println(name + " is sleeping.");
    }
}

class Dog extends Animal {
    Dog(String name) {
        super(name);
    }

    @Override
    void sound() {
        System.out.println(name + " barks: Woof Woof!");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog("Buddy");
        dog.sound();
        dog.sleep();
    }
}
```

#### **Output**
```
Buddy barks: Woof Woof!
Buddy is sleeping.
```

#### **Interface Example**

```java
interface Animal {
    void sound(); // Abstract method
    void eat();   // Abstract method
}

class Dog implements Animal {
    @Override
    public void sound() {
        System.out.println("Dog barks: Woof Woof!");
    }

    @Override
    public void eat() {
        System.out.println("Dog eats bones.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog();
        dog.sound();
        dog.eat();
    }
}
```

#### **Output**
```
Dog barks: Woof Woof!
Dog eats bones.
```

---

### **When to Use Abstract Class vs Interface**

#### **Use an Abstract Class When**:
1. **Shared State and Behavior**:
   - When multiple classes share common fields or methods (e.g., `name`, `age`).
2. **Partial Implementation**:
   - When you want to provide some shared implementation while leaving specific details to subclasses.
3. **Constructor**:
   - When you need to initialize fields through a constructor.

#### **Use an Interface When**:
1. **Behavioral Contract**:
   - When unrelated classes need to implement the same set of behaviors (e.g., `Flyable`, `Runnable`).
2. **Multiple Inheritance**:
   - When a class needs to inherit from multiple sources (e.g., `Printer` and `Scanner`).
3. **Backward Compatibility**:
   - When working with Java 8+ features like `default` and `static` methods.

---

### **Abstract Class vs Interface: Feature Comparison**

#### **1. Inheritance**

- **Abstract Class**:
  - A class can only **extend one abstract class** (single inheritance).
  - Suitable for creating a class hierarchy.

- **Interface**:
  - A class can **implement multiple interfaces**, enabling multiple inheritance.

```java
interface Printer {
    void print();
}

interface Scanner {
    void scan();
}

class MultiFunctionDevice implements Printer, Scanner {
    @Override
    public void print() {
        System.out.println("Printing document.");
    }

    @Override
    public void scan() {
        System.out.println("Scanning document.");
    }
}
```

---

#### **2. Methods**

- **Abstract Class**:
  - Can have both **abstract** and **concrete** methods.

- **Interface**:
  - Before Java 8: Only **abstract** methods.
  - Java 8+: Can have **default methods** (with implementation) and **static methods**.

```java
interface Vehicle {
    void start(); // Abstract method

    default void stop() { // Default method
        System.out.println("Vehicle is stopping.");
    }

    static void maintenance() { // Static method
        System.out.println("Vehicle maintenance required.");
    }
}
```

---

#### **3. Fields**

- **Abstract Class**:
  - Can have **instance variables**, constants, and static fields.

- **Interface**:
  - Can only have **constants** (`public static final`).

```java
interface Constants {
    int MAX_SPEED = 120; // Implicitly public static final
}

class Car implements Constants {
    void displayMaxSpeed() {
        System.out.println("Max Speed: " + MAX_SPEED);
    }
}
```

---

#### **4. Constructor**

- **Abstract Class**:
  - Can have constructors to initialize fields.

```java
abstract class Animal {
    String name;

    Animal(String name) {
        this.name = name;
    }
}
```

- **Interface**:
  - Cannot have constructors because interfaces cannot hold state.

---

#### **5. Real-World Analogy**

- **Abstract Class**:
  - Think of it as a **template** for creating related objects with shared attributes (e.g., a `Vehicle` template for `Car` and `Bike`).

- **Interface**:
  - Think of it as a **contract** for classes to adhere to specific behaviors (e.g., `Flyable` for both `Bird` and `Airplane`).

---

### **Advantages and Disadvantages**

| **Aspect**               | **Abstract Class**                      | **Interface**                          |
|--------------------------|-----------------------------------------|----------------------------------------|
| **Advantages**            | Allows shared code implementation.     | Enables multiple inheritance.          |
|                          | Provides constructors for initialization. | Defines pure abstraction.              |
| **Disadvantages**         | Single inheritance limits flexibility. | Cannot hold state or instance variables. |

---

### **Conclusion**

Both **abstract classes** and **interfaces** are powerful tools for abstraction in Java, but their use depends on the requirements of the application:

1. Use **abstract classes** when creating a class hierarchy with shared attributes and partial implementation.
2. Use **interfaces** when defining behavioral contracts for unrelated classes or when multiple inheritance is required.

By understanding their differences and applications, you can design more modular, maintainable, and extensible Java programs.

## What is a Nested Class in Java
### **What is a Nested Class in Java?**

In Java, a **nested class** is a class defined inside another class. Nested classes allow you to logically group classes that are only used in one place, improving code organization and readability. A nested class can access the members (both static and non-static) of its enclosing class, even private ones.

---

### **Types of Nested Classes**

Java supports two main types of nested classes:
1. **Non-static Nested Classes (Inner Classes)**:
   - Associated with an instance of the enclosing class.
   - Includes:
     - **Member Inner Class**
     - **Anonymous Inner Class**
     - **Local Inner Class**

2. **Static Nested Classes**:
   - Does not require an instance of the enclosing class to be accessed.

---

### **1. Non-Static Nested Classes (Inner Classes)**

#### **a. Member Inner Class**

A **member inner class** is a non-static class defined at the member level of the enclosing class. It has access to all members of the enclosing class, including private members.

#### **Example: Member Inner Class**
```java
class Outer {
    private String message = "Hello from Outer Class";

    // Member Inner Class
    class Inner {
        void display() {
            System.out.println(message); // Accessing private member of Outer class
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner(); // Creating an instance of Inner class
        inner.display();
    }
}
```

#### **Output**
```
Hello from Outer Class
```

#### **Explanation**
- The `Inner` class is defined inside the `Outer` class.
- An instance of `Inner` is created using an instance of `Outer`.

---

#### **b. Anonymous Inner Class**

An **anonymous inner class** is a class without a name. It is typically used to provide a one-time implementation of an interface or abstract class.

#### **Example: Anonymous Inner Class**
```java
interface Greeting {
    void sayHello();
}

public class Main {
    public static void main(String[] args) {
        // Anonymous inner class implementing Greeting interface
        Greeting greeting = new Greeting() {
            @Override
            public void sayHello() {
                System.out.println("Hello from Anonymous Inner Class");
            }
        };

        greeting.sayHello();
    }
}
```

#### **Output**
```
Hello from Anonymous Inner Class
```

#### **Explanation**
- The `Greeting` interface is implemented using an anonymous inner class.
- It provides a one-time implementation for the `sayHello` method.

---

#### **c. Local Inner Class**

A **local inner class** is defined inside a method or a block. Its scope is limited to that method or block.

#### **Example: Local Inner Class**
```java
class Outer {
    void display() {
        // Local Inner Class
        class Local {
            void print() {
                System.out.println("Hello from Local Inner Class");
            }
        }

        Local local = new Local();
        local.print();
    }
}

public class Main {
    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.display();
    }
}
```

#### **Output**
```
Hello from Local Inner Class
```

#### **Explanation**
- The `Local` class is defined inside the `display` method.
- Its scope is limited to the `display` method.

---

### **2. Static Nested Classes**

A **static nested class** is a static class defined inside another class. Unlike inner classes, it does not require an instance of the enclosing class to be instantiated. However, it cannot directly access non-static members of the enclosing class.

#### **Example: Static Nested Class**
```java
class Outer {
    private static String message = "Hello from Static Nested Class";

    // Static Nested Class
    static class Nested {
        void display() {
            System.out.println(message); // Accessing static member of Outer class
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Outer.Nested nested = new Outer.Nested(); // Creating an instance of Static Nested Class
        nested.display();
    }
}
```

#### **Output**
```
Hello from Static Nested Class
```

#### **Explanation**
- The `Nested` class is static and can be accessed without creating an instance of `Outer`.
- It can only access static members of the enclosing class.

---

### **Advantages of Nested Classes**

1. **Logical Grouping**:
   - Groups classes that are logically related, making the code more readable and organized.

2. **Encapsulation**:
   - Helps to hide implementation details from external classes.

3. **Access to Members**:
   - Inner classes have access to all members (including private) of the enclosing class.

4. **Compact Code**:
   - Especially useful for anonymous and local inner classes to provide one-time implementations.

---

### **Disadvantages of Nested Classes**

1. **Complexity**:
   - Nested classes can make code more complex and harder to understand.

2. **Limited Scope**:
   - Local and anonymous inner classes have a limited scope, which can sometimes restrict their usability.

3. **Performance Overhead**:
   - Inner classes may have a slight performance overhead due to their association with the enclosing class.

---

### **Comparison of Inner Classes and Static Nested Classes**

| **Aspect**             | **Inner Class**                                    | **Static Nested Class**                             |
|------------------------|---------------------------------------------------|---------------------------------------------------|
| **Requires Outer Class Instance** | Yes                                           | No                                                |
| **Access to Outer Class Members** | Can access both static and non-static members. | Can only access static members.                  |
| **Use Case**            | Used when the nested class logically belongs to the enclosing class's instance. | Used when the nested class is independent of the enclosing class's instance. |

---

### **Best Practices**

1. **Use Inner Classes**:
   - When the nested class needs access to the enclosing class's instance variables or methods.
   - Example: Event handling in GUIs.

2. **Use Static Nested Classes**:
   - When the nested class is independent of the enclosing class's instance.
   - Example: Utility classes or grouping constants.

3. **Avoid Overuse**:
   - Too many nested classes can make code harder to read and maintain.

---

### **Conclusion**

A **nested class** in Java allows better organization, encapsulation, and logical grouping of classes. Understanding the types—**member inner class**, **anonymous inner class**, **local inner class**, and **static nested class**—helps you use them effectively in different scenarios. While inner classes are tied to an instance of the enclosing class, static nested classes are independent, providing flexibility in design. By leveraging nested classes appropriately, you can write more modular and maintainable Java code.

## What is an Anonymous Inner Class in Java
### **What is an Anonymous Inner Class in Java?**

An **anonymous inner class** in Java is a type of **inner class** (nested class) that **does not have a name** and is used to provide a **one-time implementation** of an interface, abstract class, or concrete class. These classes are often used to simplify code when a class implementation is needed only once.

Anonymous inner classes are commonly used in **event handling**, **multithreading**, and **callback mechanisms**.

---

### **Key Characteristics of Anonymous Inner Classes**

1. **No Name**:
   - Anonymous inner classes are nameless and are declared and instantiated in a single statement.

2. **Implements or Extends**:
   - They can implement an **interface** or extend an **abstract class** or **concrete class**.

3. **Single Use**:
   - Typically used when the implementation is needed only once in the program.

4. **Scope**:
   - Limited to the block where it is defined.

5. **Compact Code**:
   - Provides a concise way to implement an interface or extend a class without explicitly creating a new named class.

---

### **Syntax**

```java
new SuperClassOrInterface() {
    // Override methods or define additional behavior
};
```

---

### **Types of Anonymous Inner Classes**

1. **Anonymous Inner Class Implementing an Interface**
2. **Anonymous Inner Class Extending a Class**
3. **Anonymous Inner Class as an Argument**

---

### **1. Anonymous Inner Class Implementing an Interface**

An anonymous inner class can provide a one-time implementation of an interface.

#### **Example**
```java
interface Greeting {
    void sayHello();
}

public class Main {
    public static void main(String[] args) {
        // Anonymous inner class implementing Greeting interface
        Greeting greeting = new Greeting() {
            @Override
            public void sayHello() {
                System.out.println("Hello from Anonymous Inner Class!");
            }
        };

        greeting.sayHello();
    }
}
```

#### **Output**
```
Hello from Anonymous Inner Class!
```

#### **Explanation**
- The `Greeting` interface is implemented by an anonymous inner class, providing the `sayHello` method implementation.
- The implementation is done without creating a separate named class.

---

### **2. Anonymous Inner Class Extending a Class**

An anonymous inner class can extend a concrete or abstract class and override its methods.

#### **Example**
```java
abstract class Animal {
    abstract void sound();
}

public class Main {
    public static void main(String[] args) {
        // Anonymous inner class extending Animal class
        Animal dog = new Animal() {
            @Override
            void sound() {
                System.out.println("Dog barks: Woof Woof!");
            }
        };

        dog.sound();
    }
}
```

#### **Output**
```
Dog barks: Woof Woof!
```

#### **Explanation**
- The `Animal` abstract class is extended by an anonymous inner class.
- The `sound` method is overridden to provide the desired behavior.

---

### **3. Anonymous Inner Class as an Argument**

Anonymous inner classes are often used as arguments to methods that accept an interface or abstract class as a parameter.

#### **Example**
```java
interface Calculator {
    int calculate(int a, int b);
}

public class Main {
    public static void main(String[] args) {
        // Anonymous inner class passed as an argument
        int result = performOperation(5, 3, new Calculator() {
            @Override
            public int calculate(int a, int b) {
                return a + b; // Adding the two numbers
            }
        });

        System.out.println("Result: " + result);
    }

    static int performOperation(int a, int b, Calculator calculator) {
        return calculator.calculate(a, b);
    }
}
```

#### **Output**
```
Result: 8
```

#### **Explanation**
- The `Calculator` interface is implemented using an anonymous inner class.
- The implementation is directly passed as an argument to the `performOperation` method.

---

### **Advantages of Anonymous Inner Classes**

1. **Compact and Readable Code**:
   - Eliminates the need for creating a separate named class for a single-use implementation.

2. **Simplifies Event Handling**:
   - Frequently used for handling events in graphical user interfaces (GUIs), e.g., `ActionListener`.

3. **Encapsulation**:
   - The implementation is encapsulated within a block, making the code more modular.

---

### **Disadvantages of Anonymous Inner Classes**

1. **Limited Reusability**:
   - Since the class is unnamed, it cannot be reused elsewhere in the program.

2. **Difficult Debugging**:
   - Debugging can be harder as the class has no name and is defined inline.

3. **Code Complexity**:
   - If the implementation is large, anonymous inner classes can make the code harder to read and maintain.

---

### **Common Use Case: Event Handling**

Anonymous inner classes are widely used in Java GUI applications, such as **Swing** or **JavaFX**, for handling events like button clicks.

#### **Example: Event Handling**
```java
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Anonymous Inner Class Example");
        JButton button = new JButton("Click Me");

        // Adding action listener using an anonymous inner class
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Button clicked!");
            }
        });

        frame.add(button);
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
```

#### **Output**
```
Button clicked!
```

#### **Explanation**
- The `ActionListener` interface is implemented using an anonymous inner class.
- The implementation for `actionPerformed` is directly provided within the `addActionListener` method.

---

### **Anonymous Inner Class vs Lambda Expressions**

Since **Java 8**, **lambda expressions** can be used as a shorter alternative to anonymous inner classes for implementing functional interfaces.

#### **Comparison**

| **Aspect**                     | **Anonymous Inner Class**                           | **Lambda Expression**                            |
|--------------------------------|----------------------------------------------------|-------------------------------------------------|
| **Syntax**                     | Verbose (requires overriding methods).             | Concise and readable (no method name needed).   |
| **Applicability**              | Can be used for interfaces and classes.            | Can only be used for functional interfaces (interfaces with a single abstract method). |
| **Reusability**                | Limited, as the class is unnamed.                  | Similar limitations, specific to functional interfaces. |

#### **Example**

Using an anonymous inner class:
```java
Runnable runnable = new Runnable() {
    @Override
    public void run() {
        System.out.println("Running with Anonymous Inner Class!");
    }
};
```

Using a lambda expression:
```java
Runnable runnable = () -> System.out.println("Running with Lambda Expression!");
```

---

### **Key Points to Remember**

1. **Purpose**:
   - Anonymous inner classes are used for quick and temporary implementation of interfaces or abstract classes.

2. **Limitations**:
   - Cannot define constructors or static members.
   - Limited reusability due to being nameless.

3. **Use Cases**:
   - Event handling, multithreading, and callback mechanisms.

---

### **Conclusion**

Anonymous inner classes in Java provide a simple and concise way to create one-time implementations of interfaces or abstract classes. While they are highly useful for scenarios like event handling or functional programming, they should be used judiciously to maintain code clarity and simplicity. With the introduction of **lambda expressions** in Java 8, many use cases for anonymous inner classes have shifted to using lambdas, making the code even more concise. Understanding when and how to use anonymous inner classes is essential for writing efficient and maintainable Java applications.

## What is a Nested Interface in Java
### **What is a Nested Interface in Java?**

A **nested interface** in Java is an interface that is declared inside another class, interface, or even another nested interface. Nested interfaces allow you to logically group related functionality, particularly when the interface is closely associated with its enclosing type. These interfaces are used to define contracts that other classes must implement while remaining scoped to their enclosing type.

---

### **Key Characteristics of Nested Interfaces**

1. **Declared Inside a Class or Interface**:
   - A nested interface can be declared inside a class, interface, or another nested interface.

2. **Access Modifiers**:
   - When declared inside a class:
     - The nested interface can have any access modifier (`public`, `protected`, `private`, or default).
   - When declared inside an interface:
     - The nested interface is **public** and **static** by default.

3. **Association with Enclosing Class or Interface**:
   - Nested interfaces are often used when the interface is tightly coupled with the enclosing type.

4. **Implemented Separately**:
   - A nested interface can be implemented by any class or nested class, not just the enclosing class.

5. **Static by Default (Inside Interfaces)**:
   - A nested interface inside an interface is **static** by default and does not depend on an instance of the enclosing interface.

---

### **Syntax**

```java
class OuterClass {
    interface NestedInterface {
        void method();
    }
}

interface OuterInterface {
    interface NestedInterface {
        void method();
    }
}
```

---

### **Examples**

#### **1. Nested Interface Inside a Class**

```java
class Outer {
    // Nested Interface
    interface NestedInterface {
        void display();
    }
}

// A class implementing the nested interface
class Implementer implements Outer.NestedInterface {
    @Override
    public void display() {
        System.out.println("Implementing the Nested Interface inside a class.");
    }
}

public class Main {
    public static void main(String[] args) {
        Outer.NestedInterface obj = new Implementer(); // Referencing using Outer.NestedInterface
        obj.display();
    }
}
```

#### **Output**
```
Implementing the Nested Interface inside a class.
```

#### **Explanation**
- The `NestedInterface` is declared inside the `Outer` class.
- The `Implementer` class implements the `NestedInterface`.
- The interface is accessed using the enclosing class's name (`Outer.NestedInterface`).

---

#### **2. Nested Interface Inside Another Interface**

```java
interface ParentInterface {
    // Nested Interface
    interface NestedInterface {
        void sayHello();
    }
}

// A class implementing the nested interface
class Implementer implements ParentInterface.NestedInterface {
    @Override
    public void sayHello() {
        System.out.println("Hello from Nested Interface inside an interface.");
    }
}

public class Main {
    public static void main(String[] args) {
        ParentInterface.NestedInterface obj = new Implementer(); // Referencing using ParentInterface.NestedInterface
        obj.sayHello();
    }
}
```

#### **Output**
```
Hello from Nested Interface inside an interface.
```

#### **Explanation**
- The `NestedInterface` is defined inside the `ParentInterface`.
- It is static by default, so it can be implemented without requiring an instance of `ParentInterface`.

---

#### **3. Nested Interface with Different Access Modifiers**

```java
class Outer {
    // Private nested interface
    private interface NestedInterface {
        void display();
    }

    // Inner class implementing the private nested interface
    class Inner implements NestedInterface {
        @Override
        public void display() {
            System.out.println("Accessing private nested interface from within the Outer class.");
        }
    }

    void execute() {
        NestedInterface obj = new Inner();
        obj.display();
    }
}

public class Main {
    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.execute();
    }
}
```

#### **Output**
```
Accessing private nested interface from within the Outer class.
```

#### **Explanation**
- The `NestedInterface` is declared as `private` within the `Outer` class.
- Only classes within `Outer` can implement and use the `NestedInterface`.

---

### **Use Cases of Nested Interfaces**

1. **Defining Callbacks**:
   - Nested interfaces are commonly used as callback mechanisms for communication between classes.

   #### Example:
   ```java
   class Button {
       // Nested interface for a click listener
       interface ClickListener {
           void onClick();
       }

       private ClickListener listener;

       void setClickListener(ClickListener listener) {
           this.listener = listener;
       }

       void click() {
           if (listener != null) {
               listener.onClick();
           }
       }
   }

   public class Main {
       public static void main(String[] args) {
           Button button = new Button();

           // Implementing the nested interface
           button.setClickListener(new Button.ClickListener() {
               @Override
               public void onClick() {
                   System.out.println("Button clicked!");
               }
           });

           button.click();
       }
   }
   ```

   #### **Output**
   ```
   Button clicked!
   ```

   #### **Explanation**
   - The `ClickListener` interface is nested inside the `Button` class.
   - It defines the contract for responding to button clicks.

---

2. **Grouping Related Interfaces**:
   - Nested interfaces allow grouping multiple related interfaces within a single enclosing interface or class.

   #### Example:
   ```java
   interface Device {
       interface InputDevice {
           void connect();
       }

       interface OutputDevice {
           void display();
       }
   }

   class Keyboard implements Device.InputDevice {
       @Override
       public void connect() {
           System.out.println("Keyboard connected.");
       }
   }

   class Monitor implements Device.OutputDevice {
       @Override
       public void display() {
           System.out.println("Monitor displaying output.");
       }
   }

   public class Main {
       public static void main(String[] args) {
           Device.InputDevice keyboard = new Keyboard();
           Device.OutputDevice monitor = new Monitor();

           keyboard.connect();
           monitor.display();
       }
   }
   ```

   #### **Output**
   ```
   Keyboard connected.
   Monitor displaying output.
   ```

   #### **Explanation**
   - The `InputDevice` and `OutputDevice` interfaces are grouped inside the `Device` interface, providing a logical grouping of related functionality.

---

### **Advantages of Nested Interfaces**

1. **Logical Grouping**:
   - Groups related functionality, improving code organization and readability.

2. **Encapsulation**:
   - Can restrict access to the nested interface by making it private or protected.

3. **Scoping**:
   - Keeps the interface scoped to its logical enclosing type, reducing clutter in the global namespace.

4. **Modular Design**:
   - Makes code modular and easier to maintain by keeping related code together.

---

### **Disadvantages of Nested Interfaces**

1. **Complexity**:
   - Overuse of nested interfaces can make the code harder to read and maintain.

2. **Limited Reusability**:
   - Nested interfaces with restrictive access modifiers may limit their usability outside the enclosing class.

---

### **Comparison: Nested Interface vs Nested Class**

| **Aspect**              | **Nested Interface**                                    | **Nested Class**                                   |
|-------------------------|--------------------------------------------------------|--------------------------------------------------|
| **Purpose**              | Defines a contract for behavior.                       | Defines a type with fields and methods.          |
| **Access Modifiers**     | Can be public, protected, private, or default.         | Same as nested interface.                        |
| **Static**               | Always static when inside an interface.                | Can be static or non-static.                     |
| **Usage**                | Used for defining behavior contracts.                  | Used for encapsulating related logic or data.    |

---

### **Key Points to Remember**

1. Nested interfaces allow logical grouping of related contracts.
2. When declared inside a class, they can have any access modifier.
3. When declared inside an interface, they are **public** and **static** by default.
4. Nested interfaces are commonly used in event handling and callbacks.
5. Overuse of nested interfaces can lead to complexity, so they should be used judiciously.

---

### **Conclusion**

A **nested interface** in Java is a powerful tool for encapsulating related functionality and defining behavior-specific contracts within a class or interface. It improves code organization and modularity, especially when interfaces are tightly coupled with their enclosing types. While they provide flexibility and readability, it's important to use them appropriately to avoid unnecessary complexity. By understanding the use cases and limitations, nested interfaces can be a valuable asset in building maintainable and efficient Java applications.

## What is a Marker Interface in Java
### **What is a Marker Interface in Java?**

A **Marker Interface** in Java is an interface that contains **no methods** or **fields**. It is used to indicate or "mark" a class as having a specific property or behavior. Marker interfaces are a way to provide **metadata** to objects about their capabilities, which can then be processed by the Java runtime or custom application logic.

---

### **Characteristics of Marker Interfaces**

1. **No Methods**:
   - Marker interfaces do not define any methods or fields. They are empty interfaces.

2. **Acts as a Tag**:
   - It acts as a "tag" to inform the compiler or JVM that the class implementing the marker interface has a particular capability.

3. **Used by JVM or Frameworks**:
   - Marker interfaces are often processed by JVM or libraries to provide special behavior (e.g., `Serializable`, `Cloneable`).

4. **Relies on Instance Checking**:
   - The `instanceof` operator or reflection is typically used to check if a class implements a marker interface.

---

### **Syntax**

```java
interface MarkerInterface {
    // No methods or fields
}
```

---

### **Examples of Marker Interfaces**

#### **1. Built-in Marker Interfaces in Java**

- **`Serializable`**:
  - Marks a class as serializable so that its instances can be serialized and deserialized.
  - Example:
    ```java
    import java.io.Serializable;

    class User implements Serializable {
        private String name;
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{name='" + name + "', age=" + age + "}";
        }
    }
    ```

- **`Cloneable`**:
  - Marks a class as capable of being cloned using the `clone()` method.
  - Example:
    ```java
    class User implements Cloneable {
        private String name;

        public User(String name) {
            this.name = name;
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }
    ```

- **`Remote`**:
  - Marks an object as capable of being accessed remotely via RMI (Remote Method Invocation).

#### **2. Custom Marker Interface**

You can create your own marker interface to define specific behaviors or properties for a class.

Example: Custom Marker Interface
```java
interface Auditable {
    // No methods or fields
}

// Class marked as auditable
class Transaction implements Auditable {
    private String transactionId;

    public Transaction(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }
}

public class Main {
    public static void main(String[] args) {
        Transaction transaction = new Transaction("TXN123");

        // Check if the object implements the marker interface
        if (transaction instanceof Auditable) {
            System.out.println("Transaction is auditable.");
        } else {
            System.out.println("Transaction is not auditable.");
        }
    }
}
```

#### **Output**
```
Transaction is auditable.
```

#### **Explanation**
- The `Auditable` interface marks classes that should be treated as auditable.
- The `instanceof` operator checks if the `transaction` object implements the `Auditable` marker interface.

---

### **How Marker Interfaces Work**

Marker interfaces work by **signaling the compiler or runtime** about a particular characteristic or capability of a class. For example:

1. **At Compile-Time**:
   - A marker interface can help the compiler validate the class's behavior, though this is rare.

2. **At Runtime**:
   - Marker interfaces are typically used with runtime tools like reflection or specific Java API features to perform certain tasks based on whether a class implements the marker interface.

Example: Using Reflection with a Marker Interface
```java
import java.lang.reflect.Method;

interface Loggable {
    // Marker interface
}

class Service implements Loggable {
    void process() {
        System.out.println("Processing...");
    }
}

public class Main {
    public static void main(String[] args) {
        Service service = new Service();

        // Check if the service is marked as Loggable
        if (service instanceof Loggable) {
            System.out.println("Logging enabled for this service.");
        }
    }
}
```

#### **Output**
```
Logging enabled for this service.
```

---

### **Advantages of Marker Interfaces**

1. **Simplifies Type Checking**:
   - Marker interfaces provide a straightforward way to check an object's capabilities using `instanceof`.

2. **Built-In Support in Java**:
   - The Java API already uses marker interfaces like `Serializable` and `Cloneable`, making it a familiar pattern.

3. **Logical Grouping**:
   - Marker interfaces help in logically grouping classes with similar behaviors.

4. **Supports Legacy Code**:
   - Marker interfaces are well-suited for use in existing systems where adding annotations or other mechanisms might be challenging.

---

### **Disadvantages of Marker Interfaces**

1. **No Explicit Behavior**:
   - Marker interfaces do not provide any explicit behavior, leading to potential ambiguity.

2. **Limited to Interfaces**:
   - They do not support additional metadata or attributes like annotations do.

3. **Not Flexible**:
   - If additional attributes are needed, marker interfaces cannot provide them, unlike annotations.

4. **Deprecated by Annotations**:
   - Modern Java development often favors annotations over marker interfaces due to their flexibility and capability to hold metadata.

---

### **Marker Interface vs Annotations**

| **Aspect**              | **Marker Interface**                            | **Annotation**                                      |
|-------------------------|------------------------------------------------|---------------------------------------------------|
| **Definition**           | An empty interface used to mark a class.       | A special type of metadata added to the code.     |
| **Attributes**           | Cannot store additional attributes.            | Can store attributes as key-value pairs.          |
| **Flexibility**          | Limited to tagging a class.                    | Flexible; can be applied to classes, methods, fields, etc. |
| **Runtime Processing**   | Checked using `instanceof` or reflection.       | Processed using reflection (`Annotation` API).    |
| **Modern Usage**         | Considered a legacy mechanism.                 | Preferred for modern applications.                |

#### Example: Using Annotations Instead of a Marker Interface
```java
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface Auditable {
    // Annotation with no attributes
}

@Auditable
class Transaction {
    private String transactionId;

    public Transaction(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }
}

public class Main {
    public static void main(String[] args) {
        Transaction transaction = new Transaction("TXN123");

        // Check if the class is annotated with @Auditable
        if (transaction.getClass().isAnnotationPresent(Auditable.class)) {
            System.out.println("Transaction is auditable.");
        } else {
            System.out.println("Transaction is not auditable.");
        }
    }
}
```

#### **Output**
```
Transaction is auditable.
```

---

### **Use Cases of Marker Interfaces**

1. **Serialization**:
   - Classes implementing `Serializable` can be serialized.

2. **Cloning**:
   - Classes implementing `Cloneable` can be cloned using the `clone()` method.

3. **Security Checks**:
   - Marker interfaces can signal classes that require special security privileges.

4. **Custom Contracts**:
   - Developers can create custom marker interfaces to group classes logically and enforce behavior through type checking or runtime reflection.

---

### **Conclusion**

A **Marker Interface** in Java is a simple but effective mechanism for tagging classes with specific characteristics or behaviors. While they are still widely used in legacy systems and Java's core libraries (e.g., `Serializable`, `Cloneable`), modern Java development often favors **annotations** for their flexibility and ability to include additional metadata. Marker interfaces remain a valuable tool in certain use cases, particularly where simplicity and backward compatibility are essential. Understanding their role and limitations is crucial for effectively applying them in Java applications.

## What is a Functional Interface in Java
### **What is a Functional Interface in Java?**

A **functional interface** in Java is an interface that contains **exactly one abstract method**. It can also contain other methods such as **default methods**, **static methods**, and methods inherited from `java.lang.Object`, but it must have only **one unimplemented abstract method**.

Functional interfaces are primarily used in **lambda expressions** and **method references**, which were introduced in **Java 8** to enable functional programming capabilities in Java.

---

### **Key Characteristics of a Functional Interface**

1. **Single Abstract Method**:
   - A functional interface must have one and only one abstract method.
   - This method represents the single functionality of the interface.

2. **Can Have Default and Static Methods**:
   - Additional non-abstract methods can be included (e.g., `default` and `static` methods).

3. **Annotations**:
   - Functional interfaces are annotated with `@FunctionalInterface` (optional but recommended). This annotation is used to indicate that the interface is intended to be a functional interface. If it contains more than one abstract method, the compiler will throw an error.

4. **Supports Lambda Expressions**:
   - Functional interfaces are the foundation of lambda expressions, allowing you to provide implementations in a concise and readable manner.

5. **Inherits Methods from `java.lang.Object`**:
   - Methods like `equals()`, `hashCode()`, and `toString()` from `Object` do not count as abstract methods.

---

### **Syntax**

```java
@FunctionalInterface
interface FunctionalInterfaceName {
    void singleAbstractMethod();

    // Optional: default or static methods
    default void defaultMethod() {
        System.out.println("Default method in Functional Interface");
    }

    static void staticMethod() {
        System.out.println("Static method in Functional Interface");
    }
}
```

---

### **Examples**

#### **1. Simple Functional Interface**

```java
@FunctionalInterface
interface Greeting {
    void sayHello(String name);
}

public class Main {
    public static void main(String[] args) {
        // Using a lambda expression to implement the functional interface
        Greeting greeting = (name) -> System.out.println("Hello, " + name);
        greeting.sayHello("Alice"); // Output: Hello, Alice
    }
}
```

#### **Explanation**
- The `Greeting` interface has only one abstract method `sayHello(String name)`, making it a functional interface.
- A lambda expression `(name) -> System.out.println("Hello, " + name)` is used to provide the implementation for the `sayHello` method.

---

#### **2. Functional Interface with Default and Static Methods**

```java
@FunctionalInterface
interface Calculator {
    int calculate(int a, int b);

    // Default method
    default void show() {
        System.out.println("Default method in Calculator interface");
    }

    // Static method
    static void display() {
        System.out.println("Static method in Calculator interface");
    }
}

public class Main {
    public static void main(String[] args) {
        // Using a lambda expression to implement the functional interface
        Calculator addition = (a, b) -> a + b;
        System.out.println("Addition: " + addition.calculate(10, 5)); // Output: 15

        // Calling default and static methods
        addition.show();
        Calculator.display();
    }
}
```

#### **Explanation**
- The `Calculator` interface has a single abstract method `calculate(int a, int b)`, making it a functional interface.
- A lambda expression `(a, b) -> a + b` provides the implementation for the `calculate` method.
- The interface also contains a `default` method and a `static` method, which can be called as demonstrated.

---

### **Built-in Functional Interfaces in Java**

Java provides several built-in functional interfaces in the `java.util.function` package, such as:

#### **1. `Predicate<T>`**
- Represents a condition (boolean-valued function) on a single input.
- Abstract method: `boolean test(T t)`.

#### Example:
```java
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        Predicate<Integer> isEven = (number) -> number % 2 == 0;
        System.out.println(isEven.test(4)); // Output: true
        System.out.println(isEven.test(5)); // Output: false
    }
}
```

---

#### **2. `Function<T, R>`**
- Represents a function that takes one input and produces a result.
- Abstract method: `R apply(T t)`.

#### Example:
```java
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<String, Integer> stringLength = (str) -> str.length();
        System.out.println(stringLength.apply("Java")); // Output: 4
    }
}
```

---

#### **3. `Consumer<T>`**
- Represents an operation that takes one input but does not return a result.
- Abstract method: `void accept(T t)`.

#### Example:
```java
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        Consumer<String> printMessage = (message) -> System.out.println("Message: " + message);
        printMessage.accept("Hello, Functional Interface!"); // Output: Message: Hello, Functional Interface!
    }
}
```

---

#### **4. `Supplier<T>`**
- Represents a supplier of results (no input, produces a result).
- Abstract method: `T get()`.

#### Example:
```java
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        Supplier<String> supplier = () -> "Hello from Supplier";
        System.out.println(supplier.get()); // Output: Hello from Supplier
    }
}
```

---

### **Advantages of Functional Interfaces**

1. **Support for Lambda Expressions**:
   - Functional interfaces enable lambda expressions, which lead to more concise and readable code.

2. **Reusability**:
   - Built-in functional interfaces in `java.util.function` provide reusable interfaces for common functional programming needs.

3. **Improved Readability**:
   - Reduces boilerplate code when implementing single-method interfaces.

4. **Promotes Functional Programming**:
   - Functional interfaces, together with lambda expressions, enable functional programming in Java.

---

### **Functional Interface vs Abstract Class**

| **Aspect**              | **Functional Interface**                             | **Abstract Class**                           |
|-------------------------|----------------------------------------------------|---------------------------------------------|
| **Methods**              | Only one abstract method.                          | Can have multiple abstract and concrete methods. |
| **Inheritance**          | Can be implemented by any class.                   | Can be extended by only one class.          |
| **Default and Static Methods** | Can include `default` and `static` methods.          | Can include `static` and non-static methods. |
| **State**                | Cannot have instance variables.                    | Can have instance variables.                |
| **Usage**                | Used for functional programming and lambda expressions. | Used for partial implementation and inheritance. |

---

### **Common Use Cases**

1. **Lambda Expressions**:
   - Functional interfaces provide the foundation for lambda expressions in Java.

2. **Event Handling**:
   - Used in GUI applications for handling events such as button clicks.

3. **Streams and Functional Programming**:
   - Functional interfaces are heavily used in the Java Streams API.

4. **Custom Functional Interfaces**:
   - Create your own functional interfaces to model domain-specific behaviors.

---

### **Conclusion**

A **functional interface** in Java is a powerful mechanism that enables functional programming and the use of **lambda expressions**. By containing a single abstract method, functional interfaces represent a single functionality or behavior that can be implemented in a concise and readable way. Java 8's built-in functional interfaces in the `java.util.function` package provide reusable tools for common use cases, making functional programming in Java more accessible. Understanding functional interfaces is essential for modern Java programming, especially when working with streams, collections, and asynchronous processing.

## What is a Wrapper Class
### **What is a Wrapper Class in Java?**

A **Wrapper Class** in Java is a class that allows primitive data types (like `int`, `char`, `float`, etc.) to be treated as objects. Wrapper classes are part of the **java.lang** package, and each primitive data type has a corresponding wrapper class. 

Wrapper classes are used to **wrap** primitive values into objects, enabling them to interact with classes that require objects (e.g., in collections like `ArrayList`). They also provide utility methods for converting between primitives and objects or performing operations like parsing and value conversion.

---

### **Primitive Types and Their Corresponding Wrapper Classes**

| **Primitive Type** | **Wrapper Class** |
|---------------------|-------------------|
| `byte`              | `Byte`           |
| `short`             | `Short`          |
| `int`               | `Integer`        |
| `long`              | `Long`           |
| `float`             | `Float`          |
| `double`            | `Double`         |
| `char`              | `Character`      |
| `boolean`           | `Boolean`        |

---

### **Why Do We Need Wrapper Classes?**

1. **Object-Oriented Programming**:
   - Java is an object-oriented programming language, and some frameworks or data structures (e.g., `ArrayList`) can only store objects. Wrapper classes allow primitive data types to be treated as objects.

2. **Utility Methods**:
   - Wrapper classes provide useful methods for converting, parsing, and comparing values.

3. **Type Conversion**:
   - Wrapper classes can convert between strings and primitives, or between different numeric types.

4. **Collections**:
   - Collections in Java (like `List` and `Set`) only work with objects, not primitives. Wrapper classes allow storing primitive values in these data structures.

---

### **Boxing and Unboxing**

#### **1. Boxing**
- **Boxing** refers to converting a **primitive type** into its corresponding **wrapper class object**.
  
#### **2. Unboxing**
- **Unboxing** refers to converting a **wrapper class object** back into its corresponding **primitive type**.

#### **Example: Boxing and Unboxing**
```java
public class Main {
    public static void main(String[] args) {
        // Boxing: Converting primitive to wrapper object
        int num = 10;
        Integer boxedNum = Integer.valueOf(num); // Explicit boxing
        Integer autoBoxedNum = num; // Auto-boxing

        // Unboxing: Converting wrapper object to primitive
        int unboxedNum = boxedNum.intValue(); // Explicit unboxing
        int autoUnboxedNum = autoBoxedNum; // Auto-unboxing

        System.out.println("Boxed: " + boxedNum);
        System.out.println("Auto-boxed: " + autoBoxedNum);
        System.out.println("Unboxed: " + unboxedNum);
        System.out.println("Auto-unboxed: " + autoUnboxedNum);
    }
}
```

#### **Output**
```
Boxed: 10
Auto-boxed: 10
Unboxed: 10
Auto-unboxed: 10
```

---

### **Features of Wrapper Classes**

1. **Immutability**:
   - Wrapper class objects are immutable, meaning their values cannot be changed once they are created.

2. **Utility Methods**:
   - Wrapper classes provide methods like `parseXxx()`, `valueOf()`, and others for converting between types.

3. **Caching**:
   - Some wrapper classes (like `Integer`) cache values for commonly used numbers (e.g., from -128 to 127). This improves performance for frequently used values.

---

### **Commonly Used Methods in Wrapper Classes**

| **Method**               | **Description**                                                                                       | **Example**                                                       |
|--------------------------|-------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|
| `valueOf(String s)`      | Converts a `String` to the corresponding wrapper object.                                              | `Integer.valueOf("123")` → `123`                                 |
| `parseXxx(String s)`     | Converts a `String` to the corresponding primitive value.                                             | `Integer.parseInt("123")` → `123`                                |
| `xxxValue()`             | Converts the wrapper object to the corresponding primitive type.                                      | `Integer obj = 10; obj.intValue()` → `10`                        |
| `toString()`             | Converts the value of the wrapper object to a `String`.                                              | `Integer obj = 123; obj.toString()` → `"123"`                    |
| `compareTo()`            | Compares two wrapper objects.                                                                        | `Integer obj1 = 10; obj2 = 20; obj1.compareTo(obj2)` → `-1`      |
| `equals(Object obj)`     | Checks if two wrapper objects are equal.                                                             | `Integer obj1 = 10; obj2 = 10; obj1.equals(obj2)` → `true`       |
| `hashCode()`             | Returns the hash code for the wrapper object.                                                        | `Integer obj = 123; obj.hashCode()`                              |

---

### **Examples**

#### **1. Using Wrapper Classes in Collections**

```java
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();

        // Auto-boxing: Adding primitive int values to the ArrayList
        numbers.add(10);
        numbers.add(20);

        // Auto-unboxing: Retrieving primitive int values from the ArrayList
        int num1 = numbers.get(0);
        int num2 = numbers.get(1);

        System.out.println("Numbers: " + numbers);
        System.out.println("First number: " + num1);
        System.out.println("Second number: " + num2);
    }
}
```

#### **Output**
```
Numbers: [10, 20]
First number: 10
Second number: 20
```

---

#### **2. Parsing Strings to Primitives**

```java
public class Main {
    public static void main(String[] args) {
        String strNum = "123";

        // Parsing to primitive int
        int num = Integer.parseInt(strNum);

        // Converting to Integer object
        Integer boxedNum = Integer.valueOf(strNum);

        System.out.println("Primitive int: " + num);
        System.out.println("Wrapper Integer: " + boxedNum);
    }
}
```

#### **Output**
```
Primitive int: 123
Wrapper Integer: 123
```

---

#### **3. Converting Between Primitives**

```java
public class Main {
    public static void main(String[] args) {
        int num = 100;

        // Converting int to double using wrapper classes
        Double doubleValue = Double.valueOf(num);

        // Converting double to int
        int intValue = doubleValue.intValue();

        System.out.println("Double value: " + doubleValue);
        System.out.println("Converted back to int: " + intValue);
    }
}
```

#### **Output**
```
Double value: 100.0
Converted back to int: 100
```

---

### **Advantages of Wrapper Classes**

1. **Interoperability**:
   - Enables interaction with APIs and data structures that require objects (e.g., collections, streams).

2. **Utility Methods**:
   - Provides methods for conversion, parsing, and value comparison.

3. **Object-Oriented Features**:
   - Enables treating primitive types as objects, benefiting from features like inheritance and polymorphism.

4. **Improved Code Readability**:
   - Simplifies code when working with collections or generic types.

---

### **Disadvantages of Wrapper Classes**

1. **Performance Overhead**:
   - Boxing and unboxing can introduce performance overhead compared to direct usage of primitives.

2. **Increased Memory Usage**:
   - Wrapper classes use more memory as they store additional metadata compared to primitives.

3. **Null Pointer Exceptions**:
   - When dealing with `null` wrapper objects, unboxing can throw `NullPointerException`.

---

### **Key Differences Between Primitives and Wrapper Classes**

| **Aspect**              | **Primitive Types**                        | **Wrapper Classes**                           |
|-------------------------|--------------------------------------------|-----------------------------------------------|
| **Type**                | Data types like `int`, `float`, etc.       | Classes like `Integer`, `Float`, etc.         |
| **Memory Usage**         | Less memory usage.                        | More memory usage due to object overhead.     |
| **Null Handling**        | Cannot be `null`.                         | Can be `null`.                                |
| **Collections Support**  | Not supported in collections.             | Fully supported in collections.              |
| **Utility Methods**      | Not available.                            | Provides methods like `parseXxx()` and `valueOf()`. |

---

### **Conclusion**

A **wrapper class** in Java bridges the gap between primitive types and objects, allowing primitives to be used in object-oriented contexts like collections and frameworks. With the introduction of **autoboxing** and **unboxing** in Java 5, working with wrapper classes became more intuitive and streamlined. While wrapper classes bring flexibility and utility, they come with performance overheads, so they should be used judiciously in performance-critical applications. Understanding wrapper classes and their usage is essential for mastering Java, especially when working with modern APIs and collections.

## What is Autoboxing and Unboxing in Java
### **What is Autoboxing and Unboxing in Java?**

**Autoboxing** and **Unboxing** in Java are processes introduced in **Java 5** that allow seamless conversion between **primitive types** (like `int`, `double`, etc.) and their corresponding **wrapper classes** (like `Integer`, `Double`, etc.). These features simplify code and eliminate the need for manual conversion between primitive types and objects.

- **Autoboxing**: The automatic conversion of a **primitive type** into its corresponding **wrapper class object**.
- **Unboxing**: The automatic conversion of a **wrapper class object** back into its corresponding **primitive type**.

---

### **Why Autoboxing and Unboxing Were Introduced**

Before Java 5, if you wanted to store a primitive type in a collection (like `ArrayList` or `HashMap`), you had to manually convert it to its wrapper class because collections in Java only work with objects, not primitive types.

For example:
```java
// Before Java 5
ArrayList list = new ArrayList();
list.add(Integer.valueOf(10)); // Manually boxing the int value
int num = ((Integer) list.get(0)).intValue(); // Manually unboxing
```

With autoboxing and unboxing, the code becomes much simpler:
```java
// Java 5 and later
ArrayList<Integer> list = new ArrayList<>();
list.add(10); // Autoboxing
int num = list.get(0); // Unboxing
```

---

### **Autoboxing**

#### **Definition**
Autoboxing automatically converts a primitive type into its corresponding wrapper class object.

#### **Syntax**
```java
Integer obj = 10; // Autoboxing of int to Integer
```

#### **Example**
```java
public class Main {
    public static void main(String[] args) {
        int num = 10;

        // Autoboxing: Converting int to Integer
        Integer obj = num;

        System.out.println("Primitive int: " + num);
        System.out.println("Wrapper Integer: " + obj);
    }
}
```

#### **Output**
```
Primitive int: 10
Wrapper Integer: 10
```

#### **Explanation**
- The primitive `int` is automatically converted to the `Integer` object by the Java compiler.

---

### **Unboxing**

#### **Definition**
Unboxing automatically converts a wrapper class object back into its corresponding primitive type.

#### **Syntax**
```java
Integer obj = 10; // Autoboxing
int num = obj;    // Unboxing
```

#### **Example**
```java
public class Main {
    public static void main(String[] args) {
        Integer obj = Integer.valueOf(20);

        // Unboxing: Converting Integer to int
        int num = obj;

        System.out.println("Wrapper Integer: " + obj);
        System.out.println("Primitive int: " + num);
    }
}
```

#### **Output**
```
Wrapper Integer: 20
Primitive int: 20
```

#### **Explanation**
- The `Integer` object is automatically converted back into a primitive `int` by the Java compiler.

---

### **Autoboxing and Unboxing in Collections**

One of the most common use cases for autoboxing and unboxing is when working with collections. Collections like `ArrayList`, `HashMap`, etc., store objects, not primitive types. Autoboxing and unboxing make it easy to work with collections without manually converting between primitives and wrapper objects.

#### **Example: Using Autoboxing and Unboxing in Collections**
```java
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        // Autoboxing: Adding primitive int to ArrayList
        list.add(5);
        list.add(10);

        // Unboxing: Retrieving primitive int from ArrayList
        int num1 = list.get(0);
        int num2 = list.get(1);

        System.out.println("Numbers: " + num1 + ", " + num2);
    }
}
```

#### **Output**
```
Numbers: 5, 10
```

#### **Explanation**
- `5` and `10` are automatically converted to `Integer` objects when added to the `ArrayList` (autoboxing).
- The `Integer` objects are automatically converted back to `int` when retrieved (unboxing).

---

### **Behind the Scenes**

Autoboxing and unboxing are handled by the Java compiler. The compiler automatically inserts the required code for conversions. For example:

#### **Autoboxing**
```java
Integer obj = 10; // Compiler converts it to:
Integer obj = Integer.valueOf(10);
```

#### **Unboxing**
```java
int num = obj; // Compiler converts it to:
int num = obj.intValue();
```

---

### **Common Use Cases**

#### **1. Arithmetic Operations with Wrapper Classes**
Even though wrapper classes are objects, arithmetic operations can still be performed because of autoboxing and unboxing.

#### Example:
```java
public class Main {
    public static void main(String[] args) {
        Integer a = 5; // Autoboxing
        Integer b = 10; // Autoboxing

        int sum = a + b; // Unboxing both a and b, then adding
        System.out.println("Sum: " + sum);
    }
}
```

#### **Output**
```
Sum: 15
```

---

#### **2. Comparison Between Primitives and Wrapper Objects**
Autoboxing allows comparing primitives with wrapper objects.

#### Example:
```java
public class Main {
    public static void main(String[] args) {
        Integer obj = 10; // Autoboxing
        int num = 10;

        if (obj == num) { // Unboxing of obj, then comparison
            System.out.println("Equal");
        } else {
            System.out.println("Not Equal");
        }
    }
}
```

#### **Output**
```
Equal
```

---

### **Advantages of Autoboxing and Unboxing**

1. **Simplifies Code**:
   - Reduces the need for manual conversion between primitives and wrapper classes.

2. **Improves Readability**:
   - Code becomes cleaner and easier to read.

3. **Works Seamlessly with Collections**:
   - Makes it easy to store and retrieve primitive types in collections.

4. **Supports Functional Programming**:
   - Simplifies using primitives in streams and lambda expressions introduced in Java 8.

---

### **Disadvantages of Autoboxing and Unboxing**

1. **Performance Overhead**:
   - Autoboxing and unboxing involve creating objects and accessing their methods, which adds a performance cost.

2. **NullPointerException**:
   - Unboxing a `null` wrapper object throws a `NullPointerException`.

   #### Example:
   ```java
   public class Main {
       public static void main(String[] args) {
           Integer obj = null;

           // Unboxing null object
           int num = obj; // Throws NullPointerException
       }
   }
   ```

3. **Increased Memory Usage**:
   - Wrapper objects consume more memory than primitive types.

---

### **Best Practices**

1. **Avoid Overusing Autoboxing and Unboxing**:
   - Be cautious in performance-critical code where frequent conversions may impact efficiency.

2. **Check for Null Values**:
   - Always ensure that wrapper objects are not `null` before unboxing.

3. **Use Primitives Where Possible**:
   - Use primitives when you don't need the additional functionality provided by wrapper classes.

---

### **Comparison: Primitive Types vs Wrapper Classes**

| **Aspect**             | **Primitive Types**         | **Wrapper Classes**           |
|------------------------|-----------------------------|--------------------------------|
| **Type**               | Data type (e.g., `int`).    | Object (e.g., `Integer`).     |
| **Memory Usage**        | Uses less memory.           | Uses more memory.             |
| **Performance**        | Faster.                    | Slower due to object overhead.|
| **Nullable**           | Cannot be `null`.          | Can be `null`.                |
| **Collections**        | Not supported.             | Fully supported.              |

---

### **Conclusion**

Autoboxing and unboxing are essential features in Java that bridge the gap between primitive types and objects, making the language more flexible and developer-friendly. They eliminate the need for manual conversions and simplify code when working with collections, arithmetic operations, or functional programming constructs. However, it’s important to use them judiciously to avoid performance and memory overhead, especially in high-performance applications. Understanding how autoboxing and unboxing work can help you write more efficient and robust Java programs.

## What are Access Modifiers in Java
### **What are Access Modifiers in Java?**

Access modifiers in Java are **keywords** that define the **scope** or **visibility** of classes, methods, variables, and constructors. They control how other classes and code can access members of a class. Java provides four levels of access control, allowing developers to protect and encapsulate data while exposing only necessary parts of the code.

---

### **Types of Access Modifiers in Java**

There are **four types** of access modifiers in Java:

1. **`public`**: Provides the **widest scope**. The member is accessible from **anywhere**.
2. **`protected`**: Accessible **within the same package** and by **subclasses** in other packages.
3. **`default`** (no keyword): Accessible **only within the same package**.
4. **`private`**: Provides the **most restricted scope**. The member is accessible **only within the class**.

---

### **Access Modifier Scope**

| **Modifier**   | **Class** | **Package** | **Subclass** | **World (Other Packages)** |
|----------------|-----------|-------------|--------------|----------------------------|
| `public`       | ✅         | ✅           | ✅            | ✅                          |
| `protected`    | ✅         | ✅           | ✅            | ❌                          |
| `default`      | ✅         | ✅           | ❌            | ❌                          |
| `private`      | ✅         | ❌           | ❌            | ❌                          |

---

### **Examples and Detailed Explanation**

#### **1. `public` Access Modifier**

- **Scope**: Members with `public` access can be accessed from **anywhere**, even from other packages.
- **Use Case**: Use `public` for methods or variables that need to be globally accessible.

#### **Example**
```java
package package1;

public class PublicClass {
    public String message = "Public Access Modifier";

    public void displayMessage() {
        System.out.println(message);
    }
}

// Access from another package
package package2;

import package1.PublicClass;

public class Main {
    public static void main(String[] args) {
        PublicClass obj = new PublicClass();
        obj.displayMessage(); // Accessible from another package
    }
}
```

#### **Output**
```
Public Access Modifier
```

---

#### **2. `protected` Access Modifier**

- **Scope**:
  - Members with `protected` access are accessible within the **same package** and in **subclasses** (even if the subclass is in a different package).
- **Use Case**: Use `protected` for members that should be visible to subclasses but hidden from non-related classes.

#### **Example**
```java
package package1;

public class ProtectedClass {
    protected String message = "Protected Access Modifier";

    protected void displayMessage() {
        System.out.println(message);
    }
}

// Subclass in another package
package package2;

import package1.ProtectedClass;

public class SubClass extends ProtectedClass {
    public void accessProtected() {
        displayMessage(); // Accessible in subclass
    }

    public static void main(String[] args) {
        SubClass obj = new SubClass();
        obj.accessProtected();
    }
}
```

#### **Output**
```
Protected Access Modifier
```

---

#### **3. `default` (Package-Private) Access Modifier**

- **Scope**:
  - Members with default access are accessible **only within the same package**.
  - No keyword is used for the default access modifier.
- **Use Case**: Use default access for internal logic that does not need to be exposed outside the package.

#### **Example**
```java
package package1;

class DefaultClass {
    String message = "Default Access Modifier";

    void displayMessage() {
        System.out.println(message);
    }
}

// Attempt to access from another package
package package2;

import package1.DefaultClass; // Compile-time error: DefaultClass is not visible

public class Main {
    public static void main(String[] args) {
        // DefaultClass obj = new DefaultClass(); // Compile-time error
    }
}
```

#### **Output**
```
Compile-time error: DefaultClass is not visible
```

---

#### **4. `private` Access Modifier**

- **Scope**:
  - Members with `private` access are accessible **only within the class** where they are declared.
  - They are not visible to other classes, even in the same package.
- **Use Case**: Use `private` for encapsulation, hiding sensitive or implementation-specific details.

#### **Example**
```java
package package1;

public class PrivateClass {
    private String message = "Private Access Modifier";

    private void displayMessage() {
        System.out.println(message);
    }

    public void publicMethod() {
        displayMessage(); // Accessible within the same class
    }
}

public class Main {
    public static void main(String[] args) {
        PrivateClass obj = new PrivateClass();
        // obj.displayMessage(); // Compile-time error: displayMessage() is private
        obj.publicMethod(); // Indirect access through public method
    }
}
```

#### **Output**
```
Private Access Modifier
```

---

### **Key Differences Between Access Modifiers**

| **Aspect**             | **`public`**                           | **`protected`**                     | **`default` (no modifier)**         | **`private`**                       |
|------------------------|-----------------------------------------|--------------------------------------|-------------------------------------|-------------------------------------|
| **Visibility**          | Global (anywhere in the program).      | Same package + subclasses in other packages. | Within the same package only.       | Only within the same class.         |
| **Use Case**            | API methods, globally accessible logic. | Members for subclasses or internal APIs. | Internal package logic.             | Sensitive or private data.          |
| **Inheritance**         | Available to all subclasses.           | Available to subclasses only.       | Not inherited outside the package.  | Not accessible in subclasses.       |
| **Keyword**             | `public`                              | `protected`                         | No keyword                         | `private`                          |

---

### **Access Modifiers for Top-Level Classes**

Access modifiers can also be applied to **top-level classes** (i.e., classes that are not nested inside another class).

| **Modifier**   | **Top-Level Class Visibility**         |
|----------------|----------------------------------------|
| `public`       | Class is accessible from anywhere.     |
| `default`      | Class is accessible only in its package.|
| `protected`    | Not allowed for top-level classes.     |
| `private`      | Not allowed for top-level classes.     |

#### **Example: Public and Default Classes**
```java
// Public class
public class PublicClass {
    public void display() {
        System.out.println("Public class accessible anywhere.");
    }
}

// Default class
class DefaultClass {
    void display() {
        System.out.println("Default class accessible within the package.");
    }
}
```

---

### **Practical Use of Access Modifiers**

1. **Encapsulation**:
   - Use `private` for fields and expose them through `public` getter/setter methods.

   ```java
   public class User {
       private String name; // Encapsulation

       public String getName() {
           return name;
       }

       public void setName(String name) {
           this.name = name;
       }
   }
   ```

2. **Inheritance**:
   - Use `protected` for methods or fields you want to make available to subclasses but hide from other classes.

3. **API Design**:
   - Use `public` for classes and methods that should be accessible to external modules or users.
   - Use `default` to restrict internal logic to the same package.

---

### **Best Practices**

1. **Use `private` by Default**:
   - Start with `private` for fields and methods, then increase visibility only when necessary.

2. **Minimize Use of `public`**:
   - Avoid exposing too much of the internal implementation by using `public` unnecessarily.

3. **Leverage `protected` for Extensibility**:
   - Allow subclass customization with `protected` methods or fields.

4. **Restrict Internal Logic with `default`**:
   - Use the default access modifier for package-private logic.

---

### **Conclusion**

Access modifiers in Java are critical for implementing **encapsulation**, **inheritance**, and **modular programming**. By controlling the visibility and accessibility of classes and members, you can protect sensitive data, enforce proper access control, and maintain clean, modular code. Understanding when and how to use `public`, `protected`, `default`, and `private` modifiers is essential for writing efficient and maintainable Java applications.

## What is a Package in Java
### **What is a Package in Java?**

A **package** in Java is a namespace that organizes a group of related classes and interfaces. Think of it as a folder in your file system that stores Java classes, interfaces, and sub-packages. Packages are designed to avoid naming conflicts, provide easier access control, and group related types for better code organization and management.

---

### **Why Use Packages in Java?**

1. **Avoid Naming Conflicts**:
   - Packages allow developers to use the same class names in different packages without conflicts. For example, `java.util.Date` and `java.sql.Date` can coexist because they belong to different packages.

2. **Encapsulation**:
   - Packages help control access to classes and methods using access modifiers like `public`, `protected`, and `default`.

3. **Logical Grouping**:
   - Related classes and interfaces are grouped together for better code organization and readability.

4. **Reusability**:
   - Packages enable code reuse by sharing related functionality across multiple projects.

5. **Easier Maintenance**:
   - Organized packages make code easier to maintain, debug, and refactor.

---

### **Types of Packages**

Java provides two types of packages:

1. **Built-in Packages**:
   - Predefined packages provided by the Java API (e.g., `java.util`, `java.io`, `java.sql`).

2. **User-defined Packages**:
   - Custom packages created by developers to organize their own classes and interfaces.

---

### **How to Create a Package in Java**

#### **1. Syntax for Declaring a Package**
A package is declared at the **top of a Java file** using the `package` keyword.

```java
package packageName;
```

#### **2. Example of Creating a Package**

**Step 1: Create the Package**

Save the following file as `MyPackageClass.java` inside a folder named `mypackage`:

```java
package mypackage;

public class MyPackageClass {
    public void displayMessage() {
        System.out.println("Hello from MyPackageClass!");
    }
}
```

**Step 2: Compile the File**
```bash
javac -d . MyPackageClass.java
```
- The `-d .` flag creates the `mypackage` directory and stores the `.class` file there.

**Step 3: Use the Package**

Create another Java file in the same project or a different one to use the package:

```java
import mypackage.MyPackageClass;

public class Main {
    public static void main(String[] args) {
        MyPackageClass obj = new MyPackageClass();
        obj.displayMessage();
    }
}
```

**Step 4: Compile and Run the Program**
```bash
javac Main.java
java Main
```

#### **Output**
```
Hello from MyPackageClass!
```

---

### **Using Built-in Packages**

Java provides several built-in packages such as:

1. **`java.lang`**:
   - Includes core classes like `String`, `Math`, `Integer`, `System`, etc. Automatically imported by default.

2. **`java.util`**:
   - Provides utility classes like `ArrayList`, `HashMap`, `Date`, `Collections`, etc.

3. **`java.io`**:
   - Contains classes for input and output operations, such as `File`, `BufferedReader`, `InputStream`, etc.

4. **`java.sql`**:
   - Provides classes for working with databases, such as `Connection`, `Statement`, `ResultSet`.

5. **`java.net`**:
   - Contains classes for networking, such as `Socket`, `URL`, `HttpURLConnection`.

#### **Example: Using Built-in Package**
```java
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Banana");
        System.out.println(list);
    }
}
```

---

### **Access Modifiers and Packages**

The **access modifiers** in Java determine how classes and their members (fields, methods) can be accessed across packages.

| **Modifier**   | **Same Class** | **Same Package** | **Subclass (Different Package)** | **Other Packages** |
|----------------|----------------|------------------|----------------------------------|--------------------|
| `public`       | ✅              | ✅                | ✅                                | ✅                  |
| `protected`    | ✅              | ✅                | ✅                                | ❌                  |
| `default`      | ✅              | ✅                | ❌                                | ❌                  |
| `private`      | ✅              | ❌                | ❌                                | ❌                  |

---

### **Creating Sub-Packages**

A sub-package is a package inside another package. It is used to organize classes hierarchically.

#### **Example: Creating Sub-Packages**

**Step 1: Create the Package**

```java
package mypackage.subpackage;

public class SubPackageClass {
    public void displayMessage() {
        System.out.println("Hello from SubPackageClass!");
    }
}
```

**Step 2: Compile**
```bash
javac -d . SubPackageClass.java
```

**Step 3: Use the Sub-Package**
```java
import mypackage.subpackage.SubPackageClass;

public class Main {
    public static void main(String[] args) {
        SubPackageClass obj = new SubPackageClass();
        obj.displayMessage();
    }
}
```

**Output**
```
Hello from SubPackageClass!
```

---

### **Using the `import` Keyword**

- The `import` keyword is used to include a package or class in your program.
- There are two ways to import:

1. **Specific Class Import**:
   - Imports only the specified class.
   - Example:
     ```java
     import java.util.ArrayList;
     ```

2. **Wildcard Import**:
   - Imports all classes from the package.
   - Example:
     ```java
     import java.util.*;
     ```

#### **Example**
```java
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Orange");
        System.out.println(list);
    }
}
```

---

### **Static Import**

- The `static import` feature allows members (fields and methods) of a class to be accessed **without class qualification**.

#### **Example**
```java
import static java.lang.Math.*;

public class Main {
    public static void main(String[] args) {
        double result = sqrt(16); // No need to write Math.sqrt
        System.out.println("Square Root: " + result);
    }
}
```

**Output**
```
Square Root: 4.0
```

---

### **Advantages of Packages**

1. **Code Organization**:
   - Packages help organize related classes and interfaces.

2. **Encapsulation**:
   - Packages provide controlled access to classes and members using access modifiers.

3. **Avoids Name Conflicts**:
   - Packages help prevent naming conflicts by using fully qualified names (e.g., `java.util.Date` vs. `java.sql.Date`).

4. **Reusability**:
   - Classes in a package can be reused in multiple projects.

---

### **Best Practices**

1. **Use Meaningful Names**:
   - Package names should represent the functionality or domain. For example:
     - `com.mycompany.utils` for utility classes.
     - `com.mycompany.services` for service classes.

2. **Follow Naming Conventions**:
   - Package names should be written in **lowercase** to avoid conflicts with class names.
   - Example: `mypackage`, `com.mycompany.project`.

3. **Avoid Too Many Sub-Packages**:
   - Over-complicating the package structure can make code harder to maintain.

---

### **Conclusion**

A **package** in Java is a powerful mechanism for organizing classes and interfaces. It helps in avoiding name conflicts, enforcing encapsulation, and creating reusable code components. By using Java's built-in packages or creating your own user-defined packages, you can improve the modularity, readability, and maintainability of your Java applications. Understanding packages and how to use them effectively is a crucial aspect of mastering Java development.

## What is a Mutable Class vs Immutable Class in Java
### **Mutable Class vs Immutable Class in Java**

In Java, a **mutable class** allows its objects to be modified after they are created, whereas an **immutable class** does not allow modifications to its objects once they are created. These concepts are central to understanding immutability, thread safety, and the design of classes in Java.

---

### **What is a Mutable Class?**

A **mutable class** is a class whose objects can be modified after they are created. This means you can change the state (fields) of an object of a mutable class.

#### **Characteristics of a Mutable Class**

1. **State Can Be Changed**:
   - Fields of the object can be modified after the object is created.

2. **Setter Methods**:
   - Mutable classes usually provide setter methods to update the fields.

3. **Thread Safety**:
   - Mutable classes are **not thread-safe** by default because multiple threads can modify their state simultaneously.

---

#### **Example of a Mutable Class**
```java
class MutableClass {
    private String name;

    // Constructor
    public MutableClass(String name) {
        this.name = name;
    }

    // Getter
    public String getName() {
        return name;
    }

    // Setter
    public void setName(String name) {
        this.name = name;
    }
}

public class Main {
    public static void main(String[] args) {
        MutableClass obj = new MutableClass("Alice");

        // State can be changed
        System.out.println("Initial Name: " + obj.getName());
        obj.setName("Bob");
        System.out.println("Updated Name: " + obj.getName());
    }
}
```

#### **Output**
```
Initial Name: Alice
Updated Name: Bob
```

---

### **What is an Immutable Class?**

An **immutable class** is a class whose objects cannot be modified after they are created. Any change to an object creates a new object rather than modifying the existing one.

#### **Characteristics of an Immutable Class**

1. **State Cannot Be Changed**:
   - Once an object is created, its fields cannot be modified.

2. **No Setter Methods**:
   - Immutable classes do not provide setter methods.

3. **Final Fields**:
   - Fields are declared as `final` to ensure they are assigned only once.

4. **Thread Safety**:
   - Immutable objects are inherently thread-safe because their state cannot change after creation.

5. **Copies of Fields**:
   - If a mutable object is used as a field, make defensive copies to prevent modifications from outside.

---

#### **Example of an Immutable Class**
```java
final class ImmutableClass {
    private final String name;

    // Constructor
    public ImmutableClass(String name) {
        this.name = name;
    }

    // Getter
    public String getName() {
        return name;
    }
}

public class Main {
    public static void main(String[] args) {
        ImmutableClass obj = new ImmutableClass("Alice");

        // No way to change the state of the object
        System.out.println("Name: " + obj.getName());
        // obj.setName("Bob"); // Compile-time error (no setter method)
    }
}
```

#### **Output**
```
Name: Alice
```

---

### **Key Differences Between Mutable and Immutable Classes**

| **Aspect**            | **Mutable Class**                                  | **Immutable Class**                               |
|-----------------------|--------------------------------------------------|-------------------------------------------------|
| **State**             | Can be changed after object creation.            | Cannot be changed after object creation.        |
| **Setter Methods**    | Provides setters to modify fields.               | Does not provide setters.                       |
| **Thread Safety**     | Not inherently thread-safe.                      | Thread-safe by design.                          |
| **Final Keyword**     | Fields are usually not `final`.                  | Fields are declared as `final`.                 |
| **Object Modification** | Modifies the same object.                       | Creates a new object for every modification.    |
| **Example**           | `StringBuilder`, `ArrayList`.                    | `String`, `Integer`.                            |

---

### **Mutable Class vs Immutable Class: Real-Life Analogies**

1. **Mutable Class**: A **whiteboard** where you can erase and rewrite multiple times.
2. **Immutable Class**: A **permanent marker**—once you write something, it cannot be changed.

---

### **How to Create an Immutable Class**

To make a class immutable, follow these guidelines:

1. **Declare the Class as `final`**:
   - Prevents subclasses from modifying behavior.

2. **Make All Fields `private` and `final`**:
   - Prevents direct access and ensures fields are assigned only once.

3. **Do Not Provide Setter Methods**:
   - Prevents external modification of fields.

4. **Provide Only Getters**:
   - Expose field values through getter methods.

5. **Use Defensive Copies**:
   - If a field holds a reference to a mutable object, return a copy in the getter to prevent external modification.

---

#### **Example with Defensive Copy**
```java
import java.util.Date;

final class ImmutablePerson {
    private final String name;
    private final Date birthDate;

    // Constructor
    public ImmutablePerson(String name, Date birthDate) {
        this.name = name;
        // Defensive copy of mutable object
        this.birthDate = new Date(birthDate.getTime());
    }

    // Getter for name
    public String getName() {
        return name;
    }

    // Getter for birthDate
    public Date getBirthDate() {
        // Return a copy of the date
        return new Date(birthDate.getTime());
    }
}

public class Main {
    public static void main(String[] args) {
        Date birthDate = new Date();
        ImmutablePerson person = new ImmutablePerson("Alice", birthDate);

        System.out.println("Name: " + person.getName());
        System.out.println("BirthDate: " + person.getBirthDate());

        // Attempt to modify birthDate
        birthDate.setTime(birthDate.getTime() + 1000000000L);
        System.out.println("Modified BirthDate: " + person.getBirthDate());
    }
}
```

#### **Output**
```
Name: Alice
BirthDate: [Original date]
Modified BirthDate: [Original date remains unchanged]
```

---

### **Advantages of Immutable Classes**

1. **Thread Safety**:
   - Immutable objects are inherently thread-safe, so they can be shared between threads without synchronization.

2. **Cache Efficiency**:
   - Immutable objects can be cached, as their state never changes.

3. **Predictability**:
   - Immutable objects are easier to reason about, as they do not change state unpredictably.

4. **Reliable Hashing**:
   - Immutable objects can safely be used as keys in `HashMap` or elements in `HashSet` because their hash values will not change.

---

### **Disadvantages of Immutable Classes**

1. **Memory Overhead**:
   - Since every modification creates a new object, immutable classes may lead to higher memory usage.

2. **Performance Cost**:
   - Repeated modifications can be less efficient compared to mutating an object in place.

3. **Complexity for Large Objects**:
   - Managing defensive copies and immutability for objects with multiple fields can be cumbersome.

---

### **When to Use Mutable vs Immutable Classes**

#### **Use Mutable Classes When:**
1. You need to frequently modify the state of an object.
2. You require fine-grained control over the state of an object (e.g., `ArrayList`, `StringBuilder`).

#### **Use Immutable Classes When:**
1. You need thread-safe objects without synchronization.
2. The object's state should remain constant after creation (e.g., `String`, `Integer`).
3. You want to use objects as keys in `HashMap` or `HashSet`.

---

### **Immutable vs Mutable Classes in the Java Standard Library**

| **Immutable Classes**        | **Mutable Classes**         |
|------------------------------|-----------------------------|
| `String`                     | `StringBuilder`, `StringBuffer` |
| `Integer`, `Double`, `Boolean` | `java.util.Date`, `ArrayList` |
| `BigInteger`, `BigDecimal`   | `HashMap`, `HashSet`        |

---

### **Conclusion**

The choice between mutable and immutable classes depends on the specific use case. **Mutable classes** are flexible and efficient for objects that need frequent state changes, but they require careful handling in multithreaded environments. **Immutable classes**, on the other hand, are thread-safe, predictable, and easier to use in concurrent programming. By understanding the characteristics, advantages, and trade-offs of each, you can design more robust and maintainable Java applications.

## How to Make a Class Immutable in Java
### **How to Make a Class Immutable in Java**

To make a class immutable in Java, you must ensure that the state of its objects cannot be modified after they are created. This involves several design principles and restrictions. Below is a detailed step-by-step guide to creating an immutable class in Java.

---

### **Steps to Make a Class Immutable**

1. **Declare the Class as `final`**:
   - This prevents the class from being extended. Without this, a subclass could override methods and potentially modify the state of the object.

   ```java
   public final class ImmutableClass {
       // Class definition
   }
   ```

2. **Make All Fields `private` and `final`**:
   - Declaring fields as `private` ensures they cannot be accessed directly from outside the class.
   - Declaring them as `final` ensures their values are assigned only once (either during declaration or in the constructor).

   ```java
   private final String name;
   private final int age;
   ```

3. **Do Not Provide Setter Methods**:
   - Exclude any methods that allow modification of the fields after the object is created.

   ```java
   // No setters
   ```

4. **Initialize All Fields via Constructor**:
   - Provide a constructor that initializes all fields. Once the object is created, the fields should not be modifiable.

   ```java
   public ImmutableClass(String name, int age) {
       this.name = name;
       this.age = age;
   }
   ```

5. **Return Defensive Copies of Mutable Fields**:
   - If the class contains fields that reference mutable objects (e.g., `Date`, `List`, or custom objects), return defensive copies instead of the original objects in getter methods.

   ```java
   private final Date birthDate;

   public ImmutableClass(String name, int age, Date birthDate) {
       this.name = name;
       this.age = age;
       this.birthDate = new Date(birthDate.getTime()); // Defensive copy
   }

   public Date getBirthDate() {
       return new Date(birthDate.getTime()); // Defensive copy
   }
   ```

6. **Avoid "Leaking" `this` Reference**:
   - Do not allow the `this` reference to escape during object construction. This can happen if you pass `this` to a method or another object during construction.

---

### **Complete Example: Immutable Class**

```java
import java.util.Date;

public final class ImmutableClass {
    private final String name;
    private final int age;
    private final Date birthDate;

    // Constructor initializes all fields
    public ImmutableClass(String name, int age, Date birthDate) {
        this.name = name;
        this.age = age;
        // Create a defensive copy of the mutable object
        this.birthDate = new Date(birthDate.getTime());
    }

    // Getter for name
    public String getName() {
        return name;
    }

    // Getter for age
    public int getAge() {
        return age;
    }

    // Getter for birthDate with defensive copy
    public Date getBirthDate() {
        return new Date(birthDate.getTime());
    }
}
```

---

### **Testing the Immutable Class**

```java
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Date birthDate = new Date();
        ImmutableClass immutableObject = new ImmutableClass("Alice", 30, birthDate);

        System.out.println("Name: " + immutableObject.getName());
        System.out.println("Age: " + immutableObject.getAge());
        System.out.println("BirthDate: " + immutableObject.getBirthDate());

        // Attempt to modify birthDate outside the immutable class
        birthDate.setTime(birthDate.getTime() + 1000000000L);
        System.out.println("Modified BirthDate: " + immutableObject.getBirthDate()); // Should not reflect the change
    }
}
```

---

### **Output**
```
Name: Alice
Age: 30
BirthDate: [Original Date]
Modified BirthDate: [Original Date remains unchanged]
```

---

### **Key Points in the Example**

1. **Class Declaration**:
   - `final` ensures no subclass can alter the behavior.

2. **Private and Final Fields**:
   - `private` ensures fields are not accessible directly.
   - `final` ensures fields cannot be reassigned.

3. **Defensive Copies**:
   - The constructor and getter create defensive copies of the mutable `Date` object to prevent external modification.

4. **No Setter Methods**:
   - No methods are provided to modify the fields after the object is created.

---

### **Advantages of Immutable Classes**

1. **Thread-Safety**:
   - Immutable objects are inherently thread-safe because their state cannot change, so no synchronization is required.

2. **Cache-Friendly**:
   - Immutable objects can be safely cached and reused, improving performance.

3. **Reliable Keys for Collections**:
   - Immutable objects are ideal for use as keys in `HashMap` or elements in `HashSet`, as their hash values remain consistent.

4. **Easier to Understand**:
   - Immutable objects reduce complexity because their behavior is predictable.

---

### **Disadvantages of Immutable Classes**

1. **Memory Overhead**:
   - Creating a new object for every modification can lead to increased memory usage, especially for large objects.

2. **Performance Overhead**:
   - Repeatedly creating new objects instead of modifying existing ones can be less efficient.

3. **Defensive Copies**:
   - For classes with many mutable fields, creating defensive copies can make the implementation more complex and resource-intensive.

---

### **Immutable vs Mutable Classes**

| **Aspect**             | **Immutable Class**                     | **Mutable Class**                    |
|------------------------|------------------------------------------|--------------------------------------|
| **State Modification** | Cannot be changed after creation.        | Can be modified after creation.      |
| **Thread Safety**      | Inherently thread-safe.                  | Requires synchronization for thread safety. |
| **Performance**        | Slower for frequent changes.             | Faster for frequent changes.         |
| **Examples**           | `String`, `Integer`, `BigDecimal`.       | `StringBuilder`, `ArrayList`, `HashMap`. |

---

### **When to Use Immutable Classes**

1. **Thread-Safe Operations**:
   - When you want objects that can be safely shared between multiple threads without synchronization.

2. **Keys in Collections**:
   - When objects are used as keys in collections like `HashMap` or `HashSet`.

3. **Security**:
   - When you need to ensure sensitive data is not modified after being shared.

4. **Functional Programming**:
   - Immutable objects align with functional programming principles, where objects are treated as unchangeable.

---

### **Conclusion**

Making a class immutable in Java requires careful design by following strict guidelines, such as using `final` for classes and fields, not providing setters, and ensuring defensive copies for mutable fields. Immutable classes offer significant advantages, such as thread-safety, simplicity, and reliability, but they also have trade-offs like higher memory usage and performance overhead. They are ideal for scenarios where immutability is essential, such as multithreaded applications, functional programming, and secure data handling.

## What is the Difference Between equals() and == Operator in Java
### **Difference Between `equals()` and `==` Operator in Java**

The **`equals()`** method and the **`==`** operator are used to compare objects and variables in Java, but they work differently and serve distinct purposes.

---

### **Key Differences**

| **Aspect**                | **`equals()` Method**                               | **`==` Operator**                                   |
|---------------------------|----------------------------------------------------|----------------------------------------------------|
| **Purpose**               | Used to compare the **content** or **logical equality** of two objects. | Used to compare the **references** (memory addresses) of two objects, or the values of primitive types. |
| **Type of Comparison**    | Logical comparison of the values/content of objects. | Reference comparison for objects, value comparison for primitives. |
| **Defined In**            | Defined in the `Object` class and can be overridden. | Part of Java's language syntax, cannot be overridden. |
| **Custom Implementation** | Can be overridden to define custom equality logic.  | Always checks references for objects or values for primitives. |
| **Use Case**              | Compare the data stored in two objects.             | Check if two references point to the same object in memory. |

---

### **1. `equals()` Method**

- **Definition**:
  - The `equals()` method is defined in the `Object` class and is meant to compare the **content** or **logical equality** of two objects.
  - By default, the `equals()` method in the `Object` class compares references (like `==`), but it is commonly overridden in classes like `String`, `Integer`, and custom objects to compare content.

#### **Example: Using `equals()` with Strings**

```java
public class Main {
    public static void main(String[] args) {
        String str1 = new String("Hello");
        String str2 = new String("Hello");

        System.out.println(str1.equals(str2)); // true, because the content is the same
    }
}
```

#### **Output**
```
true
```

#### **Explanation**
- Even though `str1` and `str2` are different objects in memory, their content (`"Hello"`) is the same, so `equals()` returns `true`.

---

### **2. `==` Operator**

- **Definition**:
  - The `==` operator compares **references** for objects and **values** for primitive data types.
  - When used with objects, it checks whether the two references point to the **same memory location**.

#### **Example: Using `==` with Strings**

```java
public class Main {
    public static void main(String[] args) {
        String str1 = new String("Hello");
        String str2 = new String("Hello");

        System.out.println(str1 == str2); // false, because they are different objects
    }
}
```

#### **Output**
```
false
```

#### **Explanation**
- `str1` and `str2` are two different objects in memory, even though their content is the same. Therefore, `==` returns `false`.

---

### **Detailed Comparison Scenarios**

#### **1. Comparing Primitives**

For **primitive types**, the `==` operator compares their values.

```java
public class Main {
    public static void main(String[] args) {
        int a = 10;
        int b = 10;

        System.out.println(a == b); // true, because their values are equal
    }
}
```

#### **Output**
```
true
```

#### **2. Comparing Objects**

For **objects**, the `==` operator compares references (memory locations), not the content.

```java
public class Main {
    public static void main(String[] args) {
        Integer num1 = new Integer(100);
        Integer num2 = new Integer(100);

        System.out.println(num1 == num2);       // false, because they are different objects
        System.out.println(num1.equals(num2)); // true, because their content is the same
    }
}
```

#### **Output**
```
false
true
```

---

#### **3. Comparing Strings (String Pool)**

Strings in Java are stored in a **string pool**, which allows reusing the same instance of a string literal. When comparing string literals, `==` might return `true` because they point to the same memory location.

```java
public class Main {
    public static void main(String[] args) {
        String str1 = "Hello";
        String str2 = "Hello";

        System.out.println(str1 == str2);       // true, because both point to the same object in the string pool
        System.out.println(str1.equals(str2)); // true, because their content is the same
    }
}
```

#### **Output**
```
true
true
```

#### **Explanation**
- When you use string literals (`"Hello"`), Java reuses the same object from the string pool.
- If you use `new String("Hello")`, a new object is created outside the string pool.

---

#### **4. Custom Classes**

By default, the `equals()` method in custom classes behaves like `==`. To compare content, you must override the `equals()` method.

```java
class Person {
    String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Person person = (Person) obj;
        return name.equals(person.name);
    }
}

public class Main {
    public static void main(String[] args) {
        Person p1 = new Person("Alice");
        Person p2 = new Person("Alice");

        System.out.println(p1 == p2);       // false, because they are different objects
        System.out.println(p1.equals(p2)); // true, because their content is the same
    }
}
```

#### **Output**
```
false
true
```

---

### **When to Use `equals()` vs `==`**

1. **Use `equals()` When**:
   - Comparing the **content** of two objects.
   - Comparing strings, collections, or custom objects where logical equality is required.

2. **Use `==` When**:
   - Comparing **primitive types** (e.g., `int`, `double`, etc.).
   - Checking whether two references point to the **same object in memory**.

---

### **Common Misunderstandings**

1. **Strings and `==`**:
   - Many developers mistakenly use `==` to compare strings. Always use `equals()` for string content comparison.

   ```java
   String str1 = new String("Java");
   String str2 = new String("Java");

   System.out.println(str1 == str2);       // false
   System.out.println(str1.equals(str2)); // true
   ```

2. **Custom Classes Without Overriding `equals()`**:
   - If you don’t override `equals()` in a custom class, it behaves like `==`, comparing references.

---

### **Best Practices**

1. **Override `equals()` in Custom Classes**:
   - Always override the `equals()` method when logical equality is required.

2. **Use `Objects.equals()` for Null-Safe Comparison**:
   - Java provides the `Objects.equals()` method to handle `null` values gracefully.

   ```java
   System.out.println(Objects.equals(null, "Hello")); // false
   System.out.println(Objects.equals(null, null));   // true
   ```

3. **Be Aware of String Pooling**:
   - Use `equals()` for comparing string content, even when literals seem equal with `==`.

---

### **Conclusion**

The **`equals()` method** is used for comparing the **content** (logical equality) of two objects, while the **`==` operator** compares their **references** (memory locations). For primitive types, `==` compares the actual values. Always use `equals()` when you want to check if two objects are logically the same, especially for strings, collections, or custom objects. Understanding the differences helps avoid common bugs and ensures more robust and readable Java code.

## What is the Difference Between String, StringBuilder, and StringBuffer in Java
### **Difference Between `String`, `StringBuilder`, and `StringBuffer` in Java**

In Java, `String`, `StringBuilder`, and `StringBuffer` are used to work with strings, but they differ in terms of **mutability**, **thread safety**, and **performance**.

---

### **Overview**

| **Aspect**        | **`String`**                          | **`StringBuilder`**                 | **`StringBuffer`**                  |
|--------------------|---------------------------------------|-------------------------------------|-------------------------------------|
| **Mutability**     | Immutable (cannot be changed).        | Mutable (can be changed).           | Mutable (can be changed).           |
| **Thread-Safety**  | Thread-safe (due to immutability).    | Not thread-safe.                    | Thread-safe (synchronized).         |
| **Performance**    | Slower for frequent modifications.   | Faster for single-threaded use.     | Slower due to synchronization.      |
| **Usage**          | For constant or unchanging strings.  | For mutable strings in single-threaded environments. | For mutable strings in multi-threaded environments. |

---

### **Detailed Explanation**

#### **1. `String`**

A `String` in Java is an **immutable object**. Once a `String` object is created, it cannot be modified. Any operation that seems to modify a `String` actually creates a new `String` object.

##### **Characteristics of `String`**
- **Immutable**: Once created, it cannot be changed.
- **Stored in String Pool**: String literals are stored in a special memory area called the **String Pool** for better memory management.
- **Performance**: Slower when performing frequent modifications due to new object creation.

##### **Example**
```java
public class Main {
    public static void main(String[] args) {
        String str = "Hello";
        str.concat(" World"); // Does not modify the original string
        System.out.println(str); // Output: Hello

        // Correct way to modify a string
        str = str.concat(" World");
        System.out.println(str); // Output: Hello World
    }
}
```

#### **Output**
```
Hello
Hello World
```

#### **When to Use**
- Use `String` when the value is **constant** and won't change frequently.
- Example: Keys in a `HashMap`.

---

#### **2. `StringBuilder`**

`StringBuilder` is a **mutable** class introduced in Java 5. It allows modifying string content without creating new objects, making it faster for frequent string manipulations in **single-threaded** environments.

##### **Characteristics of `StringBuilder`**
- **Mutable**: Can modify its content without creating new objects.
- **Not Thread-Safe**: Should not be used in multi-threaded environments.
- **Performance**: Faster than `String` and `StringBuffer` due to the absence of synchronization.

##### **Example**
```java
public class Main {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("Hello");
        sb.append(" World"); // Modifies the original object
        System.out.println(sb); // Output: Hello World
    }
}
```

#### **Output**
```
Hello World
```

#### **Common Methods of `StringBuilder`**
| **Method**          | **Description**                                      | **Example**                                   |
|---------------------|-----------------------------------------------------|----------------------------------------------|
| `append(String s)`   | Appends a string to the current object.              | `sb.append(" World");`                       |
| `insert(int index, String s)` | Inserts a string at the specified index.         | `sb.insert(5, " Java");`                     |
| `delete(int start, int end)`  | Deletes characters from start to end index.      | `sb.delete(0, 5);`                           |
| `reverse()`          | Reverses the string.                                 | `sb.reverse();`                              |

#### **When to Use**
- Use `StringBuilder` when string modifications are required in **single-threaded** programs.

---

#### **3. `StringBuffer`**

`StringBuffer` is similar to `StringBuilder`, but it is **thread-safe** because all its methods are **synchronized**. This makes it slower than `StringBuilder` but suitable for **multi-threaded** environments.

##### **Characteristics of `StringBuffer`**
- **Mutable**: Can modify its content without creating new objects.
- **Thread-Safe**: Methods are synchronized to ensure safe use in multi-threaded environments.
- **Performance**: Slower than `StringBuilder` due to synchronization overhead.

##### **Example**
```java
public class Main {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("Hello");
        sb.append(" World"); // Modifies the original object
        System.out.println(sb); // Output: Hello World
    }
}
```

#### **Output**
```
Hello World
```

#### **Common Methods of `StringBuffer`**
Methods of `StringBuffer` are identical to `StringBuilder`, as they share the same API.

#### **When to Use**
- Use `StringBuffer` when string modifications are required in **multi-threaded** programs.

---

### **Performance Comparison**

1. **String**:
   - Every modification creates a new object, leading to higher memory usage and slower performance for frequent changes.

2. **StringBuilder**:
   - Faster for single-threaded applications because it avoids the overhead of synchronization.

3. **StringBuffer**:
   - Slower than `StringBuilder` due to synchronization but suitable for multi-threaded applications.

---

### **Key Differences Between `String`, `StringBuilder`, and `StringBuffer`**

| **Aspect**              | **`String`**                     | **`StringBuilder`**              | **`StringBuffer`**              |
|--------------------------|-----------------------------------|-----------------------------------|----------------------------------|
| **Mutability**           | Immutable                        | Mutable                           | Mutable                          |
| **Thread-Safety**        | Thread-safe (due to immutability)| Not thread-safe                   | Thread-safe (synchronized)       |
| **Performance**          | Slow for frequent modifications  | Fast in single-threaded programs  | Slower due to synchronization    |
| **Usage**                | Constant or rarely modified data | Single-threaded string operations | Multi-threaded string operations |

---

### **Example: Comparing Performance**

```java
public class Main {
    public static void main(String[] args) {
        // Using String
        long startTime = System.currentTimeMillis();
        String str = "Hello";
        for (int i = 0; i < 10000; i++) {
            str += " World";
        }
        long endTime = System.currentTimeMillis();
        System.out.println("String Time: " + (endTime - startTime) + "ms");

        // Using StringBuilder
        startTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder("Hello");
        for (int i = 0; i < 10000; i++) {
            sb.append(" World");
        }
        endTime = System.currentTimeMillis();
        System.out.println("StringBuilder Time: " + (endTime - startTime) + "ms");

        // Using StringBuffer
        startTime = System.currentTimeMillis();
        StringBuffer sbf = new StringBuffer("Hello");
        for (int i = 0; i < 10000; i++) {
            sbf.append(" World");
        }
        endTime = System.currentTimeMillis();
        System.out.println("StringBuffer Time: " + (endTime - startTime) + "ms");
    }
}
```

#### **Output (Example Runtime)**
```
String Time: 1050ms
StringBuilder Time: 5ms
StringBuffer Time: 10ms
```

#### **Explanation**
- `String` is slow because it creates a new object for every concatenation.
- `StringBuilder` is the fastest because it avoids synchronization overhead.
- `StringBuffer` is slower than `StringBuilder` due to synchronization.

---

### **Summary**

| **Class**        | **Best For**                                    |
|-------------------|------------------------------------------------|
| **`String`**      | When the string content does not change frequently (e.g., constants, keys). |
| **`StringBuilder`** | When frequent modifications are required in single-threaded environments. |
| **`StringBuffer`** | When frequent modifications are required in multi-threaded environments. |

By choosing the right class (`String`, `StringBuilder`, or `StringBuffer`), you can optimize your code for performance, thread safety, and ease of use.

## What is the Difference Between Pass by Reference and Pass by Value
### **Difference Between Pass by Reference and Pass by Value**

In programming, **Pass by Reference** and **Pass by Value** are concepts related to how arguments are passed to functions or methods. The distinction lies in whether a method receives the **actual memory reference** of the variable or a **copy of its value**.

---

### **Key Differences**

| **Aspect**             | **Pass by Value**                                         | **Pass by Reference**                                 |
|-------------------------|---------------------------------------------------------|-----------------------------------------------------|
| **Definition**          | A copy of the variable's value is passed to the method. | The actual reference (memory address) of the variable is passed. |
| **Effect on Original Data** | Changes made inside the method do **not** affect the original variable. | Changes made inside the method **affect** the original variable. |
| **Used In**             | Languages like **Java**, **C**, **Python** (for immutable types). | Languages like **C++** (explicit with pointers), **Python** (mutable types). |
| **Implementation**      | Copies the value and works on the duplicate.            | Works directly with the memory address of the object. |
| **Safety**              | Safer, as the original data remains unchanged.          | Less safe, as the original data can be modified.     |

---

### **How Java Handles It**

Java uses **Pass by Value** for all method arguments. However, the way it works depends on whether the argument is a **primitive type** or an **object reference**.

1. **Primitive Types**:
   - The value is directly copied, so changes inside the method do not affect the original variable.
2. **Objects**:
   - The value of the reference (memory address) is passed. While you cannot change the reference itself, you can modify the object it points to.

---

### **Pass by Value (Java Example with Primitives)**

```java
public class Main {
    public static void modifyValue(int num) {
        num = 20; // Modifies the local copy
    }

    public static void main(String[] args) {
        int value = 10;
        modifyValue(value); // Pass by value
        System.out.println("Value after modification: " + value); // Output: 10
    }
}
```

#### **Output**
```
Value after modification: 10
```

#### **Explanation**
- A copy of `value` is passed to the `modifyValue` method.
- Changes to `num` inside the method do not affect the original variable `value`.

---

### **Pass by Value with Object References (Java Example)**

```java
class Person {
    String name;
}

public class Main {
    public static void modifyPerson(Person person) {
        person.name = "Alice"; // Modifies the object
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Bob";

        modifyPerson(person); // Pass by value (of the reference)
        System.out.println("Name after modification: " + person.name); // Output: Alice
    }
}
```

#### **Output**
```
Name after modification: Alice
```

#### **Explanation**
- The value of the reference to the `person` object is passed.
- Inside the method, the object that the reference points to is modified, affecting the original object.

---

### **Pass by Reference (C++ Example)**

In languages like **C++**, you can explicitly use pass-by-reference by using pointers or reference types.

```cpp
#include <iostream>
using namespace std;

void modifyValue(int &num) { // Pass by reference
    num = 20;
}

int main() {
    int value = 10;
    modifyValue(value); // Original variable is passed by reference
    cout << "Value after modification: " << value << endl; // Output: 20
    return 0;
}
```

#### **Output**
```
Value after modification: 20
```

#### **Explanation**
- The actual memory address of `value` is passed to `modifyValue`.
- Any changes made in the function affect the original variable.

---

### **Common Misunderstanding in Java**

People sometimes mistakenly assume Java supports **Pass by Reference** for objects. However, Java always uses **Pass by Value**, even for objects. What gets passed is the **value of the reference**.

#### **Example to Clarify**

```java
class Person {
    String name;
}

public class Main {
    public static void modifyReference(Person person) {
        person = new Person(); // Creates a new object
        person.name = "Alice"; // Modifies the new object
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Bob";

        modifyReference(person); // Pass by value of the reference
        System.out.println("Name after modification: " + person.name); // Output: Bob
    }
}
```

#### **Output**
```
Name after modification: Bob
```

#### **Explanation**
- The value of the reference to `person` is passed to the method.
- Reassigning `person` in the method creates a new object and does not affect the original reference.

---

### **Advantages of Pass by Value**

1. **Safety**:
   - Prevents unintended changes to the original data.
2. **Simplifies Debugging**:
   - You don’t have to worry about methods altering your data unexpectedly.

---

### **Advantages of Pass by Reference**

1. **Efficiency**:
   - Large objects are not copied, saving memory and CPU time.
2. **Direct Modification**:
   - Useful when you want methods to update the original data.

---

### **Summary**

| **Aspect**            | **Pass by Value**                                        | **Pass by Reference**                               |
|-----------------------|---------------------------------------------------------|----------------------------------------------------|
| **Key Behavior**       | Passes a copy of the value.                             | Passes the actual reference (memory address).      |
| **Impact on Original** | Does not affect the original variable.                  | Can affect the original variable.                 |
| **Java Behavior**      | Always uses Pass by Value (even for object references). | Not supported in Java.                            |
| **C++ Behavior**       | Default for primitives.                                 | Supported using pointers or references.           |

Understanding the difference between **Pass by Value** and **Pass by Reference** helps you write better, more predictable code, especially when working with methods that modify data.

## Is Java Pass by Reference or Pass by Value
### **Is Java Pass by Reference or Pass by Value?**

Java is **strictly Pass by Value**, regardless of whether the argument is a **primitive type** or an **object reference**. This means that **a copy of the variable's value** is passed to methods, not the variable itself.

However, when dealing with objects, the **value of the reference (memory address)** is passed to the method. This behavior can sometimes lead to confusion, making it appear as though Java is using Pass by Reference. Let's break it down.

---

### **Understanding Pass by Value in Java**

#### **1. Primitive Types**
For **primitive types** (`int`, `float`, `double`, etc.), a copy of the actual value is passed to the method. Changes made inside the method **do not affect the original variable**.

#### Example: Pass by Value for Primitives
```java
public class Main {
    public static void modifyValue(int num) {
        num = 20; // Modifies the local copy
    }

    public static void main(String[] args) {
        int value = 10;
        modifyValue(value); // Pass by value
        System.out.println("Value after modification: " + value); // Output: 10
    }
}
```

#### **Output**
```
Value after modification: 10
```

#### **Explanation**
- A copy of `value` is passed to the `modifyValue` method.
- Changes made to `num` (the local copy) do not affect the original variable `value`.

---

#### **2. Object References**
For **objects**, a copy of the **reference (memory address)** is passed to the method. While you cannot change the reference itself to point to a new object, you can modify the **contents** of the object the reference points to.

#### Example: Pass by Value for Object References
```java
class Person {
    String name;
}

public class Main {
    public static void modifyObject(Person person) {
        person.name = "Alice"; // Modifies the object
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Bob";

        modifyObject(person); // Pass by value (of the reference)
        System.out.println("Name after modification: " + person.name); // Output: Alice
    }
}
```

#### **Output**
```
Name after modification: Alice
```

#### **Explanation**
- The value of the reference (memory address) is passed to the `modifyObject` method.
- Inside the method, the object pointed to by the reference is modified, so the changes are visible outside the method.

---

#### **What if We Try to Reassign the Reference?**

If you try to reassign the reference inside the method, it will not affect the original reference outside the method.

#### Example: Reassigning the Reference
```java
class Person {
    String name;
}

public class Main {
    public static void modifyReference(Person person) {
        person = new Person(); // Creates a new object
        person.name = "Alice"; // Modifies the new object
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.name = "Bob";

        modifyReference(person); // Pass by value of the reference
        System.out.println("Name after modification: " + person.name); // Output: Bob
    }
}
```

#### **Output**
```
Name after modification: Bob
```

#### **Explanation**
- A copy of the reference is passed to the `modifyReference` method.
- Reassigning `person` inside the method changes only the local copy of the reference, not the original reference outside the method.

---

### **Why Java is Always Pass by Value**

1. **Primitive Types**:
   - For primitives, the actual value is copied and passed to the method.

2. **Object References**:
   - For objects, the value being passed is the **reference** to the object (the memory address), not the object itself.
   - Since the reference is a value, any changes to the object it points to are visible outside the method, but reassigning the reference itself has no effect outside the method.

---

### **Common Misconceptions**

1. **Java is Pass by Reference for Objects**:
   - Incorrect. Java passes a copy of the reference (memory address) to the object, not the reference itself.

2. **Reassigning References Affects the Original**:
   - Reassigning a reference inside a method only affects the local copy of the reference and has no effect on the original reference.

---

### **Comparison of Pass by Value and Pass by Reference**

| **Aspect**             | **Pass by Value** in Java                          | **Pass by Reference** (Not in Java)            |
|------------------------|---------------------------------------------------|-----------------------------------------------|
| **Definition**          | A copy of the variable's value is passed.         | A reference (memory address) to the variable is passed. |
| **Effect on Original**  | Changes inside the method do not affect the original variable. | Changes inside the method directly affect the original variable. |
| **Supported in Java?**  | Yes                                               | No                                            |

---

### **Key Takeaways**

1. **Java is Pass by Value**:
   - Java **always** passes a copy of the variable to methods, regardless of whether it is a primitive or an object reference.

2. **Objects Are Handled Differently**:
   - For objects, a copy of the reference is passed. While you cannot reassign the reference, you can modify the object it points to.

3. **Immutable Objects**:
   - For immutable objects like `String` or `Integer`, modifying them inside a method creates a new object, and the original object remains unchanged.

---

### **Summary**

Java is always **Pass by Value**. This applies to both **primitives** and **object references**:
- For **primitives**, a copy of the actual value is passed.
- For **objects**, a copy of the reference (memory address) is passed, allowing modifications to the object's state but not to the reference itself.

## What is an Enum in Java
### **What is an Enum in Java?**

An **Enum** (short for **enumeration**) in Java is a special data type that represents a fixed set of constants. Enums are commonly used to define a collection of related constants that can be used throughout your program. They were introduced in **Java 5** as part of the `java.lang` package.

Enums provide a type-safe way of representing a group of predefined values, such as days of the week, months of the year, or directions (NORTH, SOUTH, EAST, WEST).

---

### **Key Characteristics of Enums**

1. **Fixed Set of Constants**:
   - An enum defines a fixed set of constants, which cannot be modified or extended.

2. **Type-Safe**:
   - Enums ensure type safety, meaning you cannot assign a value to an enum variable that is not part of the enum.

3. **Predefined Methods**:
   - Enums come with several built-in methods like `values()`, `ordinal()`, and `name()`.

4. **Custom Methods and Fields**:
   - Enums can have fields, constructors, and methods, allowing them to behave like classes.

5. **Implicitly `final` and `static`**:
   - Enum constants are implicitly `public`, `static`, and `final`.

6. **Cannot Be Extended**:
   - Enums cannot be extended since they implicitly extend `java.lang.Enum`.

---

### **Syntax**

```java
enum EnumName {
    CONSTANT1, CONSTANT2, CONSTANT3;
}
```

---

### **Simple Example: Days of the Week**

```java
enum Day {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
}

public class Main {
    public static void main(String[] args) {
        Day today = Day.MONDAY;
        System.out.println("Today is: " + today);

        // Loop through all enum constants
        for (Day day : Day.values()) {
            System.out.println(day);
        }
    }
}
```

#### **Output**
```
Today is: MONDAY
MONDAY
TUESDAY
WEDNESDAY
THURSDAY
FRIDAY
SATURDAY
SUNDAY
```

---

### **Enum Methods**

Enums provide several useful methods:

1. **`values()`**:
   - Returns an array of all enum constants.
   - Example:
     ```java
     for (Day day : Day.values()) {
         System.out.println(day);
     }
     ```

2. **`ordinal()`**:
   - Returns the index (0-based) of the constant in the enum declaration.
   - Example:
     ```java
     System.out.println(Day.MONDAY.ordinal()); // Output: 0
     ```

3. **`name()`**:
   - Returns the name of the constant as a `String`.
   - Example:
     ```java
     System.out.println(Day.MONDAY.name()); // Output: MONDAY
     ```

4. **`valueOf(String name)`**:
   - Converts a `String` to the corresponding enum constant.
   - Example:
     ```java
     Day day = Day.valueOf("TUESDAY");
     System.out.println(day); // Output: TUESDAY
     ```

---

### **Advanced Example: Enum with Fields and Methods**

Enums can have fields, constructors, and methods, just like classes.

```java
enum Day {
    MONDAY("Weekday"),
    TUESDAY("Weekday"),
    WEDNESDAY("Weekday"),
    THURSDAY("Weekday"),
    FRIDAY("Weekday"),
    SATURDAY("Weekend"),
    SUNDAY("Weekend");

    private String type;

    // Constructor
    Day(String type) {
        this.type = type;
    }

    // Getter for type
    public String getType() {
        return type;
    }
}

public class Main {
    public static void main(String[] args) {
        Day today = Day.FRIDAY;

        System.out.println("Today is: " + today);
        System.out.println("It is a: " + today.getType());

        for (Day day : Day.values()) {
            System.out.println(day + " is a " + day.getType());
        }
    }
}
```

#### **Output**
```
Today is: FRIDAY
It is a: Weekday
MONDAY is a Weekday
TUESDAY is a Weekday
WEDNESDAY is a Weekday
THURSDAY is a Weekday
FRIDAY is a Weekday
SATURDAY is a Weekend
SUNDAY is a Weekend
```

---

### **Enum in Switch Statements**

Enums can be directly used in `switch` statements, making them more readable and expressive.

```java
enum Day {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
}

public class Main {
    public static void main(String[] args) {
        Day today = Day.FRIDAY;

        switch (today) {
            case MONDAY:
                System.out.println("Start of the work week!");
                break;
            case FRIDAY:
                System.out.println("End of the work week!");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println("It's the weekend!");
                break;
            default:
                System.out.println("Midweek days.");
        }
    }
}
```

#### **Output**
```
End of the work week!
```

---

### **Enum with Abstract Methods**

Enums can also have abstract methods that each constant must implement.

#### Example:
```java
enum Operation {
    ADD {
        @Override
        public int apply(int a, int b) {
            return a + b;
        }
    },
    SUBTRACT {
        @Override
        public int apply(int a, int b) {
            return a - b;
        }
    },
    MULTIPLY {
        @Override
        public int apply(int a, int b) {
            return a * b;
        }
    },
    DIVIDE {
        @Override
        public int apply(int a, int b) {
            return a / b;
        }
    };

    // Abstract method
    public abstract int apply(int a, int b);
}

public class Main {
    public static void main(String[] args) {
        int a = 10, b = 5;

        for (Operation op : Operation.values()) {
            System.out.println(op + ": " + op.apply(a, b));
        }
    }
}
```

#### **Output**
```
ADD: 15
SUBTRACT: 5
MULTIPLY: 50
DIVIDE: 2
```

---

### **Advantages of Enums**

1. **Type Safety**:
   - Prevents invalid values from being used.
   - Example: You cannot assign a value to an enum variable that is not part of the enum.

2. **Readability**:
   - Improves code readability by using meaningful names for constants.

3. **Predefined Methods**:
   - Provides built-in methods like `values()`, `ordinal()`, and `name()` for easier manipulation.

4. **Scalability**:
   - Enums can be extended with fields, constructors, and methods to include additional functionality.

5. **Switch Statement Integration**:
   - Works seamlessly with `switch` statements, simplifying control flow.

---

### **Limitations of Enums**

1. **Cannot Extend Other Classes**:
   - Enums implicitly extend `java.lang.Enum`, so they cannot extend other classes.

2. **Fixed Set of Constants**:
   - Once defined, the set of constants in an enum cannot be dynamically changed.

3. **Not Ideal for Large Numbers of Constants**:
   - Using enums for a large number of constants can make code harder to maintain.

---

### **Common Use Cases**

1. **Constants**:
   - Representing days of the week, months of the year, etc.
2. **State Management**:
   - Tracking states like `STARTED`, `PAUSED`, `STOPPED` in a game or process.
3. **Type Definitions**:
   - Defining types like `SUCCESS`, `ERROR`, `WARNING` for messages.
4. **Switch Statements**:
   - Controlling program flow based on predefined values.

---

### **Conclusion**

Enums in Java are a powerful feature for managing a fixed set of constants in a type-safe and organized way. They go beyond simple constants by allowing additional methods, fields, and even abstract behavior. Understanding and using enums effectively can greatly improve code readability, maintainability, and correctness in Java applications.

## What is the Difference Between Serialization and Deserialization
### **Difference Between Serialization and Deserialization**

In Java, **serialization** and **deserialization** are mechanisms for converting an object into a form that can be stored or transmitted and then reconstructing it back into the original object.

- **Serialization**: The process of converting an object into a sequence of bytes for storage (e.g., in a file, database) or transmission (e.g., over a network).
- **Deserialization**: The process of reconstructing the object from its serialized form (sequence of bytes) back into a fully functional Java object.

---

### **Key Differences**

| **Aspect**              | **Serialization**                                    | **Deserialization**                                   |
|-------------------------|-----------------------------------------------------|-----------------------------------------------------|
| **Definition**           | Converts an object into a sequence of bytes.         | Converts the sequence of bytes back into an object. |
| **Purpose**              | Used for storing or transmitting objects.            | Used for retrieving or reconstructing objects.      |
| **Direction**            | Object → Sequence of Bytes                           | Sequence of Bytes → Object                          |
| **Process**              | Encodes the object state.                            | Decodes the serialized data into an object.         |
| **Common Use Cases**     | Storing data in files or databases, transferring over a network. | Reading serialized data from storage or a network.  |
| **Java Class Used**      | `ObjectOutputStream`                                 | `ObjectInputStream`                                 |

---

### **Serialization and Deserialization Process in Java**

Java provides built-in support for serialization and deserialization through the **`java.io.Serializable`** interface and classes like `ObjectOutputStream` and `ObjectInputStream`.

---

### **Steps to Serialize and Deserialize an Object**

#### **1. Serialization**
- To serialize an object:
  1. Implement the `Serializable` interface in the class.
  2. Use `ObjectOutputStream` to write the object to a file or other output stream.

#### **2. Deserialization**
- To deserialize an object:
  1. Use `ObjectInputStream` to read the serialized data.
  2. Cast the data back to the original object type.

---

### **Code Example: Serialization and Deserialization**

#### **Step 1: Create a Serializable Class**
```java
import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L; // Used for version control
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + "}";
    }
}
```

#### **Step 2: Serialize the Object**
```java
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerializeExample {
    public static void main(String[] args) {
        Person person = new Person("Alice", 30);

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person.ser"))) {
            oos.writeObject(person); // Serialize the object
            System.out.println("Serialization complete: " + person);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

#### **Output**
```
Serialization complete: Person{name='Alice', age=30}
```

#### **Step 3: Deserialize the Object**
```java
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeserializeExample {
    public static void main(String[] args) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("person.ser"))) {
            Person person = (Person) ois.readObject(); // Deserialize the object
            System.out.println("Deserialization complete: " + person);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

#### **Output**
```
Deserialization complete: Person{name='Alice', age=30}
```

---

### **Explanation of the Example**

1. **Serialization**:
   - The `Person` object is converted into a sequence of bytes and stored in a file named `person.ser`.
   - `ObjectOutputStream` is used to perform this task.

2. **Deserialization**:
   - The serialized data from the `person.ser` file is read, and the `Person` object is reconstructed.
   - `ObjectInputStream` is used for this.

---

### **Key Concepts and Details**

#### **1. `Serializable` Interface**
- The `Serializable` interface is a **marker interface** (i.e., it does not contain methods).
- It is used to indicate that a class can be serialized.

#### **2. `serialVersionUID`**
- A `serialVersionUID` is a unique identifier used during deserialization to verify that the sender and receiver of a serialized object have loaded classes that are compatible with each other.
- If `serialVersionUID` is not declared, Java generates one automatically. It is recommended to declare it explicitly.

#### **Example: `serialVersionUID`**
```java
private static final long serialVersionUID = 1L;
```

#### **3. Transient Keyword**
- Fields marked with the `transient` keyword are **not serialized**.
- These fields will not be included in the serialized form of the object.

```java
private transient String password; // This field will not be serialized
```

#### **Example of `transient` Field**
```java
import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String username;
    private transient String password; // Not serialized

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{username='" + username + "', password='" + password + "'}";
    }
}
```

---

### **Common Use Cases**

1. **Storing Object States**:
   - Save an object to a file or database for later use.

2. **Network Communication**:
   - Transmit objects over a network (e.g., in distributed systems or client-server communication).

3. **Deep Copy**:
   - Serialization and deserialization can be used to create deep copies of objects.

4. **Caching**:
   - Store serialized objects in memory for caching.

---

### **Advantages of Serialization**

1. **Persistence**:
   - Allows objects to be saved and retrieved, maintaining their state.

2. **Interoperability**:
   - Enables easy transmission of objects over networks or between JVMs.

3. **Simplifies Coding**:
   - Java handles most of the work for converting objects to byte streams and vice versa.

---

### **Disadvantages of Serialization**

1. **Performance Overhead**:
   - Serialization and deserialization can be resource-intensive.

2. **Security Risks**:
   - Serialized data can be tampered with if not encrypted, leading to potential vulnerabilities.

3. **Compatibility Issues**:
   - Changes in the class definition (e.g., removing fields) can cause deserialization failures if `serialVersionUID` is not properly managed.

4. **File Size**:
   - Serialized files can be large, consuming more storage.

---

### **Comparison of Serialization and Deserialization**

| **Aspect**             | **Serialization**                                    | **Deserialization**                                   |
|------------------------|-----------------------------------------------------|-----------------------------------------------------|
| **Purpose**            | Converts an object to a byte stream.                 | Converts a byte stream back into an object.         |
| **Direction**          | Object → Bytes                                       | Bytes → Object                                      |
| **Key Classes**        | `ObjectOutputStream`, `FileOutputStream`.            | `ObjectInputStream`, `FileInputStream`.             |
| **Required Interface** | `Serializable`                                       | Not required; relies on serialized data.            |

---

### **Best Practices**

1. **Use `serialVersionUID`**:
   - Always declare `serialVersionUID` to avoid compatibility issues.

2. **Secure Serialized Data**:
   - Use encryption if sensitive data is being serialized.

3. **Avoid Serialization of Sensitive Fields**:
   - Use the `transient` keyword to exclude sensitive fields like passwords.

4. **Test for Compatibility**:
   - Ensure compatibility when deserializing objects across different versions of the class.

---

### **Conclusion**

**Serialization** and **deserialization** are powerful mechanisms for persisting and transmitting Java objects. While serialization converts objects into byte streams for storage or transmission, deserialization restores those byte streams back into usable objects. Proper understanding of these concepts, along with best practices, ensures efficient and secure use of these features in Java applications.

## What is the Difference Between SerialUUID and RandomUUID
### **Difference Between `SerialVersionUID` and `RandomUUID` in Java**

Both **`SerialVersionUID`** and **`RandomUUID`** are used in Java but serve entirely different purposes and are unrelated concepts.

---

### **1. `SerialVersionUID`**

#### **Definition**
- **`SerialVersionUID`** is a unique identifier for a class during the process of **serialization** and **deserialization** in Java.
- It ensures that a serialized object is compatible with the class definition at the time of deserialization.

---

#### **Purpose**
- Maintains compatibility of serialized objects with their class definitions across different versions of the class.
- If the `SerialVersionUID` of the serialized object and the class definition do not match, a `InvalidClassException` is thrown during deserialization.

---

#### **Key Features**
1. **Static and Final**:
   - `SerialVersionUID` must be declared as `static final long`.
   ```java
   private static final long serialVersionUID = 1L;
   ```

2. **Generated Automatically**:
   - If not explicitly defined, the Java compiler generates a default `SerialVersionUID` based on class details (like fields and methods). However, this generated value can change if the class structure changes.

3. **Custom Versioning**:
   - Developers often define their own `SerialVersionUID` to avoid compatibility issues when class definitions evolve.

---

#### **Example: `SerialVersionUID`**
```java
import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L; // Custom version
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
```

#### **What Happens Without `SerialVersionUID`?**
If the class structure changes (e.g., adding or removing fields), the automatically generated `SerialVersionUID` changes, and previously serialized objects may fail to deserialize.

---

#### **Key Use Cases**
- Ensuring compatibility during serialization/deserialization of objects.
- Avoiding deserialization errors when a class evolves over time.

---

---

### **2. `RandomUUID`**

#### **Definition**
- **`RandomUUID`** is a universally unique identifier (UUID) generated randomly. It is used for generating globally unique identifiers.

---

#### **Purpose**
- Provides a **unique identifier** that is not tied to serialization.
- Useful for scenarios where a random and globally unique value is required, such as:
  - Unique transaction IDs
  - Session IDs
  - Unique database keys

---

#### **Key Features**
1. **Globally Unique**:
   - A `UUID` is a 128-bit number that is highly unlikely to collide with another UUID.
   - It is typically represented as a 36-character string (32 hexadecimal digits and 4 hyphens).

2. **Generated Using `UUID.randomUUID()`**:
   - The `java.util.UUID` class provides the `randomUUID()` method to generate a new random UUID.

---

#### **Example: `RandomUUID`**
```java
import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        // Generate a random UUID
        UUID uuid = UUID.randomUUID();
        System.out.println("Random UUID: " + uuid);
    }
}
```

#### **Output**
```
Random UUID: 3f8c79a5-3a2d-4d6c-86cc-8b7d9fa67f89
```

---

#### **Key Use Cases**
- Generating unique identifiers for objects, transactions, or database entries.
- Session tracking in web applications.
- Distributed systems where unique identifiers are required across nodes or systems.

---

---

### **Comparison: `SerialVersionUID` vs `RandomUUID`**

| **Aspect**             | **`SerialVersionUID`**                                   | **`RandomUUID`**                                   |
|------------------------|---------------------------------------------------------|--------------------------------------------------|
| **Purpose**            | Ensures compatibility during serialization and deserialization. | Generates globally unique identifiers.            |
| **Type**               | A `long` value (static and final).                      | A 128-bit unique identifier.                     |
| **Generated By**       | Explicitly declared by the developer or automatically generated by the JVM. | Generated using `UUID.randomUUID()`.             |
| **Usage**              | Serialization and deserialization.                     | Unique identifiers for objects, transactions, or distributed systems. |
| **Scope**              | Class-level compatibility.                              | Globally unique values across systems.           |
| **Example Value**      | `1L` or `123456789L`                                    | `3f8c79a5-3a2d-4d6c-86cc-8b7d9fa67f89`           |

---

### **When to Use Each**

1. **Use `SerialVersionUID`**:
   - When implementing the `Serializable` interface.
   - To ensure serialized objects are compatible across different versions of a class.

2. **Use `RandomUUID`**:
   - When you need a globally unique identifier for objects, transactions, or systems.
   - In distributed systems to ensure identifiers are unique across nodes.

---

### **Conclusion**

- **`SerialVersionUID`** is specific to Java's serialization mechanism and ensures compatibility of serialized objects with their class definitions.
- **`RandomUUID`** is a general-purpose unique identifier that is not tied to serialization but is instead used to generate unique values for various use cases.

While both deal with unique identifiers, they are used in entirely different contexts and should not be confused.

## What is Externalization in Java
### **What is Externalization in Java?**

**Externalization** in Java is a more flexible alternative to **serialization** that allows the programmer to control how an object's state is serialized and deserialized. It provides a way to implement custom serialization logic, which can be useful when you want to optimize the serialization process or manage how the data is represented.

Java provides an interface called **`java.io.Externalizable`** that must be implemented by a class if you want to use externalization. Unlike the `Serializable` interface, which automatically serializes all fields of a class, the `Externalizable` interface gives you full control over the process.

---

### **Key Points of Externalization**

1. **Custom Serialization**:
   - With externalization, you can define exactly how an object is written to or read from a stream. You have control over which fields to serialize, how to serialize them, and how to handle object state during deserialization.

2. **Interface**:
   - The class must implement the `Externalizable` interface, which extends `java.io.Serializable` but adds two methods:
     - **`writeExternal(ObjectOutput out)`**: This method is used to serialize the object's data.
     - **`readExternal(ObjectInput in)`**: This method is used to deserialize the object's data.

3. **Performance**:
   - Externalization may provide better performance for large or complex objects because you can control the exact data being serialized and avoid unnecessary serialization of fields.

4. **Control over Serialization**:
   - You can choose to serialize only specific fields or modify the representation of the object's state during serialization and deserialization.

---

### **Methods in `Externalizable` Interface**

1. **`writeExternal(ObjectOutput out)`**:
   - This method is used to write the state of the object to an output stream.
   - It is up to the developer to specify which fields of the object to serialize.

2. **`readExternal(ObjectInput in)`**:
   - This method is used to read the state of the object from the input stream.
   - You manually read the serialized data and set the object's fields.

---

### **Steps to Use Externalization**

1. Implement the `Externalizable` interface in your class.
2. Implement the `writeExternal()` method to specify how to write the object’s state to a stream.
3. Implement the `readExternal()` method to specify how to read the object’s state from a stream.

---

### **Example of Externalization in Java**

#### **Step 1: Create the `Externalizable` Class**

```java
import java.io.*;

public class Person implements Externalizable {
    private String name;
    private int age;

    // No-argument constructor required for Externalizable
    public Person() {
        System.out.println("Person Constructor Called");
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Implement the writeExternal method
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(name);  // Write 'name' to the stream
        out.writeInt(age);      // Write 'age' to the stream
    }

    // Implement the readExternal method
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        name = (String) in.readObject();  // Read 'name' from the stream
        age = in.readInt();               // Read 'age' from the stream
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + "}";
    }
}
```

#### **Step 2: Serialize and Deserialize the Object Using Externalization**

```java
import java.io.*;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Alice", 30);

        // Serialization
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("person.ser"))) {
            person.writeExternal(out);  // Serialize using externalization
            System.out.println("Serialization complete: " + person);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Deserialization
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("person.ser"))) {
            Person newPerson = new Person();  // Create a new object
            newPerson.readExternal(in);       // Deserialize using externalization
            System.out.println("Deserialization complete: " + newPerson);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```

#### **Output**
```
Person Constructor Called
Serialization complete: Person{name='Alice', age=30}
Person Constructor Called
Deserialization complete: Person{name='Alice', age=30}
```

#### **Explanation of the Example**
- **Serialization**: The `Person` object is serialized using the `writeExternal()` method, where we explicitly specify which fields (`name` and `age`) should be written to the output stream.
- **Deserialization**: A new `Person` object is created, and the `readExternal()` method reads the serialized data and assigns values to the `name` and `age` fields.

---

### **Advantages of Externalization**

1. **Full Control**:
   - Externalization allows you to have full control over how the object's state is serialized and deserialized. You can decide which fields to serialize and how to serialize them.

2. **Performance Optimization**:
   - You can optimize the serialization process by only serializing the necessary data, which can be especially useful for large objects.

3. **Custom Serialization**:
   - You can implement custom logic for serializing complex objects, or handle versioning explicitly (e.g., skipping or renaming fields during deserialization).

---

### **Disadvantages of Externalization**

1. **Manual Effort**:
   - Since you have to explicitly handle the serialization process, it requires more effort and can lead to more complex code compared to using `Serializable`, where the process is automated.

2. **No Default Serialization**:
   - Unlike `Serializable`, where the serialization is handled automatically, with `Externalizable` you have to manage the entire process manually, including writing and reading all the fields.

3. **No Automatic Versioning**:
   - `Serializable` automatically provides version control using `serialVersionUID`. With `Externalizable`, the programmer needs to handle versioning explicitly.

---

### **When to Use Externalization**

- **Custom Serialization**: When you need to have full control over the serialization process, including which fields are serialized, how the fields are represented, and how data is managed during serialization.
- **Performance Critical Applications**: When serialization needs to be optimized, and you only want to serialize certain fields or manage how the data is stored.
- **Complex Data Structures**: When the object to be serialized contains complex fields, and you need to implement custom logic to manage how these fields are serialized.

---

### **Conclusion**

**Externalization** is a more advanced and flexible way of managing object serialization in Java. It provides the programmer with complete control over how objects are serialized and deserialized, which can be useful for optimizing performance or handling custom serialization logic. However, it requires more effort and is generally used when you need that level of control, whereas **`Serializable`** is simpler and is often used in most cases when automatic serialization is sufficient.

## What is the Difference Between Runnable, Clonable, and Callable in Java
### **Difference Between `Runnable`, `Cloneable`, and `Callable` in Java**

In Java, **`Runnable`**, **`Cloneable`**, and **`Callable`** are interfaces that serve different purposes:

1. **`Runnable`**: Used for defining a task that can be executed by a thread.
2. **`Cloneable`**: Used for creating a copy (or "clone") of an object.
3. **`Callable`**: Used for defining a task that can be executed by a thread and returns a result.

---

### **1. `Runnable` Interface**

#### **Purpose**
- The `Runnable` interface is used to define a task that can be executed by a thread. It is part of the **`java.lang`** package.

---

#### **Key Features**
1. **Single Abstract Method**:
   - The interface has a single method: `void run()`.
   - It is a functional interface, meaning it can also be used with lambda expressions.

2. **No Return Value**:
   - The `run()` method does not return a result.

3. **Thread Execution**:
   - The task defined in the `run()` method can be executed by a thread.

---

#### **Syntax**
```java
public interface Runnable {
    void run();
}
```

---

#### **Example**
```java
public class RunnableExample implements Runnable {
    @Override
    public void run() {
        System.out.println("Thread is running using Runnable!");
    }

    public static void main(String[] args) {
        RunnableExample runnableTask = new RunnableExample();
        Thread thread = new Thread(runnableTask);
        thread.start(); // Starts the thread
    }
}
```

#### **Output**
```
Thread is running using Runnable!
```

---

### **2. `Cloneable` Interface**

#### **Purpose**
- The `Cloneable` interface is used to create a **copy (or "clone")** of an object. It is part of the **`java.lang`** package.

---

#### **Key Features**
1. **Marker Interface**:
   - `Cloneable` is a **marker interface** (an interface with no methods).
   - It is used to signal to the JVM that the `Object.clone()` method is safe to invoke on an object.

2. **Object Cloning**:
   - If a class implements `Cloneable`, you can use the `clone()` method to create a copy of the object.
   - The class must override the `clone()` method to make it accessible (as `clone()` in `Object` is protected).

3. **Shallow Copy**:
   - The default `clone()` implementation in the `Object` class performs a shallow copy (field values are copied, but references to objects are not deep-copied).

---

#### **Syntax**
```java
public interface Cloneable {
    // Marker interface with no methods
}
```

---

#### **Example**
```java
class Person implements Cloneable {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); // Calls Object's clone method
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + "}";
    }
}

public class CloneableExample {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person1 = new Person("Alice", 30);
        Person person2 = (Person) person1.clone(); // Cloning person1

        System.out.println("Original: " + person1);
        System.out.println("Clone: " + person2);
    }
}
```

#### **Output**
```
Original: Person{name='Alice', age=30}
Clone: Person{name='Alice', age=30}
```

---

### **3. `Callable` Interface**

#### **Purpose**
- The `Callable` interface is used to define a task that can return a result or throw an exception. It is part of the **`java.util.concurrent`** package.

---

#### **Key Features**
1. **Single Abstract Method**:
   - The interface has a single method: `V call() throws Exception`.
   - The `call()` method returns a result of type `V`.

2. **Used with Executors**:
   - The `Callable` interface is commonly used with the **Executor Framework**, which manages thread execution and retrieves results using `Future`.

3. **Supports Checked Exceptions**:
   - Unlike `Runnable`, the `call()` method can throw checked exceptions.

---

#### **Syntax**
```java
public interface Callable<V> {
    V call() throws Exception;
}
```

---

#### **Example**
```java
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableExample implements Callable<String> {
    @Override
    public String call() throws Exception {
        return "Task executed using Callable!";
    }

    public static void main(String[] args) throws Exception {
        CallableExample callableTask = new CallableExample();

        // Create an ExecutorService to manage threads
        ExecutorService executor = Executors.newSingleThreadExecutor();

        // Submit the Callable task and get a Future object
        Future<String> result = executor.submit(callableTask);

        // Get the result from the Future object
        System.out.println("Result: " + result.get());

        // Shutdown the executor
        executor.shutdown();
    }
}
```

#### **Output**
```
Result: Task executed using Callable!
```

---

### **Comparison Table: `Runnable`, `Cloneable`, and `Callable`**

| **Aspect**            | **`Runnable`**                          | **`Cloneable`**                     | **`Callable`**                     |
|-----------------------|------------------------------------------|--------------------------------------|-------------------------------------|
| **Purpose**           | Defines a task to be executed by a thread. | Enables object cloning.             | Defines a task that returns a result or throws exceptions. |
| **Key Method**        | `void run()`                             | No methods (marker interface).       | `V call() throws Exception`         |
| **Return Value**      | Does not return a value (`void`).         | Not applicable.                      | Returns a value of type `V`.        |
| **Exception Handling**| Cannot throw checked exceptions.         | Not applicable.                      | Can throw checked exceptions.       |
| **Package**           | `java.lang`                              | `java.lang`                          | `java.util.concurrent`              |
| **Use Case**          | Used for defining tasks for threads.     | Used for creating object copies.     | Used for tasks requiring a result.  |
| **Multithreading**    | Used with `Thread` or Executor.          | Not used in multithreading.          | Used with Executor Framework.       |

---

### **Key Differences Between `Runnable` and `Callable`**

| **Aspect**            | **`Runnable`**                          | **`Callable`**                     |
|-----------------------|------------------------------------------|-------------------------------------|
| **Return Value**      | Does not return a value (`void`).         | Returns a result of type `V`.       |
| **Exception Handling**| Cannot throw checked exceptions.         | Can throw checked exceptions.       |
| **Use with Executors**| Can be used with `ExecutorService`.       | Requires `ExecutorService` to handle results. |
| **Common Usage**      | Simple tasks that don’t return a result.  | Tasks that return results or handle exceptions. |

---

### **When to Use Each**

1. **`Runnable`**:
   - Use when you need to define a task that does not return a result or throw exceptions.
   - Example: A background thread to log messages.

2. **`Cloneable`**:
   - Use when you need to create a copy of an object.
   - Example: Cloning a configuration object for testing purposes.

3. **`Callable`**:
   - Use when you need to define a task that returns a result or may throw exceptions.
   - Example: Fetching data from a database or an API in a background thread.

---

### **Conclusion**

- **`Runnable`**: Defines a task for threads without returning a result.
- **`Cloneable`**: Provides a mechanism to clone objects.
- **`Callable`**: Defines tasks for threads that return results and handle exceptions.

Understanding the distinctions and appropriate use cases for these interfaces is crucial for designing efficient and maintainable Java applications.

## What is the Difference Between Deep Cloning and Shallow Cloning in Java
### **Difference Between Deep Cloning and Shallow Cloning in Java**

In Java, **cloning** refers to creating an exact copy of an object. Depending on the way the copy is created, it can be categorized into **shallow cloning** and **deep cloning**. These two types of cloning differ in how they handle the copying of object references.

---

### **Key Differences**

| **Aspect**              | **Shallow Cloning**                                | **Deep Cloning**                                |
|-------------------------|----------------------------------------------------|------------------------------------------------|
| **Definition**          | Creates a copy of the object, but not its referenced objects. | Creates a copy of the object and all its referenced objects. |
| **Object References**   | Copies references for non-primitive fields (not the actual objects). | Creates a new copy of all referenced objects (recursively). |
| **Impact on Original**  | Changes to mutable fields in the clone affect the original object. | Changes to the clone do not affect the original object. |
| **Performance**         | Faster, as it does not recursively copy referenced objects. | Slower, as it involves deep recursive copying of all referenced objects. |
| **Implementation Complexity** | Easier to implement (default behavior of `clone()` in `Object`). | More complex to implement, requiring explicit handling of nested objects. |

---

### **1. Shallow Cloning**

#### **Definition**
- Shallow cloning creates a copy of the object but **does not clone the objects that the original object references**. Instead, it copies references to those objects.
- The default implementation of the `clone()` method in the `Object` class performs shallow cloning.

---

#### **Example of Shallow Cloning**

```java
class Address {
    String city;

    public Address(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{city='" + city + "'}";
    }
}

class Person implements Cloneable {
    String name;
    Address address;

    public Person(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); // Shallow clone
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', address=" + address + "}";
    }
}

public class ShallowCloneExample {
    public static void main(String[] args) throws CloneNotSupportedException {
        Address address = new Address("New York");
        Person person1 = new Person("Alice", address);

        // Perform shallow cloning
        Person person2 = (Person) person1.clone();

        // Modify the address in the cloned object
        person2.address.city = "Los Angeles";

        System.out.println("Original: " + person1);
        System.out.println("Clone: " + person2);
    }
}
```

#### **Output**
```
Original: Person{name='Alice', address=Address{city='Los Angeles'}}
Clone: Person{name='Alice', address=Address{city='Los Angeles'}}
```

#### **Explanation**
- The `address` field is shared between `person1` and `person2` because only the reference is copied during shallow cloning.
- Changes to `person2.address.city` affect the `address` of `person1`.

---

### **2. Deep Cloning**

#### **Definition**
- Deep cloning creates a copy of the object along with copies of all the objects it references, ensuring complete independence between the original and the cloned object.
- To implement deep cloning, you must explicitly clone each referenced object.

---

#### **Example of Deep Cloning**

```java
class Address {
    String city;

    public Address(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{city='" + city + "'}";
    }

    // Method for deep cloning
    public Address clone() {
        return new Address(this.city);
    }
}

class Person implements Cloneable {
    String name;
    Address address;

    public Person(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // Perform deep cloning
        Person clonedPerson = (Person) super.clone();
        clonedPerson.address = this.address.clone(); // Clone the nested object
        return clonedPerson;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', address=" + address + "}";
    }
}

public class DeepCloneExample {
    public static void main(String[] args) throws CloneNotSupportedException {
        Address address = new Address("New York");
        Person person1 = new Person("Alice", address);

        // Perform deep cloning
        Person person2 = (Person) person1.clone();

        // Modify the address in the cloned object
        person2.address.city = "Los Angeles";

        System.out.println("Original: " + person1);
        System.out.println("Clone: " + person2);
    }
}
```

#### **Output**
```
Original: Person{name='Alice', address=Address{city='New York'}}
Clone: Person{name='Alice', address=Address{city='Los Angeles'}}
```

#### **Explanation**
- In deep cloning, the `address` object is also cloned, creating a new instance for the `address` field in `person2`.
- Changes to `person2.address.city` do not affect the `address` field of `person1`.

---

### **Comparison Table**

| **Aspect**              | **Shallow Cloning**                                | **Deep Cloning**                                |
|-------------------------|----------------------------------------------------|------------------------------------------------|
| **Definition**          | Creates a copy of the object, but not its referenced objects. | Creates a copy of the object and all its referenced objects. |
| **References**          | Shared between the original and cloned objects.    | Independent copies for all referenced objects. |
| **Implementation**      | Uses `super.clone()` from the `Object` class.       | Requires manual implementation of `clone()` for nested objects. |
| **Impact on Original**  | Changes in mutable fields of the clone reflect in the original object. | Changes in the clone do not affect the original object. |
| **Performance**         | Faster as it does not recursively copy objects.    | Slower as it involves deep recursive copying.  |
| **Use Case**            | Suitable for objects with immutable fields.        | Suitable for objects with mutable or nested fields. |

---

### **When to Use Shallow Cloning vs Deep Cloning**

1. **Use Shallow Cloning When**:
   - The object contains only primitive fields or immutable objects (e.g., `String`, `Integer`).
   - You don't need to modify the referenced objects.

   **Example**: Cloning configuration objects that contain only immutable fields.

2. **Use Deep Cloning When**:
   - The object contains nested mutable objects or complex structures.
   - You need to ensure complete independence between the original and the cloned object.

   **Example**: Cloning data structures like trees or graphs.

---

### **Best Practices**

1. **Shallow Cloning**:
   - Use shallow cloning when you know that modifying the cloned object's state will not affect the original object.

2. **Deep Cloning**:
   - Use deep cloning if you want complete independence between the original and the cloned object, especially when working with mutable fields.

3. **Use Libraries for Complex Objects**:
   - For deeply nested or complex objects, consider using libraries like **Apache Commons Lang** (`SerializationUtils`) or **GSON** for deep cloning.

---

### **Conclusion**

- **Shallow Cloning** is faster and easier to implement but only creates a partial copy of the object. Referenced objects are shared, making changes in one object reflect in the other.
- **Deep Cloning** ensures complete independence by creating a new copy of the object and all referenced objects. However, it requires more effort and is slower due to recursive copying.

Choosing between shallow and deep cloning depends on the specific requirements of your application and the structure of the objects being cloned.

## What are Java Keywords
### **What Are Java Keywords?**

**Java keywords** are reserved words that have a predefined meaning in the Java language. These keywords cannot be used as identifiers (e.g., variable names, method names, class names). They are part of the syntax and are used to define the structure and behavior of the Java program.

---

### **Complete List of Java Keywords**

Here is a comprehensive list of all 52 keywords in Java, categorized for clarity, along with explanations and examples:

---

#### **1. Access Modifiers**
These keywords define the access level of classes, methods, and variables.

| **Keyword** | **Description**                                      | **Example**                                                                                 |
|-------------|------------------------------------------------------|---------------------------------------------------------------------------------------------|
| `public`    | Makes the member accessible from anywhere.           | `public class MyClass {}`                                                                   |
| `protected` | Accessible within the same package and subclasses.   | `protected int age;`                                                                        |
| `private`   | Accessible only within the same class.               | `private String name;`                                                                      |

---

#### **2. Control Flow Statements**
These keywords are used to control the flow of the program.

| **Keyword** | **Description**                                             | **Example**                                                                 |
|-------------|-------------------------------------------------------------|-----------------------------------------------------------------------------|
| `if`        | Executes a block of code if the condition is true.          | `if (x > 0) { System.out.println("Positive"); }`                           |
| `else`      | Executes a block of code if the `if` condition is false.    | `else { System.out.println("Negative"); }`                                 |
| `switch`    | Executes one case based on the value of a variable.         | `switch (x) { case 1: break; default: break; }`                            |
| `case`      | Defines a block of code in a `switch` statement.            | `case 1: System.out.println("One"); break;`                                |
| `default`   | Executes if no `case` matches in a `switch` statement.      | `default: System.out.println("None"); break;`                              |
| `while`     | Executes a block of code repeatedly while the condition is true. | `while (x < 10) { x++; }`                                                |
| `do`        | Executes a block of code at least once before checking the condition. | `do { x++; } while (x < 10);`                                        |
| `for`       | Executes a block of code a fixed number of times.           | `for (int i = 0; i < 10; i++) { System.out.println(i); }`                  |
| `break`     | Exits the loop or `switch` statement immediately.           | `break;`                                                                   |
| `continue`  | Skips the current iteration of a loop and moves to the next. | `continue;`                                                                |
| `return`    | Exits from a method and optionally returns a value.         | `return x + y;`                                                            |

---

#### **3. Primitive Data Types**
These keywords define the type of data variables can hold.

| **Keyword** | **Description**                                   | **Example**               |
|-------------|---------------------------------------------------|---------------------------|
| `int`       | Integer type (4 bytes).                          | `int age = 25;`           |
| `float`     | Floating-point type (4 bytes).                   | `float weight = 68.5f;`   |
| `double`    | Double-precision floating-point type (8 bytes).  | `double pi = 3.14159;`    |
| `char`      | Single character type (2 bytes).                 | `char initial = 'A';`     |
| `boolean`   | Represents true/false values.                    | `boolean isActive = true;`|
| `byte`      | Integer type (1 byte).                           | `byte smallNum = 127;`    |
| `short`     | Integer type (2 bytes).                          | `short num = 32000;`      |
| `long`      | Integer type (8 bytes).                          | `long largeNum = 100000L;`|

---

#### **4. Class-Related Keywords**
These keywords define and interact with classes and objects.

| **Keyword**  | **Description**                                                 | **Example**                              |
|--------------|-----------------------------------------------------------------|------------------------------------------|
| `class`      | Defines a class.                                                | `class MyClass {}`                       |
| `interface`  | Defines an interface.                                           | `interface MyInterface {}`               |
| `extends`    | Inherits from a parent class.                                   | `class Dog extends Animal {}`            |
| `implements` | Implements an interface.                                        | `class Dog implements Pet {}`            |
| `new`        | Creates new objects.                                            | `Dog myDog = new Dog();`                 |
| `this`       | Refers to the current object.                                   | `this.name = name;`                      |
| `super`      | Refers to the parent class.                                     | `super.display();`                       |

---

#### **5. Exception Handling Keywords**
These keywords handle errors and exceptions.

| **Keyword** | **Description**                                          | **Example**                                    |
|-------------|----------------------------------------------------------|------------------------------------------------|
| `try`       | Defines a block of code to test for exceptions.          | `try { int x = 10 / 0; }`                     |
| `catch`     | Defines a block to handle exceptions.                    | `catch (Exception e) { e.printStackTrace(); }`|
| `finally`   | Defines a block to execute after `try` or `catch`.       | `finally { System.out.println("Done"); }`     |
| `throw`     | Used to explicitly throw an exception.                   | `throw new Exception("Error!");`             |
| `throws`    | Declares exceptions that a method can throw.             | `void method() throws IOException {}`        |

---

#### **6. Modifiers**
These keywords modify the behavior or properties of classes, methods, and variables.

| **Keyword**   | **Description**                                       | **Example**                          |
|---------------|-------------------------------------------------------|--------------------------------------|
| `static`      | Defines shared members for all instances of a class.  | `static int count;`                 |
| `final`       | Prevents modification (variable, method, or class).   | `final int MAX = 100;`              |
| `abstract`    | Defines an abstract class or method.                  | `abstract class Shape {}`           |
| `synchronized`| Controls thread access to blocks or methods.          | `synchronized void method() {}`     |
| `volatile`    | Indicates a variable's value may change asynchronously. | `volatile int counter;`            |

---

#### **7. Package-Related Keywords**
These keywords organize classes and interfaces into packages.

| **Keyword** | **Description**                      | **Example**                       |
|-------------|--------------------------------------|-----------------------------------|
| `package`   | Defines the package of a class.      | `package mypackage;`             |
| `import`    | Imports classes or entire packages.  | `import java.util.Scanner;`      |

---

#### **8. Other Keywords**
These keywords perform various functions.

| **Keyword**     | **Description**                                                        | **Example**                 |
|------------------|------------------------------------------------------------------------|-----------------------------|
| `instanceof`     | Tests if an object is an instance of a specific class or subclass.     | `if (obj instanceof Dog)`  |
| `null`           | Represents the null value.                                            | `Object obj = null;`       |
| `enum`           | Defines an enumerated type.                                           | `enum Day { MON, TUE }`    |
| `assert`         | Tests assumptions during runtime.                                     | `assert x > 0 : "Error!";` |
| `transient`      | Prevents serialization of a field.                                    | `transient int id;`        |
| `native`         | Specifies a method implemented in native code (e.g., C, C++).        | `native void method();`    |
| `strictfp`       | Enforces strict floating-point calculations.                          | `strictfp class MathOps {}`|

---

### **Examples Using Multiple Keywords**

#### **Example 1: Using Access Modifiers, Primitive Types, and Control Flow**
```java
public class Main {
    public static void main(String[] args) {
        int x = 10;

        if (x > 0) {
            System.out.println("Positive Number");
        } else {
            System.out.println("Negative Number");
        }
    }
}
```

---

#### **Example 2: Using `class`, `extends`, and `override`**
```java
class Animal {
    void sound() {
        System.out.println("Animal makes a sound.");
    }
}

class Dog extends Animal {
    @Override
    void sound() {
        System.out.println("Dog barks.");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myDog = new Dog();
        myDog.sound();
    }
}
```

---

### **Summary**

Java has 52 reserved keywords that are integral to its syntax and functionality. These keywords define the structure, control flow, data types, exception handling, access, and other essential aspects of Java programming. Understanding these keywords is fundamental to mastering Java.