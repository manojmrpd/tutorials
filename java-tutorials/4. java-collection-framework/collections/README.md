

# Java Collection Framework

## Table of Contents

1. [Array vs Collection](#array-vs-collection)
2. [Queue](#queue)
   - [PriorityQueue](#priorityqueue)
   - [PriorityBlockingQueue](#priorityblockingqueue)
3. [Deque](#deque)
   - [ArrayDeque](#arraydeque)
   - [ConcurrentLinkedQueue](#concurrentlinkedqueue)
4. [List](#list)
   - [ArrayList](#arraylist)
   - [LinkedList](#linkedlist)
   - [Vector](#vector)
   - [Stack](#stack)
   - [CopyOnWriteArrayList vs CopyOnWriteArraySet](#copyonwritearraylist-vs-copyonwritearrayset)
5. [Set](#set)
   - [HashSet](#hashset)
   - [LinkedHashSet](#linkedhashset)
   - [SortedSet vs NavigableSet vs TreeSet](#sortedset-vs-navigableset-vs-treeset)
   - [ConcurrentSkipListSet](#concurrentskiplistset)
6. [Map](#map)
   - [Hashtable](#hashtable)
   - [HashMap](#hashmap)
   - [LinkedHashMap](#linkedhashmap)
   - [SortedMap vs NavigableMap vs TreeMap](#sortedmap-vs-navigablemap-vs-treemap)
   - [ConcurrentHashMap](#concurrenthashmap)
   - [ConcurrentSkipListMap](#concurrentskiplistmap)
   - [EnumMap vs IdentityHashMap vs WeakHashMap](#enummap-vs-identityhashmap-vs-weakhashmap)
7. [Data Structure Concepts](#data-structure-concepts)
   - [Internal working of LinkedList](#internal-working-of-linkedlist)
   - [Internal working of HashMap](#internal-working-of-hashmap)
   - [Internal working of ConcurrentHashMap](#internal-working-of-concurrenthashmap)
   - [Internal working of TreeMap](#internal-working-of-treemap)
   - [Resizable Array](#resizable-array)
   - [Skip List Data Structure](#skip-list-data-structure)
   - [Single vs Doubly LinkedList](#single-vs-doubly-linkedlist)
   - [Hashing/Re-hashing](#hashingre-hashing)
   - [Initial Capacity vs Load Factor](#initial-capacity-vs-load-factor)
   - [Hash collision - Chaining vs Open addressing](#hash-collision---chaining-vs-open-addressing)
   - [Treefication / Treefy Threshold](#treefication--treefy-threshold)
   - [Red-black tree / Self-balanced binary search tree](#red-black-tree--self-balanced-binary-search-tree)
   - [Compare and Swap (CAS) algorithm](#compare-and-swap-cas-algorithm)
   - [Segment locking in ConcurrentHashMap](#segment-locking-in-concurrenthashmap)
   - [FIFO vs LIFO principles](#fifo-vs-lifo-principles)
   - [Access Order of LinkedHashMap](#access-order-of-linkedhashmap)
8. [Iterator vs ListIterator vs Enumeration](#iterator-vs-listiterator-vs-enumeration)
9. [Comparator vs Comparable](#comparator-vs-comparable)
10. [Time-complexity (BigO-Notation) of all collection methods](#time-complexity-bigo-notation-of-all-collection-methods)
11. [Sorting vs Searching vs Ordering](#sorting-vs-searching-vs-ordering)
12. [Null and Duplicate Behaviour of all Collection classes](#null-and-duplicate-behaviour-of-all-Collection-classes)
13. [Fail-fast vs Fail-safe Iterator](#fail-fast-vs-fail-safe-iterator)
14. [How to avoid ConcurrentModificationException](#how-to-avoid-concurrentmodificationexception)
15. [How to avoid Thread race condition](#how-to-avoid-thread-race-condition)
16. [Synchronized vs Concurrent Collections](#synchronized-vs-concurrent-collections)
17. [Collection vs Collections](#collection-vs-collections)
18. [hashCode() and equals() method contract](#hashcode-and-equals-method-contract)

# Array Vs Collection
Here's a tabular comparison between arrays and collections in Java:

| Feature                  | Array                                     | Collection                                |
|--------------------------|-------------------------------------------|-------------------------------------------|
| Size                     | Fixed                                     | Dynamic                                   |
| Element Type             | Homogeneous                               | Heterogeneous                             |
| Access Time              | O(1) (direct indexing)                    | O(1) to O(log n) depending on type        |
| Functionality            | Limited                                   | Rich (search, sort, resize, etc.)         |
| Memory Usage             | Efficient                                 | Can be less efficient due to overhead     |
| Data Structures          | Single (array)                            | Multiple (List, Set, Queue, Map, etc.)    |
| Use Case                 | Simple, fixed-size collections            | Flexible, dynamic-size collections        |

This table provides a concise comparison of the key characteristics of arrays and collections in Java, helping you understand their differences and choose the appropriate data structure for your specific needs.

# Queue
A `Queue` in Java represents a collection of elements arranged in a specific order, typically following the "first-in, first-out" (FIFO) principle. It provides methods for adding elements to the end of the queue, removing elements from the front of the queue, and inspecting the element at the front of the queue without removing it. Here's an overview:

### Queue Interface:

1. **FIFO Principle:**
   - A `Queue` follows the "first-in, first-out" principle, where elements are added to the end of the queue (enqueue) and removed from the front of the queue (dequeue).

2. **Unbounded Size:**
   - `Queue` implementations do not have a fixed size limit by default. They can dynamically grow as needed to accommodate additional elements.

3. **Element Retrieval:**
   - In addition to adding and removing elements, `Queue` provides methods for peeking at the element at the front of the queue without removing it and checking if the queue is empty.

4. **Operations:**
   - `enqueue(element)`: Adds the specified element to the end of the queue.
   - `dequeue()`: Removes and returns the element at the front of the queue.
   - `peek()`: Returns the element at the front of the queue without removing it.
   - `isEmpty()`: Returns true if the queue contains no elements, false otherwise.
   - `size()`: Returns the number of elements in the queue.

### Common Implementations of Queue:

1. **LinkedList:**
   - Implements the `Queue` interface using a doubly linked list.
   - Provides efficient FIFO operations and allows null elements.
   - Suitable for most queue operations and provides flexibility for other list operations.

2. **ArrayDeque:**
   - Implements the `Queue` interface using a resizable array.
   - Provides efficient FIFO operations with better performance than `LinkedList` in most cases.
   - Suitable for high-performance applications and scenarios where random access is not required.

### Usage:

- Use `Queue` when you need to maintain elements in a specific order and follow the FIFO principle.
- Choose the appropriate implementation based on your requirements:
   - Use `LinkedList` when you need flexibility and simplicity in queue operations and don't require high-performance characteristics.
   - Use `ArrayDeque` when you need efficient FIFO operations with better performance and don't require random access to elements.
- Consider the performance characteristics, memory usage, and flexibility of each implementation when choosing the appropriate queue implementation for your application.
## PriorityQueue

The `PriorityQueue` class in Java provides a data structure that enables the storage of elements in a priority order, where each element is associated with a priority. It is part of the `java.util` package.

### PriorityQueue Operations

1. **Adding Elements:**
   - `boolean add(E e)`: Inserts the specified element into the queue.
   - `boolean offer(E e)`: Inserts the specified element into the queue.

2. **Removing Elements:**
   - `E remove()`: Retrieves and removes the head of the queue.
   - `E poll()`: Retrieves and removes the head of the queue, or returns `null` if the queue is empty.

3. **Accessing Elements:**
   - `E element()`: Retrieves, but does not remove, the head of the queue.
   - `E peek()`: Retrieves, but does not remove, the head of the queue, or returns `null` if the queue is empty.

4. **Other Methods:**
   - `int size()`: Returns the number of elements in the queue.
   - `boolean isEmpty()`: Checks if the queue is empty.
   - `boolean contains(Object o)`: Checks if the queue contains the specified element.
   - `boolean remove(Object o)`: Removes a single instance of the specified element from the queue.
   - `void clear()`: Removes all elements from the queue.
   - `Object[] toArray()`: Returns an array containing all of the elements in the queue.

### Internal Working of PriorityQueue

- **Data Structure:**
   - `PriorityQueue` is implemented as a binary heap, specifically a min-heap. In a min-heap, the smallest element is at the root.
   - The heap is represented as an array.

- **Heap Invariant:**
   - For a min-heap, for any given node `i` (except the root), the element at index `i` is greater than or equal to the element at index `(i-1)/2` (the parent node).

- **Heap Operations:**
   - **Insertion (`add`/`offer`):**
      - Add the new element to the end of the array (the heap).
      - Perform an "up-heap" operation (or bubble-up) to restore the heap property by comparing the added element with its parent and swapping if necessary, until the heap property is satisfied.
   - **Deletion (`poll`/`remove`):**
      - Remove the root element (the minimum element).
      - Replace the root with the last element in the heap.
      - Perform a "down-heap" operation (or bubble-down) to restore the heap property by comparing the replaced root with its children and swapping if necessary, until the heap property is satisfied.
   - **Accessing (`peek`/`element`):**
      - Simply return the root element of the heap (the minimum element).

### Big-O-Notation Time Complexity

- **Insertion (`add`/`offer`):** O(log n)
   - Adding an element involves placing it at the end and then performing up-heap operations.

- **Deletion (`poll`/`remove`):** O(log n)
   - Removing the root element involves replacing it with the last element and performing down-heap operations.

- **Accessing the minimum element (`peek`/`element`):** O(1)
   - Accessing the minimum element is a constant-time operation since it is always at the root of the heap.

- **Other Operations:**
   - **Size:** O(1)
   - **IsEmpty:** O(1)
   - **Contains:** O(n)
   - **Remove(Object o):** O(n)
   - **Clear:** O(n)
   - **toArray:** O(n)

### Realtime Examples of PriorityQueue

1. **Task Scheduling:**
   - Used in job scheduling systems where tasks are executed based on priority.

2. **Dijkstra’s Shortest Path Algorithm:**
   - PriorityQueue is used to pick the next vertex with the smallest distance in Dijkstra’s algorithm.

3. **A* Search Algorithm:**
   - PriorityQueue is used to store the nodes to be evaluated based on their f-cost (sum of g-cost and h-cost).

4. **Event-Driven Simulation:**
   - Used in simulations to process events in chronological order.

5. **Merge K Sorted Lists:**
   - Used to efficiently merge multiple sorted lists by always extracting the smallest element across the lists.

### Summary

The `PriorityQueue` class in Java is an efficient way to handle elements that need to be processed in priority order. Its internal implementation using a binary heap allows it to perform key operations like insertion and deletion in logarithmic time. This makes it suitable for a variety of applications where priority-based processing is essential.

## PriorityBlockingQueue

#### Definition:
PriorityBlockingQueue is an unbounded blocking queue that stores elements with the same priority order as the Priority Queue. It extends the AbstractQueue class and implements the BlockingQueue interface.

#### All Operations:
- **Insertion:**
   - `add(e)` or `offer(e)`: Inserts the specified element into this queue.
   - `put(e)`: Inserts the specified element into this queue, waiting if necessary for space to become available.
- **Removal:**
   - `take()`: Retrieves and removes the head of this queue, waiting if necessary until an element becomes available.
   - `poll()`: Retrieves and removes the head of this queue, or returns null if this queue is empty.
   - `remove()`: Removes a single instance of the specified element from this queue, if it is present.
- **Examination:**
   - `peek()`: Retrieves, but does not remove, the head of this queue, or returns null if this queue is empty.
- **Bulk Operations:**
   - `addAll(collection)`: Adds all of the elements in the specified collection to this queue.
- **Array Operations:**
   - `toArray()`: Returns an array containing all of the elements in this queue.

#### Internal Working:
- PriorityBlockingQueue is based on a priority heap data structure, similar to the PriorityQueue class. However, it uses different synchronization mechanisms to ensure thread-safety.
- Internally, it maintains a heap-based data structure where elements are stored according to their priority. The head of the queue is always the least (or greatest, depending on the natural ordering or comparator) element with respect to the specified ordering.
- Unlike PriorityQueue, PriorityBlockingQueue uses a variant of the "blocking" mechanism to handle concurrency. When the queue is empty, the take() method blocks the calling thread until an element becomes available.

#### Big-O-Notation Time Complexity:
- **Insertion (offer, add, put):** O(log n) - logarithmic time complexity as it involves maintaining the heap property.
- **Removal (take, poll, remove):** O(log n) - logarithmic time complexity as it involves maintaining the heap property.
- **Examination (peek):** O(1) - constant time complexity as it returns the head of the queue.
- **Search (contains):** O(n) - linear time complexity as it may need to search the entire queue.
- **Bulk Operations (addAll):** O(n log n) - logarithmic time complexity for each element added, so overall O(n log n) where n is the number of elements being added.

PriorityBlockingQueue is particularly useful in scenarios where multiple threads need access to a shared priority queue and need to wait for elements to become available or be processed. It provides thread-safe operations without the need for external synchronization, making it efficient for concurrent applications.

# Deque
A Deque, short for "Double Ended Queue", is a linear collection of elements that supports insertion and removal of elements from both ends. It can be thought of as a generalization of both stacks (Last-In-First-Out) and queues (First-In-First-Out), allowing elements to be added and removed from either end.

### Deque Operations:

1. **Insertion Operations:**
   - `addFirst(element)`: Adds the specified element at the front of the deque.
   - `addLast(element)`: Adds the specified element at the end of the deque.
   - `offerFirst(element)`: Inserts the specified element at the front of the deque if possible, returning true upon success and false if the deque is full.
   - `offerLast(element)`: Inserts the specified element at the end of the deque if possible, returning true upon success and false if the deque is full.

2. **Removal Operations:**
   - `removeFirst()`: Removes and returns the first element of the deque, throwing an exception if the deque is empty.
   - `removeLast()`: Removes and returns the last element of the deque, throwing an exception if the deque is empty.
   - `pollFirst()`: Retrieves and removes the first element of the deque, or returns null if the deque is empty.
   - `pollLast()`: Retrieves and removes the last element of the deque, or returns null if the deque is empty.

3. **Examination Operations:**
   - `getFirst()`: Retrieves, but does not remove, the first element of the deque, throwing an exception if the deque is empty.
   - `getLast()`: Retrieves, but does not remove, the last element of the deque, throwing an exception if the deque is empty.
   - `peekFirst()`: Retrieves, but does not remove, the first element of the deque, or returns null if the deque is empty.
   - `peekLast()`: Retrieves, but does not remove, the last element of the deque, or returns null if the deque is empty.

4. **Size and Emptyness:**
   - `size()`: Returns the number of elements in the deque.
   - `isEmpty()`: Returns true if the deque contains no elements, false otherwise.

### Internal Working:
- Deques can be implemented using various data structures such as doubly linked lists, resizable arrays, or circular buffers.
- Doubly linked lists are a common choice for implementing deques because they allow efficient insertion and removal operations at both ends in constant time.
- Each node in the doubly linked list contains references to the previous and next nodes, enabling bidirectional traversal and modification.

### Big-O Notation Time Complexity:
- **Insertion and Removal Operations (addFirst, addLast, removeFirst, removeLast):** O(1) - constant time complexity for doubly linked list implementation.
- **Examination Operations (getFirst, getLast, peekFirst, peekLast):** O(1) - constant time complexity for accessing elements at the ends of the deque.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the deque.
- **Search Operation (contains):** O(n) - linear time complexity as it may need to search the entire deque.

Deques are useful in scenarios where elements need to be added or removed from both ends efficiently, such as implementing double-ended queues, sliding window algorithms, or maintaining a history of recently accessed elements. They provide a versatile data structure for various applications requiring bidirectional access.

## ArrayDeque
`ArrayDeque` is a resizable-array implementation of the `Deque` interface in Java. It is similar to `ArrayList`, but it allows efficient insertion and removal operations at both ends of the deque.

### ArrayDeque Features:

1. **Resizable Array:**
   - Internally, `ArrayDeque` is backed by an array that grows as needed.
   - The array is resized when the deque reaches its capacity.

2. **Efficient Insertion and Removal:**
   - Both insertion and removal operations at the beginning and end of the deque have amortized constant time complexity, O(1).
   - This makes `ArrayDeque` suitable for implementing double-ended queues (deques) and other scenarios where efficient insertion and removal at both ends are required.

3. **No Capacity Restrictions:**
   - `ArrayDeque` does not have a fixed capacity like arrays.
   - It grows dynamically as elements are added.

4. **Not Thread-Safe:**
   - `ArrayDeque` is not synchronized, so it is not safe to use in concurrent environments without external synchronization.

### ArrayDeque Operations:

`ArrayDeque` supports all operations provided by the `Deque` interface, including insertion, removal, examination, and bulk operations. Some of the key operations include:

- **Insertion Operations:**
   - `addFirst(element)`: Adds the specified element at the front of the deque.
   - `addLast(element)`: Adds the specified element at the end of the deque.

- **Removal Operations:**
   - `removeFirst()`: Removes and returns the first element of the deque.
   - `removeLast()`: Removes and returns the last element of the deque.

- **Examination Operations:**
   - `getFirst()`: Retrieves, but does not remove, the first element of the deque.
   - `getLast()`: Retrieves, but does not remove, the last element of the deque.

- **Bulk Operations:**
   - `addAll(collection)`: Adds all of the elements in the specified collection to the deque.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the deque.
   - `isEmpty()`: Returns true if the deque contains no elements, false otherwise.

### Internal Working:

Internally, `ArrayDeque` maintains a circular array to store its elements. When elements are added or removed at the beginning or end of the deque, it adjusts the indices to maintain the circular structure efficiently. If the array reaches its capacity, it is resized to accommodate more elements.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (addFirst, addLast, removeFirst, removeLast):** O(1) - amortized constant time complexity.
- **Examination Operations (getFirst, getLast):** O(1) - constant time complexity.
- **Bulk Operations (addAll):** O(n) - linear time complexity for adding all elements of the specified collection.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the deque.

`ArrayDeque` is a versatile data structure suitable for various scenarios requiring efficient insertion and removal at both ends. It is commonly used in applications such as implementing queues, stacks, and algorithms like breadth-first search (BFS) where efficient manipulation of a double-ended queue is required.

## ConcurrentLinkedDeque
`ConcurrentLinkedDeque` is a concurrent, thread-safe implementation of the `Deque` interface introduced in Java 7. It provides a non-blocking, lock-free, and scalable alternative to `ArrayDeque` for use in concurrent environments where multiple threads need access to a shared deque.

### ConcurrentLinkedDeque Features:

1. **Concurrency Support:**
   - `ConcurrentLinkedDeque` is designed for concurrent access by multiple threads.
   - It ensures thread-safety without the need for external synchronization mechanisms such as locks.
   - Multiple threads can safely perform concurrent operations on the deque without blocking or waiting for each other.

2. **Non-Blocking Operations:**
   - Operations provided by `ConcurrentLinkedDeque` are non-blocking, meaning that threads do not wait or block when performing operations on the deque.
   - Instead of locking the entire deque, threads only lock individual nodes during specific operations, allowing other threads to continue accessing the deque concurrently.

3. **Scalability:**
   - `ConcurrentLinkedDeque` is designed to be highly scalable, meaning that its performance remains consistent even with a large number of threads accessing the deque concurrently.
   - It achieves scalability by minimizing contention and ensuring that threads can operate independently on different parts of the deque.

4. **Dynamic Structure:**
   - Like `ArrayDeque`, `ConcurrentLinkedDeque` does not have a fixed capacity and can dynamically resize as needed.
   - It adjusts its internal structure dynamically to accommodate changes in size and load.

### ConcurrentLinkedDeque Operations:

`ConcurrentLinkedDeque` supports all operations provided by the `Deque` interface, including insertion, removal, examination, and bulk operations. Some of the key operations include:

- **Insertion Operations:**
   - `addFirst(element)`: Adds the specified element at the front of the deque.
   - `addLast(element)`: Adds the specified element at the end of the deque.

- **Removal Operations:**
   - `removeFirst()`: Removes and returns the first element of the deque.
   - `removeLast()`: Removes and returns the last element of the deque.

- **Examination Operations:**
   - `getFirst()`: Retrieves, but does not remove, the first element of the deque.
   - `getLast()`: Retrieves, but does not remove, the last element of the deque.

- **Bulk Operations:**
   - `addAll(collection)`: Adds all of the elements in the specified collection to the deque.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the deque.
   - `isEmpty()`: Returns true if the deque contains no elements, false otherwise.

### Internal Working:

Internally, `ConcurrentLinkedDeque` is implemented using a doubly-linked list structure similar to `LinkedList`. Each node in the list contains references to both the previous and next nodes, allowing bidirectional traversal and modification.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (addFirst, addLast, removeFirst, removeLast):** O(1) - constant time complexity for most cases.
- **Examination Operations (getFirst, getLast):** O(1) - constant time complexity.
- **Bulk Operations (addAll):** O(n) - linear time complexity for adding all elements of the specified collection.
- **Size Operation (size):** O(n) - linear time complexity for counting the number of elements in the deque, but in most cases, it provides a constant-time estimate.

`ConcurrentLinkedDeque` is particularly useful in highly concurrent applications where multiple threads need to access a shared deque efficiently and without blocking. It provides excellent scalability and performance characteristics, making it suitable for use in multi-threaded environments such as concurrent data processing pipelines, work-stealing algorithms, and parallel computing tasks.
# List
A `List` in Java is an ordered collection of elements where each element has a specific position or index. It allows duplicate elements and maintains the insertion order of elements. The `List` interface is part of the Java Collections Framework and provides various methods to manipulate and access elements in the list.

### List Interface Methods:

1. **Insertion and Removal Operations:**
   - `add(element)`: Appends the specified element to the end of the list.
   - `add(index, element)`: Inserts the specified element at the specified position in the list.
   - `remove(index)`: Removes the element at the specified position in the list.
   - `remove(element)`: Removes the first occurrence of the specified element from the list.
   - `clear()`: Removes all elements from the list.

2. **Access and Retrieval Operations:**
   - `get(index)`: Returns the element at the specified position in the list.
   - `set(index, element)`: Replaces the element at the specified position in the list with the specified element.
   - `indexOf(element)`: Returns the index of the first occurrence of the specified element in the list.
   - `lastIndexOf(element)`: Returns the index of the last occurrence of the specified element in the list.
   - `subList(fromIndex, toIndex)`: Returns a view of the portion of the list between the specified fromIndex, inclusive, and toIndex, exclusive.

3. **Size and Emptyness:**
   - `size()`: Returns the number of elements in the list.
   - `isEmpty()`: Returns true if the list contains no elements, false otherwise.

4. **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the list.
   - `forEach(action)`: Performs the given action for each element of the list until all elements have been processed or the action throws an exception.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the end of the list.

### Common Implementations of List:

1. **ArrayList:**
   - Backed by a resizable array.
   - Provides fast random access but slower insertions and removals compared to LinkedList.

2. **LinkedList:**
   - Doubly-linked list implementation.
   - Provides fast insertions and removals but slower random access compared to ArrayList.

3. **Vector:**
   - Synchronized version of ArrayList.
   - Slower due to synchronization overhead.

4. **CopyOnWriteArrayList:**
   - Thread-safe version of ArrayList.
   - Creates a new copy of the underlying array on every write operation.

### Time Complexity:

- **Insertion and Removal Operations (add, remove):** O(n) - linear time complexity for ArrayList (due to potential resizing) and O(1) for LinkedList (for add/remove operations at the beginning or end).
- **Access and Retrieval Operations (get, set):** O(1) for ArrayList (constant time complexity for random access) and O(n) for LinkedList (linear time complexity for traversal).
- **Size Operation (size):** O(1) - constant time complexity for both ArrayList and LinkedList.
- **Search Operation (indexOf, lastIndexOf):** O(n) - linear time complexity for both ArrayList and LinkedList.

### Usage:

- Use ArrayList when you need fast random access and do not have frequent insertions/removals.
- Use LinkedList when you need fast insertions/removals and can tolerate slower random access.
- Use CopyOnWriteArrayList in concurrent environments where you need a thread-safe list with low contention on write operations.

Lists are widely used in Java programming for storing and manipulating ordered collections of elements. They provide a flexible and efficient way to work with sequences of data in various applications.

## ArrayList
`ArrayList` in Java is a resizable-array implementation of the `List` interface. It is part of the Java Collections Framework and provides dynamic storage for elements, allowing for automatic resizing as needed. `ArrayList` offers fast random access and is generally considered to be more efficient for accessing elements at specific indices compared to `LinkedList`.

### ArrayList Features:

1. **Resizable Array:**
   - Internally, `ArrayList` is backed by an array that dynamically grows as elements are added to it.
   - The array is resized when the number of elements exceeds the capacity of the array, typically doubling the size to accommodate additional elements.

2. **Fast Random Access:**
   - `ArrayList` provides constant-time access to elements at specific indices, making it suitable for scenarios where frequent random access is required.
   - Accessing elements by index using the `get(int index)` method has O(1) time complexity.

3. **Efficient Insertion and Removal at End:**
   - Adding or removing elements at the end of an `ArrayList` has amortized constant time complexity.
   - Insertion or removal at the end of the list using `add(E element)` or `remove(int index)` is generally faster compared to insertion or removal at other positions.

4. **Supports Duplicate Elements:**
   - `ArrayList` allows duplicate elements, meaning that the same element can occur multiple times in the list.
   - Elements are stored in the order they were added, allowing duplicates to maintain their insertion order.

5. **Not Thread-Safe:**
   - `ArrayList` is not synchronized, meaning it is not safe to use in concurrent environments without external synchronization mechanisms such as using `Collections.synchronizedList()`.

### ArrayList Operations:

`ArrayList` supports all operations provided by the `List` interface. Some of the key operations include:

- **Insertion and Removal Operations:**
   - `add(element)`: Appends the specified element to the end of the list.
   - `add(index, element)`: Inserts the specified element at the specified position in the list.
   - `remove(index)`: Removes the element at the specified position in the list.

- **Access and Retrieval Operations:**
   - `get(index)`: Returns the element at the specified position in the list.
   - `set(index, element)`: Replaces the element at the specified position in the list with the specified element.
   - `indexOf(element)`: Returns the index of the first occurrence of the specified element in the list.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the list.
   - `isEmpty()`: Returns true if the list contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the list.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the end of the list.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (add, remove):** O(n) - linear time complexity for adding/removing elements in the middle of the list due to potential shifting of elements.
- **Access and Retrieval Operations (get, set):** O(1) - constant time complexity for accessing/replacing elements at specific indices.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the list.
- **Search Operation (indexOf):** O(n) - linear time complexity for searching for an element in the list.

### Usage:

- Use `ArrayList` when you need fast random access and do not have frequent insertions/removals in the middle of the list.
- It is commonly used in scenarios where you need to access elements by their indices or iterate over the elements sequentially.
- Avoid using `ArrayList` in concurrent environments without synchronization, or consider using `CopyOnWriteArrayList` for thread-safe access.

## LinkedList

`LinkedList` in Java is a doubly-linked list implementation of the `List` interface. Unlike `ArrayList`, which is based on an array, `LinkedList` is based on nodes that are connected via references to both the previous and next nodes. This structure allows for efficient insertion and removal of elements at both the beginning and end of the list.

### LinkedList Features:

1. **Doubly-Linked Nodes:**
   - Each element in a `LinkedList` is represented by a node that contains references to both the previous and next nodes in the list.
   - This structure allows for bidirectional traversal and efficient insertion/removal operations at both ends of the list.

2. **Efficient Insertion and Removal:**
   - `LinkedList` provides constant-time insertion and removal operations at the beginning and end of the list.
   - Adding or removing elements at the beginning or end of the list using methods like `addFirst(E element)` or `removeFirst()` has O(1) time complexity.

3. **No Need for Array Resizing:**
   - Unlike `ArrayList`, `LinkedList` does not require resizing of an underlying array when elements are added or removed.
   - Insertions and removals do not require shifting of elements, making them more efficient, especially for large lists.

4. **Not Suitable for Random Access:**
   - While `LinkedList` allows for efficient insertion and removal operations at both ends of the list, accessing elements by index is less efficient compared to `ArrayList`.
   - Accessing elements by index using the `get(int index)` method has O(n) time complexity as it requires traversing the list from the beginning or end to reach the desired index.

5. **Not Synchronized:**
   - `LinkedList` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.

### LinkedList Operations:

`LinkedList` supports all operations provided by the `List` interface. Some of the key operations include:

- **Insertion and Removal Operations:**
   - `add(element)`: Appends the specified element to the end of the list.
   - `addFirst(element)`: Inserts the specified element at the beginning of the list.
   - `addLast(element)`: Appends the specified element to the end of the list.
   - `remove(index)`: Removes the element at the specified position in the list.
   - `removeFirst()`: Removes and returns the first element of the list.
   - `removeLast()`: Removes and returns the last element of the list.

- **Access and Retrieval Operations:**
   - `get(index)`: Returns the element at the specified position in the list.
   - `set(index, element)`: Replaces the element at the specified position in the list with the specified element.
   - `indexOf(element)`: Returns the index of the first occurrence of the specified element in the list.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the list.
   - `isEmpty()`: Returns true if the list contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the list.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the end of the list.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (add, remove):** O(1) - constant time complexity for operations at the beginning or end of the list.
- **Access and Retrieval Operations (get, set):** O(n) - linear time complexity for accessing/replacing elements by index due to traversal from the beginning or end of the list.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the list.
- **Search Operation (indexOf):** O(n) - linear time complexity for searching for an element in the list.

### Usage:

- Use `LinkedList` when you need fast insertion/removal operations at both ends of the list and do not require frequent random access by index.
- It is suitable for scenarios where you need to implement a queue, stack, or perform sequential processing of elements.
- Avoid using `LinkedList` for scenarios requiring frequent random access or when working with large lists where traversal performance is critical. In such cases, `ArrayList` may be more suitable.

## Vector
`Vector` in Java is a legacy class that implements a dynamic array, similar to `ArrayList`, but with synchronized methods to ensure thread safety. It is part of the Java Collections Framework and provides functionality similar to `ArrayList`, with the main difference being its synchronization.

### Vector Features:

1. **Dynamic Array:**
   - Internally, `Vector` is backed by an array that dynamically grows as elements are added to it.
   - The array is resized when the number of elements exceeds the capacity of the array, typically doubling the size to accommodate additional elements.

2. **Thread-Safe Operations:**
   - All methods of `Vector` are synchronized, ensuring that multiple threads can safely access and modify the vector concurrently without data corruption.
   - Synchronization is achieved by using synchronized methods, which adds overhead compared to unsynchronized collections like `ArrayList`.

3. **Backward Compatibility:**
   - `Vector` is one of the original classes in Java Collections Framework and has been available since Java 1.0.
   - It is often used in legacy codebases or situations where compatibility with older Java versions is required.

4. **Similar Interface to ArrayList:**
   - `Vector` provides a similar interface to `ArrayList`, supporting operations for adding, removing, and accessing elements in the list.

5. **Performance Considerations:**
   - Due to its synchronized nature, `Vector` operations may be slower compared to `ArrayList` for single-threaded scenarios where thread safety is not a concern.
   - For multi-threaded scenarios, `Vector` may be preferred over `ArrayList` when thread safety is paramount.

### Vector Operations:

`Vector` supports all operations provided by the `List` interface, similar to `ArrayList`. Some of the key operations include:

- **Insertion and Removal Operations:**
   - `add(element)`: Appends the specified element to the end of the vector.
   - `add(index, element)`: Inserts the specified element at the specified position in the vector.
   - `remove(index)`: Removes the element at the specified position in the vector.

- **Access and Retrieval Operations:**
   - `get(index)`: Returns the element at the specified position in the vector.
   - `set(index, element)`: Replaces the element at the specified position in the vector with the specified element.
   - `indexOf(element)`: Returns the index of the first occurrence of the specified element in the vector.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the vector.
   - `isEmpty()`: Returns true if the vector contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the vector.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the end of the vector.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (add, remove):** O(n) - linear time complexity for adding/removing elements in the middle of the vector due to potential shifting of elements.
- **Access and Retrieval Operations (get, set):** O(1) - constant time complexity for accessing/replacing elements at specific indices.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the vector.
- **Search Operation (indexOf):** O(n) - linear time complexity for searching for an element in the vector.

### Usage:

- Use `Vector` when you require thread-safe operations and compatibility with older Java versions.
- It is suitable for scenarios where multiple threads need to access and modify the collection concurrently.
- For single-threaded scenarios or situations where thread safety is not a concern, consider using `ArrayList` or other unsynchronized collections for better performance.

## Stack

`Stack` in Java represents a last-in, first-out (LIFO) data structure. It is part of the Java Collections Framework and extends the `Vector` class, providing stack-specific methods for push (adding) and pop (removing) elements. However, it's worth noting that the use of `Stack` is discouraged in favor of `Deque` implementations such as `ArrayDeque` due to performance reasons.

### Stack Features:

1. **Last-In, First-Out (LIFO) Structure:**
   - Elements are added and removed from the top of the stack.
   - The last element added to the stack is the first one to be removed.

2. **Extends Vector:**
   - `Stack` is implemented as a subclass of `Vector`.
   - It inherits methods from `Vector` but adds stack-specific methods like `push()` and `pop()`.

3. **Thread-Safe Operations:**
   - Similar to `Vector`, `Stack` methods are synchronized, making it thread-safe.
   - However, for new code, it's recommended to use synchronized collections or concurrent collections for thread safety.

4. **Performance Considerations:**
   - Due to its synchronization and inheritance from `Vector`, `Stack` operations may be slower compared to more modern alternatives like `ArrayDeque`.

5. **Not Suitable for Modern Use:**
   - While `Stack` provides a convenient way to implement a stack data structure, its use is discouraged in favor of newer implementations like `ArrayDeque` or `LinkedList`.

### Stack Operations:

`Stack` supports all operations provided by its superclass `Vector`, as well as stack-specific operations. Some of the key operations include:

- **Push and Pop Operations:**
   - `push(element)`: Pushes the specified element onto the top of the stack.
   - `pop()`: Removes and returns the top element of the stack.

- **Access and Retrieval Operations:**
   - `peek()`: Returns the top element of the stack without removing it.
   - `search(element)`: Returns the 1-based position of the specified element in the stack, or -1 if the element is not found.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the stack.
   - `isEmpty()`: Returns true if the stack contains no elements, false otherwise.

### Big-O Notation Time Complexity:

- **Push and Pop Operations (push, pop):** O(1) - constant time complexity for adding/removing elements at the top of the stack.
- **Access and Retrieval Operations (peek, search):** O(1) - constant time complexity for accessing the top element of the stack or searching for an element.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the stack.
- **Search Operation (search):** O(n) - linear time complexity for searching for an element in the stack.

### Usage:

- While `Stack` provides a convenient way to implement a stack data structure, its use is discouraged in modern Java programming due to performance reasons.
- For new code, consider using `Deque` implementations such as `ArrayDeque` for stack operations, as they offer better performance and are not synchronized by default.
- `Stack` may still be suitable for specific use cases or when maintaining compatibility with existing code that relies on it.

## CopyOnWriteArrayList vs CopyOnWriteArraySet

## CopyOnWriteArrayList
`CopyOnWriteArrayList` is a concurrent, thread-safe implementation of the `List` interface introduced in Java 5. It provides thread-safe iteration without the need for external synchronization mechanisms like explicit locking.

### CopyOnWriteArrayList Features:

1. **Thread-Safe Iteration:**
   - Iterators returned by `CopyOnWriteArrayList` are guaranteed to never throw `ConcurrentModificationException` during iteration, even if the underlying list is modified concurrently.
   - The iterator operates on a snapshot of the list taken at the time of iterator creation.

2. **Immutable Copy on Write:**
   - Every modification operation (add, set, remove, etc.) on a `CopyOnWriteArrayList` creates a new copy of the underlying array.
   - The modification is performed on the new copy, leaving the original array unchanged.
   - This ensures that ongoing iterations can continue unaffected by modifications.

3. **Read Performance:**
   - Read operations (such as `get`) are performed directly on the underlying array without synchronization, providing good performance for read-heavy workloads.

4. **Not Suitable for Write-Heavy Workloads:**
   - `CopyOnWriteArrayList` is not suitable for scenarios where write operations (such as `add`, `set`, `remove`) are frequent or performance-critical.
   - Write operations are more expensive because they involve copying the entire array.

5. **Use Cases:**
   - `CopyOnWriteArrayList` is suitable for scenarios where the list is rarely modified but frequently iterated over by multiple threads concurrently.
   - It is commonly used in scenarios such as maintaining event listeners or configurations.

### CopyOnWriteArrayList Operations:

`CopyOnWriteArrayList` supports all operations provided by the `List` interface. Some of the key operations include:

- **Insertion and Removal Operations:**
   - `add(element)`: Appends the specified element to the end of the list.
   - `remove(index)`: Removes the element at the specified position in the list.

- **Access and Retrieval Operations:**
   - `get(index)`: Returns the element at the specified position in the list.
   - `set(index, element)`: Replaces the element at the specified position in the list with the specified element.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the list.
   - `isEmpty()`: Returns true if the list contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the list.
   - `forEach(action)`: Performs the given action for each element of the list until all elements have been processed or the action throws an exception.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (add, remove):** O(n) - linear time complexity for adding/removing elements due to copying of the array.
- **Access and Retrieval Operations (get, set):** O(1) - constant time complexity for accessing/replacing elements at specific indices.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the list.

### Usage:

- Use `CopyOnWriteArrayList` in scenarios where the list is rarely modified but frequently iterated over by multiple threads concurrently.
- It provides thread-safe iteration and is suitable for scenarios such as maintaining event listeners, configurations, or caches.
- Avoid using `CopyOnWriteArrayList` in write-heavy workloads or performance-critical scenarios, as write operations are more expensive due to copying of the array.

## CopyOnWriteArraySet
`CopyOnWriteArraySet` is a concurrent, thread-safe implementation of the `Set` interface introduced in Java 5. It is similar to `CopyOnWriteArrayList` but adapted for sets, providing thread-safe iteration without the need for external synchronization mechanisms.

### CopyOnWriteArraySet Features:

1. **Thread-Safe Iteration:**
   - Iterators returned by `CopyOnWriteArraySet` are guaranteed to never throw `ConcurrentModificationException` during iteration, even if the underlying set is modified concurrently.
   - The iterator operates on a snapshot of the set taken at the time of iterator creation.

2. **Immutable Copy on Write:**
   - Similar to `CopyOnWriteArrayList`, every modification operation (add, remove, etc.) on a `CopyOnWriteArraySet` creates a new copy of the underlying array.
   - The modification is performed on the new copy, leaving the original set unchanged.

3. **Read Performance:**
   - Read operations (such as `contains`) are performed directly on the underlying array without synchronization, providing good performance for read-heavy workloads.

4. **Not Suitable for Write-Heavy Workloads:**
   - `CopyOnWriteArraySet` is not suitable for scenarios where write operations (such as `add`, `remove`) are frequent or performance-critical.
   - Write operations are more expensive because they involve copying the entire array.

5. **Use Cases:**
   - `CopyOnWriteArraySet` is suitable for scenarios where the set is rarely modified but frequently accessed by multiple threads concurrently.
   - It is commonly used in scenarios such as maintaining collections of shared resources or managing event listeners.

### CopyOnWriteArraySet Operations:

`CopyOnWriteArraySet` supports all operations provided by the `Set` interface. Some of the key operations include:

- **Insertion and Removal Operations:**
   - `add(element)`: Adds the specified element to the set if it is not already present.
   - `remove(element)`: Removes the specified element from the set if it is present.

- **Access and Retrieval Operations:**
   - `contains(element)`: Returns true if the set contains the specified element, false otherwise.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the set.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the set.
   - `forEach(action)`: Performs the given action for each element of the set until all elements have been processed or the action throws an exception.

### Big-O Notation Time Complexity:

- **Insertion and Removal Operations (add, remove):** O(n) - linear time complexity for adding/removing elements due to copying of the array.
- **Access and Retrieval Operations (contains):** O(n) - linear time complexity for searching for an element in the set.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the set.

### Usage:

- Use `CopyOnWriteArraySet` in scenarios where the set is rarely modified but frequently accessed by multiple threads concurrently.
- It provides thread-safe access and iteration and is suitable for scenarios such as maintaining collections of shared resources or managing event listeners.
- Avoid using `CopyOnWriteArraySet` in write-heavy workloads or performance-critical scenarios, as write operations are more expensive due to copying of the array.

# Set
A `Set` in Java represents a collection of unique elements, meaning that no two elements in the set are equal according to the `equals()` method. It is part of the Java Collections Framework and provides a way to store and manipulate a group of distinct objects.

### Set Interface Methods:

1. **Adding Elements:**
   - `add(element)`: Adds the specified element to the set if it is not already present.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the set.

2. **Removing Elements:**
   - `remove(element)`: Removes the specified element from the set if it is present.
   - `removeAll(collection)`: Removes all elements from the set that are present in the specified collection.

3. **Access and Retrieval:**
   - `contains(element)`: Returns true if the set contains the specified element, false otherwise.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

4. **Size and Emptyness:**
   - `size()`: Returns the number of elements in the set.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

5. **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the set.
   - `forEach(action)`: Performs the given action for each element of the set until all elements have been processed or the action throws an exception.

6. **Set Operations:**
   - `retainAll(collection)`: Retains only the elements in the set that are contained in the specified collection.
   - `clear()`: Removes all elements from the set.

### Common Implementations of Set:

1. **HashSet:**
   - Implements the `Set` interface using a hash table.
   - Provides constant-time performance for basic operations, assuming a good hash function.

2. **LinkedHashSet:**
   - Extends `HashSet` to maintain insertion order.
   - Provides predictable iteration order, which is the order in which elements were inserted into the set.

3. **TreeSet:**
   - Implements the `NavigableSet` interface using a self-balancing binary search tree (usually a Red-Black tree).
   - Provides sorted order of elements, either natural ordering or specified comparator.

4. **EnumSet:**
   - A specialized implementation of `Set` for use with enum types.
   - Extremely efficient and compact representation of enum values as bit vectors.

5. **CopyOnWriteArraySet:**
   - A concurrent, thread-safe implementation of `Set` based on `CopyOnWriteArrayList`.
   - Provides thread-safe iteration and is suitable for scenarios where the set is rarely modified but frequently accessed by multiple threads concurrently.

### Usage:

- Use `Set` when you need to store a collection of unique elements.
- Choose the appropriate implementation based on your requirements:
   - Use `HashSet` for general-purpose usage when you don't need sorted order or predictable iteration.
   - Use `LinkedHashSet` when you need to maintain insertion order.
   - Use `TreeSet` when you need sorted order of elements.
   - Use `EnumSet` for efficient storage of enum values.
   - Use `CopyOnWriteArraySet` in concurrent environments where thread-safe iteration is required and write operations are infrequent.
- Consider the characteristics of each implementation, such as performance, memory usage, and thread safety, when choosing the appropriate set implementation for your application.

## HashSet
`HashSet` in Java is an implementation of the `Set` interface that uses a hash table for storage. It is part of the Java Collections Framework and provides constant-time performance for basic operations (add, remove, contains) assuming a good hash function. Here's a detailed overview:

### HashSet Features:

1. **Hash Table Implementation:**
   - Internally, `HashSet` is backed by a hash table, which stores elements based on their hash codes.
   - Hash tables provide constant-time performance for basic operations like add, remove, and contains, on average.

2. **Unique Elements:**
   - `HashSet` does not allow duplicate elements. If an attempt is made to add a duplicate element, the operation has no effect.
   - The `equals()` method is used to determine if two elements are equal, and the `hashCode()` method is used to compute the hash code of elements.

3. **Unordered Collection:**
   - The elements in a `HashSet` are not ordered. There is no guarantee on the order in which elements are stored or iterated over.

4. **Fast Lookup:**
   - `HashSet` provides constant-time performance for operations like add, remove, and contains, assuming a good hash function.
   - The performance of `HashSet` depends on the quality of the hash function and the distribution of elements.

5. **Not Synchronized:**
   - `HashSet` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.
   - For concurrent access, use `Collections.synchronizedSet()` to create a synchronized set.

### HashSet Operations:

`HashSet` supports all operations provided by the `Set` interface. Some of the key operations include:

- **Adding Elements:**
   - `add(element)`: Adds the specified element to the set if it is not already present.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the set.

- **Removing Elements:**
   - `remove(element)`: Removes the specified element from the set if it is present.
   - `removeAll(collection)`: Removes all elements from the set that are present in the specified collection.

- **Access and Retrieval:**
   - `contains(element)`: Returns true if the set contains the specified element, false otherwise.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the set.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the set.
   - `forEach(action)`: Performs the given action for each element of the set until all elements have been processed or the action throws an exception.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (add, remove, contains):** O(1) - constant time complexity on average, assuming a good hash function and uniform distribution of elements.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the set.
- **Iteration Operation (iterator, forEach):** O(n) - linear time complexity, where n is the number of elements in the set, as it iterates over all elements.

### Usage:

- Use `HashSet` when you need a collection of unique elements and the order of elements does not matter.
- It is commonly used in scenarios such as removing duplicates from a collection, checking for membership, or implementing fast lookup tables.
- `HashSet` is not suitable for scenarios where the order of elements needs to be maintained or when concurrent access from multiple threads is required.

## LinkedHashSet
`LinkedHashSet` in Java is an implementation of the `Set` interface that extends `HashSet`. It maintains a predictable iteration order, which is the order in which elements were inserted into the set. Here's an overview of its features:

### LinkedHashSet Features:

1. **Hash Table with Linked List:**
   - Internally, `LinkedHashSet` combines the features of a hash table and a linked list.
   - It maintains a hash table for fast lookup and a linked list for maintaining the insertion order of elements.

2. **Predictable Iteration Order:**
   - Unlike `HashSet`, which does not guarantee any specific order of iteration, `LinkedHashSet` guarantees the iteration order to be the same as the order of insertion.
   - Iterating over a `LinkedHashSet` will always return elements in the order they were added.

3. **Unique Elements:**
   - Like `HashSet`, `LinkedHashSet` does not allow duplicate elements. If an attempt is made to add a duplicate element, the operation has no effect.

4. **Ordered Collection:**
   - `LinkedHashSet` provides predictable iteration order, making it suitable for scenarios where the order of elements matters.

5. **Performance Characteristics:**
   - The performance of `LinkedHashSet` for basic operations like add, remove, and contains is similar to that of `HashSet`.
   - The insertion order is maintained efficiently using the linked list, without affecting the performance of hash table operations.

6. **Not Synchronized:**
   - `LinkedHashSet` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.

### LinkedHashSet Operations:

`LinkedHashSet` supports all operations provided by the `Set` interface, as it extends `HashSet`. Some of the key operations include:

- **Adding Elements:**
   - `add(element)`: Adds the specified element to the set if it is not already present.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the set.

- **Removing Elements:**
   - `remove(element)`: Removes the specified element from the set if it is present.
   - `removeAll(collection)`: Removes all elements from the set that are present in the specified collection.

- **Access and Retrieval:**
   - `contains(element)`: Returns true if the set contains the specified element, false otherwise.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the set.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the set.
   - `forEach(action)`: Performs the given action for each element of the set until all elements have been processed or the action throws an exception.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (add, remove, contains):** O(1) - constant time complexity on average, similar to `HashSet`.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the set.
- **Iteration Operation (iterator, forEach):** O(n) - linear time complexity, where n is the number of elements in the set, as it iterates over all elements.

### Usage:

- Use `LinkedHashSet` when you need a collection of unique elements and want to maintain the insertion order of elements.
- It is useful in scenarios where you need predictable iteration order along with the benefits of a hash table for fast lookup.
- `LinkedHashSet` is not suitable for scenarios where the order of elements does not matter or when concurrent access from multiple threads is required.

## TreeSet
`TreeSet` in Java is an implementation of the `NavigableSet` interface that uses a self-balancing binary search tree (usually a Red-Black tree) for storage. It provides a sorted order of elements, either based on their natural ordering or a specified comparator. Here's an overview of its features:

### TreeSet Features:

1. **Self-Balancing Binary Search Tree:**
   - Internally, `TreeSet` is backed by a self-balancing binary search tree data structure, typically a Red-Black tree.
   - This data structure ensures that the elements are always sorted, allowing for efficient search, insertion, and deletion operations.

2. **Sorted Order of Elements:**
   - `TreeSet` maintains the elements in sorted order, either based on their natural ordering (if the elements implement the `Comparable` interface) or a specified comparator.
   - The elements are arranged in ascending order according to their natural order or the comparator's ordering.

3. **Unique Elements:**
   - Like other `Set` implementations, `TreeSet` does not allow duplicate elements. If an attempt is made to add a duplicate element, the operation has no effect.

4. **Ordered Collection:**
   - `TreeSet` provides a sorted order of elements, making it suitable for scenarios where iteration order matters or efficient search operations are required.

5. **Performance Characteristics:**
   - The performance of `TreeSet` for basic operations like add, remove, and contains depends on the height of the underlying binary search tree.
   - In general, these operations have O(log n) time complexity, where n is the number of elements in the set.

6. **Not Synchronized:**
   - `TreeSet` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.

### TreeSet Operations:

`TreeSet` supports all operations provided by the `NavigableSet` interface. Some of the key operations include:

- **Adding Elements:**
   - `add(element)`: Adds the specified element to the set if it is not already present.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the set.

- **Removing Elements:**
   - `remove(element)`: Removes the specified element from the set if it is present.
   - `removeAll(collection)`: Removes all elements from the set that are present in the specified collection.

- **Access and Retrieval:**
   - `contains(element)`: Returns true if the set contains the specified element, false otherwise.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the set.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the set.
   - `forEach(action)`: Performs the given action for each element of the set until all elements have been processed or the action throws an exception.

- **Navigational Operations:**
   - `lower(element)`, `floor(element)`, `ceiling(element)`, `higher(element)`: Return the greatest element in the set strictly less than, less than or equal to, greater than or equal to, and strictly greater than the given element, respectively.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (add, remove, contains):** O(log n) - logarithmic time complexity for basic operations on average, where n is the number of elements in the set.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the set.
- **Iteration Operation (iterator, forEach):** O(n) - linear time complexity, where n is the number of elements in the set, as it iterates over all elements.

### Usage:

- Use `TreeSet` when you need a collection of unique elements that are sorted according to their natural order or a specified comparator.
- It is useful in scenarios where you need efficient search operations, as well as maintaining elements in sorted order.
- `TreeSet` is not suitable for scenarios where the order of elements does not matter or when concurrent access from multiple threads is required.

## SortedSet vs NavigableSet vs TreeSet
Sure, here's a tabular representation comparing `SortedSet`, `NavigableSet`, and `TreeSet` based on their features:

| Feature             | SortedSet                                         | NavigableSet                                      | TreeSet                                           |
|---------------------|---------------------------------------------------|---------------------------------------------------|---------------------------------------------------|
| Purpose             | Represents a sorted set of elements              | Represents a navigable set with additional methods | Implements a navigable set using a binary tree    |
| Interface           | Extends `Set`                                     | Extends `SortedSet`                               | Implements `NavigableSet`                         |
| Inheritance         | Inherits from `Collection`, `Iterable` interfaces | Inherits from `Set`, `Collection`, `Iterable`     | Inherits from `AbstractSet`, `NavigableSet`      |
| Sorted Order        | Maintains elements in sorted order                | Maintains elements in sorted order                | Maintains elements in sorted order                |
| Navigation Methods  | No additional navigation methods                 | Provides navigation methods like `lower()`, `ceiling()`, etc. | Provides navigation methods and views           |
| Time Complexity     | O(log n) for basic operations                    | O(log n) for basic operations                     | O(log n) for basic operations                     |
| Data Structure      | Not specified                                     | Not specified                                     | Self-balancing binary search tree (Red-Black tree) |
| Use Cases           | Basic sorted set functionality                   | Navigation and manipulation based on element order | Efficient sorted set with navigation capabilities |

This table provides a comparison of the key features and characteristics of `SortedSet`, `NavigableSet`, and `TreeSet`. Depending on your requirements, you can choose the most appropriate interface or class for your use case.

## ConcurrentSkipListSet
`ConcurrentSkipListSet` is a concurrent, thread-safe implementation of the `NavigableSet` interface introduced in Java 6. It is based on a skip list data structure, which allows for efficient insertion, deletion, and search operations with logarithmic time complexity. Here's an overview of its features:

### ConcurrentSkipListSet Features:

1. **Concurrent Access:**
   - `ConcurrentSkipListSet` is designed for concurrent access from multiple threads without the need for external synchronization.
   - It provides thread-safe operations for insertion, removal, and retrieval of elements.

2. **Skip List Data Structure:**
   - Internally, `ConcurrentSkipListSet` is implemented using a skip list data structure.
   - Skip lists are probabilistic data structures that maintain a sorted set of elements with logarithmic time complexity for most operations.

3. **Sorted Order of Elements:**
   - Similar to other sorted set implementations, `ConcurrentSkipListSet` maintains its elements in sorted order.
   - The elements are arranged according to their natural ordering or a custom comparator provided during set creation.

4. **Efficient Operations:**
   - `ConcurrentSkipListSet` provides efficient logarithmic time complexity for basic operations like add, remove, and contains, even under concurrent access.
   - The skip list structure allows for efficient navigation and manipulation of elements.

5. **Scalability:**
   - `ConcurrentSkipListSet` is designed for scalability and performs well under high contention scenarios.
   - It uses lock striping and other techniques to allow concurrent access to different parts of the skip list.

6. **Not Guaranteed for Concurrent Iteration:**
   - While individual operations are thread-safe, concurrent iteration over a `ConcurrentSkipListSet` may not be strongly consistent.
   - The iterator may reflect some modifications made after the iterator was created, but it will never throw `ConcurrentModificationException`.

### ConcurrentSkipListSet Operations:

`ConcurrentSkipListSet` supports all operations provided by the `NavigableSet` interface. Some of the key operations include:

- **Adding Elements:**
   - `add(element)`: Adds the specified element to the set if it is not already present.
   - `addAll(collection)`: Adds all of the elements in the specified collection to the set.

- **Removing Elements:**
   - `remove(element)`: Removes the specified element from the set if it is present.
   - `removeAll(collection)`: Removes all elements from the set that are present in the specified collection.

- **Access and Retrieval:**
   - `contains(element)`: Returns true if the set contains the specified element, false otherwise.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Size and Emptyness:**
   - `size()`: Returns the number of elements in the set.
   - `isEmpty()`: Returns true if the set contains no elements, false otherwise.

- **Iteration and Bulk Operations:**
   - `iterator()`: Returns an iterator over the elements in the set.
   - `forEach(action)`: Performs the given action for each element of the set until all elements have been processed or the action throws an exception.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (add, remove, contains):** O(log n) - logarithmic time complexity for basic operations, even under concurrent access.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the set.
- **Iteration Operation (iterator, forEach):** Not strongly consistent in a concurrent environment.

### Usage:

- Use `ConcurrentSkipListSet` when you need a concurrent, thread-safe implementation of a sorted set with efficient operations under concurrent access.
- It is suitable for scenarios where multiple threads need to access and modify a sorted set concurrently without the need for external synchronization.
- Consider the performance characteristics and thread safety requirements of your application when choosing `ConcurrentSkipListSet` as the set implementation.

# Map
A `Map` in Java represents a collection of key-value pairs where each key is unique. It is part of the Java Collections Framework and provides a way to store and manipulate associations between keys and values. Here's an overview:

### Map Interface:

1. **Key-Value Pairs:**
   - A `Map` stores elements as key-value pairs, where each key is unique and associated with a single value.
   - Keys are used to retrieve values from the map, similar to looking up values in a dictionary.

2. **Uniqueness of Keys:**
   - Each key in a `Map` must be unique; attempting to add a duplicate key will overwrite the existing value associated with that key.

3. **Associative Data Structure:**
   - `Map` is an associative data structure, meaning it allows efficient retrieval of values based on their corresponding keys.

4. **No Duplicate Values Constraint:**
   - Unlike keys, values in a `Map` can be duplicate. Different keys can map to the same value.

5. **Mutability:**
   - `Map` implementations may or may not allow `null` keys and values, and their mutability can vary depending on the specific implementation.

### Common Implementations of Map:

1. **HashMap:**
   - Implements the `Map` interface using a hash table for storage.
   - Provides constant-time performance for basic operations (add, remove, contains), assuming a good hash function.

2. **LinkedHashMap:**
   - Extends `HashMap` to maintain insertion order of elements.
   - Provides predictable iteration order, which is the order in which entries were inserted into the map.

3. **TreeMap:**
   - Implements the `NavigableMap` interface using a self-balancing binary search tree (usually a Red-Black tree).
   - Provides a sorted order of keys, either based on their natural ordering or a specified comparator.

4. **EnumMap:**
   - A specialized implementation of `Map` for use with enum keys.
   - Extremely efficient and compact representation of enum keys as arrays.

5. **ConcurrentHashMap:**
   - A thread-safe implementation of `Map` designed for high-concurrency environments.
   - Provides scalable performance under concurrent access without the need for external synchronization.

### Usage:

- Use `Map` when you need to store and retrieve elements based on keys.
- Choose the appropriate implementation based on your requirements:
   - Use `HashMap` for general-purpose usage when you don't need ordered keys or predictable iteration order.
   - Use `LinkedHashMap` when you need to maintain insertion order or predictability in iteration order.
   - Use `TreeMap` when you need sorted order of keys or custom sorting based on a comparator.
   - Use `EnumMap` for efficient storage of enum keys.
   - Use `ConcurrentHashMap` when you need a thread-safe map with high-concurrency support.
- Consider the performance characteristics, memory usage, and concurrency requirements of your application when choosing the appropriate map implementation.

## Hashtable
`Hashtable` in Java is a legacy implementation of the `Map` interface that was part of the original Java Collections Framework. It is similar to `HashMap`, but it is synchronized, making it thread-safe for concurrent access. Here's an overview of its features:

### Hashtable Features:

1. **Hash Table Implementation:**
   - Internally, `Hashtable` is backed by a hash table data structure that stores key-value pairs.
   - It uses the hash code of keys to index into an array of buckets, allowing for efficient retrieval of values based on keys.

2. **Synchronization:**
   - `Hashtable` is synchronized, meaning it is thread-safe for concurrent access from multiple threads.
   - All methods in `Hashtable` are synchronized, which ensures that only one thread can access the table's methods at a time.

3. **Key-Value Pairs:**
   - `Hashtable` stores elements as key-value pairs, where each key is unique and maps to a single value.
   - Keys are used to retrieve values from the map, similar to looking up values in a dictionary.

4. **Uniqueness of Keys:**
   - Each key in a `Hashtable` must be unique; attempting to add a duplicate key will overwrite the existing value associated with that key.

5. **Null Keys and Values:**
   - `Hashtable` does not allow `null` keys or values. Attempting to insert `null` key or value will result in a `NullPointerException`.

6. **Performance Characteristics:**
   - Due to synchronization, `Hashtable` may have slower performance compared to `HashMap` for single-threaded applications.
   - It provides constant-time performance for basic operations (add, remove, contains) on average, assuming a good hash function and uniform distribution of elements.

### Hashtable Operations:

`Hashtable` supports all operations provided by the `Map` interface. Some of the key operations include:

- **Adding Key-Value Pairs:**
   - `put(key, value)`: Associates the specified value with the specified key in the map.
   - `putAll(map)`: Adds all key-value pairs from the specified map to this map.

- **Removing Key-Value Pairs:**
   - `remove(key)`: Removes the mapping for the specified key from the map if it is present.
   - `clear()`: Removes all mappings from the map.

- **Access and Retrieval:**
   - `get(key)`: Returns the value to which the specified key is mapped, or `null` if this map contains no mapping for the key.
   - `containsKey(key)`: Returns true if this map contains a mapping for the specified key.

- **Size and Emptyness:**
   - `size()`: Returns the number of key-value mappings in the map.
   - `isEmpty()`: Returns true if the map contains no key-value mappings.

- **Iteration and Bulk Operations:**
   - `keys()`: Returns an enumeration of the keys contained in the map.
   - `elements()`: Returns an enumeration of the values contained in the map.
   - `entrySet()`: Returns a set view of the key-value mappings contained in the map.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (put, remove, get):** O(1) - constant time complexity on average, assuming a good hash function and uniform distribution of elements.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the map.
- **Iteration Operation (keys, elements, entrySet):** O(n) - linear time complexity, where n is the number of key-value mappings in the map, as it iterates over all mappings.

### Usage:

- Use `Hashtable` when you need a thread-safe map implementation for concurrent access from multiple threads.
- It is suitable for legacy code or scenarios where synchronization is required but consider using `ConcurrentHashMap` for better performance in modern applications.
- Avoid using `Hashtable` in performance-sensitive applications due to its synchronization overhead.

## HashMap
`HashMap` in Java is an implementation of the `Map` interface that stores key-value pairs using a hash table. It provides fast performance for basic operations like adding, removing, and retrieving elements, assuming a good hash function. Here's an overview of its features:

### HashMap Features:

1. **Hash Table Implementation:**
   - Internally, `HashMap` is backed by a hash table, which stores key-value pairs based on their hash codes.
   - Hash tables provide constant-time performance for basic operations (add, remove, contains) on average, assuming a good hash function.

2. **Key-Value Pairs:**
   - `HashMap` stores elements as key-value pairs, where each key is unique and maps to a single value.
   - Keys are used to retrieve values from the map, similar to looking up values in a dictionary.

3. **Uniqueness of Keys:**
   - Each key in a `HashMap` must be unique; attempting to add a duplicate key will overwrite the existing value associated with that key.

4. **Null Keys and Values:**
   - `HashMap` allows one null key and multiple null values.
   - Both keys and values can be `null`, but the use of `null` keys should be done with caution to avoid unexpected behavior.

5. **Not Synchronized:**
   - `HashMap` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.
   - For concurrent access, use `Collections.synchronizedMap()` to create a synchronized map.

6. **Performance Characteristics:**
   - `HashMap` provides constant-time performance for basic operations like add, remove, and contains, assuming a good hash function and uniform distribution of elements.

### HashMap Operations:

`HashMap` supports all operations provided by the `Map` interface. Some of the key operations include:

- **Adding Key-Value Pairs:**
   - `put(key, value)`: Associates the specified value with the specified key in the map.
   - `putAll(map)`: Adds all key-value pairs from the specified map to this map.

- **Removing Key-Value Pairs:**
   - `remove(key)`: Removes the mapping for the specified key from the map if it is present.
   - `clear()`: Removes all mappings from the map.

- **Access and Retrieval:**
   - `get(key)`: Returns the value to which the specified key is mapped, or `null` if this map contains no mapping for the key.
   - `containsKey(key)`: Returns true if this map contains a mapping for the specified key.

- **Size and Emptyness:**
   - `size()`: Returns the number of key-value mappings in the map.
   - `isEmpty()`: Returns true if the map contains no key-value mappings.

- **Iteration and Bulk Operations:**
   - `keySet()`: Returns a set view of the keys contained in the map.
   - `values()`: Returns a collection view of the values contained in the map.
   - `entrySet()`: Returns a set view of the key-value mappings contained in the map.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (put, remove, get):** O(1) - constant time complexity on average, assuming a good hash function and uniform distribution of elements.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the map.
- **Iteration Operation (keySet, values, entrySet):** O(n) - linear time complexity, where n is the number of key-value mappings in the map, as it iterates over all mappings.

### Usage:

- Use `HashMap` when you need a fast and efficient way to store and retrieve key-value pairs.
- It is suitable for general-purpose usage and provides constant-time performance for most operations.
- Consider the characteristics of `HashMap`, such as its hash table implementation, lack of synchronization, and behavior with null keys and values, when choosing it as the map implementation for your application.

## LinkedHashMap
`LinkedHashMap` in Java is an implementation of the `Map` interface that extends `HashMap` to maintain the insertion order of elements. It provides predictable iteration order, which is the order in which entries were inserted into the map. Here's an overview of its features:

### LinkedHashMap Features:

1. **Hash Table with Linked List:**
   - Internally, `LinkedHashMap` combines the features of a hash table and a linked list.
   - It maintains a hash table for fast lookup and a linked list for maintaining the insertion order of elements.

2. **Predictable Iteration Order:**
   - Unlike `HashMap`, which does not guarantee any specific order of iteration, `LinkedHashMap` guarantees the iteration order to be the same as the order of insertion.
   - Iterating over a `LinkedHashMap` will always return elements in the order they were added.

3. **Key-Value Pairs:**
   - `LinkedHashMap` stores elements as key-value pairs, where each key is unique and maps to a single value.
   - Keys are used to retrieve values from the map, similar to looking up values in a dictionary.

4. **Uniqueness of Keys:**
   - Each key in a `LinkedHashMap` must be unique; attempting to add a duplicate key will overwrite the existing value associated with that key.

5. **Null Keys and Values:**
   - `LinkedHashMap` allows one `null` key and multiple `null` values.
   - Both keys and values can be `null`, but the use of `null` keys should be done with caution to avoid unexpected behavior.

6. **Not Synchronized:**
   - Like `HashMap`, `LinkedHashMap` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.
   - For concurrent access, use `Collections.synchronizedMap()` to create a synchronized map.

### LinkedHashMap Operations:

`LinkedHashMap` supports all operations provided by the `Map` interface, as it extends `HashMap`. Some of the key operations include:

- **Adding Key-Value Pairs:**
   - `put(key, value)`: Associates the specified value with the specified key in the map.
   - `putAll(map)`: Adds all key-value pairs from the specified map to this map.

- **Removing Key-Value Pairs:**
   - `remove(key)`: Removes the mapping for the specified key from the map if it is present.
   - `clear()`: Removes all mappings from the map.

- **Access and Retrieval:**
   - `get(key)`: Returns the value to which the specified key is mapped, or `null` if this map contains no mapping for the key.
   - `containsKey(key)`: Returns true if this map contains a mapping for the specified key.

- **Size and Emptyness:**
   - `size()`: Returns the number of key-value mappings in the map.
   - `isEmpty()`: Returns true if the map contains no key-value mappings.

- **Iteration and Bulk Operations:**
   - `keySet()`: Returns a set view of the keys contained in the map.
   - `values()`: Returns a collection view of the values contained in the map.
   - `entrySet()`: Returns a set view of the key-value mappings contained in the map.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (put, remove, get):** O(1) - constant time complexity on average, assuming a good hash function and uniform distribution of elements.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the map.
- **Iteration Operation (keySet, values, entrySet):** O(n) - linear time complexity, where n is the number of key-value mappings in the map, as it iterates over all mappings.

### Usage:

- Use `LinkedHashMap` when you need a map implementation that maintains insertion order or requires predictable iteration order.
- It is suitable for scenarios where you need to iterate over the map in the same order as elements were added or when maintaining the order of elements is important.
- Consider the characteristics of `LinkedHashMap`, such as predictable iteration order and performance characteristics, when choosing it as the map implementation for your application.

## SortedMap vs NavigableMap vs TreeMap

`SortedMap`, `NavigableMap`, and `TreeMap` are related interfaces and classes in Java that deal with sorted collections of key-value pairs. Here's an explanation of each:

### SortedMap Interface:

- **Purpose:** Represents a map that maintains its keys in sorted order.
- **Inheritance:** Extends the `Map` interface.
- **Features:**
   - Provides methods for retrieving the first and last keys, as well as a view of the portion of the map whose keys are strictly less than or greater than a specified key.
   - Does not define any methods beyond those in the `Map` interface, but inherits additional methods from `Collection` and `Iterable`.

### NavigableMap Interface:

- **Purpose:** Represents a map with navigation methods for moving through the map and performing navigational operations based on the order of keys.
- **Inheritance:** Extends the `SortedMap` interface.
- **Features:**
   - Adds methods for navigation, such as `lowerKey()`, `floorKey()`, `ceilingKey()`, and `higherKey()` to retrieve keys based on their relationship to a given key.
   - Provides methods for obtaining reverse views of the map.
   - Introduces methods for navigating and manipulating the map beyond those inherited from `SortedMap`.

### TreeMap Class:

- **Purpose:** Implements the `NavigableMap` interface using a self-balancing binary search tree, usually a Red-Black tree.
- **Features:**
   - Maintains its keys in sorted order, allowing efficient retrieval and manipulation operations.
   - Provides guaranteed log(n) time complexity for basic operations like add, remove, and contains, where n is the number of key-value mappings in the map.
   - Supports navigational operations provided by the `NavigableMap` interface, such as finding keys greater than or equal to a given key.

### When to Use Each:

- **SortedMap:** Use when you need a map that maintains its keys in sorted order and only require basic map operations. It provides basic sorted map functionality without additional navigational methods.
- **NavigableMap:** Use when you need navigation methods for moving through the map and performing operations based on the order of keys. It provides additional navigational methods beyond those offered by `SortedMap`.
- **TreeMap:** Use when you need a map that maintains its keys in sorted order and supports efficient navigation and manipulation operations. It is backed by a self-balancing binary search tree, providing efficient performance for most operations.

### Summary:

- Use `SortedMap` for basic sorted map functionality without navigation methods.
- Use `NavigableMap` for navigation methods and additional map manipulation operations.
- Use `TreeMap` when you need a sorted map with efficient navigation and manipulation capabilities, backed by a self-balancing binary search tree.

Certainly! Here's the tabular representation:

| Feature               | SortedMap                                      | NavigableMap                                      | TreeMap                                          |
|-----------------------|------------------------------------------------|---------------------------------------------------|--------------------------------------------------|
| Purpose               | Represents a sorted map of key-value pairs     | Represents a navigable map with additional methods | Implements a navigable map using a binary tree   |
| Interface             | Extends `Map`                                   | Extends `SortedMap`                               | Implements `NavigableMap`                         |
| Inheritance           | Inherits from `Map`, `Collection`, `Iterable`  | Inherits from `SortedMap`, `Collection`, `Iterable`| Inherits from `AbstractMap`, `NavigableMap`      |
| Sorted Order of Keys  | Maintains keys in sorted order                  | Maintains keys in sorted order                    | Maintains keys in sorted order                   |
| Navigation Methods    | No additional navigation methods               | Provides navigation methods like `lowerKey()`, `ceilingKey()`, etc. | Provides navigation methods and views        |
| Time Complexity       | O(log n) for basic operations                  | O(log n) for basic operations                     | O(log n) for basic operations                     |
| Data Structure        | Not specified                                  | Not specified                                     | Self-balancing binary search tree (Red-Black tree) |
| Use Cases             | Basic sorted map functionality                 | Navigation and manipulation based on key order    | Efficient sorted map with navigation capabilities |

This table provides a comparison of the key features and characteristics of `SortedMap`, `NavigableMap`, and `TreeMap`. Depending on your requirements, you can choose the most appropriate interface or class for your use case.

## TreeMap
`TreeMap` in Java is an implementation of the `NavigableMap` interface that stores key-value pairs in a sorted order based on the natural ordering of its keys or a custom comparator provided at map creation time. It uses a Red-Black tree data structure to maintain the keys in sorted order, providing efficient operations for retrieval and manipulation. Here's an overview of its features:

### TreeMap Features:

1. **Red-Black Tree Implementation:**
   - Internally, `TreeMap` is implemented using a self-balancing binary search tree, usually a Red-Black tree.
   - This data structure maintains the keys in sorted order, allowing efficient retrieval and manipulation operations.

2. **Sorted Order of Keys:**
   - `TreeMap` maintains its elements in sorted order based on the natural ordering of its keys or a custom comparator provided at map creation time.
   - The keys are arranged in ascending order according to their natural order or the ordering defined by the comparator.

3. **Key-Value Pairs:**
   - `TreeMap` stores elements as key-value pairs, where each key is associated with a single value.
   - Keys are used to retrieve values from the map, similar to looking up values in a dictionary.

4. **Uniqueness of Keys:**
   - Each key in a `TreeMap` must be unique; attempting to add a duplicate key will overwrite the existing value associated with that key.

5. **Null Keys and Values:**
   - `TreeMap` does not allow `null` keys, but it can contain multiple `null` values.
   - The use of `null` keys should be avoided, as it will result in a `NullPointerException`.

6. **Not Synchronized:**
   - Like `HashMap` and `LinkedHashMap`, `TreeMap` is not synchronized, meaning it is not thread-safe for concurrent access without external synchronization mechanisms.
   - For concurrent access, use `Collections.synchronizedMap()` to create a synchronized map.

### TreeMap Operations:

`TreeMap` supports all operations provided by the `NavigableMap` interface. Some of the key operations include:

- **Adding Key-Value Pairs:**
   - `put(key, value)`: Associates the specified value with the specified key in the map.
   - `putAll(map)`: Adds all key-value pairs from the specified map to this map.

- **Removing Key-Value Pairs:**
   - `remove(key)`: Removes the mapping for the specified key from the map if it is present.
   - `clear()`: Removes all mappings from the map.

- **Access and Retrieval:**
   - `get(key)`: Returns the value to which the specified key is mapped, or `null` if this map contains no mapping for the key.
   - `containsKey(key)`: Returns true if this map contains a mapping for the specified key.

- **Size and Emptyness:**
   - `size()`: Returns the number of key-value mappings in the map.
   - `isEmpty()`: Returns true if the map contains no key-value mappings.

- **Iteration and Bulk Operations:**
   - `keySet()`: Returns a set view of the keys contained in the map.
   - `values()`: Returns a collection view of the values contained in the map.
   - `entrySet()`: Returns a set view of the key-value mappings contained in the map.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (put, remove, get):** O(log n) - logarithmic time complexity for basic operations, where n is the number of key-value mappings in the map.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the map.
- **Iteration Operation (keySet, values, entrySet):** O(n) - linear time complexity, where n is the number of key-value mappings in the map, as it iterates over all mappings.

### Usage:

- Use `TreeMap` when you need a map implementation that maintains keys in sorted order or requires custom sorting based on a comparator.
- It is suitable for scenarios where you need efficient search operations and maintaining elements in sorted order.
- Consider the characteristics of `TreeMap`, such as sorted order of keys and performance characteristics, when choosing it as the map implementation for your application.

## ConcurrentHashMap
`ConcurrentHashMap` in Java is a thread-safe implementation of the `Map` interface designed for high-concurrency environments. It provides scalability and concurrent access without the need for external synchronization mechanisms. Here's an overview of its features:

### ConcurrentHashMap Features:

1. **Concurrent Access:**
   - `ConcurrentHashMap` supports concurrent access from multiple threads without the need for external synchronization.
   - It provides high concurrency and scalability, allowing multiple threads to read and write to the map concurrently.

2. **Partitioned Segments:**
   - Internally, `ConcurrentHashMap` divides the map into segments, each of which acts as an independent hash table.
   - Segmentation reduces contention by allowing multiple threads to access different segments simultaneously.

3. **Fine-Grained Locking:**
   - Each segment in `ConcurrentHashMap` is guarded by a separate lock, enabling fine-grained locking at the segment level.
   - Fine-grained locking minimizes contention and allows for efficient concurrent access to different parts of the map.

4. **Thread-Safe Operations:**
   - `ConcurrentHashMap` provides thread-safe operations for basic map operations like put, remove, and get.
   - Concurrent read and write operations do not block each other, improving throughput and reducing contention.

5. **Scalability:**
   - `ConcurrentHashMap` is designed for scalability and performs well under high contention scenarios.
   - It dynamically adjusts its internal structure to accommodate the number of threads accessing the map, ensuring efficient concurrent access.

6. **Supports Null Keys and Values:**
   - `ConcurrentHashMap` allows one null key and multiple null values.
   - Both keys and values can be `null`, but the use of `null` keys should be done with caution to avoid unexpected behavior.

### ConcurrentHashMap Operations:

`ConcurrentHashMap` supports all operations provided by the `Map` interface. Some of the key operations include:

- **Adding Key-Value Pairs:**
   - `put(key, value)`: Associates the specified value with the specified key in the map.
   - `putIfAbsent(key, value)`: Associates the specified value with the specified key if the key is not already associated with a value.

- **Removing Key-Value Pairs:**
   - `remove(key)`: Removes the mapping for the specified key from the map if it is present.
   - `remove(key, value)`: Removes the mapping for the specified key only if it is currently mapped to the specified value.

- **Access and Retrieval:**
   - `get(key)`: Returns the value to which the specified key is mapped, or `null` if this map contains no mapping for the key.
   - `containsKey(key)`: Returns true if this map contains a mapping for the specified key.

- **Size and Emptyness:**
   - `size()`: Returns the number of key-value mappings in the map.
   - `isEmpty()`: Returns true if the map contains no key-value mappings.

- **Iteration and Bulk Operations:**
   - `keySet()`: Returns a set view of the keys contained in the map.
   - `values()`: Returns a collection view of the values contained in the map.
   - `entrySet()`: Returns a set view of the key-value mappings contained in the map.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (put, remove, get):** O(1) - constant time complexity on average, assuming a good hash function and uniform distribution of elements.
- **Size Operation (size):** O(n) - linear time complexity, where n is the number of key-value mappings in the map, due to the need to traverse all segments.
- **Iteration Operation (keySet, values, entrySet):** O(n) - linear time complexity, where n is the number of key-value mappings in the map, as it iterates over all mappings.

### Usage:

- Use `ConcurrentHashMap` when you need a thread-safe map implementation for high-concurrency environments.
- It is suitable for scenarios where multiple threads need to read and write to the map concurrently without the need for external synchronization.
- Consider the performance characteristics, scalability, and thread safety requirements of your application when choosing `ConcurrentHashMap` as the map implementation.

## ConcurrentSkipListMap
`ConcurrentSkipListMap` is a concurrent, thread-safe implementation of the `NavigableMap` interface introduced in Java 6. It is based on a skip list data structure, which allows for efficient insertion, deletion, and search operations with logarithmic time complexity. Here's an overview of its features:

### ConcurrentSkipListMap Features:

1. **Concurrent Access:**
   - `ConcurrentSkipListMap` is designed for concurrent access from multiple threads without the need for external synchronization.
   - It provides thread-safe operations for insertion, removal, and retrieval of key-value pairs.

2. **Skip List Data Structure:**
   - Internally, `ConcurrentSkipListMap` is implemented using a skip list data structure.
   - Skip lists are probabilistic data structures that maintain a sorted map of key-value pairs with logarithmic time complexity for most operations.

3. **Sorted Order of Keys:**
   - Similar to other sorted map implementations, `ConcurrentSkipListMap` maintains its keys in sorted order.
   - The keys are arranged according to their natural ordering or a custom comparator provided during map creation.

4. **Efficient Operations:**
   - `ConcurrentSkipListMap` provides efficient logarithmic time complexity for basic operations like add, remove, and contains, even under concurrent access.
   - The skip list structure allows for efficient navigation and manipulation of key-value pairs.

5. **Scalability:**
   - `ConcurrentSkipListMap` is designed for scalability and performs well under high contention scenarios.
   - It uses lock striping and other techniques to allow concurrent access to different parts of the skip list.

6. **Not Guaranteed for Concurrent Iteration:**
   - While individual operations are thread-safe, concurrent iteration over a `ConcurrentSkipListMap` may not be strongly consistent.
   - The iterator may reflect some modifications made after the iterator was created, but it will never throw `ConcurrentModificationException`.

### ConcurrentSkipListMap Operations:

`ConcurrentSkipListMap` supports all operations provided by the `NavigableMap` interface. Some of the key operations include:

- **Adding Key-Value Pairs:**
   - `put(key, value)`: Associates the specified value with the specified key in the map.
   - `putAll(map)`: Adds all key-value pairs from the specified map to this map.

- **Removing Key-Value Pairs:**
   - `remove(key)`: Removes the mapping for the specified key from the map if it is present.
   - `clear()`: Removes all mappings from the map.

- **Access and Retrieval:**
   - `get(key)`: Returns the value to which the specified key is mapped, or `null` if this map contains no mapping for the key.
   - `containsKey(key)`: Returns true if this map contains a mapping for the specified key.

- **Size and Emptyness:**
   - `size()`: Returns the number of key-value mappings in the map.
   - `isEmpty()`: Returns true if the map contains no key-value mappings.

- **Iteration and Bulk Operations:**
   - `keySet()`: Returns a navigable set view of the keys contained in the map.
   - `values()`: Returns a collection view of the values contained in the map.
   - `entrySet()`: Returns a set view of the key-value mappings contained in the map.

### Big-O Notation Time Complexity:

- **Insertion, Removal, and Search Operations (put, remove, get):** O(log n) - logarithmic time complexity for basic operations, even under concurrent access.
- **Size Operation (size):** O(1) - constant time complexity for accessing the size of the map.
- **Iteration Operation (keySet, values, entrySet):** Not strongly consistent in a concurrent environment.

### Usage:

- Use `ConcurrentSkipListMap` when you need a concurrent, thread-safe implementation of a sorted map with efficient operations under concurrent access.
- It is suitable for scenarios where multiple threads need to access and modify a sorted map concurrently without the need for external synchronization.
- Consider the performance characteristics and thread safety requirements of your application when choosing `ConcurrentSkipListMap` as the map implementation.

## EnumMap vs IdentityHashMap vs WeakHashMap
`EnumMap`, `IdentityHashMap`, and `WeakHashMap` are different implementations of the `Map` interface in Java, each designed for specific use cases. Let's compare them based on their features and typical usage scenarios:

### EnumMap:

1. **Key Restriction:**
   - `EnumMap` is designed to be used with enum keys exclusively.
   - It provides a high-performance implementation for enum-based maps.

2. **Memory Efficiency:**
   - `EnumMap` is highly memory-efficient, as it is internally represented as an array with enum ordinal values as indices.

3. **No Null Keys:**
   - `EnumMap` does not allow `null` keys, as it is specifically designed for enum keys.

4. **Performance:**
   - `EnumMap` provides very fast performance for enum-based mappings, with constant-time complexity for basic operations.

5. **Usage:**
   - Use `EnumMap` when you have a map with enum keys and want maximum performance and memory efficiency.

### IdentityHashMap:

1. **Identity-Based Equality:**
   - `IdentityHashMap` uses reference equality (`==`) rather than object equality (`equals()` method) for comparing keys.
   - It compares keys based on their memory addresses, making it useful for scenarios where you need to distinguish between equal objects.

2. **Null Keys and Values:**
   - `IdentityHashMap` allows both `null` keys and `null` values.

3. **Performance:**
   - `IdentityHashMap` provides linear-time complexity for most operations due to the identity-based comparison.
   - Performance may degrade with a large number of elements.

4. **Usage:**
   - Use `IdentityHashMap` when you need identity-based comparison of keys or when you want to use objects with custom equality semantics.

### WeakHashMap:

1. **Weak References:**
   - `WeakHashMap` holds weak references to its keys, allowing them to be garbage-collected when no longer referenced elsewhere in the application.
   - This makes it useful for implementing caches and other data structures where you want entries to be automatically removed when no longer needed.

2. **Memory Efficiency:**
   - `WeakHashMap` can help prevent memory leaks by automatically removing entries associated with keys that have been garbage-collected.

3. **Performance:**
   - Performance of `WeakHashMap` may be slower compared to other map implementations due to the additional overhead of managing weak references.

4. **Null Keys and Values:**
   - `WeakHashMap` allows both `null` keys and `null` values.

5. **Usage:**
   - Use `WeakHashMap` when you need a map that automatically removes entries when keys are no longer referenced elsewhere in the application, preventing memory leaks.

### Comparison Summary:

- Use `EnumMap` for high-performance enum-based mappings with no `null` keys.
- Use `IdentityHashMap` for identity-based comparison of keys or when you need to distinguish between equal objects based on their memory addresses.
- Use `WeakHashMap` when you need a map that automatically removes entries associated with keys that have been garbage-collected, helping to prevent memory leaks.

Here's a tabular comparison of `EnumMap`, `IdentityHashMap`, and `WeakHashMap` based on their features and usage:

| Feature             | EnumMap                           | IdentityHashMap                  | WeakHashMap                      |
|---------------------|-----------------------------------|----------------------------------|----------------------------------|
| Key Restriction     | Exclusive to enum keys            | Any object as keys               | Any object as keys               |
| Memory Efficiency  | Highly efficient                  | Moderate                         | Moderate                         |
| Null Keys Allowed   | No                                | Yes                              | Yes                              |
| Equality Comparison | Uses enum ordinal values         | Uses reference equality          | Uses reference equality          |
| Garbage Collection  | No                               | No                               | Automatically removes entries when keys are garbage-collected |
| Performance         | Fast, constant-time complexity   | Moderate, linear-time complexity | Moderate, additional overhead due to weak references |
| Typical Use Case    | Enum-based mappings               | Identity-based comparison of keys | Preventing memory leaks by removing entries |

This table provides a concise comparison of `EnumMap`, `IdentityHashMap`, and `WeakHashMap` based on their key features, usage scenarios, and performance characteristics. Depending on your specific requirements, you can choose the most suitable map implementation for your application.

# Data Structure Concepts

## Internal working of LinkedList
The internal workings of a `LinkedList` in Java involve nodes linked together in a linear sequence. Here's how it typically operates:

### Node Structure:

1. **Node Object:**
   - Each element in a `LinkedList` is represented by a node object.
   - The node object contains two fields:
      - Data: Holds the value of the element.
      - Next: A reference to the next node in the sequence.

2. **Doubly Linked List:**
   - In a doubly linked list, each node also contains a reference to the previous node in the sequence.
   - This allows traversal of the list in both forward and backward directions.

### List Structure:

3. **Head and Tail References:**
   - The `LinkedList` class maintains references to the first and last nodes in the list, often called the head and tail, respectively.
   - These references allow efficient insertion and removal operations at both ends of the list.

4. **Insertion and Removal:**
   - Inserting an element involves creating a new node, updating references to link it into the list, and adjusting the head and tail references if necessary.
   - Removing an element involves updating references to bypass the node to be removed and adjusting the head and tail references if necessary.

### Performance Characteristics:

5. **Accessing Elements:**
   - Accessing elements by index in a `LinkedList` requires traversing the list from either the head or tail, which has a time complexity of O(n).
   - This is less efficient than accessing elements by index in an array-based list like `ArrayList` (O(1)).

6. **Insertion and Removal:**
   - Insertion and removal operations at the beginning or end of a `LinkedList` have constant-time complexity (O(1)).
   - Insertion and removal operations in the middle of the list require traversing to the insertion/removal point, which has a time complexity of O(n/2) on average.

7. **Memory Overhead:**
   - `LinkedList` consumes more memory compared to array-based lists like `ArrayList` due to the overhead of maintaining node objects and references.

### Iteration:

8. **Traversal:**
   - Iterating over a `LinkedList` involves starting at the head or tail and following the next or previous references to visit each node in sequence.
   - This traversal process has a time complexity of O(n) since it visits each node once.

### Summary:

- `LinkedList` is a linear data structure composed of nodes linked together.
- Each node contains data and references to the next and previous nodes.
- Insertion and removal operations are efficient at both ends of the list but less efficient in the middle.
- Accessing elements by index has a time complexity of O(n).
- `LinkedList` is suitable for scenarios where frequent insertion and removal operations are performed at the beginning or end of the list and when random access by index is not a primary concern.

## Internal working of HashMap
The internal workings of a `HashMap` in Java involve several key components and concepts. Here's a breakdown of how it typically operates:

### Hashing:

1. **Hash Function:**
   - When you add a key-value pair to a `HashMap`, the key's hash code is computed using the `hashCode()` method.
   - The hash code is then passed through a hash function to determine the index (bucket) where the key-value pair will be stored in the underlying array.

### Buckets and Arrays:

2. **Array of Buckets:**
   - Internally, a `HashMap` maintains an array of buckets (also called bins or slots) to store key-value pairs.
   - The array size is typically a power of two, allowing for efficient computation of the index using bitwise operations.

3. **Index Calculation:**
   - To determine the index where a key-value pair will be stored, the hash code is modified to ensure it falls within the valid range of indices (0 to array length - 1).
   - This is typically done by applying a bitwise AND operation with the array length minus one, ensuring the hash code wraps around if it exceeds the array size.

### Collision Resolution:

4. **Handling Collisions:**
   - Collisions occur when different keys hash to the same index (bucket) in the array.
   - To handle collisions, `HashMap` uses a linked list (or in Java 8 and later, a balanced tree) at each bucket to store multiple key-value pairs that hash to the same index.
   - Each node in the linked list (or tree) contains a key-value pair along with a reference to the next node (if any).

### Load Factor and Rehashing:

5. **Load Factor:**
   - `HashMap` maintains a load factor threshold, which is the ratio of the number of key-value mappings to the array size.
   - When the load factor exceeds a certain threshold (typically 0.75), the `HashMap` is resized and rehashed to maintain efficiency.

6. **Rehashing:**
   - Rehashing involves creating a new, larger array of buckets and redistributing the existing key-value pairs into the new array based on their updated hash codes.
   - The array size is typically doubled during rehashing to maintain a good balance between space and time complexity.

### Performance Characteristics:

7. **Time Complexity:**
   - In the best case, adding, removing, and accessing elements in a `HashMap` has constant-time complexity (O(1)).
   - In the worst case, when many keys hash to the same index and form long linked lists, the time complexity for these operations degrades to linear (O(n)).
   - The average-case time complexity for basic operations is O(1), assuming a good hash function and uniform distribution of keys.

### Internal Improvements in Java 8 and Later:

8. **Tree Node:**
   - In Java 8 and later, if the number of elements in a bucket exceeds a certain threshold, the linked list is transformed into a balanced tree (red-black tree).
   - This improves worst-case performance from O(n) to O(log n) for operations like adding, removing, and accessing elements in a bucket.

### Summary:

- `HashMap` uses hashing to store and retrieve key-value pairs efficiently.
- It handles collisions using linked lists (or trees) at each bucket.
- Rehashing is performed to maintain a low load factor and ensure efficient performance.
- Internal improvements in Java 8 and later, such as tree nodes, further optimize performance for large hash collisions.

## Internal working of LinkedHashMap
The internal working of a `LinkedHashMap` in Java is similar to that of a regular `HashMap`, with the addition of maintaining a doubly linked list alongside the hash table to preserve insertion order. Here's a breakdown of how it typically operates:

### Hashing and Buckets:

1. **Hash Function and Array:**
   - Like `HashMap`, `LinkedHashMap` uses a hash function to compute the index (bucket) where each key-value pair will be stored in the underlying array.
   - It maintains an array of buckets to store these pairs.

2. **Index Calculation:**
   - The hash code of the key is computed, and the index is determined using the hash function.
   - The index calculation process is the same as in `HashMap`.

### Doubly Linked List for Order:

3. **Maintaining Order:**
   - In addition to the hash table, `LinkedHashMap` maintains a doubly linked list of entries.
   - This linked list preserves the insertion order of the key-value pairs.
   - Each entry in the linked list contains a reference to the previous and next entries, forming a chain from the first insertion to the last.

4. **Linked List Entry:**
   - Each entry in the linked list corresponds to a key-value pair stored in the hash table.
   - The linked list is updated whenever a new key-value pair is added to the map or when an existing pair is accessed or modified.

### Performance Characteristics:

5. **Time Complexity:**
   - `LinkedHashMap` provides the same time complexity as `HashMap` for basic operations such as adding, removing, and accessing elements.
   - In the best case, these operations have constant-time complexity (O(1)).
   - In the worst case, when many keys hash to the same index and form long linked lists, the time complexity degrades to linear (O(n)).
   - Iterating over the map, however, is more efficient in `LinkedHashMap` since it maintains insertion order.

### Internal Improvements:

6. **Enhanced Iteration:**
   - Because of the linked list, iterating over a `LinkedHashMap` returns elements in the order they were inserted.
   - This is in contrast to `HashMap`, where the iteration order is not specified.

### Summary:

- `LinkedHashMap` extends the functionality of `HashMap` by maintaining a doubly linked list alongside the hash table.
- This linked list preserves the insertion order of key-value pairs, allowing for predictable iteration order.
- The underlying hash table structure remains the same, with key-value pairs stored in buckets based on their hash codes.
- The combination of hash table and linked list provides efficient performance for basic operations and predictable iteration order, making `LinkedHashMap` suitable for scenarios where both order preservation and fast access are important.

## Internal working of ConcurrentHashMap
The internal workings of a `ConcurrentHashMap` in Java involve several key components and concepts to ensure thread-safe concurrent access and efficient performance. Here's a breakdown of how it typically operates:

### Segmented Design:

1. **Segmented Structure:**
   - `ConcurrentHashMap` divides its underlying data structure into multiple segments (or partitions).
   - Each segment operates as an independent hash table, allowing concurrent access to different segments without blocking each other.

2. **Lock Striping:**
   - Lock striping is used to control access to each segment.
   - Instead of having a single lock for the entire map, `ConcurrentHashMap` employs multiple locks (one per segment) to allow concurrent access to different segments.

### Hash Table:

3. **Hash Table per Segment:**
   - Each segment contains its own hash table data structure.
   - This hash table stores key-value pairs, similar to a regular `HashMap`.

4. **Bucketized Entries:**
   - Each bucket in the hash table contains a linked list (or a tree in Java 8 and later) of key-value pairs.
   - Collisions are handled within each bucket using these linked lists or trees.

### Operations:

5. **Concurrent Access:**
   - `ConcurrentHashMap` allows multiple threads to read and write concurrently without external synchronization.
   - Threads accessing different segments can operate independently without blocking each other.

6. **Read Operations:**
   - Read operations (such as `get`) are usually lock-free and can be performed concurrently across segments.

7. **Write Operations:**
   - Write operations (such as `put`, `remove`) typically require locking the affected segment.
   - Locking is done at the segment level, allowing other threads to concurrently access different segments.

### Resizing:

8. **Dynamic Sizing:**
   - `ConcurrentHashMap` dynamically adjusts its size (number of segments) based on the number of elements and concurrency level.
   - When resizing, new segments are added or existing segments are split to distribute the load more evenly.

9. **Array of Segments:**
   - Internally, `ConcurrentHashMap` maintains an array of segments to store its data.
   - The array size is typically a power of two, allowing for efficient computation of the segment index using bitwise operations.

### Performance Characteristics:

10. **Load Factor:**
   - `ConcurrentHashMap` maintains a load factor threshold for each segment to control resizing.
   - When the load factor exceeds a certain threshold, the segment is resized to maintain efficiency.

11. **Concurrent Iteration:**
   - Iterating over a `ConcurrentHashMap` may or may not reflect the most recent updates, as it does not provide strong consistency guarantees during iteration.
   - However, iteration is typically fast and doesn't require locking.

### Summary:

- `ConcurrentHashMap` employs a segmented design with lock striping to allow concurrent access to different segments.
- Each segment contains its own hash table, allowing concurrent operations without blocking each other.
- Read operations are usually lock-free, while write operations require segment-level locking.
- Dynamic resizing and load balancing ensure efficient performance and scalability.
- `ConcurrentHashMap` is suitable for scenarios requiring high concurrency and performance, such as multi-threaded applications with frequent read and write operations.

## Internal Working of TreeMap
The internal workings of a `TreeMap` in Java involve a self-balancing binary search tree (usually a Red-Black tree) to maintain the elements in sorted order. Here's how it typically operates:

### Binary Search Tree:

1. **Node Structure:**
   - Each element in a `TreeMap` is represented by a node in the binary search tree.
   - Each node contains a key-value pair and references to its left child, right child, and parent nodes.

2. **Binary Search Property:**
   - The elements in the tree are organized such that for any given node:
      - All keys in the left subtree are less than the key of the node.
      - All keys in the right subtree are greater than the key of the node.
   - This property allows for efficient searching, insertion, and removal operations.

### Red-Black Tree:

3. **Balanced Tree:**
   - `TreeMap` typically uses a Red-Black tree as its underlying data structure.
   - Red-Black trees are self-balancing binary search trees that ensure the tree remains balanced during insertions and removals.
   - Balancing helps maintain efficient performance for various operations.

4. **Coloring and Rotations:**
   - Red-Black trees use coloring and rotations to maintain balance.
   - Each node in the tree is colored red or black, and specific rules are enforced to ensure balance is maintained after insertions and removals.

### Operations:

5. **Insertion:**
   - When adding a new element to the `TreeMap`, it is inserted into the tree based on its key.
   - The tree is then rebalanced if necessary to maintain the Red-Black tree properties.

6. **Removal:**
   - Removing an element from the `TreeMap` involves finding the node containing the key to be removed and adjusting the tree structure accordingly.
   - The tree is rebalanced after removal to maintain balance and ensure efficient performance.

7. **Search:**
   - Searching for an element in the `TreeMap` involves traversing the tree from the root node based on the key comparison.
   - Binary search is performed to locate the node containing the desired key.

### Iteration:

8. **In-order Traversal:**
   - Iterating over a `TreeMap` returns elements in sorted order based on their keys.
   - This is achieved by performing an in-order traversal of the binary search tree, which visits nodes in ascending order of keys.

### Performance Characteristics:

9. **Balanced Tree:**
   - Red-Black trees ensure the height of the tree remains balanced, resulting in efficient performance for various operations.
   - The height of a Red-Black tree with n nodes is O(log n), providing efficient search, insertion, and removal operations.

### Summary:

- `TreeMap` uses a Red-Black tree as its underlying data structure to maintain elements in sorted order.
- Red-Black trees ensure balance through coloring and rotations, resulting in efficient performance for search, insertion, and removal operations.
- Iteration over a `TreeMap` returns elements in sorted order based on their keys.
- `TreeMap` is suitable for scenarios requiring sorted data and efficient search, insertion, and removal operations.

## Resizable Array
A resizable array, also known as a dynamic array or ArrayList in Java, is a data structure that combines features of arrays and linked lists. It allows for efficient insertion, deletion, and access of elements while automatically resizing itself when necessary to accommodate a variable number of elements. Here's how it typically operates:

### Internal Structure:

1. **Array-based Structure:**
   - Internally, a resizable array is based on a regular fixed-size array.
   - The array is initially allocated with a certain capacity to hold elements.

2. **Dynamic Sizing:**
   - Unlike a traditional array with a fixed size, a resizable array dynamically adjusts its capacity as elements are added or removed.
   - When the array reaches its capacity limit, it automatically reallocates a larger array and copies the existing elements into it.

### Operations:

3. **Insertion and Deletion:**
   - Adding an element to the end of a resizable array typically has constant-time complexity (O(1)), as long as the array has available space.
   - When the array needs to be resized, the time complexity for adding an element becomes O(n), where n is the number of elements in the array.
   - Removing an element from the end of the array also has constant-time complexity (O(1)).

4. **Access and Modification:**
   - Accessing and modifying elements by index in a resizable array has constant-time complexity (O(1)).
   - This is because elements are stored contiguously in memory, allowing for direct access using the index.

### Resizing Strategy:

5. **Capacity Doubling:**
   - Resizable arrays typically double their capacity when resizing.
   - This strategy ensures amortized constant-time complexity for adding elements, as resizing occurs less frequently.

6. **Shrinking:**
   - Some implementations of resizable arrays may also shrink their capacity when the number of elements decreases significantly.
   - This helps conserve memory when the array size reduces over time.

### Performance Characteristics:

7. **Amortized Constant-Time Complexity:**
   - Resizable arrays provide amortized constant-time complexity (O(1)) for adding and removing elements at the end of the array.
   - This means that on average, adding or removing an element takes constant time, even though occasional resizing may incur linear-time complexity.

### Usage:

8. **Versatility:**
   - Resizable arrays are widely used in various programming scenarios where dynamic storage is required, such as implementing lists, stacks, queues, and dynamic collections.
   - They offer a balance between the efficiency of array-based access and the flexibility of dynamic resizing.

### Summary:

- A resizable array dynamically adjusts its capacity to accommodate a variable number of elements.
- It combines the efficient access of arrays with automatic resizing to provide dynamic storage capabilities.
- Resizable arrays typically offer amortized constant-time complexity for adding and removing elements at the end of the array, making them versatile and widely used in various programming scenarios.

## Single vs Doubly LinkedList
A single linked list and a doubly linked list are both linear data structures used for storing collections of elements. They differ primarily in how they link their elements together and the operations they support. Here's a comparison between the two:

### Single Linked List:

1. **Node Structure:**
   - Each element in a single linked list is represented by a node that contains the element's value and a reference (pointer) to the next node in the sequence.

2. **Linking:**
   - Nodes are linked together in a unidirectional manner, with each node pointing to the next node in the sequence.
   - The last node typically points to null to indicate the end of the list.

3. **Traversal:**
   - Traversing a single linked list is done sequentially, starting from the head (the first node) and following the next references until the end of the list is reached.

4. **Memory Efficiency:**
   - Single linked lists require less memory per node compared to doubly linked lists since they only store references to the next node.

5. **Insertion and Deletion:**
   - Insertion and deletion operations at the beginning of a single linked list have constant-time complexity (O(1)), as they only require updating the head reference.
   - Insertion and deletion operations in the middle or end of the list have linear-time complexity (O(n)), as they require traversal to find the insertion/deletion point.

### Doubly Linked List:

1. **Node Structure:**
   - Each element in a doubly linked list is represented by a node that contains the element's value and references to both the next and previous nodes in the sequence.

2. **Linking:**
   - Nodes are linked together in a bidirectional manner, with each node pointing to both the next and previous nodes in the sequence.

3. **Traversal:**
   - Traversing a doubly linked list can be done in both forward and backward directions, as each node has references to both its next and previous nodes.

4. **Memory Efficiency:**
   - Doubly linked lists require more memory per node compared to single linked lists, as they store references to both the next and previous nodes.

5. **Insertion and Deletion:**
   - Insertion and deletion operations in a doubly linked list have constant-time complexity (O(1)) at both the beginning and end of the list, as they only require updating the appropriate references.
   - Insertion and deletion operations in the middle of the list still have linear-time complexity (O(n)), as they require traversal to find the insertion/deletion point.

### Usage:

- Single linked lists are suitable when forward traversal is primarily required, and memory efficiency is a concern.
- Doubly linked lists are suitable when bidirectional traversal is required or when efficient insertion and deletion operations are needed at both ends of the list.

### Summary:

- Single linked lists and doubly linked lists are both linear data structures used for storing collections of elements.
- Single linked lists have unidirectional links and are memory-efficient but support only forward traversal.
- Doubly linked lists have bidirectional links and support both forward and backward traversal but require more memory per node. They also provide efficient insertion and deletion operations at both ends of the list.

## Hashing/Re-hashing
Hashing and rehashing are fundamental concepts used in hash table-based data structures like HashMaps to efficiently store and retrieve key-value pairs. Here's how they typically work:

### Hashing:

1. **Hash Function:**
   - Hashing begins with a hash function, which takes a key as input and produces a hash code, a numerical value typically used to index into an array.
   - The hash function should ideally distribute keys uniformly across the range of hash codes to minimize collisions.

2. **Index Calculation:**
   - The hash code is then mapped to an index within the array, typically by applying a modulo operation with the array size.
   - This determines where the corresponding key-value pair will be stored within the array.

3. **Storage:**
   - The key-value pair is stored at the computed index in the array, forming the basis of the hash table's storage mechanism.

### Rehashing:

4. **Load Factor:**
   - Rehashing is triggered when the number of key-value pairs stored in the hash table exceeds a certain threshold, known as the load factor.
   - The load factor is a measure of how full the hash table is, typically represented as the ratio of the number of elements to the size of the array.

5. **Resize:**
   - When the load factor exceeds a predefined threshold (e.g., 0.75), the hash table is resized to accommodate more elements.
   - The size of the underlying array is increased, usually doubling its capacity to maintain efficiency.

6. **Rehashing Process:**
   - During rehashing, all existing key-value pairs are rehashed and redistributed into the new, larger array.
   - This involves recalculating the hash codes of each key and mapping them to new indices in the resized array.

7. **Array Reallocation:**
   - Once all key-value pairs have been rehashed and moved to the new array, the old array is discarded, and the hash table begins using the resized array for storage.

### Benefits:

- **Efficiency:** Rehashing ensures that the hash table remains efficient even as the number of elements grows.
- **Balanced Load:** By resizing the array dynamically, rehashing helps maintain a balanced load factor, reducing the likelihood of collisions and improving performance.
- **Adaptability:** Hash tables with rehashing can adapt to changing workloads by dynamically adjusting their size as needed.

### Summary:

- **Hashing** is the process of mapping keys to indices in an array using a hash function.
- **Rehashing** is the process of resizing and reorganizing the hash table when the load factor exceeds a certain threshold.
- Rehashing ensures efficient storage and retrieval of key-value pairs in hash table-based data structures like HashMaps, helping to maintain a balanced load and adaptability to changing workloads.

## Initial Capacity vs Load Factor
In hash table-based data structures like HashMaps, the initial capacity and load factor are parameters that influence the performance and behavior of the data structure. Here's how they typically work:

### Initial Capacity:

1. **Definition:**
   - The initial capacity refers to the initial size of the underlying array used to store key-value pairs in the hash table.
   - When a hash table is created, an initial array of a certain size is allocated based on this parameter.

2. **Purpose:**
   - Setting an appropriate initial capacity is important for minimizing the need for resizing the hash table shortly after creation.
   - Choosing a larger initial capacity can reduce the frequency of resizing operations, improving performance and memory usage, especially if the number of elements to be stored is known in advance.

3. **Performance Impact:**
   - A larger initial capacity may consume more memory initially but can potentially reduce the number of resizing operations required as elements are added to the hash table.
   - Conversely, a smaller initial capacity may result in more frequent resizing operations, especially if the number of elements added exceeds the initial capacity.

### Load Factor:

4. **Definition:**
   - The load factor is a threshold value that determines when the hash table should be resized.
   - It represents the ratio of the number of elements stored in the hash table to the size of the underlying array.

5. **Purpose:**
   - The load factor is used to control the balance between memory usage and performance in the hash table.
   - When the number of elements exceeds a certain fraction of the total capacity (defined by the load factor), the hash table is resized to accommodate more elements.

6. **Performance Impact:**
   - A higher load factor means that the hash table can store more elements before needing to resize, potentially reducing the frequency of resizing operations.
   - However, a higher load factor also increases the likelihood of collisions, which can degrade performance.
   - Conversely, a lower load factor reduces the likelihood of collisions but may result in more frequent resizing operations, especially if the hash table contains many elements.

### Choosing Values:

7. **Optimization:**
   - Choosing appropriate values for the initial capacity and load factor depends on factors such as expected usage patterns, memory constraints, and performance requirements.
   - Generally, a balance should be struck between minimizing memory usage and reducing the frequency of resizing operations.

8. **Guidelines:**
   - A common guideline is to choose an initial capacity that is a power of two to take advantage of bitwise operations for better performance.
   - A load factor of around 0.75 is often recommended as a good balance between memory usage and performance in many scenarios.

### Summary:

- **Initial Capacity:** Determines the initial size of the underlying array when the hash table is created, influencing memory usage and resizing frequency.
- **Load Factor:** Controls the balance between memory usage and performance by specifying when the hash table should be resized based on the number of elements stored.
- Choosing appropriate values for these parameters depends on factors such as expected usage patterns, memory constraints, and performance requirements.

## Hash collision - Chaining vs Open addressing
Hash collision resolution is a crucial aspect of hash table-based data structures like HashMaps, where multiple keys may hash to the same index in the underlying array. Two common techniques for resolving hash collisions are chaining and open addressing. Here's how they typically work:

### Chaining:

1. **Linked Lists:**
   - Chaining involves storing multiple key-value pairs that hash to the same index in a linked list (or other data structure) at that index in the array.
   - Each element in the array contains a reference to the head of the linked list.

2. **Insertion:**
   - When a new key-value pair is added to the hash table and hashes to a location that is already occupied, it is simply appended to the linked list at that location.
   - This allows multiple elements with different keys but the same hash code to coexist at the same index.

3. **Lookup and Removal:**
   - To find or remove an element with a specific key, the hash table traverses the linked list at the corresponding index until it finds the desired element or reaches the end of the list.

### Open Addressing:

4. **Probing Sequence:**
   - Open addressing involves searching for an alternate location within the hash table when a collision occurs, rather than storing the colliding element in a separate data structure.
   - It uses a probing sequence to determine the next index to try when a collision occurs.

5. **Insertion:**
   - When a new key-value pair is added and hashes to an occupied location, the hash table probes for the next available (unoccupied) location according to a predetermined sequence.
   - Common probing techniques include linear probing (trying consecutive indices), quadratic probing (using a quadratic function to determine the next index), and double hashing (using a secondary hash function to compute the step size).

6. **Lookup and Removal:**
   - To find or remove an element with a specific key, the hash table uses the same probing sequence to locate the element in the table.
   - During lookup, the probing sequence is followed until the desired key is found or an empty slot is encountered.
   - During removal, the slot containing the element to be removed is marked as "deleted," and subsequent lookups treat it as available.

### Performance and Considerations:

7. **Performance:**
   - Chaining tends to be simpler to implement and may be more efficient when the number of collisions is relatively low, as it does not require additional probing.
   - Open addressing can be more memory-efficient and cache-friendly, as it avoids storing pointers to additional data structures.
   - However, the choice between chaining and open addressing often depends on factors such as the expected number of collisions, the desired performance characteristics, and the specific requirements of the application.

8. **Load Factor and Resizing:**
   - Both chaining and open addressing may require resizing the hash table when the load factor exceeds a certain threshold to maintain efficiency, though the specifics of resizing may differ between the two techniques.

### Summary:

- **Chaining:** Stores colliding elements in separate data structures (e.g., linked lists) at the same index.
- **Open Addressing:** Searches for alternate locations within the hash table when collisions occur, using a probing sequence.
- The choice between chaining and open addressing depends on factors such as collision frequency, memory usage, and performance requirements. Each technique has its own advantages and trade-offs in terms of simplicity, memory efficiency, and cache performance.

## Treefication / Treefy Threshold
The "treefication" or "treefy" threshold, often referred to as the "tree threshold," is a parameter used in hash table implementations, such as Java's `HashMap`, to determine when to switch from using a linked list to a balanced tree structure to store hash collisions. Here's how it typically works:

### Treefication Threshold:

1. **Initial Data Structure:**
   - In a hash table, collisions may occur when multiple keys hash to the same index in the underlying array.
   - Traditionally, hash tables resolve collisions by chaining, where each slot in the array contains a linked list of key-value pairs.
   - As the number of elements in the hash table increases, so does the length of these linked lists.

2. **Threshold Value:**
   - The treefication threshold is a specific size or length of a linked list at which point the hash table switches from using a linked list to a balanced tree structure to store elements at that index.
   - Once the number of elements in a linked list exceeds this threshold, the list is converted into a tree to maintain performance.

### Tree Structure:

3. **Balanced Tree:**
   - The tree structure used is typically a self-balancing binary search tree, such as a Red-Black tree or an AVL tree.
   - These trees ensure efficient searching, insertion, and deletion operations, maintaining performance even with a large number of elements.

4. **Conversion Process:**
   - When the treefication threshold is reached, the linked list is converted into a balanced tree.
   - The elements in the linked list are redistributed into the tree structure, which may involve sorting them based on their hash codes or keys.

### Benefits:

5. **Performance Improvement:**
   - Switching from a linked list to a balanced tree for large linked lists can improve performance, especially for operations like searching, insertion, and deletion.
   - Balanced trees have better worst-case time complexity for these operations compared to linked lists, which have linear-time complexity.

6. **Space Efficiency:**
   - Balanced trees can also be more space-efficient than linked lists for large collections of elements, as they have lower memory overhead per element.

### Implementation Considerations:

7. **Threshold Value Selection:**
   - The treefication threshold value is typically chosen based on empirical data and performance testing to balance the overhead of maintaining trees with the performance benefits they provide.
   - It is often set to a value that strikes a balance between the frequency of conversions and the improvement in performance.

8. **Dynamic Adjustment:**
   - Some hash table implementations dynamically adjust the treefication threshold based on factors such as the number of elements in the hash table or the size of individual linked lists.
   - This allows the threshold to adapt to changes in the workload and distribution of keys, optimizing performance under varying conditions.

### Summary:

- The treefication threshold is a parameter used in hash table implementations to determine when to switch from using linked lists to balanced trees to store hash collisions.
- It helps balance the trade-off between memory overhead and performance by converting large linked lists into more efficient tree structures.
- The threshold value is chosen based on performance considerations and may be dynamically adjusted to optimize performance under varying workloads.

## Red-black tree / Self-balanced binary search tree
A Red-Black Tree is a type of self-balancing binary search tree (BST) that maintains balance during insertion and deletion operations, ensuring efficient performance for search, insertion, and deletion operations. Here's how it typically works:

### Binary Search Tree (BST) Overview:

1. **Binary Search Property:**
   - In a binary search tree, each node has at most two children: a left child and a right child.
   - The binary search property ensures that for every node:
      - All nodes in the left subtree have values less than the node's value.
      - All nodes in the right subtree have values greater than the node's value.

2. **Searching:**
   - The binary search tree provides efficient search operations by leveraging its sorted structure, allowing for logarithmic-time complexity (O(log n)) search operations.

### Red-Black Tree Properties:

3. **Coloring:**
   - In addition to the properties of a binary search tree, a Red-Black Tree assigns colors to each node: red or black.
   - Each node in the tree is either red or black.

4. **Balancing Rules:**
   - Red-Black Trees adhere to a set of balancing rules that ensure the tree remains balanced after insertion and deletion operations.
   - These balancing rules help maintain a balanced tree height, ensuring efficient search, insertion, and deletion operations.

### Balancing Rules:

5. **Rule 1 - Root Property:**
   - The root node is always black.

6. **Rule 2 - Red Property:**
   - Red nodes cannot have red children. In other words, no two adjacent nodes in the tree can both be red.

7. **Rule 3 - Black Height Property:**
   - Every path from a node to its descendant leaves must have the same number of black nodes. This property ensures that the tree remains balanced.

### Insertion and Deletion:

8. **Insertion:**
   - When a new node is inserted into the tree, it is initially colored red.
   - The tree is then adjusted as necessary to maintain the Red-Black Tree properties while ensuring balance.

9. **Deletion:**
   - When a node is deleted from the tree, the tree is adjusted to maintain the Red-Black Tree properties and balance.
   - This typically involves restructuring the tree and recoloring nodes as necessary.

### Performance Characteristics:

10. **Balanced Height:**
   - Red-Black Trees guarantee that the height of the tree is logarithmic relative to the number of nodes, ensuring efficient search, insertion, and deletion operations with worst-case time complexity of O(log n).

### Usage:

11. **Common Applications:**
   - Red-Black Trees are widely used in various applications, including as the underlying data structure for implementing associative arrays (e.g., Java's TreeMap) and in many other situations where efficient search, insertion, and deletion operations are required.

### Summary:

- Red-Black Trees are self-balancing binary search trees that maintain balance through the use of color and a set of balancing rules.
- They ensure a balanced tree height, providing efficient search, insertion, and deletion operations with worst-case time complexity of O(log n).
- Red-Black Trees are commonly used in various applications due to their efficient performance characteristics and balanced structure.

## Skip List Data Structure
Skip List is a probabilistic data structure that provides logarithmic-time average-case performance for search, insertion, and deletion operations, similar to balanced trees like Red-Black Trees, but with simpler implementation and potentially better average-case performance characteristics. Here's how it typically works:

### Basic Structure:

1. **Levels:**
   - A Skip List consists of multiple levels, with the bottom level containing all the elements sorted in ascending order.
   - Each level is a singly linked list containing a subset of elements from the lower level.

2. **Nodes:**
   - Each node contains a key-value pair and references to the next node in the same level and the node below it (if it exists).

### Express Lanes:

3. **Express Lanes:**
   - Skip Lists use "express lanes" to accelerate the search process.
   - Higher levels of the Skip List contain fewer elements but have larger gaps between nodes.
   - By using express lanes, the Skip List can skip over large sections of the list during searches, reducing the number of comparisons needed.

4. **Tower Nodes:**
   - Nodes with multiple levels (tower nodes) act as access points to higher levels, allowing for efficient traversal of the list.

### Search Operation:

5. **Search Algorithm:**
   - To search for a key in a Skip List, the algorithm starts from the top-left corner (the highest level, leftmost node).
   - It compares the key with the next node in the current level. If the key is greater, it moves to the next node; otherwise, it moves down to the next level and repeats the process.

6. **Efficiency:**
   - The use of express lanes allows the search algorithm to skip over large portions of the list, resulting in average-case time complexity of O(log n), similar to balanced trees.

### Insertion and Deletion:

7. **Insertion:**
   - Inserting an element into a Skip List involves determining the levels at which the new element should appear (using randomization to determine the levels).
   - The new element is inserted into each level's linked list at the appropriate position, with references adjusted accordingly.

8. **Deletion:**
   - Deleting an element from a Skip List involves removing the element from each level's linked list, with references updated accordingly.

### Performance Characteristics:

9. **Average-Case Performance:**
   - Skip Lists provide logarithmic-time average-case performance for search, insertion, and deletion operations, making them suitable for applications where efficient average-case performance is essential.

### Usage:

10. **Applications:**
   - Skip Lists are used in various applications, including database systems, network routers, and data structures libraries, where efficient search, insertion, and deletion operations are required, and simplicity of implementation is preferred over more complex balanced trees.

### Summary:

- Skip List is a probabilistic data structure that provides logarithmic-time average-case performance for search, insertion, and deletion operations.
- It consists of multiple levels of linked lists, with express lanes allowing for efficient traversal.
- Skip Lists offer a simpler implementation compared to balanced trees while still providing efficient average-case performance characteristics, making them suitable for various applications.

## Compare and Swap (CAS) algorithm

The Compare and Swap (CAS) algorithm is a low-level atomic operation used in concurrent programming to implement synchronization without locks. It is a fundamental building block for lock-free and wait-free algorithms and data structures. Here's a detailed explanation of how CAS works:

### Basic Operation:

1. **Atomicity:**
   - CAS is an atomic operation provided by modern CPUs and supported by many programming languages and libraries.
   - It performs a conditional update to a memory location in an atomic manner, ensuring that the operation appears to occur instantaneously without interference from other threads.

2. **Operands:**
   - CAS takes three operands: a memory location (usually a memory address or a reference), an expected old value, and a new value.
   - It compares the value in the memory location with the expected old value.
   - If the two values match, CAS atomically updates the memory location with the new value.

3. **Conditional Update:**
   - CAS performs a conditional update: if the value in the memory location matches the expected old value, it updates the memory location with the new value.
   - If the value does not match (indicating that the memory location has been modified by another thread), the operation fails, and the memory location remains unchanged.

### Algorithm:

4. **Pseudocode:**
   ```
   function CAS(location, expectedValue, newValue):
       currentValue = value at location
       if currentValue == expectedValue:
           value at location = newValue
           return true  // Successful update
       else:
           return false // Update failed
   ```

### Significance:

5. **Lock-Free Synchronization:**
   - CAS is the foundation of lock-free synchronization techniques, where multiple threads can progress concurrently without waiting for locks.
   - Lock-free algorithms avoid the overhead and potential issues associated with traditional locks or mutexes, such as contention and deadlock.

6. **Non-Blocking Algorithms:**
   - CAS enables the design of non-blocking algorithms, where threads can make progress even if some threads are delayed or blocked.
   - Non-blocking algorithms are valuable for systems requiring high throughput, low latency, and scalability.

7. **Concurrency Control:**
   - CAS is used to implement atomic operations and data structures in concurrent programming, such as atomic variables, atomic counters, queues, stacks, and hash tables.
   - It ensures that operations on shared data structures are performed atomically, preventing race conditions and data corruption.

### Challenges:

8. **ABA Problem:**
   - CAS may be susceptible to the ABA problem, where a value changes from A to B and then back to A, leading to unintended behavior.
   - Strategies like using version numbers, timestamps, or double-word CAS are employed to address the ABA problem.

### Usage:

9. **Concurrency Libraries:**
   - CAS is provided by many programming languages and concurrency libraries, such as Java's `java.util.concurrent.atomic` package, C++'s `std::atomic` operations, and Go's atomic package.
   - It is used extensively in multi-threaded applications to implement efficient and scalable synchronization mechanisms.

### Summary:

- Compare and Swap (CAS) is an atomic operation used in concurrent programming to perform conditional updates to memory locations.
- It enables lock-free and wait-free synchronization techniques, allowing multiple threads to progress concurrently without traditional locks.
- CAS is a fundamental building block for implementing non-blocking algorithms and data structures, ensuring atomicity and thread safety in concurrent environments.
- Despite its power, CAS may require careful handling to address challenges like the ABA problem, especially in complex concurrent algorithms and data structures.

## Segment locking in ConcurrentHashMap
Segment locking in `ConcurrentHashMap` is a concurrency control mechanism used to achieve thread-safety and efficient parallelism in Java's `ConcurrentHashMap` implementation. It's a form of partitioning where the map is divided into multiple segments, each with its own separate lock. Here's how segment locking works in `ConcurrentHashMap`:

### Basic Concept:

1. **Segmented Design:**
   - `ConcurrentHashMap` divides its internal data structure into multiple segments (or partitions), typically using an array-like structure.

2. **Independent Locking:**
   - Each segment has its own lock, allowing multiple threads to concurrently access different segments without contention.

### Operations:

3. **Lock Granularity:**
   - When performing operations such as `put`, `get`, `remove`, etc., the map determines the segment corresponding to the key and acquires the lock associated with that segment.
   - This allows concurrent access to different segments while ensuring exclusive access within each segment.

4. **Concurrent Access:**
   - Threads accessing different segments can proceed concurrently without contention, improving parallelism and throughput.
   - However, threads accessing the same segment may contend for the segment's lock, but this contention is limited to a smaller subset of the map, reducing contention compared to a single global lock.

### Benefits:

5. **Reduced Contention:**
   - Segment locking reduces contention compared to using a single global lock for the entire map.
   - By partitioning the map into segments, concurrent threads accessing different segments can operate independently without blocking each other.

6. **Scalability:**
   - Segment locking improves scalability by allowing multiple threads to work concurrently on different segments of the map.
   - This approach can effectively utilize multiple CPU cores and increase throughput in multi-threaded environments.

### Implementation Details:

7. **Segment Class:**
   - Internally, `ConcurrentHashMap` uses a nested static class called `Segment` to represent each segment.
   - Each `Segment` maintains its own array of hash buckets and a corresponding lock for synchronization.

8. **Locking Strategy:**
   - The locking strategy in `ConcurrentHashMap` follows a coarse-grained approach, where each segment is protected by a single lock.
   - This approach strikes a balance between concurrency and overhead, providing thread-safety while minimizing lock contention.

### Performance:

9. **Trade-offs:**
   - While segment locking improves concurrency and scalability, it introduces some overhead due to maintaining multiple locks and potential contention for segment locks.
   - However, the benefits of reduced contention and improved parallelism often outweigh the overhead, especially in highly concurrent scenarios.

### Summary:

- Segment locking in `ConcurrentHashMap` is a concurrency control mechanism that divides the map into multiple segments, each with its own lock.
- It reduces contention by allowing concurrent access to different segments while ensuring exclusive access within each segment.
- Segment locking improves scalability and parallelism in multi-threaded environments, balancing concurrency and overhead to achieve thread-safety and efficient parallelism.

## FIFO vs LIFO principles
FIFO (First-In-First-Out) and LIFO (Last-In-First-Out) are two fundamental principles used to manage the order in which items are processed or accessed in various data structures and algorithms. Here's how they differ:

### FIFO (First-In-First-Out):

1. **Definition:**
   - FIFO, also known as "queueing" or "First-Come-First-Served," follows the principle that the first item to be enqueued (added) into a data structure is the first one to be dequeued (removed).
   - It operates like a line or queue in real life, where the first person to join the line is the first one to be served.

2. **Operations:**
   - Enqueue: Adds an item to the end of the queue.
   - Dequeue: Removes and returns the item from the front of the queue.
   - Peek: Retrieves the item from the front of the queue without removing it.

3. **Usage:**
   - FIFO is commonly used in scenarios where order preservation is important, such as task scheduling, message queues, and breadth-first traversal algorithms.

4. **Data Structures:**
   - FIFO can be implemented using various data structures, including arrays, linked lists, and queues.

### LIFO (Last-In-First-Out):

5. **Definition:**
   - LIFO, also known as "stacking" or "Last-Come-First-Served," follows the principle that the last item to be pushed (added) into a data structure is the first one to be popped (removed).
   - It operates like a stack of plates, where the last plate placed on top of the stack is the first one to be removed.

6. **Operations:**
   - Push: Adds an item to the top of the stack.
   - Pop: Removes and returns the item from the top of the stack.
   - Peek: Retrieves the item from the top of the stack without removing it.

7. **Usage:**
   - LIFO is commonly used in scenarios where the most recently added items are the most relevant or need to be processed first, such as function call stacks, undo mechanisms, and depth-first traversal algorithms.

8. **Data Structures:**
   - LIFO can be implemented using various data structures, including arrays, linked lists, and stacks.

### Comparison:

9. **Ordering:**
   - FIFO preserves the order in which items are added, while LIFO reverses the order.
   - In FIFO, older items are dequeued before newer ones, whereas in LIFO, newer items are popped before older ones.

10. **Behavior:**
   - FIFO is like waiting in a line, where the first person in line is served first.
   - LIFO is like stacking objects, where the last object placed on top is the first to be removed.

### Summary:

- **FIFO (First-In-First-Out):** Preserves the order of items, with the first item added being the first to be removed.
- **LIFO (Last-In-First-Out):** Reverses the order of items, with the last item added being the first to be removed.
- Both principles have distinct use cases and are implemented using various data structures, such as queues for FIFO and stacks for LIFO, each catering to different requirements and scenarios.

## Access Order of LinkedHashMap
In Java, `LinkedHashMap` is a class that extends `HashMap` to provide a predictable iteration order. It maintains a doubly-linked list running through all of its entries, which defines the iteration ordering.

The `LinkedHashMap` provides two modes for ordering: insertion order and access order. By default, it maintains entries according to their insertion order, meaning the order in which entries were added to the map. However, it also offers an option to maintain entries according to their access order, where the most recently accessed entry is moved to the end of the iteration order.

### Access Order Mode:

In access order mode, whenever an entry is accessed (via `get`, `put`, or `putIfAbsent` operations), it is moved to the end of the iteration order, signifying that it was the most recently accessed entry.

### Enabling Access Order:

To enable access order mode, you can pass `true` as the second argument to the `LinkedHashMap` constructor or use the `accessOrder` parameter when calling the `Collections.newMap` factory method.

```java
// Create a LinkedHashMap with access order enabled
Map<K, V> linkedHashMap = new LinkedHashMap<>(capacity, loadFactor, true);
```

### Usage:

Access order mode is useful in scenarios where you want to implement least recently used (LRU) caching or maintain a cache of recently accessed items. By evicting the least recently accessed items, you can ensure that the cache remains bounded and contains the most relevant data.

### Summary:

- `LinkedHashMap` maintains a predictable iteration order using a doubly-linked list.
- By default, it maintains entries according to their insertion order.
- Access order mode can be enabled to maintain entries according to their access order, where the most recently accessed entry is moved to the end of the iteration order.
- Access order mode is useful for implementing LRU caching or maintaining a cache of recently accessed items.

# Iterator vs ListIterator vs Enumeration
Let's delve into the detailed operations provided by `Iterator`, `ListIterator`, and `Enumeration` interfaces in Java, including their capabilities and limitations:

### Enumeration:

1. **Forward Traversal:**
   - `hasMoreElements()`: Checks if there are more elements in the enumeration.
   - `nextElement()`: Retrieves the next element in the enumeration and advances the cursor.

2. **Capabilities:**
   - Forward-only traversal: Supports only forward iteration over the elements.
   - No modification: Doesn't provide methods for adding, removing, or modifying elements during traversal.

3. **Usage:**
   - Typically used with legacy collections like `Vector` and `Hashtable`.
   - Suitable for simple forward iteration without the need for modification operations.

### Iterator:

4. **Forward Traversal:**
   - `hasNext()`: Checks if there are more elements in the collection.
   - `next()`: Retrieves the next element in the collection and advances the cursor.

5. **Removal Operations:**
   - `remove()`: Removes the last element returned by the iterator from the underlying collection.

6. **Capabilities:**
   - Forward traversal with removal: Supports forward iteration and allows removal of elements during traversal.
   - No bidirectional traversal: Doesn't support backward traversal like `ListIterator`.

7. **Usage:**
   - Widely used with most collections in Java, including lists, sets, and maps.
   - Suitable for iterating over collections and performing removal operations.

### ListIterator:

8. **Bidirectional Traversal:**
   - `hasNext()`: Checks if there are more elements in the list.
   - `next()`: Retrieves the next element in the list and advances the cursor.
   - `hasPrevious()`: Checks if there are previous elements in the list.
   - `previous()`: Retrieves the previous element in the list and moves the cursor backward.

9. **Modification Operations:**
   - `add()`: Inserts a new element into the list at the current cursor position.
   - `remove()`: Removes the last element returned by the iterator from the underlying list.
   - `set()`: Replaces the last element returned by the iterator with a new element.

10. **Capabilities:**
   - Bidirectional traversal with modification: Supports traversal in both forward and backward directions and allows modification of list elements.
   - Suitable for scenarios where bidirectional traversal or modification of list elements is required.

11. **Usage:**
   - Primarily used with list implementations like `ArrayList`, `LinkedList`, and `Vector`.
   - Suitable for scenarios where both forward and backward traversal or modification of list elements is required.

### Summary:

- **Enumeration:** Basic interface for forward-only traversal, suitable for simple iteration without modification.
- **Iterator:** Standard interface for forward traversal with removal operations, widely used with most collections in Java.
- **ListIterator:** Specialized interface for bidirectional traversal and modification of lists, primarily used with list implementations.

Certainly! Here's a tabular representation comparing `Enumeration`, `Iterator`, and `ListIterator` in Java:

| Feature              | Enumeration                                      | Iterator                                  | ListIterator                             |
|----------------------|--------------------------------------------------|-------------------------------------------|------------------------------------------|
| **Traversal**        | Forward only                                     | Forward only                              | Bidirectional (Forward & Backward)      |
| **Collections**      | Legacy collections (e.g., `Vector`, `Hashtable`) | Most collections (e.g., `ArrayList`, `HashSet`, `HashMap`) | Lists (e.g., `ArrayList`, `LinkedList`) |
| **Forward Methods**  | `hasMoreElements()`, `nextElement()`            | `hasNext()`, `next()`                     | `hasNext()`, `next()`                    |
| **Backward Methods** | N/A                                              | N/A                                       | `hasPrevious()`, `previous()`            |
| **Modification**     | No                                               | `remove()`                                | `add()`, `remove()`, `set()`            |
| **Use Case**         | Simple forward iteration                         | Forward iteration with removal operations | Bidirectional traversal and modification |

This table provides a quick overview of the features and capabilities of `Enumeration`, `Iterator`, and `ListIterator`, making it easier to understand their differences and use cases in Java.

# Comparator vs Comparable
Here's a comparison between `Comparator` and `Comparable` in Java:

| Feature            | Comparable                                         | Comparator                                    |
|--------------------|----------------------------------------------------|-----------------------------------------------|
| **Interface**      | `Comparable<T>`                                   | `Comparator<T>`                              |
| **Defined in**     | Class of the element being compared                | Separate class or lambda expression           |
| **Implemented by** | Classes whose instances need to be compared        | Separate classes or lambda expressions       |
| **Method**         | `compareTo(T o)`                                  | `compare(T o1, T o2)`                         |
| **Usage**          | Natural ordering of elements                       | Custom ordering of elements                   |
| **Single Type**    | Generic for one type of element                    | Generic for one type of element               |
| **Comparable**     | Objects are compared to themselves                 | Objects are compared by an external authority |
| **Mutability**     | Determines natural order of objects                | Allows multiple orderings of objects         |
| **Natural Sorting**| Automatically sorted in collections like `TreeSet` | Requires explicit specification for sorting  |

### Summary:

- **Comparable:**
   - Used for defining natural ordering of objects within the class itself.
   - Implemented by the class of the objects being compared.
   - Requires overriding the `compareTo()` method to specify the natural ordering.
   - Automatically applied for sorting in collections like `TreeSet`.

- **Comparator:**
   - Used for defining custom ordering of objects by an external authority.
   - Implemented by a separate class or lambda expression.
   - Requires implementing the `compare()` method to specify the custom ordering.
   - Can define multiple orderings for the same class and allows sorting based on different criteria.

In essence, `Comparable` is more intrinsic to the object being compared and provides a natural ordering, while `Comparator` allows for more flexibility and external control over sorting criteria.

# Time-complexity (BigO-Notation) of all collection methods
The time complexity (Big O notation) of collection methods can vary depending on the specific implementation of the collection and the operation being performed. Below is a general overview of the time complexity for common operations on various collections in Java:

### Lists:

1. **ArrayList:**
   - Access (get): O(1)
   - Search (contains): O(n)
   - Insertion (add at end): O(1) amortized, O(n) worst-case
   - Insertion (add at arbitrary index): O(n)
   - Deletion (remove at end): O(1) amortized, O(n) worst-case
   - Deletion (remove at arbitrary index): O(n)

2. **LinkedList:**
   - Access (get): O(n)
   - Search (contains): O(n)
   - Insertion (add at end): O(1)
   - Insertion (add at arbitrary index): O(n)
   - Deletion (remove at end): O(1)
   - Deletion (remove at arbitrary index): O(n)

### Sets:

3. **HashSet:**
   - Insertion (add): O(1) average-case, O(n) worst-case
   - Search (contains): O(1) average-case, O(n) worst-case
   - Deletion (remove): O(1) average-case, O(n) worst-case

4. **LinkedHashSet:**
   - Insertion (add): O(1)
   - Search (contains): O(1)
   - Deletion (remove): O(1)

5. **TreeSet:**
   - Insertion (add): O(log n)
   - Search (contains): O(log n)
   - Deletion (remove): O(log n)

### Maps:

6. **HashMap:**
   - Insertion (put): O(1) average-case, O(n) worst-case
   - Search (get): O(1) average-case, O(n) worst-case
   - Deletion (remove): O(1) average-case, O(n) worst-case

7. **LinkedHashMap:**
   - Insertion (put): O(1)
   - Search (get): O(1)
   - Deletion (remove): O(1)

8. **TreeMap:**
   - Insertion (put): O(log n)
   - Search (get): O(log n)
   - Deletion (remove): O(log n)

### Queues:

9. **ArrayDeque:**
   - Insertion (add): O(1)
   - Removal (remove): O(1)
   - Retrieval (peek): O(1)

10. **LinkedList (as Queue):**
- Insertion (add): O(1)
- Removal (remove): O(1)
- Retrieval (peek): O(1)

11. **PriorityQueue:**
- Insertion (add): O(log n)
- Removal (remove): O(log n)
- Retrieval (peek): O(1)

### Summary:

- The time complexity of collection methods varies depending on the specific data structure used for implementation.
- In general, collections that offer constant-time access (e.g., ArrayList, HashSet) have better performance for operations like access and insertion/removal at the end.
- Collections that require traversal or maintaining order (e.g., LinkedList, TreeSet) may have higher time complexity for certain operations like searching and insertion/removal at arbitrary positions.
- It's important to consider the time complexity of collection methods when selecting the appropriate data structure for your application's requirements.

Here's a tabular representation of the time complexity (Big O notation) for common methods in various Java collections:

| Collection    | Method              | Average Case Complexity | Worst Case Complexity |
|---------------|---------------------|-------------------------|-----------------------|
| **ArrayList** | Access (get)        | O(1)                    | O(1)                  |
|               | Search (contains)   | O(n)                    | O(n)                  |
|               | Insertion (add)     | O(1) amortized          | O(n)                  |
|               | Deletion (remove)   | O(1) amortized          | O(n)                  |
| **LinkedList**| Access (get)        | O(n)                    | O(n)                  |
|               | Search (contains)   | O(n)                    | O(n)                  |
|               | Insertion (add)     | O(1)                    | O(n)                  |
|               | Deletion (remove)   | O(1)                    | O(n)                  |
| **HashSet**   | Insertion (add)     | O(1) average-case       | O(n) worst-case       |
|               | Search (contains)   | O(1) average-case       | O(n) worst-case       |
|               | Deletion (remove)   | O(1) average-case       | O(n) worst-case       |
| **LinkedHashSet** | Insertion (add)  | O(1)                    | O(1)                  |
|               | Search (contains)   | O(1)                    | O(1)                  |
|               | Deletion (remove)   | O(1)                    | O(1)                  |
| **TreeSet**   | Insertion (add)     | O(log n)                | O(log n)              |
|               | Search (contains)   | O(log n)                | O(log n)              |
|               | Deletion (remove)   | O(log n)                | O(log n)              |
| **HashMap**   | Insertion (put)     | O(1) average-case       | O(n) worst-case       |
|               | Search (get)        | O(1) average-case       | O(n) worst-case       |
|               | Deletion (remove)   | O(1) average-case       | O(n) worst-case       |
| **LinkedHashMap** | Insertion (put)  | O(1)                    | O(1)                  |
|               | Search (get)        | O(1)                    | O(1)                  |
|               | Deletion (remove)   | O(1)                    | O(1)                  |
| **TreeMap**   | Insertion (put)     | O(log n)                | O(log n)              |
|               | Search (get)        | O(log n)                | O(log n)              |
|               | Deletion (remove)   | O(log n)                | O(log n)              |
| **ArrayDeque**| Insertion (add)     | O(1)                    | O(1)                  |
|               | Removal (remove)    | O(1)                    | O(1)                  |
|               | Retrieval (peek)    | O(1)                    | O(1)                  |
| **LinkedList**| Insertion (add)     | O(1)                    | O(1)                  |
| (as Queue)    | Removal (remove)    | O(1)                    | O(1)                  |
|               | Retrieval (peek)    | O(1)                    | O(1)                  |
| **PriorityQueue** | Insertion (add)  | O(log n)                | O(log n)              |
|               | Removal (remove)    | O(log n)                | O(log n)              |
|               | Retrieval (peek)    | O(1)                    | O(1)                  |

Please note that these complexities are based on general considerations and may vary depending on the specific implementation and usage patterns.

# Null and Duplicate Behaviour of all Collection classes
Let's discuss the behavior of Java collection classes regarding null values and duplicates:

### Lists:

1. **ArrayList**:
   - Allows null values and duplicates.
   - Supports random access and maintains insertion order.

2. **LinkedList**:
   - Allows null values and duplicates.
   - Supports sequential access and maintains insertion order.

### Sets:

3. **HashSet**:
   - Allows one null value and avoids duplicates.
   - Does not guarantee insertion order.

4. **LinkedHashSet**:
   - Allows one null value and avoids duplicates.
   - Maintains insertion order.

5. **TreeSet**:
   - Does not allow null values.
   - Avoids duplicates.
   - Elements are sorted in natural order or according to a specified comparator.

### Queues:

6. **ArrayDeque**:
   - Allows null values and duplicates.
   - Supports FIFO (First-In-First-Out) order.

7. **LinkedList (as Queue)**:
   - Allows null values and duplicates.
   - Supports FIFO order.

### Maps:

8. **HashMap**:
   - Allows one null key and multiple null values.
   - Does not allow duplicate keys.
   - Does not guarantee key-value pair order.

9. **LinkedHashMap**:
   - Allows one null key and multiple null values.
   - Does not allow duplicate keys.
   - Maintains insertion order or access order, depending on the constructor used.

10. **TreeMap**:
   - Does not allow null keys.
   - Does not allow duplicate keys.
   - Elements are sorted in natural order or according to a specified comparator.

### Summary:

- Most collection classes in Java allow null values, but the behavior regarding duplicates varies.
- Sets and maps typically do not allow duplicates, ensuring that each element/key is unique.
- The behavior regarding null values and duplicates should be considered when selecting the appropriate collection class for a specific use case.

Here's a comprehensive tabular overview of the behavior of Java collection classes regarding null values and duplicates:
Certainly, let's include `Deque` in the table:

| Collection       | Null Values Allowed | Duplicates Allowed | Ordering        |
|------------------|---------------------|---------------------|-----------------|
| **Lists**        |                     |                     |                 |
| ArrayList        | ✓                   | ✓                   | Insertion Order |
| LinkedList       | ✓                   | ✓                   | Insertion Order |
| **Sets**         |                     |                     |                 |
| HashSet          | ✓                   | ✘                   | Unordered       |
| LinkedHashSet    | ✓                   | ✘                   | Insertion Order |
| TreeSet          | ✘                   | ✘                   | Sorted          |
| **Queues**       |                     |                     |                 |
| ArrayDeque       | ✓                   | ✓                   | FIFO            |
| LinkedList (as Queue) | ✓              | ✓                   | FIFO            |
| **Deque**        |                     |                     |                 |
| ArrayDeque       | ✓                   | ✓                   | FIFO/LIFO       |
| LinkedList       | ✓                   | ✓                   | FIFO/LIFO       |
| **Maps**         |                     |                     |                 |
| HashMap          | ✓ (Key and Values)  | ✘ (Keys)            | Unordered       |
| LinkedHashMap    | ✓ (Key and Values)  | ✘ (Keys)            | Insertion Order |
| TreeMap          | ✘ (Keys)            | ✘                   | Sorted          |

This table now includes `Deque` collection classes, specifying their behavior regarding null values, duplicates, and ordering.
This table provides a clear overview of the behavior of Java collection classes concerning null values, duplicates, and ordering, helping in selecting the appropriate collection for specific use cases.

# Fail-fast vs Fail-safe Iterator
Here's a comparison between fail-fast and fail-safe iterators in Java:

| Feature                | Fail-Fast Iterator                                          | Fail-Safe Iterator                                        |
|------------------------|-------------------------------------------------------------|-----------------------------------------------------------|
| **Behavior on Concurrent Modification** | Detects and throws `ConcurrentModificationException` immediately if the collection is modified during iteration. | Does not throw any exception if the collection is modified during iteration. |
| **Iterator Creation**  | Typically used by most Java collection classes, including ArrayList, HashSet, HashMap, etc. | Commonly used by concurrent collection classes like ConcurrentHashMap, CopyOnWriteArrayList, etc. |
| **Concurrency Control** | Offers better performance in single-threaded environments but may lead to inconsistencies in concurrent environments. | Designed for use in concurrent environments and ensures thread safety by creating a snapshot of the collection at the time of iteration. |
| **Usage**              | Suitable for situations where the collection is not expected to be modified during iteration, providing fast-fail behavior to detect unexpected modifications. | Ideal for concurrent scenarios where multiple threads may modify the collection simultaneously, ensuring that the iterator does not fail even if the collection is modified. |
| **Exception Thrown**   | Throws `ConcurrentModificationException` if the collection is structurally modified during iteration. | Does not throw any exception even if the collection is modified during iteration. |
| **Underlying Mechanism** | Uses an internal `modCount` or similar mechanism to track structural modifications in the collection. | Typically uses `CopyOnWriteArrayList` or similar techniques to maintain a separate copy of the collection during iteration to avoid concurrent modification issues. |

### Summary:

- **Fail-Fast Iterator:** Detects and throws `ConcurrentModificationException` immediately if the collection is modified during iteration, offering fast-fail behavior. Commonly used in non-concurrent environments.

- **Fail-Safe Iterator:** Does not throw any exception if the collection is modified during iteration. It ensures thread safety by creating a snapshot of the collection at the time of iteration, making it suitable for concurrent environments.

# How to avoid ConcurrentModificationException
To avoid `ConcurrentModificationException`, which occurs when a collection is modified while it is being iterated, you can employ various strategies depending on your specific use case and requirements:

1. **Use Iterator's remove() method:**
   - Instead of directly modifying the collection while iterating, use the iterator's `remove()` method to safely remove elements during iteration.
   - Example:
     ```java
     Iterator<String> iterator = list.iterator();
     while (iterator.hasNext()) {
         String element = iterator.next();
         if (condition) {
             iterator.remove(); // Safe removal
         }
     }
     ```

2. **Use synchronized blocks:**
   - If you are working with concurrent access to a collection, use synchronized blocks to ensure thread safety during iteration and modification.
   - Example:
     ```java
     synchronized (list) {
         for (String element : list) {
             // Perform operations
         }
     }
     ```

3. **Use concurrent collections:**
   - Consider using concurrent collections like `ConcurrentHashMap`, `CopyOnWriteArrayList`, etc., which are designed to handle concurrent modifications without throwing `ConcurrentModificationException`.
   - Example:
     ```java
     ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();
     map.put("key", 1);
     for (String key : map.keySet()) {
         // Safe to modify map here
     }
     ```

4. **Use explicit copying:**
   - Create a copy of the collection before iterating if modifications are expected during iteration. This avoids concurrent modification issues by working on a separate copy.
   - Example:
     ```java
     List<String> copy = new ArrayList<>(originalList);
     for (String element : copy) {
         // Perform operations
     }
     ```

5. **Avoid modifying collection during iteration:**
   - Restructure your code to avoid modifying the collection while it is being iterated. Instead, accumulate modifications and apply them after the iteration is complete.
   - Example:
     ```java
     List<String> toRemove = new ArrayList<>();
     for (String element : list) {
         if (condition) {
             toRemove.add(element);
         }
     }
     list.removeAll(toRemove);
     ```

Choose the approach that best fits your scenario, considering factors such as thread safety, performance, and the specific requirements of your application.

# How to avoid Thread race condition
To avoid thread race conditions, where the outcome of a program depends on the relative timing or interleaving of multiple threads, you can employ various synchronization techniques and best practices:

1. **Use Synchronization:**
   - Use synchronized blocks or methods to ensure that critical sections of code are accessed by only one thread at a time.
   - Example:
     ```java
     synchronized (lockObject) {
         // Critical section
     }
     ```

2. **Use Locks:**
   - Utilize explicit locks from the `java.util.concurrent.locks` package, such as `ReentrantLock`, to provide exclusive access to critical sections of code.
   - Example:
     ```java
     Lock lock = new ReentrantLock();
     lock.lock();
     try {
         // Critical section
     } finally {
         lock.unlock();
     }
     ```

3. **Use Atomic Variables:**
   - Use atomic variables from the `java.util.concurrent.atomic` package, such as `AtomicInteger`, to perform atomic operations without explicit synchronization.
   - Example:
     ```java
     AtomicInteger counter = new AtomicInteger();
     counter.incrementAndGet(); // Atomic increment operation
     ```

4. **Use Thread-Safe Data Structures:**
   - Utilize thread-safe data structures from the `java.util.concurrent` package, such as `ConcurrentHashMap`, `ConcurrentLinkedQueue`, etc., which are designed for concurrent access.
   - Example:
     ```java
     ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();
     map.put("key", 1); // Thread-safe put operation
     ```

5. **Avoid Sharing Mutable State:**
   - Minimize the sharing of mutable state between threads to reduce the likelihood of race conditions.
   - Immutable objects or thread-local variables can help avoid shared mutable state.

6. **Use Thread-Safe APIs:**
   - When using third-party libraries or APIs, ensure that they are thread-safe or use them in a thread-safe manner.

7. **Ensure Proper Memory Visibility:**
   - Use `volatile` keyword, `synchronized` blocks, or `java.util.concurrent` utilities like `Atomic*` classes to ensure proper visibility of shared variables across threads.

8. **Avoid Busy Waiting:**
   - Use high-level concurrency utilities such as `wait()`, `notify()`, `notifyAll()`, or blocking queues instead of busy waiting to coordinate threads.

9. **Test Concurrent Code:**
   - Write unit tests specifically designed to uncover race conditions and test your concurrent code under different scenarios.

By applying these techniques, you can effectively mitigate race conditions and ensure the correctness and reliability of your multi-threaded Java applications.

# Synchronized vs Concurrent Collections
Synchronized collections and concurrent collections both provide mechanisms for managing access to shared data structures in a multi-threaded environment, but they differ in their approach and usage:

### Synchronized Collections:

1. **Usage:**
   - Synchronized collections are regular collections (e.g., `ArrayList`, `HashMap`, etc.) wrapped with synchronization mechanisms, typically using `Collections.synchronizedXXX()` factory methods.
   - They provide thread-safety by synchronizing all methods that modify the collection on a common lock object.
   - Example:
     ```java
     List<String> synchronizedList = Collections.synchronizedList(new ArrayList<>());
     ```

2. **Lock Granularity:**
   - Synchronized collections lock the entire collection during modification operations, allowing only one thread to access it at a time.
   - This can lead to contention and reduced concurrency, especially in scenarios with high contention.

3. **Usage Considerations:**
   - Suitable for scenarios where the number of concurrent threads is low or moderate, and the collection is accessed mostly for reads.
   - They are less efficient than concurrent collections in highly concurrent environments due to their coarse-grained locking.

### Concurrent Collections:

1. **Usage:**
   - Concurrent collections are specifically designed for high-concurrency scenarios, providing better scalability and performance compared to synchronized collections.
   - They are part of the `java.util.concurrent` package and include classes like `ConcurrentHashMap`, `ConcurrentSkipListMap`, `ConcurrentLinkedQueue`, etc.
   - Example:
     ```java
     ConcurrentHashMap<String, Integer> concurrentMap = new ConcurrentHashMap<>();
     ```

2. **Lock Granularity:**
   - Concurrent collections use finer-grained locking strategies, often employing techniques like lock-striping or lock-free algorithms to allow multiple threads to access the collection concurrently.
   - They offer better scalability by reducing contention among threads.

3. **Usage Considerations:**
   - Ideal for highly concurrent scenarios where multiple threads may read, write, or modify the collection simultaneously.
   - Provide better performance and scalability compared to synchronized collections in high-concurrency environments.

### Summary:

- **Synchronized Collections:**
   - Provide thread-safety by synchronizing access to the entire collection.
   - Suitable for low to moderate concurrency scenarios but may suffer from contention issues in highly concurrent environments.

- **Concurrent Collections:**
   - Designed for high-concurrency scenarios, offering better scalability and performance.
   - Use finer-grained locking strategies to allow multiple threads to access the collection concurrently, reducing contention.

When selecting between synchronized and concurrent collections, consider the concurrency requirements of your application and choose the appropriate collection type to ensure thread safety and optimal performance.

Here's a tabular comparison between synchronized collections and concurrent collections:

| Aspect                        | Synchronized Collections                           | Concurrent Collections                         |
|-------------------------------|---------------------------------------------------|-----------------------------------------------|
| **Usage**                     | Regular collections wrapped with synchronization mechanisms. | Specifically designed for high-concurrency scenarios. |
| **Concurrency Control**       | Coarse-grained locking, locking the entire collection during modification operations. | Fine-grained locking strategies, allowing multiple threads to access the collection concurrently. |
| **Lock Granularity**          | Locks the entire collection during modification operations. | Uses finer-grained locking strategies, such as lock-striping or lock-free algorithms. |
| **Scalability**               | May suffer from contention issues in highly concurrent environments. | Offers better scalability and performance in high-concurrency scenarios. |
| **Performance**               | Generally less efficient in highly concurrent environments due to contention. | Provides better performance and scalability compared to synchronized collections. |
| **Java Package**              | `java.util.Collections`                           | `java.util.concurrent`                       |
| **Examples**                  | `Collections.synchronizedList(new ArrayList<>())` | `ConcurrentHashMap`, `ConcurrentLinkedQueue`, etc. |
| **Suitability**               | Suitable for low to moderate concurrency scenarios. | Ideal for highly concurrent scenarios with multiple threads accessing the collection simultaneously. |

### Summary:

- **Synchronized Collections:**
   - Use coarse-grained locking and wrap regular collections with synchronization mechanisms.
   - Suitable for low to moderate concurrency scenarios but may suffer from contention in highly concurrent environments.

- **Concurrent Collections:**
   - Use finer-grained locking strategies and are specifically designed for high-concurrency scenarios.
   - Offer better scalability and performance compared to synchronized collections in highly concurrent environments.

When choosing between synchronized and concurrent collections, consider the concurrency requirements and performance characteristics of your application to select the most appropriate collection type.

# Collection vs Collections
The terms "Collection" and "Collections" in Java refer to different concepts:

### Collection:

- **Concept:**
   - `Collection` (singular) is an interface in the `java.util` package.
   - It represents a group of objects, known as elements, and provides a unified way to manipulate and organize these elements.

- **Purpose:**
   - `Collection` provides a high-level abstraction for working with different types of collections, such as lists, sets, queues, etc.
   - It defines common methods for adding, removing, querying, and iterating over elements in a collection.

- **Example:**
  ```java
  Collection<String> list = new ArrayList<>();
  list.add("Hello");
  list.add("World");
  System.out.println(list); // Output: [Hello, World]
  ```

### Collections:

- **Concept:**
   - `Collections` (plural) is a utility class in the `java.util` package.
   - It consists of static methods that operate on or return collections, providing various utility functions for working with collections.

- **Purpose:**
   - `Collections` class contains methods for performing operations such as sorting, searching, shuffling, synchronization, etc., on collections.
   - It serves as a helper class for common collection-related tasks, offering functionalities not directly available in the `Collection` interface.

- **Example:**
  ```java
  List<String> list = new ArrayList<>();
  list.add("World");
  list.add("Hello");
  Collections.sort(list); // Sorts the list
  System.out.println(list); // Output: [Hello, World]
  ```

### Summary:

- **Collection (Interface):**
   - Represents a group of elements and provides a unified way to manipulate them.
   - Part of the core Java Collections Framework.

- **Collections (Utility Class):**
   - Provides static utility methods for working with collections.
   - Offers functionalities such as sorting, searching, shuffling, synchronization, etc.

In summary, "Collection" represents a group of elements as an interface, while "Collections" is a utility class providing various operations and functionalities for working with collections.

# hashCode() and equals() method contract
The `hashCode()` and `equals()` methods in Java are used to implement object equality and are part of the `Object` class. They work together to define how objects are compared and stored in collections such as `HashSet`, `HashMap`, `Hashtable`, etc. Understanding their contract is crucial to ensuring correct behavior when using collections.

### `equals()` Method:

- **Definition:**
   - The `equals()` method is used to compare the equality of two objects.
   - It is a method inherited from the `Object` class and is overridden in subclasses to provide custom equality comparison logic.

- **Contract:**
   - Reflexive: For any non-null reference value `x`, `x.equals(x)` should return `true`.
   - Symmetric: For any non-null reference values `x` and `y`, `x.equals(y)` should return `true` if and only if `y.equals(x)` returns `true`.
   - Transitive: For any non-null reference values `x`, `y`, and `z`, if `x.equals(y)` returns `true` and `y.equals(z)` returns `true`, then `x.equals(z)` should return `true`.
   - Consistent: For any non-null reference values `x` and `y`, multiple invocations of `x.equals(y)` consistently return the same result.
   - Non-nullity: For any non-null reference value `x`, `x.equals(null)` should return `false`.

### `hashCode()` Method:

- **Definition:**
   - The `hashCode()` method returns a hash code value for the object, which is used to store objects in hash-based data structures like `HashSet` or `HashMap`.
   - It is also inherited from the `Object` class and is overridden to provide a custom hash code calculation.

- **Contract:**
   - Consistency with `equals()`: If two objects are equal according to the `equals()` method, then they must have the same hash code value.
   - Consistency with `hashCode()`: Multiple invocations of the `hashCode()` method on the same object must consistently return the same integer, provided no information used in `equals()` comparisons on the object is modified.
   - Non-nullity: The `hashCode()` method must not throw an exception for any object.

### Summary:

- The `equals()` method defines object equality based on custom comparison logic.
- The `hashCode()` method generates a hash code value used for efficient storage and retrieval in hash-based collections.
- Both methods must be implemented consistently according to their contracts to ensure correct behavior when using collections.
  Let's create an example class called `Person` to demonstrate the implementation of the `equals()` and `hashCode()` methods according to their contracts:

```java
public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Getters and setters

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && name.equals(person.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + age;
        return result;
    }

    public static void main(String[] args) {
        Person person1 = new Person("John", 30);
        Person person2 = new Person("John", 30);
        Person person3 = new Person("Jane", 25);

        // Testing equals() method
        System.out.println("person1 equals person2: " + person1.equals(person2)); // true
        System.out.println("person1 equals person3: " + person1.equals(person3)); // false

        // Testing hashCode() method
        System.out.println("HashCode of person1: " + person1.hashCode());
        System.out.println("HashCode of person2: " + person2.hashCode());
        System.out.println("HashCode of person3: " + person3.hashCode());
    }
}
```

In this example:

- We have a `Person` class with `name` and `age` attributes.
- We override the `equals()` method to compare `Person` objects based on both `name` and `age`.
- We override the `hashCode()` method to generate a hash code based on the `name` and `age`.
- In the `main()` method, we create three `Person` objects and test the `equals()` and `hashCode()` methods.