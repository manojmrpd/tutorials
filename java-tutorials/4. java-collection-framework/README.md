# Java Collection Framework

# Table of Contents

1. **Introduction**
   - [What is Collection Framework in Java?](#what-is-collection-framework-in-java)
   - [What is the difference between Array and Collection?](#what-is-the-difference-between-array-and-collection)

2. **Queue and Related Interfaces**
   - [What is Queue?](#what-is-queue)  
   - [What is PriorityQueue?](#what-is-priorityqueue)  
   - [What is PriorityBlockingQueue?](#what-is-priorityblockingqueue)  
   - [What is Deque?](#what-is-deque)  
   - [What is the difference between Queue and Deque?](#what-is-the-difference-between-queue-and-deque)
   - [What is ArrayDeque?](#what-is-arraydeque)  
   - [What is ConcurrentLinkedQueue?](#what-is-concurrentlinkedqueue)

3. **List and Related Classes**
   - [What is List?](#what-is-list)  
   - [What is ArrayList?](#what-is-arraylist)  
   - [What is LinkedList?](#what-is-linkedlist)  
   - [What is Vector?](#what-is-vector)  
   - [What is Stack?](#what-is-stack)  
   - [What is the difference between ArrayList and LinkedList?](#what-is-the-difference-between-arraylist-and-linkedlist)  
   - [What is the difference between CopyOnWriteArrayList and CopyOnWriteArraySet?](#what-is-the-difference-between-copyonwritearraylist-and-copyonwritearrayset)

4. **Set and Related Interfaces**
   - [What is Set?](#what-is-set)  
   - [What is HashSet?](#what-is-hashset)  
   - [What is LinkedHashSet?](#what-is-linkedhashset)  
   - [What is the difference between SortedSet, NavigableSet, and TreeSet?](#what-is-the-difference-between-sortedset-navigableset-and-treeset)  
   - [What is ConcurrentSkipListSet?](#what-is-concurrentskiplistset)

5. **Map and Related Classes**
   - [What is Map?](#what-is-map)  
   - [What is Hashtable?](#what-is-hashtable)  
   - [What is HashMap?](#what-is-hashmap)  
   - [What is LinkedHashMap?](#what-is-linkedhashmap)  
   - [What is the difference between SortedMap, NavigableMap, and TreeMap?](#what-is-the-difference-between-sortedmap-navigablemap-and-treemap)  
   - [What is ConcurrentHashMap?](#what-is-concurrenthashmap)  
   - [What is ConcurrentSkipListMap?](#what-is-concurrentskiplistmap)  
   - [What is the difference between EnumMap, IdentityHashMap, and WeakHashMap?](#what-is-the-difference-between-enummap-identityhashmap-and-weakhashmap)

6. **Internal Workings**
   - [Explain the internal working of LinkedList.](#explain-the-internal-working-of-linkedlist)  
   - [Explain the internal working of HashMap.](#explain-the-internal-working-of-hashmap)  
   - [Explain the internal working of ConcurrentHashMap.](#explain-the-internal-working-of-concurrenthashmap)  
   - [Explain the internal working of TreeMap.](#explain-the-internal-working-of-treemap)

7. **Core Concepts and Principles**
   - [What is a Resizable Array?](#what-is-a-resizable-array)  
   - [What is the Skip List Data Structure?](#what-is-the-skip-list-data-structure)  
   - [What is the difference between Single and Doubly LinkedList?](#what-is-the-difference-between-single-and-doubly-linkedlist)  
   - [What is Hashing/Re-hashing?](#what-is-hashingre-hashing)  
   - [What is Initial Capacity vs Load Factor?](#what-is-initial-capacity-vs-load-factor)  
   - [What is a Hash Collision - Chaining vs Open Addressing?](#what-is-a-hash-collision---chaining-vs-open-addressing)  
   - [What is Treefication / Treefy Threshold?](#what-is-treefication--treefy-threshold)  
   - [What is a Red-Black Tree / Self-balanced Binary Search Tree?](#what-is-a-red-black-tree--self-balanced-binary-search-tree)  
   - [What is the Compare and Swap (CAS) Algorithm?](#what-is-the-compare-and-swap-cas-algorithm)  
   - [What is Segment Locking in ConcurrentHashMap?](#what-is-segment-locking-in-concurrenthashmap)

8. **Ordering and Iteration**
   - [What is FIFO vs LIFO principle?](#what-is-fifo-vs-lifo-principle)  
   - [What is the Access Order of LinkedHashMap?](#what-is-the-access-order-of-linkedhashmap)  
   - [What is the difference between Iterator, ListIterator, and Enumeration?](#what-is-the-difference-between-iterator-listiterator-and-enumeration)  
   - [What is the difference between Comparator and Comparable?](#what-is-the-difference-between-comparator-and-comparable)

9. **Performance and Exceptions**
   - [Explain the Time Complexity (Big-O Notation) of all collection methods.](#explain-the-time-complexity-big-o-notation-of-all-collection-methods)  
   - [Explain Sorting, Searching, and Ordering of all collection classes.](#explain-sorting-searching-and-ordering-of-all-collection-classes)  
   - [Explain Null and Duplicate Behavior of all Collection classes.](#explain-null-and-duplicate-behavior-of-all-collection-classes)  
   - [What is the difference between Fail-fast and Fail-safe Iterator?](#what-is-the-difference-between-fail-fast-and-fail-safe-iterator)  
   - [How to avoid ConcurrentModificationException?](#how-to-avoid-concurrentmodificationexception)  
   - [How to avoid Thread Race Conditions?](#how-to-avoid-thread-race-conditions)

10. **Synchronized and Concurrent Collections**
    - [What is the difference between Synchronized and Concurrent Collections?](#what-is-the-difference-between-synchronized-and-concurrent-collections)

11. **Miscellaneous**
    - [What is the difference between Collection and Collections?](#what-is-the-difference-between-collection-and-collections)  
    - [What is the difference between hashCode() and equals() method contract?](#what-is-the-difference-between-hashcode-and-equals-method-contract)

# What is Collection Framework in Java?
## **Collection Framework in Java**

The **Collection Framework** in Java is a set of classes and interfaces that implement commonly used data structures such as Lists, Sets, and Queues. It provides a unified architecture to store, manipulate, and retrieve groups of objects efficiently.

### **Hierarchy of Collection Framework in Java**

Java Collection Framework consists of the following core interfaces:

1. **Collection Interface (Root Interface)**
   - Extends `Iterable`
   - Contains common methods like `add()`, `remove()`, `size()`, `isEmpty()`, etc.

2. **List Interface** (Ordered Collection)
   - Allows duplicate elements
   - Preserves insertion order
   - Supports random access (indexed access)
   - Implemented by:
     - `ArrayList`
     - `LinkedList`
     - `Vector` (Thread-safe, Legacy)
     - `Stack` (LIFO)

3. **Set Interface** (Unique Elements)
   - Does not allow duplicates
   - Implemented by:
     - `HashSet` (Unordered)
     - `LinkedHashSet` (Ordered by insertion)
     - `TreeSet` (Sorted)

4. **Queue Interface** (FIFO Structure)
   - Used for holding elements before processing
   - Implemented by:
     - `PriorityQueue` (Sorted Order)
     - `ArrayDeque` (Deque operations)

5. **Deque Interface** (Double-Ended Queue)
   - Supports element insertion/removal from both ends
   - Implemented by:
     - `ArrayDeque`
     - `LinkedList`

6. **Map Interface** (Key-Value Pairs)
   - Stores key-value pairs (Keys must be unique)
   - Does not extend `Collection`
   - Implemented by:
     - `HashMap` (Unordered, Fast Access)
     - `LinkedHashMap` (Insertion Order Maintained)
     - `TreeMap` (Sorted by Key)
     - `Hashtable` (Thread-safe, Legacy)
     - `ConcurrentHashMap` (Thread-safe)

### **Hierarchy Diagram**
```
                 Iterable
                     │
                 Collection
         ┌─────────┴───────────┐
        List                 Set                 Queue
      ┌──┴────┐       ┌───────┴───────┐     ┌───────────┐
  ArrayList  LinkedList  HashSet   LinkedHashSet   PriorityQueue
      │          │                     │
    Vector     Stack                TreeSet
      │
    Stack
```
### **Performance & Time Complexity of Collection Classes**
| Collection Class  | Data Structure | Insertion | Deletion | Search | Access |
|------------------|--------------|-----------|----------|--------|--------|
| **ArrayList** | Dynamic Array | O(1) (amortized) | O(n) | O(n) | O(1) |
| **LinkedList** | Doubly Linked List | O(1) | O(1) | O(n) | O(n) |
| **Vector** | Dynamic Array (Thread-safe) | O(1) (amortized) | O(n) | O(n) | O(1) |
| **Stack** | Dynamic Array (LIFO) | O(1) | O(1) | O(n) | O(1) |
| **HashSet** | Hash Table | O(1) | O(1) | O(1) | - |
| **LinkedHashSet** | Hash Table + LinkedList | O(1) | O(1) | O(1) | - |
| **TreeSet** | Red-Black Tree | O(log n) | O(log n) | O(log n) | - |
| **PriorityQueue** | Binary Heap | O(log n) | O(log n) | O(n) | - |
| **ArrayDeque** | Resizable Array | O(1) | O(1) | O(n) | - |
| **HashMap** | Hash Table | O(1) | O(1) | O(1) | O(1) |
| **LinkedHashMap** | Hash Table + LinkedList | O(1) | O(1) | O(1) | O(1) |
| **TreeMap** | Red-Black Tree | O(log n) | O(log n) | O(log n) | O(log n) |

### **Detailed Explanation of Collection Classes:**

#### **1. List Implementations**
- **ArrayList**: Uses a **dynamic array**, fast for random access, slow for insert/delete in the middle.
- **LinkedList**: Uses a **doubly linked list**, efficient for insert/delete in the middle, slow for access.
- **Vector**: Like `ArrayList`, but **synchronized** (thread-safe).
- **Stack**: Extends `Vector`, **Last-In-First-Out (LIFO)** operations.

#### **2. Set Implementations**
- **HashSet**: Uses **hash table**, provides constant-time performance for basic operations.
- **LinkedHashSet**: Like `HashSet` but maintains **insertion order**.
- **TreeSet**: Uses a **Red-Black Tree**, sorts elements in **ascending order**.

#### **3. Queue Implementations**
- **PriorityQueue**: A **binary heap**, orders elements based on priority.
- **ArrayDeque**: Faster than `Stack` and `LinkedList`, allows **double-ended queue** operations.

#### **4. Map Implementations**
- **HashMap**: Uses **hash table**, allows fast retrieval but does not guarantee order.
- **LinkedHashMap**: Like `HashMap` but maintains **insertion order**.
- **TreeMap**: Uses **Red-Black Tree**, sorts keys in natural order.
- **Hashtable**: Like `HashMap`, but **synchronized** (thread-safe).

### **Conclusion**
- Use **ArrayList** when frequent **access** is needed.
- Use **LinkedList** when frequent **insertion/deletion** is needed.
- Use **HashSet** for unique elements **without order**.
- Use **TreeSet** for **sorted unique elements**.
- Use **PriorityQueue** for **priority-based** ordering.
- Use **HashMap** for **fast key-value lookups**.
- Use **TreeMap** for **sorted key-value pairs**.

# What is the difference between Array and Collection?
### **Difference Between Array and Collection in Java**

Both **Array** and **Collection** are used to store multiple elements in Java, but they have significant differences in terms of functionality, flexibility, and performance.

| Feature           | **Array** | **Collection (Java Collection Framework)** |
|------------------|---------|----------------------------------|
| **Size** | Fixed size (defined at creation) | Dynamic size (can grow or shrink) |
| **Data Type** | Can store **both primitive types and objects** | Can store **only objects** (Wrapper class needed for primitives) |
| **Performance** | Fast access (direct indexing) | Slightly slower (due to extra operations) |
| **Memory** | Efficient (uses contiguous memory) | Uses more memory (extra structures for resizing, maintaining links) |
| **Flexibility** | Less flexible (no built-in methods for searching, sorting, etc.) | More flexible (provides built-in methods like `add()`, `remove()`, `sort()`, etc.) |
| **Implementation** | Part of core Java (no extra imports needed) | Part of `java.util` package (must be imported) |
| **Iterating Elements** | Uses `for` or `while` loop | Uses `Iterator`, `forEach()`, and enhanced `for` loop |
| **Type of Storage** | Can store homogeneous data (same type) | Can store heterogeneous data (with Generics) |
| **Multithreading** | Not synchronized | Some collections (like `Vector`, `ConcurrentHashMap`) are synchronized |
| **Operations** | No built-in methods for insert, delete, search | Supports many operations like insertion, deletion, searching, sorting |

---

### **When to Use What?**
- **Use Arrays when:**
  - You need **fixed-size** storage.
  - You need **fast random access** (direct indexing).
  - You are working with **primitive data types**.

- **Use Collection when:**
  - You need a **dynamic** data structure.
  - You need to **store objects**.
  - You need **advanced operations** like sorting, searching, etc.

Would you like a practical code example demonstrating the differences? 😊

# What is Queue?
## **Queue in Java**
A **Queue** is a linear data structure that follows the **FIFO (First-In-First-Out)** principle, meaning elements are added at the **rear** and removed from the **front**.

### **Queue Interface (Java.util.Queue)**
The `Queue` interface is part of the **Java Collection Framework** and is implemented by multiple classes like:
- **PriorityQueue** (Ordered based on priority)
- **LinkedList** (Doubly linked list structure)
- **ArrayDeque** (Resizable array implementation)

---

## **Hierarchy of Queue in Java**
```
            Collection
                │
              Queue
      ┌────────┴────────┐
  Deque             PriorityQueue
   │                    │
ArrayDeque         (Min-Heap, Max-Heap)
```
---

## **Queue Methods with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Inserts an element, throws exception if queue is full. | `queue.add(10);` |
| **`offer(E e)`** | Inserts an element, returns `false` if queue is full. | `queue.offer(20);` |
| **`remove()`** | Removes and returns the front element, throws an exception if empty. | `int val = queue.remove();` |
| **`poll()`** | Removes and returns the front element, returns `null` if empty. | `Integer val = queue.poll();` |
| **`element()`** | Retrieves the front element without removal, throws exception if empty. | `int front = queue.element();` |
| **`peek()`** | Retrieves the front element without removal, returns `null` if empty. | `Integer front = queue.peek();` |

---

## **Example Usage of Queue in Java**
```java
import java.util.*;

public class QueueExample {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        // Adding elements
        queue.add(10);
        queue.offer(20);
        queue.offer(30);

        // Retrieving elements
        System.out.println("Front element: " + queue.peek()); // Output: 10

        // Removing elements
        System.out.println("Removed: " + queue.poll()); // Output: 10

        // Checking the front element after removal
        System.out.println("Front after removal: " + queue.element()); // Output: 20
    }
}
```

---

## **Comparison of Queue Operations & Time Complexity**
| Operation | `LinkedList` (Queue) | `PriorityQueue` | `ArrayDeque` |
|-----------|----------------------|----------------|-------------|
| **Enqueue (add/offer)** | O(1) | O(log n) | O(1) |
| **Dequeue (remove/poll)** | O(1) | O(log n) | O(1) |
| **Peek (element/peek)** | O(1) | O(1) | O(1) |
| **Contains/Search** | O(n) | O(n) | O(n) |

---

## **Types of Queues in Java**
1. **PriorityQueue** (Orders elements based on priority)
   ```java
   PriorityQueue<Integer> pq = new PriorityQueue<>();
   pq.add(5);
   pq.add(1);
   pq.add(3);
   System.out.println(pq.poll()); // Output: 1 (smallest element)
   ```
   
2. **Deque (Double-Ended Queue)** (`ArrayDeque`)
   ```java
   Deque<Integer> deque = new ArrayDeque<>();
   deque.addFirst(10);
   deque.addLast(20);
   System.out.println(deque.removeFirst()); // Output: 10
   ```

3. **BlockingQueue (Thread-Safe)**
   - `ArrayBlockingQueue`
   - `LinkedBlockingQueue`
   ```java
   BlockingQueue<Integer> bq = new ArrayBlockingQueue<>(3);
   bq.put(1);
   bq.put(2);
   System.out.println(bq.take()); // Output: 1
   ```

---

### **Conclusion**
- Use **`LinkedList`** if frequent insertions/deletions are needed.
- Use **`PriorityQueue`** if elements need to be processed in priority order.
- Use **`ArrayDeque`** for fast **FIFO** and **LIFO** operations.
- Use **`BlockingQueue`** for **multithreaded applications**.

Here’s a **real-time implementation of Queue** in Java with different use cases:

---

### **1. Bank Service Simulation using Queue**
A real-world scenario where a queue is used is in a **bank service system**, where customers arrive and are served in a **FIFO (First-In-First-Out) order**.

```java
import java.util.LinkedList;
import java.util.Queue;

class BankService {
    public static void main(String[] args) {
        Queue<String> customerQueue = new LinkedList<>();

        // Customers arriving
        customerQueue.add("Customer 1");
        customerQueue.add("Customer 2");
        customerQueue.add("Customer 3");

        System.out.println("Customers in queue: " + customerQueue);

        // Serving customers
        while (!customerQueue.isEmpty()) {
            System.out.println("Serving: " + customerQueue.poll());
        }

        System.out.println("All customers served.");
    }
}
```
**Output:**
```
Customers in queue: [Customer 1, Customer 2, Customer 3]
Serving: Customer 1
Serving: Customer 2
Serving: Customer 3
All customers served.
```

---

### **2. Task Scheduling using PriorityQueue**
A **PriorityQueue** is useful when tasks have different priorities (e.g., in CPU scheduling).

```java
import java.util.PriorityQueue;

class Task implements Comparable<Task> {
    String name;
    int priority;

    public Task(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    @Override
    public int compareTo(Task t) {
        return this.priority - t.priority; // Min Heap (low priority first)
    }

    @Override
    public String toString() {
        return "[Task: " + name + ", Priority: " + priority + "]";
    }
}

public class TaskScheduler {
    public static void main(String[] args) {
        PriorityQueue<Task> taskQueue = new PriorityQueue<>();

        // Adding tasks with different priorities
        taskQueue.add(new Task("Fix Bug", 2));
        taskQueue.add(new Task("Code Review", 3));
        taskQueue.add(new Task("Implement Feature", 1));

        System.out.println("Task execution order:");
        while (!taskQueue.isEmpty()) {
            System.out.println(taskQueue.poll());
        }
    }
}
```

**Output:**
```
Task execution order:
[Task: Implement Feature, Priority: 1]
[Task: Fix Bug, Priority: 2]
[Task: Code Review, Priority: 3]
```

---

### **3. Circular Queue using ArrayDeque**
A circular queue is used when we need a **fixed-size queue** that wraps around when full.

```java
import java.util.ArrayDeque;

class CircularQueue {
    public static void main(String[] args) {
        ArrayDeque<Integer> queue = new ArrayDeque<>(5);

        // Enqueue
        queue.offer(10);
        queue.offer(20);
        queue.offer(30);
        queue.offer(40);
        queue.offer(50);

        System.out.println("Queue: " + queue);

        // Dequeue and enqueue again
        queue.poll();  // Remove 10
        queue.offer(60); // Add 60

        System.out.println("After rotating: " + queue);
    }
}
```

**Output:**
```
Queue: [10, 20, 30, 40, 50]
After rotating: [20, 30, 40, 50, 60]
```

---

### **4. Producer-Consumer Problem using BlockingQueue**
In **multithreading**, a `BlockingQueue` is used for thread-safe producer-consumer problems.

```java
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class Producer implements Runnable {
    private BlockingQueue<Integer> queue;

    public Producer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            for (int i = 1; i <= 5; i++) {
                System.out.println("Produced: " + i);
                queue.put(i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Consumer implements Runnable {
    private BlockingQueue<Integer> queue;

    public Consumer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Integer value = queue.take();
                System.out.println("Consumed: " + value);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class ProducerConsumerExample {
    public static void main(String[] args) {
        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(3);

        Thread producerThread = new Thread(new Producer(queue));
        Thread consumerThread = new Thread(new Consumer(queue));

        producerThread.start();
        consumerThread.start();
    }
}
```

**Output (Interleaved Output)**
```
Produced: 1
Consumed: 1
Produced: 2
Consumed: 2
Produced: 3
Consumed: 3
Produced: 4
Consumed: 4
Produced: 5
Consumed: 5
```

---

### **Final Summary**
| Use Case | Queue Type | Implementation |
|----------|-----------|---------------|
| **Bank Queue System** | FIFO Queue | `LinkedList` |
| **Task Scheduler** | Priority Queue | `PriorityQueue` |
| **Circular Buffer** | Double-ended Queue | `ArrayDeque` |
| **Multithreading (Producer-Consumer)** | Thread-Safe Queue | `BlockingQueue` |

These real-world examples demonstrate how queues are used in various applications. Would you like further enhancements, such as **GUI-based queue simulations**? 😊

# What is PriorityQueue?
## **PriorityQueue in Java**
`PriorityQueue` is a special type of **queue** that orders elements based on their **natural ordering** (ascending order for numbers) or a **custom comparator**. Unlike a regular queue (FIFO), a `PriorityQueue` retrieves elements **based on priority**.

### **Key Features of PriorityQueue**
- **Elements are sorted** based on their **natural order** (Min-Heap) or a **custom Comparator**.
- **No null elements** are allowed.
- **Duplicates are allowed**.
- **Not thread-safe**, but `PriorityBlockingQueue` can be used in a multithreading environment.
- **Uses a Binary Heap** for efficient ordering.

---

## **PriorityQueue Hierarchy**
```
       Collection
           │
        Queue
           │
    PriorityQueue
```
`PriorityQueue` implements the **Queue interface** and extends the **AbstractQueue class**.

---

## **PriorityQueue Methods**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Inserts an element; throws exception if insertion fails. | `pq.add(10);` |
| **`offer(E e)`** | Inserts an element; returns `false` if it fails. | `pq.offer(20);` |
| **`peek()`** | Retrieves the **highest priority** element without removing it. | `System.out.println(pq.peek());` |
| **`poll()`** | Retrieves and removes the **highest priority** element. | `int top = pq.poll();` |
| **`remove()`** | Removes and returns the highest-priority element; throws exception if empty. | `pq.remove();` |
| **`size()`** | Returns the number of elements in the queue. | `pq.size();` |
| **`clear()`** | Removes all elements. | `pq.clear();` |
| **`contains(Object o)`** | Checks if an element exists. | `pq.contains(30);` |

---

## **Example: Basic Usage of PriorityQueue**
```java
import java.util.PriorityQueue;

public class PriorityQueueExample {
    public static void main(String[] args) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();

        // Adding elements
        pq.add(30);
        pq.add(10);
        pq.add(20);

        // Printing top element
        System.out.println("Top element: " + pq.peek()); // Output: 10

        // Removing elements
        System.out.println("Removed: " + pq.poll()); // Output: 10
        System.out.println("Removed: " + pq.poll()); // Output: 20

        // Remaining elements
        System.out.println("Remaining Queue: " + pq); // Output: [30]
    }
}
```

---

## **Time Complexity Comparison**
| Operation | Time Complexity |
|-----------|----------------|
| **Insertion (`add()` or `offer()`)** | O(log n) |
| **Deletion (`poll()` or `remove()`)** | O(log n) |
| **Peek (`peek()`)** | O(1) |
| **Search (`contains()`)** | O(n) |

---

## **Real-Time Implementations of PriorityQueue**

### **1. Task Scheduling System**
Tasks are scheduled based on their priority (Lower priority number means higher urgency).

```java
import java.util.PriorityQueue;

class Task implements Comparable<Task> {
    String taskName;
    int priority;

    public Task(String taskName, int priority) {
        this.taskName = taskName;
        this.priority = priority;
    }

    @Override
    public int compareTo(Task other) {
        return this.priority - other.priority; // Min Heap: lower priority value means higher priority
    }

    @Override
    public String toString() {
        return "Task: " + taskName + ", Priority: " + priority;
    }
}

public class TaskScheduler {
    public static void main(String[] args) {
        PriorityQueue<Task> taskQueue = new PriorityQueue<>();

        // Adding tasks
        taskQueue.add(new Task("Fix Bug", 2));
        taskQueue.add(new Task("Code Review", 3));
        taskQueue.add(new Task("Develop Feature", 1));

        // Processing tasks
        while (!taskQueue.isEmpty()) {
            System.out.println("Processing: " + taskQueue.poll());
        }
    }
}
```

**Output:**
```
Processing: Task: Develop Feature, Priority: 1
Processing: Task: Fix Bug, Priority: 2
Processing: Task: Code Review, Priority: 3
```

---

### **2. Emergency Room Simulation**
Patients with higher severity (lower severity number means more critical) are treated first.

```java
import java.util.PriorityQueue;

class Patient implements Comparable<Patient> {
    String name;
    int severity;

    public Patient(String name, int severity) {
        this.name = name;
        this.severity = severity;
    }

    @Override
    public int compareTo(Patient p) {
        return this.severity - p.severity; // Higher severity gets priority
    }

    @Override
    public String toString() {
        return "Patient: " + name + ", Severity: " + severity;
    }
}

public class EmergencyRoom {
    public static void main(String[] args) {
        PriorityQueue<Patient> hospitalQueue = new PriorityQueue<>();

        // Adding patients
        hospitalQueue.add(new Patient("Alice", 3));
        hospitalQueue.add(new Patient("Bob", 1));  // Most critical
        hospitalQueue.add(new Patient("Charlie", 2));

        // Treating patients
        while (!hospitalQueue.isEmpty()) {
            System.out.println("Treating: " + hospitalQueue.poll());
        }
    }
}
```

**Output:**
```
Treating: Patient: Bob, Severity: 1
Treating: Patient: Charlie, Severity: 2
Treating: Patient: Alice, Severity: 3
```

---

### **3. Job Processing in a Printer**
A printer processes print jobs in priority order.

```java
import java.util.PriorityQueue;

class PrintJob implements Comparable<PrintJob> {
    String documentName;
    int priority;

    public PrintJob(String documentName, int priority) {
        this.documentName = documentName;
        this.priority = priority;
    }

    @Override
    public int compareTo(PrintJob pj) {
        return this.priority - pj.priority; // Higher priority prints first
    }

    @Override
    public String toString() {
        return "PrintJob: " + documentName + ", Priority: " + priority;
    }
}

public class PrintJobManager {
    public static void main(String[] args) {
        PriorityQueue<PrintJob> printQueue = new PriorityQueue<>();

        printQueue.add(new PrintJob("Project Report", 2));
        printQueue.add(new PrintJob("Invoice", 1));
        printQueue.add(new PrintJob("Presentation", 3));

        while (!printQueue.isEmpty()) {
            System.out.println("Printing: " + printQueue.poll());
        }
    }
}
```

**Output:**
```
Printing: PrintJob: Invoice, Priority: 1
Printing: PrintJob: Project Report, Priority: 2
Printing: PrintJob: Presentation, Priority: 3
```

---

### **Conclusion**
| Use Case | Data Type | Priority Logic |
|----------|-----------|---------------|
| **Task Scheduling** | Task Objects | Lower number → Higher priority |
| **Emergency Room** | Patient Objects | Lower severity → Higher priority |
| **Print Job** | Document Objects | Lower priority value → Higher priority |

`PriorityQueue` is useful in scenarios requiring **automatic ordering** based on priority.

# What is PriorityBlockingQueue?
`PriorityBlockingQueue` is a **thread-safe** implementation of `PriorityQueue` that is part of Java’s **concurrent** package. It is used in **multi-threaded applications** where multiple threads need access to a priority queue concurrently.

## **Key Features of PriorityBlockingQueue**
- Implements **BlockingQueue** interface.
- Allows **unbounded capacity** (grows dynamically).
- Elements are sorted based on **natural ordering** or a **custom comparator**.
- **Thread-safe**, supports multiple producer-consumer threads.
- Unlike other BlockingQueues, it **does not block on insertion/removal** when the queue is empty or full.
- **Null elements are not allowed.**
- Uses **binary heap** internally.

---

## **Class Declaration**
```java
public class PriorityBlockingQueue<E> extends AbstractQueue<E> 
    implements BlockingQueue<E>, Serializable
```

---

## **PriorityBlockingQueue Methods**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Inserts an element, throws exception if insertion fails. | `pq.add(10);` |
| **`offer(E e)`** | Inserts an element, returns `false` if insertion fails. | `pq.offer(20);` |
| **`put(E e)`** | Inserts element, does **not block** since it's unbounded. | `pq.put(30);` |
| **`poll()`** | Retrieves and removes the highest-priority element, returns `null` if empty. | `int top = pq.poll();` |
| **`take()`** | Removes and returns the highest-priority element, **blocks if empty**. | `int item = pq.take();` |
| **`peek()`** | Retrieves the highest-priority element without removing it. | `System.out.println(pq.peek());` |
| **`remove()`** | Removes and returns the highest-priority element, throws exception if empty. | `pq.remove();` |
| **`size()`** | Returns the number of elements in the queue. | `pq.size();` |
| **`clear()`** | Removes all elements. | `pq.clear();` |
| **`contains(Object o)`** | Checks if an element exists. | `pq.contains(30);` |

---

## **Example: Basic Usage of PriorityBlockingQueue**
```java
import java.util.concurrent.PriorityBlockingQueue;

public class PriorityBlockingQueueExample {
    public static void main(String[] args) {
        PriorityBlockingQueue<Integer> pq = new PriorityBlockingQueue<>();

        // Adding elements
        pq.add(30);
        pq.add(10);
        pq.add(20);

        // Printing top element
        System.out.println("Top element: " + pq.peek()); // Output: 10

        // Removing elements
        System.out.println("Removed: " + pq.poll()); // Output: 10
        System.out.println("Removed: " + pq.poll()); // Output: 20

        // Remaining elements
        System.out.println("Remaining Queue: " + pq); // Output: [30]
    }
}
```

---

## **Time Complexity Comparison**
| Operation | Time Complexity |
|-----------|----------------|
| **Insertion (`add()` or `put()`)** | O(log n) |
| **Deletion (`poll()` or `take()`)** | O(log n) |
| **Peek (`peek()`)** | O(1) |
| **Search (`contains()`)** | O(n) |

---

## **Real-Time Implementations of PriorityBlockingQueue**

### **1. Multi-Threaded Task Scheduler (Producer-Consumer)**
A `PriorityBlockingQueue` ensures that high-priority tasks are executed first in a **multi-threaded** environment.

```java
import java.util.concurrent.PriorityBlockingQueue;

class Task implements Comparable<Task> {
    String name;
    int priority;

    public Task(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    @Override
    public int compareTo(Task other) {
        return this.priority - other.priority; // Lower value means higher priority
    }

    @Override
    public String toString() {
        return "Task: " + name + ", Priority: " + priority;
    }
}

class TaskProducer implements Runnable {
    private PriorityBlockingQueue<Task> queue;

    public TaskProducer(PriorityBlockingQueue<Task> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            queue.put(new Task("Fix Bug", 2));
            queue.put(new Task("Code Review", 3));
            queue.put(new Task("Develop Feature", 1));
            System.out.println("Tasks added.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class TaskConsumer implements Runnable {
    private PriorityBlockingQueue<Task> queue;

    public TaskConsumer(PriorityBlockingQueue<Task> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (!queue.isEmpty()) {
                System.out.println("Processing: " + queue.take());
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class MultiThreadedTaskScheduler {
    public static void main(String[] args) {
        PriorityBlockingQueue<Task> taskQueue = new PriorityBlockingQueue<>();

        Thread producerThread = new Thread(new TaskProducer(taskQueue));
        Thread consumerThread = new Thread(new TaskConsumer(taskQueue));

        producerThread.start();
        consumerThread.start();
    }
}
```

**Output:**
```
Tasks added.
Processing: Task: Develop Feature, Priority: 1
Processing: Task: Fix Bug, Priority: 2
Processing: Task: Code Review, Priority: 3
```

---

### **2. Emergency Room Simulation (Multi-threaded)**
Patients with higher severity get treated first.

```java
import java.util.concurrent.PriorityBlockingQueue;

class Patient implements Comparable<Patient> {
    String name;
    int severity;

    public Patient(String name, int severity) {
        this.name = name;
        this.severity = severity;
    }

    @Override
    public int compareTo(Patient other) {
        return this.severity - other.severity; // Lower severity gets priority
    }

    @Override
    public String toString() {
        return "Patient: " + name + ", Severity: " + severity;
    }
}

class Doctor implements Runnable {
    private PriorityBlockingQueue<Patient> patientQueue;

    public Doctor(PriorityBlockingQueue<Patient> patientQueue) {
        this.patientQueue = patientQueue;
    }

    @Override
    public void run() {
        try {
            while (!patientQueue.isEmpty()) {
                System.out.println("Treating: " + patientQueue.take());
                Thread.sleep(1500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class Hospital {
    public static void main(String[] args) {
        PriorityBlockingQueue<Patient> patientQueue = new PriorityBlockingQueue<>();

        // Adding patients
        patientQueue.add(new Patient("Alice", 3));
        patientQueue.add(new Patient("Bob", 1)); // Most critical
        patientQueue.add(new Patient("Charlie", 2));

        // Doctor treating patients
        Thread doctorThread = new Thread(new Doctor(patientQueue));
        doctorThread.start();
    }
}
```

**Output:**
```
Treating: Patient: Bob, Severity: 1
Treating: Patient: Charlie, Severity: 2
Treating: Patient: Alice, Severity: 3
```

---

## **Final Comparison**
| Feature | `PriorityQueue` | `PriorityBlockingQueue` |
|---------|----------------|------------------------|
| **Thread-Safe** | ❌ No | ✅ Yes |
| **Blocking on Empty Queue** | ❌ No | ✅ Yes (`take()`) |
| **Blocking on Full Queue** | ❌ No (Unbounded) | ✅ No (Unbounded) |
| **Ordering** | ✅ Natural/Custom | ✅ Natural/Custom |
| **Null Elements** | ❌ Not allowed | ❌ Not allowed |

---

## **Conclusion**
`PriorityBlockingQueue` is ideal for **multi-threaded** applications that need a **priority-based** queue.

Would you like me to enhance this with **GUI or REST API-based job scheduling**? 😊

# What is Deque?
A **Deque (Double-Ended Queue)** is a data structure that allows **insertion and removal** from **both ends** (**front and rear**). It can be used as both a **Queue (FIFO)** and **Stack (LIFO)**.

---

## **Hierarchy of Deque in Java**
```
        Collection
            │
          Queue
            │
          Deque
     ┌──────────────┐
ArrayDeque     LinkedList
```

### **Classes Implementing Deque:**
1. **`ArrayDeque`** (Resizable array-based implementation, faster than `Stack` and `LinkedList`)
2. **`LinkedList`** (Doubly linked list implementation)

---

## **Deque Methods with Examples**

| Method | Description | Example Usage |
|--------|------------|--------------|
| **`addFirst(E e)`** | Inserts an element at the front, throws exception if full. | `deque.addFirst(10);` |
| **`offerFirst(E e)`** | Inserts an element at the front, returns `false` if full. | `deque.offerFirst(20);` |
| **`addLast(E e)`** | Inserts an element at the rear, throws exception if full. | `deque.addLast(30);` |
| **`offerLast(E e)`** | Inserts an element at the rear, returns `false` if full. | `deque.offerLast(40);` |
| **`removeFirst()`** | Removes and returns the first element, throws exception if empty. | `deque.removeFirst();` |
| **`pollFirst()`** | Removes and returns the first element, returns `null` if empty. | `Integer front = deque.pollFirst();` |
| **`removeLast()`** | Removes and returns the last element, throws exception if empty. | `deque.removeLast();` |
| **`pollLast()`** | Removes and returns the last element, returns `null` if empty. | `Integer last = deque.pollLast();` |
| **`getFirst()`** | Retrieves but does not remove the first element, throws exception if empty. | `int first = deque.getFirst();` |
| **`peekFirst()`** | Retrieves but does not remove the first element, returns `null` if empty. | `Integer first = deque.peekFirst();` |
| **`getLast()`** | Retrieves but does not remove the last element, throws exception if empty. | `int last = deque.getLast();` |
| **`peekLast()`** | Retrieves but does not remove the last element, returns `null` if empty. | `Integer last = deque.peekLast();` |
| **`size()`** | Returns the number of elements. | `deque.size();` |
| **`clear()`** | Removes all elements. | `deque.clear();` |
| **`contains(Object o)`** | Checks if an element exists. | `deque.contains(50);` |

---

## **Example: Basic Usage of Deque**
```java
import java.util.Deque;
import java.util.ArrayDeque;

public class DequeExample {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();

        // Adding elements at both ends
        deque.addFirst(10);
        deque.addLast(20);
        deque.offerFirst(5);
        deque.offerLast(30);

        System.out.println("Deque after insertions: " + deque); // [5, 10, 20, 30]

        // Retrieving elements
        System.out.println("First Element: " + deque.peekFirst()); // Output: 5
        System.out.println("Last Element: " + deque.peekLast()); // Output: 30

        // Removing elements from both ends
        deque.pollFirst();
        deque.pollLast();
        System.out.println("Deque after removals: " + deque); // [10, 20]
    }
}
```

---

## **Time Complexity of Deque Operations**
| Operation | **ArrayDeque** | **LinkedList** |
|-----------|--------------|---------------|
| **Insert at Front (`addFirst() / offerFirst()`)** | O(1) | O(1) |
| **Insert at Rear (`addLast() / offerLast()`)** | O(1) | O(1) |
| **Remove from Front (`pollFirst() / removeFirst()`)** | O(1) | O(1) |
| **Remove from Rear (`pollLast() / removeLast()`)** | O(1) | O(1) |
| **Get First/Last (`peekFirst()/peekLast()`)** | O(1) | O(1) |
| **Search (`contains()`)** | O(n) | O(n) |

---

## **Real-Time Implementations of Deque**

### **1. Browser History (Back & Forward Navigation)**
Deque is used to implement browser history, where we move **back and forward** between pages.

```java
import java.util.Deque;
import java.util.LinkedList;

class BrowserHistory {
    Deque<String> history = new LinkedList<>();
    Deque<String> forwardStack = new LinkedList<>();

    public void visit(String url) {
        history.addLast(url);
        forwardStack.clear(); // Clear forward stack on a new visit
        System.out.println("Visited: " + url);
    }

    public void back() {
        if (history.size() > 1) {
            forwardStack.addFirst(history.removeLast());
            System.out.println("Back to: " + history.getLast());
        } else {
            System.out.println("No previous page.");
        }
    }

    public void forward() {
        if (!forwardStack.isEmpty()) {
            history.addLast(forwardStack.pollFirst());
            System.out.println("Forward to: " + history.getLast());
        } else {
            System.out.println("No forward page.");
        }
    }
}

public class BrowserNavigation {
    public static void main(String[] args) {
        BrowserHistory browser = new BrowserHistory();
        browser.visit("google.com");
        browser.visit("stackoverflow.com");
        browser.visit("github.com");

        browser.back(); // Output: Back to: stackoverflow.com
        browser.back(); // Output: Back to: google.com
        browser.forward(); // Output: Forward to: stackoverflow.com
    }
}
```

---

### **2. Palindrome Checker**
Using `Deque` to check if a string is a palindrome.

```java
import java.util.Deque;
import java.util.ArrayDeque;

public class PalindromeChecker {
    public static boolean isPalindrome(String str) {
        Deque<Character> deque = new ArrayDeque<>();

        for (char c : str.toCharArray()) {
            deque.addLast(c);
        }

        while (deque.size() > 1) {
            if (deque.pollFirst() != deque.pollLast()) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String word = "racecar";
        System.out.println(word + " is palindrome? " + isPalindrome(word)); // true
    }
}
```

---

### **3. Sliding Window Maximum (Deque for Optimization)**
Finding the maximum element in every window of size `k` in an array using `Deque`.

```java
import java.util.Deque;
import java.util.LinkedList;

public class SlidingWindowMax {
    public static void slidingWindowMax(int[] arr, int k) {
        Deque<Integer> deque = new LinkedList<>();

        for (int i = 0; i < arr.length; i++) {
            while (!deque.isEmpty() && deque.peekFirst() < i - k + 1) {
                deque.pollFirst();
            }

            while (!deque.isEmpty() && arr[deque.peekLast()] <= arr[i]) {
                deque.pollLast();
            }

            deque.addLast(i);

            if (i >= k - 1) {
                System.out.print(arr[deque.peekFirst()] + " ");
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, -1, -3, 5, 3, 6, 7};
        int k = 3;
        slidingWindowMax(arr, k); // Output: 3 3 5 5 6 7
    }
}
```

---

## **Final Thoughts**
| Use Case | Data Structure | Implementation |
|----------|---------------|---------------|
| **Browser History** | Deque (Back & Forward) | `LinkedList` |
| **Palindrome Checker** | Deque (Two-Pointer) | `ArrayDeque` |
| **Sliding Window Maximum** | Deque (Optimization) | `LinkedList` |


# What is the difference between Queue and Deque?

Both **Queue** and **Deque (Double-Ended Queue)** are part of the **Java Collection Framework** and are used to store and manage elements. However, they have significant differences in terms of functionality and usage.

---

### **Key Differences Between Queue and Deque**
| Feature | **Queue** | **Deque (Double-Ended Queue)** |
|---------|----------|------------------------------|
| **Definition** | A **linear data structure** that follows **FIFO (First In, First Out)**. | A **linear data structure** that allows insertion and deletion from **both ends**. |
| **Insertion** | Elements are inserted at the **rear**. | Elements can be inserted at **both front and rear**. |
| **Deletion** | Elements are removed from the **front**. | Elements can be removed from **both front and rear**. |
| **Flexibility** | Restricted operations (Only **enqueue** at the rear and **dequeue** from the front). | More flexible, supports **double-ended** operations. |
| **Types** | - Simple Queue (FIFO) <br> - Priority Queue <br> - Circular Queue <br> - BlockingQueue | - **Deque (Double-Ended Queue)** <br> - **ArrayDeque (Efficient)** <br> - **LinkedList (Deque Implementation)** |
| **Methods** | `add()`, `offer()`, `remove()`, `poll()`, `peek()`, `element()` | `addFirst()`, `addLast()`, `removeFirst()`, `removeLast()`, `peekFirst()`, `peekLast()` |
| **Use Cases** | Task Scheduling, Printer Queue, Call Center Queue | Browser History, Palindrome Checker, Sliding Window Problems |

---

### **Diagram Representation**
#### **Queue (FIFO)**
```
Front [10, 20, 30, 40, 50] Rear
Enqueue →  Add at Rear
Dequeue →  Remove from Front
```

#### **Deque (Both Ends)**
```
Front [10, 20, 30, 40, 50] Rear
Can add/remove from both front & rear
```

---

### **Example 1: Queue Implementation (FIFO)**
```java
import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();

        queue.add(10);
        queue.add(20);
        queue.add(30);

        System.out.println("Front Element: " + queue.peek()); // Output: 10
        System.out.println("Removed: " + queue.poll()); // Output: 10
        System.out.println("Remaining Queue: " + queue); // Output: [20, 30]
    }
}
```

---

### **Example 2: Deque Implementation (Both Ends)**
```java
import java.util.Deque;
import java.util.ArrayDeque;

public class DequeExample {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();

        // Adding elements from both ends
        deque.addFirst(10);
        deque.addLast(20);
        deque.offerFirst(5);
        deque.offerLast(30);

        System.out.println("Deque: " + deque); // Output: [5, 10, 20, 30]

        // Removing from both ends
        deque.pollFirst();
        deque.pollLast();
        System.out.println("After Removals: " + deque); // Output: [10, 20]
    }
}
```

---

### **Time Complexity Comparison**
| Operation | **Queue (LinkedList)** | **Deque (ArrayDeque)** |
|-----------|-----------------|-----------------|
| **Enqueue (`add() / offer()`)** | O(1) | O(1) |
| **Dequeue (`poll() / remove()`)** | O(1) | O(1) |
| **Insert at Front (`addFirst() / offerFirst()`)** | ❌ Not Supported | O(1) |
| **Insert at Rear (`addLast() / offerLast()`)** | O(1) | O(1) |
| **Remove at Front (`pollFirst() / removeFirst()`)** | O(1) | O(1) |
| **Remove at Rear (`pollLast() / removeLast()`)** | ❌ Not Supported | O(1) |
| **Search (`contains()`)** | O(n) | O(n) |

---

### **When to Use What?**
| **Use Case** | **Choose Queue** | **Choose Deque** |
|-------------|-----------------|-----------------|
| **FIFO operations only** | ✅ Yes | ❌ No |
| **Insert/Remove at both ends** | ❌ No | ✅ Yes |
| **Task Scheduling (Job Queues, CPU Scheduling)** | ✅ Yes | ❌ No |
| **Sliding Window Maximum** | ❌ No | ✅ Yes |
| **Browser History (Back & Forward Navigation)** | ❌ No | ✅ Yes |
| **Deque as Stack** | ❌ No | ✅ Yes (`ArrayDeque` replaces `Stack`) |

---

### **Conclusion**
- **Use `Queue`** when you only need **FIFO** operations.
- **Use `Deque`** when you need **both front and rear operations**.

# What is ArrayDeque?
`ArrayDeque` is a **resizable, array-based implementation** of the `Deque` interface in Java. It provides a more efficient alternative to `Stack`, `Queue`, and `LinkedList` in most use cases.

---

## **Key Features of `ArrayDeque`**
✅ **Faster than `Stack` and `LinkedList`**  
✅ **Dynamic Resizing** (No fixed capacity)  
✅ **Supports `FIFO` (Queue) and `LIFO` (Stack) operations**  
✅ **No capacity restrictions**  
✅ **Not thread-safe** (Use `ConcurrentLinkedDeque` for thread safety)  
❌ **Does not allow `null` elements**  
❌ **Not synchronized**  

---

## **Hierarchy of ArrayDeque**
```
        Collection
            │
          Queue
            │
          Deque
            │
       ArrayDeque
```
- Implements **Deque**  
- Can be used as **Stack (LIFO)** or **Queue (FIFO)**  

---

## **ArrayDeque Methods with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`addFirst(E e)`** | Inserts an element at the front, throws exception if full. | `deque.addFirst(10);` |
| **`offerFirst(E e)`** | Inserts an element at the front, returns `false` if full. | `deque.offerFirst(20);` |
| **`addLast(E e)`** | Inserts an element at the rear, throws exception if full. | `deque.addLast(30);` |
| **`offerLast(E e)`** | Inserts an element at the rear, returns `false` if full. | `deque.offerLast(40);` |
| **`removeFirst()`** | Removes and returns the first element, throws exception if empty. | `deque.removeFirst();` |
| **`pollFirst()`** | Removes and returns the first element, returns `null` if empty. | `Integer front = deque.pollFirst();` |
| **`removeLast()`** | Removes and returns the last element, throws exception if empty. | `deque.removeLast();` |
| **`pollLast()`** | Removes and returns the last element, returns `null` if empty. | `Integer last = deque.pollLast();` |
| **`getFirst()`** | Retrieves but does not remove the first element, throws exception if empty. | `int first = deque.getFirst();` |
| **`peekFirst()`** | Retrieves but does not remove the first element, returns `null` if empty. | `Integer first = deque.peekFirst();` |
| **`getLast()`** | Retrieves but does not remove the last element, throws exception if empty. | `int last = deque.getLast();` |
| **`peekLast()`** | Retrieves but does not remove the last element, returns `null` if empty. | `Integer last = deque.peekLast();` |
| **`size()`** | Returns the number of elements. | `deque.size();` |
| **`clear()`** | Removes all elements. | `deque.clear();` |
| **`contains(Object o)`** | Checks if an element exists. | `deque.contains(50);` |

---

## **Example: Basic Usage of `ArrayDeque`**
```java
import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeExample {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();

        // Adding elements at both ends
        deque.addFirst(10);
        deque.addLast(20);
        deque.offerFirst(5);
        deque.offerLast(30);

        System.out.println("Deque after insertions: " + deque); // Output: [5, 10, 20, 30]

        // Retrieving elements
        System.out.println("First Element: " + deque.peekFirst()); // Output: 5
        System.out.println("Last Element: " + deque.peekLast()); // Output: 30

        // Removing from both ends
        deque.pollFirst();
        deque.pollLast();
        System.out.println("Deque after removals: " + deque); // Output: [10, 20]
    }
}
```

---

## **Time Complexity Comparison**
| Operation | **ArrayDeque** |
|-----------|--------------|
| **Insert at Front (`addFirst() / offerFirst()`)** | O(1) |
| **Insert at Rear (`addLast() / offerLast()`)** | O(1) |
| **Remove from Front (`pollFirst() / removeFirst()`)** | O(1) |
| **Remove from Rear (`pollLast() / removeLast()`)** | O(1) |
| **Get First/Last (`peekFirst()/peekLast()`)** | O(1) |
| **Search (`contains()`)** | O(n) |

---

## **Real-Time Implementations of `ArrayDeque`**

### **1. Browser History Navigation (Back & Forward)**
```java
import java.util.ArrayDeque;
import java.util.Deque;

class BrowserHistory {
    Deque<String> history = new ArrayDeque<>();
    Deque<String> forwardStack = new ArrayDeque<>();

    public void visit(String url) {
        history.addLast(url);
        forwardStack.clear(); // Clear forward stack on new visit
        System.out.println("Visited: " + url);
    }

    public void back() {
        if (history.size() > 1) {
            forwardStack.addFirst(history.removeLast());
            System.out.println("Back to: " + history.getLast());
        } else {
            System.out.println("No previous page.");
        }
    }

    public void forward() {
        if (!forwardStack.isEmpty()) {
            history.addLast(forwardStack.pollFirst());
            System.out.println("Forward to: " + history.getLast());
        } else {
            System.out.println("No forward page.");
        }
    }
}

public class BrowserNavigation {
    public static void main(String[] args) {
        BrowserHistory browser = new BrowserHistory();
        browser.visit("google.com");
        browser.visit("stackoverflow.com");
        browser.visit("github.com");

        browser.back(); // Output: Back to: stackoverflow.com
        browser.back(); // Output: Back to: google.com
        browser.forward(); // Output: Forward to: stackoverflow.com
    }
}
```

---

### **2. Palindrome Checker**
```java
import java.util.ArrayDeque;
import java.util.Deque;

public class PalindromeChecker {
    public static boolean isPalindrome(String str) {
        Deque<Character> deque = new ArrayDeque<>();

        for (char c : str.toCharArray()) {
            deque.addLast(c);
        }

        while (deque.size() > 1) {
            if (deque.pollFirst() != deque.pollLast()) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String word = "racecar";
        System.out.println(word + " is palindrome? " + isPalindrome(word)); // true
    }
}
```

---

### **3. Stack Implementation Using `ArrayDeque`**
`ArrayDeque` is a better alternative to `Stack`.

```java
import java.util.ArrayDeque;
import java.util.Deque;

public class StackUsingArrayDeque {
    public static void main(String[] args) {
        Deque<Integer> stack = new ArrayDeque<>();

        stack.push(10);
        stack.push(20);
        stack.push(30);

        System.out.println("Top Element: " + stack.peek()); // 30
        System.out.println("Popped: " + stack.pop()); // 30
        System.out.println("Stack: " + stack); // [10, 20]
    }
}
```

---

## **Final Comparison: `ArrayDeque` vs `Stack` vs `Queue`**
| Feature | `ArrayDeque` | `Stack` | `Queue` |
|---------|-------------|--------|--------|
| **FIFO & LIFO** | ✅ Both | ❌ Only LIFO | ✅ FIFO |
| **Dynamic Resizing** | ✅ Yes | ❌ No | ✅ Yes |
| **Thread-Safe** | ❌ No | ❌ No | ❌ No |
| **Performance** | ✅ Faster | ❌ Slower | ✅ Good |

---

## **Conclusion**
- **Use `ArrayDeque` as a `Queue`** if you need **fast insertions & deletions from both ends**.
- **Use `ArrayDeque` as a `Stack`** instead of `Stack` (More efficient).
- **Avoid `LinkedList` for deque operations** (Slower than `ArrayDeque`).

# What is ConcurrentLinkedQueue?
`ConcurrentLinkedQueue<E>` is a **thread-safe, non-blocking, FIFO (First-In-First-Out) queue** in Java that is designed for high-throughput multi-threaded applications. It is part of the **java.util.concurrent** package and uses a **lock-free, non-blocking algorithm** based on **CAS (Compare-And-Swap)** operations.

---

## **Key Features of `ConcurrentLinkedQueue`**
✅ **Thread-safe** (Uses non-blocking lock-free algorithms)  
✅ **FIFO (First-In-First-Out) order**  
✅ **Non-blocking (No thread waits on insert/removal operations)**  
✅ **Better performance than synchronized alternatives**  
✅ **Does not allow `null` elements**  
❌ **Unbounded queue (Can grow indefinitely, no capacity limit)**  
❌ **Does not provide blocking operations like `take()`**  

---

## **Class Declaration**
```java
public class ConcurrentLinkedQueue<E> extends AbstractQueue<E>
    implements Queue<E>, Serializable
```

---

## **Hierarchy of `ConcurrentLinkedQueue`**
```
       Collection
           │
         Queue
           │
 ConcurrentLinkedQueue
```
- Implements **Queue Interface**  
- Uses **Lock-Free Algorithm (CAS-based)**  

---

## **Methods of `ConcurrentLinkedQueue`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Inserts an element at the rear, throws exception if insertion fails. | `queue.add(10);` |
| **`offer(E e)`** | Inserts an element at the rear, returns `false` if insertion fails. | `queue.offer(20);` |
| **`peek()`** | Retrieves but does not remove the head element; returns `null` if empty. | `System.out.println(queue.peek());` |
| **`poll()`** | Retrieves and removes the head element; returns `null` if empty. | `int top = queue.poll();` |
| **`remove()`** | Removes and returns the head element, throws exception if empty. | `queue.remove();` |
| **`size()`** | Returns the number of elements (not always accurate in concurrent scenarios). | `queue.size();` |
| **`clear()`** | Removes all elements. | `queue.clear();` |
| **`contains(Object o)`** | Checks if an element exists. | `queue.contains(30);` |

---

## **Example: Basic Usage of `ConcurrentLinkedQueue`**
```java
import java.util.concurrent.ConcurrentLinkedQueue;

public class ConcurrentLinkedQueueExample {
    public static void main(String[] args) {
        ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>();

        // Adding elements
        queue.add(10);
        queue.offer(20);
        queue.offer(30);

        // Retrieving elements
        System.out.println("Front Element: " + queue.peek()); // Output: 10

        // Removing elements
        System.out.println("Removed: " + queue.poll()); // Output: 10
        System.out.println("Remaining Queue: " + queue); // Output: [20, 30]
    }
}
```

---

## **Time Complexity of `ConcurrentLinkedQueue`**
| Operation | **Time Complexity** |
|-----------|----------------|
| **Insertion (`add()` or `offer()`)** | O(1) |
| **Deletion (`poll()` or `remove()`)** | O(1) |
| **Peek (`peek()`)** | O(1) |
| **Search (`contains()`)** | O(n) |

---

## **Real-Time Implementations of `ConcurrentLinkedQueue`**

### **1. Multi-Threaded Task Processor**
A `ConcurrentLinkedQueue` can be used for **task scheduling** in a **multi-threaded application**.

```java
import java.util.concurrent.ConcurrentLinkedQueue;

class TaskProcessor implements Runnable {
    private ConcurrentLinkedQueue<String> taskQueue;

    public TaskProcessor(ConcurrentLinkedQueue<String> queue) {
        this.taskQueue = queue;
    }

    @Override
    public void run() {
        while (!taskQueue.isEmpty()) {
            String task = taskQueue.poll();
            if (task != null) {
                System.out.println(Thread.currentThread().getName() + " Processing: " + task);
                try {
                    Thread.sleep(500); // Simulate task execution
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

public class MultiThreadedTaskProcessor {
    public static void main(String[] args) {
        ConcurrentLinkedQueue<String> taskQueue = new ConcurrentLinkedQueue<>();

        // Adding tasks to the queue
        taskQueue.add("Task 1");
        taskQueue.add("Task 2");
        taskQueue.add("Task 3");
        taskQueue.add("Task 4");

        // Creating multiple worker threads
        Thread worker1 = new Thread(new TaskProcessor(taskQueue), "Worker 1");
        Thread worker2 = new Thread(new TaskProcessor(taskQueue), "Worker 2");

        // Start processing
        worker1.start();
        worker2.start();
    }
}
```

**Output (Interleaved due to multi-threading)**
```
Worker 1 Processing: Task 1
Worker 2 Processing: Task 2
Worker 1 Processing: Task 3
Worker 2 Processing: Task 4
```

---

### **2. Event Queue for Logging System**
A logging system can use `ConcurrentLinkedQueue` to store logs generated from multiple threads.

```java
import java.util.concurrent.ConcurrentLinkedQueue;

class Logger {
    private ConcurrentLinkedQueue<String> logQueue = new ConcurrentLinkedQueue<>();

    public void log(String message) {
        logQueue.add(Thread.currentThread().getName() + ": " + message);
    }

    public void processLogs() {
        while (!logQueue.isEmpty()) {
            System.out.println("Processing Log: " + logQueue.poll());
        }
    }
}

public class LoggingSystem {
    public static void main(String[] args) {
        Logger logger = new Logger();

        // Multiple threads generating logs
        Runnable logGenerator = () -> {
            for (int i = 1; i <= 3; i++) {
                logger.log("Log Message " + i);
            }
        };

        Thread thread1 = new Thread(logGenerator, "Thread 1");
        Thread thread2 = new Thread(logGenerator, "Thread 2");

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Processing logs
        logger.processLogs();
    }
}
```

**Output (Interleaved Logs)**
```
Processing Log: Thread 1: Log Message 1
Processing Log: Thread 2: Log Message 1
Processing Log: Thread 1: Log Message 2
Processing Log: Thread 2: Log Message 2
Processing Log: Thread 1: Log Message 3
Processing Log: Thread 2: Log Message 3
```

---

## **Comparison: `ConcurrentLinkedQueue` vs Other Queues**
| Feature | `ConcurrentLinkedQueue` | `LinkedList (Queue)` | `ArrayBlockingQueue` |
|---------|-----------------|-----------------|----------------|
| **Thread-Safe** | ✅ Yes | ❌ No | ✅ Yes |
| **Blocking Operations** | ❌ No | ❌ No | ✅ Yes (`take()`) |
| **FIFO Order** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Lock-Free Algorithm** | ✅ Yes (CAS-based) | ❌ No | ❌ No (Locks used) |
| **Performance** | ✅ High | ❌ Slower | ❌ Moderate |
| **Unbounded Growth** | ✅ Yes | ✅ Yes | ❌ No (Fixed Size) |

---

## **Conclusion**
- `ConcurrentLinkedQueue` is **best for multi-threaded applications** where non-blocking **FIFO** operations are required.
- Use `ConcurrentLinkedQueue` in **task scheduling, log processing, and event-driven systems**.
- It is **not suitable for bounded queues** (use `BlockingQueue` like `ArrayBlockingQueue` if you need capacity limits).

# What is List?
A **List** in Java is an **ordered collection** that allows **duplicate elements**. It maintains **insertion order** and provides **indexed access** to elements.

### **Key Features of List**
✅ **Ordered Collection (Maintains Insertion Order)**  
✅ **Allows Duplicate Elements**  
✅ **Allows Null Elements**  
✅ **Index-Based Access (get/set methods available)**  
✅ **Resizable (Unlike Arrays)**  
❌ **Not Thread-Safe (Use `Collections.synchronizedList()` for safety)**  

---

## **Hierarchy of List in Java**
```
       Collection
           │
         List
    ┌──────┼────────┐
ArrayList  LinkedList  Vector
                          │
                        Stack
```

- `ArrayList`: Uses **dynamic array**, fast for **random access**.  
- `LinkedList`: Uses **doubly linked list**, fast for **insertions & deletions**.  
- `Vector`: Like `ArrayList` but **thread-safe** (synchronized).  
- `Stack`: Extends `Vector`, **Last-In-First-Out (LIFO) order**.  

---

## **List Methods with Examples**
| Method | Description | Example |
|--------|------------|---------|
| **`add(E e)`** | Appends an element to the end. | `list.add(10);` |
| **`add(int index, E e)`** | Inserts element at specified index. | `list.add(1, 20);` |
| **`get(int index)`** | Retrieves element at index. | `int val = list.get(2);` |
| **`set(int index, E e)`** | Updates element at index. | `list.set(1, 30);` |
| **`remove(int index)`** | Removes element at index. | `list.remove(2);` |
| **`remove(Object o)`** | Removes first occurrence of element. | `list.remove(Integer.valueOf(10));` |
| **`contains(Object o)`** | Checks if element exists. | `list.contains(20);` |
| **`indexOf(Object o)`** | Returns index of first occurrence. | `int i = list.indexOf(30);` |
| **`lastIndexOf(Object o)`** | Returns index of last occurrence. | `int i = list.lastIndexOf(30);` |
| **`size()`** | Returns number of elements. | `int size = list.size();` |
| **`clear()`** | Removes all elements. | `list.clear();` |
| **`isEmpty()`** | Checks if list is empty. | `boolean b = list.isEmpty();` |

---

## **Example: Basic Usage of `ArrayList`**
```java
import java.util.*;

public class ListExample {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        // Adding elements
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(1, 15); // Insert at index 1

        // Retrieving elements
        System.out.println("Element at index 2: " + list.get(2)); // Output: 20

        // Removing elements
        list.remove(1); // Removes element at index 1 (15)

        // Updating elements
        list.set(1, 25);

        System.out.println("Final List: " + list); // Output: [10, 25, 30]
    }
}
```

---

## **Time Complexity of List Implementations**
| Operation | **ArrayList** | **LinkedList** | **Vector** |
|-----------|-------------|--------------|---------|
| **Insert at End (`add()`)** | O(1) (amortized) | O(1) | O(1) |
| **Insert at Middle (`add(index, e)`)** | O(n) | O(n) | O(n) |
| **Remove at End (`remove(size-1)`)** | O(1) | O(1) | O(1) |
| **Remove at Middle (`remove(index)`)** | O(n) | O(n) | O(n) |
| **Get (`get(index)`)** | O(1) | O(n) | O(1) |
| **Set (`set(index, e)`)** | O(1) | O(n) | O(1) |
| **Search (`contains()`)** | O(n) | O(n) | O(n) |

---

## **Real-Time Implementations of `List`**

### **1. Employee Management System (Using `ArrayList`)**
```java
import java.util.*;

class Employee {
    int id;
    String name;

    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{id=" + id + ", name='" + name + "'}";
    }
}

public class EmployeeManager {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();

        // Adding employees
        employees.add(new Employee(101, "Alice"));
        employees.add(new Employee(102, "Bob"));
        employees.add(new Employee(103, "Charlie"));

        // Searching for an employee
        for (Employee e : employees) {
            if (e.id == 102) {
                System.out.println("Found: " + e);
            }
        }

        // Removing an employee
        employees.removeIf(e -> e.id == 103);

        System.out.println("Employee List: " + employees);
    }
}
```

**Output:**
```
Found: Employee{id=102, name='Bob'}
Employee List: [Employee{id=101, name='Alice'}, Employee{id=102, name='Bob'}]
```

---

### **2. Implementing a Playlist (Using `LinkedList`)**
```java
import java.util.LinkedList;

class Song {
    String title;
    
    public Song(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}

public class Playlist {
    public static void main(String[] args) {
        LinkedList<Song> playlist = new LinkedList<>();

        // Adding songs
        playlist.add(new Song("Song A"));
        playlist.add(new Song("Song B"));
        playlist.addFirst(new Song("Intro Song"));
        playlist.addLast(new Song("Closing Song"));

        System.out.println("Current Playlist: " + playlist);

        // Playing the first song and removing it
        System.out.println("Playing: " + playlist.pollFirst());

        System.out.println("Remaining Playlist: " + playlist);
    }
}
```

**Output:**
```
Current Playlist: [Intro Song, Song A, Song B, Closing Song]
Playing: Intro Song
Remaining Playlist: [Song A, Song B, Closing Song]
```

---

### **3. Stock Price Tracker (Using `Vector`)**
```java
import java.util.Vector;

public class StockPriceTracker {
    public static void main(String[] args) {
        Vector<Double> stockPrices = new Vector<>();

        // Adding stock prices
        stockPrices.add(150.5);
        stockPrices.add(152.3);
        stockPrices.add(148.7);
        stockPrices.add(153.2);

        System.out.println("Stock Prices: " + stockPrices);

        // Removing the first stock price
        stockPrices.remove(0);

        System.out.println("Updated Stock Prices: " + stockPrices);
    }
}
```

**Output:**
```
Stock Prices: [150.5, 152.3, 148.7, 153.2]
Updated Stock Prices: [152.3, 148.7, 153.2]
```

---

## **Final Comparison: `ArrayList` vs `LinkedList` vs `Vector`**
| Feature | `ArrayList` | `LinkedList` | `Vector` |
|---------|------------|-------------|---------|
| **Best for** | Fast access | Fast insertion/deletion | Thread safety |
| **Indexing** | ✅ Fast (O(1)) | ❌ Slow (O(n)) | ✅ Fast (O(1)) |
| **Insertion** | ❌ Slow (O(n)) | ✅ Fast (O(1)) | ❌ Slow (O(n)) |
| **Thread-Safe** | ❌ No | ❌ No | ✅ Yes |
| **Performance** | ✅ Fast | ❌ Slower | ❌ Slower |

---

## **Conclusion**
- Use **`ArrayList`** for **fast access** and **random retrieval**.
- Use **`LinkedList`** for **frequent insertions and deletions**.
- Use **`Vector`** in **multi-threaded environments**.

# What is ArrayList?
`ArrayList` is a **resizable array implementation** of the `List` interface in Java. It is part of the **Java Collection Framework** and provides **dynamic resizing**, **random access**, and various utility methods.

---

## **Key Features of `ArrayList`**
✅ **Dynamic Resizing** (Automatically expands when full)  
✅ **Ordered Collection (Maintains insertion order)**  
✅ **Allows Duplicate Elements**  
✅ **Allows Null Elements**  
✅ **Index-Based Access (O(1) time complexity)**  
❌ **Slower Insert/Delete in the Middle (O(n) time complexity)**  
❌ **Not Thread-Safe (Use `Collections.synchronizedList()` for safety)**  

---

## **Hierarchy of `ArrayList`**
```
       Collection
           │
         List
           │
      ArrayList
```
- Implements **List Interface**  
- Uses **dynamic array internally**  

---

## **ArrayList Methods with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Appends an element to the end. | `list.add(10);` |
| **`add(int index, E e)`** | Inserts element at the specified index. | `list.add(1, 20);` |
| **`get(int index)`** | Retrieves element at index. | `int val = list.get(2);` |
| **`set(int index, E e)`** | Updates element at index. | `list.set(1, 30);` |
| **`remove(int index)`** | Removes element at index. | `list.remove(2);` |
| **`remove(Object o)`** | Removes first occurrence of element. | `list.remove(Integer.valueOf(10));` |
| **`contains(Object o)`** | Checks if element exists. | `list.contains(20);` |
| **`indexOf(Object o)`** | Returns index of first occurrence. | `int i = list.indexOf(30);` |
| **`lastIndexOf(Object o)`** | Returns index of last occurrence. | `int i = list.lastIndexOf(30);` |
| **`size()`** | Returns number of elements. | `int size = list.size();` |
| **`clear()`** | Removes all elements. | `list.clear();` |
| **`isEmpty()`** | Checks if list is empty. | `boolean b = list.isEmpty();` |

---

## **Example: Basic Usage of `ArrayList`**
```java
import java.util.ArrayList;

public class ArrayListExample {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        // Adding elements
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(1, 15); // Insert at index 1

        // Retrieving elements
        System.out.println("Element at index 2: " + list.get(2)); // Output: 20

        // Removing elements
        list.remove(1); // Removes element at index 1 (15)

        // Updating elements
        list.set(1, 25);

        System.out.println("Final List: " + list); // Output: [10, 25, 30]
    }
}
```

---

## **Time Complexity of `ArrayList` Operations**
| Operation | **Time Complexity** |
|-----------|----------------|
| **Insert at End (`add(e)`)** | O(1) (Amortized) |
| **Insert at Middle (`add(index, e)`)** | O(n) |
| **Remove at End (`remove(size-1)`)** | O(1) |
| **Remove at Middle (`remove(index)`)** | O(n) |
| **Get (`get(index)`)** | O(1) |
| **Set (`set(index, e)`)** | O(1) |
| **Search (`contains(e)`)** | O(n) |

---

## **Real-Time Implementations of `ArrayList`**

### **1. Employee Management System**
```java
import java.util.ArrayList;

class Employee {
    int id;
    String name;

    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{id=" + id + ", name='" + name + "'}";
    }
}

public class EmployeeManager {
    public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();

        // Adding employees
        employees.add(new Employee(101, "Alice"));
        employees.add(new Employee(102, "Bob"));
        employees.add(new Employee(103, "Charlie"));

        // Searching for an employee
        for (Employee e : employees) {
            if (e.id == 102) {
                System.out.println("Found: " + e);
            }
        }

        // Removing an employee
        employees.removeIf(e -> e.id == 103);

        System.out.println("Employee List: " + employees);
    }
}
```

**Output:**
```
Found: Employee{id=102, name='Bob'}
Employee List: [Employee{id=101, name='Alice'}, Employee{id=102, name='Bob'}]
```

---

### **2. Student Grades System**
```java
import java.util.ArrayList;

public class StudentGrades {
    public static void main(String[] args) {
        ArrayList<Integer> grades = new ArrayList<>();

        // Adding grades
        grades.add(85);
        grades.add(90);
        grades.add(78);
        grades.add(92);

        // Calculate Average
        int sum = 0;
        for (int grade : grades) {
            sum += grade;
        }
        double average = (double) sum / grades.size();
        System.out.println("Average Grade: " + average);
    }
}
```

**Output:**
```
Average Grade: 86.25
```

---

### **3. Shopping Cart System**
```java
import java.util.ArrayList;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " - $" + price;
    }
}

public class ShoppingCart {
    public static void main(String[] args) {
        ArrayList<Product> cart = new ArrayList<>();

        // Adding products
        cart.add(new Product("Laptop", 899.99));
        cart.add(new Product("Mouse", 25.99));
        cart.add(new Product("Keyboard", 49.99));

        // Calculating total price
        double total = 0;
        for (Product p : cart) {
            total += p.price;
        }

        System.out.println("Shopping Cart: " + cart);
        System.out.println("Total Price: $" + total);
    }
}
```

**Output:**
```
Shopping Cart: [Laptop - $899.99, Mouse - $25.99, Keyboard - $49.99]
Total Price: $975.97
```

---

## **Comparison: `ArrayList` vs `LinkedList` vs `Vector`**
| Feature | `ArrayList` | `LinkedList` | `Vector` |
|---------|------------|-------------|---------|
| **Best for** | Fast access | Fast insertion/deletion | Thread safety |
| **Indexing** | ✅ Fast (O(1)) | ❌ Slow (O(n)) | ✅ Fast (O(1)) |
| **Insertion** | ❌ Slow (O(n)) | ✅ Fast (O(1)) | ❌ Slow (O(n)) |
| **Thread-Safe** | ❌ No | ❌ No | ✅ Yes |
| **Performance** | ✅ Fast | ❌ Slower | ❌ Slower |

---

## **Conclusion**
- Use **`ArrayList`** for **fast access** and **random retrieval**.
- Avoid `ArrayList` when **frequent insertions & deletions** are required (use `LinkedList` instead).
- Use **`Vector`** only when **thread-safety is required**.

# What is LinkedList?
`LinkedList` is a **doubly linked list** implementation of the `List` and `Deque` interfaces in Java. It allows efficient insertions and deletions compared to `ArrayList`, but access time is slower due to **O(n) search complexity**.

---

## **Key Features of `LinkedList`**
✅ **Doubly Linked List Implementation** (Each node has a reference to previous & next nodes)  
✅ **Efficient Insertions & Deletions** (O(1) at both ends)  
✅ **Maintains Insertion Order**  
✅ **Allows Duplicate Elements & Null Values**  
✅ **Implements `Deque`, allowing Stack & Queue functionalities**  
❌ **Slower Random Access (`O(n)`)**  
❌ **Uses More Memory (`prev` & `next` references for each node)**  
❌ **Not Thread-Safe (Use `Collections.synchronizedList()` for safety)**  

---

## **Hierarchy of `LinkedList`**
```
        Collection
            │
          List
            │
       LinkedList
    ┌──────────────┐
 Queue             Deque
```
- Implements **List, Queue, and Deque interfaces**  
- Supports **FIFO (Queue), LIFO (Stack), and List functionalities**  

---

## **Methods of `LinkedList` with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Appends element at the end. | `list.add(10);` |
| **`add(int index, E e)`** | Inserts element at the specified index. | `list.add(1, 20);` |
| **`get(int index)`** | Retrieves element at index. | `int val = list.get(2);` |
| **`set(int index, E e)`** | Updates element at index. | `list.set(1, 30);` |
| **`remove(int index)`** | Removes element at index. | `list.remove(2);` |
| **`remove(Object o)`** | Removes first occurrence of element. | `list.remove(Integer.valueOf(10));` |
| **`contains(Object o)`** | Checks if element exists. | `list.contains(20);` |
| **`indexOf(Object o)`** | Returns index of first occurrence. | `int i = list.indexOf(30);` |
| **`lastIndexOf(Object o)`** | Returns index of last occurrence. | `int i = list.lastIndexOf(30);` |
| **`size()`** | Returns number of elements. | `int size = list.size();` |
| **`clear()`** | Removes all elements. | `list.clear();` |
| **`isEmpty()`** | Checks if list is empty. | `boolean b = list.isEmpty();` |

---

## **Example: Basic Usage of `LinkedList`**
```java
import java.util.LinkedList;

public class LinkedListExample {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();

        // Adding elements
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(1, 15); // Insert at index 1

        // Retrieving elements
        System.out.println("Element at index 2: " + list.get(2)); // Output: 20

        // Removing elements
        list.remove(1); // Removes element at index 1 (15)

        // Updating elements
        list.set(1, 25);

        System.out.println("Final List: " + list); // Output: [10, 25, 30]
    }
}
```

---

## **Time Complexity of `LinkedList` Operations**
| Operation | **Time Complexity** |
|-----------|----------------|
| **Insert at End (`add(e)`)** | O(1) |
| **Insert at Middle (`add(index, e)`)** | O(n) |
| **Remove at End (`remove(size-1)`)** | O(1) |
| **Remove at Middle (`remove(index)`)** | O(n) |
| **Get (`get(index)`)** | O(n) |
| **Set (`set(index, e)`)** | O(n) |
| **Search (`contains(e)`)** | O(n) |

---

## **Real-Time Implementations of `LinkedList`**

### **1. Train Management System (Doubly Linked List Example)**
```java
import java.util.LinkedList;

class Train {
    String name;

    public Train(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

public class TrainManagement {
    public static void main(String[] args) {
        LinkedList<Train> trains = new LinkedList<>();

        // Adding trains
        trains.addLast(new Train("Express 1"));
        trains.addLast(new Train("Express 2"));
        trains.addFirst(new Train("Local Train"));

        System.out.println("Train Schedule: " + trains);

        // Departing the first train
        trains.removeFirst();
        System.out.println("After Departure: " + trains);
    }
}
```

**Output:**
```
Train Schedule: [Local Train, Express 1, Express 2]
After Departure: [Express 1, Express 2]
```

---

### **2. Implementing a Browser History (Using `LinkedList`)**
```java
import java.util.LinkedList;

public class BrowserHistory {
    public static void main(String[] args) {
        LinkedList<String> history = new LinkedList<>();

        // Visiting pages
        history.add("Google");
        history.add("StackOverflow");
        history.add("GitHub");

        System.out.println("Browsing History: " + history);

        // Back navigation (remove last)
        history.removeLast();
        System.out.println("After going back: " + history);
    }
}
```

**Output:**
```
Browsing History: [Google, StackOverflow, GitHub]
After going back: [Google, StackOverflow]
```

---

### **3. Task Management System (Using `LinkedList` as a Queue)**
```java
import java.util.LinkedList;
import java.util.Queue;

class Task {
    String name;
    
    public Task(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

public class TaskManager {
    public static void main(String[] args) {
        Queue<Task> taskQueue = new LinkedList<>();

        // Adding tasks
        taskQueue.add(new Task("Fix Bug"));
        taskQueue.add(new Task("Code Review"));
        taskQueue.add(new Task("Develop Feature"));

        // Processing tasks
        while (!taskQueue.isEmpty()) {
            System.out.println("Processing: " + taskQueue.poll());
        }
    }
}
```

**Output:**
```
Processing: Fix Bug
Processing: Code Review
Processing: Develop Feature
```

---

## **Comparison: `LinkedList` vs `ArrayList` vs `Vector`**
| Feature | `LinkedList` | `ArrayList` | `Vector` |
|---------|-------------|-------------|---------|
| **Best for** | Frequent insertions/deletions | Fast access | Thread safety |
| **Indexing** | ❌ Slow (O(n)) | ✅ Fast (O(1)) | ✅ Fast (O(1)) |
| **Insertion** | ✅ Fast (O(1)) | ❌ Slow (O(n)) | ❌ Slow (O(n)) |
| **Thread-Safe** | ❌ No | ❌ No | ✅ Yes |
| **Performance** | ❌ Slower for Access | ✅ Faster | ❌ Slower |

---

## **Conclusion**
- Use **`LinkedList`** for **frequent insertions & deletions**.
- Use **`ArrayList`** if **random access speed is more important**.
- Avoid `LinkedList` if **you need fast `get()` operations**.

# What is the difference between ArrayList and LinkedList?
### **Difference Between `ArrayList` and `LinkedList` in Java**
Both `ArrayList` and `LinkedList` implement the `List` interface, but they have different **underlying data structures** and **performance characteristics**.

---

## **Key Differences Between `ArrayList` and `LinkedList`**
| Feature | **ArrayList** | **LinkedList** |
|---------|-------------|--------------|
| **Data Structure** | **Dynamic array** (Resizes dynamically) | **Doubly linked list** (Each node has `next` and `prev` references) |
| **Memory Usage** | Less memory (Only stores elements) | More memory (Stores elements + pointers to next and previous nodes) |
| **Insertion at End (`add(e)`)** | O(1) (Amortized) | O(1) |
| **Insertion at Middle (`add(index, e)`)** | O(n) (Shifts elements) | O(n) (Traverses to index) |
| **Insertion at Beginning (`add(0, e)`)** | O(n) (Shifts elements) | O(1) |
| **Removal from End (`remove(size-1)`)** | O(1) | O(1) |
| **Removal from Middle (`remove(index)`)** | O(n) | O(n) |
| **Removal from Beginning (`remove(0)`)** | O(n) (Shifts elements) | O(1) |
| **Search (`get(index)`)** | O(1) (Direct Access) | O(n) (Sequential Traversal) |
| **Iteration Performance** | Fast | Slower than `ArrayList` |
| **Best for** | Frequent access to elements | Frequent insertions & deletions |

---

## **When to Use What?**
| **Scenario** | **Use `ArrayList`** | **Use `LinkedList`** |
|-------------|------------------|------------------|
| **Frequent Insertions/Deletions at Beginning or Middle** | ❌ No | ✅ Yes |
| **Frequent Insertions at End** | ✅ Yes | ✅ Yes |
| **Fast Random Access (`get(index)`)** | ✅ Yes | ❌ No |
| **Memory Efficiency** | ✅ Yes (No extra pointers) | ❌ No (Extra pointers increase memory usage) |
| **Iterator Performance** | ✅ Fast | ❌ Slower |

---

## **Example: Performance Difference**
### **1. Insert 100,000 Elements at the Beginning**
```java
import java.util.*;

public class PerformanceTest {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();

        // Measure time for ArrayList
        long startTime = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            arrayList.add(0, i); // O(n)
        }
        long endTime = System.nanoTime();
        System.out.println("ArrayList Insertion Time: " + (endTime - startTime) / 1e6 + " ms");

        // Measure time for LinkedList
        startTime = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            linkedList.add(0, i); // O(1)
        }
        endTime = System.nanoTime();
        System.out.println("LinkedList Insertion Time: " + (endTime - startTime) / 1e6 + " ms");
    }
}
```
**Expected Output:**
```
ArrayList Insertion Time: 500+ ms
LinkedList Insertion Time: 5-10 ms
```
🔹 `LinkedList` is **much faster** for **insertions at the beginning**.

---

### **2. Access 100,000 Random Elements**
```java
import java.util.*;

public class AccessTest {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();
        
        // Fill both lists
        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
            linkedList.add(i);
        }

        // Measure time for ArrayList
        long startTime = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            int val = arrayList.get(i); // O(1)
        }
        long endTime = System.nanoTime();
        System.out.println("ArrayList Access Time: " + (endTime - startTime) / 1e6 + " ms");

        // Measure time for LinkedList
        startTime = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            int val = linkedList.get(i); // O(n)
        }
        endTime = System.nanoTime();
        System.out.println("LinkedList Access Time: " + (endTime - startTime) / 1e6 + " ms");
    }
}
```
**Expected Output:**
```
ArrayList Access Time: 5-10 ms
LinkedList Access Time: 200+ ms
```
🔹 `ArrayList` is **much faster** for **random access**.

---

## **Final Conclusion**
- **Use `ArrayList` when:**
  - **Frequent random access (`get(index)`)** is needed.
  - **Memory efficiency** is required.
  - **Insertions/Deletions happen mostly at the end.**

- **Use `LinkedList` when:**
  - **Frequent insertions/deletions at the beginning/middle**.
  - **You don't need fast random access.**
  - **Memory is not a concern.**

Would you like a **GUI-based Contact Manager using `ArrayList`?** 😊

# What is Vector?
`Vector` is a **dynamic array** implementation of the `List` interface, similar to `ArrayList`, but with one key difference: **it is synchronized** (thread-safe). It is part of the **Java Collection Framework** and is used in multi-threaded environments where multiple threads need to access the list safely.

---

## **Key Features of `Vector`**
✅ **Thread-Safe (Synchronized Methods)**  
✅ **Maintains Insertion Order**  
✅ **Allows Duplicate Elements & Null Values**  
✅ **Supports Random Access (Fast `get(index)`)**  
✅ **Dynamic Resizing (Doubles in Size When Full)**  
❌ **Slower Performance (Due to Synchronization Overhead)**  
❌ **Uses More Memory (Capacity Grows in Large Chunks)**  
❌ **Better Alternatives (`ArrayList` is Preferred for Non-Threaded Use)**  

---

## **Hierarchy of `Vector`**
```
       Collection
           │
         List
           │
        Vector
           │
        Stack
```
- Implements **List Interface**  
- Extends **AbstractList Class**  
- **Stack** extends `Vector` (LIFO functionality)  

---

## **Methods of `Vector` with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Adds element at the end. | `vector.add(10);` |
| **`add(int index, E e)`** | Inserts element at the specified index. | `vector.add(1, 20);` |
| **`get(int index)`** | Retrieves element at index. | `int val = vector.get(2);` |
| **`set(int index, E e)`** | Updates element at index. | `vector.set(1, 30);` |
| **`remove(int index)`** | Removes element at index. | `vector.remove(2);` |
| **`remove(Object o)`** | Removes first occurrence of element. | `vector.remove(Integer.valueOf(10));` |
| **`contains(Object o)`** | Checks if element exists. | `vector.contains(20);` |
| **`indexOf(Object o)`** | Returns index of first occurrence. | `int i = vector.indexOf(30);` |
| **`lastIndexOf(Object o)`** | Returns index of last occurrence. | `int i = vector.lastIndexOf(30);` |
| **`size()`** | Returns number of elements. | `int size = vector.size();` |
| **`clear()`** | Removes all elements. | `vector.clear();` |
| **`isEmpty()`** | Checks if list is empty. | `boolean b = vector.isEmpty();` |
| **`firstElement()`** | Retrieves the first element. | `int first = vector.firstElement();` |
| **`lastElement()`** | Retrieves the last element. | `int last = vector.lastElement();` |

---

## **Example: Basic Usage of `Vector`**
```java
import java.util.Vector;

public class VectorExample {
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();

        // Adding elements
        vector.add(10);
        vector.add(20);
        vector.add(30);
        vector.add(1, 15); // Insert at index 1

        // Retrieving elements
        System.out.println("Element at index 2: " + vector.get(2)); // Output: 20

        // Removing elements
        vector.remove(1); // Removes element at index 1 (15)

        // Updating elements
        vector.set(1, 25);

        System.out.println("Final Vector: " + vector); // Output: [10, 25, 30]
    }
}
```

---

## **Time Complexity of `Vector` Operations**
| Operation | **Time Complexity** |
|-----------|----------------|
| **Insert at End (`add(e)`)** | O(1) (Amortized) |
| **Insert at Middle (`add(index, e)`)** | O(n) |
| **Remove at End (`remove(size-1)`)** | O(1) |
| **Remove at Middle (`remove(index)`)** | O(n) |
| **Get (`get(index)`)** | O(1) |
| **Set (`set(index, e)`)** | O(1) |
| **Search (`contains(e)`)** | O(n) |

---

## **Real-Time Implementations of `Vector`**

### **1. Multi-Threaded Stock Price Tracker**
```java
import java.util.Vector;

class StockPriceTracker {
    private Vector<Double> prices = new Vector<>();

    public synchronized void addPrice(double price) {
        prices.add(price);
        System.out.println("New Price Added: " + price);
    }

    public synchronized void printPrices() {
        System.out.println("Stock Prices: " + prices);
    }
}

public class StockTrackerApp {
    public static void main(String[] args) {
        StockPriceTracker tracker = new StockPriceTracker();

        // Simulating multiple threads updating stock prices
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                tracker.addPrice(100 + Math.random() * 50);
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                tracker.addPrice(200 + Math.random() * 50);
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        tracker.printPrices();
    }
}
```

---

### **2. Thread-Safe Chat Message Storage**
```java
import java.util.Vector;

public class ChatApplication {
    private static Vector<String> messages = new Vector<>();

    public static void addMessage(String message) {
        messages.add(message);
    }

    public static void displayMessages() {
        System.out.println("Chat Messages: " + messages);
    }

    public static void main(String[] args) {
        // Adding messages from multiple users
        Thread user1 = new Thread(() -> {
            addMessage("User1: Hello!");
            addMessage("User1: How are you?");
        });

        Thread user2 = new Thread(() -> {
            addMessage("User2: Hi!");
            addMessage("User2: I'm good, you?");
        });

        user1.start();
        user2.start();

        try {
            user1.join();
            user2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Display messages
        displayMessages();
    }
}
```

---

## **Comparison: `Vector` vs `ArrayList` vs `LinkedList`**
| Feature | **Vector** | **ArrayList** | **LinkedList** |
|---------|-----------|-------------|-------------|
| **Thread-Safe** | ✅ Yes | ❌ No | ❌ No |
| **Performance** | ❌ Slower (Synchronization Overhead) | ✅ Faster | ❌ Slower |
| **Insertion at End (`add()`)** | ✅ Fast O(1) | ✅ Fast O(1) | ✅ Fast O(1) |
| **Insertion at Middle (`add(index, e)`)** | ❌ Slow O(n) | ❌ Slow O(n) | ❌ Slow O(n) |
| **Deletion at Middle (`remove(index)`)** | ❌ Slow O(n) | ❌ Slow O(n) | ❌ Slow O(n) |
| **Random Access (`get(index)`)** | ✅ Fast O(1) | ✅ Fast O(1) | ❌ Slow O(n) |
| **Best Use Case** | **Multi-Threaded Applications** | **General-Purpose Lists** | **Frequent Insertions & Deletions** |

---

## **Conclusion**
- **Use `Vector` when multiple threads need synchronized access**.
- **Avoid `Vector` if you don't need thread-safety (use `ArrayList` instead)**.
- **Use `LinkedList` only if frequent insertions & deletions are needed**.


# What is Stack?
A **Stack** is a **Last-In-First-Out (LIFO)** data structure where elements are added and removed from the **top**. In Java, `Stack` is implemented as a subclass of `Vector` in the **Java Collection Framework**.

---

## **Key Features of `Stack`**
✅ **LIFO (Last-In-First-Out) Order**  
✅ **Thread-Safe (Inherited from `Vector`)**  
✅ **Allows `null` Elements**  
✅ **Dynamic Resizing**  
❌ **Slower Performance (Synchronization Overhead from `Vector`)**  
❌ **Not the Best Choice for Large Data Sets (Use `ArrayDeque` Instead)**  

---

## **Hierarchy of `Stack`**
```
       Collection
           │
         List
           │
        Vector
           │
        Stack
```
- **Extends `Vector`** (inherits thread-safety)  
- **Implements `LIFO` structure**  

---

## **Methods of `Stack` with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`push(E e)`** | Inserts an element at the top. | `stack.push(10);` |
| **`pop()`** | Removes and returns the top element, throws exception if empty. | `int top = stack.pop();` |
| **`peek()`** | Retrieves the top element without removing it. | `int top = stack.peek();` |
| **`empty()`** | Checks if stack is empty. | `boolean isEmpty = stack.empty();` |
| **`search(Object o)`** | Returns the position (1-based) of element from top; returns `-1` if not found. | `int pos = stack.search(20);` |

---

## **Example: Basic Usage of `Stack`**
```java
import java.util.Stack;

public class StackExample {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();

        // Pushing elements onto the stack
        stack.push(10);
        stack.push(20);
        stack.push(30);

        // Peek at the top element
        System.out.println("Top Element: " + stack.peek()); // Output: 30

        // Popping elements
        System.out.println("Popped: " + stack.pop()); // Output: 30

        // Checking if the stack is empty
        System.out.println("Is Stack Empty? " + stack.empty()); // Output: false

        // Searching for an element
        System.out.println("Position of 10: " + stack.search(10)); // Output: 2
    }
}
```

---

## **Time Complexity of `Stack` Operations**
| Operation | **Time Complexity** |
|-----------|----------------|
| **Push (`push(e)`)** | O(1) |
| **Pop (`pop()`)** | O(1) |
| **Peek (`peek()`)** | O(1) |
| **Search (`search(e)`)** | O(n) |

---

## **Real-Time Implementations of `Stack`**

### **1. Balancing Parentheses (`Stack` for Expression Validation)**
```java
import java.util.Stack;

public class ParenthesesValidator {
    public static boolean isValid(String expression) {
        Stack<Character> stack = new Stack<>();

        for (char c : expression.toCharArray()) {
            if (c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else if (c == ')' && !stack.isEmpty() && stack.peek() == '(') {
                stack.pop();
            } else if (c == '}' && !stack.isEmpty() && stack.peek() == '{') {
                stack.pop();
            } else if (c == ']' && !stack.isEmpty() && stack.peek() == '[') {
                stack.pop();
            } else {
                return false;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        String expression = "{[()]}";
        System.out.println("Is Valid? " + isValid(expression)); // Output: true
    }
}
```

---

### **2. Browser Back-Button Functionality**
```java
import java.util.Stack;

class BrowserHistory {
    private Stack<String> history = new Stack<>();

    public void visit(String url) {
        history.push(url);
        System.out.println("Visited: " + url);
    }

    public void back() {
        if (!history.isEmpty()) {
            System.out.println("Going Back from: " + history.pop());
        } else {
            System.out.println("No previous pages.");
        }
    }
}

public class Browser {
    public static void main(String[] args) {
        BrowserHistory browser = new BrowserHistory();

        browser.visit("Google.com");
        browser.visit("StackOverflow.com");
        browser.visit("GitHub.com");

        browser.back(); // Output: Going Back from GitHub.com
        browser.back(); // Output: Going Back from StackOverflow.com
    }
}
```

---

### **3. Undo-Redo Functionality**
```java
import java.util.Stack;

class TextEditor {
    private Stack<String> undoStack = new Stack<>();
    private Stack<String> redoStack = new Stack<>();

    public void write(String text) {
        undoStack.push(text);
        redoStack.clear();
        System.out.println("Written: " + text);
    }

    public void undo() {
        if (!undoStack.isEmpty()) {
            redoStack.push(undoStack.pop());
            System.out.println("Undo Action");
        } else {
            System.out.println("Nothing to undo");
        }
    }

    public void redo() {
        if (!redoStack.isEmpty()) {
            undoStack.push(redoStack.pop());
            System.out.println("Redo Action");
        } else {
            System.out.println("Nothing to redo");
        }
    }
}

public class TextEditorApp {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();

        editor.write("Hello");
        editor.write("World");
        
        editor.undo(); // Undo "World"
        editor.redo(); // Redo "World"
    }
}
```

---

## **Comparison: `Stack` vs `ArrayDeque` (Recommended Alternative)**
| Feature | **Stack** | **ArrayDeque (Better Alternative)** |
|---------|---------|--------------------------|
| **Performance** | Slower (`Vector` overhead) | ✅ Faster |
| **Thread-Safe** | ✅ Yes (Synchronized) | ❌ No (Use `Collections.synchronizedDeque()`) |
| **LIFO Support** | ✅ Yes | ✅ Yes |
| **Preferred For** | Legacy Code | ✅ New Applications |

🔹 **Use `ArrayDeque` instead of `Stack` for better performance**.

---

## **Conclusion**
- Use **`Stack`** for **LIFO-based tasks** like **Undo-Redo, Back Navigation, Expression Parsing**.
- Avoid `Stack` for performance reasons (use `ArrayDeque` instead).
- **Choose `ArrayDeque` over `Stack` unless you need thread safety**.


# What is the difference between CopyOnWriteArrayList and CopyOnWriteArraySet?
Both `CopyOnWriteArrayList` and `CopyOnWriteArraySet` are part of the **java.util.concurrent** package and provide thread-safe collections. They **create a new copy of the underlying array whenever a modification is made**, ensuring thread safety **without using locks**.

---

## **Key Differences**
| Feature | **CopyOnWriteArrayList** | **CopyOnWriteArraySet** |
|---------|-----------------|------------------|
| **Implements** | `List` Interface | `Set` Interface |
| **Allows Duplicates?** | ✅ Yes | ❌ No |
| **Maintains Insertion Order?** | ✅ Yes | ❌ No (Unordered) |
| **Thread-Safe?** | ✅ Yes (Lock-Free) | ✅ Yes (Lock-Free) |
| **Iteration** | Uses snapshot of array, does not reflect changes made after iteration starts | Uses snapshot of array, does not reflect changes made after iteration starts |
| **Best For** | Read-heavy applications requiring **ordered elements with duplicates** | Read-heavy applications requiring **unique elements only** |

---

## **Hierarchy of `CopyOnWriteArrayList` and `CopyOnWriteArraySet`**
```
        Collection
           │
        ┌──┴───┐
      List    Set
       │        │
CopyOnWriteArrayList  CopyOnWriteArraySet
```
- **`CopyOnWriteArrayList`**: Implements **List**, allows duplicates, maintains insertion order.
- **`CopyOnWriteArraySet`**: Implements **Set**, **does not allow duplicates**, does not maintain insertion order.

---

## **Methods of `CopyOnWriteArrayList`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Adds an element at the end. | `list.add(10);` |
| **`add(int index, E e)`** | Inserts an element at the specified index. | `list.add(1, 20);` |
| **`get(int index)`** | Retrieves element at index. | `int val = list.get(2);` |
| **`set(int index, E e)`** | Updates element at index. | `list.set(1, 30);` |
| **`remove(int index)`** | Removes element at index. | `list.remove(2);` |
| **`contains(Object o)`** | Checks if element exists. | `list.contains(20);` |
| **`size()`** | Returns the number of elements. | `list.size();` |
| **`iterator()`** | Returns an iterator (snapshot-based). | `Iterator<Integer> it = list.iterator();` |

---

## **Methods of `CopyOnWriteArraySet`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Adds an element if not already present. | `set.add(10);` |
| **`remove(Object o)`** | Removes the specified element. | `set.remove(10);` |
| **`contains(Object o)`** | Checks if element exists. | `set.contains(20);` |
| **`size()`** | Returns the number of elements. | `set.size();` |
| **`iterator()`** | Returns an iterator (snapshot-based). | `Iterator<Integer> it = set.iterator();` |

---

## **Example: `CopyOnWriteArrayList` (Thread-Safe List)**
```java
import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListExample {
    public static void main(String[] args) {
        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();

        // Adding elements
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(10); // Duplicates allowed

        System.out.println("Initial List: " + list);

        // Iterating
        for (Integer num : list) {
            System.out.println("Reading: " + num);
        }

        // Modifications do not affect iterators
        list.add(40);
        System.out.println("After Modification: " + list);
    }
}
```
**Output:**
```
Initial List: [10, 20, 30, 10]
Reading: 10
Reading: 20
Reading: 30
Reading: 10
After Modification: [10, 20, 30, 10, 40]
```
🔹 **Safe for iteration**, but modifications require copying the array.

---

## **Example: `CopyOnWriteArraySet` (Thread-Safe Set)**
```java
import java.util.concurrent.CopyOnWriteArraySet;

public class CopyOnWriteArraySetExample {
    public static void main(String[] args) {
        CopyOnWriteArraySet<Integer> set = new CopyOnWriteArraySet<>();

        // Adding elements
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(10); // Duplicates are NOT added

        System.out.println("Initial Set: " + set);

        // Iterating
        for (Integer num : set) {
            System.out.println("Reading: " + num);
        }

        // Modifications do not affect iterators
        set.add(40);
        System.out.println("After Modification: " + set);
    }
}
```
**Output:**
```
Initial Set: [10, 20, 30]
Reading: 10
Reading: 20
Reading: 30
After Modification: [10, 20, 30, 40]
```
🔹 **Ensures uniqueness**, but modifications require copying the array.

---

## **Time Complexity Comparison**
| Operation | **CopyOnWriteArrayList** | **CopyOnWriteArraySet** |
|-----------|----------------|----------------|
| **Insertion (`add(e)`)** | O(n) (Due to Copying) | O(n) (Checks for Duplicates) |
| **Deletion (`remove(e)`)** | O(n) (Copying Required) | O(n) (Copying Required) |
| **Search (`contains(e)`)** | O(n) | O(n) |
| **Iteration (`iterator()`)** | O(n) | O(n) |

🔹 **Both have the same complexity, but `CopyOnWriteArraySet` prevents duplicate entries.**

---

## **When to Use What?**
| **Scenario** | **Use `CopyOnWriteArrayList`** | **Use `CopyOnWriteArraySet`** |
|-------------|------------------|------------------|
| **Need Ordered Elements?** | ✅ Yes | ❌ No |
| **Allow Duplicates?** | ✅ Yes | ❌ No |
| **High Read Operations?** | ✅ Yes | ✅ Yes |
| **High Write Operations?** | ❌ No (Expensive Copies) | ❌ No (Expensive Copies) |

🔹 **Use `CopyOnWriteArrayList` for thread-safe ordered lists with duplicates.**  
🔹 **Use `CopyOnWriteArraySet` for thread-safe unique element storage.**

---

## **Real-World Use Cases**
### **1. Logging System (Using `CopyOnWriteArrayList`)**
```java
import java.util.concurrent.CopyOnWriteArrayList;

public class Logger {
    private static CopyOnWriteArrayList<String> logs = new CopyOnWriteArrayList<>();

    public static void log(String message) {
        logs.add(message);
    }

    public static void printLogs() {
        for (String log : logs) {
            System.out.println("Log: " + log);
        }
    }

    public static void main(String[] args) {
        log("Error 404");
        log("User Logged In");
        printLogs();
    }
}
```
🔹 **Ensures thread-safe logging while keeping old logs available.**

---

### **2. Unique User Sessions (Using `CopyOnWriteArraySet`)**
```java
import java.util.concurrent.CopyOnWriteArraySet;

public class UserSessionManager {
    private static CopyOnWriteArraySet<String> activeUsers = new CopyOnWriteArraySet<>();

    public static void addUser(String user) {
        activeUsers.add(user);
    }

    public static void removeUser(String user) {
        activeUsers.remove(user);
    }

    public static void printActiveUsers() {
        System.out.println("Active Users: " + activeUsers);
    }

    public static void main(String[] args) {
        addUser("Alice");
        addUser("Bob");
        addUser("Alice"); // Will not be added again
        printActiveUsers();
    }
}
```
🔹 **Ensures unique active user sessions.**

---

## **Final Thoughts**
- **`CopyOnWriteArrayList`** is **best for ordered, duplicate-allowing collections**.
- **`CopyOnWriteArraySet`** is **best for unique elements**.
- **Both are inefficient for frequent modifications** (Use `ConcurrentHashMap` if needed).

Here’s a **real-time implementation** of both **`CopyOnWriteArrayList` and `CopyOnWriteArraySet`** in practical use cases.

---

## **1. Real-Time Chat Application (Using `CopyOnWriteArrayList`)**
A **thread-safe chat room** where multiple users can send messages **without worrying about concurrent modifications**.

```java
import java.util.concurrent.CopyOnWriteArrayList;

class ChatRoom {
    private static CopyOnWriteArrayList<String> messages = new CopyOnWriteArrayList<>();

    // Method to send a message
    public static void sendMessage(String user, String message) {
        messages.add(user + ": " + message);
    }

    // Method to display chat history
    public static void displayChatHistory() {
        System.out.println("Chat History:");
        for (String msg : messages) {
            System.out.println(msg);
        }
    }
}

public class ChatApplication {
    public static void main(String[] args) {
        // Simulating multiple users sending messages
        Thread user1 = new Thread(() -> {
            ChatRoom.sendMessage("Alice", "Hello, everyone!");
            ChatRoom.sendMessage("Alice", "How's it going?");
        });

        Thread user2 = new Thread(() -> {
            ChatRoom.sendMessage("Bob", "Hi Alice!");
            ChatRoom.sendMessage("Bob", "I'm doing great, thanks!");
        });

        user1.start();
        user2.start();

        try {
            user1.join();
            user2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Display chat history
        ChatRoom.displayChatHistory();
    }
}
```

### **Output**
```
Chat History:
Alice: Hello, everyone!
Alice: How's it going?
Bob: Hi Alice!
Bob: I'm doing great, thanks!
```
🔹 **Why `CopyOnWriteArrayList`?**  
- **Multiple users can send messages concurrently without synchronization issues.**
- **Snapshot-based iteration ensures that older messages remain accessible while new ones are added.**

---

## **2. Unique Online Users Tracker (Using `CopyOnWriteArraySet`)**
A **real-time system** for tracking unique users who have logged into an application.

```java
import java.util.concurrent.CopyOnWriteArraySet;

class OnlineUsers {
    private static CopyOnWriteArraySet<String> activeUsers = new CopyOnWriteArraySet<>();

    // Method to add a user
    public static void loginUser(String username) {
        activeUsers.add(username);
        System.out.println(username + " has logged in.");
    }

    // Method to remove a user
    public static void logoutUser(String username) {
        activeUsers.remove(username);
        System.out.println(username + " has logged out.");
    }

    // Display currently active users
    public static void displayActiveUsers() {
        System.out.println("Active Users: " + activeUsers);
    }
}

public class UserSessionManager {
    public static void main(String[] args) {
        // Simulating multiple users logging in
        Thread user1 = new Thread(() -> {
            OnlineUsers.loginUser("Alice");
            OnlineUsers.loginUser("Bob");
        });

        Thread user2 = new Thread(() -> {
            OnlineUsers.loginUser("Charlie");
            OnlineUsers.loginUser("Alice"); // Duplicate login attempt
        });

        user1.start();
        user2.start();

        try {
            user1.join();
            user2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Display active users
        OnlineUsers.displayActiveUsers();

        // Simulating user logout
        OnlineUsers.logoutUser("Alice");
        OnlineUsers.displayActiveUsers();
    }
}
```

### **Output**
```
Alice has logged in.
Bob has logged in.
Charlie has logged in.
Alice has logged in. (Duplicate, so not added again)
Active Users: [Alice, Bob, Charlie]
Alice has logged out.
Active Users: [Bob, Charlie]
```
🔹 **Why `CopyOnWriteArraySet`?**  
- **Ensures only unique users remain in the active session list.**
- **Thread-safe without explicit synchronization, making it ideal for concurrent user sessions.**

---

## **3. Notification System (Using `CopyOnWriteArrayList`)**
A **real-time notification system** where multiple users receive notifications.

```java
import java.util.concurrent.CopyOnWriteArrayList;

class NotificationSystem {
    private static CopyOnWriteArrayList<String> notifications = new CopyOnWriteArrayList<>();

    public static void addNotification(String notification) {
        notifications.add(notification);
        System.out.println("New Notification: " + notification);
    }

    public static void displayNotifications() {
        System.out.println("All Notifications:");
        for (String note : notifications) {
            System.out.println(note);
        }
    }
}

public class NotificationApp {
    public static void main(String[] args) {
        // Multiple users generating notifications
        Thread user1 = new Thread(() -> {
            NotificationSystem.addNotification("User Alice liked your post.");
        });

        Thread user2 = new Thread(() -> {
            NotificationSystem.addNotification("User Bob commented on your photo.");
        });

        user1.start();
        user2.start();

        try {
            user1.join();
            user2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Displaying all notifications
        NotificationSystem.displayNotifications();
    }
}
```

### **Output**
```
New Notification: User Alice liked your post.
New Notification: User Bob commented on your photo.
All Notifications:
User Alice liked your post.
User Bob commented on your photo.
```
🔹 **Why `CopyOnWriteArrayList`?**  
- **Ensures safe modifications while multiple threads add notifications.**
- **No `ConcurrentModificationException` while iterating.**

---

## **4. Unique Chat Room Participants (Using `CopyOnWriteArraySet`)**
Ensures **only unique participants** in a group chat.

```java
import java.util.concurrent.CopyOnWriteArraySet;

class ChatRoomParticipants {
    private static CopyOnWriteArraySet<String> participants = new CopyOnWriteArraySet<>();

    public static void joinChat(String user) {
        if (participants.add(user)) {
            System.out.println(user + " joined the chat.");
        } else {
            System.out.println(user + " is already in the chat.");
        }
    }

    public static void leaveChat(String user) {
        participants.remove(user);
        System.out.println(user + " left the chat.");
    }

    public static void displayParticipants() {
        System.out.println("Chat Participants: " + participants);
    }
}

public class ChatRoomManager {
    public static void main(String[] args) {
        // Multiple users joining chat
        Thread user1 = new Thread(() -> {
            ChatRoomParticipants.joinChat("Alice");
            ChatRoomParticipants.joinChat("Bob");
        });

        Thread user2 = new Thread(() -> {
            ChatRoomParticipants.joinChat("Charlie");
            ChatRoomParticipants.joinChat("Alice"); // Duplicate attempt
        });

        user1.start();
        user2.start();

        try {
            user1.join();
            user2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Displaying participants
        ChatRoomParticipants.displayParticipants();

        // Removing a user
        ChatRoomParticipants.leaveChat("Alice");
        ChatRoomParticipants.displayParticipants();
    }
}
```

### **Output**
```
Alice joined the chat.
Bob joined the chat.
Charlie joined the chat.
Alice is already in the chat.
Chat Participants: [Alice, Bob, Charlie]
Alice left the chat.
Chat Participants: [Bob, Charlie]
```
🔹 **Why `CopyOnWriteArraySet`?**  
- **Ensures each participant is unique** (no duplicates).  
- **Safe for concurrent modifications.**  

---

## **Final Summary**
| **Use Case** | **Use `CopyOnWriteArrayList`** | **Use `CopyOnWriteArraySet`** |
|-------------|------------------|------------------|
| **Chat Messages** | ✅ Yes | ❌ No |
| **Unique Active Users** | ❌ No | ✅ Yes |
| **Notification System** | ✅ Yes | ❌ No |
| **Chat Room Participants** | ❌ No | ✅ Yes |


# What is Set?
A **Set** in Java is a **collection that does not allow duplicate elements**. It is part of the **Java Collection Framework** and is commonly used for **ensuring uniqueness** in stored data.

---

## **Key Features of `Set`**
✅ **No Duplicate Elements**  
✅ **Allows `null` (Only one null entry allowed)**  
✅ **Unordered (`HashSet`), Ordered (`LinkedHashSet`), Sorted (`TreeSet`)**  
✅ **Efficient Searching (`HashSet` O(1), `TreeSet` O(log n))**  
❌ **No Direct Index Access (Unlike `List`)**  
❌ **Slower Iteration Compared to Lists**  

---

## **Hierarchy of `Set`**
```
       Collection
           │
          Set
     ┌────┴────┐
  HashSet  SortedSet
               │
            TreeSet
```

### **Types of `Set` in Java**
1. **`HashSet`** (Unordered, O(1) operations)
2. **`LinkedHashSet`** (Maintains insertion order, O(1) operations)
3. **`TreeSet`** (Sorted elements, O(log n) operations)

---

## **Methods of `Set` with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Adds an element (if not already present). | `set.add(10);` |
| **`remove(Object o)`** | Removes the specified element. | `set.remove(10);` |
| **`contains(Object o)`** | Checks if an element exists. | `set.contains(20);` |
| **`size()`** | Returns the number of elements. | `set.size();` |
| **`clear()`** | Removes all elements. | `set.clear();` |
| **`isEmpty()`** | Checks if the set is empty. | `boolean b = set.isEmpty();` |
| **`iterator()`** | Returns an iterator over the elements. | `Iterator<Integer> it = set.iterator();` |

---

## **Example: Basic Usage of `HashSet`**
```java
import java.util.HashSet;

public class HashSetExample {
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();

        // Adding elements
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(10); // Duplicate, will not be added

        System.out.println("HashSet: " + set);

        // Checking for an element
        System.out.println("Contains 20? " + set.contains(20));

        // Removing an element
        set.remove(10);

        System.out.println("After Removal: " + set);
    }
}
```
**Output:**
```
HashSet: [10, 20, 30]
Contains 20? true
After Removal: [20, 30]
```
🔹 **Ensures uniqueness, unordered elements, fast operations.**

---

## **Time Complexity of `Set` Implementations**
| Operation | **HashSet (O(1))** | **LinkedHashSet (O(1))** | **TreeSet (O(log n))** |
|-----------|------------------|------------------|------------------|
| **Insertion (`add(e)`)** | O(1) | O(1) | O(log n) |
| **Deletion (`remove(e)`)** | O(1) | O(1) | O(log n) |
| **Search (`contains(e)`)** | O(1) | O(1) | O(log n) |
| **Iteration** | O(n) (Unordered) | O(n) (Maintains Order) | O(n) (Sorted Order) |

---

## **Real-Time Implementations of `Set`**

### **1. Removing Duplicate Emails (`HashSet`)**
```java
import java.util.HashSet;

public class UniqueEmails {
    public static void main(String[] args) {
        HashSet<String> emails = new HashSet<>();

        emails.add("alice@example.com");
        emails.add("bob@example.com");
        emails.add("alice@example.com"); // Duplicate

        System.out.println("Unique Emails: " + emails);
    }
}
```
**Output:**
```
Unique Emails: [alice@example.com, bob@example.com]
```
🔹 **Ensures each email is unique.**

---

### **2. Maintaining Ordered Usernames (`LinkedHashSet`)**
```java
import java.util.LinkedHashSet;

public class OrderedUsernames {
    public static void main(String[] args) {
        LinkedHashSet<String> usernames = new LinkedHashSet<>();

        usernames.add("Alice");
        usernames.add("Bob");
        usernames.add("Charlie");

        System.out.println("Usernames in Order: " + usernames);
    }
}
```
**Output:**
```
Usernames in Order: [Alice, Bob, Charlie]
```
🔹 **Maintains insertion order.**

---

### **3. Storing Unique Sorted Product Codes (`TreeSet`)**
```java
import java.util.TreeSet;

public class SortedProducts {
    public static void main(String[] args) {
        TreeSet<Integer> productCodes = new TreeSet<>();

        productCodes.add(105);
        productCodes.add(101);
        productCodes.add(103);

        System.out.println("Sorted Product Codes: " + productCodes);
    }
}
```
**Output:**
```
Sorted Product Codes: [101, 103, 105]
```
🔹 **Automatically sorted, useful for range queries.**

---

### **4. Fast Duplicate Detection in a Streaming Service (`HashSet`)**
```java
import java.util.HashSet;

public class DuplicateDetector {
    public static void main(String[] args) {
        HashSet<String> watchedMovies = new HashSet<>();

        String[] movies = {"Inception", "Interstellar", "Inception", "Tenet"};

        for (String movie : movies) {
            if (!watchedMovies.add(movie)) {
                System.out.println("Duplicate Movie Found: " + movie);
            }
        }
    }
}
```
**Output:**
```
Duplicate Movie Found: Inception
```
🔹 **Detects duplicate movie views instantly.**

---

### **5. Real-Time Online User Tracking (`LinkedHashSet`)**
```java
import java.util.LinkedHashSet;

public class OnlineUsers {
    private static LinkedHashSet<String> activeUsers = new LinkedHashSet<>();

    public static void userLogin(String username) {
        activeUsers.add(username);
        System.out.println(username + " logged in.");
    }

    public static void userLogout(String username) {
        activeUsers.remove(username);
        System.out.println(username + " logged out.");
    }

    public static void displayActiveUsers() {
        System.out.println("Active Users: " + activeUsers);
    }

    public static void main(String[] args) {
        userLogin("Alice");
        userLogin("Bob");
        userLogin("Charlie");

        displayActiveUsers();

        userLogout("Alice");
        displayActiveUsers();
    }
}
```
**Output:**
```
Alice logged in.
Bob logged in.
Charlie logged in.
Active Users: [Alice, Bob, Charlie]
Alice logged out.
Active Users: [Bob, Charlie]
```
🔹 **Keeps track of active users efficiently.**

---

## **Final Summary**
| **Use Case** | **Use `HashSet`** | **Use `LinkedHashSet`** | **Use `TreeSet`** |
|-------------|------------------|------------------|------------------|
| **Fast Lookup** | ✅ Yes (O(1)) | ✅ Yes (O(1)) | ❌ No (O(log n)) |
| **Preserve Order** | ❌ No | ✅ Yes | ✅ Sorted |
| **Sorting Required?** | ❌ No | ❌ No | ✅ Yes |
| **Best For** | Unique fast lookups | Ordered unique data | Sorted unique data |

---
## **Conclusion**
- **Use `HashSet` when speed is required, and order doesn’t matter.**
- **Use `LinkedHashSet` when insertion order should be preserved.**
- **Use `TreeSet` when sorting is needed.**

## **Difference Between `List` and `Set` in Java**

Both **`List` and `Set`** are part of the **Java Collection Framework**, but they serve different purposes.

---

## **Key Differences Between `List` and `Set`**
| Feature | **List** | **Set** |
|---------|---------|---------|
| **Definition** | An ordered collection that allows duplicates. | A collection that does not allow duplicate elements. |
| **Duplicates** | ✅ Yes, allows duplicates. | ❌ No, does not allow duplicates. |
| **Order** | ✅ Maintains insertion order (except for `ArrayList`). | ❌ Does not maintain insertion order (`HashSet`), but `LinkedHashSet` does. |
| **Index-Based Access** | ✅ Yes, elements can be accessed via `get(index)`. | ❌ No, elements cannot be accessed via an index. |
| **Sorting** | ❌ Not sorted (unless explicitly sorted using `Collections.sort()`). | ✅ `TreeSet` stores elements in **sorted** order. |
| **Performance** | Slower for searching (`O(n)`) | Faster searching (`O(1)` in `HashSet`, `O(log n)` in `TreeSet`) |
| **Implementation Classes** | `ArrayList`, `LinkedList`, `Vector`, `Stack` | `HashSet`, `LinkedHashSet`, `TreeSet` |
| **When to Use?** | When order and duplicates matter. | When uniqueness of elements is required. |

---

## **Tabular Comparison of `List` vs `Set` with Operations**
| **Operation** | **List (`ArrayList`, `LinkedList`)** | **Set (`HashSet`, `TreeSet`, `LinkedHashSet`)** |
|--------------|---------------------------------|---------------------------------|
| **Insertion (`add(e)`)** | ✅ `O(1)` in `ArrayList` (Amortized) <br> ❌ `O(n)` for `LinkedList` (Traverses to index) | ✅ `O(1)` in `HashSet` <br> ✅ `O(log n)` in `TreeSet` (Sorted) |
| **Deletion (`remove(e)`)** | ❌ `O(n)` in `ArrayList` (Shifting required) <br> ✅ `O(1)` in `LinkedList` (Fast for first/last) | ✅ `O(1)` in `HashSet` <br> ✅ `O(log n)` in `TreeSet` |
| **Search (`contains(e)`)** | ❌ `O(n)` (Linear Search) | ✅ `O(1)` in `HashSet` <br> ✅ `O(log n)` in `TreeSet` |
| **Access (`get(index)`)** | ✅ `O(1)` for `ArrayList` <br> ❌ `O(n)` for `LinkedList` | ❌ Not allowed (No index-based access) |
| **Sorting** | ❌ Not sorted (`Collections.sort()` required) | ✅ `TreeSet` maintains sorted order (`O(log n)`) |
| **Iteration Performance** | ✅ Fast (`O(n)`) | ✅ Fast (`O(n)`) but `TreeSet` may be slower |
| **Duplicates Allowed?** | ✅ Yes | ❌ No |
| **Maintains Insertion Order?** | ✅ Yes (`ArrayList`, `LinkedList`) | ❌ No (`HashSet`), ✅ Yes (`LinkedHashSet`) |
| **Best For** | Ordered collection where duplicates are needed | Unique elements, faster searching |

---

## **When to Use `List` vs `Set`**
| **Use Case** | **Choose `List`** | **Choose `Set`** |
|-------------|------------------|------------------|
| **Fast Random Access (`get(index)`)** | ✅ Yes (`ArrayList`) | ❌ No |
| **Ensuring Unique Elements** | ❌ No | ✅ Yes |
| **Insertion Order Matters** | ✅ Yes (`LinkedList`) | ✅ Yes (`LinkedHashSet`) |
| **Fast Search Required** | ❌ No | ✅ Yes (`HashSet`) |
| **Sorting Required** | ❌ No (Requires sorting manually) | ✅ Yes (`TreeSet`) |

---

## **Example Demonstrating `List` vs `Set`**
```java
import java.util.*;

public class ListVsSetExample {
    public static void main(String[] args) {
        // Using List
        List<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Banana");
        list.add("Apple"); // Duplicate allowed

        System.out.println("List: " + list); // Output: [Apple, Banana, Apple]

        // Using Set
        Set<String> set = new HashSet<>();
        set.add("Apple");
        set.add("Banana");
        set.add("Apple"); // Duplicate ignored

        System.out.println("Set: " + set); // Output: [Apple, Banana]
    }
}
```
🔹 **`List` allows duplicates, `Set` does not**.

---

## **Real-Time Use Cases**
### **1. Storing Student Names (Avoiding Duplicates) – `HashSet`**
```java
import java.util.HashSet;

public class UniqueStudentNames {
    public static void main(String[] args) {
        HashSet<String> students = new HashSet<>();

        students.add("Alice");
        students.add("Bob");
        students.add("Alice"); // Duplicate ignored

        System.out.println("Students: " + students);
    }
}
```
**Output:**
```
Students: [Alice, Bob]
```
🔹 **Ensures only unique student names are stored.**

---

### **2. Maintaining Shopping Cart Items (Allowing Duplicates) – `ArrayList`**
```java
import java.util.ArrayList;

public class ShoppingCart {
    public static void main(String[] args) {
        ArrayList<String> cart = new ArrayList<>();

        cart.add("Laptop");
        cart.add("Mouse");
        cart.add("Laptop"); // Duplicate allowed

        System.out.println("Shopping Cart: " + cart);
    }
}
```
**Output:**
```
Shopping Cart: [Laptop, Mouse, Laptop]
```
🔹 **Allows duplicates (same item can be bought multiple times).**

---

### **3. Keeping Ordered Unique Visitor IPs – `LinkedHashSet`**
```java
import java.util.LinkedHashSet;

public class UniqueVisitors {
    public static void main(String[] args) {
        LinkedHashSet<String> visitors = new LinkedHashSet<>();

        visitors.add("192.168.1.1");
        visitors.add("192.168.1.2");
        visitors.add("192.168.1.1"); // Duplicate ignored

        System.out.println("Unique Visitors: " + visitors);
    }
}
```
**Output:**
```
Unique Visitors: [192.168.1.1, 192.168.1.2]
```
🔹 **Ensures unique visitor tracking while maintaining order.**

---

### **4. Ranking Players (Sorted Order) – `TreeSet`**
```java
import java.util.TreeSet;

public class PlayerRanking {
    public static void main(String[] args) {
        TreeSet<Integer> scores = new TreeSet<>();

        scores.add(85);
        scores.add(90);
        scores.add(78);

        System.out.println("Sorted Scores: " + scores);
    }
}
```
**Output:**
```
Sorted Scores: [78, 85, 90]
```
🔹 **Stores unique player scores in sorted order automatically.**

---

## **Final Summary**
| **Feature** | **List (`ArrayList`, `LinkedList`)** | **Set (`HashSet`, `TreeSet`, `LinkedHashSet`)** |
|------------|---------------------------------|---------------------------------|
| **Duplicates Allowed?** | ✅ Yes | ❌ No |
| **Maintains Order?** | ✅ Yes (`ArrayList`, `LinkedList`) | ❌ No (`HashSet`), ✅ Yes (`LinkedHashSet`) |
| **Sorting Required?** | ❌ No | ✅ `TreeSet` maintains sorting |
| **Fast Lookup?** | ❌ No (O(n)) | ✅ Yes (`HashSet` O(1), `TreeSet` O(log n)) |
| **Index-Based Access?** | ✅ Yes (`ArrayList`) | ❌ No |

---

## **Conclusion**
- **Use `List` when duplicates are allowed and order matters.**
- **Use `Set` when you need unique elements and fast lookups.**
- **Use `TreeSet` for sorted unique elements.**
- **Use `LinkedHashSet` for ordered unique elements.**

# What is HashSet?
A **`HashSet`** in Java is a collection that **stores unique elements** in an **unordered manner**. It is part of the **Java Collection Framework** and is implemented using a **HashMap** internally.

---

## **Key Features of `HashSet`**
✅ **No Duplicate Elements**  
✅ **Allows `null` (Only one `null` allowed)**  
✅ **Fast Performance (`O(1)` operations on average)**  
✅ **Efficient for Searching and Lookup**  
❌ **No Insertion Order Guarantee**  
❌ **Not Synchronized (Use `Collections.synchronizedSet()` for thread safety)**  

---

## **Hierarchy of `HashSet`**
```
       Collection
           │
          Set
           │
        HashSet
```
- **Implements `Set` Interface**  
- **Uses `HashMap` internally to store elements**  

---

## **Methods of `HashSet` with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Adds an element if it is not already present. | `set.add(10);` |
| **`remove(Object o)`** | Removes the specified element. | `set.remove(10);` |
| **`contains(Object o)`** | Checks if the element exists in the set. | `set.contains(20);` |
| **`size()`** | Returns the number of elements in the set. | `int size = set.size();` |
| **`clear()`** | Removes all elements from the set. | `set.clear();` |
| **`isEmpty()`** | Checks if the set is empty. | `boolean b = set.isEmpty();` |
| **`iterator()`** | Returns an iterator over the elements. | `Iterator<Integer> it = set.iterator();` |

---

## **Example: Basic Usage of `HashSet`**
```java
import java.util.HashSet;

public class HashSetExample {
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();

        // Adding elements
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(10); // Duplicate ignored

        System.out.println("HashSet: " + set);

        // Checking for an element
        System.out.println("Contains 20? " + set.contains(20));

        // Removing an element
        set.remove(10);

        System.out.println("After Removal: " + set);
    }
}
```

**Output:**
```
HashSet: [10, 20, 30]
Contains 20? true
After Removal: [20, 30]
```
🔹 **Ensures uniqueness and provides fast lookups.**

---

## **Time Complexity of `HashSet` Operations**
| Operation | **Time Complexity** |
|-----------|------------------|
| **Insertion (`add(e)`)** | O(1) (On average) |
| **Deletion (`remove(e)`)** | O(1) (On average) |
| **Search (`contains(e)`)** | O(1) (On average) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **In the worst case (hash collisions), `add()`, `remove()`, and `contains()` can take O(n).**

---

## **Real-Time Implementations of `HashSet`**

### **1. Removing Duplicate Emails (`HashSet`)**
```java
import java.util.HashSet;

public class UniqueEmails {
    public static void main(String[] args) {
        HashSet<String> emails = new HashSet<>();

        emails.add("alice@example.com");
        emails.add("bob@example.com");
        emails.add("alice@example.com"); // Duplicate ignored

        System.out.println("Unique Emails: " + emails);
    }
}
```
**Output:**
```
Unique Emails: [alice@example.com, bob@example.com]
```
🔹 **Ensures unique emails are stored.**

---

### **2. Unique Lottery Ticket Numbers (`HashSet`)**
```java
import java.util.HashSet;
import java.util.Random;

public class LotteryNumbers {
    public static void main(String[] args) {
        HashSet<Integer> lotteryNumbers = new HashSet<>();
        Random random = new Random();

        // Generating 5 unique random numbers
        while (lotteryNumbers.size() < 5) {
            lotteryNumbers.add(random.nextInt(100) + 1); // Numbers between 1 and 100
        }

        System.out.println("Lottery Numbers: " + lotteryNumbers);
    }
}
```
**Output Example:**
```
Lottery Numbers: [3, 45, 22, 88, 12]
```
🔹 **Ensures unique numbers in a lottery draw.**

---

### **3. Detecting Duplicate Movie Titles in a Streaming Service (`HashSet`)**
```java
import java.util.HashSet;

public class DuplicateDetector {
    public static void main(String[] args) {
        HashSet<String> watchedMovies = new HashSet<>();

        String[] movies = {"Inception", "Interstellar", "Inception", "Tenet"};

        for (String movie : movies) {
            if (!watchedMovies.add(movie)) {
                System.out.println("Duplicate Movie Found: " + movie);
            }
        }
    }
}
```
**Output:**
```
Duplicate Movie Found: Inception
```
🔹 **Detects duplicate movie titles instantly.**

---

### **4. Online Exam System: Unique Question Bank (`HashSet`)**
```java
import java.util.HashSet;

public class ExamQuestionBank {
    public static void main(String[] args) {
        HashSet<String> questions = new HashSet<>();

        questions.add("What is Java?");
        questions.add("Explain OOP concepts.");
        questions.add("What is a HashSet?");
        questions.add("What is Java?"); // Duplicate ignored

        System.out.println("Unique Questions: " + questions);
    }
}
```
**Output:**
```
Unique Questions: [What is Java?, Explain OOP concepts., What is a HashSet?]
```
🔹 **Ensures unique questions in an exam.**

---

## **Comparison: `HashSet` vs `TreeSet` vs `LinkedHashSet`**
| Feature | **HashSet** | **TreeSet** | **LinkedHashSet** |
|---------|------------|------------|----------------|
| **Order** | ❌ No order | ✅ Sorted order | ✅ Maintains insertion order |
| **Performance** | ✅ O(1) on average | ❌ O(log n) | ✅ O(1) |
| **Allows `null`?** | ✅ Yes | ❌ No | ✅ Yes |
| **Best For** | Fast lookups | Sorted elements | Maintaining order |

---

## **When to Use `HashSet`?**
| **Scenario** | **Use `HashSet`** |
|-------------|------------------|
| **Fast Lookup Required** | ✅ Yes |
| **No Order Required** | ✅ Yes |
| **Duplicates Must Be Avoided** | ✅ Yes |
| **High-Performance Data Storage** | ✅ Yes |
| **Sorting is Needed** | ❌ No (Use `TreeSet` instead) |
| **Insertion Order Should Be Maintained** | ❌ No (Use `LinkedHashSet` instead) |

---

## **Final Summary**
- **Use `HashSet` for fast searching and uniqueness when order doesn’t matter.**
- **Use `TreeSet` if sorting is required.**
- **Use `LinkedHashSet` if insertion order needs to be maintained.**

# What is LinkedHashSet?
A **`LinkedHashSet`** in Java is a hybrid collection that combines the features of **`HashSet`** and **`LinkedList`**. It maintains **insertion order** while ensuring **unique elements**.

---

## **Key Features of `LinkedHashSet`**
✅ **No Duplicate Elements** (Same as `HashSet`)  
✅ **Maintains Insertion Order** (Unlike `HashSet`)  
✅ **Allows `null` (Only one `null` allowed)**  
✅ **Fast Performance (`O(1)` for `add`, `remove`, `contains`)**  
✅ **Uses a Doubly Linked List Internally**  
❌ **Not Synchronized (Use `Collections.synchronizedSet()` for thread safety)**  
❌ **Higher Memory Usage (Due to Linked List References)**  

---

## **Hierarchy of `LinkedHashSet`**
```
       Collection
           │
          Set
           │
      HashSet
           │
     LinkedHashSet
```
- **Extends `HashSet`**
- **Implements `Set` Interface**
- **Uses `LinkedHashMap` internally to maintain insertion order**

---

## **Methods of `LinkedHashSet` with Examples**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Adds an element if it is not already present. | `set.add(10);` |
| **`remove(Object o)`** | Removes the specified element. | `set.remove(10);` |
| **`contains(Object o)`** | Checks if the element exists in the set. | `set.contains(20);` |
| **`size()`** | Returns the number of elements in the set. | `int size = set.size();` |
| **`clear()`** | Removes all elements from the set. | `set.clear();` |
| **`isEmpty()`** | Checks if the set is empty. | `boolean b = set.isEmpty();` |
| **`iterator()`** | Returns an iterator over the elements. | `Iterator<Integer> it = set.iterator();` |

---

## **Example: Basic Usage of `LinkedHashSet`**
```java
import java.util.LinkedHashSet;

public class LinkedHashSetExample {
    public static void main(String[] args) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();

        // Adding elements
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(10); // Duplicate ignored

        System.out.println("LinkedHashSet: " + set);

        // Checking for an element
        System.out.println("Contains 20? " + set.contains(20));

        // Removing an element
        set.remove(10);

        System.out.println("After Removal: " + set);
    }
}
```

**Output:**
```
LinkedHashSet: [10, 20, 30]
Contains 20? true
After Removal: [20, 30]
```
🔹 **Maintains insertion order while ensuring uniqueness.**

---

## **Time Complexity of `LinkedHashSet` Operations**
| Operation | **Time Complexity** |
|-----------|------------------|
| **Insertion (`add(e)`)** | O(1) |
| **Deletion (`remove(e)`)** | O(1) |
| **Search (`contains(e)`)** | O(1) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Maintains constant time complexity like `HashSet`, but uses extra memory for maintaining order.**

---

## **Real-Time Implementations of `LinkedHashSet`**

### **1. Maintaining Ordered Unique Usernames (`LinkedHashSet`)**
```java
import java.util.LinkedHashSet;

public class OrderedUsernames {
    public static void main(String[] args) {
        LinkedHashSet<String> usernames = new LinkedHashSet<>();

        usernames.add("Alice");
        usernames.add("Bob");
        usernames.add("Charlie");
        usernames.add("Alice"); // Duplicate ignored

        System.out.println("Usernames in Order: " + usernames);
    }
}
```
**Output:**
```
Usernames in Order: [Alice, Bob, Charlie]
```
🔹 **Ensures each username is unique and keeps them in the order of insertion.**

---

### **2. Maintaining Ordered Web Page History (`LinkedHashSet`)**
```java
import java.util.LinkedHashSet;

public class BrowserHistory {
    public static void main(String[] args) {
        LinkedHashSet<String> history = new LinkedHashSet<>();

        history.add("Google.com");
        history.add("StackOverflow.com");
        history.add("GitHub.com");
        history.add("Google.com"); // Duplicate ignored

        System.out.println("Web History: " + history);
    }
}
```
**Output:**
```
Web History: [Google.com, StackOverflow.com, GitHub.com]
```
🔹 **Prevents duplicate pages from appearing in history while maintaining order.**

---

### **3. Unique Ordered Product List (`LinkedHashSet`)**
```java
import java.util.LinkedHashSet;

public class ProductCatalog {
    public static void main(String[] args) {
        LinkedHashSet<String> products = new LinkedHashSet<>();

        products.add("Laptop");
        products.add("Mouse");
        products.add("Keyboard");
        products.add("Mouse"); // Duplicate ignored

        System.out.println("Product Catalog: " + products);
    }
}
```
**Output:**
```
Product Catalog: [Laptop, Mouse, Keyboard]
```
🔹 **Ensures product list remains unique while maintaining order.**

---

## **Comparison: `LinkedHashSet` vs `HashSet` vs `TreeSet`**
| Feature | **LinkedHashSet** | **HashSet** | **TreeSet** |
|---------|----------------|------------|------------|
| **Order Maintained?** | ✅ Yes (Insertion Order) | ❌ No Order | ✅ Sorted Order |
| **Performance** | ✅ O(1) | ✅ O(1) | ❌ O(log n) |
| **Memory Usage** | ❌ Higher (Due to Linked List) | ✅ Lower | ✅ Moderate |
| **Allows `null`?** | ✅ Yes | ✅ Yes | ❌ No |
| **Best Use Case** | Unique elements with order | Unique elements only | Unique sorted elements |

---

## **When to Use `LinkedHashSet`?**
| **Scenario** | **Use `LinkedHashSet`** |
|-------------|------------------|
| **Need to maintain insertion order?** | ✅ Yes |
| **Duplicates must be avoided?** | ✅ Yes |
| **Fast lookup required?** | ✅ Yes |
| **Sorting needed?** | ❌ No (Use `TreeSet` instead) |
| **Memory usage is not a concern?** | ✅ Yes |

---

## **Final Summary**
- **Use `LinkedHashSet` when you need unique elements and want to preserve insertion order.**
- **Use `HashSet` if order doesn’t matter but performance does.**
- **Use `TreeSet` if you need elements sorted automatically.**

# What is the difference between SortedSet, NavigableSet, and TreeSet?

In Java, **`SortedSet`**, **`NavigableSet`**, and **`TreeSet`** belong to the **Java Collection Framework** and are used to store **unique elements in sorted order**.

---

## **Key Differences**
| Feature | **SortedSet** | **NavigableSet** | **TreeSet** |
|---------|-------------|----------------|----------|
| **Definition** | A `Set` that maintains elements in **sorted order**. | A `SortedSet` with **navigation methods** (higher, lower, floor, ceiling). | A concrete implementation of `NavigableSet` using a **balanced binary search tree**. |
| **Ordering** | Maintains **natural sorting** or uses a **custom comparator**. | Maintains **natural sorting** with navigation features. | Uses a **Red-Black Tree** for **sorted storage**. |
| **Methods Available** | Basic methods for ordering. | Additional **navigation methods** like `floor()`, `ceiling()`, `higher()`, `lower()`. | Implements all methods from `SortedSet` and `NavigableSet`. |
| **Implementation Class** | **`TreeSet`** | **`TreeSet`** | **Red-Black Tree Implementation** |
| **Null Elements** | ❌ Not allowed in natural order. | ❌ Not allowed in natural order. | ❌ Not allowed in natural order. |

---

## **Hierarchy of `SortedSet`, `NavigableSet`, and `TreeSet`**
```
       Collection
           │
          Set
           │
      SortedSet
           │
     NavigableSet
           │
        TreeSet
```
- **`SortedSet`**: Maintains elements in sorted order.
- **`NavigableSet`**: Extends `SortedSet`, adding methods for **navigation**.
- **`TreeSet`**: Implements `NavigableSet` using a **balanced Red-Black Tree**.

---

## **Methods in `SortedSet`, `NavigableSet`, and `TreeSet`**
### **Methods in `SortedSet`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`first()`** | Returns the **first (lowest) element**. | `set.first();` |
| **`last()`** | Returns the **last (highest) element**. | `set.last();` |
| **`headSet(E toElement)`** | Returns elements **less than `toElement`**. | `set.headSet(50);` |
| **`tailSet(E fromElement)`** | Returns elements **greater than or equal to `fromElement`**. | `set.tailSet(20);` |
| **`subSet(E fromElement, E toElement)`** | Returns elements in the range **[`fromElement`, `toElement`)**. | `set.subSet(10, 50);` |

---

### **Additional Methods in `NavigableSet`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`lower(E e)`** | Returns the **greatest element** less than `e`. | `set.lower(40);` |
| **`floor(E e)`** | Returns the **greatest element** less than or equal to `e`. | `set.floor(40);` |
| **`higher(E e)`** | Returns the **smallest element** greater than `e`. | `set.higher(40);` |
| **`ceiling(E e)`** | Returns the **smallest element** greater than or equal to `e`. | `set.ceiling(40);` |
| **`descendingSet()`** | Returns a **reverse view** of the elements. | `set.descendingSet();` |

---

## **Example: Using `TreeSet` with `SortedSet` Methods**
```java
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetExample {
    public static void main(String[] args) {
        SortedSet<Integer> set = new TreeSet<>();

        // Adding elements
        set.add(10);
        set.add(30);
        set.add(50);
        set.add(70);

        System.out.println("SortedSet: " + set);
        System.out.println("First Element: " + set.first());
        System.out.println("Last Element: " + set.last());
        System.out.println("HeadSet(50): " + set.headSet(50));
        System.out.println("TailSet(30): " + set.tailSet(30));
        System.out.println("SubSet(10, 50): " + set.subSet(10, 50));
    }
}
```
**Output:**
```
SortedSet: [10, 30, 50, 70]
First Element: 10
Last Element: 70
HeadSet(50): [10, 30]
TailSet(30): [30, 50, 70]
SubSet(10, 50): [10, 30]
```

---

## **Example: Using `TreeSet` with `NavigableSet` Methods**
```java
import java.util.NavigableSet;
import java.util.TreeSet;

public class NavigableSetExample {
    public static void main(String[] args) {
        NavigableSet<Integer> set = new TreeSet<>();

        set.add(10);
        set.add(30);
        set.add(50);
        set.add(70);

        System.out.println("NavigableSet: " + set);
        System.out.println("Lower(40): " + set.lower(40));
        System.out.println("Floor(50): " + set.floor(50));
        System.out.println("Higher(40): " + set.higher(40));
        System.out.println("Ceiling(50): " + set.ceiling(50));
        System.out.println("DescendingSet: " + set.descendingSet());
    }
}
```
**Output:**
```
NavigableSet: [10, 30, 50, 70]
Lower(40): 30
Floor(50): 50
Higher(40): 50
Ceiling(50): 50
DescendingSet: [70, 50, 30, 10]
```

---

## **Time Complexity of Operations**
| Operation | **TreeSet (`SortedSet`, `NavigableSet`)** |
|-----------|------------------|
| **Insertion (`add(e)`)** | O(log n) |
| **Deletion (`remove(e)`)** | O(log n) |
| **Search (`contains(e)`)** | O(log n) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Uses a Red-Black Tree (Self-Balancing Binary Search Tree).**

---

## **Real-World Applications**
### **1. Storing Unique Sorted Product Prices (`TreeSet`)**
```java
import java.util.TreeSet;

public class ProductPrices {
    public static void main(String[] args) {
        TreeSet<Double> prices = new TreeSet<>();

        prices.add(199.99);
        prices.add(99.99);
        prices.add(299.99);
        prices.add(199.99); // Duplicate ignored

        System.out.println("Product Prices (Sorted): " + prices);
    }
}
```
🔹 **Keeps product prices sorted and unique.**

---

### **2. Implementing Auto-Suggestion for Search Terms (`NavigableSet`)**
```java
import java.util.TreeSet;

public class AutoSuggestion {
    public static void main(String[] args) {
        TreeSet<String> words = new TreeSet<>();

        words.add("apple");
        words.add("banana");
        words.add("grape");
        words.add("mango");

        // Suggest words starting from "b"
        System.out.println("Words starting from 'b': " + words.tailSet("b"));
    }
}
```
🔹 **Returns sorted suggestions for search terms.**

---

## **Final Summary**
| Feature | **SortedSet** | **NavigableSet** | **TreeSet** |
|---------|-------------|----------------|----------|
| **Sorted?** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Navigation Methods?** | ❌ No | ✅ Yes | ✅ Yes |
| **Self-Balancing?** | ❌ No | ❌ No | ✅ Yes (Red-Black Tree) |
| **Best For?** | Sorted Unique Data | Navigable Sorted Data | Ordered Unique Data with Fast Operations |

---

## **Conclusion**
- **Use `SortedSet` if only basic sorting is needed.**
- **Use `NavigableSet` if you need extra navigation functions (`floor()`, `ceiling()`).**
- **Use `TreeSet` as a concrete implementation for sorted and unique elements.**

# What is ConcurrentSkipListSet?
`ConcurrentSkipListSet` is a **thread-safe, sorted, and concurrent version** of `TreeSet`. It is part of the **`java.util.concurrent` package** and implements **`NavigableSet`** using a **Skip List** instead of a Red-Black Tree (used by `TreeSet`).

---

## **Key Features of `ConcurrentSkipListSet`**
✅ **Thread-Safe** (Lock-Free Mechanism)  
✅ **Maintains Natural Sorting Order**  
✅ **Faster Concurrent Reads than `TreeSet`**  
✅ **Non-Blocking, Lock-Free Data Structure**  
✅ **Implements `NavigableSet` and `SortedSet`**  
❌ **Slower Writes Compared to `TreeSet` (O(log n))**  
❌ **Higher Memory Usage (Due to Skip List Pointers)**  

---

## **Hierarchy of `ConcurrentSkipListSet`**
```
       Collection
           │
          Set
           │
      SortedSet
           │
     NavigableSet
           │
ConcurrentSkipListSet
```
- **Implements `NavigableSet` and `SortedSet`**
- **Uses Skip List Instead of Tree for Sorting**
- **Thread-Safe Alternative to `TreeSet`**

---

## **Methods of `ConcurrentSkipListSet`**
### **Methods from `SortedSet`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`first()`** | Returns the first (lowest) element. | `set.first();` |
| **`last()`** | Returns the last (highest) element. | `set.last();` |
| **`headSet(E toElement)`** | Returns elements **less than `toElement`**. | `set.headSet(50);` |
| **`tailSet(E fromElement)`** | Returns elements **greater than or equal to `fromElement`**. | `set.tailSet(20);` |
| **`subSet(E fromElement, E toElement)`** | Returns elements in the range **[`fromElement`, `toElement`)**. | `set.subSet(10, 50);` |

---

### **Additional Methods from `NavigableSet`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`lower(E e)`** | Returns the greatest element **less than `e`**. | `set.lower(40);` |
| **`floor(E e)`** | Returns the greatest element **less than or equal to `e`**. | `set.floor(40);` |
| **`higher(E e)`** | Returns the smallest element **greater than `e`**. | `set.higher(40);` |
| **`ceiling(E e)`** | Returns the smallest element **greater than or equal to `e`**. | `set.ceiling(40);` |
| **`descendingSet()`** | Returns a **reverse view** of the elements. | `set.descendingSet();` |

---

### **Methods from `ConcurrentSkipListSet`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`add(E e)`** | Inserts the element while maintaining order. | `set.add(30);` |
| **`remove(Object o)`** | Removes the specified element. | `set.remove(30);` |
| **`contains(Object o)`** | Checks if the set contains an element. | `set.contains(40);` |
| **`size()`** | Returns the number of elements. | `set.size();` |
| **`clear()`** | Removes all elements. | `set.clear();` |
| **`isEmpty()`** | Checks if the set is empty. | `set.isEmpty();` |
| **`iterator()`** | Returns an iterator over the elements. | `Iterator<Integer> it = set.iterator();` |

---

## **Example: Basic Usage of `ConcurrentSkipListSet`**
```java
import java.util.concurrent.ConcurrentSkipListSet;

public class ConcurrentSkipListSetExample {
    public static void main(String[] args) {
        ConcurrentSkipListSet<Integer> set = new ConcurrentSkipListSet<>();

        // Adding elements
        set.add(10);
        set.add(50);
        set.add(30);
        set.add(20);

        System.out.println("ConcurrentSkipListSet: " + set);

        // Navigation operations
        System.out.println("First Element: " + set.first());
        System.out.println("Last Element: " + set.last());
        System.out.println("Higher than 20: " + set.higher(20));
        System.out.println("Lower than 50: " + set.lower(50));

        // Removing an element
        set.remove(30);
        System.out.println("After Removal: " + set);
    }
}
```

**Output:**
```
ConcurrentSkipListSet: [10, 20, 30, 50]
First Element: 10
Last Element: 50
Higher than 20: 30
Lower than 50: 30
After Removal: [10, 20, 50]
```
🔹 **Thread-safe, maintains sorted order, allows concurrent access.**

---

## **Time Complexity of `ConcurrentSkipListSet` Operations**
| Operation | **Time Complexity** |
|-----------|------------------|
| **Insertion (`add(e)`)** | O(log n) |
| **Deletion (`remove(e)`)** | O(log n) |
| **Search (`contains(e)`)** | O(log n) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Performs similar to `TreeSet` but optimized for concurrency.**

---

## **Comparison: `ConcurrentSkipListSet` vs `TreeSet` vs `HashSet`**
| Feature | **ConcurrentSkipListSet** | **TreeSet** | **HashSet** |
|---------|----------------|---------|---------|
| **Thread-Safe?** | ✅ Yes | ❌ No | ❌ No |
| **Ordering Maintained?** | ✅ Yes (Sorted Order) | ✅ Yes (Sorted Order) | ❌ No |
| **Performance** | 🚀 Optimized for concurrent access | ⚡ Faster for single-threaded use | ⚡ Fastest but unordered |
| **Best For** | Concurrent Sorted Data | Single-threaded sorted data | Fast unordered data |

---

## **Real-Time Use Cases**
### **1. Managing a Live Leaderboard (`ConcurrentSkipListSet`)**
```java
import java.util.concurrent.ConcurrentSkipListSet;

public class LiveLeaderboard {
    public static void main(String[] args) {
        ConcurrentSkipListSet<Integer> scores = new ConcurrentSkipListSet<>();

        // Multiple users submitting scores
        scores.add(300);
        scores.add(450);
        scores.add(200);
        scores.add(450); // Duplicate ignored

        System.out.println("Leaderboard Scores: " + scores);
    }
}
```
🔹 **Ensures real-time updates and sorted scores for a game leaderboard.**

---

### **2. Real-Time Stock Price Monitoring (`ConcurrentSkipListSet`)**
```java
import java.util.concurrent.ConcurrentSkipListSet;

public class StockPriceMonitor {
    public static void main(String[] args) {
        ConcurrentSkipListSet<Double> stockPrices = new ConcurrentSkipListSet<>();

        // Simulating stock price updates
        stockPrices.add(150.5);
        stockPrices.add(152.3);
        stockPrices.add(148.7);
        stockPrices.add(153.2);

        System.out.println("Stock Prices: " + stockPrices);
    }
}
```
🔹 **Ensures stock prices remain sorted and accessible concurrently.**

---

### **3. Unique Concurrent Logging System (`ConcurrentSkipListSet`)**
```java
import java.util.concurrent.ConcurrentSkipListSet;

public class LogSystem {
    private static ConcurrentSkipListSet<String> logs = new ConcurrentSkipListSet<>();

    public static void log(String message) {
        logs.add(message);
    }

    public static void displayLogs() {
        System.out.println("Logs: " + logs);
    }

    public static void main(String[] args) {
        log("Error 404: Page not found");
        log("User logged in");
        log("Database connection established");

        displayLogs();
    }
}
```
🔹 **Ensures unique and sorted logs in a multi-threaded environment.**

---

## **Final Summary**
| Feature | **ConcurrentSkipListSet** | **TreeSet** | **HashSet** |
|---------|----------------|---------|---------|
| **Thread-Safe?** | ✅ Yes | ❌ No | ❌ No |
| **Sorted?** | ✅ Yes | ✅ Yes | ❌ No |
| **Lock-Free?** | ✅ Yes | ❌ No | ❌ No |
| **Best For?** | Concurrent Sorted Data | Single-threaded Sorted Data | Fast Unordered Data |

---
## **Conclusion**
- **Use `ConcurrentSkipListSet` for real-time sorted, thread-safe data.**
- **Use `TreeSet` if thread-safety is not needed.**
- **Use `HashSet` for unordered, fast lookup needs.**

# What is Map?
A **`Map`** in Java is a collection that **stores key-value pairs**. It is part of the **Java Collection Framework** and provides efficient methods for **fast lookups, insertions, and deletions**.

---

## **Key Features of `Map`**
✅ **Stores Key-Value Pairs**  
✅ **Keys Must Be Unique**  
✅ **Allows `null` Keys (Only One) and `null` Values**  
✅ **Efficient Lookups (`O(1)` for `HashMap`, `O(log n)` for `TreeMap`)**  
❌ **Not Ordered (`HashMap`), Ordered (`LinkedHashMap`), Sorted (`TreeMap`)**  
❌ **Not Thread-Safe (Use `ConcurrentHashMap` for thread safety)**  

---

## **Hierarchy of `Map` in Java**
```
       Map
        │
 ┌──────┴──────┐
 |            SortedMap
 HashMap       │
 |        NavigableMap
 LinkedHashMap │
             TreeMap
```
- **`HashMap`** – Fastest lookup, unordered.  
- **`LinkedHashMap`** – Maintains insertion order.  
- **`TreeMap`** – Maintains sorted order (`O(log n)`).  
- **`ConcurrentHashMap`** – Thread-safe `HashMap`.  

---

## **Methods of `Map`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`put(K key, V value)`** | Inserts a key-value pair. | `map.put(1, "Alice");` |
| **`get(K key)`** | Retrieves the value associated with the key. | `map.get(1);` |
| **`remove(K key)`** | Removes the mapping for the key. | `map.remove(1);` |
| **`containsKey(K key)`** | Checks if a key exists in the map. | `map.containsKey(2);` |
| **`containsValue(V value)`** | Checks if a value exists in the map. | `map.containsValue("Alice");` |
| **`size()`** | Returns the number of key-value pairs. | `map.size();` |
| **`isEmpty()`** | Checks if the map is empty. | `map.isEmpty();` |
| **`clear()`** | Removes all key-value pairs. | `map.clear();` |
| **`keySet()`** | Returns a set of all keys. | `Set<K> keys = map.keySet();` |
| **`values()`** | Returns a collection of all values. | `Collection<V> values = map.values();` |
| **`entrySet()`** | Returns a set of all key-value pairs. | `Set<Map.Entry<K,V>> entries = map.entrySet();` |

---

## **Example: Basic Usage of `HashMap`**
```java
import java.util.HashMap;

public class HashMapExample {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        // Adding key-value pairs
        map.put(1, "Alice");
        map.put(2, "Bob");
        map.put(3, "Charlie");

        System.out.println("Map: " + map);
        System.out.println("Value for key 2: " + map.get(2));

        // Removing a key
        map.remove(1);
        System.out.println("After Removal: " + map);

        // Checking if a key exists
        System.out.println("Contains key 3? " + map.containsKey(3));

        // Iterating over keys
        for (Integer key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
```

**Output:**
```
Map: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Fast lookups and modifications with `HashMap`.**

---

## **Time Complexity of `Map` Implementations**
| Operation | **HashMap (O(1))** | **LinkedHashMap (O(1))** | **TreeMap (O(log n))** |
|-----------|------------------|------------------|------------------|
| **Insertion (`put(k, v)`)** | O(1) | O(1) | O(log n) |
| **Deletion (`remove(k)`)** | O(1) | O(1) | O(log n) |
| **Search (`containsKey(k)`)** | O(1) | O(1) | O(log n) |
| **Iteration** | O(n) | O(n) (Preserves Order) | O(n) |

🔹 **Choose `HashMap` for best performance unless sorting is required (`TreeMap`).**

---

## **Real-Time Implementations of `Map`**

### **1. Student Grade Tracking (`HashMap`)**
```java
import java.util.HashMap;

public class StudentGrades {
    public static void main(String[] args) {
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("Alice", 85);
        grades.put("Bob", 90);
        grades.put("Charlie", 78);

        System.out.println("Grades: " + grades);

        // Checking a student's grade
        System.out.println("Alice's Grade: " + grades.get("Alice"));
    }
}
```
🔹 **Keeps track of student grades using `HashMap`.**

---

### **2. Maintaining Website Visit Order (`LinkedHashMap`)**
```java
import java.util.LinkedHashMap;

public class WebPageVisits {
    public static void main(String[] args) {
        LinkedHashMap<String, Integer> visits = new LinkedHashMap<>();

        visits.put("Google", 1000);
        visits.put("StackOverflow", 500);
        visits.put("GitHub", 300);

        System.out.println("Website Visits: " + visits);
    }
}
```
🔹 **Maintains the order in which websites were visited.**

---

### **3. Storing Sorted Bank Account Balances (`TreeMap`)**
```java
import java.util.TreeMap;

public class BankAccounts {
    public static void main(String[] args) {
        TreeMap<Integer, String> accounts = new TreeMap<>();

        accounts.put(1003, "Charlie");
        accounts.put(1001, "Alice");
        accounts.put(1002, "Bob");

        System.out.println("Sorted Bank Accounts: " + accounts);
    }
}
```
🔹 **Ensures bank accounts are sorted by account number.**

---

## **Comparison: `HashMap` vs `LinkedHashMap` vs `TreeMap`**
| Feature | **HashMap** | **LinkedHashMap** | **TreeMap** |
|---------|-----------|-----------------|----------|
| **Thread-Safe?** | ❌ No | ❌ No | ❌ No |
| **Maintains Order?** | ❌ No | ✅ Yes (Insertion Order) | ✅ Yes (Sorted Order) |
| **Performance** | ✅ Fastest | ✅ Fast (Maintains Order) | ❌ Slower (`O(log n)`) |
| **Best Use Case** | Fast Lookups | Ordered Key-Value Storage | Sorted Key-Value Storage |

---

## **When to Use Which `Map`?**
| **Scenario** | **Use `HashMap`** | **Use `LinkedHashMap`** | **Use `TreeMap`** |
|-------------|------------------|------------------|------------------|
| **Fastest Lookup (`O(1)`)** | ✅ Yes | ✅ Yes | ❌ No |
| **Maintain Insertion Order** | ❌ No | ✅ Yes | ❌ No |
| **Maintain Sorted Order** | ❌ No | ❌ No | ✅ Yes |
| **Best For** | **General-Purpose Key-Value Storage** | **Cache Implementations** | **Sorted Data Structures** |

---

## **Final Summary**
- **Use `HashMap` for fast, unordered key-value storage.**
- **Use `LinkedHashMap` if insertion order must be preserved.**
- **Use `TreeMap` for automatically sorted keys.**

# What is Hashtable?
# **`Hashtable` in Java**
A **`Hashtable`** in Java is a **thread-safe, synchronized** implementation of the `Map` interface that stores **key-value pairs**. It is similar to `HashMap` but is **synchronized**, meaning it is suitable for **multi-threaded environments**.

---

## **Key Features of `Hashtable`**
✅ **Thread-Safe (Synchronized Methods)**  
✅ **No `null` Keys or Values Allowed**  
✅ **Fast Lookups (`O(1)`)**  
✅ **Uses Hashing for Storage**  
❌ **Synchronized (Lower Performance in Single-Threaded Environments)**  
❌ **Iteration Overhead Due to Synchronization**  
❌ **Prefer `ConcurrentHashMap` for Better Performance in Multithreading**  

---

## **Hierarchy of `Hashtable`**
```
        Map
         │
      Hashtable
```
- **Implements `Map<K,V>` Interface**  
- **Synchronized Alternative to `HashMap`**  
- **Does NOT allow `null` keys or values**  
- **Legacy Class (Introduced in Java 1.0, Before `HashMap`)**  

---

## **Methods in `Hashtable`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`put(K key, V value)`** | Inserts a key-value pair. | `table.put(1, "Alice");` |
| **`get(K key)`** | Retrieves the value associated with the key. | `table.get(1);` |
| **`remove(K key)`** | Removes the mapping for the key. | `table.remove(1);` |
| **`containsKey(K key)`** | Checks if a key exists. | `table.containsKey(2);` |
| **`containsValue(V value)`** | Checks if a value exists. | `table.containsValue("Alice");` |
| **`size()`** | Returns the number of key-value pairs. | `table.size();` |
| **`isEmpty()`** | Checks if the table is empty. | `table.isEmpty();` |
| **`clear()`** | Removes all entries. | `table.clear();` |
| **`keySet()`** | Returns a `Set` of keys. | `Set<K> keys = table.keySet();` |
| **`values()`** | Returns a collection of values. | `Collection<V> values = table.values();` |
| **`entrySet()`** | Returns a set of key-value pairs. | `Set<Map.Entry<K,V>> entries = table.entrySet();` |

---

## **Example: Basic Usage of `Hashtable`**
```java
import java.util.Hashtable;

public class HashtableExample {
    public static void main(String[] args) {
        Hashtable<Integer, String> table = new Hashtable<>();

        // Adding key-value pairs
        table.put(1, "Alice");
        table.put(2, "Bob");
        table.put(3, "Charlie");

        System.out.println("Hashtable: " + table);
        System.out.println("Value for key 2: " + table.get(2));

        // Removing a key
        table.remove(1);
        System.out.println("After Removal: " + table);

        // Checking if a key exists
        System.out.println("Contains key 3? " + table.containsKey(3));

        // Iterating over keys
        for (Integer key : table.keySet()) {
            System.out.println("Key: " + key + ", Value: " + table.get(key));
        }
    }
}
```

**Output:**
```
Hashtable: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Synchronized key-value storage, fast lookup, but no `null` keys/values.**

---

## **Time Complexity of `Hashtable` Operations**
| Operation | **Hashtable (`O(1)`)** |
|-----------|------------------|
| **Insertion (`put(k, v)`)** | O(1) (Amortized) |
| **Deletion (`remove(k)`)** | O(1) |
| **Search (`containsKey(k)`)** | O(1) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Worst case O(n) if there are hash collisions.**

---

## **Comparison: `Hashtable` vs `HashMap` vs `ConcurrentHashMap`**
| Feature | **Hashtable** | **HashMap** | **ConcurrentHashMap** |
|---------|-------------|-------------|------------------|
| **Thread-Safe?** | ✅ Yes | ❌ No | ✅ Yes |
| **Allows `null` Keys?** | ❌ No | ✅ Yes | ❌ No |
| **Allows `null` Values?** | ❌ No | ✅ Yes | ❌ No |
| **Performance** | ❌ Slower (Synchronized) | ✅ Faster (No Locks) | ✅ Faster (Segmented Locking) |
| **Iteration Performance** | ❌ Slower | ✅ Faster | ✅ Better for Multithreading |
| **Use Case** | Legacy Multithreading | General Use | High-Performance Multithreading |

🔹 **Use `ConcurrentHashMap` instead of `Hashtable` in multi-threaded environments.**  
🔹 **Use `HashMap` for better performance in single-threaded applications.**  

---

## **Real-Time Implementations of `Hashtable`**

### **1. Employee Directory (Thread-Safe Lookup)**
```java
import java.util.Hashtable;

public class EmployeeDirectory {
    public static void main(String[] args) {
        Hashtable<Integer, String> employees = new Hashtable<>();

        employees.put(101, "Alice");
        employees.put(102, "Bob");
        employees.put(103, "Charlie");

        System.out.println("Employee Directory: " + employees);
        System.out.println("Employee ID 102: " + employees.get(102));
    }
}
```
🔹 **Thread-safe employee lookup system.**

---

### **2. Flight Seat Booking System**
```java
import java.util.Hashtable;

public class FlightSeatBooking {
    public static void main(String[] args) {
        Hashtable<Integer, String> seats = new Hashtable<>();

        seats.put(1, "Alice");
        seats.put(2, "Bob");

        System.out.println("Seat 1 booked by: " + seats.get(1));

        // Checking if a seat is available
        if (!seats.containsKey(3)) {
            System.out.println("Seat 3 is available for booking.");
        }
    }
}
```
🔹 **Prevents duplicate seat bookings in flights.**

---

### **3. Banking System (Account Balance Lookup)**
```java
import java.util.Hashtable;

public class BankSystem {
    public static void main(String[] args) {
        Hashtable<Integer, Double> accounts = new Hashtable<>();

        accounts.put(1001, 5000.00);
        accounts.put(1002, 10000.50);

        System.out.println("Account 1002 Balance: $" + accounts.get(1002));

        // Checking if an account exists
        if (accounts.containsKey(1003)) {
            System.out.println("Account 1003 exists.");
        } else {
            System.out.println("Account 1003 does not exist.");
        }
    }
}
```
🔹 **Ensures secure account balance retrieval in a multi-threaded system.**

---

## **When to Use `Hashtable`?**
| **Scenario** | **Use `Hashtable`** |
|-------------|------------------|
| **Thread-Safe Key-Value Storage?** | ✅ Yes |
| **Fast Lookup Needed (`O(1)`)** | ✅ Yes |
| **Allows `null` Keys/Values?** | ❌ No |
| **Concurrent Operations?** | ✅ Yes (But `ConcurrentHashMap` is Better) |
| **Legacy System Compatibility?** | ✅ Yes |

---

## **Final Summary**
- **Use `Hashtable` for synchronized key-value storage in legacy applications.**
- **Use `HashMap` if synchronization is not needed.**
- **Use `ConcurrentHashMap` for better multi-threading support.**

# What is HashMap?
A **`HashMap`** in Java is a **key-value-based** data structure that provides **fast lookups, insertions, and deletions** using a **hashing mechanism**. It is part of the **Java Collection Framework** and implements the **`Map` interface**.

---

## **Key Features of `HashMap`**
✅ **Stores Key-Value Pairs**  
✅ **Allows `null` Keys (Only One) and `null` Values**  
✅ **Fast Lookup (`O(1)` on Average, `O(n)` in Worst Case)**  
✅ **Not Synchronized (Use `Collections.synchronizedMap()` for thread safety)**  
❌ **Unordered (No Guarantee of Insertion Order)**  
❌ **Not Thread-Safe (Use `ConcurrentHashMap` for multi-threading)**  

---

## **Hierarchy of `HashMap`**
```
        Map
         │
      HashMap
```
- **Implements `Map<K,V>` Interface**  
- **Uses Hashing for Efficient Storage & Lookup**  
- **Backed by an Array of Linked Lists (Bucket Chaining)**  

---

## **Methods in `HashMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`put(K key, V value)`** | Inserts a key-value pair. | `map.put(1, "Alice");` |
| **`get(K key)`** | Retrieves the value associated with the key. | `map.get(1);` |
| **`remove(K key)`** | Removes the mapping for the key. | `map.remove(1);` |
| **`containsKey(K key)`** | Checks if a key exists. | `map.containsKey(2);` |
| **`containsValue(V value)`** | Checks if a value exists. | `map.containsValue("Alice");` |
| **`size()`** | Returns the number of key-value pairs. | `map.size();` |
| **`isEmpty()`** | Checks if the map is empty. | `map.isEmpty();` |
| **`clear()`** | Removes all key-value pairs. | `map.clear();` |
| **`keySet()`** | Returns a set of all keys. | `Set<K> keys = map.keySet();` |
| **`values()`** | Returns a collection of all values. | `Collection<V> values = map.values();` |
| **`entrySet()`** | Returns a set of all key-value pairs. | `Set<Map.Entry<K,V>> entries = map.entrySet();` |

---

## **Example: Basic Usage of `HashMap`**
```java
import java.util.HashMap;

public class HashMapExample {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        // Adding key-value pairs
        map.put(1, "Alice");
        map.put(2, "Bob");
        map.put(3, "Charlie");

        System.out.println("HashMap: " + map);
        System.out.println("Value for key 2: " + map.get(2));

        // Removing a key
        map.remove(1);
        System.out.println("After Removal: " + map);

        // Checking if a key exists
        System.out.println("Contains key 3? " + map.containsKey(3));

        // Iterating over keys
        for (Integer key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
```

**Output:**
```
HashMap: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Fast lookups and modifications with `HashMap`.**

---

## **Time Complexity of `HashMap` Operations**
| Operation | **HashMap (O(1) on Average, O(n) Worst Case)** |
|-----------|------------------|
| **Insertion (`put(k, v)`)** | O(1) |
| **Deletion (`remove(k)`)** | O(1) |
| **Search (`containsKey(k)`)** | O(1) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Worst case O(n) if many hash collisions occur.**

---

## **Comparison: `HashMap` vs `LinkedHashMap` vs `TreeMap`**
| Feature | **HashMap** | **LinkedHashMap** | **TreeMap** |
|---------|-----------|-----------------|----------|
| **Thread-Safe?** | ❌ No | ❌ No | ❌ No |
| **Maintains Order?** | ❌ No | ✅ Yes (Insertion Order) | ✅ Yes (Sorted Order) |
| **Performance** | ✅ Fastest | ✅ Fast (Maintains Order) | ❌ Slower (`O(log n)`) |
| **Best Use Case** | Fast Lookups | Ordered Key-Value Storage | Sorted Key-Value Storage |

---

## **Real-Time Implementations of `HashMap`**

### **1. Student Grade Tracking (`HashMap`)**
```java
import java.util.HashMap;

public class StudentGrades {
    public static void main(String[] args) {
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("Alice", 85);
        grades.put("Bob", 90);
        grades.put("Charlie", 78);

        System.out.println("Grades: " + grades);

        // Checking a student's grade
        System.out.println("Alice's Grade: " + grades.get("Alice"));
    }
}
```
🔹 **Keeps track of student grades using `HashMap`.**

---

### **2. Maintaining Website Visit Order (`LinkedHashMap`)**
```java
import java.util.LinkedHashMap;

public class WebPageVisits {
    public static void main(String[] args) {
        LinkedHashMap<String, Integer> visits = new LinkedHashMap<>();

        visits.put("Google", 1000);
        visits.put("StackOverflow", 500);
        visits.put("GitHub", 300);

        System.out.println("Website Visits: " + visits);
    }
}
```
🔹 **Maintains the order in which websites were visited.**

---

### **3. Storing Sorted Bank Account Balances (`TreeMap`)**
```java
import java.util.TreeMap;

public class BankAccounts {
    public static void main(String[] args) {
        TreeMap<Integer, String> accounts = new TreeMap<>();

        accounts.put(1003, "Charlie");
        accounts.put(1001, "Alice");
        accounts.put(1002, "Bob");

        System.out.println("Sorted Bank Accounts: " + accounts);
    }
}
```
🔹 **Ensures bank accounts are sorted by account number.**

---

## **When to Use Which `Map`?**
| **Scenario** | **Use `HashMap`** | **Use `LinkedHashMap`** | **Use `TreeMap`** |
|-------------|------------------|------------------|------------------|
| **Fastest Lookup (`O(1)`)** | ✅ Yes | ✅ Yes | ❌ No |
| **Maintain Insertion Order** | ❌ No | ✅ Yes | ❌ No |
| **Maintain Sorted Order** | ❌ No | ❌ No | ✅ Yes |
| **Best For** | **General-Purpose Key-Value Storage** | **Cache Implementations** | **Sorted Data Structures** |

---

## **Final Summary**
- **Use `HashMap` for fast, unordered key-value storage.**
- **Use `LinkedHashMap` if insertion order must be preserved.**
- **Use `TreeMap` for automatically sorted keys.**

# What is LinkedHashMap?
A **`LinkedHashMap`** in Java is a **key-value-based** data structure that maintains the **insertion order** of elements while providing fast lookup operations. It is part of the **Java Collection Framework** and extends `HashMap`.

---

## **Key Features of `LinkedHashMap`**
✅ **Stores Key-Value Pairs**  
✅ **Maintains Insertion Order**  
✅ **Allows `null` Keys and Values**  
✅ **Fast Lookup (`O(1)` on Average, `O(n)` in Worst Case)**  
✅ **Supports **Access-Order** (Used for Caching)**  
❌ **Not Thread-Safe (Use `Collections.synchronizedMap()` for thread safety)**  
❌ **Slightly Slower than `HashMap` (Due to Linked List Overhead)**  

---

## **Hierarchy of `LinkedHashMap`**
```
        Map
         │
      HashMap
         │
   LinkedHashMap
```
- **Extends `HashMap<K,V>`**  
- **Uses a Hash Table + Doubly Linked List for maintaining order**  
- **Implements `Map<K,V>` Interface**  

---

## **Methods in `LinkedHashMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`put(K key, V value)`** | Inserts a key-value pair. | `map.put(1, "Alice");` |
| **`get(K key)`** | Retrieves the value associated with the key. | `map.get(1);` |
| **`remove(K key)`** | Removes the mapping for the key. | `map.remove(1);` |
| **`containsKey(K key)`** | Checks if a key exists. | `map.containsKey(2);` |
| **`containsValue(V value)`** | Checks if a value exists. | `map.containsValue("Alice");` |
| **`size()`** | Returns the number of key-value pairs. | `map.size();` |
| **`isEmpty()`** | Checks if the map is empty. | `map.isEmpty();` |
| **`clear()`** | Removes all key-value pairs. | `map.clear();` |
| **`keySet()`** | Returns a `Set` of all keys. | `Set<K> keys = map.keySet();` |
| **`values()`** | Returns a collection of all values. | `Collection<V> values = map.values();` |
| **`entrySet()`** | Returns a set of key-value pairs. | `Set<Map.Entry<K,V>> entries = map.entrySet();` |
| **`getOrDefault(K key, V defaultValue)`** | Returns the value for the key, or a default if key is missing. | `map.getOrDefault(5, "Not Found");` |

---

## **Example: Basic Usage of `LinkedHashMap`**
```java
import java.util.LinkedHashMap;

public class LinkedHashMapExample {
    public static void main(String[] args) {
        LinkedHashMap<Integer, String> map = new LinkedHashMap<>();

        // Adding key-value pairs
        map.put(1, "Alice");
        map.put(2, "Bob");
        map.put(3, "Charlie");

        System.out.println("LinkedHashMap: " + map);
        System.out.println("Value for key 2: " + map.get(2));

        // Removing a key
        map.remove(1);
        System.out.println("After Removal: " + map);

        // Checking if a key exists
        System.out.println("Contains key 3? " + map.containsKey(3));

        // Iterating over keys
        for (Integer key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
```

**Output:**
```
LinkedHashMap: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Maintains insertion order while providing fast lookups.**

---

## **Time Complexity of `LinkedHashMap` Operations**
| Operation | **LinkedHashMap (O(1) on Average, O(n) Worst Case)** |
|-----------|------------------|
| **Insertion (`put(k, v)`)** | O(1) |
| **Deletion (`remove(k)`)** | O(1) |
| **Search (`containsKey(k)`)** | O(1) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Worst case O(n) if there are many hash collisions.**

---

## **Comparison: `LinkedHashMap` vs `HashMap` vs `TreeMap`**
| Feature | **LinkedHashMap** | **HashMap** | **TreeMap** |
|---------|----------------|-----------|----------|
| **Thread-Safe?** | ❌ No | ❌ No | ❌ No |
| **Maintains Order?** | ✅ Yes (Insertion Order) | ❌ No | ✅ Yes (Sorted Order) |
| **Performance** | ✅ Fast (`O(1)`) | ✅ Fastest (`O(1)`) | ❌ Slower (`O(log n)`) |
| **Best Use Case** | Ordered Key-Value Storage | Fastest Lookups | Sorted Key-Value Storage |

---

## **Real-Time Implementations of `LinkedHashMap`**

### **1. Maintaining Order of API Call Logs**
```java
import java.util.LinkedHashMap;

public class APILogSystem {
    public static void main(String[] args) {
        LinkedHashMap<Integer, String> apiLogs = new LinkedHashMap<>();

        apiLogs.put(1, "User Login");
        apiLogs.put(2, "Fetch User Data");
        apiLogs.put(3, "Update Profile");

        System.out.println("API Logs: " + apiLogs);
    }
}
```
🔹 **Maintains order of API calls to track them easily.**

---

### **2. Caching System using `LinkedHashMap`**
```java
import java.util.LinkedHashMap;
import java.util.Map;

public class LRUCache<K, V> extends LinkedHashMap<K, V> {
    private final int capacity;

    public LRUCache(int capacity) {
        super(capacity, 0.75f, true);
        this.capacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > capacity;
    }

    public static void main(String[] args) {
        LRUCache<Integer, String> cache = new LRUCache<>(3);

        cache.put(1, "Page A");
        cache.put(2, "Page B");
        cache.put(3, "Page C");
        cache.get(1); // Access page A
        cache.put(4, "Page D"); // Removes Page B (Least Recently Used)

        System.out.println("Cache: " + cache);
    }
}
```
**Output:**
```
Cache: {3=Page C, 1=Page A, 4=Page D}
```
🔹 **Implements Least Recently Used (LRU) caching using `LinkedHashMap`.**

---

### **3. Online Shopping Cart (Maintains Order of Items)**
```java
import java.util.LinkedHashMap;

public class ShoppingCart {
    public static void main(String[] args) {
        LinkedHashMap<String, Integer> cart = new LinkedHashMap<>();

        cart.put("Laptop", 1);
        cart.put("Mouse", 2);
        cart.put("Keyboard", 1);

        System.out.println("Shopping Cart: " + cart);
    }
}
```
🔹 **Maintains the order in which items were added to the cart.**

---

## **When to Use `LinkedHashMap`?**
| **Scenario** | **Use `LinkedHashMap`** |
|-------------|------------------|
| **Fast Lookup Needed (`O(1)`)** | ✅ Yes |
| **Preserve Insertion Order** | ✅ Yes |
| **Implementing LRU Cache** | ✅ Yes |
| **High-Speed Data Retrieval** | ✅ Yes |
| **Sorted Data Needed?** | ❌ No (Use `TreeMap`) |

---

## **Final Summary**
- **Use `LinkedHashMap` for ordered key-value storage.**
- **Use `HashMap` if order does not matter.**
- **Use `TreeMap` for automatically sorted keys.**

# What is the difference between SortedMap, NavigableMap, and TreeMap?
`SortedMap`, `NavigableMap`, and `TreeMap` are part of Java’s **`Map` interface** hierarchy and provide key-value mappings with **sorted ordering**.

---

## **Key Differences**
| Feature | **SortedMap** | **NavigableMap** | **TreeMap** |
|---------|-------------|----------------|----------|
| **Definition** | A `Map` that maintains elements in **sorted order**. | A `SortedMap` with **navigation methods** (`higher`, `lower`, `floor`, `ceiling`). | A concrete implementation of `NavigableMap` using a **balanced Red-Black Tree**. |
| **Ordering** | Maintains **natural sorting** or uses a **custom comparator**. | Maintains **natural sorting** with navigation features. | Uses a **Red-Black Tree** for **sorted storage**. |
| **Methods Available** | Basic methods for ordering. | Additional **navigation methods** like `floor()`, `ceiling()`, `higher()`, `lower()`. | Implements all methods from `SortedMap` and `NavigableMap`. |
| **Implementation Class** | **`TreeMap`** | **`TreeMap`** | **Red-Black Tree Implementation** |
| **Null Keys** | ❌ Not allowed in natural order. | ❌ Not allowed in natural order. | ❌ Not allowed in natural order. |

---

## **Hierarchy of `SortedMap`, `NavigableMap`, and `TreeMap`**
```
        Map
         │
     SortedMap
         │
    NavigableMap
         │
      TreeMap
```
- **`SortedMap`**: Maintains elements in sorted order.
- **`NavigableMap`**: Extends `SortedMap`, adding methods for **navigation**.
- **`TreeMap`**: Implements `NavigableMap` using a **balanced Red-Black Tree**.

---

## **Methods in `SortedMap`, `NavigableMap`, and `TreeMap`**
### **Methods in `SortedMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`firstKey()`** | Returns the **first (lowest) key**. | `map.firstKey();` |
| **`lastKey()`** | Returns the **last (highest) key**. | `map.lastKey();` |
| **`headMap(K toKey)`** | Returns key-value pairs **less than `toKey`**. | `map.headMap(50);` |
| **`tailMap(K fromKey)`** | Returns key-value pairs **greater than or equal to `fromKey`**. | `map.tailMap(20);` |
| **`subMap(K fromKey, K toKey)`** | Returns key-value pairs in the range **[`fromKey`, `toKey`)**. | `map.subMap(10, 50);` |

---

### **Additional Methods in `NavigableMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`lowerKey(K key)`** | Returns the greatest key **less than `key`**. | `map.lowerKey(40);` |
| **`floorKey(K key)`** | Returns the greatest key **less than or equal to `key`**. | `map.floorKey(40);` |
| **`higherKey(K key)`** | Returns the smallest key **greater than `key`**. | `map.higherKey(40);` |
| **`ceilingKey(K key)`** | Returns the smallest key **greater than or equal to `key`**. | `map.ceilingKey(40);` |
| **`descendingMap()`** | Returns a **reverse view** of the map. | `map.descendingMap();` |

---

## **Example: Using `TreeMap` with `SortedMap` Methods**
```java
import java.util.SortedMap;
import java.util.TreeMap;

public class SortedMapExample {
    public static void main(String[] args) {
        SortedMap<Integer, String> map = new TreeMap<>();

        // Adding key-value pairs
        map.put(10, "Apple");
        map.put(30, "Banana");
        map.put(50, "Cherry");
        map.put(70, "Date");

        System.out.println("SortedMap: " + map);
        System.out.println("First Key: " + map.firstKey());
        System.out.println("Last Key: " + map.lastKey());
        System.out.println("HeadMap(50): " + map.headMap(50));
        System.out.println("TailMap(30): " + map.tailMap(30));
        System.out.println("SubMap(10, 50): " + map.subMap(10, 50));
    }
}
```

**Output:**
```
SortedMap: {10=Apple, 30=Banana, 50=Cherry, 70=Date}
First Key: 10
Last Key: 70
HeadMap(50): {10=Apple, 30=Banana}
TailMap(30): {30=Banana, 50=Cherry, 70=Date}
SubMap(10, 50): {10=Apple, 30=Banana}
```

---

## **Example: Using `TreeMap` with `NavigableMap` Methods**
```java
import java.util.NavigableMap;
import java.util.TreeMap;

public class NavigableMapExample {
    public static void main(String[] args) {
        NavigableMap<Integer, String> map = new TreeMap<>();

        map.put(10, "Apple");
        map.put(30, "Banana");
        map.put(50, "Cherry");
        map.put(70, "Date");

        System.out.println("NavigableMap: " + map);
        System.out.println("Lower Key (40): " + map.lowerKey(40));
        System.out.println("Floor Key (50): " + map.floorKey(50));
        System.out.println("Higher Key (40): " + map.higherKey(40));
        System.out.println("Ceiling Key (50): " + map.ceilingKey(50));
        System.out.println("DescendingMap: " + map.descendingMap());
    }
}
```

**Output:**
```
NavigableMap: {10=Apple, 30=Banana, 50=Cherry, 70=Date}
Lower Key (40): 30
Floor Key (50): 50
Higher Key (40): 50
Ceiling Key (50): 50
DescendingMap: {70=Date, 50=Cherry, 30=Banana, 10=Apple}
```

---

## **Time Complexity of Operations**
| Operation | **TreeMap (`SortedMap`, `NavigableMap`)** |
|-----------|------------------|
| **Insertion (`put(k, v)`)** | O(log n) |
| **Deletion (`remove(k)`)** | O(log n) |
| **Search (`containsKey(k)`)** | O(log n) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Uses a Red-Black Tree (Self-Balancing Binary Search Tree).**

---

## **Real-World Applications**
### **1. Storing Sorted Product Prices (`TreeMap`)**
```java
import java.util.TreeMap;

public class ProductPrices {
    public static void main(String[] args) {
        TreeMap<Double, String> prices = new TreeMap<>();

        prices.put(199.99, "Laptop");
        prices.put(99.99, "Mouse");
        prices.put(299.99, "Monitor");

        System.out.println("Product Prices (Sorted): " + prices);
    }
}
```
🔹 **Keeps product prices sorted and unique.**

---

## **Final Summary**
| Feature | **SortedMap** | **NavigableMap** | **TreeMap** |
|---------|-------------|----------------|----------|
| **Sorted?** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Navigation Methods?** | ❌ No | ✅ Yes | ✅ Yes |
| **Self-Balancing?** | ❌ No | ❌ No | ✅ Yes (Red-Black Tree) |
| **Best For?** | Sorted Unique Data | Navigable Sorted Data | Ordered Unique Data with Fast Operations |

---

## **Conclusion**
- **Use `SortedMap` for basic sorted key-value storage.**
- **Use `NavigableMap` for enhanced searching and navigation.**
- **Use `TreeMap` for sorted, self-balancing key-value storage.**

# What is ConcurrentHashMap?
A **`ConcurrentHashMap`** in Java is a **thread-safe, high-performance** implementation of the `Map` interface. Unlike `Hashtable`, which synchronizes the entire map, `ConcurrentHashMap` **uses segment-based locking (or bucket locking)** to allow **concurrent read/write operations**.

---

## **Key Features of `ConcurrentHashMap`**
✅ **Thread-Safe Without Synchronized Blocks**  
✅ **Faster Reads Compared to `Hashtable`**  
✅ **Does Not Allow `null` Keys or `null` Values**  
✅ **Segmented Locking (Improves Performance in Multi-Threading)**  
✅ **Iterators Do Not Throw `ConcurrentModificationException`**  
❌ **Slightly Slower than `HashMap` in Single-Threaded Environments**  

---

## **Hierarchy of `ConcurrentHashMap`**
```
        Map
         │
  ConcurrentMap
         │
ConcurrentHashMap
```
- **Implements `ConcurrentMap<K, V>`**  
- **Uses Segmented Locking (Multiple Locks Instead of One Global Lock)**  
- **Thread-Safe Alternative to `HashMap`**  

---

## **Methods in `ConcurrentHashMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`put(K key, V value)`** | Inserts a key-value pair. | `map.put(1, "Alice");` |
| **`get(K key)`** | Retrieves the value associated with the key. | `map.get(1);` |
| **`remove(K key)`** | Removes the mapping for the key. | `map.remove(1);` |
| **`containsKey(K key)`** | Checks if a key exists. | `map.containsKey(2);` |
| **`containsValue(V value)`** | Checks if a value exists. | `map.containsValue("Alice");` |
| **`size()`** | Returns the number of key-value pairs. | `map.size();` |
| **`isEmpty()`** | Checks if the map is empty. | `map.isEmpty();` |
| **`clear()`** | Removes all key-value pairs. | `map.clear();` |
| **`keySet()`** | Returns a set of all keys. | `Set<K> keys = map.keySet();` |
| **`values()`** | Returns a collection of all values. | `Collection<V> values = map.values();` |
| **`entrySet()`** | Returns a set of key-value pairs. | `Set<Map.Entry<K,V>> entries = map.entrySet();` |
| **`compute(K key, BiFunction<K, V, V> remappingFunction)`** | Computes a new value for the key. | `map.compute(1, (k, v) -> v + " Updated");` |
| **`putIfAbsent(K key, V value)`** | Adds a key-value pair only if the key is absent. | `map.putIfAbsent(4, "David");` |

---

## **Example: Basic Usage of `ConcurrentHashMap`**
```java
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();

        // Adding key-value pairs
        map.put(1, "Alice");
        map.put(2, "Bob");
        map.put(3, "Charlie");

        System.out.println("ConcurrentHashMap: " + map);
        System.out.println("Value for key 2: " + map.get(2));

        // Removing a key
        map.remove(1);
        System.out.println("After Removal: " + map);

        // Checking if a key exists
        System.out.println("Contains key 3? " + map.containsKey(3));

        // Iterating over keys
        for (Integer key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
```

**Output:**
```
ConcurrentHashMap: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Thread-safe without global locks, allows concurrent access.**

---

## **Time Complexity of `ConcurrentHashMap` Operations**
| Operation | **ConcurrentHashMap (O(1) on Average, O(n) Worst Case)** |
|-----------|------------------|
| **Insertion (`put(k, v)`)** | O(1) |
| **Deletion (`remove(k)`)** | O(1) |
| **Search (`containsKey(k)`)** | O(1) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Worst case O(n) if there are too many hash collisions.**

---

## **Comparison: `ConcurrentHashMap` vs `HashMap` vs `Hashtable`**
| Feature | **ConcurrentHashMap** | **HashMap** | **Hashtable** |
|---------|----------------|-----------|----------|
| **Thread-Safe?** | ✅ Yes | ❌ No | ✅ Yes |
| **Allows `null` Keys?** | ❌ No | ✅ Yes | ❌ No |
| **Allows `null` Values?** | ❌ No | ✅ Yes | ❌ No |
| **Performance** | ✅ Fastest in Multi-Threading | ✅ Fastest in Single-Threading | ❌ Slow (Global Lock) |
| **Best Use Case** | High-Concurrency Systems | General Use | Legacy Thread-Safety |

---

## **Real-Time Implementations of `ConcurrentHashMap`**

### **1. Online Banking System for Account Balances (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class BankSystem {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, Double> accounts = new ConcurrentHashMap<>();

        accounts.put(1001, 5000.00);
        accounts.put(1002, 10000.50);

        System.out.println("Account 1002 Balance: $" + accounts.get(1002));

        // Checking if an account exists
        if (accounts.containsKey(1003)) {
            System.out.println("Account 1003 exists.");
        } else {
            System.out.println("Account 1003 does not exist.");
        }
    }
}
```
🔹 **Ensures secure account balance retrieval in a multi-threaded system.**

---

### **2. Real-Time Flight Seat Booking (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class FlightBookingSystem {
    private static ConcurrentHashMap<Integer, String> seats = new ConcurrentHashMap<>();

    public static void bookSeat(int seatNumber, String passenger) {
        if (seats.putIfAbsent(seatNumber, passenger) == null) {
            System.out.println(passenger + " booked seat " + seatNumber);
        } else {
            System.out.println("Seat " + seatNumber + " is already booked.");
        }
    }

    public static void main(String[] args) {
        bookSeat(1, "Alice");
        bookSeat(2, "Bob");
        bookSeat(1, "Charlie"); // Should fail

        System.out.println("Final Seat Booking: " + seats);
    }
}
```
🔹 **Prevents duplicate seat bookings in flights.**

---

### **3. Real-Time User Authentication (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class AuthenticationService {
    private static ConcurrentHashMap<String, String> users = new ConcurrentHashMap<>();

    public static void registerUser(String username, String password) {
        if (users.putIfAbsent(username, password) == null) {
            System.out.println(username + " registered successfully.");
        } else {
            System.out.println(username + " already exists.");
        }
    }

    public static void main(String[] args) {
        registerUser("Alice", "pass123");
        registerUser("Bob", "secure456");
        registerUser("Alice", "newpass"); // Should fail

        System.out.println("Registered Users: " + users);
    }
}
```
🔹 **Ensures unique usernames in a multi-threaded login system.**

---

## **When to Use `ConcurrentHashMap`?**
| **Scenario** | **Use `ConcurrentHashMap`** |
|-------------|------------------|
| **Thread-Safe Key-Value Storage?** | ✅ Yes |
| **Fast Lookup Needed (`O(1)`)** | ✅ Yes |
| **Allows `null` Keys/Values?** | ❌ No |
| **Concurrent Operations?** | ✅ Yes (Better than `Hashtable`) |

---

## **Final Summary**
- **Use `ConcurrentHashMap` for high-speed, concurrent key-value storage.**
- **Use `HashMap` if synchronization is not needed.**
- **Use `Hashtable` for legacy thread safety (but `ConcurrentHashMap` is better).**

# What is ConcurrentSkipListMap?
# **`ConcurrentSkipListMap` in Java**
`ConcurrentSkipListMap` is a **thread-safe, sorted map** that implements **`NavigableMap`** and **`ConcurrentMap`** using a **skip list** instead of a tree or hash table. It is part of `java.util.concurrent` and is an alternative to `TreeMap` for concurrent applications.

---

## **Key Features of `ConcurrentSkipListMap`**
✅ **Thread-Safe and Non-Blocking (No Locks Required)**  
✅ **Maintains Sorted Order of Keys (Like `TreeMap`)**  
✅ **Faster Reads and Writes Compared to `TreeMap` in Multi-Threading**  
✅ **Logarithmic Time Complexity (`O(log n)`) for Most Operations**  
✅ **Supports `NavigableMap` Features (`lowerKey()`, `higherKey()`, etc.)**  
❌ **Higher Memory Usage (Skip List Structure)**  
❌ **Slower than `ConcurrentHashMap` for Lookups**  

---

## **Hierarchy of `ConcurrentSkipListMap`**
```
        Map
         │
  ConcurrentMap
         │
   NavigableMap
         │
ConcurrentSkipListMap
```
- **Implements `NavigableMap<K, V>`**  
- **Implements `ConcurrentMap<K, V>`**  
- **Uses a Skip List Instead of a Tree or Hash Table**  

---

## **Methods in `ConcurrentSkipListMap`**
### **Methods from `NavigableMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`firstKey()`** | Returns the **first (lowest) key**. | `map.firstKey();` |
| **`lastKey()`** | Returns the **last (highest) key**. | `map.lastKey();` |
| **`headMap(K toKey, boolean inclusive)`** | Returns key-value pairs **less than `toKey`**. | `map.headMap(50, true);` |
| **`tailMap(K fromKey, boolean inclusive)`** | Returns key-value pairs **greater than or equal to `fromKey`**. | `map.tailMap(20, false);` |
| **`subMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive)`** | Returns key-value pairs in the range **[`fromKey`, `toKey`]**. | `map.subMap(10, true, 50, false);` |

---

### **Additional Methods from `ConcurrentMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`putIfAbsent(K key, V value)`** | Adds a key-value pair only if the key is absent. | `map.putIfAbsent(4, "David");` |
| **`compute(K key, BiFunction<K, V, V> remappingFunction)`** | Computes a new value for the key. | `map.compute(1, (k, v) -> v + " Updated");` |
| **`replace(K key, V oldValue, V newValue)`** | Replaces the key’s value only if it matches `oldValue`. | `map.replace(3, "Old", "New");` |

---

## **Example: Basic Usage of `ConcurrentSkipListMap`**
```java
import java.util.concurrent.ConcurrentSkipListMap;

public class ConcurrentSkipListMapExample {
    public static void main(String[] args) {
        ConcurrentSkipListMap<Integer, String> map = new ConcurrentSkipListMap<>();

        // Adding key-value pairs
        map.put(10, "Apple");
        map.put(30, "Banana");
        map.put(50, "Cherry");
        map.put(70, "Date");

        System.out.println("ConcurrentSkipListMap: " + map);
        System.out.println("First Key: " + map.firstKey());
        System.out.println("Last Key: " + map.lastKey());
        System.out.println("HeadMap(50): " + map.headMap(50, true));
        System.out.println("TailMap(30): " + map.tailMap(30, false));
        System.out.println("SubMap(10, 50): " + map.subMap(10, true, 50, false));
    }
}
```

**Output:**
```
ConcurrentSkipListMap: {10=Apple, 30=Banana, 50=Cherry, 70=Date}
First Key: 10
Last Key: 70
HeadMap(50): {10=Apple, 30=Banana, 50=Cherry}
TailMap(30): {50=Cherry, 70=Date}
SubMap(10, 50): {10=Apple, 30=Banana}
```

---

## **Time Complexity of `ConcurrentSkipListMap` Operations**
| Operation | **ConcurrentSkipListMap (`O(log n)`)** |
|-----------|------------------|
| **Insertion (`put(k, v)`)** | O(log n) |
| **Deletion (`remove(k)`)** | O(log n) |
| **Search (`containsKey(k)`)** | O(log n) |
| **Iteration (`iterator()`)** | O(n) |

🔹 **Uses a Skip List (Layered Linked Lists) for Logarithmic Performance.**

---

## **Comparison: `ConcurrentSkipListMap` vs `TreeMap` vs `ConcurrentHashMap`**
| Feature | **ConcurrentSkipListMap** | **TreeMap** | **ConcurrentHashMap** |
|---------|----------------|---------|------------------|
| **Thread-Safe?** | ✅ Yes | ❌ No | ✅ Yes |
| **Sorted Order?** | ✅ Yes | ✅ Yes | ❌ No |
| **Performance** | ✅ Faster in Multi-Threading | ❌ Slower in Multi-Threading | ✅ Fastest Lookups |
| **Best Use Case** | Multi-Threaded Sorted Data | Single-Threaded Sorted Data | Fastest Multi-Threaded Lookups |

---

## **Real-Time Implementations of `ConcurrentSkipListMap`**

### **1. Storing Sorted Stock Prices (Real-Time Data)**
```java
import java.util.concurrent.ConcurrentSkipListMap;

public class StockPriceMonitor {
    public static void main(String[] args) {
        ConcurrentSkipListMap<Double, String> stockPrices = new ConcurrentSkipListMap<>();

        stockPrices.put(150.5, "AAPL");
        stockPrices.put(152.3, "GOOGL");
        stockPrices.put(148.7, "MSFT");
        stockPrices.put(153.2, "TSLA");

        System.out.println("Stock Prices: " + stockPrices);
    }
}
```
🔹 **Ensures stock prices remain sorted and accessible concurrently.**

---

### **2. Live Auction System (Sorted Bidding System)**
```java
import java.util.concurrent.ConcurrentSkipListMap;

public class AuctionSystem {
    public static void main(String[] args) {
        ConcurrentSkipListMap<Integer, String> bids = new ConcurrentSkipListMap<>();

        bids.put(100, "Alice");
        bids.put(200, "Bob");
        bids.put(150, "Charlie");

        System.out.println("Highest Bidder: " + bids.lastEntry());
    }
}
```
🔹 **Ensures the highest bid is always at the top.**

---

### **3. Online User Session Tracking (Real-Time Sorted Sessions)**
```java
import java.util.concurrent.ConcurrentSkipListMap;

public class UserSessionTracking {
    public static void main(String[] args) {
        ConcurrentSkipListMap<Long, String> userSessions = new ConcurrentSkipListMap<>();

        userSessions.put(System.currentTimeMillis(), "User1");
        userSessions.put(System.currentTimeMillis() + 1000, "User2");

        System.out.println("User Sessions: " + userSessions);
    }
}
```
🔹 **Ensures user sessions are ordered based on login time.**

---

## **When to Use `ConcurrentSkipListMap`?**
| **Scenario** | **Use `ConcurrentSkipListMap`** |
|-------------|------------------|
| **Thread-Safe Sorted Key-Value Storage?** | ✅ Yes |
| **Faster Alternative to `TreeMap` in Multi-Threading?** | ✅ Yes |
| **Logarithmic Time Complexity (`O(log n)`)** | ✅ Yes |
| **Sorted Data Needed?** | ✅ Yes |
| **Fastest Concurrent Read/Write Performance?** | ❌ No (Use `ConcurrentHashMap`) |

---

## **Final Summary**
- **Use `ConcurrentSkipListMap` for sorted key-value storage in concurrent environments.**
- **Use `TreeMap` if multi-threading is not required.**
- **Use `ConcurrentHashMap` for unordered, fastest lookups.**

# What is the difference between EnumMap, IdentityHashMap, and WeakHashMap?

Java provides specialized implementations of the `Map` interface for specific use cases. `EnumMap`, `IdentityHashMap`, and `WeakHashMap` have unique properties that make them useful in different scenarios.

---

## **Key Differences**
| Feature | **EnumMap** | **IdentityHashMap** | **WeakHashMap** |
|---------|-------------|----------------|------------|
| **Definition** | A high-performance `Map` designed for `enum` keys. | Uses **reference equality (`==`)** instead of `equals()`. | Uses **weak references** for keys, allowing garbage collection. |
| **Key Comparison** | Compares `enum` keys using their **ordinal values** (faster than `HashMap`). | Compares keys using **`==` (Reference Equality)** instead of `equals()`. | Keys are **weak references**, automatically removed if unreferenced elsewhere. |
| **Null Keys Allowed?** | ❌ No | ✅ Yes | ✅ Yes |
| **Null Values Allowed?** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Thread-Safe?** | ❌ No | ❌ No | ❌ No |
| **Ordering** | Maintains the natural order of `enum` keys. | No specific order. | No specific order. |
| **Use Case** | Best for **mapping enum constants**. | Best for **identity-based key comparison** (not value-based). | Best for **caching, preventing memory leaks**. |

---

## **Hierarchy of `EnumMap`, `IdentityHashMap`, and `WeakHashMap`**
```
        Map
         │
 ┌──────┼───────────┐
 EnumMap IdentityHashMap WeakHashMap
```
- **`EnumMap<K extends Enum<K>, V>`** → Optimized for `enum` keys.
- **`IdentityHashMap<K, V>`** → Uses reference equality (`==`) instead of `equals()`.
- **`WeakHashMap<K, V>`** → Uses weak references, allowing garbage collection of unused keys.

---

## **Methods in `EnumMap`, `IdentityHashMap`, and `WeakHashMap`**
| Method | Description | Example Usage |
|--------|------------|--------------|
| **`put(K key, V value)`** | Inserts a key-value pair. | `map.put(Day.MONDAY, "Workday");` |
| **`get(K key)`** | Retrieves the value for the given key. | `map.get(Day.MONDAY);` |
| **`remove(K key)`** | Removes a key-value pair. | `map.remove(Day.FRIDAY);` |
| **`containsKey(K key)`** | Checks if the key exists. | `map.containsKey(Day.SUNDAY);` |
| **`containsValue(V value)`** | Checks if a value exists. | `map.containsValue("Holiday");` |
| **`size()`** | Returns the number of key-value pairs. | `map.size();` |
| **`clear()`** | Removes all entries. | `map.clear();` |
| **`keySet()`** | Returns a set of all keys. | `Set<K> keys = map.keySet();` |
| **`values()`** | Returns a collection of all values. | `Collection<V> values = map.values();` |
| **`entrySet()`** | Returns a set of key-value pairs. | `Set<Map.Entry<K,V>> entries = map.entrySet();` |

---

## **Example: Using `EnumMap`**
```java
import java.util.EnumMap;

enum Day {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

public class EnumMapExample {
    public static void main(String[] args) {
        EnumMap<Day, String> schedule = new EnumMap<>(Day.class);

        schedule.put(Day.MONDAY, "Workday");
        schedule.put(Day.SATURDAY, "Weekend");
        schedule.put(Day.SUNDAY, "Rest Day");

        System.out.println("EnumMap: " + schedule);
    }
}
```
**Output:**
```
EnumMap: {MONDAY=Workday, SATURDAY=Weekend, SUNDAY=Rest Day}
```
🔹 **Faster than `HashMap` for `enum` keys.**

---

## **Example: Using `IdentityHashMap`**
```java
import java.util.IdentityHashMap;

public class IdentityHashMapExample {
    public static void main(String[] args) {
        IdentityHashMap<String, String> map = new IdentityHashMap<>();

        String key1 = new String("Hello");
        String key2 = new String("Hello");

        map.put(key1, "Value1");
        map.put(key2, "Value2");

        System.out.println("IdentityHashMap: " + map);
    }
}
```
**Output:**
```
IdentityHashMap: {Hello=Value1, Hello=Value2}
```
🔹 **Two different `String` objects are treated as separate keys.**

---

## **Example: Using `WeakHashMap`**
```java
import java.util.WeakHashMap;

public class WeakHashMapExample {
    public static void main(String[] args) {
        WeakHashMap<Object, String> map = new WeakHashMap<>();

        Object key1 = new Object();
        Object key2 = new Object();

        map.put(key1, "Value1");
        map.put(key2, "Value2");

        key1 = null; // Make key1 eligible for garbage collection

        System.gc(); // Request Garbage Collection

        System.out.println("WeakHashMap: " + map);
    }
}
```
🔹 **After garbage collection, the `key1` entry may be removed automatically.**

---

## **Time Complexity Comparison**
| Operation | **EnumMap (`O(1)`)** | **IdentityHashMap (`O(1)`)** | **WeakHashMap (`O(1)`)** |
|-----------|------------------|------------------|------------------|
| **Insertion (`put(k, v)`)** | O(1) | O(1) | O(1) |
| **Deletion (`remove(k)`)** | O(1) | O(1) | O(1) |
| **Search (`containsKey(k)`)** | O(1) | O(1) | O(1) |
| **Iteration (`iterator()`)** | O(n) | O(n) | O(n) |

🔹 **`WeakHashMap` keys may be automatically removed if unreferenced.**

---

## **Comparison Table: `EnumMap` vs `IdentityHashMap` vs `WeakHashMap`**
| Feature | **EnumMap** | **IdentityHashMap** | **WeakHashMap** |
|---------|-------------|----------------|------------|
| **Performance** | ✅ Fastest (`O(1)`) | ✅ Fast (`O(1)`) | ✅ Fast (`O(1)`) |
| **Best Use Case** | Mapping `enum` constants. | When **reference equality (`==`)** is needed. | When keys should be **garbage collected automatically**. |
| **Null Key Allowed?** | ❌ No | ✅ Yes | ✅ Yes |
| **Null Value Allowed?** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Thread-Safe?** | ❌ No | ❌ No | ❌ No |
| **Ordering** | **Enum Order** | No Order | No Order |

---

## **When to Use Which Map?**
| **Scenario** | **Use `EnumMap`** | **Use `IdentityHashMap`** | **Use `WeakHashMap`** |
|-------------|------------------|------------------|------------------|
| **Fastest Lookup for Enum Keys** | ✅ Yes | ❌ No | ❌ No |
| **Identity-Based Key Comparison (`==` instead of `equals()`)** | ❌ No | ✅ Yes | ❌ No |
| **Garbage Collection of Unused Keys (Memory Optimization)** | ❌ No | ❌ No | ✅ Yes |
| **Best For** | Enum-Based Data Structures | Reference-Based Caching | Automatic Memory Cleanup |

---

## **Final Summary**
- **Use `EnumMap` for efficient mapping of `enum` keys.**
- **Use `IdentityHashMap` when key uniqueness should be based on reference equality (`==`).**
- **Use `WeakHashMap` when keys should be automatically garbage collected.**

# Explain the internal working of LinkedList?
Java's **`LinkedList`** is a **doubly linked list** implementation of the `List` and `Deque` interfaces. Unlike an `ArrayList`, which uses a dynamic array, `LinkedList` consists of **nodes** that store data and references to the **next** and **previous** nodes.

---

## **Internal Structure of `LinkedList`**
Each element (node) in a `LinkedList` consists of:
- **`data`** → Stores the actual value.
- **`next`** → Points to the next node in the list.
- **`prev`** → Points to the previous node in the list.

```
Head → [data | prev=null | next] ↔ [data | prev | next] ↔ [data | prev | next=null] ← Tail
```
- The **first node (head)** has `prev = null`.
- The **last node (tail)** has `next = null`.

---

## **How `LinkedList` Works Internally?**
### **1. Adding Elements (`add(E e)`)**
- If the list is empty, the new node becomes both `head` and `tail`.
- Otherwise, the new node is added after the `tail`, and the `tail` pointer is updated.

**Time Complexity:**  
- `add(E e)` → **O(1)** (Appending at the end)  
- `add(int index, E e)` → **O(n)** (Inserting at a specific index)

---

### **2. Removing Elements (`remove(E e)`)**
- If removing from the head: `head = head.next` (O(1))
- If removing from the tail: `tail = tail.prev` (O(1))
- If removing from the middle: The list is traversed to find the node (`O(n)`)

**Time Complexity:**  
- `remove(E e)` → **O(n)** (Searching for an element)  
- `removeFirst()` / `removeLast()` → **O(1)**

---

### **3. Searching Elements (`contains(E e)`)**
- The list is traversed from `head` to `tail` to find the element.

**Time Complexity:**  
- **O(n)** (No direct index access like `ArrayList`)

---

### **4. Accessing Elements (`get(int index)`)**
- Starts from `head` and traverses **`index` times**.

**Time Complexity:**  
- **O(n)** (Slower than `ArrayList`'s `O(1)` random access)

---

### **5. Iteration**
- Uses `Iterator` or `ListIterator` to traverse in both directions.

**Time Complexity:**  
- **O(n)** for full traversal.

---

## **Example: Basic Usage of `LinkedList`**
```java
import java.util.LinkedList;

public class LinkedListExample {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();

        // Adding elements
        list.add("Alice");
        list.add("Bob");
        list.add("Charlie");

        System.out.println("LinkedList: " + list);

        // Adding at a specific index
        list.add(1, "David");

        // Removing an element
        list.remove("Charlie");

        // Accessing an element
        System.out.println("First Element: " + list.getFirst());

        // Iterating
        for (String name : list) {
            System.out.println(name);
        }
    }
}
```
**Output:**
```
LinkedList: [Alice, Bob, Charlie]
First Element: Alice
Alice
David
Bob
```
🔹 **Maintains order, but slower access than `ArrayList`.**

---

## **Time Complexity of `LinkedList` Operations**
| Operation | **LinkedList (`O(n)`)** | **ArrayList (`O(1)`)** |
|-----------|------------------|------------------|
| **Insertion (`add(e)`)** | ✅ O(1) (At end) | ✅ O(1) (At end) |
| **Insertion (`add(index, e)`)** | ❌ O(n) | ✅ O(n) |
| **Deletion (`remove(e)`)** | ❌ O(n) | ✅ O(n) |
| **Search (`contains(e)`)** | ❌ O(n) | ✅ O(1) |
| **Access (`get(index)`)** | ❌ O(n) | ✅ O(1) |

🔹 **Use `LinkedList` if frequent insertions/deletions are required.**  
🔹 **Use `ArrayList` for fast index-based access.**

---

## **Real-World Implementations of `LinkedList`**

### **1. Implementing a Playlist (`LinkedList`)**
```java
import java.util.LinkedList;

public class MusicPlaylist {
    public static void main(String[] args) {
        LinkedList<String> playlist = new LinkedList<>();

        playlist.add("Song A");
        playlist.add("Song B");
        playlist.add("Song C");

        System.out.println("Current Playlist: " + playlist);

        // Removing the first song (played)
        playlist.removeFirst();

        System.out.println("After playing first song: " + playlist);
    }
}
```
🔹 **Efficiently removes the first played song using `removeFirst()`.**

---

### **2. Browser Back & Forward Navigation (`Deque` Implementation)**
```java
import java.util.LinkedList;
import java.util.Deque;

public class BrowserHistory {
    public static void main(String[] args) {
        Deque<String> history = new LinkedList<>();

        // User visits pages
        history.push("Google.com");
        history.push("StackOverflow.com");
        history.push("GitHub.com");

        System.out.println("Current Page: " + history.peek());

        // Back navigation
        history.pop();
        System.out.println("Back to: " + history.peek());
    }
}
```
🔹 **Simulates back navigation using `push()` (adding) and `pop()` (removing).**

---

### **3. Implementing a Queue Using `LinkedList`**
```java
import java.util.LinkedList;
import java.util.Queue;

public class CustomerServiceQueue {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();

        queue.add("Customer 1");
        queue.add("Customer 2");
        queue.add("Customer 3");

        System.out.println("First Customer: " + queue.peek());

        // Serve the first customer
        queue.poll();

        System.out.println("Queue after serving: " + queue);
    }
}
```
🔹 **Useful for customer service queues where FIFO order is needed.**

---

### **4. Undo/Redo Functionality (`LinkedList` as Stack)**
```java
import java.util.LinkedList;

public class UndoRedo {
    public static void main(String[] args) {
        LinkedList<String> history = new LinkedList<>();

        history.push("Typed: Hello");
        history.push("Typed: World");
        history.push("Deleted: World");

        System.out.println("Undo: " + history.pop()); // Undo last action
        System.out.println("Current History: " + history);
    }
}
```
🔹 **Simulates undo/redo functionality using `push()` and `pop()`.**

---

## **When to Use `LinkedList`?**
| **Scenario** | **Use `LinkedList`?** |
|-------------|------------------|
| **Frequent Insertions/Deletions?** | ✅ Yes |
| **Fast Access to Middle Elements?** | ❌ No |
| **FIFO (Queue Implementation)?** | ✅ Yes |
| **LIFO (Stack Implementation)?** | ✅ Yes |
| **Fast Search/Random Access?** | ❌ No (Use `ArrayList`) |

---

## **Final Summary**
- **Use `LinkedList` when you need frequent insertions/deletions.**
- **Use `ArrayList` for fast random access.**
- **`LinkedList` is useful for queues, stacks, and history-based applications.**

# Explain the internal working of HashMap?
A **`HashMap`** in Java is a key-value-based data structure that provides **fast lookups, insertions, and deletions** using a **hashing mechanism**. It is part of the **Java Collection Framework** and is implemented using **arrays and linked lists (or trees in some cases).**

---

## **Internal Structure of `HashMap`**
A `HashMap` uses an **array of buckets**, where each bucket stores a **linked list** (or a tree if the number of collisions exceeds a threshold).  
Each entry in the `HashMap` consists of:
1. **Key** → Unique identifier for a value.
2. **Value** → The actual data associated with the key.
3. **Hash Code** → Generated by `hashCode()` method to determine the bucket.
4. **Next (Linked Node or Tree Node)** → Pointer to the next element in case of collisions.

### **Internal Representation**
```
HashMap<K, V>
 ┌──────────────┬──────────────┬──────────────┬──────────────┐
 │ Bucket 0     │ Bucket 1     │ Bucket 2     │ Bucket 3     │
 ├──────────────┼──────────────┼──────────────┼──────────────┤
 │ NULL         │ (K1,V1) ───▶ │ (K2,V2) ───▶ │ (K3,V3)      │
 │              │ (collision)  │ (collision)  │              │
 └──────────────┴──────────────┴──────────────┴──────────────┘
```

### **Steps to Store a Key-Value Pair**
1. **Compute the Hash**  
   - `hashCode()` method generates an integer hash code.
   - Example: `"Alice".hashCode() = 6328190`
   
2. **Compute the Bucket Index**  
   - Uses `(hashCode & (n - 1))` formula where `n` is the bucket size (usually power of 2).  
   - Example: `6328190 % 16 = 6` → Entry is stored in bucket index `6`.

3. **Handle Collisions (If Needed)**  
   - If multiple keys map to the same bucket, a **Linked List (Java 7)** or **Balanced Tree (Java 8)** is used.

---

## **How `HashMap` Works Internally?**
### **1. Adding Elements (`put(K key, V value)`)**
- **Step 1**: Compute the hash code.
- **Step 2**: Determine the bucket index.
- **Step 3**: If no collision, store the key-value pair.
- **Step 4**: If a collision occurs:
  - **Java 7**: Uses a **linked list** (appends at the end).
  - **Java 8+**: If more than `8` elements, converts the linked list to a **Red-Black Tree** for faster lookups.

---

### **2. Retrieving Elements (`get(K key)`)**
- **Step 1**: Compute the hash code of the key.
- **Step 2**: Locate the bucket index.
- **Step 3**: Traverse the bucket (if needed) and find the key using `equals()`.

**Time Complexity:**
- **Best Case (`O(1)`)** → Direct access with unique keys.
- **Worst Case (`O(log n)`)** → If a bucket has a tree instead of a list.

---

### **3. Handling Collisions**
**Collision occurs when two keys have the same hash index.**  
- **Java 7** → Uses **Linked List** (slower retrieval in case of many collisions).  
- **Java 8+** → If **more than 8 nodes**, converts to a **Balanced Tree** (`O(log n)`).  

---

### **4. Resizing Mechanism**
When the number of elements exceeds **75% of the capacity (`load factor = 0.75`)**, `HashMap`:
1. **Doubles its size** (e.g., from `16` to `32` buckets).
2. **Rehashes** existing elements to new positions.

**Example:**
```
Old Capacity: 16   →  Resized Capacity: 32
Entry Rehashing: Each key's hash is recomputed for the new bucket index.
```

---

### **5. Removing Elements (`remove(K key)`)**
- **Step 1**: Compute hash.
- **Step 2**: Locate the bucket.
- **Step 3**: If multiple elements exist, traverse the bucket and remove the correct entry.

---

## **Example: Basic Usage of `HashMap`**
```java
import java.util.HashMap;

public class HashMapExample {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        // Adding key-value pairs
        map.put(1, "Alice");
        map.put(2, "Bob");
        map.put(3, "Charlie");

        System.out.println("HashMap: " + map);
        System.out.println("Value for key 2: " + map.get(2));

        // Removing a key
        map.remove(1);
        System.out.println("After Removal: " + map);

        // Checking if a key exists
        System.out.println("Contains key 3? " + map.containsKey(3));

        // Iterating over keys
        for (Integer key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
```
**Output:**
```
HashMap: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Fast lookups and modifications with `HashMap`.**

---

## **Time Complexity of `HashMap` Operations**
| Operation | **Best Case (`O(1)`)** | **Worst Case (`O(log n)`)** |
|-----------|------------------|------------------|
| **Insertion (`put(k, v)`)** | ✅ O(1) | ❌ O(log n) |
| **Deletion (`remove(k)`)** | ✅ O(1) | ❌ O(log n) |
| **Search (`get(k)`)** | ✅ O(1) | ❌ O(log n) |

🔹 **Worst case happens when all keys collide in the same bucket.**

---

## **Real-World Implementations of `HashMap`**

### **1. Employee Directory (`HashMap`)**
```java
import java.util.HashMap;

public class EmployeeDirectory {
    public static void main(String[] args) {
        HashMap<Integer, String> employees = new HashMap<>();

        employees.put(101, "Alice");
        employees.put(102, "Bob");
        employees.put(103, "Charlie");

        System.out.println("Employee Directory: " + employees);
    }
}
```
🔹 **Quick lookup for employee names using IDs.**

---

### **2. URL Shortener (Key-Value Mapping)**
```java
import java.util.HashMap;

public class URLShortener {
    public static void main(String[] args) {
        HashMap<String, String> urlMap = new HashMap<>();

        urlMap.put("abc123", "https://www.google.com");
        urlMap.put("xyz789", "https://www.github.com");

        System.out.println("Original URL: " + urlMap.get("abc123"));
    }
}
```
🔹 **Stores short URLs mapped to full URLs for quick access.**

---

### **3. Caching System (`HashMap`)**
```java
import java.util.HashMap;

public class CacheSystem {
    public static void main(String[] args) {
        HashMap<Integer, String> cache = new HashMap<>();

        cache.put(1, "Data A");
        cache.put(2, "Data B");

        System.out.println("Cache Data: " + cache);
    }
}
```
🔹 **Caches frequently accessed data for quick retrieval.**

---

## **When to Use `HashMap`?**
| **Scenario** | **Use `HashMap`?** |
|-------------|------------------|
| **Fast Lookups (`O(1)`)** | ✅ Yes |
| **Key-Value Pair Storage** | ✅ Yes |
| **Thread-Safe?** | ❌ No (Use `ConcurrentHashMap`) |
| **Needs Ordered Data?** | ❌ No (Use `LinkedHashMap`) |
| **Needs Sorted Data?** | ❌ No (Use `TreeMap`) |

---

## **Final Summary**
- **Use `HashMap` for fast, unordered key-value storage.**
- **Use `LinkedHashMap` if insertion order must be preserved.**
- **Use `TreeMap` for automatically sorted keys.**

# Explain the internal working of ConcurrentHashMap?
A **`ConcurrentHashMap`** in Java is a **thread-safe, high-performance** implementation of the `Map` interface. Unlike `HashMap`, which is not synchronized, and `Hashtable`, which locks the entire map, `ConcurrentHashMap` **uses segment-based locking (or bucket-level locking)** to allow **concurrent read and write operations**.

---

## **Internal Structure of `ConcurrentHashMap`**
- **`ConcurrentHashMap` uses a bucket-based structure where each bucket (a node) can be modified independently.**
- **Instead of locking the entire map, it locks only specific buckets (segments) when modifying them.**
- This approach improves performance in **multi-threaded environments**.

### **Key Components**
1. **Buckets** → Each bucket stores key-value pairs.
2. **Hash Function** → Computes the bucket index for a key.
3. **Node** → Stores key-value pairs inside a bucket.
4. **Segment Locking** → Each bucket has its own lock for thread safety.
5. **Tree-based Buckets** → If a bucket contains more than **8 nodes**, it converts into a **Red-Black Tree** for efficient searching.

---

## **How `ConcurrentHashMap` Works Internally?**
### **1. Adding Elements (`put(K key, V value)`)**
1. **Compute the hash code** of the key.
2. **Determine the bucket index**.
3. **If the bucket is empty**, place the key-value pair in the bucket.
4. **If the bucket is occupied**, check:
   - If **no collisions**, store the new key-value pair.
   - If **collision occurs**, traverse the linked list or tree in the bucket.
   - If more than **8 nodes exist**, convert the linked list to a **Red-Black Tree** for efficiency.
5. **Locking Mechanism**: Only the affected bucket is locked, not the entire map.

---

### **2. Retrieving Elements (`get(K key)`)**
1. **Compute the hash code**.
2. **Determine the bucket index**.
3. **Search in the bucket** (either linked list or tree).
4. **Read operations are non-blocking** → No lock is required for `get()`.

**Time Complexity:**
- **Best Case (`O(1)`)** → Direct access.
- **Worst Case (`O(log n)`)** → If a tree structure exists.

---

### **3. Handling Collisions**
- If multiple keys map to the same bucket, `ConcurrentHashMap`:
  - Uses a **linked list** (before Java 8).
  - Converts to a **Red-Black Tree** (if bucket size exceeds 8).

---

### **4. Resizing Mechanism**
When the number of elements **exceeds 75% of the capacity**, `ConcurrentHashMap`:
1. **Doubles the capacity** (e.g., from `16` to `32` buckets).
2. **Uses multiple threads** to rehash existing elements (faster than `HashMap`).

---

### **5. Removing Elements (`remove(K key)`)**
1. **Compute hash**.
2. **Locate bucket**.
3. **Traverse bucket** and remove the correct entry.
4. **Bucket Locking** ensures that only one thread can modify it.

---

## **Example: Basic Usage of `ConcurrentHashMap`**
```java
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();

        // Adding key-value pairs
        map.put(1, "Alice");
        map.put(2, "Bob");
        map.put(3, "Charlie");

        System.out.println("ConcurrentHashMap: " + map);
        System.out.println("Value for key 2: " + map.get(2));

        // Removing a key
        map.remove(1);
        System.out.println("After Removal: " + map);

        // Checking if a key exists
        System.out.println("Contains key 3? " + map.containsKey(3));

        // Iterating over keys
        for (Integer key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
```

**Output:**
```
ConcurrentHashMap: {1=Alice, 2=Bob, 3=Charlie}
Value for key 2: Bob
After Removal: {2=Bob, 3=Charlie}
Contains key 3? true
Key: 2, Value: Bob
Key: 3, Value: Charlie
```
🔹 **Thread-safe without blocking other threads, allows concurrent access.**

---

## **Time Complexity of `ConcurrentHashMap` Operations**
| Operation | **Best Case (`O(1)`)** | **Worst Case (`O(log n)`)** |
|-----------|------------------|------------------|
| **Insertion (`put(k, v)`)** | ✅ O(1) | ❌ O(log n) |
| **Deletion (`remove(k)`)** | ✅ O(1) | ❌ O(log n) |
| **Search (`get(k)`)** | ✅ O(1) | ❌ O(log n) |

🔹 **Worst case happens when all keys collide in the same bucket, leading to a tree-based search.**

---

## **Comparison: `ConcurrentHashMap` vs `HashMap` vs `Hashtable`**
| Feature | **ConcurrentHashMap** | **HashMap** | **Hashtable** |
|---------|----------------|-----------|----------|
| **Thread-Safe?** | ✅ Yes | ❌ No | ✅ Yes |
| **Allows `null` Keys?** | ❌ No | ✅ Yes | ❌ No |
| **Allows `null` Values?** | ❌ No | ✅ Yes | ❌ No |
| **Performance** | ✅ Fastest in Multi-Threading | ✅ Fastest in Single-Threading | ❌ Slow (Global Lock) |
| **Best Use Case** | High-Concurrency Systems | General Use | Legacy Thread-Safety |

---

## **Real-Time Implementations of `ConcurrentHashMap`**

### **1. Online Banking System for Account Balances (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class BankSystem {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, Double> accounts = new ConcurrentHashMap<>();

        accounts.put(1001, 5000.00);
        accounts.put(1002, 10000.50);

        System.out.println("Account 1002 Balance: $" + accounts.get(1002));

        // Checking if an account exists
        if (accounts.containsKey(1003)) {
            System.out.println("Account 1003 exists.");
        } else {
            System.out.println("Account 1003 does not exist.");
        }
    }
}
```
🔹 **Ensures secure account balance retrieval in a multi-threaded system.**

---

### **2. Real-Time Flight Seat Booking (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class FlightBookingSystem {
    private static ConcurrentHashMap<Integer, String> seats = new ConcurrentHashMap<>();

    public static void bookSeat(int seatNumber, String passenger) {
        if (seats.putIfAbsent(seatNumber, passenger) == null) {
            System.out.println(passenger + " booked seat " + seatNumber);
        } else {
            System.out.println("Seat " + seatNumber + " is already booked.");
        }
    }

    public static void main(String[] args) {
        bookSeat(1, "Alice");
        bookSeat(2, "Bob");
        bookSeat(1, "Charlie"); // Should fail

        System.out.println("Final Seat Booking: " + seats);
    }
}
```
🔹 **Prevents duplicate seat bookings in flights.**

---

### **3. Real-Time User Authentication (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class AuthenticationService {
    private static ConcurrentHashMap<String, String> users = new ConcurrentHashMap<>();

    public static void registerUser(String username, String password) {
        if (users.putIfAbsent(username, password) == null) {
            System.out.println(username + " registered successfully.");
        } else {
            System.out.println(username + " already exists.");
        }
    }

    public static void main(String[] args) {
        registerUser("Alice", "pass123");
        registerUser("Bob", "secure456");
        registerUser("Alice", "newpass"); // Should fail

        System.out.println("Registered Users: " + users);
    }
}
```
🔹 **Ensures unique usernames in a multi-threaded login system.**

---

## **Final Summary**
- **Use `ConcurrentHashMap` for high-speed, concurrent key-value storage.**
- **Use `HashMap` if synchronization is not needed.**
- **Use `Hashtable` for legacy thread safety (but `ConcurrentHashMap` is better).**

# Explain the internal working of TreeMap?
### **Internal Working of TreeMap in Java**
`TreeMap` in Java is a part of the `java.util` package and implements the `NavigableMap` interface, which extends `SortedMap`. It is a Red-Black tree-based implementation of a `Map`, which means it maintains the keys in sorted order. The sorting is based on natural ordering (`Comparable`) or a custom `Comparator` provided at the time of initialization.

### **Internal Data Structure**
TreeMap is implemented using a **Red-Black Tree**, a self-balancing binary search tree (BST). This ensures that all operations have a logarithmic time complexity (`O(log n)`) for insertion, deletion, and search.

- **Red-Black Tree Properties**:
  1. Every node is either red or black.
  2. The root node is always black.
  3. Red nodes cannot have red children (no two consecutive red nodes).
  4. Every path from a node to its descendant NULL nodes has the same number of black nodes.
  5. A new node is always inserted as red.

### **Constructors of TreeMap**
1. **TreeMap()** – Default constructor (natural ordering of keys).
2. **TreeMap(Comparator<? super K> comparator)** – Orders elements using a custom comparator.
3. **TreeMap(Map<? extends K, ? extends V> m)** – Constructs a map with entries from another `Map`.
4. **TreeMap(SortedMap<K, ? extends V> m)** – Constructs a map with entries from another `SortedMap`.

---

## **Operations in TreeMap**
TreeMap supports various operations like insertion, deletion, searching, and navigation operations. Below is a detailed explanation of all major operations.

### **1. Insertion (`put(K key, V value)`)**
**Steps:**
1. Start at the root and compare the key to be inserted with the current node's key.
2. If the key is smaller, move to the left subtree; if larger, move to the right subtree.
3. Insert the node at the correct position and color it red.
4. Balance the Red-Black Tree by performing rotations and recoloring.

**Example:**
```java
import java.util.*;

public class TreeMapExample {
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();

        // Inserting elements
        treeMap.put(5, "Five");
        treeMap.put(2, "Two");
        treeMap.put(8, "Eight");
        treeMap.put(1, "One");
        treeMap.put(3, "Three");

        System.out.println(treeMap);
    }
}
```
**Output:**
```
{1=One, 2=Two, 3=Three, 5=Five, 8=Eight}
```
*Insertion maintains the sorted order based on keys.*

---

### **2. Deletion (`remove(K key)`)**
**Steps:**
1. Find the node to be deleted.
2. If the node has:
   - **No children:** Simply remove it.
   - **One child:** Replace it with its child.
   - **Two children:** Replace it with the in-order successor, then delete the successor.
3. Fix the Red-Black Tree property violations by performing rotations and recoloring.

**Example:**
```java
treeMap.remove(2);
System.out.println(treeMap);
```
**Output:**
```
{1=One, 3=Three, 5=Five, 8=Eight}
```
---

### **3. Searching (`get(K key)`)**
TreeMap performs a **binary search** using the Red-Black tree.

**Example:**
```java
System.out.println(treeMap.get(5)); // Output: Five
System.out.println(treeMap.get(10)); // Output: null
```
Time Complexity: **O(log n)**

---

### **4. Traversal**
**Methods to traverse the TreeMap:**
1. **EntrySet Traversal**
```java
for (Map.Entry<Integer, String> entry : treeMap.entrySet()) {
    System.out.println(entry.getKey() + " => " + entry.getValue());
}
```
2. **KeySet Traversal**
```java
for (Integer key : treeMap.keySet()) {
    System.out.println(key);
}
```
3. **Values Traversal**
```java
for (String value : treeMap.values()) {
    System.out.println(value);
}
```

---

### **5. First and Last Entry**
- `firstEntry()` → Returns the first (smallest) key-value pair.
- `lastEntry()` → Returns the last (largest) key-value pair.

**Example:**
```java
System.out.println(treeMap.firstEntry()); // Output: 1=One
System.out.println(treeMap.lastEntry()); // Output: 8=Eight
```

---

### **6. Floor, Ceiling, Higher, and Lower Key**
- `floorKey(k)`: Greatest key ≤ k
- `ceilingKey(k)`: Smallest key ≥ k
- `higherKey(k)`: Smallest key > k
- `lowerKey(k)`: Greatest key < k

**Example:**
```java
System.out.println(treeMap.floorKey(4));  // Output: 3
System.out.println(treeMap.ceilingKey(4)); // Output: 5
System.out.println(treeMap.higherKey(5));  // Output: 8
System.out.println(treeMap.lowerKey(5));   // Output: 3
```

---

### **7. Polling First and Last Entry**
- `pollFirstEntry()`: Removes and returns the first (smallest) entry.
- `pollLastEntry()`: Removes and returns the last (largest) entry.

**Example:**
```java
System.out.println(treeMap.pollFirstEntry()); // Output: 1=One
System.out.println(treeMap.pollLastEntry());  // Output: 8=Eight
```

---

### **8. Submaps (Subrange Views)**
- `subMap(fromKey, toKey)`: Returns a submap between `fromKey` (inclusive) and `toKey` (exclusive).
- `headMap(toKey)`: Returns all keys < `toKey`.
- `tailMap(fromKey)`: Returns all keys ≥ `fromKey`.

**Example:**
```java
System.out.println(treeMap.subMap(2, 5)); // Output: {2=Two, 3=Three}
System.out.println(treeMap.headMap(5)); // Output: {1=One, 2=Two, 3=Three}
System.out.println(treeMap.tailMap(3)); // Output: {3=Three, 5=Five, 8=Eight}
```

---

## **Time Complexity Analysis**
| Operation   | Time Complexity |
|-------------|---------------|
| Insertion   | O(log n)      |
| Deletion    | O(log n)      |
| Search      | O(log n)      |
| Traversal   | O(n)          |

---

## **Conclusion**
- `TreeMap` maintains elements in **sorted order**.
- It is implemented using a **Red-Black Tree**, ensuring logarithmic time complexity for most operations.
- It is useful for scenarios where **sorted data retrieval** and **range-based queries** are needed.

Would you like a more in-depth explanation of a specific operation? 🚀

# What is a Resizable Array?
### **What is a Resizable Array?**
A **Resizable Array** is an array-like data structure that can dynamically increase or decrease its size as elements are added or removed. Unlike a **fixed-size array**, which has a predefined length that cannot change, a resizable array automatically adjusts its capacity as needed.

### **Key Characteristics of a Resizable Array:**
1. **Dynamic Growth**: When elements are added beyond its current capacity, a larger array is allocated, and existing elements are copied over.
2. **Efficient Access**: Like arrays, a resizable array provides **O(1)** time complexity for element access.
3. **Amortized Doubling Strategy**: To optimize performance, when an array resizes, its capacity typically doubles.
4. **Automatic Shrinking**: Some implementations shrink when elements are removed to save memory.

---

## **Implementation of Resizable Arrays in Java**
Java provides a built-in implementation of a resizable array through **`ArrayList`**, which is part of the `java.util` package.

### **Internal Working of `ArrayList` (Java’s Resizable Array)**
1. `ArrayList` internally uses an **array** to store elements.
2. When the array gets **full**, a **new array** with **double the size** is created, and elements are copied over.
3. Removing elements **does not immediately shrink** the array, but in some implementations, it may resize when many elements are removed.

### **Constructors of `ArrayList`**
- `ArrayList()` → Creates an empty list with an initial capacity of 10.
- `ArrayList(int capacity)` → Creates an empty list with a specified initial capacity.
- `ArrayList(Collection<? extends E> c)` → Creates a list initialized with elements from another collection.

---

## **Operations on Resizable Array (`ArrayList`)**
### **1. Creating a Resizable Array**
```java
import java.util.*;

public class ResizableArrayExample {
    public static void main(String[] args) {
        // Creating an ArrayList
        ArrayList<Integer> numbers = new ArrayList<>();

        // Adding elements
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);

        System.out.println(numbers);  // Output: [10, 20, 30]
    }
}
```
### **Explanation:**
- The `ArrayList` initially has a default capacity of **10**.
- When elements are added, they are stored in an **internal array**.
- When more elements are added beyond its current capacity, it **automatically resizes**.

---

### **2. Capacity Expansion**
By default, when an `ArrayList` reaches its current capacity, it increases in size using the formula:
```java
newCapacity = oldCapacity * 1.5
```

#### **Example of Automatic Resizing**
```java
import java.util.*;

public class ArrayListResizing {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(2); // Initial capacity is 2

        // Adding elements beyond initial capacity
        list.add(1);
        list.add(2);
        list.add(3);  // Causes resizing

        System.out.println(list); // Output: [1, 2, 3]
    }
}
```
### **Explanation:**
1. Initially, capacity is **2**.
2. The third element **cannot fit**, so a **new array (of increased size) is created** and elements are copied over.

---

### **3. Accessing Elements (O(1) Time Complexity)**
Elements can be accessed in **constant time** using `get(index)`.
```java
System.out.println(numbers.get(1)); // Output: 20
```

---

### **4. Insertion at a Specific Index**
```java
numbers.add(1, 15); // Insert 15 at index 1
System.out.println(numbers); // Output: [10, 15, 20, 30]
```
### **Explanation:**
- Elements are shifted to make space for the new element.
- **Time Complexity: O(n)** due to shifting.

---

### **5. Updating Elements**
```java
numbers.set(2, 25); // Change index 2 value to 25
System.out.println(numbers); // Output: [10, 15, 25, 30]
```

---

### **6. Removing Elements**
```java
numbers.remove(1); // Removes the element at index 1
System.out.println(numbers); // Output: [10, 25, 30]
```
### **Explanation:**
- **Shifting occurs** after removal.
- **Time Complexity: O(n)** for shifting.

---

### **7. Resizing Downward (Trimming to Size)**
When many elements are removed, memory is wasted. The `trimToSize()` method helps reduce memory usage.
```java
numbers.trimToSize();
```
**This reduces the internal capacity to match the current number of elements.**

---

### **8. Searching Elements (`contains()`)**
```java
System.out.println(numbers.contains(25)); // Output: true
System.out.println(numbers.contains(100)); // Output: false
```
**Time Complexity: O(n) in worst case.**

---

### **9. Iterating Over Elements**
#### **Using `for-each` loop**
```java
for (int num : numbers) {
    System.out.println(num);
}
```
#### **Using Iterator**
```java
Iterator<Integer> it = numbers.iterator();
while (it.hasNext()) {
    System.out.println(it.next());
}
```

---

### **10. Sorting Elements**
```java
Collections.sort(numbers);
System.out.println(numbers);
```
**Time Complexity: O(n log n) using TimSort.**

---

## **Comparison of Resizable Array (`ArrayList`) vs. Fixed Array**
| Feature           | Fixed Array (`int[]`) | Resizable Array (`ArrayList`) |
|------------------|------------------|------------------|
| Size            | Fixed at creation | Grows dynamically |
| Performance     | Fast (O(1) access) | Slightly slower (resizing overhead) |
| Memory Usage   | Efficient | Uses extra memory when resized |
| Insertion      | O(n) for shifting | O(n) for shifting |
| Deletion       | O(n) for shifting | O(n) for shifting |
| Flexibility    | Low | High |

---

## **Custom Implementation of Resizable Array**
If you want to implement your own version of `ArrayList`, here is how:

```java
class MyArrayList {
    private int[] arr;
    private int size;
    private int capacity;

    public MyArrayList() {
        capacity = 10;
        arr = new int[capacity];
        size = 0;
    }

    private void resize() {
        capacity *= 2;
        int[] newArr = new int[capacity];
        System.arraycopy(arr, 0, newArr, 0, size);
        arr = newArr;
    }

    public void add(int value) {
        if (size == capacity) {
            resize();
        }
        arr[size++] = value;
    }

    public int get(int index) {
        if (index >= size) throw new IndexOutOfBoundsException();
        return arr[index];
    }

    public void remove(int index) {
        if (index >= size) throw new IndexOutOfBoundsException();
        for (int i = index; i < size - 1; i++) {
            arr[i] = arr[i + 1];
        }
        size--;
    }

    public int size() {
        return size;
    }
}

public class Test {
    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();
        list.add(10);
        list.add(20);
        list.add(30);
        System.out.println(list.get(1)); // Output: 20
        list.remove(1);
        System.out.println(list.get(1)); // Output: 30
    }
}
```
---

## **Conclusion**
- **Resizable Arrays** provide a **flexible** alternative to fixed-size arrays.
- **Java’s `ArrayList`** is an optimized implementation with **automatic resizing**.
- Understanding **how resizing works** helps in **memory-efficient programming**.

Would you like an example in another language like Python (`list` or `numpy`)? 🚀

# What is the Skip List Data Structure?
# **Skip List Data Structure in Java**
A **Skip List** is a probabilistic data structure that allows **fast search, insertion, and deletion** operations similar to a balanced tree but without the need for complex rotations. It is an alternative to **balanced search trees** like AVL or Red-Black trees.

### **Key Features of Skip List**
- It is a **linked list-based** data structure.
- It uses **multiple layers** of linked lists to optimize search times.
- Search, insert, and delete operations have **O(log n) expected time complexity**.
- It uses **randomization** to determine the levels of nodes.

---

## **1. How Does a Skip List Work?**
A **Skip List** consists of multiple levels of **linked lists**. The lowest level contains **all elements**, while higher levels contain **fewer elements** (acting as "express lanes" for faster searching).

- **Each node contains**:
  - A key (or value).
  - Multiple forward pointers (to nodes at different levels).

- **Skip List Hierarchy:**
  - **Level 0 (Bottom Level)** → Contains all elements.
  - **Level 1** → Contains some elements (every 2nd element on average).
  - **Level 2** → Contains fewer elements (every 4th element on average), and so on.

- **Randomization in Levels:** Each node is assigned a **random height**, which determines how many levels it appears in.

### **Example Skip List Structure**
```
Level 3:  [ 1 ] -------- [ 10 ] -------- [ 50 ]
Level 2:  [ 1 ] ---- [ 5 ] ---- [ 10 ] -- [ 25 ] -- [ 50 ]
Level 1:  [ 1 ] -- [ 2 ] -- [ 5 ] -- [ 7 ] -- [ 10 ] -- [ 25 ] -- [ 50 ]
Level 0:  [ 1 ] -- [ 2 ] -- [ 3 ] -- [ 4 ] -- [ 5 ] -- [ 6 ] -- [ 7 ] -- [ 8 ] -- [ 9 ] -- [ 10 ] -- [ 25 ] -- [ 50 ]
```
---

## **2. Time Complexity**
| Operation   | Average Case | Worst Case |
|-------------|-------------|------------|
| Search      | **O(log n)**  | **O(n)** (rare) |
| Insertion   | **O(log n)**  | **O(n)** (rare) |
| Deletion    | **O(log n)**  | **O(n)** (rare) |

---

## **3. Implementation of Skip List in Java**
### **Step 1: Node Structure**
Each node contains:
- The **key (value)**.
- An array of **forward pointers** (pointing to nodes in different levels).

```java
import java.util.Random;

class SkipListNode {
    int key;
    SkipListNode[] forward;

    public SkipListNode(int key, int level) {
        this.key = key;
        this.forward = new SkipListNode[level + 1]; // Array for different levels
    }
}
```

---

### **Step 2: Skip List Structure**
The `SkipList` class contains:
- `MAX_LEVEL` → The maximum height of the skip list.
- `head` → Dummy head node.
- `level` → The highest level of the skip list.

```java
class SkipList {
    private static final int MAX_LEVEL = 5;
    private SkipListNode head;
    private int level;
    private Random random;

    public SkipList() {
        this.level = 0;
        this.head = new SkipListNode(-1, MAX_LEVEL); // Dummy head node
        this.random = new Random();
    }

    private int randomLevel() {
        int lvl = 0;
        while (random.nextBoolean() && lvl < MAX_LEVEL) {
            lvl++;
        }
        return lvl;
    }
```
- `randomLevel()` generates a random height for new nodes.

---

### **Step 3: Insertion Operation**
**Algorithm:**
1. Start from the highest level and move forward until finding the correct position.
2. Insert the new node at a **random level**.
3. Update the forward pointers.

```java
    public void insert(int key) {
        SkipListNode[] update = new SkipListNode[MAX_LEVEL + 1];
        SkipListNode current = head;

        // Find the position to insert the new key
        for (int i = level; i >= 0; i--) {
            while (current.forward[i] != null && current.forward[i].key < key) {
                current = current.forward[i];
            }
            update[i] = current;
        }

        // Generate random level for new node
        int newLevel = randomLevel();

        // If new node's level is greater than current level, update head
        if (newLevel > level) {
            for (int i = level + 1; i <= newLevel; i++) {
                update[i] = head;
            }
            level = newLevel;
        }

        // Create new node and insert it
        SkipListNode newNode = new SkipListNode(key, newLevel);
        for (int i = 0; i <= newLevel; i++) {
            newNode.forward[i] = update[i].forward[i];
            update[i].forward[i] = newNode;
        }
        System.out.println("Inserted: " + key);
    }
```

---

### **Step 4: Search Operation**
**Algorithm:**
1. Start from the highest level.
2. Traverse forward pointers until the key is found.

```java
    public boolean search(int key) {
        SkipListNode current = head;

        for (int i = level; i >= 0; i--) {
            while (current.forward[i] != null && current.forward[i].key < key) {
                current = current.forward[i];
            }
        }

        current = current.forward[0];
        return current != null && current.key == key;
    }
```

---

### **Step 5: Deletion Operation**
**Algorithm:**
1. Find the node using the **update array**.
2. Remove forward pointers to exclude the node.

```java
    public void delete(int key) {
        SkipListNode[] update = new SkipListNode[MAX_LEVEL + 1];
        SkipListNode current = head;

        for (int i = level; i >= 0; i--) {
            while (current.forward[i] != null && current.forward[i].key < key) {
                current = current.forward[i];
            }
            update[i] = current;
        }

        current = current.forward[0];

        if (current != null && current.key == key) {
            for (int i = 0; i <= level; i++) {
                if (update[i].forward[i] != current) break;
                update[i].forward[i] = current.forward[i];
            }

            while (level > 0 && head.forward[level] == null) {
                level--;
            }

            System.out.println("Deleted: " + key);
        }
    }
```

---

### **Step 6: Displaying the Skip List**
```java
    public void display() {
        for (int i = level; i >= 0; i--) {
            SkipListNode node = head.forward[i];
            System.out.print("Level " + i + ": ");
            while (node != null) {
                System.out.print(node.key + " -> ");
                node = node.forward[i];
            }
            System.out.println("NULL");
        }
    }
```

---

## **6. Testing the Skip List**
```java
public class SkipListDemo {
    public static void main(String[] args) {
        SkipList skipList = new SkipList();

        skipList.insert(3);
        skipList.insert(6);
        skipList.insert(7);
        skipList.insert(9);
        skipList.insert(12);
        skipList.insert(19);

        skipList.display();

        System.out.println("Search 9: " + skipList.search(9));
        skipList.delete(7);
        skipList.display();
    }
}
```

---

## **Conclusion**
- **Skip List is an alternative to balanced trees** with **logarithmic** search and insert operations.
- It uses **randomization** for structure, making it **simpler than AVL or Red-Black trees**.
- **Practical applications** include **databases, in-memory storage, and distributed systems**.

Would you like an explanation of **real-world applications** or **comparisons with BSTs**? 🚀

# What is the difference between Single and Doubly LinkedList?
A **linked list** is a linear data structure in which elements are stored in nodes that are connected using **pointers**. Each node contains:
1. **Data** – The actual value stored in the node.
2. **Pointer (Reference)** – A reference (or pointer) to the next node in the list.

---

## **1. Singly Linked List (SLL)**
A **Singly Linked List (SLL)** consists of nodes where **each node points to the next node**, forming a **one-directional chain**.

### **Node Structure of SLL**
Each node contains:
- `data` (stores value).
- `next` (stores reference to the next node).

```java
class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
        this.next = null;  // Initially, next is null
    }
}
```

---

### **Operations in Singly Linked List**
#### **1. Insert at the Beginning**
```java
public void insertAtBeginning(int data) {
    Node newNode = new Node(data);
    newNode.next = head;
    head = newNode;
}
```

#### **2. Insert at the End**
```java
public void insertAtEnd(int data) {
    Node newNode = new Node(data);
    if (head == null) {
        head = newNode;
        return;
    }
    Node temp = head;
    while (temp.next != null) {
        temp = temp.next;
    }
    temp.next = newNode;
}
```

#### **3. Delete a Node**
```java
public void delete(int key) {
    Node temp = head, prev = null;
    
    if (temp != null && temp.data == key) {
        head = temp.next;
        return;
    }

    while (temp != null && temp.data != key) {
        prev = temp;
        temp = temp.next;
    }

    if (temp == null) return;
    prev.next = temp.next;
}
```

#### **4. Search for an Element**
```java
public boolean search(int key) {
    Node temp = head;
    while (temp != null) {
        if (temp.data == key) return true;
        temp = temp.next;
    }
    return false;
}
```

#### **5. Display Linked List**
```java
public void display() {
    Node temp = head;
    while (temp != null) {
        System.out.print(temp.data + " -> ");
        temp = temp.next;
    }
    System.out.println("NULL");
}
```

---

### **Full Implementation of Singly Linked List**
```java
class SinglyLinkedList {
    Node head;

    // Insert at the beginning
    public void insertAtBeginning(int data) {
        Node newNode = new Node(data);
        newNode.next = head;
        head = newNode;
    }

    // Insert at the end
    public void insertAtEnd(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
            return;
        }
        Node temp = head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = newNode;
    }

    // Delete a node
    public void delete(int key) {
        Node temp = head, prev = null;

        if (temp != null && temp.data == key) {
            head = temp.next;
            return;
        }

        while (temp != null && temp.data != key) {
            prev = temp;
            temp = temp.next;
        }

        if (temp == null) return;
        prev.next = temp.next;
    }

    // Search a node
    public boolean search(int key) {
        Node temp = head;
        while (temp != null) {
            if (temp.data == key) return true;
            temp = temp.next;
        }
        return false;
    }

    // Display linked list
    public void display() {
        Node temp = head;
        while (temp != null) {
            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }
        System.out.println("NULL");
    }

    public static void main(String[] args) {
        SinglyLinkedList sll = new SinglyLinkedList();
        sll.insertAtBeginning(10);
        sll.insertAtBeginning(20);
        sll.insertAtEnd(30);
        sll.insertAtEnd(40);
        sll.display(); // Output: 20 -> 10 -> 30 -> 40 -> NULL
        sll.delete(10);
        sll.display(); // Output: 20 -> 30 -> 40 -> NULL
        System.out.println("Search 30: " + sll.search(30)); // Output: true
    }
}
```

---

## **2. Doubly Linked List (DLL)**
A **Doubly Linked List (DLL)** contains nodes where **each node has two pointers**:
- `prev` – Points to the **previous node**.
- `next` – Points to the **next node**.

### **Node Structure of DLL**
```java
class DNode {
    int data;
    DNode prev;
    DNode next;

    public DNode(int data) {
        this.data = data;
        this.prev = null;
        this.next = null;
    }
}
```

---

### **Operations in Doubly Linked List**
#### **1. Insert at the Beginning**
```java
public void insertAtBeginning(int data) {
    DNode newNode = new DNode(data);
    if (head == null) {
        head = newNode;
        return;
    }
    newNode.next = head;
    head.prev = newNode;
    head = newNode;
}
```

#### **2. Insert at the End**
```java
public void insertAtEnd(int data) {
    DNode newNode = new DNode(data);
    if (head == null) {
        head = newNode;
        return;
    }
    DNode temp = head;
    while (temp.next != null) {
        temp = temp.next;
    }
    temp.next = newNode;
    newNode.prev = temp;
}
```

#### **3. Delete a Node**
```java
public void delete(int key) {
    DNode temp = head;

    if (temp != null && temp.data == key) {
        head = temp.next;
        if (head != null) head.prev = null;
        return;
    }

    while (temp != null && temp.data != key) {
        temp = temp.next;
    }

    if (temp == null) return;

    if (temp.next != null) temp.next.prev = temp.prev;
    if (temp.prev != null) temp.prev.next = temp.next;
}
```

#### **4. Display DLL Forward and Backward**
```java
public void displayForward() {
    DNode temp = head;
    while (temp != null) {
        System.out.print(temp.data + " <-> ");
        temp = temp.next;
    }
    System.out.println("NULL");
}

public void displayBackward() {
    DNode temp = head;
    if (temp == null) return;
    while (temp.next != null) temp = temp.next;

    while (temp != null) {
        System.out.print(temp.data + " <-> ");
        temp = temp.prev;
    }
    System.out.println("NULL");
}
```

---

### **Full Implementation of Doubly Linked List**
```java
class DoublyLinkedList {
    DNode head;

    public void insertAtBeginning(int data) { /* Implementation above */ }
    public void insertAtEnd(int data) { /* Implementation above */ }
    public void delete(int key) { /* Implementation above */ }
    public void displayForward() { /* Implementation above */ }
    public void displayBackward() { /* Implementation above */ }

    public static void main(String[] args) {
        DoublyLinkedList dll = new DoublyLinkedList();
        dll.insertAtBeginning(10);
        dll.insertAtEnd(20);
        dll.insertAtEnd(30);
        dll.displayForward(); // Output: 10 <-> 20 <-> 30 <-> NULL
        dll.displayBackward(); // Output: 30 <-> 20 <-> 10 <-> NULL
        dll.delete(20);
        dll.displayForward(); // Output: 10 <-> 30 <-> NULL
    }
}
```

---

## **Conclusion**
| Feature | Singly Linked List | Doubly Linked List |
|---------|------------------|------------------|
| Direction | One-way | Two-way |
| Memory Usage | Less (single pointer) | More (extra prev pointer) |
| Search Time | O(n) | O(n) |
| Insert/Delete at Head | O(1) | O(1) |
| Insert/Delete at Tail | O(n) | O(1) |


# What is Hashing/Re-hashing?
Hashing is a technique used to **map data to a fixed-size representation** using a hash function. It is widely used in **data structures** like **HashMap, HashSet**, and **HashTable** in Java.

---

## **1. What is Hashing?**
Hashing is a **process of mapping a key to an index** in an array or table using a **hash function**.

### **Key Components of Hashing**
1. **Hash Function**: Converts a key into an index.
2. **Hash Table**: A data structure (array) used to store key-value pairs.
3. **Collision Handling**: Techniques to resolve conflicts when multiple keys map to the same index.

### **Example of Hashing**
A hash function converts keys into indices:

| Key  | Hash Function (`key % 10`) | Index |
|------|--------------------|-------|
| 25   | `25 % 10 = 5`     | 5     |
| 40   | `40 % 10 = 0`     | 0     |
| 35   | `35 % 10 = 5`     | 5 (Collision) |

---

## **2. Hash Function in Java**
A **good hash function** should:
- **Be fast to compute**.
- **Distribute keys uniformly**.
- **Minimize collisions**.

Example:
```java
public int hashFunction(int key, int tableSize) {
    return key % tableSize;
}
```

---

## **3. Collision Handling Techniques**
### **1. Separate Chaining**
Each index of the hash table stores a **linked list** to handle collisions.

```java
import java.util.LinkedList;

class HashTable {
    private int size = 10;
    private LinkedList<Integer>[] table;

    public HashTable() {
        table = new LinkedList[size];
        for (int i = 0; i < size; i++) {
            table[i] = new LinkedList<>();
        }
    }

    private int hashFunction(int key) {
        return key % size;
    }

    public void insert(int key) {
        int index = hashFunction(key);
        table[index].add(key);
    }

    public boolean search(int key) {
        int index = hashFunction(key);
        return table[index].contains(key);
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(i + " => " + table[i]);
        }
    }
}

public class HashTableExample {
    public static void main(String[] args) {
        HashTable ht = new HashTable();
        ht.insert(10);
        ht.insert(20);
        ht.insert(30);
        ht.insert(25);
        ht.insert(35);

        ht.display();
        System.out.println("Search 25: " + ht.search(25)); // true
    }
}
```

---

### **2. Open Addressing**
Instead of using linked lists, open addressing finds an alternative **empty slot** for colliding elements.

#### **(a) Linear Probing**
Finds the next available slot by checking **next indices**.
```java
private int linearProbing(int key, int i) {
    return (hashFunction(key) + i) % size;
}
```

#### **(b) Quadratic Probing**
Avoids clustering by using squared increments.
```java
private int quadraticProbing(int key, int i) {
    return (hashFunction(key) + i * i) % size;
}
```

#### **(c) Double Hashing**
Uses **two hash functions** to reduce clustering.
```java
private int doubleHashing(int key, int i) {
    int h1 = key % size;
    int h2 = 7 - (key % 7);
    return (h1 + i * h2) % size;
}
```

---

## **4. What is Rehashing?**
**Rehashing** is the process of **increasing the size** of the hash table and **recomputing hash functions** for all keys.

### **Why Rehashing?**
1. When **load factor exceeds a threshold**, performance drops.
2. Helps maintain **O(1) average time complexity**.
3. Reduces **collisions**.

### **Formula for Load Factor**
\[
\text{Load Factor} = \frac{\text{Number of Elements}}{\text{Table Size}}
\]
- **Default threshold = 0.75**.
- When load factor **> 0.75**, rehashing happens.

---

### **Rehashing Example in Java**
```java
import java.util.LinkedList;

class RehashingHashTable {
    private int size;
    private LinkedList<Integer>[] table;
    private int count;

    public RehashingHashTable(int initialSize) {
        this.size = initialSize;
        this.count = 0;
        table = new LinkedList[size];
        for (int i = 0; i < size; i++) {
            table[i] = new LinkedList<>();
        }
    }

    private int hashFunction(int key) {
        return key % size;
    }

    private void rehash() {
        System.out.println("Rehashing...");
        int newSize = size * 2;
        LinkedList<Integer>[] newTable = new LinkedList[newSize];

        for (int i = 0; i < newSize; i++) {
            newTable[i] = new LinkedList<>();
        }

        for (LinkedList<Integer> bucket : table) {
            for (int key : bucket) {
                int newIndex = key % newSize;
                newTable[newIndex].add(key);
            }
        }

        size = newSize;
        table = newTable;
    }

    public void insert(int key) {
        if ((double) count / size > 0.75) {
            rehash();
        }
        int index = hashFunction(key);
        table[index].add(key);
        count++;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(i + " => " + table[i]);
        }
    }
}

public class RehashingExample {
    public static void main(String[] args) {
        RehashingHashTable hashTable = new RehashingHashTable(4);

        hashTable.insert(10);
        hashTable.insert(20);
        hashTable.insert(30);
        hashTable.insert(40); // Triggers rehashing
        hashTable.insert(50);

        hashTable.display();
    }
}
```

---

## **5. Java’s Built-in Hashing (HashMap & HashSet)**
Java provides **`HashMap`** and **`HashSet`**, which internally use **hashing**.

### **Example: HashMap (Uses Hashing)**
```java
import java.util.HashMap;

public class HashMapExample {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        // Insert elements
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");

        System.out.println(map); // Output: {1=One, 2=Two, 3=Three}

        // Search element
        System.out.println("Contains key 2? " + map.containsKey(2));

        // Remove element
        map.remove(2);
        System.out.println(map); // Output: {1=One, 3=Three}
    }
}
```

---

## **6. Comparison of Hashing Methods**
| Method | Time Complexity (Avg) | Time Complexity (Worst) | Space Complexity |
|--------|-----------------|-----------------|----------------|
| Separate Chaining | O(1) | O(n) | O(n) |
| Linear Probing | O(1) | O(n) | O(n) |
| Quadratic Probing | O(1) | O(n) | O(n) |
| Double Hashing | O(1) | O(n) | O(n) |

---

## **7. Conclusion**
✅ **Hashing is a fast technique** used in hash tables, maps, and sets.  
✅ **Rehashing improves performance** by dynamically resizing the table.  
✅ **Java's HashMap & HashSet use hashing** internally.  
✅ **Collision handling** is crucial for maintaining efficiency.


# What is Initial Capacity vs Load Factor?
When working with **HashMap, HashSet, and other Hash-based collections** in Java, two important concepts are:

1. **Initial Capacity** – The starting size of the hash table (number of buckets).
2. **Load Factor** – The threshold that determines when the hash table needs to be resized (rehashing).

---

## **1. What is Initial Capacity?**
**Initial capacity** refers to the number of **buckets** in the hash table **when it is first created**.

- In **HashMap and HashSet**, elements are stored in **buckets**, and a hash function determines which bucket a key should go into.
- By default, the **initial capacity is 16** (in Java’s `HashMap` and `HashSet`).
- If you know the **expected number of elements**, setting a good initial capacity can improve performance.

### **Example: Setting Initial Capacity in HashMap**
```java
import java.util.HashMap;

public class InitialCapacityExample {
    public static void main(String[] args) {
        // HashMap with an initial capacity of 32
        HashMap<Integer, String> map = new HashMap<>(32);
        
        map.put(1, "One");
        map.put(2, "Two");
        
        System.out.println("HashMap: " + map);
    }
}
```
**Default Initial Capacity**: If no capacity is specified, Java uses **16**.

---

## **2. What is Load Factor?**
**Load factor** is the **measure of how full the hash table can get before it resizes** (rehashing).

- Load factor is a **value between 0 and 1**.
- The default **load factor is 0.75** (75% full before resizing).
- When the **number of elements** exceeds `capacity × loadFactor`, Java **doubles the capacity** and rehashes the elements.

### **Formula for Resizing**
\[
\text{Threshold} = \text{Capacity} \times \text{Load Factor}
\]

| Initial Capacity | Load Factor | Resize Threshold |
|------------------|------------|------------------|
| 16              | 0.75       | 12 (16 × 0.75)  |
| 32              | 0.75       | 24 (32 × 0.75)  |
| 64              | 0.5        | 32 (64 × 0.5)   |

### **Example: Setting Load Factor**
```java
import java.util.HashMap;

public class LoadFactorExample {
    public static void main(String[] args) {
        // HashMap with initial capacity 16 and load factor 0.5
        HashMap<Integer, String> map = new HashMap<>(16, 0.5f);

        for (int i = 1; i <= 9; i++) {
            map.put(i, "Value " + i);
        }

        System.out.println("HashMap before threshold reached: " + map);
    }
}
```
- Since the **initial capacity is 16** and the **load factor is 0.5**, resizing occurs when we **insert the 9th element** (`16 × 0.5 = 8`).

---

## **3. Initial Capacity vs Load Factor Comparison**
| Feature            | Initial Capacity  | Load Factor |
|--------------------|------------------|------------|
| Definition        | The starting size of the hash table. | The percentage of the table that can be filled before resizing. |
| Default Value     | 16 | 0.75 |
| When it Affects Performance? | If too small, frequent resizing occurs. | If too high, collisions increase. |
| Best Usage        | If you know the number of elements beforehand, set a high initial capacity. | If memory is a concern, adjust the load factor to optimize usage. |

---

## **4. When to Adjust Initial Capacity and Load Factor?**
✅ **Use a Higher Initial Capacity** if:
- You know in advance that many elements will be stored.
- You want to avoid frequent resizing (rehashing).

✅ **Use a Lower Load Factor** if:
- You want to **reduce collisions** for better performance.
- You have **many elements but want to avoid rehashing**.

✅ **Use a Higher Load Factor** if:
- Memory usage is a concern.
- You are okay with some increase in collisions.

---

## **5. Real-world Example: Using HashMap with Custom Initial Capacity & Load Factor**
```java
import java.util.HashMap;

public class HashMapOptimization {
    public static void main(String[] args) {
        // Initial capacity 32, Load factor 0.9 (Resize happens later)
        HashMap<Integer, String> optimizedMap = new HashMap<>(32, 0.9f);

        for (int i = 1; i <= 28; i++) {
            optimizedMap.put(i, "Value " + i);
        }

        System.out.println("Optimized HashMap: " + optimizedMap);
    }
}
```
- **Initial Capacity = 32**, **Load Factor = 0.9**
- **Resize happens at 28 elements** (`32 × 0.9 = 28`).

---

## **6. Conclusion**
- **Initial Capacity** defines the **starting number of buckets**.
- **Load Factor** determines **when resizing (rehashing) happens**.
- **Optimal tuning** improves **performance** and **memory efficiency**.

Would you like a **detailed explanation of HashMap's internal working**? 🚀

# What is a Hash Collision - Chaining vs Open Addressing?
## **1. What is a Hash Collision?**
A **hash collision** occurs when two **different keys** produce the **same index** using a **hash function**.

### **Example: Collision**
Assume we use a simple hash function:
index=(hash function+i)modtable sizeindex=keymodtable size
For a hash table of **size 10**:

| Key  | Hash Function (`key % 10`) | Index |
|------|--------------------|-------|
| 25   | `25 % 10 = 5`     | 5     |
| 35   | `35 % 10 = 5`     | 5 (Collision) |

Since **both 25 and 35** map to the **same index (5)**, a **collision occurs**.

---

## **2. How to Handle Collisions?**
There are **two main methods** to handle collisions:
1. **Chaining (Separate Chaining)**
2. **Open Addressing (Probing techniques)**

---

## **3. Collision Handling Method 1: Chaining (Separate Chaining)**
**Chaining** stores multiple elements at the **same index** using a **linked list**.

### **How it Works?**
- Instead of storing a **single element** in each index, we store a **linked list of elements**.
- When a collision happens, the new element is added to the **linked list at that index**.

### **Example**
```
Index   | Data (Linked List)
---------------------------
0       | 10 → 20 → 30
1       | 
2       | 12
3       | 33 → 23
4       | 
5       | 25 → 35
```

### **Java Implementation of Chaining**
```java
import java.util.LinkedList;

class HashTable {
    private int size = 10;
    private LinkedList<Integer>[] table;

    public HashTable() {
        table = new LinkedList[size];
        for (int i = 0; i < size; i++) {
            table[i] = new LinkedList<>();
        }
    }

    private int hashFunction(int key) {
        return key % size;
    }

    public void insert(int key) {
        int index = hashFunction(key);
        table[index].add(key);
    }

    public boolean search(int key) {
        int index = hashFunction(key);
        return table[index].contains(key);
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(i + " => " + table[i]);
        }
    }
}

public class ChainingExample {
    public static void main(String[] args) {
        HashTable ht = new HashTable();
        ht.insert(10);
        ht.insert(20);
        ht.insert(30);
        ht.insert(25);
        ht.insert(35);
        ht.display();

        System.out.println("Search 25: " + ht.search(25)); // Output: true
    }
}
```

### **Advantages of Chaining**
✅ **Easy implementation** (Uses linked lists).  
✅ **Efficient in high-load factors** (More elements than buckets).  
✅ **Insertions are always O(1)** (No need to find an empty slot).  

### **Disadvantages of Chaining**
❌ **Extra space for pointers (linked list overhead).**  
❌ **Search time increases if the list gets too long (O(n) worst-case).**  

---

## **4. Collision Handling Method 2: Open Addressing**
**Open Addressing** does not use extra memory (like linked lists). Instead, it **finds another empty slot in the table** when a collision occurs.

### **Types of Open Addressing:**
1. **Linear Probing**
2. **Quadratic Probing**
3. **Double Hashing**

---

### **(a) Linear Probing**
- If the slot is **occupied**, check the **next available** slot.
- Uses:
  index=(hash function+i)modtable size
  where `i = 0, 1, 2, ...` until an empty slot is found.

### **Example of Linear Probing**
```
Insert 25 → Hash index = 5 ✅
Insert 35 → Hash index = 5 (Collision) ❌ → Next available = 6 ✅
Insert 45 → Hash index = 5 (Collision) ❌ → Next = 6 (Occupied) ❌ → Next = 7 ✅
```
| Index | Data |
|--------|-----|
| 5      | 25  |
| 6      | 35  |
| 7      | 45  |

#### **Java Implementation of Linear Probing**
```java
class LinearProbingHashTable {
    private int size = 10;
    private int[] table;

    public LinearProbingHashTable() {
        table = new int[size];
        for (int i = 0; i < size; i++) {
            table[i] = -1; // -1 indicates empty slot
        }
    }

    private int hashFunction(int key) {
        return key % size;
    }

    public void insert(int key) {
        int index = hashFunction(key);
        int i = 0;
        while (table[(index + i) % size] != -1) {
            i++;
        }
        table[(index + i) % size] = key;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.println(i + " => " + table[i]);
        }
    }
}

public class LinearProbingExample {
    public static void main(String[] args) {
        LinearProbingHashTable lht = new LinearProbingHashTable();
        lht.insert(25);
        lht.insert(35);
        lht.insert(45);
        lht.display();
    }
}
```

### **Advantages of Linear Probing**
✅ **Space-efficient** (No extra memory needed).  
✅ **Faster for small datasets** (No overhead of linked lists).  

### **Disadvantages of Linear Probing**
❌ **Clustering problem** (consecutive occupied slots slow down insertions).  
❌ **Deletion is complex** (Lazy deletion needed).  

---

### **(b) Quadratic Probing**
- Avoids clustering by jumping **quadratically** instead of linearly.
- Uses:
  \[
  \text{index} = (\text{hash function} + i^2) \mod \text{table size}
  \]

### **(c) Double Hashing**
- Uses **two hash functions** to compute new indices.
- Formula:
  \[
  \text{index} = (\text{hash1} + i \times \text{hash2}) \mod \text{table size}
  \]

---

## **5. Comparison: Chaining vs Open Addressing**
| Feature           | Chaining | Open Addressing |
|------------------|----------|----------------|
| Extra Memory    | Uses extra memory (Linked List) | No extra memory needed |
| Performance    | Fast for insertions (O(1) average) | Slower for insertions (O(n) worst case) |
| Clustering Issue | No clustering | Linear Probing has clustering |
| Search Time | O(1) (Avg), O(n) (Worst) | O(1) (Avg), O(n) (Worst) |
| Deletion Complexity | Easy | Complex (Lazy deletion required) |
| Best Used When | Large data, unpredictable size | Small, memory-constrained environments |

---

## **6. Which Method to Use?**
✅ **Use Chaining if:**
- The number of elements is **greater than the capacity**.
- **Fast insertions and deletions** are required.
- **Extra memory is not a concern**.

✅ **Use Open Addressing if:**
- The hash table **has enough empty slots**.
- You want to **avoid using extra memory**.
- Insertions are **relatively rare**.

---

## **7. Conclusion**
- **Hash Collisions** occur when two keys map to the same index.
- **Chaining** uses **linked lists** to store multiple elements at one index.
- **Open Addressing** finds an **empty slot using probing techniques**.
- **Chaining is better for large datasets**, while **Open Addressing works well in small, memory-efficient scenarios**.

Would you like a deeper explanation of **Quadratic Probing or Double Hashing**? 🚀

# What is Treefication / Treefy Threshold?
## **1. What is Treefication (Treeify) in Java?**
Treefication (also called **Treeify**) is the process where a **Linked List in a HashMap bucket is converted into a Red-Black Tree** when the number of elements in a bucket exceeds a certain threshold. This optimization helps in reducing **worst-case time complexity** from **O(n) to O(log n)** for lookups, insertions, and deletions.

---
## **2. Why is Treefication Needed?**
- **HashMap in Java uses an array of buckets**, where each bucket stores elements using **Linked Lists** (by default).
- When multiple keys **hash to the same bucket (collision)**, elements form a **linked list**.
- If too many elements (e.g., **8 or more**) end up in the **same bucket**, the time complexity of search operations degrades from **O(1) to O(n)**.
- **Solution?** Convert the linked list into a **Red-Black Tree**, reducing the lookup time to **O(log n)**.

---

## **3. Treeify Threshold in Java**
- The threshold to **convert a linked list into a tree** in Java’s `HashMap` is **8** (`TREEIFY_THRESHOLD = 8`).
- The threshold to **convert a tree back to a linked list** when elements decrease is **6** (`UNTREEIFY_THRESHOLD = 6`).
- However, **treeification only occurs if the total capacity of the HashMap is at least 64** (`MIN_TREEIFY_CAPACITY = 64`). Otherwise, the HashMap **resizes instead of treeifying**.

### **Threshold Values in Java (from HashMap source code)**
```java
static final int TREEIFY_THRESHOLD = 8;
static final int UNTREEIFY_THRESHOLD = 6;
static final int MIN_TREEIFY_CAPACITY = 64;
```

---

## **4. How Treefication Works in Java’s HashMap?**
1. **When adding elements, collisions occur** → Elements are stored in a linked list.
2. **If a bucket’s size exceeds 8 (`TREEIFY_THRESHOLD`)** → The linked list **converts into a Red-Black Tree**.
3. **If elements are removed and fall below 6 (`UNTREEIFY_THRESHOLD`)** → The tree **converts back to a linked list**.
4. **If capacity is below 64 (`MIN_TREEIFY_CAPACITY`)** → The HashMap **resizes instead of treeifying**.

---

## **5. Treeification Process (Before & After)**
### **Before Treeification (Linked List Structure)**
```
Bucket 5:   (key=15) → (key=25) → (key=35) → (key=45) → (key=55) → (key=65) → (key=75) → (key=85)
```
### **After Treeification (Converted into Red-Black Tree)**
```
          45
         /  \
       25    65
      /  \   /  \
    15   35 55  75
                 \
                 85
```

---

## **6. Java Code Demonstrating Treefication**
```java
import java.util.HashMap;

public class TreeificationExample {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        // Inserting elements that will cause hash collisions
        for (int i = 1; i <= 20; i++) {
            map.put(i * 10, "Value " + i);
        }

        System.out.println("HashMap after inserting elements: " + map);
    }
}
```
✅ If **multiple elements hash to the same bucket**, treeification occurs **automatically** after the threshold is exceeded.

---

## **7. Treeify vs Resize: What Happens First?**
- If a bucket exceeds **8 elements**, Java **first checks if the capacity is at least 64**.
  - If **capacity is < 64** → **HashMap resizes instead of treeifying**.
  - If **capacity is ≥ 64** → **Bucket is treeified** into a **Red-Black Tree**.

---

## **8. Performance Analysis**
| **Scenario**           | **Data Structure Used** | **Time Complexity** |
|----------------------|-------------------|----------------|
| **Few collisions**  | Array of Linked Lists | **O(1)** |
| **High collisions (`< 8`)** | Linked List | **O(n)** |
| **High collisions (`≥ 8`)** | Red-Black Tree | **O(log n)** |

---

## **9. Summary**
1. **Treefication converts a Linked List into a Red-Black Tree when collisions exceed 8**.
2. **Treeification happens only if capacity ≥ 64**; otherwise, **resizing occurs**.
3. **De-treeification (back to Linked List) happens when elements reduce below 6**.
4. **This optimization improves performance, reducing worst-case lookup time from O(n) to O(log n)**.

Would you like a **deeper explanation of Red-Black Trees** or a **HashMap debugging example**? 🚀

# What is a Red-Black Tree / Self-balanced Binary Search Tree?
A **Red-Black Tree (RBT)** is a **self-balancing binary search tree (BST)** where each node follows additional rules to ensure that no path in the tree is significantly longer than others. This guarantees **O(log n)** time complexity for **insertion, deletion, and search** operations.

---

## **1. Why Red-Black Tree?**
A **Binary Search Tree (BST)** can become **skewed**, leading to **O(n)** search time in the worst case.  
For example, inserting nodes in sorted order results in:
```
    1
     \
      2
       \
        3
         \
          4
           \
            5  (Skewed BST - Worst Case)
```
**Red-Black Trees prevent skewing** by maintaining balance using a set of **coloring rules**.

---

## **2. Properties of Red-Black Tree**
Each node follows these rules:

1. **Each node is either RED or BLACK**.
2. **The root is always BLACK**.
3. **Every path from root to a NULL leaf has the same number of BLACK nodes**.
4. **A RED node cannot have RED children** (No two consecutive RED nodes).
5. **Insertion and deletion maintain these properties using rotations and recoloring**.

---

## **3. Example of Red-Black Tree**
```
        (10B)
       /     \
    (5R)    (20R)
   /   \    /    \
 (1B) (7B) (15B) (30B)
```
- The **root (10) is BLACK**.
- **Red nodes (5, 20) have only BLACK children**.
- Every path from root to NULL has **the same number of BLACK nodes**.

---

## **4. Operations in Red-Black Tree**
### **4.1 Insertion in Red-Black Tree**
- Insert the node as in a **BST**.
- The new node is **always RED**.
- Fix violations using **recoloring or rotations**.

### **Example: Inserting 25**
1. Insert 25 as RED.
2. If parent (20) is RED (Rule Violation).
3. Fix by **rotation and recoloring**.

### **Insertion Cases**
1. **Parent is BLACK** → No problem ✅
2. **Parent is RED & Uncle is RED** → Recolor parent and uncle, make grandparent RED.
3. **Parent is RED & Uncle is BLACK** → Rotate (Left or Right) to balance.

---

### **4.2 Deletion in Red-Black Tree**
- If the node is **RED**, delete normally.
- If **BLACK**, replace with a child and fix violations using recoloring and rotations.

### **Deletion Cases**
1. **Node is RED** → Simply remove ✅
2. **Node is BLACK**:
   - If the child is **RED**, replace the node and recolor.
   - If the child is **BLACK**, perform **double-black fix** (using sibling rotations).

---

## **5. Red-Black Tree Implementation in Java**
### **5.1 Node Structure**
Each node has:
- **Value (`data`)**
- **Color (`RED` or `BLACK`)**
- **Left and right children**
- **Parent reference**

```java
class Node {
    int data;
    Node left, right, parent;
    boolean color; // true = RED, false = BLACK

    public Node(int data) {
        this.data = data;
        this.left = this.right = this.parent = null;
        this.color = true; // New nodes are always RED
    }
}
```

---

### **5.2 Red-Black Tree Class**
Contains methods for:
- **Insertion (`insert()` and `fixInsertion()`)**
- **Rotations (`leftRotate()`, `rightRotate()`)**
- **Deletion (`delete() and fixDeletion()`)**

```java
class RedBlackTree {
    private Node root;
    private final boolean RED = true;
    private final boolean BLACK = false;

    // Left Rotate
    private void leftRotate(Node x) {
        Node y = x.right;
        x.right = y.left;

        if (y.left != null)
            y.left.parent = x;

        y.parent = x.parent;

        if (x.parent == null)
            root = y;
        else if (x == x.parent.left)
            x.parent.left = y;
        else
            x.parent.right = y;

        y.left = x;
        x.parent = y;
    }

    // Right Rotate
    private void rightRotate(Node x) {
        Node y = x.left;
        x.left = y.right;

        if (y.right != null)
            y.right.parent = x;

        y.parent = x.parent;

        if (x.parent == null)
            root = y;
        else if (x == x.parent.right)
            x.parent.right = y;
        else
            x.parent.left = y;

        y.right = x;
        x.parent = y;
    }

    // Insert Node
    public void insert(int data) {
        Node newNode = new Node(data);
        if (root == null) {
            root = newNode;
            root.color = BLACK; // Root must be black
            return;
        }

        Node parent = null, temp = root;
        while (temp != null) {
            parent = temp;
            if (data < temp.data)
                temp = temp.left;
            else
                temp = temp.right;
        }

        newNode.parent = parent;
        if (data < parent.data)
            parent.left = newNode;
        else
            parent.right = newNode;

        fixInsertion(newNode);
    }

    // Fix violations after insertion
    private void fixInsertion(Node node) {
        while (node.parent != null && node.parent.color == RED) {
            Node grandparent = node.parent.parent;
            if (node.parent == grandparent.left) {
                Node uncle = grandparent.right;
                if (uncle != null && uncle.color == RED) {
                    node.parent.color = BLACK;
                    uncle.color = BLACK;
                    grandparent.color = RED;
                    node = grandparent;
                } else {
                    if (node == node.parent.right) {
                        node = node.parent;
                        leftRotate(node);
                    }
                    node.parent.color = BLACK;
                    grandparent.color = RED;
                    rightRotate(grandparent);
                }
            } else {
                Node uncle = grandparent.left;
                if (uncle != null && uncle.color == RED) {
                    node.parent.color = BLACK;
                    uncle.color = BLACK;
                    grandparent.color = RED;
                    node = grandparent;
                } else {
                    if (node == node.parent.left) {
                        node = node.parent;
                        rightRotate(node);
                    }
                    node.parent.color = BLACK;
                    grandparent.color = RED;
                    leftRotate(grandparent);
                }
            }
        }
        root.color = BLACK;
    }

    // Inorder Traversal
    public void inorder(Node node) {
        if (node != null) {
            inorder(node.left);
            System.out.print(node.data + (node.color ? "R " : "B ") + " ");
            inorder(node.right);
        }
    }

    public void display() {
        inorder(root);
        System.out.println();
    }
}
```

---

### **5.3 Testing the Red-Black Tree**
```java
public class RedBlackTreeExample {
    public static void main(String[] args) {
        RedBlackTree rbt = new RedBlackTree();
        rbt.insert(10);
        rbt.insert(20);
        rbt.insert(30);
        rbt.insert(15);
        rbt.insert(25);

        System.out.println("Red-Black Tree:");
        rbt.display();
    }
}
```

✅ The tree remains **balanced** after inserting elements.

---

## **6. Performance Comparison**
| Operation  | BST (Unbalanced) | Red-Black Tree |
|------------|----------------|---------------|
| **Insertion** | O(n) | O(log n) |
| **Deletion** | O(n) | O(log n) |
| **Search** | O(n) | O(log n) |

---

## **7. Where are Red-Black Trees Used?**
✅ **Java's TreeMap & TreeSet** internally use **Red-Black Trees**.  
✅ **Database Indexing** uses self-balancing trees for **fast searching**.  
✅ **Networking (Routing Tables)** for optimized **IP address lookup**.

---

## **8. Conclusion**
- A **Red-Black Tree** is a **self-balancing BST** that maintains **O(log n) time complexity**.
- It ensures **balance using color properties and rotations**.
- Used in **Java Collections (`TreeMap`, `TreeSet`) and databases**.

Would you like a **deletion implementation** or **AVL Tree comparison**? 🚀
# What is the Compare and Swap (CAS) Algorithm?
The **Compare and Swap (CAS) algorithm** is an **atomic operation** used in **multi-threaded programming** to achieve **lock-free synchronization**. It allows threads to **update a shared variable only if its value has not changed** since last checked.

---

## **1. What is Compare and Swap (CAS)?**
The **Compare and Swap (CAS) algorithm** operates on **three values**:
1. **Memory Location (V)** – The variable to be updated.
2. **Expected Value (E)** – The original value expected.
3. **New Value (N)** – The new value to be updated.

### **CAS Algorithm Steps**
1. Check if the **current value** at memory location `V` matches the **expected value `E`**.
2. - If **yes**, update `V` to `N` (**successful swap**).
   - If **no**, do nothing (**failure, retry later**).

---

## **2. CAS in Action (Example)**
### **Pseudocode of Compare and Swap**
```java
boolean compareAndSwap(int *V, int E, int N) {
    if (*V == E) { // If expected value matches current value
        *V = N;    // Update value
        return true; // Swap successful
    }
    return false; // Swap failed
}
```

### **Example: Compare and Swap**
| Memory Location (`V`) | Expected Value (`E`) | New Value (`N`) | Outcome |
|----------------|----------------|----------|-----------|
| `5`           | `5`            | `10`     | ✅ Success (Update to `10`) |
| `5`           | `3`            | `10`     | ❌ Fail (Retry later) |

---

## **3. Java Implementation of CAS**
Java provides built-in support for **CAS operations** in the `java.util.concurrent.atomic` package.

### **Using `AtomicInteger` (Java CAS)**
```java
import java.util.concurrent.atomic.AtomicInteger;

public class CASExample {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);

        // Attempt to update value using CAS
        boolean success = atomicInteger.compareAndSet(5, 10); // Expected = 5, New = 10

        System.out.println("CAS Success: " + success);
        System.out.println("Updated Value: " + atomicInteger.get());
    }
}
```
✅ **Output:**
```
CAS Success: true
Updated Value: 10
```

---

## **4. CAS vs Lock-based Synchronization**
| Feature       | CAS (Lock-Free) | Locks (Synchronized) |
|--------------|----------------|------------------|
| **Performance** | **Faster** (No waiting) | **Slower** (Thread blocking) |
| **Thread Safety** | Yes | Yes |
| **Deadlocks?** | ❌ No deadlocks | ✅ Can cause deadlocks |
| **Usage in Java** | `AtomicInteger`, `AtomicLong`, `ConcurrentHashMap` | `synchronized`, `ReentrantLock` |

---

## **5. Problems with CAS**
1. **ABA Problem** – The value changes **from A → B → A** but CAS doesn’t detect it.
2. **High CPU Usage** – If many threads retry, it can cause high CPU consumption.
3. **Not Ideal for Complex Data Structures** – Works best for simple variables like `int`.

✅ **Solution to ABA Problem?** Use **AtomicStampedReference**.

### **Fixing ABA Problem with `AtomicStampedReference`**
```java
import java.util.concurrent.atomic.AtomicStampedReference;

public class ABAProblemSolution {
    public static void main(String[] args) {
        AtomicStampedReference<Integer> atomicRef = new AtomicStampedReference<>(5, 0);

        int stamp = atomicRef.getStamp(); // Get initial version
        boolean success = atomicRef.compareAndSet(5, 10, stamp, stamp + 1); // Update value

        System.out.println("CAS Success: " + success);
        System.out.println("New Value: " + atomicRef.getReference());
    }
}
```

✅ **Fixes ABA problem** by **tracking changes with a version number**.

---

## **6. Where is CAS Used?**
✅ **Java's `Atomic` classes (`AtomicInteger`, `AtomicLong`, etc.)**  
✅ **Concurrent data structures (`ConcurrentHashMap`, `ConcurrentLinkedQueue`)**  
✅ **Lock-free algorithms** for **high-performance** applications.  

Would you like a **deep dive into Java’s atomic classes**? 🚀

It looks like you're working on implementing the **Compare and Swap (CAS) Algorithm**. Below, I'll provide:

1. **A simple CAS implementation in Java (without `AtomicInteger`)**
2. **A thread-safe CAS implementation using `AtomicInteger`**
3. **Handling the ABA problem with `AtomicStampedReference`**
4. **Performance comparison of CAS vs Locks**

---

## **1. Basic CAS Algorithm Implementation (Manual)**
This implementation **simulates CAS** without using `AtomicInteger`. It directly checks and updates a variable in a thread-safe way.

```java
class CAS {
    private volatile int value; // Shared variable

    public CAS(int initialValue) {
        this.value = initialValue;
    }

    public boolean compareAndSwap(int expected, int newValue) {
        synchronized (this) { // Ensuring atomicity manually
            if (value == expected) {
                value = newValue;
                return true;
            }
            return false;
        }
    }

    public int getValue() {
        return value;
    }
}

public class CASManualExample {
    public static void main(String[] args) {
        CAS cas = new CAS(5);

        // Trying CAS operation
        boolean success = cas.compareAndSwap(5, 10); // Expecting 5, setting 10
        System.out.println("CAS Success: " + success);
        System.out.println("New Value: " + cas.getValue());

        // Trying with a wrong expected value
        success = cas.compareAndSwap(5, 15); // Expecting 5, but current value is 10
        System.out.println("CAS Success: " + success);
        System.out.println("Value after failed CAS: " + cas.getValue());
    }
}
```
✅ **Output**
```
CAS Success: true
New Value: 10
CAS Success: false
Value after failed CAS: 10
```
🔹 This simulates **CAS without using `AtomicInteger`** but requires explicit `synchronized` for thread safety.

---

## **2. Thread-Safe CAS using `AtomicInteger`**
Instead of writing custom logic, Java provides `AtomicInteger`, which **internally uses CAS** for fast, lock-free updates.

```java
import java.util.concurrent.atomic.AtomicInteger;

public class CASThreadSafeExample {
    public static void main(String[] args) {
        AtomicInteger atomicValue = new AtomicInteger(5);

        // CAS Operation: If value is 5, change it to 10
        boolean success = atomicValue.compareAndSet(5, 10);
        System.out.println("CAS Success: " + success);
        System.out.println("Updated Value: " + atomicValue.get());

        // CAS Operation: Expecting 5, setting to 15 (this will fail)
        success = atomicValue.compareAndSet(5, 15);
        System.out.println("CAS Success: " + success);
        System.out.println("Value after failed CAS: " + atomicValue.get());
    }
}
```
✅ **Output**
```
CAS Success: true
Updated Value: 10
CAS Success: false
Value after failed CAS: 10
```
🔹 **No locks are used!** This makes `AtomicInteger` faster than `synchronized`.

---

## **3. Handling the ABA Problem with `AtomicStampedReference`**
### **What is the ABA Problem?**
If a thread reads **value = A**, another thread changes **A → B → A**, the first thread **doesn't detect the change**, leading to potential issues.

🔹 **Solution?** `AtomicStampedReference`, which tracks a **version (stamp)** along with the value.

```java
import java.util.concurrent.atomic.AtomicStampedReference;

public class ABAProblemSolution {
    public static void main(String[] args) {
        AtomicStampedReference<Integer> atomicRef = new AtomicStampedReference<>(5, 0);

        int[] stampHolder = new int[1]; // Array to store version

        // Get initial value and version
        int currentValue = atomicRef.get(stampHolder);
        int currentStamp = stampHolder[0];

        System.out.println("Initial Value: " + currentValue + " | Version: " + currentStamp);

        // First CAS operation
        boolean success = atomicRef.compareAndSet(5, 10, currentStamp, currentStamp + 1);
        System.out.println("CAS Success: " + success);
        System.out.println("New Value: " + atomicRef.getReference() + " | New Version: " + atomicRef.getStamp());

        // Simulating ABA (Changing 10 → 5)
        atomicRef.compareAndSet(10, 5, atomicRef.getStamp(), atomicRef.getStamp() + 1);

        // Now another CAS with old stamp (Fails due to different version)
        success = atomicRef.compareAndSet(5, 20, currentStamp, currentStamp + 1);
        System.out.println("CAS Success after ABA: " + success);
    }
}
```
✅ **Output**
```
Initial Value: 5 | Version: 0
CAS Success: true
New Value: 10 | New Version: 1
CAS Success after ABA: false
```
🔹 **The CAS fails due to version mismatch, preventing ABA issues!**

---

## **4. CAS vs Lock-based Synchronization (Performance Test)**
Let's test **CAS vs Synchronized Locks** in a multi-threaded environment.

### **CAS Using `AtomicInteger`**
```java
import java.util.concurrent.atomic.AtomicInteger;

public class CASPerformanceTest {
    static AtomicInteger atomicCounter = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000000; i++) {
                atomicCounter.incrementAndGet();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000000; i++) {
                atomicCounter.incrementAndGet();
            }
        });

        long startTime = System.nanoTime();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        long endTime = System.nanoTime();

        System.out.println("CAS Final Counter: " + atomicCounter.get());
        System.out.println("CAS Execution Time: " + (endTime - startTime) + " ns");
    }
}
```

---

### **Using `synchronized` Lock**
```java
public class LockPerformanceTest {
    static int counter = 0;

    public static synchronized void increment() {
        counter++;
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000000; i++) {
                increment();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000000; i++) {
                increment();
            }
        });

        long startTime = System.nanoTime();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        long endTime = System.nanoTime();

        System.out.println("Lock Final Counter: " + counter);
        System.out.println("Lock Execution Time: " + (endTime - startTime) + " ns");
    }
}
```

---

## **5. CAS vs Lock Performance Results**
| **Method**           | **Execution Time** | **Thread Safety** | **Locking?** |
|----------------------|-------------------|----------------|----------|
| **CAS (`AtomicInteger`)** | ✅ **Faster (~O(1))** | ✅ Yes | ❌ No |
| **Lock (`synchronized`)** | ❌ **Slower (~O(n))** | ✅ Yes | ✅ Yes |

✅ **CAS is much faster** because **it doesn’t block threads**, while `synchronized` forces **waiting**.

---

## **6. Summary**
- **CAS is a lock-free algorithm** used for **atomic updates** in multi-threading.
- **Java provides CAS via `AtomicInteger`, `AtomicReference`, and `AtomicStampedReference`**.
- **CAS avoids deadlocks but suffers from the ABA problem**, which is fixed with **`AtomicStampedReference`**.
- **CAS is faster than `synchronized` locks** in most cases.

🚀 **Next Steps?** Would you like a **low-level CPU explanation of CAS (using CPU registers)**? 😃

# What is Segment Locking in ConcurrentHashMap?
## **1. What is Segment Locking?**
Segment Locking is a **lock partitioning mechanism** used in `ConcurrentHashMap` (Java versions **prior to Java 8**) to improve performance in multi-threaded environments.

### **How Does it Work?**
- Instead of locking the **entire map** (`synchronized` or `ReentrantLock`), `ConcurrentHashMap` **divides the map into segments**.
- Each segment acts as an **independent mini-HashMap with its own lock**.
- Multiple threads **can access different segments concurrently** without blocking each other.

✅ **Result?** **Higher concurrency and better performance** compared to using `synchronized HashMap`.

---

## **2. `ConcurrentHashMap` Before and After Java 8**
| **Java Version**  | **Implementation** |
|-------------------|-------------------|
| **Before Java 8** | Used **Segment Locking** (Partitioned `ReentrantLocks`) |
| **Java 8 and Later** | Uses **Bucket-Level CAS (`compareAndSet`) and Locking only when necessary** |

🔹 **Segment Locking was replaced in Java 8** with a more optimized **fine-grained locking** system.

---

## **3. Internal Structure of `ConcurrentHashMap` (Pre-Java 8)**
- `ConcurrentHashMap` was divided into **segments (default: 16)**.
- Each **segment** contained **a portion of the hash table** and had its own **lock**.
- Threads modifying different segments **could proceed in parallel**.

### **Example: Segmented `ConcurrentHashMap` (Before Java 8)**
```
ConcurrentHashMap with 16 Segments:
------------------------------------------------
Segment[0]   | Segment[1]   | Segment[2]   | ... | Segment[15]
------------------------------------------------
 {key1, val1} | {key5, val5} | {key9, val9} | ... | {keyN, valN}
------------------------------------------------
```
- If **Thread 1** modifies `Segment[2]` and **Thread 2** modifies `Segment[7]`, they work **without blocking each other**.

---

## **4. Java Implementation of Segment Locking (`ConcurrentHashMap`)**
Although we **cannot directly access segments in Java 8**, we can **simulate segment locking using a custom implementation**.

### **Custom Implementation of `SegmentedHashMap`**
```java
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;
import java.util.Arrays;

class SegmentedHashMap<K, V> {
    private static final int NUM_SEGMENTS = 16;
    private final Node<K, V>[] buckets;
    private final Lock[] locks;

    static class Node<K, V> {
        final K key;
        V value;
        Node<K, V> next;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
            this.next = null;
        }
    }

    @SuppressWarnings("unchecked")
    public SegmentedHashMap() {
        buckets = (Node<K, V>[]) new Node[NUM_SEGMENTS];
        locks = new ReentrantLock[NUM_SEGMENTS];
        Arrays.setAll(locks, i -> new ReentrantLock());
    }

    private int hash(K key) {
        return Math.abs(key.hashCode() % NUM_SEGMENTS);
    }

    public void put(K key, V value) {
        int index = hash(key);
        locks[index].lock(); // Lock the segment
        try {
            Node<K, V> node = new Node<>(key, value);
            node.next = buckets[index];
            buckets[index] = node;
        } finally {
            locks[index].unlock(); // Unlock after operation
        }
    }

    public V get(K key) {
        int index = hash(key);
        locks[index].lock();
        try {
            Node<K, V> current = buckets[index];
            while (current != null) {
                if (current.key.equals(key)) {
                    return current.value;
                }
                current = current.next;
            }
        } finally {
            locks[index].unlock();
        }
        return null;
    }

    public void remove(K key) {
        int index = hash(key);
        locks[index].lock();
        try {
            Node<K, V> current = buckets[index], prev = null;
            while (current != null) {
                if (current.key.equals(key)) {
                    if (prev == null) {
                        buckets[index] = current.next;
                    } else {
                        prev.next = current.next;
                    }
                    return;
                }
                prev = current;
                current = current.next;
            }
        } finally {
            locks[index].unlock();
        }
    }

    public void display() {
        for (int i = 0; i < NUM_SEGMENTS; i++) {
            System.out.print("Segment[" + i + "]: ");
            Node<K, V> current = buckets[i];
            while (current != null) {
                System.out.print("{" + current.key + "=" + current.value + "} ");
                current = current.next;
            }
            System.out.println();
        }
    }
}

public class SegmentLockingExample {
    public static void main(String[] args) {
        SegmentedHashMap<Integer, String> map = new SegmentedHashMap<>();

        map.put(1, "One");
        map.put(17, "Seventeen"); // Collides with index 1 due to mod operation
        map.put(5, "Five");

        System.out.println("Value for key 1: " + map.get(1));
        System.out.println("Value for key 17: " + map.get(17));

        map.display();

        map.remove(1);
        System.out.println("After removing key 1:");
        map.display();
    }
}
```
✅ **Output**
```
Value for key 1: One
Value for key 17: Seventeen
Segment[1]: {17=Seventeen} {1=One}
Segment[5]: {5=Five}
After removing key 1:
Segment[1]: {17=Seventeen}
Segment[5]: {5=Five}
```
🔹 **Each segment operates independently**, allowing multiple threads to modify different segments **without blocking each other**.

---

## **5. `ConcurrentHashMap` in Java 8 (No Segment Locking)**
In **Java 8 and later**, `ConcurrentHashMap`:
- **Removed Segment Locking**.
- Uses **bucket-level fine-grained locking + CAS (`compareAndSet`)**.
- Uses **Red-Black Trees** for handling high collisions.

### **Example: Java 8 `ConcurrentHashMap`**
```java
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();

        map.put(1, "One");
        map.put(2, "Two");

        System.out.println(map.get(1)); // Output: One
        map.remove(2);

        System.out.println("Final Map: " + map);
    }
}
```
✅ **Much simpler implementation in Java 8+!**

---

## **6. Performance Comparison**
| Feature | Java 7 (`ConcurrentHashMap`) | Java 8+ (`ConcurrentHashMap`) |
|---------|-----------------------------|-----------------------------|
| **Locking** | **Segment Locking** (Locks per segment) | **Bucket-level CAS** (Faster, fewer locks) |
| **Write Performance** | Slower (Thread waits if the segment is locked) | Faster (Less contention using CAS) |
| **Read Performance** | Faster (Multiple reads allowed) | Even Faster (Lock-free reads) |
| **Collision Handling** | Linked Lists in segments | Linked List & **Treeify (Red-Black Trees)** |
| **Best Use Case** | Java 7, Moderate concurrency | Java 8+, High concurrency |

---

## **7. Conclusion**
✅ **Segment Locking (Pre-Java 8)**
- Uses **multiple locks** for different segments.
- Allows **parallel writes in different segments**.

✅ **Java 8+ (`ConcurrentHashMap`)**
- Removed segment locking.
- Uses **fine-grained locking & CAS** for better performance.
- Uses **Treeification (Red-Black Trees) for high collisions**.

🔹 **Segment Locking was a great improvement in Java 7, but Java 8's `ConcurrentHashMap` is even more optimized!**

Would you like a **deep dive into Java 8’s fine-grained locking mechanism?** 🚀


# What is FIFO vs LIFO principle?
FIFO (**First In, First Out**) and LIFO (**Last In, First Out**) are **two fundamental principles** used in data structures like **queues and stacks**.

---

## **1. FIFO (First In, First Out)**
### **Definition**
- **The element that is inserted first is removed first.**
- Works like a **queue** (e.g., people standing in a line).
- **Example in real life:** A queue in a bank or supermarket.

### **FIFO Data Structure: Queue**
- FIFO is implemented using **Queue** in Java (`LinkedList`, `PriorityQueue`, `ArrayDeque`).
- Java provides `Queue` and `Deque` interfaces for FIFO behavior.

### **FIFO Implementation in Java**
```java
import java.util.LinkedList;
import java.util.Queue;

public class FIFOExample {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>(); // FIFO

        // Adding elements
        queue.add("A");
        queue.add("B");
        queue.add("C");

        System.out.println("Queue: " + queue); // Output: [A, B, C]

        // Removing elements (FIFO)
        System.out.println("Removed: " + queue.poll()); // A (First in, First out)
        System.out.println("Removed: " + queue.poll()); // B

        System.out.println("Queue after removals: " + queue); // Output: [C]
    }
}
```
✅ **Output**
```
Queue: [A, B, C]
Removed: A
Removed: B
Queue after removals: [C]
```
🔹 **A (First in) is removed first** → FIFO principle.

---

## **2. LIFO (Last In, First Out)**
### **Definition**
- **The last inserted element is removed first.**
- Works like a **stack** (e.g., stack of plates).
- **Example in real life:** Books stacked on top of each other.

### **LIFO Data Structure: Stack**
- Java provides **`Stack` class** (`push()` to add, `pop()` to remove).
- `Deque` (Double-ended queue) is preferred over `Stack` due to better performance.

### **LIFO Implementation in Java (Using `Stack`)**
```java
import java.util.Stack;

public class LIFOExample {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>(); // LIFO

        // Adding elements
        stack.push("A");
        stack.push("B");
        stack.push("C");

        System.out.println("Stack: " + stack); // Output: [A, B, C]

        // Removing elements (LIFO)
        System.out.println("Removed: " + stack.pop()); // C (Last in, First out)
        System.out.println("Removed: " + stack.pop()); // B

        System.out.println("Stack after removals: " + stack); // Output: [A]
    }
}
```
✅ **Output**
```
Stack: [A, B, C]
Removed: C
Removed: B
Stack after removals: [A]
```
🔹 **C (Last in) is removed first** → LIFO principle.

---

## **3. FIFO vs LIFO: Key Differences**
| Feature          | FIFO (Queue) | LIFO (Stack) |
|-----------------|-------------|-------------|
| **Definition**  | First In, First Out | Last In, First Out |
| **Data Structure** | Queue | Stack |
| **Insertion (`add()` / `push()`)** | At the **end** | At the **top** |
| **Removal (`poll()` / `pop()`)** | From the **front** | From the **top** |
| **Example** | Customer queue in a bank | Stack of plates |

---

## **4. Advanced FIFO and LIFO Implementations**
### **FIFO Using `ArrayDeque` (Better than `LinkedList`)**
```java
import java.util.ArrayDeque;
import java.util.Queue;

public class FIFOArrayDeque {
    public static void main(String[] args) {
        Queue<Integer> queue = new ArrayDeque<>();

        queue.offer(10);
        queue.offer(20);
        queue.offer(30);

        System.out.println("Queue: " + queue);
        System.out.println("Removed: " + queue.poll()); // 10
        System.out.println("Queue after removal: " + queue);
    }
}
```

### **LIFO Using `ArrayDeque` (Better than `Stack`)**
```java
import java.util.Deque;
import java.util.ArrayDeque;

public class LIFOArrayDeque {
    public static void main(String[] args) {
        Deque<Integer> stack = new ArrayDeque<>();

        stack.push(10);
        stack.push(20);
        stack.push(30);

        System.out.println("Stack: " + stack);
        System.out.println("Removed: " + stack.pop()); // 30
        System.out.println("Stack after removal: " + stack);
    }
}
```
🔹 **`ArrayDeque` is faster than `Stack` for LIFO operations!**

---

## **5. Real-World Use Cases of FIFO and LIFO**
| **Use Case**            | **FIFO (Queue)** | **LIFO (Stack)** |
|-------------------------|-----------------|-----------------|
| **Print Job Scheduling** | ✅ Yes (Jobs are processed in order) | ❌ No |
| **CPU Scheduling** (Round Robin) | ✅ Yes (Process scheduling) | ❌ No |
| **Undo/Redo Functionality** | ❌ No | ✅ Yes (Last action undone first) |
| **Web Browser Back Button** | ❌ No | ✅ Yes (Last visited page first) |

---

## **6. Conclusion**
- **FIFO (Queue)**: The first element inserted is the first to be removed.
- **LIFO (Stack)**: The last element inserted is the first to be removed.
- **Use `Queue` (`LinkedList`, `ArrayDeque`) for FIFO** operations.
- **Use `Stack` or `Deque` for LIFO** operations.

Would you like to see **multi-threaded FIFO/LIFO examples**? 🚀

# What is the Access Order of LinkedHashMap?
In Java, `LinkedHashMap` maintains the **order of elements** based on insertion or access. The **access order mode** allows elements to be **reordered based on usage**, making it useful for **caching mechanisms like LRU (Least Recently Used) Cache**.

---

## **1. What is Access Order in `LinkedHashMap`?**
A `LinkedHashMap` has two **ordering modes**:
1. **Insertion Order (Default Mode)** → Elements are retrieved in the order they were inserted.
2. **Access Order (`accessOrder = true`)** → Elements are reordered **based on access (recently accessed moves to end)**.

✅ **By default, `LinkedHashMap` preserves insertion order**, but if `accessOrder = true`, it behaves like an **LRU Cache**.

---

## **2. How to Enable Access Order in `LinkedHashMap`**
We enable access order by setting `accessOrder = true` in the constructor:
```java
LinkedHashMap<K, V> map = new LinkedHashMap<>(initialCapacity, loadFactor, true);
```
- **Initial Capacity** → Number of elements before resizing.
- **Load Factor** → Threshold for resizing (default 0.75).
- **Access Order (`true`)** → Enables **LRU behavior**.

---

## **3. Example of Access Order in `LinkedHashMap`**
```java
import java.util.LinkedHashMap;
import java.util.Map;

public class AccessOrderExample {
    public static void main(String[] args) {
        // LinkedHashMap with access order enabled (LRU-like behavior)
        LinkedHashMap<Integer, String> map = new LinkedHashMap<>(5, 0.75f, true);

        // Adding elements
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        map.put(5, "Five");

        System.out.println("Before Access: " + map);

        // Access some elements
        map.get(2); // Accessing key 2
        map.get(4); // Accessing key 4

        System.out.println("After Access: " + map);
    }
}
```
✅ **Output**
```
Before Access: {1=One, 2=Two, 3=Three, 4=Four, 5=Five}
After Access: {1=One, 3=Three, 5=Five, 2=Two, 4=Four}
```
🔹 **Elements 2 and 4 moved to the end after being accessed**.

---

## **4. Use Case: Implementing LRU Cache with `LinkedHashMap`**
A **Least Recently Used (LRU) Cache** removes **oldest accessed elements** when it reaches capacity.

### **LRU Cache Implementation Using `LinkedHashMap`**
```java
import java.util.LinkedHashMap;
import java.util.Map;

class LRUCache<K, V> extends LinkedHashMap<K, V> {
    private final int capacity;

    public LRUCache(int capacity) {
        super(capacity, 0.75f, true); // true enables access order
        this.capacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > capacity; // Remove eldest if size exceeds capacity
    }
}

public class LRUCacheExample {
    public static void main(String[] args) {
        LRUCache<Integer, String> cache = new LRUCache<>(3);

        // Adding elements
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(3, "Three");

        System.out.println("Cache: " + cache);

        // Access some elements
        cache.get(1);
        cache.put(4, "Four"); // Triggers eviction (LRU strategy)

        System.out.println("Cache after accessing 1 and adding 4: " + cache);
    }
}
```
✅ **Output**
```
Cache: {1=One, 2=Two, 3=Three}
Cache after accessing 1 and adding 4: {3=Three, 1=One, 4=Four}
```
🔹 **Key `2` was removed (Least Recently Used)**.

---

## **5. Comparison: Insertion Order vs Access Order**
| **Feature**        | **Insertion Order (`false`)** | **Access Order (`true`)** |
|--------------------|-----------------------------|--------------------------|
| **Ordering**       | Maintains insertion order | Reorders based on access |
| **Usage**          | General-purpose ordering | LRU Cache implementation |
| **Performance**    | O(1) lookup | O(1) lookup but element may move |
| **When to Use?**   | When order **should not change** | When **recently used items should be prioritized** |

---

## **6. Conclusion**
✅ **`LinkedHashMap` supports both insertion and access order**.  
✅ **Access order (`true`) turns it into an LRU-like structure**.  
✅ **Perfect for implementing LRU Caches** (used in memory-sensitive applications).  

# What is the difference between Iterator, ListIterator, and Enumeration?
In Java, **Iterator, ListIterator, and Enumeration** are interfaces used to **iterate over collections** like `ArrayList`, `LinkedList`, `HashSet`, etc.

---

## **1. Quick Comparison Table**
| Feature             | `Enumeration` | `Iterator` | `ListIterator` |
|---------------------|--------------|-----------|---------------|
| **Introduced In**  | Java 1.0 | Java 1.2 | Java 1.2 |
| **Used For** | `Vector`, `Hashtable` (Legacy classes) | Any `Collection` | `List` (Only `ArrayList`, `LinkedList`) |
| **Forward Traversal** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Backward Traversal** | ❌ No | ❌ No | ✅ Yes |
| **Remove Elements** | ❌ No | ✅ Yes (`remove()`) | ✅ Yes (`remove()`) |
| **Modify Elements** | ❌ No | ❌ No | ✅ Yes (`set()`) |
| **Best Use Case** | Legacy code | General-purpose iteration | List traversal (both directions) |

---

## **2. `Enumeration` (Legacy Iterator)**
- `Enumeration` is used in **legacy collections** (`Vector`, `Hashtable`).
- **Only allows forward traversal** (`nextElement()`).
- **Cannot remove elements** from the collection.

### **Example of `Enumeration`**
```java
import java.util.Vector;
import java.util.Enumeration;

public class EnumerationExample {
    public static void main(String[] args) {
        Vector<String> vector = new Vector<>();
        vector.add("A");
        vector.add("B");
        vector.add("C");

        Enumeration<String> enumeration = vector.elements();

        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement()); // Output: A, B, C
        }
    }
}
```
✅ **Output**
```
A
B
C
```
🔹 **Limitations of Enumeration**:
- Cannot remove elements (`remove()` is not available).
- Only supports **forward traversal**.

---

## **3. `Iterator` (Universal Cursor)**
- Works with **all Java collections** (`ArrayList`, `HashSet`, `HashMap` keys, etc.).
- **Supports element removal** (`remove()` method).
- **Only allows forward traversal** (`next()`).

### **Example of `Iterator`**
```java
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IteratorExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        Iterator<String> iterator = list.iterator();

        while (iterator.hasNext()) {
            String value = iterator.next();
            System.out.println(value);
            if (value.equals("B")) {
                iterator.remove(); // Removes "B"
            }
        }
        System.out.println("List after removal: " + list);
    }
}
```
✅ **Output**
```
A
B
C
List after removal: [A, C]
```
🔹 **Limitations of Iterator**:
- **Cannot traverse backward**.
- **Cannot modify elements** (only `remove()` is available).

---

## **4. `ListIterator` (Bi-Directional Iterator)**
- Works **only with List implementations** (`ArrayList`, `LinkedList`).
- **Supports both forward and backward traversal** (`next()`, `previous()`).
- **Can modify elements** using `set()`.

### **Example of `ListIterator`**
```java
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        ListIterator<String> listIterator = list.listIterator();

        // Forward traversal
        while (listIterator.hasNext()) {
            String value = listIterator.next();
            System.out.println("Forward: " + value);

            // Modify an element
            if (value.equals("B")) {
                listIterator.set("X"); // Replaces "B" with "X"
            }
        }

        // Backward traversal
        while (listIterator.hasPrevious()) {
            System.out.println("Backward: " + listIterator.previous());
        }

        System.out.println("Final List: " + list);
    }
}
```
✅ **Output**
```
Forward: A
Forward: B
Forward: C
Backward: C
Backward: X
Backward: A
Final List: [A, X, C]
```
🔹 **Advantages of `ListIterator`**:
- Can **traverse both forward and backward**.
- Can **modify elements** (`set()`, `add()`, `remove()`).

---

## **5. Key Differences Explained**
### **5.1 When to Use `Enumeration`?**
- When working with **legacy collections** (`Vector`, `Hashtable`).
- When **modification is not required** (Only reading elements).

✅ **Best for**: Read-only operations on old collections.

### **5.2 When to Use `Iterator`?**
- When **traversing any collection** (`List`, `Set`, `Map` keys).
- When **removal of elements is needed**.
- When **backward traversal is NOT required**.

✅ **Best for**: General-purpose iteration and **removing elements safely**.

### **5.3 When to Use `ListIterator`?**
- When working with **List collections** (`ArrayList`, `LinkedList`).
- When **backward traversal is needed**.
- When **modification of elements is required** (`set()`).

✅ **Best for**: **Modifying Lists and bidirectional traversal**.

---

## **6. Performance Comparison**
| Feature        | `Enumeration` | `Iterator` | `ListIterator` |
|---------------|--------------|-----------|---------------|
| **Performance** | Fast | Medium | Slower (More Features) |
| **Memory Usage** | Low | Medium | Higher (Bi-Directional Pointers) |
| **Flexibility** | Low | Medium | High |

---

## **7. Conclusion**
| **Feature**    | **Enumeration** | **Iterator** | **ListIterator** |
|---------------|----------------|-------------|---------------|
| **Works on**  | `Vector`, `Hashtable` | Any `Collection` | Only `List` (`ArrayList`, `LinkedList`) |
| **Traversal** | Forward only | Forward only | Forward & Backward |
| **Modify Elements** | ❌ No | ❌ No | ✅ Yes (`set()`) |
| **Remove Elements** | ❌ No | ✅ Yes (`remove()`) | ✅ Yes (`remove()`) |
| **Add Elements** | ❌ No | ❌ No | ✅ Yes (`add()`) |

✅ **Use `Enumeration` for legacy code, `Iterator` for general collections, and `ListIterator` for modifying lists and bidirectional traversal.** 🚀

Would you like **a deep dive into how iterators work internally in Java?** 😃

# What is the difference between Comparator and Comparable?
In Java, both **`Comparator`** and **`Comparable`** are used for **sorting objects** but have key differences.

---

## **1. Quick Comparison Table**
| Feature | `Comparable` | `Comparator` |
|---------|-------------|-------------|
| **Package** | `java.lang` | `java.util` |
| **Sorting Logic Defined In** | Inside the class itself (`compareTo` method) | Separate class (`compare` method) |
| **Method** | `int compareTo(Object o)` | `int compare(Object o1, Object o2)` |
| **Modifies Original Class?** | ✅ Yes (Class implements `Comparable`) | ❌ No (Separate implementation) |
| **Sort Order** | **Single natural sorting** (default order) | **Multiple custom sorting** (e.g., by name, age, salary) |
| **Best Use Case** | When objects have **one natural order** | When sorting logic **can change dynamically** |

---

## **2. `Comparable` (Used for Natural Sorting)**
- The **class itself implements `Comparable`**.
- Defines a **default sorting order** (e.g., ascending order for numbers, alphabetical order for strings).
- Uses **`compareTo()` method**.

### **Example: Sorting by Employee Salary using `Comparable`**
```java
import java.util.*;

class Employee implements Comparable<Employee> {
    int id;
    String name;
    double salary;

    public Employee(int id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    // Implementing compareTo method (Natural Sorting: Salary Ascending)
    @Override
    public int compareTo(Employee other) {
        return Double.compare(this.salary, other.salary); // Ascending Order
    }

    @Override
    public String toString() {
        return id + " - " + name + " - $" + salary;
    }
}

public class ComparableExample {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(101, "Alice", 50000));
        employees.add(new Employee(102, "Bob", 70000));
        employees.add(new Employee(103, "Charlie", 60000));

        // Sorting using Comparable (by Salary)
        Collections.sort(employees);

        System.out.println("Employees sorted by Salary:");
        for (Employee emp : employees) {
            System.out.println(emp);
        }
    }
}
```
✅ **Output**
```
Employees sorted by Salary:
101 - Alice - $50000.0
103 - Charlie - $60000.0
102 - Bob - $70000.0
```
🔹 **Sorting happens by salary in ascending order** (Natural Sorting).  

🔹 **Limitations of `Comparable`**
- We **cannot change sorting criteria dynamically** (e.g., sorting by name or ID instead).

---

## **3. `Comparator` (Custom Sorting)**
- **Separates sorting logic from the class definition**.
- Used when **multiple sorting criteria** are required.
- Uses **`compare()` method** in a separate class.

### **Example: Sorting Employees by Name using `Comparator`**
```java
import java.util.*;

class Employee {
    int id;
    String name;
    double salary;

    public Employee(int id, String name, double salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return id + " - " + name + " - $" + salary;
    }
}

// Comparator to sort by Name (Alphabetical Order)
class NameComparator implements Comparator<Employee> {
    @Override
    public int compare(Employee e1, Employee e2) {
        return e1.name.compareTo(e2.name);
    }
}

// Comparator to sort by Salary (Descending Order)
class SalaryComparator implements Comparator<Employee> {
    @Override
    public int compare(Employee e1, Employee e2) {
        return Double.compare(e2.salary, e1.salary); // Descending Order
    }
}

public class ComparatorExample {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(101, "Alice", 50000));
        employees.add(new Employee(102, "Bob", 70000));
        employees.add(new Employee(103, "Charlie", 60000));

        // Sorting by Name
        Collections.sort(employees, new NameComparator());
        System.out.println("Employees sorted by Name:");
        for (Employee emp : employees) {
            System.out.println(emp);
        }

        // Sorting by Salary Descending
        Collections.sort(employees, new SalaryComparator());
        System.out.println("\nEmployees sorted by Salary (Descending):");
        for (Employee emp : employees) {
            System.out.println(emp);
        }
    }
}
```
✅ **Output**
```
Employees sorted by Name:
101 - Alice - $50000.0
102 - Bob - $70000.0
103 - Charlie - $60000.0

Employees sorted by Salary (Descending):
102 - Bob - $70000.0
103 - Charlie - $60000.0
101 - Alice - $50000.0
```
🔹 **Advantages of `Comparator`**
- Sorting logic **does not modify the original class**.
- Can define **multiple sorting criteria**.

---

## **4. Using `Comparator` with Lambda Expressions (Java 8)**
Instead of writing separate classes, we can use **Lambda expressions**.

```java
import java.util.*;

public class ComparatorLambdaExample {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(101, "Alice", 50000));
        employees.add(new Employee(102, "Bob", 70000));
        employees.add(new Employee(103, "Charlie", 60000));

        // Sorting using Lambda (By Name)
        employees.sort((e1, e2) -> e1.name.compareTo(e2.name));

        System.out.println("Employees sorted by Name:");
        employees.forEach(System.out::println);

        // Sorting using Lambda (By Salary Descending)
        employees.sort((e1, e2) -> Double.compare(e2.salary, e1.salary));

        System.out.println("\nEmployees sorted by Salary (Descending):");
        employees.forEach(System.out::println);
    }
}
```
✅ **Output**
```
Employees sorted by Name:
101 - Alice - $50000.0
102 - Bob - $70000.0
103 - Charlie - $60000.0

Employees sorted by Salary (Descending):
102 - Bob - $70000.0
103 - Charlie - $60000.0
101 - Alice - $50000.0
```
🔹 **Lambda expressions make sorting much cleaner!** 🚀

---

## **5. When to Use `Comparable` vs `Comparator`**
| **Use Case** | **Use `Comparable`** | **Use `Comparator`** |
|-------------|--------------------|---------------------|
| **Single sorting order required** | ✅ Yes | ❌ No |
| **Sorting logic inside the class** | ✅ Yes | ❌ No |
| **Multiple sorting orders needed** | ❌ No | ✅ Yes |
| **Sorting order can change dynamically** | ❌ No | ✅ Yes |
| **Modifying the original class is allowed** | ✅ Yes | ❌ No |

---

## **6. Conclusion**
| Feature | `Comparable` | `Comparator` |
|---------|-------------|-------------|
| **Definition** | Sorting logic inside the class | Separate class for sorting logic |
| **Sorting Method** | `compareTo(Object o)` | `compare(Object o1, Object o2)` |
| **Default Sorting?** | ✅ Yes (Single order) | ❌ No (Can define multiple orders) |
| **Flexibility** | ❌ No | ✅ Yes |
| **Usage** | When **default sorting** is required | When **sorting logic can change** |

✅ **Use `Comparable` when a class has only one natural sorting order** (e.g., sorting Employees by salary).  
✅ **Use `Comparator` when sorting needs to be flexible** (e.g., sorting Employees by name, salary, or other criteria dynamically).

Would you like **a comparison with `TreeSet` and `TreeMap` sorting?** 🚀

# Explain the Time Complexity (Big-O Notation) of all collection methods?
Here’s a **detailed time complexity analysis** for different **Java Collection Framework (JCF) classes** like **ArrayList, LinkedList, HashMap, TreeMap, HashSet, etc.**

---

## **1. Java Collections Time Complexity Table**
| **Operation**  | **ArrayList** | **LinkedList** | **HashMap** | **TreeMap** | **HashSet** | **TreeSet** |
|---------------|-------------|--------------|-----------|---------|----------|---------|
| **Insertion (add/put)** | O(1) (Amortized) | O(1) at head, O(n) at index | O(1) (Amortized) | O(log n) | O(1) (Amortized) | O(log n) |
| **Deletion (remove)** | O(n) | O(1) at head, O(n) at index | O(1) (Amortized) | O(log n) | O(1) (Amortized) | O(log n) |
| **Search (contains/get)** | O(n) | O(n) | O(1) (Amortized) | O(log n) | O(1) (Amortized) | O(log n) |
| **Access (get by index/key)** | O(1) | O(n) | O(1) | O(log n) | O(1) | O(log n) |
| **Iteration (for loop, iterator)** | O(n) | O(n) | O(n) | O(n) | O(n) | O(n) |

✅ **Key Observations:**
- `ArrayList` is **faster for access** but **slower for insertions/removals in the middle**.
- `LinkedList` is **faster for insertions/removals** but **slow for access**.
- `HashMap` and `HashSet` provide **O(1) operations**, but worst case is **O(n)** (if too many collisions occur).
- `TreeMap` and `TreeSet` use **Red-Black Trees**, leading to **O(log n) performance**.

---

## **2. Time Complexity for Individual Collections**
### **2.1 `ArrayList` (Backed by Dynamic Array)**
| **Operation** | **Time Complexity** |
|--------------|---------------------|
| `add(E e)` | **O(1)** (Amortized), **O(n)** (If resizing is needed) |
| `add(int index, E e)` | **O(n)** (Shift elements) |
| `remove(int index)` | **O(n)** (Shift elements) |
| `get(int index)` | **O(1)** |
| `contains(Object o)` | **O(n)** |
| `size()` | **O(1)** |

### **2.2 `LinkedList` (Doubly Linked List)**
| **Operation** | **Time Complexity** |
|--------------|---------------------|
| `add(E e)` | **O(1)** |
| `add(int index, E e)` | **O(n)** |
| `remove(int index)` | **O(n)** |
| `get(int index)` | **O(n)** |
| `contains(Object o)` | **O(n)** |
| `size()` | **O(1)** |

### **2.3 `HashMap` (Backed by Hash Table)**
| **Operation** | **Time Complexity** |
|--------------|---------------------|
| `put(K key, V value)` | **O(1) Amortized**, **O(n) Worst** (Rehashing or many collisions) |
| `get(Object key)` | **O(1) Amortized**, **O(n) Worst** |
| `remove(Object key)` | **O(1) Amortized**, **O(n) Worst** |
| `containsKey(Object key)` | **O(1)** |
| `containsValue(Object value)` | **O(n)** |
| `size()` | **O(1)** |

### **2.4 `TreeMap` (Backed by Red-Black Tree)**
| **Operation** | **Time Complexity** |
|--------------|---------------------|
| `put(K key, V value)` | **O(log n)** |
| `get(Object key)` | **O(log n)** |
| `remove(Object key)` | **O(log n)** |
| `containsKey(Object key)` | **O(log n)** |
| `containsValue(Object value)` | **O(n)** |
| `size()` | **O(1)** |

### **2.5 `HashSet` (Backed by HashMap)**
| **Operation** | **Time Complexity** |
|--------------|---------------------|
| `add(E e)` | **O(1) Amortized**, **O(n) Worst** |
| `remove(Object o)` | **O(1) Amortized**, **O(n) Worst** |
| `contains(Object o)` | **O(1) Amortized**, **O(n) Worst** |
| `size()` | **O(1)** |

### **2.6 `TreeSet` (Backed by TreeMap)**
| **Operation** | **Time Complexity** |
|--------------|---------------------|
| `add(E e)` | **O(log n)** |
| `remove(Object o)` | **O(log n)** |
| `contains(Object o)` | **O(log n)** |
| `size()` | **O(1)** |

---

## **3. Best Choice Based on Use Cases**
| **Use Case** | **Best Choice** | **Why?** |
|-------------|---------------|---------|
| **Fastest access (index-based lookup)** | `ArrayList` | O(1) random access |
| **Fast insert/delete in middle** | `LinkedList` | O(1) insert/delete at head/tail |
| **Fast search using keys** | `HashMap` | O(1) average search |
| **Sorted key-value mapping** | `TreeMap` | O(log n) for sorted keys |
| **Unique elements, fast lookup** | `HashSet` | O(1) average case |
| **Sorted set of elements** | `TreeSet` | O(log n) for sorted unique elements |

---

## **4. Special Notes on Performance**
✅ **Resizing Cost in `ArrayList` & `HashMap`**
- `ArrayList` grows by **50%** when full.
- `HashMap` resizes **when load factor exceeds 0.75** (default behavior).

✅ **Worst Case for `HashMap`**
- If many collisions occur, it **degrades to O(n) time**.
- **Java 8+ uses Red-Black Trees for large collisions** to keep time **O(log n)**.

✅ **TreeMap vs HashMap**
- `TreeMap` is **slower (O(log n))** than `HashMap` (O(1)).
- Use `TreeMap` **only if sorting is required**.

---

## **5. Conclusion**
| **Collection** | **Best For** |
|--------------|------------|
| **ArrayList** | Fast access (O(1)), slow insert/remove (O(n)) |
| **LinkedList** | Fast insert/remove (O(1) at head/tail), slow access (O(n)) |
| **HashMap** | Fast key-value retrieval (O(1) avg, O(n) worst) |
| **TreeMap** | Sorted key-value retrieval (O(log n)) |
| **HashSet** | Fast unique element storage (O(1)) |
| **TreeSet** | Sorted unique elements (O(log n)) |

✅ **Use `HashMap` or `HashSet` when order doesn’t matter**.  
✅ **Use `TreeMap` or `TreeSet` when sorting is needed**.  
✅ **Use `ArrayList` for fast access, `LinkedList` for fast insertions/deletions**.

Would you like **code examples for performance testing**? 🚀

# Explain Sorting, Searching, and Ordering of all collection classes?
Sorting, searching, and ordering are **fundamental operations** in Java collections. Different collection classes have **different performance characteristics** for these operations.

---

## **1. Comparison Table: Sorting, Searching, and Ordering in Java Collections**
| **Collection**  | **Sorting Support** | **Searching Efficiency** | **Ordering Mechanism** |
|---------------|----------------|----------------|------------------|
| **ArrayList** | ✅ Supported using `Collections.sort()` | 🔍 **O(n) Linear Search**, **O(log n) Binary Search (if sorted)** | **Insertion Order** |
| **LinkedList** | ✅ Supported using `Collections.sort()` | 🔍 **O(n) Linear Search** | **Insertion Order** |
| **HashSet** | ❌ Not Sorted | 🔍 **O(1) Search (Avg), O(n) Worst Case** | **No Order (Unordered)** |
| **LinkedHashSet** | ❌ Not Sorted | 🔍 **O(1) Search (Avg), O(n) Worst Case** | **Insertion Order** |
| **TreeSet** | ✅ **Sorted by Default (Natural Order)** | 🔍 **O(log n) (Tree-based Search)** | **Sorted Order (Ascending by Default)** |
| **HashMap** | ❌ Not Sorted | 🔍 **O(1) Search (Avg), O(n) Worst Case** | **No Order (Unordered Keys)** |
| **LinkedHashMap** | ❌ Not Sorted | 🔍 **O(1) Search (Avg), O(n) Worst Case** | **Insertion Order (Maintained)** |
| **TreeMap** | ✅ **Sorted by Default (Natural Order of Keys)** | 🔍 **O(log n) (Tree-based Search)** | **Sorted Order (Keys Sorted by Natural Order or Comparator)** |

✅ **Key Observations:**
- `TreeSet` and `TreeMap` **automatically sort elements**.
- `ArrayList` and `LinkedList` **must be manually sorted**.
- `HashSet`, `HashMap` do **not maintain order**.
- `LinkedHashSet` and `LinkedHashMap` **maintain insertion order**.

---

## **2. Sorting Collections in Java**
Sorting can be done using:
- **Natural Order (`Comparable`)**
- **Custom Order (`Comparator`)**
- **`Collections.sort()` for Lists**
- **TreeSet & TreeMap (Sorted Automatically)**

### **2.1 Sorting `ArrayList` using `Collections.sort()`**
```java
import java.util.*;

public class SortingExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("Banana", "Apple", "Cherry"));
        
        Collections.sort(list); // Sorting in natural order
        System.out.println("Sorted List: " + list);
    }
}
```
✅ **Output**
```
Sorted List: [Apple, Banana, Cherry]
```
🔹 **Time Complexity**: O(n log n) (Merge Sort internally).

---

### **2.2 Sorting with a Custom `Comparator`**
```java
import java.util.*;

class Product {
    String name;
    int price;
    Product(String name, int price) {
        this.name = name;
        this.price = price;
    }
    public String toString() {
        return name + " - $" + price;
    }
}

// Comparator to sort by price
class PriceComparator implements Comparator<Product> {
    public int compare(Product p1, Product p2) {
        return Integer.compare(p1.price, p2.price);
    }
}

public class CustomSortingExample {
    public static void main(String[] args) {
        List<Product> products = new ArrayList<>();
        products.add(new Product("Laptop", 1000));
        products.add(new Product("Phone", 700));
        products.add(new Product("Tablet", 500));

        // Sorting by price using Comparator
        Collections.sort(products, new PriceComparator());
        System.out.println("Products sorted by Price: " + products);
    }
}
```
✅ **Output**
```
Products sorted by Price: [Tablet - $500, Phone - $700, Laptop - $1000]
```
🔹 **Use `Comparator` when sorting dynamically by different fields.**

---

## **3. Searching in Java Collections**
- **`contains()` → Linear search (O(n))**
- **`binarySearch()` → Requires sorted list (O(log n))**
- **`HashSet` & `HashMap` → O(1) (Best for fast lookups)**

### **3.1 Searching in an `ArrayList`**
```java
import java.util.*;

public class SearchingExample {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(10, 20, 30, 40, 50);

        // Searching using contains() (O(n))
        System.out.println("Contains 30? " + list.contains(30));

        // Searching using binarySearch() (O(log n)) - List must be sorted
        System.out.println("Index of 30: " + Collections.binarySearch(list, 30));
    }
}
```
✅ **Output**
```
Contains 30? true
Index of 30: 2
```
🔹 **Use `binarySearch()` when the list is sorted**.

---

### **3.2 Searching in a `HashSet` (O(1))**
```java
import java.util.*;

public class HashSetSearchExample {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>(Arrays.asList(10, 20, 30, 40, 50));

        System.out.println("Contains 30? " + set.contains(30)); // O(1) lookup
    }
}
```
✅ **Output**
```
Contains 30? true
```
🔹 **`HashSet` is best for quick searches (O(1))**.

---

### **3.3 Searching in a `TreeSet` (O(log n))**
```java
import java.util.*;

public class TreeSetSearchExample {
    public static void main(String[] args) {
        TreeSet<Integer> set = new TreeSet<>(Arrays.asList(10, 20, 30, 40, 50));

        System.out.println("Higher than 30: " + set.higher(30)); // Finds next higher element
    }
}
```
✅ **Output**
```
Higher than 30: 40
```
🔹 **`TreeSet` allows range queries efficiently**.

---

## **4. Ordering in Java Collections**
| **Collection** | **Ordering Type** | **Example** |
|---------------|----------------|----------------|
| `ArrayList` | **Insertion Order** | `[1, 2, 3]` remains `[1, 2, 3]` |
| `LinkedList` | **Insertion Order** | `[A, B, C]` remains `[A, B, C]` |
| `HashSet` | **Unordered** | `{B, A, C}` → Random order |
| `LinkedHashSet` | **Insertion Order** | `{1, 3, 2}` remains `{1, 3, 2}` |
| `TreeSet` | **Sorted Order** | `{5, 2, 8}` → `{2, 5, 8}` |
| `HashMap` | **Unordered Keys** | Keys are randomly ordered |
| `LinkedHashMap` | **Insertion Order of Keys** | `{2, 1, 3}` remains `{2, 1, 3}` |
| `TreeMap` | **Sorted Order of Keys** | `{B=2, A=1}` → `{A=1, B=2}` |

---

## **5. Conclusion**
✅ **Use `Collections.sort()` for sorting lists, and `TreeSet`/`TreeMap` for automatic sorting.**  
✅ **For fast searching, use `HashMap` or `HashSet` (`O(1)`).**  
✅ **For range-based searching, use `TreeSet` or `TreeMap` (`O(log n)`).**  
✅ **For maintaining insertion order, use `LinkedHashSet` and `LinkedHashMap`.**  

Would you like **benchmark comparisons of sorting/searching methods?** 🚀

# Explain Null and Duplicate Behavior of all Collection classes?
Different **Java Collection Framework (JCF) classes** handle `null` values and duplicate elements **differently**. Some allow **multiple `null` values**, while others **do not allow duplicates**.

---

## **1. Tabular Comparison: Null & Duplicate Handling in Java Collections**
| **Collection Class** | **Allows `null` values?** | **Allows Duplicates?** | **Ordering** |
|----------------------|----------------|-----------------|------------------|
| **ArrayList** | ✅ Yes (Multiple `null` values allowed) | ✅ Yes | **Insertion Order** |
| **LinkedList** | ✅ Yes (Multiple `null` values allowed) | ✅ Yes | **Insertion Order** |
| **HashSet** | ✅ Yes (Only one `null` allowed) | ❌ No | **Unordered** |
| **LinkedHashSet** | ✅ Yes (Only one `null` allowed) | ❌ No | **Insertion Order** |
| **TreeSet** | ❌ No (`NullPointerException` for `null`) | ❌ No | **Sorted Order** |
| **HashMap** | ✅ Yes (One `null` key, multiple `null` values) | ❌ No duplicate keys, ✅ Duplicate values allowed | **Unordered** |
| **LinkedHashMap** | ✅ Yes (One `null` key, multiple `null` values) | ❌ No duplicate keys, ✅ Duplicate values allowed | **Insertion Order** |
| **TreeMap** | ❌ No (`NullPointerException` for `null` keys, but allows `null` values) | ❌ No duplicate keys, ✅ Duplicate values allowed | **Sorted Order (Keys)** |

✅ **Key Observations:**
- **Lists (`ArrayList`, `LinkedList`)** allow **multiple `null` values** and **duplicates**.
- **Sets (`HashSet`, `LinkedHashSet`, `TreeSet`)** do **not allow duplicate elements**.
- **TreeSet and TreeMap** do **not allow `null` keys** because they require elements to be comparable.
- **Maps (`HashMap`, `LinkedHashMap`, `TreeMap`)** allow **one `null` key** but multiple `null` values.

---

## **2. Null & Duplicate Behavior for Each Collection Class**
### **2.1 `ArrayList` (Allows Duplicates & Multiple `null`s)**
```java
import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("A"); // Duplicate allowed
        list.add(null); // Multiple nulls allowed
        list.add(null);

        System.out.println("ArrayList: " + list);
    }
}
```
✅ **Output**
```
ArrayList: [A, B, A, null, null]
```
🔹 **Allows duplicates and multiple `null` values**.

---

### **2.2 `HashSet` (No Duplicates, Only One `null`)**
```java
import java.util.HashSet;
import java.util.Set;

public class HashSetExample {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("A");
        set.add("B");
        set.add("A"); // Duplicate ignored
        set.add(null); // One null allowed
        set.add(null); // Ignored

        System.out.println("HashSet: " + set);
    }
}
```
✅ **Output**
```
HashSet: [null, A, B]
```
🔹 **Duplicates are ignored, only one `null` allowed**.

---

### **2.3 `TreeSet` (No Nulls, No Duplicates)**
```java
import java.util.TreeSet;
import java.util.Set;

public class TreeSetExample {
    public static void main(String[] args) {
        Set<String> set = new TreeSet<>();
        set.add("A");
        set.add("B");
        set.add("A"); // Duplicate ignored
        // set.add(null); // Throws NullPointerException

        System.out.println("TreeSet: " + set);
    }
}
```
✅ **Output**
```
TreeSet: [A, B]
```
🔹 **Does NOT allow `null`, throws `NullPointerException`**.

---

### **2.4 `HashMap` (One `null` Key, Multiple `null` Values)**
```java
import java.util.HashMap;
import java.util.Map;

public class HashMapExample {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("A", "Apple");
        map.put("B", "Banana");
        map.put("A", "Avocado"); // Duplicate key, overrides value
        map.put(null, "NullValue"); // One null key allowed
        map.put("C", null); // Multiple null values allowed

        System.out.println("HashMap: " + map);
    }
}
```
✅ **Output**
```
HashMap: {null=NullValue, A=Avocado, B=Banana, C=null}
```
🔹 **Only one `null` key is allowed, but multiple `null` values are allowed**.

---

### **2.5 `TreeMap` (No Null Keys, Multiple Null Values)**
```java
import java.util.TreeMap;
import java.util.Map;

public class TreeMapExample {
    public static void main(String[] args) {
        Map<String, String> map = new TreeMap<>();
        map.put("A", "Apple");
        map.put("B", "Banana");
        // map.put(null, "NullValue"); // Throws NullPointerException
        map.put("C", null); // Null values are allowed

        System.out.println("TreeMap: " + map);
    }
}
```
✅ **Output**
```
TreeMap: {A=Apple, B=Banana, C=null}
```
🔹 **Does NOT allow `null` keys but allows `null` values**.

---

## **3. Summary of `null` and Duplicate Behavior**
| **Collection** | **Allows Null Values?** | **Allows Duplicates?** |
|--------------|------------------|------------------|
| **ArrayList** | ✅ Yes (Multiple `null`s) | ✅ Yes |
| **LinkedList** | ✅ Yes (Multiple `null`s) | ✅ Yes |
| **HashSet** | ✅ Yes (Only one `null`) | ❌ No |
| **LinkedHashSet** | ✅ Yes (Only one `null`) | ❌ No |
| **TreeSet** | ❌ No (`NullPointerException`) | ❌ No |
| **HashMap** | ✅ Yes (One `null` key, multiple `null` values) | ❌ No duplicate keys, ✅ Duplicate values allowed |
| **LinkedHashMap** | ✅ Yes (One `null` key, multiple `null` values) | ❌ No duplicate keys, ✅ Duplicate values allowed |
| **TreeMap** | ❌ No (`NullPointerException` for keys) | ❌ No duplicate keys, ✅ Duplicate values allowed |

---

## **4. Best Collection for Handling `null` and Duplicates**
| **Use Case** | **Best Collection** | **Reason** |
|-------------|----------------|----------------|
| **Allow Multiple `null`s and Duplicates** | `ArrayList`, `LinkedList` | Allows multiple `null`s and duplicates |
| **Allow One `null`, No Duplicates** | `HashSet`, `LinkedHashSet` | Allows one `null`, unique elements |
| **Do Not Allow `null`, Auto-Sorting** | `TreeSet`, `TreeMap` | Throws `NullPointerException` for keys |
| **Allow `null` Key, Unique Keys** | `HashMap`, `LinkedHashMap` | Allows one `null` key, multiple `null` values |

---

## **5. Conclusion**
✅ **Lists (`ArrayList`, `LinkedList`) allow multiple `null`s and duplicates**.  
✅ **Sets (`HashSet`, `LinkedHashSet`) allow only one `null` and no duplicates**.  
✅ **TreeSet & TreeMap do NOT allow `null` keys** due to sorting.  
✅ **Maps (`HashMap`, `LinkedHashMap`) allow one `null` key but multiple `null` values**.  

Would you like **performance benchmarks for different operations?** 🚀

# What is the difference between Fail-fast and Fail-safe Iterator?
Iterators in Java can be classified into **Fail-Fast** and **Fail-Safe** based on how they handle concurrent modifications during iteration.

---

## **1. Quick Comparison Table**
| **Feature**         | **Fail-Fast Iterator** | **Fail-Safe Iterator** |
|--------------------|---------------------|---------------------|
| **Definition**     | Throws `ConcurrentModificationException` if collection is modified during iteration | Allows modifications while iterating |
| **Thread-Safety** | ❌ **Not thread-safe** | ✅ **Thread-safe** |
| **Works on**      | `ArrayList`, `LinkedList`, `HashSet`, `HashMap` (default iterators) | `CopyOnWriteArrayList`, `ConcurrentHashMap` |
| **Modification Allowed?** | ❌ No (Modifications cause failure) | ✅ Yes (Does not throw exception) |
| **Performance**   | ✅ **Faster** (No extra memory) | ❌ **Slower** (Creates a copy) |
| **Best Use Case** | When modification during iteration is **not required** | When iteration must continue even if modified |

✅ **Key Observations:**
- **Fail-Fast Iterators detect structural modifications and throw `ConcurrentModificationException`.**
- **Fail-Safe Iterators operate on a cloned collection to avoid exceptions.**

---

## **2. Fail-Fast Iterator (Throws Exception on Modification)**
### **How Does Fail-Fast Work?**
- Uses an **internal modification count (`modCount`)**.
- If `modCount` changes **outside the iterator**, it throws `ConcurrentModificationException`.

### **Example of Fail-Fast Iterator (`ArrayList`)**
```java
import java.util.*;

public class FailFastExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("A", "B", "C"));

        Iterator<String> iterator = list.iterator();
        
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
            list.add("D"); // Modifying while iterating (Causes Exception)
        }
    }
}
```
❌ **Throws Exception:**
```
Exception in thread "main" java.util.ConcurrentModificationException
```
🔹 **Cause:** `list.add("D")` modifies the collection during iteration.

---

### **How to Prevent `ConcurrentModificationException`?**
#### ✅ **Solution 1: Use `Iterator.remove()` Instead of `List.remove()`**
```java
import java.util.*;

public class SafeIteratorExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("A", "B", "C"));

        Iterator<String> iterator = list.iterator();
        
        while (iterator.hasNext()) {
            String value = iterator.next();
            if (value.equals("B")) {
                iterator.remove(); // Safe removal
            }
        }
        System.out.println("Modified List: " + list);
    }
}
```
✅ **Output**
```
Modified List: [A, C]
```
🔹 **`Iterator.remove()` ensures safe modification**.

#### ✅ **Solution 2: Use `CopyOnWriteArrayList` (Fail-Safe Iterator)**
```java
import java.util.concurrent.CopyOnWriteArrayList;

public class SafeIterationExample {
    public static void main(String[] args) {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>(new String[]{"A", "B", "C"});

        for (String s : list) {
            list.add("D"); // No Exception (Fail-Safe)
        }
        System.out.println("Final List: " + list);
    }
}
```
✅ **Output**
```
Final List: [A, B, C, D, D, D]
```
🔹 **`CopyOnWriteArrayList` allows modification while iterating**.

---

## **3. Fail-Safe Iterator (No Exception on Modification)**
### **How Does Fail-Safe Work?**
- **Iterates over a cloned collection (snapshot)**.
- **Modification does not affect the original iterator**.

### **Fail-Safe Iterator Example (`ConcurrentHashMap`)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class FailSafeExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");

        for (Integer key : map.keySet()) {
            map.put(4, "Four"); // No Exception (Fail-Safe)
        }
        
        System.out.println("Final Map: " + map);
    }
}
```
✅ **Output**
```
Final Map: {1=One, 2=Two, 3=Three, 4=Four}
```
🔹 **Even though the map is modified, iteration continues safely**.

---

## **4. Where are Fail-Fast and Fail-Safe Iterators Used?**
| **Iterator Type** | **Example Collections** |
|------------------|--------------------|
| **Fail-Fast** | `ArrayList`, `LinkedList`, `HashSet`, `HashMap`, `TreeSet`, `TreeMap` |
| **Fail-Safe** | `CopyOnWriteArrayList`, `ConcurrentHashMap`, `ConcurrentLinkedQueue` |

---

## **5. Key Differences Between Fail-Fast and Fail-Safe**
| **Feature**         | **Fail-Fast Iterator** | **Fail-Safe Iterator** |
|--------------------|---------------------|---------------------|
| **Behavior** | Throws `ConcurrentModificationException` | Allows modification while iterating |
| **Modification Handling** | Does not allow modification | Works on a cloned collection |
| **Performance** | ✅ Faster | ❌ Slower (Cloning adds overhead) |
| **Thread-Safety** | ❌ Not thread-safe | ✅ Thread-safe |
| **Used In** | `ArrayList`, `LinkedList`, `HashSet`, `HashMap` | `CopyOnWriteArrayList`, `ConcurrentHashMap` |

---

## **6. When to Use Which Iterator?**
| **Scenario** | **Best Iterator** |
|------------|----------------|
| **Single-threaded environment** | Fail-Fast (Faster) |
| **Multi-threaded environment** | Fail-Safe (`ConcurrentHashMap`) |
| **Modification while iterating is required** | Fail-Safe (`CopyOnWriteArrayList`) |
| **High-performance iteration (no overhead)** | Fail-Fast (`ArrayList`) |

✅ **Fail-Fast is better when performance matters**.  
✅ **Fail-Safe is better when concurrent modifications are needed**.

---

## **7. Conclusion**
- **Fail-Fast Iterators** (`ArrayList`, `HashSet`, `HashMap`) detect modifications and throw **`ConcurrentModificationException`**.
- **Fail-Safe Iterators** (`CopyOnWriteArrayList`, `ConcurrentHashMap`) **work on a copy**, allowing modifications **without exceptions**.
- **Best Practice**: Use **Fail-Safe Iterators** in multi-threaded environments to prevent concurrency issues.

🚀 Would you like **performance benchmarks for Fail-Fast vs Fail-Safe Iteration?** 😃
# How to avoid ConcurrentModificationException?
The **`ConcurrentModificationException` (CME)** occurs when a **collection is modified while being iterated** using a **Fail-Fast Iterator** (like those of `ArrayList`, `HashSet`, `HashMap`, etc.).

---

## **1. When Does `ConcurrentModificationException` Occur?**
A `ConcurrentModificationException` occurs when:
- You modify a collection **while iterating over it** using an **Iterator**.
- You use a **Fail-Fast collection** (`ArrayList`, `HashMap`, etc.).
- Structural modifications are made **outside the iterator’s methods**.

### **Example of `ConcurrentModificationException`**
```java
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CMEExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String value = iterator.next();
            if (value.equals("B")) {
                list.remove(value); // ❌ Throws ConcurrentModificationException
            }
        }
    }
}
```
❌ **Output:**
```
Exception in thread "main" java.util.ConcurrentModificationException
```
🔹 **Cause:** `list.remove(value)` modifies the collection **outside the iterator**.

---

## **2. How to Avoid `ConcurrentModificationException`?**
### ✅ **Solution 1: Use `Iterator.remove()` Instead of `List.remove()`**
`Iterator.remove()` is the **safe way** to remove elements **during iteration**.

```java
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SafeRemoveExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String value = iterator.next();
            if (value.equals("B")) {
                iterator.remove(); // ✅ Safe removal
            }
        }
        System.out.println("Modified List: " + list);
    }
}
```
✅ **Output:**
```
Modified List: [A, C]
```
🔹 **Why?** `iterator.remove()` modifies the collection **safely without breaking the iteration**.

---

### ✅ **Solution 2: Use `CopyOnWriteArrayList` for Safe Iteration**
`CopyOnWriteArrayList` creates a **separate copy** of the list while iterating, preventing `ConcurrentModificationException`.

```java
import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteExample {
    public static void main(String[] args) {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        for (String value : list) {
            if (value.equals("B")) {
                list.remove(value); // ✅ No exception (Fail-Safe)
            }
        }
        System.out.println("Modified List: " + list);
    }
}
```
✅ **Output:**
```
Modified List: [A, C]
```
🔹 **Why?** `CopyOnWriteArrayList` **works on a cloned copy**, allowing safe modifications.

---

### ✅ **Solution 3: Use `ConcurrentHashMap` for Safe Iteration**
If using a `HashMap`, replace it with **`ConcurrentHashMap`**, which allows safe modifications **during iteration**.

```java
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");

        for (Integer key : map.keySet()) {
            if (key == 2) {
                map.remove(key); // ✅ No exception (Fail-Safe)
            }
        }
        System.out.println("Modified Map: " + map);
    }
}
```
✅ **Output:**
```
Modified Map: {1=One, 3=Three}
```
🔹 **Why?** `ConcurrentHashMap` allows **concurrent modifications without exceptions**.

---

### ✅ **Solution 4: Iterate Using `ListIterator` (For Lists)**
`ListIterator` allows **safe modifications** while iterating **in both forward and backward directions**.

```java
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");

        ListIterator<String> iterator = list.listIterator();
        while (iterator.hasNext()) {
            String value = iterator.next();
            if (value.equals("B")) {
                iterator.remove(); // ✅ Safe modification
            }
        }
        System.out.println("Modified List: " + list);
    }
}
```
✅ **Output:**
```
Modified List: [A, C]
```
🔹 **Why?** `ListIterator.remove()` **modifies the list safely during iteration**.

---

### ✅ **Solution 5: Use a `Stream` with `filter()` (Java 8+)**
Streams provide **functional-style iteration** and allow safe filtering **without modifying the original list**.

```java
import java.util.*;
import java.util.stream.Collectors;

public class StreamExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("A", "B", "C"));

        // Create a new filtered list
        List<String> modifiedList = list.stream()
                .filter(value -> !value.equals("B")) // ✅ Removes "B" safely
                .collect(Collectors.toList());

        System.out.println("Modified List: " + modifiedList);
    }
}
```
✅ **Output:**
```
Modified List: [A, C]
```
🔹 **Why?** Streams create a **new modified list**, preventing modification issues.

---

### ✅ **Solution 6: Collect Elements to Remove and Delete After Iteration**
If direct modification is not possible, collect elements to remove **in a separate list** and remove them **after iteration**.

```java
import java.util.*;

public class CollectAndRemoveExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("A", "B", "C"));

        List<String> toRemove = new ArrayList<>();
        for (String value : list) {
            if (value.equals("B")) {
                toRemove.add(value); // Collect elements to remove
            }
        }
        list.removeAll(toRemove); // Remove after iteration

        System.out.println("Modified List: " + list);
    }
}
```
✅ **Output:**
```
Modified List: [A, C]
```
🔹 **Why?** We avoid modifying the list **while iterating**, preventing `ConcurrentModificationException`.

---

## **3. Summary of Solutions**
| **Solution** | **Best for** | **Modification Allowed?** | **Performance** |
|-------------|-------------|---------------------|----------------|
| `Iterator.remove()` | Lists, Sets | ✅ Yes | ✅ Fast |
| `CopyOnWriteArrayList` | Lists (Multi-threading) | ✅ Yes | ❌ Slower (Copies List) |
| `ConcurrentHashMap` | Maps (Multi-threading) | ✅ Yes | ✅ Fast |
| `ListIterator.remove()` | Lists (Forward & Backward) | ✅ Yes | ✅ Fast |
| Streams (`filter()`) | Functional-style operations | ❌ No (Creates New List) | ✅ Fast |
| Collect & Remove Later | Any Collection | ✅ Yes | ✅ Moderate |

---

## **4. Conclusion**
- **Use `Iterator.remove()`** if modifying during iteration.
- **Use `CopyOnWriteArrayList` / `ConcurrentHashMap`** for **thread-safe modification**.
- **Use `Streams` or `Collect & Remove Later`** for **functional-style safe iteration**.
- **Avoid modifying collections directly during iteration (Fail-Fast behavior).**

🚀 **Next Steps?** Would you like **performance benchmarks for these solutions?** 😃

# How to avoid Thread Race Conditions?
A **race condition** occurs when multiple threads **access and modify shared resources concurrently**, leading to **unexpected behavior**.

---

## **1. What is a Race Condition?**
A race condition happens when:
- **Multiple threads** access a shared variable **without synchronization**.
- **The final result depends on the timing of thread execution**.

### **Example of a Race Condition**
```java
class Counter {
    int count = 0;

    void increment() {
        count++; // Not thread-safe
    }
}

public class RaceConditionExample {
    public static void main(String[] args) {
        Counter counter = new Counter();

        // Creating multiple threads that increment the counter
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) counter.increment();
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) counter.increment();
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Final Count: " + counter.count); // ❌ May not be 2000
    }
}
```
❌ **Issue**: The expected output is `2000`, but due to race conditions, it may be **less than 2000**.

---

## **2. How to Avoid Race Conditions?**
### ✅ **Solution 1: Use `synchronized` Keyword**
Synchronizing the method ensures **only one thread executes it at a time**.

```java
class Counter {
    int count = 0;

    synchronized void increment() { // Synchronized method
        count++;
    }
}
```
✅ **Benefits:** Prevents simultaneous access to `increment()`.

---

### ✅ **Solution 2: Use `synchronized` Block (More Efficient)**
Instead of synchronizing the entire method, **only synchronize the critical section**.

```java
class Counter {
    int count = 0;

    void increment() {
        synchronized (this) { // Lock only critical section
            count++;
        }
    }
}
```
✅ **Benefits:** Better performance than synchronizing the entire method.

---

### ✅ **Solution 3: Use `ReentrantLock` (More Control Over Locking)**
`ReentrantLock` provides **more control** over locking, including **tryLock() and fair locking**.

```java
import java.util.concurrent.locks.ReentrantLock;

class Counter {
    int count = 0;
    private final ReentrantLock lock = new ReentrantLock();

    void increment() {
        lock.lock();
        try {
            count++;
        } finally {
            lock.unlock(); // Always release the lock
        }
    }
}
```
✅ **Benefits:** Prevents deadlocks, allows manual unlocking.

---

### ✅ **Solution 4: Use `AtomicInteger` for Thread-Safe Increment**
`AtomicInteger` provides **lock-free thread safety**.

```java
import java.util.concurrent.atomic.AtomicInteger;

class Counter {
    AtomicInteger count = new AtomicInteger(0);

    void increment() {
        count.incrementAndGet(); // Atomic operation
    }
}
```
✅ **Benefits:** Faster than locks, no blocking.

---

### ✅ **Solution 5: Use `volatile` for Visibility (Not Synchronization)**
`volatile` ensures that **changes are visible across threads**, but does **not prevent race conditions**.

```java
class Counter {
    volatile int count = 0;

    void increment() {
        count++; // ❌ Still Not Thread-Safe (Needs Synchronization)
    }
}
```
❌ **Issue:** Does not prevent **simultaneous modifications**.

✅ **Use `volatile` for flags**, not counters.

---

### ✅ **Solution 6: Use `ThreadLocal` for Per-Thread Storage**
Each thread gets its **own copy** of a variable.

```java
class ThreadLocalExample {
    static ThreadLocal<Integer> threadLocalCounter = ThreadLocal.withInitial(() -> 0);

    public static void main(String[] args) {
        Runnable task = () -> {
            threadLocalCounter.set(threadLocalCounter.get() + 1);
            System.out.println(Thread.currentThread().getName() + ": " + threadLocalCounter.get());
        };

        Thread t1 = new Thread(task);
        Thread t2 = new Thread(task);

        t1.start();
        t2.start();
    }
}
```
✅ **Benefits:** Each thread **has its own counter**, no race conditions.

---

### ✅ **Solution 7: Use `Concurrent Collections` (`ConcurrentHashMap`, `CopyOnWriteArrayList`)**
Java provides **thread-safe collections** to prevent race conditions.

```java
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMapExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();
        map.put(1, "One");

        new Thread(() -> map.put(2, "Two")).start();
        new Thread(() -> map.put(3, "Three")).start();

        System.out.println("Final Map: " + map);
    }
}
```
✅ **Benefits:** Safe for concurrent updates.

---

## **3. Performance Comparison of Different Approaches**
| **Method** | **Thread-Safety?** | **Performance** | **Best Use Case** |
|-----------|----------------|---------------|------------------|
| **Synchronized Method** | ✅ Yes | ❌ Slower (Locks entire method) | Simple cases with low contention |
| **Synchronized Block** | ✅ Yes | ✅ Better (Locks only critical section) | Moderate contention |
| **ReentrantLock** | ✅ Yes | ✅ Fine-grained control | When manual locking/unlocking is needed |
| **AtomicInteger** | ✅ Yes | ✅ Fastest (No blocking) | Simple counters |
| **volatile** | ❌ No (Visibility only) | ✅ Fast | Flags (not for increments) |
| **ThreadLocal** | ✅ Yes | ✅ No sharing | Per-thread storage |
| **ConcurrentHashMap** | ✅ Yes | ✅ High-performance | Safe concurrent access |

---

## **4. Conclusion**
| **Best Approach?** | **Use When...** |
|------------------|----------------|
| `synchronized` method/block | Simple cases with moderate contention |
| `ReentrantLock` | Fine-grained locking is required |
| `AtomicInteger` | Simple atomic operations (best for counters) |
| `volatile` | Ensuring visibility of flags (not for incrementing values) |
| `ThreadLocal` | Each thread should have its own copy of a variable |
| `Concurrent Collections` | Multi-threaded environments with shared collections |

✅ **Use `AtomicInteger` for simple counters**.  
✅ **Use `Concurrent Collections` for shared data structures**.  
✅ **Use `synchronized` or `ReentrantLock` for complex thread coordination**.

🚀 **Need a detailed benchmark comparison of these approaches?** 😃

# What is the difference between Synchronized and Concurrent Collections?
Java provides two types of thread-safe collections: **Synchronized Collections** (legacy synchronization) and **Concurrent Collections** (modern, scalable alternatives).

---

## **1. Quick Comparison Table**
| Feature               | **Synchronized Collections** | **Concurrent Collections** |
|-----------------------|----------------------------|----------------------------|
| **Definition**        | Uses **synchronization (`synchronized`)** to make collections thread-safe | Uses **non-blocking, lock-free algorithms** for better concurrency |
| **Package**          | `java.util.Collections.synchronizedX()` | `java.util.concurrent` package |
| **Performance**       | ❌ **Slower (Locks entire collection)** | ✅ **Faster (Uses segment-level locking or non-blocking methods)** |
| **Concurrency Level** | ❌ **Low (Single thread at a time)** | ✅ **High (Multiple threads can access different parts)** |
| **Examples**          | `Collections.synchronizedList(new ArrayList<>())` | `ConcurrentHashMap`, `CopyOnWriteArrayList`, `ConcurrentLinkedQueue` |
| **Best Use Case**     | When **only one thread modifies at a time** | When **multiple threads modify simultaneously** |

✅ **Key Observations:**
- **Synchronized Collections** use **global locks** (`synchronized`).
- **Concurrent Collections** use **segment locks, lock-free algorithms, or copy-on-write** for better performance.

---

## **2. Synchronized Collections (Global Lock)**
Java provides **synchronized wrappers** for standard collections.

### **How to Create Synchronized Collections?**
```java
import java.util.*;

public class SynchronizedCollectionExample {
    public static void main(String[] args) {
        List<String> list = Collections.synchronizedList(new ArrayList<>());
        Map<Integer, String> map = Collections.synchronizedMap(new HashMap<>());
        Set<String> set = Collections.synchronizedSet(new HashSet<>());
    }
}
```

### **Problems with Synchronized Collections**
1. **Global Locking:** Only one thread **can access the collection at a time**, reducing performance.
2. **Iterator is NOT Thread-Safe:** Even though the collection is synchronized, iterators still **throw `ConcurrentModificationException`**.

```java
List<String> list = Collections.synchronizedList(new ArrayList<>());
list.add("A");
list.add("B");

for (String value : list) { // ❌ May throw ConcurrentModificationException
    list.add("C"); // Modifying while iterating
}
```

### **Best Use Case for Synchronized Collections**
- When **only one thread modifies** at a time.
- When **multi-threaded reads are allowed, but modifications are rare**.

---

## **3. Concurrent Collections (Better Performance)**
The **`java.util.concurrent`** package provides high-performance thread-safe collections.

### **Examples of Concurrent Collections**
| **Collection Type** | **Synchronized Collection** | **Concurrent Collection (Better Alternative)** |
|--------------------|--------------------------|----------------------------------|
| **List** | `Collections.synchronizedList(new ArrayList<>())` | `CopyOnWriteArrayList` |
| **Set** | `Collections.synchronizedSet(new HashSet<>())` | `ConcurrentSkipListSet` |
| **Queue** | `Collections.synchronizedList(new LinkedList<>())` | `ConcurrentLinkedQueue`, `ArrayBlockingQueue` |
| **Map** | `Collections.synchronizedMap(new HashMap<>())` | `ConcurrentHashMap` |

---

## **4. How `ConcurrentHashMap` is Better than `SynchronizedMap`**
### **Synchronized Map (Global Lock)**
```java
Map<Integer, String> map = Collections.synchronizedMap(new HashMap<>());

synchronized (map) { // Explicit synchronization needed for iteration
    for (Map.Entry<Integer, String> entry : map.entrySet()) {
        System.out.println(entry.getKey() + " -> " + entry.getValue());
    }
}
```
❌ **Problem:** Uses a **single lock** for all operations → Slower performance.

---

### **ConcurrentHashMap (Segmented Locks)**
```java
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMapExample {
    public static void main(String[] args) {
        ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();
        map.put(1, "One");
        map.put(2, "Two");

        // No need for explicit synchronization
        for (Integer key : map.keySet()) {
            map.put(3, "Three"); // ✅ No exception (Fail-Safe)
        }

        System.out.println("Final Map: " + map);
    }
}
```
✅ **Why `ConcurrentHashMap` is Faster?**
- Uses **bucket-level locks instead of global lock**.
- **Multiple threads can modify different buckets at the same time**.
- **Iterator is Fail-Safe** (modifications do not throw `ConcurrentModificationException`).

---

## **5. How `CopyOnWriteArrayList` is Better than `SynchronizedList`**
### **Synchronized List (Locks on Every Operation)**
```java
import java.util.*;

public class SynchronizedListExample {
    public static void main(String[] args) {
        List<String> list = Collections.synchronizedList(new ArrayList<>());
        
        synchronized (list) { // Manual synchronization needed for iteration
            for (String item : list) {
                System.out.println(item);
            }
        }
    }
}
```
❌ **Problem:** Every read operation needs explicit synchronization.

---

### **CopyOnWriteArrayList (No Locks for Read)**
```java
import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteExample {
    public static void main(String[] args) {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();
        list.add("A");
        list.add("B");

        for (String value : list) {
            list.add("C"); // ✅ No exception (Fail-Safe)
        }

        System.out.println("Final List: " + list);
    }
}
```
✅ **Why `CopyOnWriteArrayList` is Better?**
- **Read operations are lock-free** (Fast reads).
- **Modifications create a new copy**, avoiding race conditions.
- **Iterator is Fail-Safe** (No `ConcurrentModificationException`).

---

## **6. Performance Comparison: Synchronized vs Concurrent Collections**
| **Operation** | **Synchronized Collection** | **Concurrent Collection** |
|--------------|----------------|----------------|
| **Read Performance** | ❌ **Slower** (Locks every read) | ✅ **Faster** (Lock-free reads) |
| **Write Performance** | ❌ **Slower** (Global Lock) | ✅ **Faster** (Segmented Locks / Copy-on-Write) |
| **Concurrent Access** | ❌ **Single-threaded writes** | ✅ **Multi-threaded writes** |
| **Iterator Behavior** | ❌ Fail-Fast (`ConcurrentModificationException`) | ✅ Fail-Safe (No exceptions) |
| **Best for** | Small multi-threaded applications | High-performance concurrent applications |

✅ **Key Takeaways:**
- **Use `ConcurrentHashMap` instead of `synchronizedMap()`**.
- **Use `CopyOnWriteArrayList` instead of `synchronizedList()`**.
- **Use `ConcurrentLinkedQueue` instead of `synchronizedList()` for queue operations**.

---

## **7. When to Use Synchronized vs Concurrent Collections?**
| **Use Case** | **Best Collection** |
|-------------|----------------|
| **Read-heavy, few writes** | `CopyOnWriteArrayList`, `CopyOnWriteArraySet` |
| **High concurrency (multi-threaded updates)** | `ConcurrentHashMap`, `ConcurrentLinkedQueue` |
| **Single-threaded writes, multiple reads** | `Collections.synchronizedList()`, `Collections.synchronizedMap()` |
| **Multiple threads adding/removing from a queue** | `ConcurrentLinkedQueue`, `BlockingQueue` |
| **Fast multi-threaded sorted set** | `ConcurrentSkipListSet` |

---

## **8. Conclusion**
| **Feature** | **Synchronized Collections** | **Concurrent Collections** |
|------------|----------------------|----------------------|
| **Performance** | ❌ Slower (Global Lock) | ✅ Faster (Fine-grained Locks) |
| **Read Efficiency** | ❌ Slow (Explicit Locks) | ✅ Fast (Lock-Free Reads) |
| **Thread Safety** | ✅ Yes (Locks Entire Collection) | ✅ Yes (Fine-Grained Locking) |
| **Iterator** | ❌ Fail-Fast (`ConcurrentModificationException`) | ✅ Fail-Safe (No Exception) |
| **Use Case** | Small-scale multi-threaded apps | High-performance multi-threading |

✅ **Use `Concurrent Collections` for better performance in multi-threaded applications**.  
✅ **Use `Synchronized Collections` only when modifications are rare**.

🚀 **Next Steps?** Would you like a **benchmark comparison of different concurrent collections?** 😃

# What is the difference between Collection and Collections?
In Java, **`Collection`** and **`Collections`** are two different concepts, despite their similar names.

---

## **1. Quick Comparison Table**
| Feature | `Collection` | `Collections` |
|---------|-------------|--------------|
| **Type** | **Interface** | **Utility Class** |
| **Package** | `java.util` | `java.util` |
| **Usage** | Represents a group of objects (root interface for Java collections) | Provides **utility methods** for working with collections |
| **Implemented By** | `List`, `Set`, `Queue` | No implementation (Static utility methods) |
| **Methods Defined** | `add()`, `remove()`, `size()`, `iterator()` | `sort()`, `reverse()`, `synchronizedList()`, `max()`, `min()`, `binarySearch()` |
| **Example** | `List<String> list = new ArrayList<>();` | `Collections.sort(list);` |

✅ **Key Observations:**
- **`Collection` is an interface** that defines common operations for data structures.
- **`Collections` is a utility class** that provides helper methods for working with collections.

---

## **2. What is `Collection`? (Interface)**
`Collection<E>` is the **root interface** for all Java collection types (`List`, `Set`, `Queue`).

### **Hierarchy of `Collection` Interface**
```
Collection<E>
├── List<E>  (ArrayList, LinkedList, Vector, Stack)
├── Set<E>   (HashSet, LinkedHashSet, TreeSet)
├── Queue<E> (PriorityQueue, LinkedList, Deque)
```

### **Example Usage of `Collection`**
```java
import java.util.*;

public class CollectionExample {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>(); // Can be any List, Set, or Queue
        collection.add("A");
        collection.add("B");
        collection.add("C");

        System.out.println("Collection: " + collection);
    }
}
```
✅ **Output**
```
Collection: [A, B, C]
```
🔹 **Why Use `Collection`?**  
- Allows **flexibility** (you can switch between `ArrayList`, `HashSet`, etc.).
- Provides **basic methods** like `add()`, `remove()`, `size()`, `contains()`.

---

## **3. What is `Collections`? (Utility Class)**
`Collections` is a **final utility class** in `java.util` that contains **static methods** for manipulating collections.

### **Common Methods in `Collections`**
| **Method** | **Usage** |
|-----------|----------|
| `sort(List<T>)` | Sorts a list in natural order |
| `reverse(List<T>)` | Reverses the elements in a list |
| `binarySearch(List<T>, T key)` | Searches a sorted list using binary search |
| `synchronizedList(List<T>)` | Returns a synchronized version of a list |
| `max(Collection<T>)` | Returns the maximum element in a collection |
| `min(Collection<T>)` | Returns the minimum element in a collection |

---

### **Example: Using `Collections.sort()`**
```java
import java.util.*;

public class CollectionsExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("Banana", "Apple", "Cherry"));

        Collections.sort(list); // Sorting the list
        System.out.println("Sorted List: " + list);

        Collections.reverse(list); // Reversing the list
        System.out.println("Reversed List: " + list);
    }
}
```
✅ **Output**
```
Sorted List: [Apple, Banana, Cherry]
Reversed List: [Cherry, Banana, Apple]
```
🔹 **Why Use `Collections`?**  
- Provides **utility methods** to work with collections efficiently.
- Reduces boilerplate code.

---

## **4. Key Differences Explained**
### **4.1 `Collection` is a Parent Interface, `Collections` is a Helper Class**
```java
// Collection (Interface)
Collection<Integer> numbers = new ArrayList<>();
numbers.add(10);
numbers.add(20);
System.out.println(numbers); // [10, 20]

// Collections (Utility Class)
Collections.sort((List<Integer>) numbers);
System.out.println(numbers); // [10, 20] (Sorted)
```

### **4.2 `Collection` Defines Behavior, `Collections` Provides Helpers**
- `Collection` **defines** methods like `add()`, `remove()`, `size()`, `iterator()`.
- `Collections` **provides** methods like `sort()`, `shuffle()`, `synchronizedList()`.

---

## **5. When to Use What?**
| **Use Case** | **Use `Collection`** | **Use `Collections`** |
|-------------|----------------------|----------------------|
| Define a **List, Set, or Queue** | ✅ `Collection<String> col = new ArrayList<>();` | ❌ Not applicable |
| Add/remove elements | ✅ `collection.add("A");` | ❌ Not applicable |
| Sort, reverse, or shuffle a collection | ❌ Not applicable | ✅ `Collections.sort(list);` |
| Get min/max value in a collection | ❌ Not applicable | ✅ `Collections.max(list);` |
| Synchronize a collection | ❌ Not applicable | ✅ `Collections.synchronizedList(list);` |

✅ **Use `Collection` for defining data structures, and `Collections` for working with them.**

---

## **6. Conclusion**
| Feature | `Collection` | `Collections` |
|---------|-------------|--------------|
| **Type** | Interface | Utility Class |
| **Purpose** | Defines standard methods for all collections | Provides static helper methods for collections |
| **Usage** | Create and manage a collection | Perform operations on collections (sort, reverse, etc.) |
| **Examples** | `List`, `Set`, `Queue` | `sort()`, `reverse()`, `binarySearch()` |


# What is the difference between hashCode() and equals() method contract?
In Java, `hashCode()` and `equals()` are **closely related** and follow a strict contract to ensure correct behavior in **hash-based collections** like `HashMap`, `HashSet`, and `HashTable`.

---

## **1. Quick Comparison Table**
| **Feature** | **`hashCode()`** | **`equals()`** |
|------------|----------------|---------------|
| **Purpose** | Returns an **integer hash code** for the object | Checks if **two objects are equal** |
| **Defined In** | `Object` class (`public native int hashCode()`) | `Object` class (`public boolean equals(Object obj)`) |
| **Return Type** | `int` (numeric hash code) | `boolean` (`true` if objects are equal, `false` otherwise) |
| **Default Behavior** | Returns unique integer for each object | Compares memory references (same as `==`) |
| **When to Override?** | When using objects in **hash-based collections** (`HashMap`, `HashSet`) | When defining **custom object equality** |
| **Contract with `equals()`** | If two objects are **equal (`equals()` returns `true`)**, their `hashCode()` **must be the same** | If two objects **have the same `hashCode()`**, they **may not be equal** |
| **Best Use Case** | Used for **efficient retrieval** in hash-based collections | Used for **logical equality comparison** |

✅ **Key Observations:**
- **`equals()` determines equality**, while **`hashCode()` helps in hashing-based collections**.
- **If `equals()` returns `true`, `hashCode()` must be the same**.
- **Two objects with the same `hashCode()` may not be equal** (hash collision).

---

## **2. `hashCode()` and `equals()` Contract**
1. **If two objects are equal (`a.equals(b) == true`), then their hash codes must be the same (`a.hashCode() == b.hashCode()`).**
2. **If two objects have the same `hashCode()`, they may or may not be equal.**
3. **Overriding `equals()` without overriding `hashCode()` can break hash-based collections (`HashSet`, `HashMap`).**

---

## **3. Example of Incorrect `hashCode()` and `equals()` Implementation**
🔴 **Problem: Without Overriding `hashCode()`**
```java
import java.util.HashSet;
import java.util.Objects;

class Employee {
    int id;
    String name;

    Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Employee employee = (Employee) obj;
        return id == employee.id && name.equals(employee.name);
    }
}

public class HashCodeProblem {
    public static void main(String[] args) {
        HashSet<Employee> employees = new HashSet<>();
        Employee e1 = new Employee(1, "Alice");
        Employee e2 = new Employee(1, "Alice");

        employees.add(e1);
        employees.add(e2); // ❌ Should not add duplicate, but it does!

        System.out.println("Employee Set Size: " + employees.size()); // Output: 2 (Incorrect!)
    }
}
```
❌ **Issue:**  
- `e1.equals(e2) == true`, but **they have different `hashCode()` values (default `Object.hashCode()`)**.
- `HashSet` treats them as different objects → **Incorrect duplicate detection**.

---

## **4. Correct `hashCode()` and `equals()` Implementation**
✅ **Solution: Override Both Methods**
```java
import java.util.HashSet;
import java.util.Objects;

class Employee {
    int id;
    String name;

    Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Employee employee = (Employee) obj;
        return id == employee.id && name.equals(employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name); // Generates consistent hash
    }
}

public class HashCodeCorrect {
    public static void main(String[] args) {
        HashSet<Employee> employees = new HashSet<>();
        Employee e1 = new Employee(1, "Alice");
        Employee e2 = new Employee(1, "Alice");

        employees.add(e1);
        employees.add(e2); // ✅ Now correctly detects duplicate

        System.out.println("Employee Set Size: " + employees.size()); // Output: 1 (Correct!)
    }
}
```
✅ **Fix:**  
- **`hashCode()` returns the same value for equal objects**.
- **`equals()` ensures logical equality**.

---

## **5. When to Override `hashCode()` and `equals()`**
| **Scenario** | **Should Override?** | **Why?** |
|-------------|-------------------|---------|
| **Using objects in `HashSet`, `HashMap`, `HashTable`** | ✅ Yes | Ensures correct hash-based lookups |
| **Comparing objects based on logical equality** | ✅ Yes | Ensures meaningful comparison |
| **Objects do not need equality comparison** | ❌ No | Default `Object.equals()` is sufficient |

---

## **6. Real-World Example: HashMap Key Lookup**
```java
import java.util.HashMap;
import java.util.Objects;

class Person {
    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Person person = (Person) obj;
        return age == person.age && name.equals(person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

public class HashMapKeyExample {
    public static void main(String[] args) {
        HashMap<Person, String> map = new HashMap<>();
        Person p1 = new Person("John", 30);
        Person p2 = new Person("John", 30);

        map.put(p1, "Developer");
        System.out.println(map.get(p2)); // ✅ Works because hashCode() is correct

        System.out.println("Map Size: " + map.size()); // ✅ 1 (Correct!)
    }
}
```
✅ **Why?**
- **Without `hashCode()`, `p2` would not find `p1` in the map.**
- `hashCode()` ensures `p1` and `p2` fall into the **same hash bucket**.

---

## **7. Summary of `hashCode()` and `equals()`**
| **Rule** | **Description** |
|---------|--------------|
| **1. Equal objects must have the same `hashCode()`** | `a.equals(b) == true → a.hashCode() == b.hashCode()` |
| **2. Objects with the same `hashCode()` may or may not be equal** | `a.hashCode() == b.hashCode()` does **not guarantee `a.equals(b)`** (hash collisions can occur) |
| **3. Always override `hashCode()` when overriding `equals()`** | Ensures correct behavior in hash-based collections |

---

## **8. Conclusion**
| **Feature** | **`hashCode()`** | **`equals()`** |
|------------|----------------|----------------|
| **Purpose** | Generates **hash code for fast lookups** | Defines **logical equality** |
| **Required for** | **Hash-based collections** (`HashMap`, `HashSet`) | **Object comparison (`==` replacement)** |
| **Contract** | Same `hashCode()` for equal objects | Must return `true` if objects are logically equal |
| **Best Practice** | Override when using in `HashMap` or `HashSet` | Override when checking object equality |

🚀 **Takeaway**: **Always override `hashCode()` when overriding `equals()`** to ensure correct behavior in hash-based collections.
