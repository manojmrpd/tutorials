# Java-8 Features

# Table of Contents

### 1. Introduction
- [What are Java 8 Features?](#what-are-java-8-features)

### 2. Lambda Expressions and Functional Interfaces
- [What is Lambda Expressions?](#what-is-lambda-expressions)
- [What is Functional Interfaces?](#what-is-functional-interfaces)
- [What are Pre-defined Functional Interfaces in Java?](#what-are-pre-defined-functional-interfaces-in-java)
- [What is the difference between Consumer vs BiConsumer?](#what-is-the-difference-between-consumer-vs-biconsumer)
- [What is Supplier?](#what-is-supplier)
- [What is the difference between Function vs BiFunction?](#what-is-the-difference-between-function-vs-bifunction)
- [What is the difference between UnaryOperator vs BinaryOperator?](#what-is-the-difference-between-unaryoperator-vs-binaryoperator)

### 3. Method References
- [What is Method References?](#what-is-method-references)
- [What is Static Method Reference?](#what-is-static-method-reference)
- [What is Instance Method Reference of a Particular Object?](#what-is-instance-method-reference-of-a-particular-object)
- [What is Instance Method Reference of an Arbitrary Object of a Particular Type?](#what-is-instance-method-reference-of-an-arbitrary-object-of-a-particular-type)
- [What is Constructor Reference?](#what-is-constructor-reference)

### 4. Optional API
- [What is Optional Interface?](#what-is-optional-interface)
- [What is Optional.of()?](#what-is-optionalof)
- [What is Optional.ofNullable()?](#what-is-optionalofnullable)
- [What is Optional.empty()?](#what-is-optionalempty)
- [What is Optional.get()?](#what-is-optionalget)
- [What is Optional.isPresent()?](#what-is-optionalispresent)
- [What is Optional.isEmpty()?](#what-is-optionalisempty)
- [What is Optional.filter()?](#what-is-optionalfilter)
- [What is Optional.map()?](#what-is-optionalmap)
- [What is Optional.flatMap()?](#what-is-optionalflatmap)
- [What is Optional.ifPresent()?](#what-is-optionalifpresent)
- [What is Optional.ifPresentOrElse()?](#what-is-optionalifpresentorelse)
- [What is Optional.orElse()?](#what-is-optionalorelse)
- [What is Optional.orElseGet()?](#what-is-optionalorelseget)
- [What is Optional.orElseThrow()?](#what-is-optionalorelsethrow)

### 5. Streams API
- [What is Streams API?](#what-is-streams-api)
- [What is Intermediate Operations in Java Stream API?](#what-is-intermediate-operations-in-java-stream-api)
- [What is Terminal Operations in Java Stream API?](#what-is-terminal-operations-in-java-stream-api)

### 6. Intermediate Operations
- [What is filter()?](#what-is-filter)
- [What is map(), mapToInt(), mapToLong(), mapToDouble()?](#what-is-map-maptoint-maptolong-maptodouble)
- [What is flatMap(), flatMapToInt(), flatMapToLong(), flatMapToDouble()?](#what-is-flatmap-flatmaptoint-flatmaptolong-flatmaptodouble)
- [What is distinct()?](#what-is-distinct)
- [What is sorted()?](#what-is-sorted)
- [What is skip()?](#what-is-skip)
- [What is limit()?](#what-is-limit)
- [What is peek()?](#what-is-peek)

### 7. Terminal Operations
- [What is forEach()?](#what-is-foreach)
- [What is reduce()?](#what-is-reduce)
- [What is sum()?](#what-is-sum)
- [What is min()?](#what-is-min)
- [What is max()?](#what-is-max)
- [What is average()?](#what-is-average)
- [What is count()?](#what-is-count)
- [What is findFirst()?](#what-is-findfirst)
- [What is findAny()?](#what-is-findany)
- [What is allMatch()?](#what-is-allmatch)
- [What is noneMatch()?](#what-is-nonematch)
- [What is anyMatch()?](#what-is-anymatch)
- [What is Terminal Operations - collect()?](#what-is-terminal-operations---collect)

### 8. Collectors
- [What is Collectors.toArray()?](#what-is-collectorstotoarray)
- [What is Collectors.toCollection()?](#what-is-collectorstocollection)
- [What is Collectors.toList()?](#what-is-collectorstolist)
- [What is Collectors.toSet()?](#what-is-collectorstoset)
- [What is Collectors.toMap()?](#what-is-collectorstomap)
- [What is Collectors.toUnmodifiableList()?](#what-is-collectorstounmodifiablelist)
- [What is Collectors.toUnmodifiableSet()?](#what-is-collectorstounmodifiableset)
- [What is Collectors.toUnmodifiableMap()?](#what-is-collectorstounmodifiablemap)
- [What is Collectors.groupingBy()?](#what-is-collectorsgroupingby)
- [What is Collectors.groupingByConcurrent()?](#what-is-collectorsgroupingbyconcurrent)
- [What is Collectors.partitioningBy()?](#what-is-collectorspartitioningby)
- [What is Collectors.mapping()?](#what-is-collectorsmapping)
- [What is Collectors.flatMapping()?](#what-is-collectorsflatmapping)
- [What is Collectors.averaging()?](#what-is-collectorsaveraging)
- [What is Collectors.summing()?](#what-is-collectorssumming)
- [What is Collectors.minBy()?](#what-is-collectorsminby)
- [What is Collectors.maxBy()?](#what-is-collectorsmaxby)
- [What is Collectors.counting()?](#what-is-collectorscounting)
- [What is Collectors.joining()?](#what-is-collectorsjoining)
- [What is Collectors.collectingAndThen()?](#what-is-collectorscollectingandthen)

### 9. Comparator
- [What is Comparator?](#what-is-comparator)
- [What is Comparator.comparing()?](#what-is-comparatorcomparing)
- [What is Comparator.comparingInt()?](#what-is-comparatorcomparingint)
- [What is Comparator.comparingLong()?](#what-is-comparatorcomparinglong)
- [What is Comparator.comparingDouble()?](#what-is-comparatorcomparingdouble)
- [What is Comparator.naturalOrder()?](#what-is-comparatornaturalorder)
- [What is Comparator.reverseOrder()?](#what-is-comparatorreverseorder)
- [What is Comparator.thenComparing()?](#what-is-comparatorthencomparing)

### 10. Parallel Streams
- [What is the difference between Parallel Stream vs Sequential Stream?](#what-is-the-difference-between-parallel-stream-vs-sequential-stream)
- [Explain Internal Working of Parallel Stream?](#explain-internal-working-of-parallel-stream)
- [What is Task Splitting using SplitIterator?](#what-is-task-splitting-using-splititerator)
- [What is Task Submission using ForkJoinPool?](#what-is-task-submission-using-forkjoinpool)

# What are Java 8 Features?
Java 8 is a significant release of the Java programming language, introducing many new features that make coding easier, cleaner, and more efficient. Below is a detailed explanation of the major features introduced in Java 8, along with examples and explanations.

---

## **1. Lambda Expressions**
Lambda expressions are a way to define anonymous functions in Java. They enable functional programming by allowing functions to be passed as arguments or returned as values.

### **Syntax:**
```java
(parameters) -> expression
```
or
```java
(parameters) -> { statements; }
```

### **Example:**
```java
import java.util.Arrays;
import java.util.List;

public class LambdaExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        
        // Without Lambda
        for (String name : names) {
            System.out.println(name);
        }
        
        // With Lambda
        names.forEach(name -> System.out.println(name));
    }
}
```

**Explanation:** The `forEach` method accepts a lambda expression that acts as a short, inline function to print each name.

---

## **2. Functional Interfaces**
A functional interface is an interface with a single abstract method (SAM). Java 8 introduced the `@FunctionalInterface` annotation to ensure an interface has only one abstract method.

### **Example:**
```java
@FunctionalInterface
interface Greeting {
    void sayHello(String name);
}

public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        Greeting greeting = name -> System.out.println("Hello, " + name);
        greeting.sayHello("Alice");
    }
}
```

**Explanation:** Here, the `Greeting` interface is functional, and its single abstract method `sayHello` is implemented using a lambda expression.

---

## **3. Stream API**
The Stream API provides a way to process collections and sequences of data declaratively. Streams support operations like filtering, mapping, and reducing.

### **Example:**
```java
import java.util.Arrays;
import java.util.List;

public class StreamExample {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);

        // Filtering even numbers and printing
        numbers.stream()
               .filter(n -> n % 2 == 0)
               .forEach(System.out::println);

        // Mapping numbers to their squares and printing
        numbers.stream()
               .map(n -> n * n)
               .forEach(System.out::println);
    }
}
```

**Explanation:** The `filter` method selects even numbers, and `map` transforms numbers into their squares.

---

## **4. Default and Static Methods in Interfaces**
Interfaces can now have methods with default and static implementations.

### **Example:**
```java
interface Vehicle {
    default void start() {
        System.out.println("Starting vehicle...");
    }

    static void service() {
        System.out.println("Servicing vehicle...");
    }
}

public class DefaultMethodExample implements Vehicle {
    public static void main(String[] args) {
        Vehicle car = new DefaultMethodExample();
        car.start(); // Default method
        Vehicle.service(); // Static method
    }
}
```

**Explanation:** Default methods allow backward compatibility, while static methods in interfaces provide utility implementations.

---

## **5. Method References**
Method references allow you to refer to methods directly using `::`. They simplify lambda expressions when a method already exists.

### **Example:**
```java
import java.util.Arrays;
import java.util.List;

public class MethodReferenceExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        // Using method reference
        names.forEach(System.out::println);
    }
}
```

**Explanation:** The `System.out::println` reference replaces a lambda expression like `name -> System.out.println(name)`.

---

## **6. Optional Class**
The `Optional` class is a container object used to handle null values gracefully and avoid `NullPointerException`.

### **Example:**
```java
import java.util.Optional;

public class OptionalExample {
    public static void main(String[] args) {
        Optional<String> optional = Optional.ofNullable("Hello");
        
        // Check if value is present
        if (optional.isPresent()) {
            System.out.println(optional.get());
        }

        // Using orElse
        String value = optional.orElse("Default Value");
        System.out.println(value);

        // Using ifPresent
        optional.ifPresent(System.out::println);
    }
}
```

**Explanation:** The `Optional` class encapsulates the possible absence of a value, offering methods like `orElse` and `ifPresent` for safe handling.

---

## **7. Date and Time API**
Java 8 introduced a new `java.time` package for better handling of dates and times.

### **Example:**
```java
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;

public class DateTimeExample {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        LocalDateTime dateTime = LocalDateTime.now();

        System.out.println("Current Date: " + date);
        System.out.println("Current Time: " + time);
        System.out.println("Current DateTime: " + dateTime);
    }
}
```

**Explanation:** The `java.time` package includes classes like `LocalDate`, `LocalTime`, and `LocalDateTime` for cleaner date and time manipulation.

---

## **8. Nashorn JavaScript Engine**
Java 8 introduced the Nashorn JavaScript engine to execute JavaScript code in Java applications.

### **Example:**
```java
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class NashornExample {
    public static void main(String[] args) throws Exception {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        engine.eval("print('Hello from JavaScript');");
    }
}
```

**Explanation:** The `ScriptEngine` interface allows execution of JavaScript code.

---

## **9. Collectors in Stream API**
Collectors are used for collecting and processing data from streams.

### **Example:**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorsExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        // Joining names into a single string
        String result = names.stream()
                             .collect(Collectors.joining(", "));
        System.out.println(result);

        // Collecting into a list
        List<String> filtered = names.stream()
                                     .filter(name -> name.startsWith("A"))
                                     .collect(Collectors.toList());
        System.out.println(filtered);
    }
}
```

**Explanation:** `Collectors` offer methods like `toList`, `joining`, and `groupingBy` for transforming stream data into collections or strings.

---

## **10. CompletableFuture**
The `CompletableFuture` class simplifies asynchronous programming and handling of future tasks.

### **Example:**
```java
import java.util.concurrent.CompletableFuture;

public class CompletableFutureExample {
    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> "Hello")
                         .thenApply(message -> message + " World")
                         .thenAccept(System.out::println);
    }
}
```

**Explanation:** `CompletableFuture` allows chaining of asynchronous tasks using methods like `thenApply` and `thenAccept`.

---

These features collectively make Java 8 a powerful and modern programming platform. Do you need help with any specific feature or a deeper dive into any concept?

# What is Lambda Expressions?
### **Lambda Expressions in Java**

Lambda expressions were introduced in **Java 8** and are one of the most powerful and notable features of the release. They enable functional programming by allowing you to define **anonymous functions** in a concise and readable way. Lambda expressions are primarily used to implement functional interfaces.

---

### **Definition:**
A **lambda expression** is a short block of code that takes parameters and returns a value. They provide a clear and concise way to represent **one method interface** using an expression.

---

### **Syntax of Lambda Expression:**

```
(parameters) -> expression
or
(parameters) -> { statements; }
```

1. **Parameters**: Represents the input parameters for the function.
2. **Arrow Operator (`->`)**: Separates parameters and body of the lambda expression.
3. **Body**: Contains the implementation logic.

---

### **Key Points:**
- Lambda expressions are used to replace **anonymous classes** for implementing functional interfaces.
- They reduce boilerplate code and improve readability.
- They can be used in APIs like **Stream API** and **Collections Framework**.

---

### **Functional Interface:**
A **functional interface** is an interface that contains only one abstract method. Lambda expressions work only with functional interfaces. Java provides the `@FunctionalInterface` annotation to ensure an interface is functional.

**Example of a Functional Interface:**
```java
@FunctionalInterface
interface Calculator {
    int add(int a, int b);
}
```

---

### **Examples of Lambda Expressions**

#### **1. Basic Example**
```java
@FunctionalInterface
interface Greeting {
    void sayHello(String name);
}

public class LambdaExample {
    public static void main(String[] args) {
        // Using a lambda expression
        Greeting greeting = (name) -> System.out.println("Hello, " + name);
        greeting.sayHello("Alice");
    }
}
```

**Explanation:**
- `Greeting` is a functional interface with one method, `sayHello(String name)`.
- The lambda expression `(name) -> System.out.println("Hello, " + name)` provides the implementation.

---

#### **2. Lambda Expression with Multiple Parameters**
```java
@FunctionalInterface
interface Calculator {
    int add(int a, int b);
}

public class CalculatorExample {
    public static void main(String[] args) {
        // Using lambda expression to implement add method
        Calculator calculator = (a, b) -> a + b;
        System.out.println("Sum: " + calculator.add(10, 20));
    }
}
```

**Explanation:**
- `(a, b) -> a + b` defines the logic for adding two numbers.
- The lambda expression replaces the need for a separate implementation of the `Calculator` interface.

---

#### **3. Lambda Expression in Collections**
Lambda expressions can simplify operations like sorting and iterating through collections.

##### **Sorting a List with Lambda**
```java
import java.util.Arrays;
import java.util.List;

public class ListSortExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David");

        // Sorting using Lambda Expression
        names.sort((a, b) -> a.compareTo(b));
        names.forEach(name -> System.out.println(name));
    }
}
```

**Explanation:**
- `names.sort((a, b) -> a.compareTo(b))` sorts the list in ascending order.
- `names.forEach(name -> System.out.println(name))` iterates and prints each name.

---

#### **4. Lambda Expression in Stream API**
```java
import java.util.Arrays;
import java.util.List;

public class StreamExample {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        // Using Lambda to filter and print even numbers
        numbers.stream()
               .filter(n -> n % 2 == 0)
               .forEach(n -> System.out.println(n));
    }
}
```

**Explanation:**
- `filter(n -> n % 2 == 0)` filters even numbers.
- `forEach(n -> System.out.println(n))` prints each filtered number.

---

#### **5. Lambda Expression with Threads**
```java
public class ThreadExample {
    public static void main(String[] args) {
        // Without Lambda
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread without Lambda");
            }
        }).start();

        // With Lambda
        new Thread(() -> System.out.println("Thread with Lambda")).start();
    }
}
```

**Explanation:**
- The lambda expression `() -> System.out.println("Thread with Lambda")` replaces the verbose `Runnable` implementation.

---

### **Advantages of Lambda Expressions**
1. **Conciseness**: Reduces boilerplate code.
2. **Readability**: Improves readability by providing clear and concise syntax.
3. **Functional Programming**: Enables functional-style programming in Java.
4. **Improved APIs**: Works seamlessly with new APIs like Stream API and Collection enhancements.

---

### **How Lambda Expressions Work Internally:**

Lambda expressions are converted into **synthetic methods** in bytecode, and the JVM handles them using **invokedynamic instructions**. The **functional interface** acts as the target type for the lambda expression.

---

### **Limitations of Lambda Expressions:**
1. **One Abstract Method Only**: They can only be used with functional interfaces.
2. **Code Debugging**: Debugging lambda expressions can sometimes be challenging due to their compact nature.
3. **Overhead**: May increase overhead for developers unfamiliar with functional programming.

---

### **Real-World Usage of Lambda Expressions:**

#### **1. Event Handling in GUI:**
```java
import javax.swing.JButton;
import javax.swing.JFrame;

public class LambdaInGUI {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Lambda Example");
        JButton button = new JButton("Click Me");

        // Event Handling with Lambda
        button.addActionListener(e -> System.out.println("Button Clicked"));

        frame.add(button);
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
```

#### **2. Custom Filtering:**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CustomFilterExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David");

        // Filtering names starting with 'A'
        List<String> filtered = names.stream()
                                     .filter(name -> name.startsWith("A"))
                                     .collect(Collectors.toList());
        System.out.println(filtered);
    }
}
```

---

### **Conclusion:**
Lambda expressions make Java more powerful and expressive by enabling functional programming features. They simplify code by removing the need for boilerplate code like anonymous classes, making applications easier to write and maintain. By combining lambda expressions with functional interfaces, Stream API, and collections, Java developers can write more readable and efficient code.

# What is Functional Interfaces?
### **Functional Interfaces in Java**

A **Functional Interface** in Java is an interface that contains exactly **one abstract method**. These interfaces are designed to support **lambda expressions** and **method references**, introduced in Java 8. They enable functional programming in Java by providing a type to represent functions.

---

### **Key Characteristics of Functional Interfaces:**

1. **Single Abstract Method (SAM)**: A functional interface must have exactly one abstract method. This is why they are sometimes referred to as **SAM interfaces**.
2. **Default and Static Methods**: Functional interfaces can also have multiple **default** and **static** methods, but only one abstract method.
3. **`@FunctionalInterface` Annotation**: While optional, this annotation is used to explicitly indicate that the interface is meant to be a functional interface. It also ensures that the interface has only one abstract method, helping to avoid accidental additions of other abstract methods.
4. **Compatibility**: Functional interfaces can be implemented using **lambda expressions**, **method references**, or **anonymous classes**.

---

### **Syntax of Functional Interface**

```java
@FunctionalInterface
interface InterfaceName {
    abstractMethod(); // Only one abstract method
}
```

---

### **Built-in Functional Interfaces in Java 8**
Java 8 provides a set of commonly used functional interfaces in the `java.util.function` package:

1. **Predicate**: Represents a condition or filter (boolean-valued function).
2. **Function**: Represents a function that takes one argument and produces a result.
3. **Supplier**: Represents a supplier of results (no input, just output).
4. **Consumer**: Represents an operation that accepts a single input and performs some action (no return value).
5. **BiFunction**: Represents a function that takes two arguments and produces a result.

---

### **Examples of Functional Interfaces**

#### **1. Custom Functional Interface**

##### **Example:**
```java
@FunctionalInterface
interface Greeting {
    void sayHello(String name);
}

public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        // Using Lambda Expression
        Greeting greeting = name -> System.out.println("Hello, " + name);
        greeting.sayHello("Alice");

        // Using Anonymous Class
        Greeting anonymousGreeting = new Greeting() {
            @Override
            public void sayHello(String name) {
                System.out.println("Hi, " + name);
            }
        };
        anonymousGreeting.sayHello("Bob");
    }
}
```

**Explanation:**
- The `Greeting` interface has one abstract method, making it a functional interface.
- It is implemented using both a **lambda expression** and an **anonymous class**.

---

#### **2. Using `Predicate`**

##### **Example:**
```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        // Predicate to check if a string starts with "A"
        Predicate<String> startsWithA = name -> name.startsWith("A");

        // Filtering and printing names
        names.stream()
             .filter(startsWithA)
             .forEach(System.out::println);
    }
}
```

**Explanation:**
- The `Predicate` functional interface has a single abstract method `test(T t)`, which takes an argument and returns a boolean.
- The lambda expression `name -> name.startsWith("A")` implements this method.

---

#### **3. Using `Function`**

##### **Example:**
```java
import java.util.function.Function;

public class FunctionExample {
    public static void main(String[] args) {
        // Function to calculate the square of a number
        Function<Integer, Integer> square = n -> n * n;

        System.out.println("Square of 4: " + square.apply(4));
        System.out.println("Square of 7: " + square.apply(7));
    }
}
```

**Explanation:**
- The `Function` interface has a method `apply(T t)`, which accepts one input and returns a result.
- The lambda expression `n -> n * n` defines the logic to calculate the square of a number.

---

#### **4. Using `Consumer`**

##### **Example:**
```java
import java.util.function.Consumer;

public class ConsumerExample {
    public static void main(String[] args) {
        // Consumer to print a string in uppercase
        Consumer<String> printUpperCase = str -> System.out.println(str.toUpperCase());

        printUpperCase.accept("hello");
        printUpperCase.accept("world");
    }
}
```

**Explanation:**
- The `Consumer` interface has a method `accept(T t)` that performs an action but does not return a result.
- The lambda expression `str -> System.out.println(str.toUpperCase())` implements this action.

---

#### **5. Using `Supplier`**

##### **Example:**
```java
import java.util.function.Supplier;

public class SupplierExample {
    public static void main(String[] args) {
        // Supplier to generate a random number
        Supplier<Double> randomSupplier = () -> Math.random();

        System.out.println("Random Number: " + randomSupplier.get());
        System.out.println("Random Number: " + randomSupplier.get());
    }
}
```

**Explanation:**
- The `Supplier` interface has a method `get()` that does not accept any arguments but returns a result.
- The lambda expression `() -> Math.random()` provides the implementation to generate random numbers.

---

#### **6. Using `BiFunction`**

##### **Example:**
```java
import java.util.function.BiFunction;

public class BiFunctionExample {
    public static void main(String[] args) {
        // BiFunction to concatenate two strings
        BiFunction<String, String, String> concatenate = (a, b) -> a + b;

        System.out.println(concatenate.apply("Hello, ", "World!"));
    }
}
```

**Explanation:**
- The `BiFunction` interface has a method `apply(T t, U u)` that accepts two arguments and produces a result.
- The lambda expression `(a, b) -> a + b` concatenates two strings.

---

### **Creating a Custom Functional Interface**

##### **Example:**
```java
@FunctionalInterface
interface MathOperation {
    int operate(int a, int b);
}

public class CustomFunctionalInterface {
    public static void main(String[] args) {
        // Implementing the interface using lambda
        MathOperation addition = (a, b) -> a + b;
        MathOperation multiplication = (a, b) -> a * b;

        System.out.println("Addition: " + addition.operate(5, 3));
        System.out.println("Multiplication: " + multiplication.operate(5, 3));
    }
}
```

**Explanation:**
- The `MathOperation` interface has a single abstract method `operate`.
- Different implementations (addition, multiplication) are provided using lambda expressions.

---

### **Why Use Functional Interfaces?**
1. **Enable Lambda Expressions**: Functional interfaces act as a target type for lambda expressions and method references.
2. **Clean Code**: Reduces boilerplate code by avoiding verbose anonymous class implementations.
3. **Built-in Utilities**: Provides built-in functional interfaces like `Predicate`, `Function`, `Consumer`, etc., to handle common functional patterns.
4. **Improved APIs**: Makes APIs like **Streams** and **Collections** more expressive and declarative.

---

### **Conclusion**
Functional interfaces are a core concept in Java 8's functional programming paradigm. They provide a framework for using lambda expressions and method references effectively. By leveraging built-in functional interfaces and creating custom ones, developers can write concise, readable, and powerful code. If you need help implementing functional interfaces in any specific scenario, feel free to ask!

# What are Pre-defined Functional Interfaces in Java?
Java 8 introduced **predefined functional interfaces** in the `java.util.function` package to support **lambda expressions** and **method references**. These interfaces simplify the implementation of common operations, like filtering, mapping, or consuming data.

Below is a detailed explanation of the most commonly used **predefined functional interfaces** with examples and use cases.

---

### **Key Characteristics of Predefined Functional Interfaces**
1. **Single Abstract Method (SAM):** Each predefined functional interface contains exactly one abstract method.
2. **Default and Static Methods:** These interfaces can also have default or static methods to provide additional functionality.
3. **Common Use Cases:** They are primarily used in functional programming constructs like the **Stream API**, **Collection Framework**, and **optional processing**.

---

### **Classification of Predefined Functional Interfaces**

| Functional Interface | Abstract Method | Purpose |
|-----------------------|-----------------|---------|
| **Predicate**         | `boolean test(T t)` | Represents a condition that returns a boolean value. |
| **Function**          | `R apply(T t)` | Takes one argument and produces a result. |
| **Consumer**          | `void accept(T t)` | Performs an operation on the given input without returning a result. |
| **Supplier**          | `T get()` | Supplies a result without taking any input. |
| **UnaryOperator**     | `T apply(T t)` | A specialization of `Function` for the same input and output type. |
| **BinaryOperator**    | `T apply(T t1, T t2)` | A specialization of `BiFunction` for the same input and output type. |
| **BiPredicate**       | `boolean test(T t, U u)` | Represents a condition with two inputs. |
| **BiFunction**        | `R apply(T t, U u)` | Takes two arguments and produces a result. |
| **BiConsumer**        | `void accept(T t, U u)` | Performs an operation on two inputs without returning a result. |

---

### **1. Predicate**
A `Predicate` represents a condition (boolean-valued function) used for filtering or testing logic.

#### **Abstract Method:**
```java
boolean test(T t);
```

#### **Example:**
```java
import java.util.function.Predicate;

public class PredicateExample {
    public static void main(String[] args) {
        Predicate<Integer> isEven = number -> number % 2 == 0;

        System.out.println("Is 4 even? " + isEven.test(4));
        System.out.println("Is 5 even? " + isEven.test(5));
    }
}
```

**Explanation:**
- The `test` method checks whether a number is even.
- The lambda expression `number -> number % 2 == 0` defines the logic.

---

### **2. Function**
A `Function` represents a function that takes one input and produces a result.

#### **Abstract Method:**
```java
R apply(T t);
```

#### **Example:**
```java
import java.util.function.Function;

public class FunctionExample {
    public static void main(String[] args) {
        Function<String, Integer> stringLength = str -> str.length();

        System.out.println("Length of 'Hello': " + stringLength.apply("Hello"));
        System.out.println("Length of 'Java 8': " + stringLength.apply("Java 8"));
    }
}
```

**Explanation:**
- The `apply` method takes a string and returns its length.
- The lambda expression `str -> str.length()` provides the implementation.

---

### **3. Consumer**
A `Consumer` performs an operation on a single input and does not return any result.

#### **Abstract Method:**
```java
void accept(T t);
```

#### **Example:**
```java
import java.util.function.Consumer;

public class ConsumerExample {
    public static void main(String[] args) {
        Consumer<String> printMessage = message -> System.out.println("Message: " + message);

        printMessage.accept("Hello, World!");
        printMessage.accept("Java Functional Interfaces");
    }
}
```

**Explanation:**
- The `accept` method prints a message to the console.
- The lambda expression `message -> System.out.println("Message: " + message)` provides the implementation.

---

### **4. Supplier**
A `Supplier` provides a result without taking any input.

#### **Abstract Method:**
```java
T get();
```

#### **Example:**
```java
import java.util.function.Supplier;

public class SupplierExample {
    public static void main(String[] args) {
        Supplier<Double> randomSupplier = () -> Math.random();

        System.out.println("Random Number: " + randomSupplier.get());
        System.out.println("Random Number: " + randomSupplier.get());
    }
}
```

**Explanation:**
- The `get` method generates a random number.
- The lambda expression `() -> Math.random()` provides the implementation.

---

### **5. UnaryOperator**
A `UnaryOperator` is a specialized version of the `Function` interface where the input and output are of the same type.

#### **Abstract Method:**
```java
T apply(T t);
```

#### **Example:**
```java
import java.util.function.UnaryOperator;

public class UnaryOperatorExample {
    public static void main(String[] args) {
        UnaryOperator<Integer> square = n -> n * n;

        System.out.println("Square of 5: " + square.apply(5));
        System.out.println("Square of 7: " + square.apply(7));
    }
}
```

**Explanation:**
- The `apply` method calculates the square of a number.

---

### **6. BinaryOperator**
A `BinaryOperator` is a specialized version of `BiFunction` where the input and output types are the same.

#### **Abstract Method:**
```java
T apply(T t1, T t2);
```

#### **Example:**
```java
import java.util.function.BinaryOperator;

public class BinaryOperatorExample {
    public static void main(String[] args) {
        BinaryOperator<Integer> multiply = (a, b) -> a * b;

        System.out.println("Product of 3 and 5: " + multiply.apply(3, 5));
        System.out.println("Product of 7 and 8: " + multiply.apply(7, 8));
    }
}
```

**Explanation:**
- The `apply` method multiplies two numbers.

---

### **7. BiPredicate**
A `BiPredicate` represents a condition (boolean-valued function) with two inputs.

#### **Abstract Method:**
```java
boolean test(T t, U u);
```

#### **Example:**
```java
import java.util.function.BiPredicate;

public class BiPredicateExample {
    public static void main(String[] args) {
        BiPredicate<Integer, Integer> isGreater = (a, b) -> a > b;

        System.out.println("Is 5 greater than 3? " + isGreater.test(5, 3));
        System.out.println("Is 2 greater than 4? " + isGreater.test(2, 4));
    }
}
```

**Explanation:**
- The `test` method checks whether one number is greater than the other.

---

### **8. BiFunction**
A `BiFunction` takes two inputs and produces a result.

#### **Abstract Method:**
```java
R apply(T t, U u);
```

#### **Example:**
```java
import java.util.function.BiFunction;

public class BiFunctionExample {
    public static void main(String[] args) {
        BiFunction<String, String, String> concatenate = (a, b) -> a + b;

        System.out.println("Concatenation: " + concatenate.apply("Hello, ", "World!"));
    }
}
```

**Explanation:**
- The `apply` method concatenates two strings.

---

### **9. BiConsumer**
A `BiConsumer` performs an operation on two inputs without returning a result.

#### **Abstract Method:**
```java
void accept(T t, U u);
```

#### **Example:**
```java
import java.util.function.BiConsumer;

public class BiConsumerExample {
    public static void main(String[] args) {
        BiConsumer<String, Integer> printNameAndAge = (name, age) -> 
            System.out.println("Name: " + name + ", Age: " + age);

        printNameAndAge.accept("Alice", 25);
        printNameAndAge.accept("Bob", 30);
    }
}
```

**Explanation:**
- The `accept` method prints a name and age.

---

### **Advantages of Predefined Functional Interfaces**

1. **Code Reusability**: Eliminates the need to create custom interfaces for common operations.
2. **Readability**: Provides a clean and concise way to write code.
3. **Stream API Integration**: Simplifies working with streams and collections.
4. **Standardization**: Encourages consistent coding practices across Java projects.

---

### **Conclusion**
Predefined functional interfaces in Java 8 simplify the implementation of functional programming concepts. They allow developers to write cleaner, more expressive, and more efficient code. By using these interfaces with lambda expressions, method references, and the Stream API, developers can solve complex problems in a more declarative style. Let me know if you'd like to explore a specific use case or need further clarification!

# What is the difference between Consumer vs BiConsumer?
### **Difference Between Consumer and BiConsumer in Java**

Both `Consumer` and `BiConsumer` are predefined functional interfaces in Java 8, part of the `java.util.function` package. They are used to perform operations on input data without returning a result. However, the key difference lies in the number of arguments they take.

---

### **Key Differences**

| Feature             | `Consumer`                  | `BiConsumer`                 |
|---------------------|-----------------------------|------------------------------|
| **Definition**       | Represents an operation that accepts **one input argument** and produces no result. | Represents an operation that accepts **two input arguments** and produces no result. |
| **Abstract Method**  | `void accept(T t)`          | `void accept(T t, U u)`       |
| **Number of Inputs** | Single input                | Two inputs                   |
| **Return Type**      | `void`                      | `void`                       |
| **Use Cases**        | Performing operations on a single object, e.g., printing, modifying. | Performing operations on two related objects, e.g., combining, comparing, or mapping two values. |

---

### **Consumer Functional Interface**

#### **Definition:**
`Consumer` represents an operation that accepts a **single input** and performs an action on it but does not return any result.

#### **Abstract Method:**
```java
void accept(T t);
```

#### **Example 1: Basic Consumer**
```java
import java.util.function.Consumer;

public class ConsumerExample {
    public static void main(String[] args) {
        Consumer<String> printMessage = message -> System.out.println("Message: " + message);

        printMessage.accept("Hello, World!");
        printMessage.accept("Java Functional Interfaces");
    }
}
```

**Explanation:**
- The lambda expression `message -> System.out.println("Message: " + message)` implements the `accept` method.
- It takes a single input (a message) and performs the action of printing it.

#### **Example 2: Consumer with a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerListExample {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        // Using Consumer to print each name
        Consumer<String> printName = name -> System.out.println("Name: " + name);
        names.forEach(printName);
    }
}
```

**Explanation:**
- The `forEach` method accepts a `Consumer` to perform an action on each element of the list.
- The lambda `name -> System.out.println("Name: " + name)` prints each name.

---

### **BiConsumer Functional Interface**

#### **Definition:**
`BiConsumer` represents an operation that accepts **two inputs** and performs an action on them but does not return any result.

#### **Abstract Method:**
```java
void accept(T t, U u);
```

#### **Example 1: Basic BiConsumer**
```java
import java.util.function.BiConsumer;

public class BiConsumerExample {
    public static void main(String[] args) {
        BiConsumer<String, Integer> printNameAndAge = (name, age) -> 
            System.out.println("Name: " + name + ", Age: " + age);

        printNameAndAge.accept("Alice", 25);
        printNameAndAge.accept("Bob", 30);
    }
}
```

**Explanation:**
- The lambda expression `(name, age) -> System.out.println("Name: " + name + ", Age: " + age)` implements the `accept` method.
- It takes two inputs (name and age) and performs the action of printing them together.

#### **Example 2: BiConsumer with a Map**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class BiConsumerMapExample {
    public static void main(String[] args) {
        Map<String, Integer> personAgeMap = new HashMap<>();
        personAgeMap.put("Alice", 25);
        personAgeMap.put("Bob", 30);
        personAgeMap.put("Charlie", 35);

        // Using BiConsumer to print each key-value pair
        BiConsumer<String, Integer> printEntry = (name, age) -> 
            System.out.println("Name: " + name + ", Age: " + age);
        personAgeMap.forEach(printEntry);
    }
}
```

**Explanation:**
- The `forEach` method of the `Map` accepts a `BiConsumer` to perform an action on each key-value pair.
- The lambda `(name, age) -> System.out.println("Name: " + name + ", Age: " + age)` prints each entry.

---

### **Comparison Through Examples**

#### **Single Input with Consumer**
```java
import java.util.function.Consumer;

public class ConsumerVsBiConsumer {
    public static void main(String[] args) {
        // Single input using Consumer
        Consumer<String> greet = name -> System.out.println("Hello, " + name);
        greet.accept("Alice");
    }
}
```

#### **Two Inputs with BiConsumer**
```java
import java.util.function.BiConsumer;

public class ConsumerVsBiConsumer {
    public static void main(String[] args) {
        // Two inputs using BiConsumer
        BiConsumer<String, Integer> greetWithAge = (name, age) -> 
            System.out.println("Hello, " + name + ". You are " + age + " years old.");
        greetWithAge.accept("Alice", 25);
    }
}
```

**Key Difference:**
- `Consumer` works with **one input** (e.g., a single name).
- `BiConsumer` works with **two inputs** (e.g., a name and an age).

---

### **When to Use Consumer vs BiConsumer**

1. **Use `Consumer`:**
   - When you need to perform an operation on a **single object**.
   - Examples: Logging messages, modifying a list, or printing items.

2. **Use `BiConsumer`:**
   - When you need to perform an operation on **two related objects**.
   - Examples: Processing key-value pairs in a `Map`, combining two objects, or comparing two items.

---

### **Conclusion**
The main difference between `Consumer` and `BiConsumer` lies in the number of inputs they accept:
- `Consumer` operates on **one input**.
- `BiConsumer` operates on **two inputs**.

Both are powerful tools for functional programming in Java and are commonly used in data processing tasks, especially with collections and the Stream API. Let me know if you'd like further examples or more in-depth explanations!

# What is Supplier?
### **Supplier in Java**

`Supplier` is a **functional interface** introduced in Java 8 under the `java.util.function` package. It represents a supplier of results, meaning it **does not take any input but returns a result** when its method is invoked.

---

### **Key Characteristics of Supplier**

1. **No Input Parameters:** The `Supplier` interface does not take any arguments.
2. **Returns a Value:** It provides a value when its method is called.
3. **Abstract Method:** It has a single abstract method, `T get()`, where `T` is the type of the result.
4. **Common Use Cases:** Suppliers are often used for:
   - Lazy generation of values.
   - Fetching or generating data dynamically.
   - Creating objects, especially for factories or caching.

---

### **Functional Method of Supplier**

#### **Abstract Method:**
```java
T get();
```

- **`get`**: This method generates or retrieves a result of type `T`.

---

### **Default and Static Methods**

The `Supplier` interface has no default or static methods. It relies solely on the `get` method to produce results.

---

### **Basic Syntax**
```java
Supplier<T> supplier = () -> { return value; };
```

- **`T`**: The type of the result.
- **Lambda Expression**: Used to define the logic for generating the result.

---

### **Examples of Supplier**

#### **1. Simple Supplier Example**
```java
import java.util.function.Supplier;

public class SupplierExample {
    public static void main(String[] args) {
        // Supplier to generate a string
        Supplier<String> stringSupplier = () -> "Hello, Supplier!";
        
        System.out.println(stringSupplier.get()); // Output: Hello, Supplier!
    }
}
```

**Explanation:**
- `Supplier<String>` supplies a `String` when `get()` is called.
- The lambda expression `() -> "Hello, Supplier!"` defines the result.

---

#### **2. Supplier for Generating Random Numbers**
```java
import java.util.function.Supplier;
import java.util.Random;

public class RandomNumberSupplier {
    public static void main(String[] args) {
        Supplier<Integer> randomSupplier = () -> new Random().nextInt(100);

        System.out.println("Random Number 1: " + randomSupplier.get());
        System.out.println("Random Number 2: " + randomSupplier.get());
    }
}
```

**Explanation:**
- The `Supplier<Integer>` generates a random number between 0 and 99 using `Random.nextInt()`.
- Every call to `get()` produces a new random number.

---

#### **3. Supplier for Lazy Evaluation**
Lazy evaluation means that a value is generated only when needed.

##### **Example:**
```java
import java.util.function.Supplier;

public class LazyEvaluationExample {
    public static void main(String[] args) {
        Supplier<Double> lazyValue = () -> {
            System.out.println("Computing the value...");
            return Math.random();
        };

        System.out.println("Calling get() for the first time:");
        System.out.println("Value: " + lazyValue.get());

        System.out.println("Calling get() for the second time:");
        System.out.println("Value: " + lazyValue.get());
    }
}
```

**Explanation:**
- The value is computed only when `get()` is called, enabling on-demand (lazy) evaluation.
- Each call to `get()` recomputes and supplies a new value.

---

#### **4. Supplier as a Factory**
Suppliers can be used as factories to create new objects.

##### **Example:**
```java
import java.util.function.Supplier;

class Car {
    private String name;

    public Car(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Car: " + name;
    }
}

public class SupplierFactoryExample {
    public static void main(String[] args) {
        Supplier<Car> carSupplier = () -> new Car("Tesla Model S");

        Car car = carSupplier.get();
        System.out.println(car);
    }
}
```

**Explanation:**
- The `Supplier<Car>` acts as a factory for creating `Car` objects.
- The lambda expression `() -> new Car("Tesla Model S")` creates a new `Car` object when `get()` is called.

---

#### **5. Supplier with Streams**
Suppliers are useful for generating data in streams.

##### **Example:**
```java
import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamSupplierExample {
    public static void main(String[] args) {
        Supplier<Double> randomSupplier = () -> Math.random();

        // Generating a stream of random numbers
        Stream.generate(randomSupplier)
              .limit(5)
              .forEach(System.out::println);
    }
}
```

**Explanation:**
- The `Stream.generate` method accepts a `Supplier` to produce elements for the stream.
- The lambda `() -> Math.random()` supplies random numbers.

---

#### **6. Supplier with Caching**
Suppliers can cache results to ensure a value is computed only once and reused.

##### **Example:**
```java
import java.util.function.Supplier;

public class CachedSupplierExample {
    public static void main(String[] args) {
        Supplier<Double> cachedSupplier = createCachedSupplier(() -> Math.random());

        System.out.println("First Call: " + cachedSupplier.get());
        System.out.println("Second Call: " + cachedSupplier.get());
    }

    public static <T> Supplier<T> createCachedSupplier(Supplier<T> original) {
        final Object[] cache = new Object[1];
        return () -> {
            if (cache[0] == null) {
                cache[0] = original.get();
            }
            return (T) cache[0];
        };
    }
}
```

**Explanation:**
- The `createCachedSupplier` method wraps a `Supplier` to compute and cache the result on the first call.
- Subsequent calls return the cached value.

---

### **Advantages of Supplier**

1. **Simplifies Code**: Removes the need for verbose logic to generate results.
2. **Lazy Evaluation**: Results are generated only when needed, improving performance in some scenarios.
3. **Versatility**: Can be used for factories, caching, or dynamically generating data.
4. **Integration with APIs**: Works seamlessly with the Stream API and other functional programming constructs.

---

### **Use Cases of Supplier**

1. **Random Number Generation:** Dynamic generation of random values.
2. **Lazy Initialization:** Delaying computation until the result is actually required.
3. **Object Factories:** Creating objects dynamically without exposing the creation logic.
4. **Stream Data Sources:** Supplying elements for infinite streams.
5. **Caching Results:** Computing a value once and reusing it.

---

### **Key Points to Remember**

1. `Supplier` is a functional interface with a single method, `get()`.
2. It does not accept any input but produces a result.
3. Often used for lazy evaluation, factories, and infinite data generation.
4. Works well with functional programming constructs like Streams and Lambdas.

---

### **Conclusion**

The `Supplier` interface is a powerful functional interface for scenarios where values need to be generated or retrieved without input parameters. Its simplicity and versatility make it an essential tool in Java 8's functional programming paradigm. Whether it's for lazy evaluation, random number generation, or factory methods, `Supplier` provides a clean and efficient way to handle such operations. Let me know if you'd like to dive deeper into any specific use case or implementation!

# What is the difference between Function vs BiFunction?
### **Difference Between `Function` and `BiFunction` in Java**

Both `Function` and `BiFunction` are predefined functional interfaces in Java 8, part of the `java.util.function` package. They are used to perform transformations or computations. However, the key difference lies in the **number of arguments** they accept.

---

### **Key Differences**

| Feature              | `Function`                            | `BiFunction`                              |
|----------------------|----------------------------------------|-------------------------------------------|
| **Definition**        | Represents a function that takes **one argument** and produces a result. | Represents a function that takes **two arguments** and produces a result. |
| **Abstract Method**   | `R apply(T t)`                       | `R apply(T t, U u)`                       |
| **Number of Inputs**  | Takes **one input**.                 | Takes **two inputs**.                     |
| **Return Type**       | Returns a result (`R`).              | Returns a result (`R`).                   |
| **Use Cases**         | Used for mapping, transforming, or converting a single input. | Used for computations or transformations involving two inputs. |

---

### **Function Functional Interface**

#### **Definition:**
The `Function` interface represents a function that accepts a single argument and produces a result.

#### **Abstract Method:**
```java
R apply(T t);
```
- **`T`**: Type of the input argument.
- **`R`**: Type of the result.

#### **Common Use Cases:**
- Mapping objects to their properties (e.g., extracting a field).
- Transforming a value into another type (e.g., converting `String` to `Integer`).

---

#### **Example: Function**

##### **1. Basic Example**
```java
import java.util.function.Function;

public class FunctionExample {
    public static void main(String[] args) {
        // Function to calculate the square of a number
        Function<Integer, Integer> square = n -> n * n;

        System.out.println("Square of 4: " + square.apply(4));  // Output: 16
        System.out.println("Square of 7: " + square.apply(7));  // Output: 49
    }
}
```

##### **Explanation:**
- `Function<Integer, Integer>` takes an `Integer` as input and returns its square as output.
- The lambda expression `n -> n * n` implements the transformation logic.

---

##### **2. Function with String Length**
```java
import java.util.function.Function;

public class StringLengthExample {
    public static void main(String[] args) {
        // Function to get the length of a string
        Function<String, Integer> stringLength = str -> str.length();

        System.out.println("Length of 'Hello': " + stringLength.apply("Hello"));  // Output: 5
        System.out.println("Length of 'Java': " + stringLength.apply("Java"));    // Output: 4
    }
}
```

##### **Explanation:**
- The `Function<String, Integer>` takes a `String` as input and returns its length.

---

### **BiFunction Functional Interface**

#### **Definition:**
The `BiFunction` interface represents a function that accepts **two arguments** and produces a result.

#### **Abstract Method:**
```java
R apply(T t, U u);
```
- **`T`**: Type of the first input argument.
- **`U`**: Type of the second input argument.
- **`R`**: Type of the result.

#### **Common Use Cases:**
- Combining two values to produce a result.
- Performing arithmetic or other operations involving two inputs.
- Comparing or merging two objects.

---

#### **Example: BiFunction**

##### **1. Basic Example**
```java
import java.util.function.BiFunction;

public class BiFunctionExample {
    public static void main(String[] args) {
        // BiFunction to calculate the product of two numbers
        BiFunction<Integer, Integer, Integer> multiply = (a, b) -> a * b;

        System.out.println("Product of 3 and 5: " + multiply.apply(3, 5));  // Output: 15
        System.out.println("Product of 7 and 8: " + multiply.apply(7, 8));  // Output: 56
    }
}
```

##### **Explanation:**
- `BiFunction<Integer, Integer, Integer>` takes two `Integer` arguments and returns their product.
- The lambda expression `(a, b) -> a * b` defines the computation logic.

---

##### **2. BiFunction for String Concatenation**
```java
import java.util.function.BiFunction;

public class StringConcatenationExample {
    public static void main(String[] args) {
        // BiFunction to concatenate two strings
        BiFunction<String, String, String> concatenate = (a, b) -> a + b;

        System.out.println(concatenate.apply("Hello, ", "World!"));  // Output: Hello, World!
    }
}
```

##### **Explanation:**
- `BiFunction<String, String, String>` takes two `String` inputs and produces a concatenated `String` result.

---

### **Comparison Through Examples**

#### **Single Input with Function**
```java
import java.util.function.Function;

public class FunctionVsBiFunction {
    public static void main(String[] args) {
        // Using Function
        Function<String, String> greet = name -> "Hello, " + name;
        System.out.println(greet.apply("Alice"));  // Output: Hello, Alice
    }
}
```

#### **Two Inputs with BiFunction**
```java
import java.util.function.BiFunction;

public class FunctionVsBiFunction {
    public static void main(String[] args) {
        // Using BiFunction
        BiFunction<String, Integer, String> greetWithAge = (name, age) -> 
            "Hello, " + name + ". You are " + age + " years old.";
        System.out.println(greetWithAge.apply("Alice", 25));  // Output: Hello, Alice. You are 25 years old.
    }
}
```

---

### **When to Use `Function` vs `BiFunction`**

1. **Use `Function`:**
   - When a transformation involves **one input**.
   - Examples:
     - Extracting a property from an object.
     - Converting one data type to another.

2. **Use `BiFunction`:**
   - When a transformation involves **two inputs**.
   - Examples:
     - Combining two values to produce a result.
     - Calculating something based on two inputs (e.g., multiplication, merging strings).

---

### **Advanced Examples**

#### **1. Function for Data Mapping**
```java
import java.util.function.Function;

public class DataMappingExample {
    public static void main(String[] args) {
        Function<Integer, String> numberToString = n -> "Number: " + n;

        System.out.println(numberToString.apply(10));  // Output: Number: 10
    }
}
```

---

#### **2. BiFunction for Object Creation**
```java
import java.util.function.BiFunction;

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + "}";
    }
}

public class BiFunctionObjectCreation {
    public static void main(String[] args) {
        // BiFunction to create a Person object
        BiFunction<String, Integer, Person> personCreator = (name, age) -> new Person(name, age);

        Person person = personCreator.apply("Alice", 30);
        System.out.println(person);  // Output: Person{name='Alice', age=30}
    }
}
```

---

### **Conclusion**

- **`Function`** is used for single-input transformations, while **`BiFunction`** is used for operations that involve two inputs.
- Both interfaces simplify the implementation of functional programming concepts by reducing boilerplate code.
- Use `Function` for simpler operations and `BiFunction` when combining or working with two inputs. 

Let me know if you'd like further examples or deeper explanations!

Here are detailed **e-commerce retail examples** for both `Function` and `BiFunction` in practical scenarios to show how they can be applied to common tasks:

---

### **Using `Function`**

#### **1. Apply Discount to Product Price**
In an e-commerce application, you can use a `Function` to apply a discount to the price of a product.

##### **Example:**
```java
import java.util.function.Function;

public class DiscountExample {
    public static void main(String[] args) {
        // Function to calculate discounted price
        Function<Double, Double> applyDiscount = price -> price * 0.9; // 10% discount

        double originalPrice = 100.0;
        double discountedPrice = applyDiscount.apply(originalPrice);

        System.out.println("Original Price: $" + originalPrice);
        System.out.println("Discounted Price: $" + discountedPrice);
    }
}
```

##### **Explanation:**
- The `Function<Double, Double>` takes the original price as input and returns the discounted price as output.

---

#### **2. Generate a Slug for a Product Name**
Product URLs often need a "slug" for SEO purposes. A `Function` can transform a product name into a URL-friendly format.

##### **Example:**
```java
import java.util.function.Function;

public class SlugExample {
    public static void main(String[] args) {
        // Function to generate a URL slug from a product name
        Function<String, String> generateSlug = name -> name.toLowerCase().replace(" ", "-");

        String productName = "Wireless Bluetooth Speaker";
        String slug = generateSlug.apply(productName);

        System.out.println("Product Name: " + productName);
        System.out.println("Slug: " + slug);
    }
}
```

##### **Explanation:**
- The `Function<String, String>` transforms a product name into a slug by converting it to lowercase and replacing spaces with hyphens.

---

#### **3. Calculate Shipping Fee Based on Distance**
A `Function` can compute the shipping fee based on the distance of delivery.

##### **Example:**
```java
import java.util.function.Function;

public class ShippingFeeExample {
    public static void main(String[] args) {
        // Function to calculate shipping fee based on distance
        Function<Integer, Double> calculateShippingFee = distance -> distance * 1.5;

        int distanceInKm = 20; // Distance in kilometers
        double shippingFee = calculateShippingFee.apply(distanceInKm);

        System.out.println("Distance: " + distanceInKm + " km");
        System.out.println("Shipping Fee: $" + shippingFee);
    }
}
```

##### **Explanation:**
- The `Function<Integer, Double>` calculates the shipping fee by multiplying the distance by a rate.

---

#### **4. Format Product Price for Display**
Format product prices for consistent display on the website.

##### **Example:**
```java
import java.text.DecimalFormat;
import java.util.function.Function;

public class PriceFormatterExample {
    public static void main(String[] args) {
        // Function to format price to 2 decimal places
        Function<Double, String> formatPrice = price -> new DecimalFormat("$#.00").format(price);

        double productPrice = 49.95;
        String formattedPrice = formatPrice.apply(productPrice);

        System.out.println("Formatted Price: " + formattedPrice);
    }
}
```

##### **Explanation:**
- The `Function<Double, String>` formats a price to two decimal places for consistent display.

---

### **Using `BiFunction`**

#### **1. Calculate Total Price for a Product**
You can use a `BiFunction` to calculate the total price of a product by multiplying its price by the quantity purchased.

##### **Example:**
```java
import java.util.function.BiFunction;

public class TotalPriceExample {
    public static void main(String[] args) {
        // BiFunction to calculate total price
        BiFunction<Double, Integer, Double> calculateTotalPrice = (price, quantity) -> price * quantity;

        double unitPrice = 19.99;
        int quantity = 3;
        double totalPrice = calculateTotalPrice.apply(unitPrice, quantity);

        System.out.println("Unit Price: $" + unitPrice);
        System.out.println("Quantity: " + quantity);
        System.out.println("Total Price: $" + totalPrice);
    }
}
```

##### **Explanation:**
- The `BiFunction<Double, Integer, Double>` takes the unit price and quantity as inputs and calculates the total price.

---

#### **2. Combine Product Name and Category**
You can use a `BiFunction` to generate a full display name for a product by combining its name and category.

##### **Example:**
```java
import java.util.function.BiFunction;

public class ProductNameExample {
    public static void main(String[] args) {
        // BiFunction to combine product name and category
        BiFunction<String, String, String> combineNameAndCategory = (name, category) -> name + " (" + category + ")";

        String productName = "Wireless Headphones";
        String category = "Electronics";
        String displayName = combineNameAndCategory.apply(productName, category);

        System.out.println("Display Name: " + displayName);
    }
}
```

##### **Explanation:**
- The `BiFunction<String, String, String>` combines the product name and category to generate a descriptive display name.

---

#### **3. Calculate Discounted Price for Bulk Purchase**
You can use a `BiFunction` to calculate the discounted price based on the unit price and the number of items purchased.

##### **Example:**
```java
import java.util.function.BiFunction;

public class BulkDiscountExample {
    public static void main(String[] args) {
        // BiFunction to calculate bulk discount
        BiFunction<Double, Integer, Double> calculateBulkDiscount = (price, quantity) -> {
            double discount = quantity > 10 ? 0.1 : 0.0; // 10% discount for more than 10 items
            return price * quantity * (1 - discount);
        };

        double unitPrice = 50.0;
        int quantity = 12;
        double discountedPrice = calculateBulkDiscount.apply(unitPrice, quantity);

        System.out.println("Discounted Price: $" + discountedPrice);
    }
}
```

##### **Explanation:**
- The `BiFunction<Double, Integer, Double>` computes the discounted price based on the quantity purchased.

---

#### **4. Generate Invoice Entry**
You can use a `BiFunction` to generate a line item for an invoice that combines product name and total price.

##### **Example:**
```java
import java.util.function.BiFunction;

public class InvoiceExample {
    public static void main(String[] args) {
        // BiFunction to create an invoice entry
        BiFunction<String, Double, String> createInvoiceEntry = (productName, totalPrice) -> 
            "Product: " + productName + ", Total: $" + totalPrice;

        String productName = "Gaming Laptop";
        double totalPrice = 1500.00;
        String invoiceEntry = createInvoiceEntry.apply(productName, totalPrice);

        System.out.println(invoiceEntry);
    }
}
```

##### **Explanation:**
- The `BiFunction<String, Double, String>` generates a string that represents a line item in an invoice.

---

#### **5. Calculate Final Price with Tax**
You can use a `BiFunction` to calculate the final price of a product by adding tax to the base price.

##### **Example:**
```java
import java.util.function.BiFunction;

public class TaxCalculationExample {
    public static void main(String[] args) {
        // BiFunction to calculate final price with tax
        BiFunction<Double, Double, Double> calculateFinalPrice = (price, taxRate) -> price + (price * taxRate);

        double basePrice = 100.0;
        double taxRate = 0.08; // 8% tax
        double finalPrice = calculateFinalPrice.apply(basePrice, taxRate);

        System.out.println("Base Price: $" + basePrice);
        System.out.println("Tax Rate: " + (taxRate * 100) + "%");
        System.out.println("Final Price: $" + finalPrice);
    }
}
```

##### **Explanation:**
- The `BiFunction<Double, Double, Double>` calculates the final price by applying the tax rate to the base price.

---

### **Summary of Use Cases**

| Functional Interface | Use Cases                                                                 |
|-----------------------|--------------------------------------------------------------------------|
| **Function**          | Transforming single inputs, e.g., applying discounts, formatting prices. |
| **BiFunction**        | Combining or transforming two inputs, e.g., bulk discounts, tax calculations. |

Both `Function` and `BiFunction` are versatile and simplify common operations in e-commerce systems by reducing boilerplate code and making the logic reusable. Let me know if you need additional examples or further clarification!

# What is the difference between UnaryOperator vs BinaryOperator?
### **Difference Between `UnaryOperator` and `BinaryOperator` in Java (E-commerce/Retail Examples)**

Both `UnaryOperator` and `BinaryOperator` are **predefined functional interfaces** in Java 8, part of the `java.util.function` package. They are specializations of `Function` and `BiFunction`, respectively.

However, the key difference between them is:

| Feature              | `UnaryOperator`                            | `BinaryOperator`                          |
|----------------------|-------------------------------------------|-------------------------------------------|
| **Definition**        | A function that takes **one input** and returns a result of the same type. | A function that takes **two inputs** and returns a result of the same type. |
| **Abstract Method**   | `T apply(T t);`                          | `T apply(T t, T u);`                      |
| **Number of Inputs**  | Takes **one** input.                     | Takes **two** inputs.                     |
| **Return Type**       | Returns a result of the **same type** as input. | Returns a result of the **same type** as inputs. |
| **Extends**           | `Function<T, T>`                         | `BiFunction<T, T, T>`                      |
| **Use Cases**         | Used for modifying a single value (e.g., applying tax, discounts). | Used for combining two values (e.g., merging prices, calculating total). |

---

## **1. UnaryOperator Functional Interface**

### **Definition:**
A `UnaryOperator<T>` is a specialized version of `Function<T, T>` where the input and output types are the **same**.

### **Abstract Method:**
```java
T apply(T t);
```
- **`T`**: Type of the input and output.

### **Common Use Cases in E-commerce:**
- Applying discounts to product prices.
- Calculating **tax** on a single product price.
- Increasing/decreasing stock quantity by **one factor**.
- Formatting a product name.
- Adding **delivery charges**.

---

### **UnaryOperator Example: Apply Discount on a Product Price**
```java
import java.util.function.UnaryOperator;

public class DiscountExample {
    public static void main(String[] args) {
        // UnaryOperator to apply a 10% discount
        UnaryOperator<Double> applyDiscount = price -> price * 0.9;

        double originalPrice = 100.0;
        double discountedPrice = applyDiscount.apply(originalPrice);

        System.out.println("Original Price: $" + originalPrice);
        System.out.println("Discounted Price: $" + discountedPrice);
    }
}
```

**Explanation:**
- The `UnaryOperator<Double>` applies a **10% discount**.
- It takes **one price** as input and returns the **discounted price**.

---

### **UnaryOperator Example: Add Tax to Product Price**
```java
import java.util.function.UnaryOperator;

public class TaxExample {
    public static void main(String[] args) {
        // UnaryOperator to apply 8% tax
        UnaryOperator<Double> addTax = price -> price * 1.08;

        double basePrice = 50.0;
        double finalPrice = addTax.apply(basePrice);

        System.out.println("Base Price: $" + basePrice);
        System.out.println("Final Price with Tax: $" + finalPrice);
    }
}
```

**Explanation:**
- The `UnaryOperator<Double>` applies **8% tax**.
- It transforms a **single product price**.

---

### **UnaryOperator Example: Format Product Name**
```java
import java.util.function.UnaryOperator;

public class FormatProductName {
    public static void main(String[] args) {
        // UnaryOperator to format product name
        UnaryOperator<String> formatName = name -> name.toUpperCase().replace(" ", "_");

        String productName = "Wireless Earbuds";
        String formattedName = formatName.apply(productName);

        System.out.println("Formatted Product Name: " + formattedName);
    }
}
```

**Explanation:**
- Converts a **product name to uppercase** and replaces spaces with underscores (`_`).

---

### **UnaryOperator Example: Increase Stock by 10%**
```java
import java.util.function.UnaryOperator;

public class StockUpdateExample {
    public static void main(String[] args) {
        // UnaryOperator to increase stock by 10%
        UnaryOperator<Integer> increaseStock = stock -> (int) (stock * 1.10);

        int currentStock = 100;
        int newStock = increaseStock.apply(currentStock);

        System.out.println("Current Stock: " + currentStock);
        System.out.println("Updated Stock: " + newStock);
    }
}
```

**Explanation:**
- Increases stock by **10%**.

---

## **2. BinaryOperator Functional Interface**

### **Definition:**
A `BinaryOperator<T>` is a specialized version of `BiFunction<T, T, T>` where the **two inputs and the result** are of the **same type**.

### **Abstract Method:**
```java
T apply(T t, T u);
```
- **`T`**: Type of both inputs and the output.

### **Common Use Cases in E-commerce:**
- **Merging** two product prices (e.g., combining base price and tax).
- Calculating **total cost** (price × quantity).
- Finding **higher/lower** price (max/min price comparison).
- Combining **inventory quantities**.

---

### **BinaryOperator Example: Calculate Total Price (Price × Quantity)**
```java
import java.util.function.BinaryOperator;

public class TotalPriceExample {
    public static void main(String[] args) {
        // BinaryOperator to calculate total price
        BinaryOperator<Double> calculateTotal = (price, quantity) -> price * quantity;

        double unitPrice = 20.0;
        double totalPrice = calculateTotal.apply(unitPrice, 3.0); // Buying 3 items

        System.out.println("Total Price: $" + totalPrice);
    }
}
```

**Explanation:**
- `BinaryOperator<Double>` calculates **total cost**.

---

### **BinaryOperator Example: Combine Base Price and Tax**
```java
import java.util.function.BinaryOperator;

public class PriceTaxExample {
    public static void main(String[] args) {
        // BinaryOperator to add tax to a price
        BinaryOperator<Double> addTax = (price, tax) -> price + (price * tax);

        double basePrice = 100.0;
        double finalPrice = addTax.apply(basePrice, 0.08); // 8% tax

        System.out.println("Final Price with Tax: $" + finalPrice);
    }
}
```

**Explanation:**
- `BinaryOperator<Double>` adds **tax to the base price**.

---

### **BinaryOperator Example: Find Maximum Price Between Two Products**
```java
import java.util.function.BinaryOperator;

public class MaxPriceExample {
    public static void main(String[] args) {
        // BinaryOperator to find max price
        BinaryOperator<Double> maxPrice = (price1, price2) -> Math.max(price1, price2);

        double priceA = 150.0;
        double priceB = 180.0;
        double higherPrice = maxPrice.apply(priceA, priceB);

        System.out.println("Higher Price: $" + higherPrice);
    }
}
```

**Explanation:**
- Finds **higher price** between two products.

---

### **BinaryOperator Example: Merge Two Warehouses' Stock Quantities**
```java
import java.util.function.BinaryOperator;

public class StockMergeExample {
    public static void main(String[] args) {
        // BinaryOperator to merge stock from two warehouses
        BinaryOperator<Integer> mergeStock = (stock1, stock2) -> stock1 + stock2;

        int warehouse1 = 300;
        int warehouse2 = 500;
        int totalStock = mergeStock.apply(warehouse1, warehouse2);

        System.out.println("Total Stock Available: " + totalStock);
    }
}
```

**Explanation:**
- Merges **stock from two warehouses**.

---

## **Key Takeaways**

| Feature             | `UnaryOperator`                          | `BinaryOperator`                        |
|---------------------|-----------------------------------------|-----------------------------------------|
| **Inputs**          | Takes **one** input.                   | Takes **two** inputs.                   |
| **Returns**         | Returns a result **of the same type**. | Returns a result **of the same type**. |
| **Best Used For**   | Transformations on a single value.     | Combining two values of the same type. |
| **Example Use Cases** | Discount, tax, stock update.          | Total price, merging stock, finding max. |

---

## **Conclusion**
- Use **`UnaryOperator`** when **one** value needs modification (e.g., applying discount).
- Use **`BinaryOperator`** when **two** values need merging (e.g., total price, tax addition).

Let me know if you need further explanations or custom examples! 🚀

# What is Method References?

## **Introduction**
Method references in Java 8 provide a shorthand notation for **lambda expressions** when the lambda body simply calls an existing method. They make the code more readable and reduce verbosity.

### **Key Points about Method References**
1. They **replace lambda expressions** when a method already exists for the operation.
2. They help make the code **more readable and concise**.
3. They are mainly used with **functional interfaces**.
4. They use the **`::` (double colon) operator**.

---

## **Syntax of Method Reference**
```java
ClassName::methodName
```
or
```java
Object::methodName
```

---

## **Types of Method References**
Java supports **four types** of method references:

| Method Reference Type | Syntax Example | Equivalent Lambda Expression |
|-----------------------|---------------|-----------------------------|
| **1. Reference to a Static Method** | `ClassName::staticMethod` | `(args) -> ClassName.staticMethod(args)` |
| **2. Reference to an Instance Method of a Specific Object** | `instance::instanceMethod` | `(args) -> instance.instanceMethod(args)` |
| **3. Reference to an Instance Method of an Arbitrary Object** | `ClassName::instanceMethod` | `(obj, args) -> obj.instanceMethod(args)` |
| **4. Reference to a Constructor (Constructor Reference)** | `ClassName::new` | `(args) -> new ClassName(args)` |

---

## **1. Reference to a Static Method**
If a method is **static**, we can refer to it using the class name.

### **Example: Printing a List of Products (E-commerce Scenario)**
```java
import java.util.Arrays;
import java.util.List;

public class StaticMethodReferenceExample {
    public static void printProductName(String name) {
        System.out.println("Product Name: " + name);
    }

    public static void main(String[] args) {
        List<String> products = Arrays.asList("Laptop", "Smartphone", "Tablet");

        // Using lambda expression
        products.forEach(name -> StaticMethodReferenceExample.printProductName(name));

        // Using method reference
        products.forEach(StaticMethodReferenceExample::printProductName);
    }
}
```

### **Explanation:**
- Instead of writing a lambda `name -> StaticMethodReferenceExample.printProductName(name)`, we use **method reference**: `StaticMethodReferenceExample::printProductName`.

---

## **2. Reference to an Instance Method of a Specific Object**
If we have an **instance method**, we can refer to it using a specific object.

### **Example: Apply Discount on a Product**
```java
import java.util.function.Consumer;

public class InstanceMethodReferenceExample {
    public void applyDiscount(double price) {
        System.out.println("Discounted Price: $" + (price * 0.9));
    }

    public static void main(String[] args) {
        InstanceMethodReferenceExample obj = new InstanceMethodReferenceExample();

        Consumer<Double> discountLambda = price -> obj.applyDiscount(price);
        discountLambda.accept(100.0);

        // Using method reference
        Consumer<Double> discountMethodRef = obj::applyDiscount;
        discountMethodRef.accept(200.0);
    }
}
```

### **Explanation:**
- `Consumer<Double> discountMethodRef = obj::applyDiscount;` replaces the lambda `price -> obj.applyDiscount(price)`.
- The `applyDiscount()` method is called for each price.

---

## **3. Reference to an Instance Method of an Arbitrary Object**
This is used when a method is **called on an instance of a class that will be provided later**.

### **Example: Sort a List of Product Names Alphabetically**
```java
import java.util.Arrays;
import java.util.List;

public class ArbitraryInstanceMethodReference {
    public static void main(String[] args) {
        List<String> productNames = Arrays.asList("Laptop", "Tablet", "Smartphone");

        // Using lambda
        productNames.sort((p1, p2) -> p1.compareToIgnoreCase(p2));

        // Using method reference
        productNames.sort(String::compareToIgnoreCase);

        System.out.println(productNames);
    }
}
```

### **Explanation:**
- The method `compareToIgnoreCase()` is called on the **elements of the list**, so we use `String::compareToIgnoreCase`.
- The equivalent lambda is `(p1, p2) -> p1.compareToIgnoreCase(p2)`.

---

## **4. Reference to a Constructor (Constructor Reference)**
When we need to **create a new object**, we can use a constructor reference.

### **Example: Creating Product Objects**
```java
import java.util.function.Supplier;

class Product {
    public Product() {
        System.out.println("New Product Created");
    }
}

public class ConstructorReferenceExample {
    public static void main(String[] args) {
        // Using lambda
        Supplier<Product> productLambda = () -> new Product();

        // Using constructor reference
        Supplier<Product> productMethodRef = Product::new;

        // Creating objects
        productLambda.get();
        productMethodRef.get();
    }
}
```

### **Explanation:**
- `Supplier<Product> productMethodRef = Product::new;` replaces `() -> new Product()`.
- It creates a new `Product` object when `.get()` is called.

---

## **More E-commerce Retail Examples**

### **Example 1: Convert Product Names to Uppercase**
```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ProductNameConverter {
    public static void main(String[] args) {
        List<String> productNames = Arrays.asList("laptop", "mobile", "tablet");

        // Using lambda
        productNames.replaceAll(name -> name.toUpperCase());

        // Using method reference
        productNames.replaceAll(String::toUpperCase);

        System.out.println(productNames);
    }
}
```
**Explanation:**
- `String::toUpperCase` is used to convert each product name to uppercase.

---

### **Example 2: Compare Products by Price**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}

public class ProductPriceSorter {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200),
            new Product("Tablet", 600),
            new Product("Smartphone", 800)
        );

        // Using lambda
        products.sort((p1, p2) -> Double.compare(p1.getPrice(), p2.getPrice()));

        // Using method reference
        products.sort(ProductPriceSorter::compareByPrice);

        for (Product product : products) {
            System.out.println(product.name + " - $" + product.price);
        }
    }

    public static int compareByPrice(Product p1, Product p2) {
        return Double.compare(p1.getPrice(), p2.getPrice());
    }
}
```
**Explanation:**
- `ProductPriceSorter::compareByPrice` replaces `(p1, p2) -> compareByPrice(p1, p2)`, making the code cleaner.

---

## **Advantages of Method References**
1. **Concise and Readable**: Reduces redundant lambda expressions.
2. **Code Reusability**: Uses existing methods without rewriting them.
3. **Better Performance**: Optimized and easier to understand.

---

## **Summary of Method Reference Types**

| Type | Syntax | Example |
|------|--------|---------|
| **Static Method** | `Class::staticMethod` | `Math::abs` |
| **Instance Method (Specific Object)** | `instance::instanceMethod` | `product::getPrice` |
| **Instance Method (Arbitrary Object)** | `Class::instanceMethod` | `String::toUpperCase` |
| **Constructor Reference** | `Class::new` | `Product::new` |

---

## **Conclusion**
Method references **simplify** Java code by replacing lambda expressions with existing method names. They are widely used in functional programming, especially in **Stream API, sorting, and functional interfaces**. 

Would you like additional examples or explanations? 🚀

# What is Static Method Reference?

## **Introduction**
A **Static Method Reference** in Java refers to a static method of a class using the `::` **(double colon operator)**. It is a shorthand for writing lambda expressions that call a static method.

### **Key Points About Static Method Reference**
1. **Used as a replacement for lambda expressions** that invoke a static method.
2. **More readable and concise** than explicit lambda expressions.
3. Works with **functional interfaces** like `Consumer`, `Function`, `Predicate`, etc.
4. Syntax:  
   ```java
   ClassName::staticMethodName
   ```
5. Used commonly in **Stream API**, **Collections**, and **functional programming**.

---

## **Syntax of Static Method Reference**
```java
ClassName::staticMethod
```
- **`ClassName`** → The class that contains the static method.
- **`staticMethod`** → The name of the static method.

### **Equivalent Lambda Expression**
```java
(args) -> ClassName.staticMethod(args)
```
A **static method reference** is a direct replacement for a lambda expression that calls a static method.

---

## **Example 1: Static Method Reference vs Lambda Expression**
We have a static method `printMessage` inside the class `MessagePrinter`. 

```java
import java.util.function.Consumer;

class MessagePrinter {
    public static void printMessage(String message) {
        System.out.println("Message: " + message);
    }
}

public class StaticMethodReferenceExample {
    public static void main(String[] args) {
        // Using lambda expression
        Consumer<String> lambdaPrinter = message -> MessagePrinter.printMessage(message);
        lambdaPrinter.accept("Hello, Java!");

        // Using static method reference
        Consumer<String> methodRefPrinter = MessagePrinter::printMessage;
        methodRefPrinter.accept("Hello, Static Method Reference!");
    }
}
```
### **Explanation:**
1. The **lambda expression**:  
   ```java
   message -> MessagePrinter.printMessage(message)
   ```
   calls `printMessage()` inside `MessagePrinter`.

2. The **method reference**:  
   ```java
   MessagePrinter::printMessage
   ```
   replaces the lambda by directly referring to the static method.

**Output:**
```
Message: Hello, Java!
Message: Hello, Static Method Reference!
```

---

## **Example 2: Using Static Method Reference in Stream API (Sorting)**
Sorting a list of product names in an **e-commerce application**.

```java
import java.util.Arrays;
import java.util.List;

public class ProductSorter {
    public static void sortProducts(String product1, String product2) {
        System.out.println("Comparing: " + product1 + " and " + product2);
    }

    public static void main(String[] args) {
        List<String> products = Arrays.asList("Laptop", "Smartphone", "Tablet");

        // Using lambda expression
        products.sort((p1, p2) -> p1.compareToIgnoreCase(p2));

        // Using static method reference
        products.sort(String::compareToIgnoreCase);

        System.out.println(products);
    }
}
```

### **Explanation:**
1. **Lambda Expression:**
   ```java
   (p1, p2) -> p1.compareToIgnoreCase(p2)
   ```
   sorts product names.

2. **Method Reference:**
   ```java
   String::compareToIgnoreCase
   ```
   directly references the existing method for sorting.

**Output:**
```
[Laptop, Smartphone, Tablet]
```

---

## **Example 3: Using Static Method Reference with `Function` (E-commerce: Apply Discount)**
A function applies a **10% discount** on a product price.

```java
import java.util.function.Function;

class PriceCalculator {
    public static double applyDiscount(double price) {
        return price * 0.9; // 10% discount
    }
}

public class StaticMethodReferenceDiscount {
    public static void main(String[] args) {
        // Using lambda expression
        Function<Double, Double> discountLambda = price -> PriceCalculator.applyDiscount(price);
        System.out.println("Discounted Price (Lambda): $" + discountLambda.apply(100.0));

        // Using static method reference
        Function<Double, Double> discountMethodRef = PriceCalculator::applyDiscount;
        System.out.println("Discounted Price (Method Reference): $" + discountMethodRef.apply(100.0));
    }
}
```

### **Explanation:**
- **Lambda Expression:**  
  ```java
  price -> PriceCalculator.applyDiscount(price)
  ```
- **Static Method Reference:**  
  ```java
  PriceCalculator::applyDiscount
  ```
- The method reference makes the code **more concise**.

**Output:**
```
Discounted Price (Lambda): $90.0
Discounted Price (Method Reference): $90.0
```

---

## **Example 4: Static Method Reference with `Predicate` (E-commerce: Check Product Availability)**
Check if a product is **in stock** using `Predicate<T>`.

```java
import java.util.function.Predicate;

class StockManager {
    public static boolean isProductAvailable(int stock) {
        return stock > 0;
    }
}

public class StaticMethodReferenceStock {
    public static void main(String[] args) {
        // Using lambda expression
        Predicate<Integer> isAvailableLambda = stock -> StockManager.isProductAvailable(stock);
        System.out.println("Product Available? " + isAvailableLambda.test(10));

        // Using static method reference
        Predicate<Integer> isAvailableMethodRef = StockManager::isProductAvailable;
        System.out.println("Product Available? " + isAvailableMethodRef.test(0));
    }
}
```

### **Explanation:**
- **Lambda Expression:**  
  ```java
  stock -> StockManager.isProductAvailable(stock)
  ```
- **Static Method Reference:**  
  ```java
  StockManager::isProductAvailable
  ```
- The static method reference improves readability.

**Output:**
```
Product Available? true
Product Available? false
```

---

## **Example 5: Static Method Reference for Logging in an E-commerce Application**
A static method is used for logging order details.

```java
import java.util.function.Consumer;

class OrderLogger {
    public static void logOrder(String orderDetails) {
        System.out.println("Logging Order: " + orderDetails);
    }
}

public class StaticMethodReferenceLogger {
    public static void main(String[] args) {
        // Using lambda expression
        Consumer<String> orderLoggerLambda = details -> OrderLogger.logOrder(details);
        orderLoggerLambda.accept("Order #123: 2 items purchased");

        // Using static method reference
        Consumer<String> orderLoggerMethodRef = OrderLogger::logOrder;
        orderLoggerMethodRef.accept("Order #124: 5 items purchased");
    }
}
```

### **Explanation:**
- The static method reference **simplifies logging**.

**Output:**
```
Logging Order: Order #123: 2 items purchased
Logging Order: Order #124: 5 items purchased
```

---

## **Advantages of Static Method References**
| Feature | Benefit |
|---------|---------|
| **Readability** | Eliminates redundant lambda expressions. |
| **Conciseness** | Reduces code complexity by directly referring to existing methods. |
| **Performance** | More optimized by the JVM than lambda expressions. |
| **Reusability** | Utilizes existing static methods without modifying them. |

---

## **When to Use Static Method References?**
- **When the lambda body calls a static method.**
- **When working with functional interfaces** like `Consumer`, `Function`, `Predicate`, etc.
- **When sorting or filtering data using the Stream API.**
- **When improving logging and debugging processes.**

---

## **Conclusion**
Static Method References (`ClassName::staticMethod`) provide a **cleaner, more readable, and concise alternative** to lambda expressions when working with static methods. They are widely used in **functional programming, stream operations, and data processing**.

# What is Instance Method Reference of a Particular Object?
## **Introduction**
An **Instance Method Reference of a Particular Object** refers to an **instance method** of a specific object using the `::` (double colon operator). This feature, introduced in **Java 8**, helps improve code readability by eliminating redundant lambda expressions.

### **Key Points:**
1. **Replaces Lambda Expressions**: It provides a shorthand syntax when a lambda simply calls an instance method on a particular object.
2. **Syntax**:  
   ```java
   objectName::instanceMethod
   ```
3. **Works with Functional Interfaces**: Typically used with interfaces like `Consumer`, `Function`, `Predicate`, etc.
4. **Increases Readability**: Avoids redundant lambda code.

---

## **Syntax**
```java
object::instanceMethod
```
- **`object`** → The instance of the class.
- **`instanceMethod`** → The instance method to be referenced.

### **Equivalent Lambda Expression**
```java
(args) -> object.instanceMethod(args)
```
The instance method reference is a **shorter alternative** to the lambda expression that calls the method.

---

## **Example 1: Printing Order Details (E-commerce Scenario)**
Let's define a class `OrderProcessor` with an instance method `processOrder`.

```java
import java.util.function.Consumer;

class OrderProcessor {
    public void processOrder(String order) {
        System.out.println("Processing Order: " + order);
    }
}

public class InstanceMethodReferenceExample {
    public static void main(String[] args) {
        OrderProcessor orderProcessor = new OrderProcessor();

        // Using lambda expression
        Consumer<String> orderLambda = order -> orderProcessor.processOrder(order);
        orderLambda.accept("Order #12345");

        // Using method reference
        Consumer<String> orderMethodRef = orderProcessor::processOrder;
        orderMethodRef.accept("Order #67890");
    }
}
```

### **Explanation:**
1. **Lambda Expression:**  
   ```java
   order -> orderProcessor.processOrder(order)
   ```
   is calling `processOrder(order)`.
   
2. **Method Reference:**  
   ```java
   orderProcessor::processOrder
   ```
   eliminates redundancy, making the code **cleaner and more readable**.

**Output:**
```
Processing Order: Order #12345
Processing Order: Order #67890
```

---

## **Example 2: Logging Order Details**
Logging each order with a method reference.

```java
import java.util.Arrays;
import java.util.List;

class OrderLogger {
    public void logOrder(String orderDetails) {
        System.out.println("Logging Order: " + orderDetails);
    }
}

public class OrderLoggerExample {
    public static void main(String[] args) {
        List<String> orders = Arrays.asList("Order #101", "Order #102", "Order #103");

        OrderLogger logger = new OrderLogger();

        // Using lambda expression
        orders.forEach(order -> logger.logOrder(order));

        // Using method reference
        orders.forEach(logger::logOrder);
    }
}
```

### **Explanation:**
- The method reference `logger::logOrder` replaces `(order) -> logger.logOrder(order)`.
- The `forEach` method calls `logOrder` for each order.

**Output:**
```
Logging Order: Order #101
Logging Order: Order #102
Logging Order: Order #103
```

---

## **Example 3: Instance Method Reference with `Function`**
Transforming a **product name** to uppercase.

```java
import java.util.function.Function;

class ProductFormatter {
    public String formatName(String name) {
        return name.toUpperCase();
    }
}

public class ProductFormatterExample {
    public static void main(String[] args) {
        ProductFormatter formatter = new ProductFormatter();

        // Using lambda expression
        Function<String, String> formatLambda = name -> formatter.formatName(name);
        System.out.println(formatLambda.apply("Laptop"));

        // Using method reference
        Function<String, String> formatMethodRef = formatter::formatName;
        System.out.println(formatMethodRef.apply("Smartphone"));
    }
}
```

### **Explanation:**
- `Function<String, String>` transforms a **string to uppercase**.
- The method reference `formatter::formatName` simplifies the lambda.

**Output:**
```
LAPTOP
SMARTPHONE
```

---

## **Example 4: Applying Discounts Using Method References**
Applying a **10% discount** to a product price.

```java
import java.util.function.UnaryOperator;

class PriceCalculator {
    public double applyDiscount(double price) {
        return price * 0.9;
    }
}

public class PriceCalculatorExample {
    public static void main(String[] args) {
        PriceCalculator calculator = new PriceCalculator();

        // Using lambda expression
        UnaryOperator<Double> discountLambda = price -> calculator.applyDiscount(price);
        System.out.println("Discounted Price: $" + discountLambda.apply(200.0));

        // Using method reference
        UnaryOperator<Double> discountMethodRef = calculator::applyDiscount;
        System.out.println("Discounted Price: $" + discountMethodRef.apply(200.0));
    }
}
```

### **Explanation:**
- `UnaryOperator<Double>` modifies a **product price**.
- The method reference `calculator::applyDiscount` replaces the lambda.

**Output:**
```
Discounted Price: $180.0
Discounted Price: $180.0
```

---

## **Example 5: Checking Product Availability Using `Predicate`**
Checking if a **product is in stock**.

```java
import java.util.function.Predicate;

class Inventory {
    public boolean isAvailable(int stock) {
        return stock > 0;
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        // Using lambda expression
        Predicate<Integer> isInStockLambda = stock -> inventory.isAvailable(stock);
        System.out.println("Product Available? " + isInStockLambda.test(5));

        // Using method reference
        Predicate<Integer> isInStockMethodRef = inventory::isAvailable;
        System.out.println("Product Available? " + isInStockMethodRef.test(0));
    }
}
```

### **Explanation:**
- `Predicate<Integer>` checks if stock is greater than zero.
- The method reference `inventory::isAvailable` replaces the lambda.

**Output:**
```
Product Available? true
Product Available? false
```

---

## **Advantages of Instance Method Reference of a Particular Object**
| Feature | Benefit |
|---------|---------|
| **Readability** | Eliminates redundant lambda expressions. |
| **Conciseness** | Makes the code more compact. |
| **Performance** | JVM optimizes method references better than lambda expressions. |
| **Reusability** | Uses existing instance methods without modifications. |

---

## **When to Use Instance Method References?**
✅ **When you have an existing instance method that performs an action.**  
✅ **When using functional interfaces like `Consumer`, `Function`, `Predicate`, etc.**  
✅ **When iterating over lists using `forEach`.**  
✅ **When improving readability and reducing redundant lambda expressions.**  

---

## **Summary**
| Type | Syntax | Example |
|------|--------|---------|
| **Instance Method Reference of a Particular Object** | `object::instanceMethod` | `orderProcessor::processOrder` |
| **Equivalent Lambda Expression** | `(args) -> object.instanceMethod(args)` | `(order) -> orderProcessor.processOrder(order)` |

---

## **Conclusion**
Instance method references (`object::instanceMethod`) make **lambda expressions more concise and readable**. They are commonly used in **logging, formatting, applying discounts, and checking stock availability** in **e-commerce applications**.

# What is Instance Method Reference of an Arbitrary Object of a Particular Type?

## **Introduction**
An **Instance Method Reference of an Arbitrary Object of a Particular Type** in Java allows you to refer to an **instance method** of an object that **will be provided later** at runtime. This method reference is particularly useful when working with **streams, collections, and functional interfaces**.

### **Key Points:**
1. **Works on an instance method of an object that is not explicitly specified.**
2. **Used with functional interfaces like `Function`, `Predicate`, `Consumer`, etc.**
3. **Commonly used in `forEach`, `sort`, and `map` operations in Streams.**
4. **Syntax:**  
   ```java
   ClassName::instanceMethod
   ```
5. **Equivalent Lambda Expression:**  
   ```java
   (obj, args) -> obj.instanceMethod(args)
   ```

---

## **Syntax**
```java
ClassName::instanceMethod
```
- **`ClassName`** → The class that contains the instance method.
- **`instanceMethod`** → The instance method name.

---

## **Example 1: Sorting a List of Product Names**
Sorting a list of product names **alphabetically**.

```java
import java.util.Arrays;
import java.util.List;

public class ProductSorter {
    public static void main(String[] args) {
        List<String> products = Arrays.asList("Laptop", "Smartphone", "Tablet");

        // Using lambda expression
        products.sort((p1, p2) -> p1.compareToIgnoreCase(p2));

        // Using instance method reference of an arbitrary object
        products.sort(String::compareToIgnoreCase);

        System.out.println(products);
    }
}
```

### **Explanation:**
1. **Lambda Expression:**  
   ```java
   (p1, p2) -> p1.compareToIgnoreCase(p2)
   ```
   compares two product names.

2. **Method Reference:**  
   ```java
   String::compareToIgnoreCase
   ```
   refers to `compareToIgnoreCase()` of an **arbitrary `String` object**.

**Output:**
```
[Laptop, Smartphone, Tablet]
```

---

## **Example 2: Converting Product Names to Uppercase**
Using method reference to **convert product names to uppercase**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductFormatter {
    public static void main(String[] args) {
        List<String> productNames = Arrays.asList("laptop", "mobile", "tablet");

        // Using lambda expression
        List<String> uppercaseNamesLambda = productNames.stream()
                .map(name -> name.toUpperCase())
                .collect(Collectors.toList());

        // Using method reference
        List<String> uppercaseNamesMethodRef = productNames.stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(uppercaseNamesMethodRef);
    }
}
```

### **Explanation:**
- **Lambda Expression:**  
  ```java
  name -> name.toUpperCase()
  ```
  calls `toUpperCase()` for each product name.

- **Method Reference:**  
  ```java
  String::toUpperCase
  ```
  refers to `toUpperCase()` method of an **arbitrary `String` object**.

**Output:**
```
[LAPTOP, MOBILE, TABLET]
```

---

## **Example 3: Checking Product Availability Using `Predicate`**
Filtering products that **contain "phone"** in their names.

```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProductFilter {
    public static void main(String[] args) {
        List<String> products = Arrays.asList("Laptop", "Smartphone", "Tablet", "Headphones");

        // Using lambda expression
        List<String> filteredLambda = products.stream()
                .filter(product -> product.contains("phone"))
                .collect(Collectors.toList());

        // Using method reference
        List<String> filteredMethodRef = products.stream()
                .filter("phone"::contains)
                .collect(Collectors.toList());

        System.out.println(filteredMethodRef);
    }
}
```

### **Explanation:**
- **Lambda Expression:**  
  ```java
  product -> product.contains("phone")
  ```
  checks if `"phone"` is in the product name.

- **Method Reference:**  
  ```java
  "phone"::contains
  ```
  refers to `contains()` method of an **arbitrary `String` object**.

**Output:**
```
[Smartphone, Headphones]
```

---

## **Example 4: Extracting Product Prices**
Using method reference to **extract prices** from a list of products.

```java
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

class Product {
    private String name;
    private double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}

public class ProductPriceExtractor {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product("Laptop", 1200.0),
                new Product("Tablet", 600.0),
                new Product("Smartphone", 800.0)
        );

        // Using lambda expression
        List<Double> pricesLambda = products.stream()
                .map(product -> product.getPrice())
                .collect(Collectors.toList());

        // Using method reference
        List<Double> pricesMethodRef = products.stream()
                .map(Product::getPrice)
                .collect(Collectors.toList());

        System.out.println(pricesMethodRef);
    }
}
```

### **Explanation:**
- **Lambda Expression:**  
  ```java
  product -> product.getPrice()
  ```
  calls `getPrice()` for each product.

- **Method Reference:**  
  ```java
  Product::getPrice
  ```
  refers to `getPrice()` method of an **arbitrary `Product` object**.

**Output:**
```
[1200.0, 600.0, 800.0]
```

---

## **Example 5: Printing Order Details Using `Consumer`**
Logging **each order's details**.

```java
import java.util.Arrays;
import java.util.List;

class Order {
    private String orderId;

    public Order(String orderId) {
        this.orderId = orderId;
    }

    public void printOrder() {
        System.out.println("Processing Order: " + orderId);
    }
}

public class OrderProcessor {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
                new Order("ORD123"),
                new Order("ORD456"),
                new Order("ORD789")
        );

        // Using lambda expression
        orders.forEach(order -> order.printOrder());

        // Using method reference
        orders.forEach(Order::printOrder);
    }
}
```

### **Explanation:**
- **Lambda Expression:**  
  ```java
  order -> order.printOrder()
  ```
  calls `printOrder()` for each order.

- **Method Reference:**  
  ```java
  Order::printOrder
  ```
  refers to `printOrder()` method of an **arbitrary `Order` object**.

**Output:**
```
Processing Order: ORD123
Processing Order: ORD456
Processing Order: ORD789
```

---

## **Advantages of Instance Method Reference of an Arbitrary Object**
| Feature | Benefit |
|---------|---------|
| **Readability** | Eliminates redundant lambda expressions. |
| **Conciseness** | Makes code compact and expressive. |
| **Performance** | JVM optimizes method references better than lambda expressions. |
| **Reusability** | Uses existing instance methods without modifications. |

---

## **When to Use Instance Method References?**
✅ **When working with Streams for data processing.**  
✅ **When iterating over lists using `forEach`.**  
✅ **When transforming or filtering data in collections.**  
✅ **When using functional interfaces like `Function`, `Predicate`, `Consumer`.**  

---

## **Conclusion**
Instance Method References of an Arbitrary Object (`ClassName::instanceMethod`) **simplify code, improve readability, and enhance performance**. They are widely used in **functional programming, collections, and stream operations**.

Would you like more **real-world examples** or further clarifications? 🚀

# What is Constructor Reference?
### **Introduction**
A **constructor reference** in Java allows you to refer to a class constructor using the `::new` operator. It is a concise way to create new objects within functional programming, especially when working with **lambda expressions** and **functional interfaces**.

### **Key Points**
- Constructor references replace explicit object creation in **lambda expressions**.
- They use the **`::new`** syntax.
- They work with **functional interfaces** that return objects.
- They improve **readability** and **reduce boilerplate**.

---

## **Syntax**
```java
ClassName::new
```
Equivalent to:
```java
() -> new ClassName();
(args) -> new ClassName(args);
```

---

## **Types of Constructor References**

| Type | Syntax | Equivalent Lambda |
|------|--------|------------------|
| **No-Argument Constructor** | `ClassName::new` | `() -> new ClassName()` |
| **Parameterized Constructor** | `ClassName::new` | `(args) -> new ClassName(args)` |
| **Array Constructor** | `Type[]::new` | `(size) -> new Type[size]` |

---

## **No-Argument Constructor Reference**
Used when a constructor does **not** take parameters.

### **Example: Creating Product Objects**
```java
import java.util.function.Supplier;

class Product {
    public Product() {
        System.out.println("New Product Created");
    }
}

public class ConstructorReferenceExample {
    public static void main(String[] args) {
        // Using lambda
        Supplier<Product> lambdaProduct = () -> new Product();
        lambdaProduct.get();

        // Using constructor reference
        Supplier<Product> methodRefProduct = Product::new;
        methodRefProduct.get();
    }
}
```
### **Explanation**
- `Supplier<Product>` provides an object.
- `Product::new` replaces `() -> new Product()`, making the code cleaner.

**Output:**
```
New Product Created
New Product Created
```

---

## **Parameterized Constructor Reference**
Used when a constructor takes **arguments**.

### **Example: Creating Products with Names**
```java
import java.util.function.Function;

class Product {
    String name;
    public Product(String name) {
        this.name = name;
        System.out.println("Product Created: " + name);
    }
}

public class ConstructorReferenceExample {
    public static void main(String[] args) {
        // Using lambda
        Function<String, Product> lambdaProduct = name -> new Product(name);
        lambdaProduct.apply("Laptop");

        // Using constructor reference
        Function<String, Product> methodRefProduct = Product::new;
        methodRefProduct.apply("Smartphone");
    }
}
```
### **Explanation**
- `Function<String, Product>` maps a name to a `Product`.
- `Product::new` replaces `(name) -> new Product(name)`.

**Output:**
```
Product Created: Laptop
Product Created: Smartphone
```

---

## **Array Constructor Reference**
Used when creating **arrays** dynamically.

### **Example: Creating a String Array**
```java
import java.util.function.IntFunction;

public class ArrayReferenceExample {
    public static void main(String[] args) {
        // Using lambda
        IntFunction<String[]> lambdaArray = size -> new String[size];
        String[] lambdaResult = lambdaArray.apply(5);

        // Using constructor reference
        IntFunction<String[]> methodRefArray = String[]::new;
        String[] methodRefResult = methodRefArray.apply(5);

        System.out.println("Array size: " + methodRefResult.length);
    }
}
```
### **Explanation**
- `IntFunction<String[]>` maps an integer (size) to a `String[]`.
- `String[]::new` replaces `size -> new String[size]`.

**Output:**
```
Array size: 5
```

---

## **Real-World E-commerce Examples**

### **1. Creating Orders with Constructor Reference**
```java
import java.util.function.Function;

class Order {
    String orderId;
    public Order(String orderId) {
        this.orderId = orderId;
        System.out.println("Order Created: " + orderId);
    }
}

public class OrderReferenceExample {
    public static void main(String[] args) {
        // Using lambda
        Function<String, Order> lambdaOrder = id -> new Order(id);
        lambdaOrder.apply("ORD123");

        // Using constructor reference
        Function<String, Order> methodRefOrder = Order::new;
        methodRefOrder.apply("ORD456");
    }
}
```
**Output:**
```
Order Created: ORD123
Order Created: ORD456
```

---

### **2. Creating a List of Products Using Streams**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    public Product(String name) {
        this.name = name;
    }
}

public class ProductListExample {
    public static void main(String[] args) {
        List<String> productNames = Arrays.asList("Laptop", "Tablet", "Smartphone");

        // Using constructor reference to create Product objects
        List<Product> products = productNames.stream()
                                             .map(Product::new)
                                             .collect(Collectors.toList());

        products.forEach(p -> System.out.println("Product: " + p.name));
    }
}
```
**Output:**
```
Product: Laptop
Product: Tablet
Product: Smartphone
```

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Readability** | Removes redundant `new` calls in lambdas. |
| **Conciseness** | Simplifies object creation. |
| **Performance** | JVM optimizes constructor references efficiently. |
| **Reusability** | Uses existing constructors without modification. |

---

## **When to Use Constructor References?**
✅ **When working with Streams for object creation.**  
✅ **When using functional interfaces like `Supplier`, `Function`, `BiFunction`.**  
✅ **When improving readability and reducing redundant lambdas.**  
✅ **When dynamically creating arrays.**  

---

## **Conclusion**
Constructor references (`ClassName::new`) provide a **cleaner and more efficient way to create objects** in Java. They are widely used in **Streams, Collections, and functional programming**.

Would you like more advanced examples? 🚀

# What is Optional Interface?
### **Introduction**
The **Optional** class in Java, introduced in **Java 8** (`java.util.Optional`), is a container object that may or may not contain a **non-null** value. It is used to avoid **NullPointerException (NPE)** and makes code more **readable, maintainable, and safe**.

### **Key Points**
- **Handles null values** without explicit checks.
- Provides methods like `isPresent()`, `orElse()`, and `ifPresent()`.
- Supports **functional programming** by allowing chaining with methods like `map()` and `flatMap()`.
- Helps avoid **unnecessary null checks** in the code.

---

## **Syntax**
```java
Optional<Type> variable = Optional.ofNullable(value);
```

---

## **Ways to Create an Optional Object**
| Method | Description | Example |
|--------|-------------|---------|
| `Optional.of(value)` | Creates an `Optional` with a **non-null value** (throws exception if null). | `Optional<String> opt = Optional.of("Laptop");` |
| `Optional.ofNullable(value)` | Creates an `Optional` with a **nullable value**. | `Optional<String> opt = Optional.ofNullable(null);` |
| `Optional.empty()` | Creates an **empty Optional**. | `Optional<String> opt = Optional.empty();` |

---

## **Basic Example (Avoiding NullPointerException)**
```java
import java.util.Optional;

public class OptionalExample {
    public static void main(String[] args) {
        String productName = null;

        // Without Optional (NullPointerException Risk)
        try {
            System.out.println(productName.toUpperCase()); 
        } catch (NullPointerException e) {
            System.out.println("Caught NullPointerException");
        }

        // With Optional (Safe)
        Optional<String> optionalProduct = Optional.ofNullable(productName);
        System.out.println(optionalProduct.map(String::toUpperCase).orElse("No Product Available"));
    }
}
```
### **Output**
```
Caught NullPointerException
No Product Available
```

---

## **E-Commerce Use Cases for Optional**

### **1. Avoiding Null Checks When Fetching Product Details**
```java
import java.util.Optional;

class Product {
    private String name;
    
    public Product(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
}

class ProductService {
    public Optional<Product> getProductById(int id) {
        if (id == 1) {
            return Optional.of(new Product("Laptop"));
        } else {
            return Optional.empty(); // No product found
        }
    }
}

public class OptionalProductExample {
    public static void main(String[] args) {
        ProductService productService = new ProductService();

        Optional<Product> product = productService.getProductById(2);

        // Without Optional: Risk of NullPointerException
        System.out.println(product.isPresent() ? product.get().getName() : "Product Not Found");

        // With Optional (Safer)
        System.out.println(product.map(Product::getName).orElse("Product Not Found"));
    }
}
```
### **Output**
```
Product Not Found
Product Not Found
```

---

### **2. Handling Null in Customer Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private String name;
    private Integer loyaltyPoints;

    public Customer(String name, Integer loyaltyPoints) {
        this.name = name;
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customerWithPoints = new Customer("Alice", 100);
        Customer customerWithoutPoints = new Customer("Bob", null);

        System.out.println(customerWithPoints.getLoyaltyPoints().orElse(0)); // Output: 100
        System.out.println(customerWithoutPoints.getLoyaltyPoints().orElse(0)); // Output: 0
    }
}
```
- Customers who **don’t have** loyalty points get **0** instead of `null`.

---

### **3. Checking If a Product Is Available in Inventory**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 10);
        stock.put("Tablet", 0);
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product));
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        String product = "Tablet";

        int availableStock = inventory.getStock(product).filter(s -> s > 0).orElse(0);
        
        System.out.println(product + " Available Stock: " + availableStock);
    }
}
```
### **Output**
```
Tablet Available Stock: 0
```
- **`filter(s -> s > 0)`** ensures that only positive stock values are returned.

---

### **4. Applying Discount Only If Product is Eligible**
```java
import java.util.Optional;

class Product {
    private String name;
    private Double price;

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Optional<Double> applyDiscount(double discountRate) {
        return price > 100 ? Optional.of(price * (1 - discountRate)) : Optional.empty();
    }
}

public class DiscountExample {
    public static void main(String[] args) {
        Product expensiveProduct = new Product("Gaming Laptop", 1200.0);
        Product cheapProduct = new Product("Mouse", 30.0);

        System.out.println("Discounted Price: " +
                expensiveProduct.applyDiscount(0.10).orElse(expensiveProduct.price));

        System.out.println("Discounted Price: " +
                cheapProduct.applyDiscount(0.10).orElse(cheapProduct.price));
    }
}
```
### **Output**
```
Discounted Price: 1080.0
Discounted Price: 30.0
```
- **Expensive products** get a discount, but cheaper ones remain unchanged.

---

## **Useful Methods in `Optional`**

| Method | Description | Example |
|--------|-------------|---------|
| `isPresent()` | Checks if a value is present | `opt.isPresent()` |
| `orElse(defaultValue)` | Returns value if present, otherwise returns a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Returns value if present, otherwise uses Supplier function | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow()` | Returns value if present, otherwise throws an exception | `opt.orElseThrow(() -> new Exception("Not found"))` |
| `map(Function)` | Transforms the value if present | `opt.map(String::toUpperCase)` |
| `flatMap(Function)` | Similar to `map()`, but used when function returns `Optional<T>` | `opt.flatMap(User::getAddressOptional)` |
| `filter(Predicate)` | Returns value if predicate is true, otherwise returns empty | `opt.filter(val -> val > 10)` |

---

## **Advantages**
✅ **Avoids NullPointerException (NPE)**.  
✅ **Improves Readability** (no need for `if (value != null)`).  
✅ **Encourages Functional Programming** (works well with `map()`, `filter()`).  
✅ **Standardizes Null Handling** across the codebase.  

---

## **Conclusion**
The **Optional** class is a powerful tool for **handling missing values** in Java applications. It is widely used in **e-commerce** to handle scenarios like:
- **Checking product availability**
- **Fetching customer loyalty points**
- **Applying discounts conditionally**
- **Returning default values instead of null**

# What is Optional.of()?

### **Introduction**
The `Optional.of()` method is a part of **Java 8’s** `java.util.Optional` class. It is used to create an `Optional` object that contains a **non-null value**. If the provided value is `null`, it will throw a **NullPointerException (NPE)**.

### **Key Points**
- Used to **wrap a non-null value** inside an `Optional`.
- Throws **NullPointerException** if the value is `null`.
- Helps avoid **explicit null checks** in code.
- Encourages **functional programming** and cleaner code.

---

## **Syntax**
```java
Optional<T> optionalValue = Optional.of(value);
```
- **T** → Type of value stored inside `Optional`.
- **value** → The non-null object to wrap.

### **Equivalent to:**
```java
if (value == null) {
    throw new NullPointerException();
} else {
    return Optional.of(value);
}
```

---

## **Example 1: Creating an Optional with a Non-Null Value**
```java
import java.util.Optional;

public class OptionalOfExample {
    public static void main(String[] args) {
        String productName = "Laptop";
        
        // Creating Optional using Optional.of()
        Optional<String> optionalProduct = Optional.of(productName);
        
        System.out.println("Product Name: " + optionalProduct.get());
    }
}
```
### **Output**
```
Product Name: Laptop
```
- The `Optional.of()` method stores `"Laptop"` inside an `Optional` container.

---

## **Example 2: Handling NullPointerException in Optional.of()**
```java
import java.util.Optional;

public class OptionalOfNullExample {
    public static void main(String[] args) {
        String productName = null;

        // This will throw NullPointerException
        Optional<String> optionalProduct = Optional.of(productName);

        System.out.println(optionalProduct.get());
    }
}
```
### **Output**
```
Exception in thread "main" java.lang.NullPointerException
```
- Since `productName` is `null`, `Optional.of(null)` **throws NullPointerException**.

---

## **Best Practice: Use `Optional.ofNullable()` for Null Safety**
If the value might be `null`, use **`Optional.ofNullable()`** instead:
```java
Optional<String> optionalProduct = Optional.ofNullable(productName);
```
This prevents **NullPointerException** by returning `Optional.empty()` if the value is `null`.

---

## **E-commerce Use Cases for `Optional.of()`**

### **1. Fetching Product Details**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

public class OptionalProductExample {
    public static void main(String[] args) {
        Product product = new Product("Smartphone");

        // Ensuring non-null Product
        Optional<Product> optionalProduct = Optional.of(product);

        System.out.println("Product Name: " + optionalProduct.get().getName());
    }
}
```
### **Output**
```
Product Name: Smartphone
```
- Ensures that `Product` is **not null** before use.

---

### **2. Validating Customer Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.of(loyaltyPoints); // Ensures it is not null
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customer = new Customer(150);
        
        System.out.println("Loyalty Points: " + customer.getLoyaltyPoints().get());
    }
}
```
### **Output**
```
Loyalty Points: 150
```
- The `Optional.of(loyaltyPoints)` ensures the value **cannot be null**.

---

### **3. Ensuring Non-Null Stock Availability**
```java
import java.util.Optional;

class Inventory {
    private Integer stock;

    public Inventory(Integer stock) {
        this.stock = stock;
    }

    public Optional<Integer> getStock() {
        return Optional.of(stock); // Ensures stock is always non-null
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory(10);
        
        System.out.println("Available Stock: " + inventory.getStock().get());
    }
}
```
### **Output**
```
Available Stock: 10
```
- Ensures **stock availability** is never `null`.

---

## **Common Methods Used with `Optional.of()`**

### **1. `isPresent()`**
```java
Optional<String> opt = Optional.of("Laptop");
System.out.println(opt.isPresent()); // Output: true
```

### **2. `get()`**
```java
Optional<String> opt = Optional.of("Tablet");
System.out.println(opt.get()); // Output: Tablet
```

### **3. `orElse()`**
```java
Optional<String> opt = Optional.of("Mouse");
System.out.println(opt.orElse("Default Product")); // Output: Mouse
```

### **4. `map()`**
```java
Optional<String> opt = Optional.of("TV");
System.out.println(opt.map(String::toUpperCase).get()); // Output: TV
```

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures values are never `null` inside `Optional`. |
| **Encourages Functional Programming** | Works well with `map()`, `filter()`, etc. |
| **Readability** | Makes the code **cleaner** and more **maintainable**. |

---

## **When to Use `Optional.of()`?**
✅ **When you are sure the value is NOT null.**  
✅ **For ensuring non-null return values from methods.**  
✅ **When dealing with non-nullable data like Product ID, Prices, etc.**  

❌ **Do NOT use `Optional.of()` with potentially null values.** Instead, use:
```java
Optional.ofNullable(value);
```

---

## **Conclusion**
`Optional.of()` is a **powerful tool** for ensuring **non-null values** in Java. It is widely used in **e-commerce applications** for handling **product details, stock availability, and customer data** safely.

# What is Optional.ofNullable()?

### **Introduction**
The **`Optional.ofNullable()`** method in Java **(introduced in Java 8)** is used to create an `Optional` object that **may or may not contain a value**. Unlike `Optional.of()`, it **does not throw a `NullPointerException`** if the value is `null`. Instead, it returns `Optional.empty()`.

### **Key Points**
- **Handles both `null` and non-null values safely**.
- Returns an `Optional` containing the value if **not null**, otherwise returns an **empty Optional**.
- Prevents `NullPointerException` while avoiding unnecessary **null checks**.
- Encourages **functional programming** and **cleaner code**.

---

## **Syntax**
```java
Optional<T> optionalValue = Optional.ofNullable(value);
```
- **T** → Type of value stored inside `Optional`.
- **value** → The object that may be `null` or non-null.

### **Equivalent to**
```java
if (value != null) {
    return Optional.of(value);
} else {
    return Optional.empty();
}
```

---

## **Difference Between `Optional.of()` and `Optional.ofNullable()`**
| Method | Accepts `null`? | Throws `NullPointerException`? | Use Case |
|--------|---------------|-----------------------------|----------|
| `Optional.of(value)` | ❌ No | ✅ Yes (if `null`) | When you are **100% sure** the value is not `null`. |
| `Optional.ofNullable(value)` | ✅ Yes | ❌ No | When the value **might be null**. |

---

## **Example 1: Using `Optional.ofNullable()` to Handle Null Safely**
```java
import java.util.Optional;

public class OptionalNullableExample {
    public static void main(String[] args) {
        String productName = null;

        // Using Optional.ofNullable() (Safe)
        Optional<String> optionalProduct = Optional.ofNullable(productName);

        // Checking if value is present
        System.out.println(optionalProduct.isPresent() ? optionalProduct.get() : "No Product Available");
    }
}
```
### **Output**
```
No Product Available
```
- Since `productName` is `null`, `Optional.ofNullable()` returns `Optional.empty()`.

---

## **E-Commerce Use Cases**

### **1. Retrieving Product Name Without Null Checks**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name); // Handles null safely
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Product product = new Product(null); // Product has no name

        System.out.println(product.getName().orElse("Unknown Product"));
    }
}
```
### **Output**
```
Unknown Product
```
- `Optional.ofNullable(name)` prevents `NullPointerException` when `name` is `null`.

---

### **2. Checking Stock Availability Without Null Checks**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 5);
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product)); // Prevents NullPointerException
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        String product = "Smartphone";
        int availableStock = inventory.getStock(product).orElse(0);

        System.out.println("Available Stock: " + availableStock);
    }
}
```
### **Output**
```
Available Stock: 0
```
- If `product` is not in `stock`, it returns `Optional.empty()` instead of `null`.

---

### **3. Retrieving Customer Loyalty Points Safely**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customer = new Customer(null); // No loyalty points

        int points = customer.getLoyaltyPoints().orElse(0);

        System.out.println("Loyalty Points: " + points);
    }
}
```
### **Output**
```
Loyalty Points: 0
```
- If the customer **doesn’t have** loyalty points, it **returns `0` instead of `null`**.

---

### **4. Filtering Discounted Products Using `Optional`**
```java
import java.util.Optional;

class Product {
    private Double price;

    public Product(Double price) {
        this.price = price;
    }

    public Optional<Double> applyDiscount(double discountRate) {
        return price > 100 ? Optional.of(price * (1 - discountRate)) : Optional.empty();
    }
}

public class DiscountExample {
    public static void main(String[] args) {
        Product product = new Product(80.0); // Price is below discount threshold

        double discountedPrice = product.applyDiscount(0.10).orElse(product.price);

        System.out.println("Final Price: $" + discountedPrice);
    }
}
```
### **Output**
```
Final Price: $80.0
```
- If the product **is not eligible for a discount**, it returns the **original price**.

---

## **Common Methods Used with `Optional.ofNullable()`**

| Method | Description | Example |
|--------|-------------|---------|
| `isPresent()` | Checks if a value is present | `opt.isPresent()` |
| `orElse(defaultValue)` | Returns value if present, otherwise a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Returns value if present, otherwise uses Supplier function | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow()` | Returns value if present, otherwise throws an exception | `opt.orElseThrow(() -> new Exception("Not found"))` |
| `map(Function)` | Transforms the value if present | `opt.map(String::toUpperCase)` |
| `filter(Predicate)` | Returns value if predicate is true, otherwise returns empty | `opt.filter(val -> val > 10)` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures safe handling of nullable values. |
| **Eliminates Explicit Null Checks** | Replaces `if (value != null)`. |
| **Improves Readability** | Cleaner, more maintainable code. |
| **Works with Functional Programming** | Supports `map()`, `filter()`, and `flatMap()`. |

---

## **When to Use `Optional.ofNullable()`?**
✅ **When a value might be `null`** (e.g., database queries, external API responses).  
✅ **When you want to avoid explicit `null` checks** in your code.  
✅ **When working with Streams and Functional Programming**.  
❌ **Avoid using it inside loops or performance-sensitive code**, as `Optional` adds slight overhead.  

---

## **Conclusion**
`Optional.ofNullable()` is a **safe and effective** way to handle nullable values in Java. It is widely used in **e-commerce applications** to handle **product details, stock availability, and customer data** safely.

# What is Optional.empty()?
### **Introduction**
The `Optional.empty()` method in Java **(introduced in Java 8)** is used to create an **empty `Optional`** instance that **contains no value**. It is a useful alternative to returning `null`, helping to avoid **NullPointerException (NPE)** and making code **more readable and maintainable**.

---

## **Key Points**
- Creates an **empty `Optional` object** with no value.
- **Avoids returning `null`**, making code safer.
- Works well with **functional programming** and **stream operations**.
- Helps in **default handling** using methods like `orElse()` and `orElseGet()`.

---

## **Syntax**
```java
Optional<T> emptyOptional = Optional.empty();
```
This is equivalent to:
```java
Optional<T> emptyOptional = Optional.ofNullable(null);
```

---

## **Example 1: Creating an Empty Optional**
```java
import java.util.Optional;

public class OptionalEmptyExample {
    public static void main(String[] args) {
        Optional<String> emptyOpt = Optional.empty();

        System.out.println("Is value present? " + emptyOpt.isPresent()); // false
    }
}
```
### **Output**
```
Is value present? false
```
- Since `Optional.empty()` contains **no value**, `isPresent()` returns `false`.

---

## **E-commerce Use Cases**

### **1. Handling Missing Product Details**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public static Optional<Product> findById(int id) {
        return id == 1 ? Optional.of(new Product("Laptop")) : Optional.empty();
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Optional<Product> product = Product.findById(2);

        System.out.println("Product: " + product.map(p -> p.name).orElse("Product Not Found"));
    }
}
```
### **Output**
```
Product: Product Not Found
```
- If the product does not exist, `findById()` returns `Optional.empty()` instead of `null`.

---

### **2. Checking Stock Availability**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 10);
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product)); // Returns Optional.empty() if product is missing
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        
        String product = "Smartphone";
        int availableStock = inventory.getStock(product).orElse(0);

        System.out.println("Available Stock: " + availableStock);
    }
}
```
### **Output**
```
Available Stock: 0
```
- `Optional.empty()` ensures that missing stock **defaults to 0**.

---

### **3. Returning Default Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customer = new Customer(null);

        int points = customer.getLoyaltyPoints().orElse(0);

        System.out.println("Loyalty Points: " + points);
    }
}
```
### **Output**
```
Loyalty Points: 0
```
- If loyalty points are `null`, `Optional.empty()` ensures **default points of 0**.

---

## **Common Methods Used with `Optional.empty()`**
| Method | Description | Example |
|--------|-------------|---------|
| `isPresent()` | Checks if a value is present | `emptyOpt.isPresent()` → `false` |
| `orElse(defaultValue)` | Returns value if present, otherwise a default value | `emptyOpt.orElse("Default")` |
| `orElseGet(Supplier)` | Returns value if present, otherwise uses Supplier function | `emptyOpt.orElseGet(() -> "Fetched Default")` |
| `orElseThrow()` | Throws an exception if value is not present | `emptyOpt.orElseThrow(() -> new Exception("No value"))` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Avoids returning `null`, making code safer. |
| **Encourages Functional Programming** | Works with `map()`, `filter()`, and `flatMap()`. |
| **Improves Readability** | Replaces `if (value != null)`, making code cleaner. |

---

## **When to Use `Optional.empty()`?**
✅ **When a method might return no value** (e.g., searching for a product).  
✅ **When avoiding `null` in APIs and method returns**.  
✅ **When working with streams and functional interfaces**.  

❌ **Avoid excessive use** as it adds slight overhead.  

---

## **Conclusion**
`Optional.empty()` is a **powerful tool** for handling **missing values safely** in Java. It is widely used in **e-commerce applications** to handle **product availability, stock management, and customer data** effectively.

Would you like more real-world use cases? 🚀

# What is Optional.get()?

### **Introduction**
The **`Optional.get()`** method in Java **(introduced in Java 8)** is used to retrieve the value stored inside an `Optional` object. However, if the `Optional` is empty, calling `get()` will throw a `NoSuchElementException`. 

### **Key Points**
- **Returns the stored value** inside an `Optional`.
- Throws **`NoSuchElementException`** if the `Optional` is empty.
- Should **only be used when you are certain** that a value exists.
- Safer alternatives include **`orElse()`, `orElseGet()`, and `orElseThrow()`**.

---

## **Syntax**
```java
T value = optionalValue.get();
```
- **T** → Type of the stored value.
- **optionalValue** → An `Optional<T>` object that contains or does not contain a value.

---

## **Example 1: Using `get()` Safely**
```java
import java.util.Optional;

public class OptionalGetExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");

        if (optionalProduct.isPresent()) {
            System.out.println("Product: " + optionalProduct.get());
        }
    }
}
```
### **Output**
```
Product: Laptop
```
- Since `optionalProduct` contains `"Laptop"`, `get()` returns it.

---

## **Example 2: `get()` Throws `NoSuchElementException` for Empty Optional**
```java
import java.util.Optional;

public class OptionalEmptyGetExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        System.out.println(emptyOptional.get()); // Throws NoSuchElementException
    }
}
```
### **Output**
```
Exception in thread "main" java.util.NoSuchElementException: No value present
```
- Since `Optional.empty()` contains **no value**, calling `get()` results in **an exception**.

---

## **Best Practices: Use Safe Alternatives Instead of `get()`**

| Alternative | Usage |
|-------------|---------|
| `orElse(defaultValue)` | Returns the value if present, otherwise returns a default. |
| `orElseGet(Supplier)` | Returns the value if present, otherwise calls a Supplier function. |
| `orElseThrow(Supplier)` | Returns the value if present, otherwise throws a custom exception. |

---

## **E-Commerce Use Cases**

### **1. Fetching Product Name Safely**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Product product = new Product("Smartphone");

        // Safe alternative: Using orElse()
        System.out.println("Product: " + product.getName().orElse("No Product Found"));
    }
}
```
### **Output**
```
Product: Smartphone
```
- **`orElse("No Product Found")`** ensures that a meaningful default is returned.

---

### **2. Checking Stock Availability Without `get()`**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 10);
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product));
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        
        String product = "Smartphone";
        int availableStock = inventory.getStock(product).orElse(0);

        System.out.println("Available Stock: " + availableStock);
    }
}
```
### **Output**
```
Available Stock: 0
```
- Instead of using `get()`, **`orElse(0)`** ensures a default stock count.

---

### **3. Retrieving Customer Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customer = new Customer(null);

        int points = customer.getLoyaltyPoints().orElse(0);

        System.out.println("Loyalty Points: " + points);
    }
}
```
### **Output**
```
Loyalty Points: 0
```
- **No `get()` used**, ensuring no `NoSuchElementException`.

---

### **4. Using `orElseThrow()` Instead of `get()`**
```java
import java.util.Optional;

class Order {
    private String orderId;

    public Order(String orderId) {
        this.orderId = orderId;
    }

    public Optional<String> getOrderId() {
        return Optional.ofNullable(orderId);
    }
}

public class OrderExample {
    public static void main(String[] args) {
        Order order = new Order(null);

        // Using orElseThrow instead of get()
        String orderId = order.getOrderId().orElseThrow(() -> new RuntimeException("Order ID is missing"));

        System.out.println("Order ID: " + orderId);
    }
}
```
### **Output**
```
Exception in thread "main" java.lang.RuntimeException: Order ID is missing
```
- **Ensures that missing order IDs trigger an exception**.

---

## **When Should You Use `get()`?**
✅ **When you are 100% sure** that `Optional` contains a value.  
✅ **When you've already checked `isPresent()`** before calling `get()`.  

❌ **Do NOT use `get()` when the value might be missing**. Use `orElse()`, `orElseGet()`, or `orElseThrow()` instead.

---

## **Comparison Table: `get()` vs Safer Alternatives**
| Method | Behavior | Example |
|--------|----------|---------|
| `get()` | Returns the value if present, else throws `NoSuchElementException` | `opt.get()` |
| `orElse(default)` | Returns the value if present, otherwise returns a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Returns the value if present, otherwise calls a Supplier function | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow(Supplier)` | Returns the value if present, otherwise throws an exception | `opt.orElseThrow(() -> new Exception("Not found"))` |

---

## **Advantages of Avoiding `get()`**
| Feature | Benefit |
|---------|---------|
| **Avoids `NoSuchElementException`** | No risk of unhandled exceptions. |
| **Encourages Defensive Programming** | Allows default values instead of exceptions. |
| **Enhances Readability** | Eliminates unnecessary null checks. |

---

## **Conclusion**
- **`Optional.get()` should be avoided unless you are certain the value exists**.
- **Safer alternatives** (`orElse()`, `orElseGet()`, `orElseThrow()`) **should be preferred**.
- **In e-commerce applications**, these techniques ensure **robust product searches, inventory checks, and order processing**.

Would you like more real-world examples? 🚀

# What is Optional.isPresent()?
### **Introduction**
The **`Optional.isPresent()`** method in Java **(introduced in Java 8)** is used to check whether an `Optional` object contains a **non-null value**. It returns:
- **`true`** if a value is present.
- **`false`** if the `Optional` is empty.

### **Key Points**
- **Avoids NullPointerException** by allowing a **safe way** to check for the presence of a value.
- **Reduces the need for explicit `null` checks**.
- Often used with **functional interfaces** like `ifPresent()` for **cleaner** and **readable** code.

---

## **Syntax**
```java
boolean exists = optionalValue.isPresent();
```
- Returns `true` if `Optional` contains a value, otherwise `false`.

### **Equivalent to**
```java
if (optionalValue != null) {
    return true;
} else {
    return false;
}
```

---

## **Example 1: Using `isPresent()`**
```java
import java.util.Optional;

public class OptionalIsPresentExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");

        if (optionalProduct.isPresent()) {
            System.out.println("Product Available: " + optionalProduct.get());
        } else {
            System.out.println("Product Not Available");
        }
    }
}
```
### **Output**
```
Product Available: Laptop
```
- Since `optionalProduct` contains `"Laptop"`, `isPresent()` returns `true`.

---

## **Example 2: `isPresent()` with Empty Optional**
```java
import java.util.Optional;

public class OptionalIsPresentEmptyExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        if (emptyOptional.isPresent()) {
            System.out.println("Value exists: " + emptyOptional.get());
        } else {
            System.out.println("No value present");
        }
    }
}
```
### **Output**
```
No value present
```
- Since `emptyOptional` is empty, `isPresent()` returns `false`.

---

## **E-Commerce Use Cases**

### **1. Checking If Product Name Exists**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Product product = new Product(null);

        if (product.getName().isPresent()) {
            System.out.println("Product Name: " + product.getName().get());
        } else {
            System.out.println("Product Name Not Available");
        }
    }
}
```
### **Output**
```
Product Name Not Available
```
- Ensures **safe checking** without `NullPointerException`.

---

### **2. Checking Stock Availability**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 5);
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product));
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        String product = "Smartphone";
        if (inventory.getStock(product).isPresent()) {
            System.out.println(product + " is in stock.");
        } else {
            System.out.println(product + " is out of stock.");
        }
    }
}
```
### **Output**
```
Smartphone is out of stock.
```
- **Prevents null checks**, making the code **cleaner**.

---

### **3. Validating Customer Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customer = new Customer(null);

        if (customer.getLoyaltyPoints().isPresent()) {
            System.out.println("Loyalty Points: " + customer.getLoyaltyPoints().get());
        } else {
            System.out.println("No Loyalty Points Available");
        }
    }
}
```
### **Output**
```
No Loyalty Points Available
```
- `isPresent()` **avoids null checks** and ensures **safe execution**.

---

## **Alternative: Using `ifPresent()` for Cleaner Code**
Instead of `isPresent()` + `get()`, use `ifPresent()` for **better readability**.

### **Example: Using `ifPresent()` Instead of `isPresent()`**
```java
import java.util.Optional;

public class IfPresentExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Smartphone");

        // Instead of using isPresent() + get()
        optionalProduct.ifPresent(product -> System.out.println("Product: " + product));
    }
}
```
### **Output**
```
Product: Smartphone
```
- **`ifPresent()`** automatically executes **only when a value exists**.

---

## **Common Methods Used with `Optional.isPresent()`**
| Method | Description | Example |
|--------|-------------|---------|
| `isPresent()` | Checks if a value is present | `opt.isPresent()` → `true` or `false` |
| `ifPresent(Consumer)` | Executes a function if a value exists | `opt.ifPresent(System.out::println);` |
| `orElse(defaultValue)` | Returns the value if present, otherwise returns a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow(Supplier)` | Throws an exception if value is missing | `opt.orElseThrow(() -> new RuntimeException("Not found"))` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures safe checking of values. |
| **Improves Readability** | Avoids `if (value != null)` checks. |
| **Works Well with Functional Programming** | Encourages usage of `ifPresent()`, `map()`, and `filter()`. |

---

## **When to Use `isPresent()`?**
✅ **When you want to check if a value exists before performing an operation**.  
✅ **When working with nullable values in APIs or database results**.  
✅ **When handling missing values in Collections and Streams**.  

❌ **Avoid using `isPresent()` with `get()`**. Instead, use **`ifPresent()`**, **`orElse()`**, or **`orElseThrow()`**.

---

## **Conclusion**
`Optional.isPresent()` provides a **safe way** to check for values, helping to **avoid null checks** and **prevent `NullPointerException`**. It is widely used in **e-commerce applications** for **checking stock, validating user data, and retrieving optional product details**.

# What is Optional.isEmpty()?
### **Introduction**
The **`Optional.isEmpty()`** method in Java **(introduced in Java 11)** is used to check whether an `Optional` object is **empty** (i.e., does not contain a value). It is the **opposite** of `isPresent()`.

### **Key Points**
- Returns **`true`** if `Optional` contains **no value**.
- Returns **`false`** if `Optional` contains a value.
- Helps in writing **more readable code** by avoiding explicit `null` checks.
- **Available from Java 11** onwards.

---

## **Syntax**
```java
boolean isEmpty = optionalValue.isEmpty();
```
- Returns `true` if `Optional` is **empty**, otherwise returns `false`.

### **Equivalent to**
```java
boolean isEmpty = !optionalValue.isPresent();
```

---

## **Difference Between `isPresent()` and `isEmpty()`**
| Method | Returns `true` When | Introduced In |
|--------|--------------------|--------------|
| `isPresent()` | `Optional` contains a value | Java 8 |
| `isEmpty()` | `Optional` is empty (no value) | Java 11 |

---

## **Example 1: Using `isEmpty()` to Check If Optional Is Empty**
```java
import java.util.Optional;

public class OptionalIsEmptyExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        if (emptyOptional.isEmpty()) {
            System.out.println("No value present");
        }
    }
}
```
### **Output**
```
No value present
```
- Since `emptyOptional` contains **no value**, `isEmpty()` returns **`true`**.

---

## **Example 2: `isEmpty()` vs `isPresent()`**
```java
import java.util.Optional;

public class OptionalCheckExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");
        Optional<String> emptyOptional = Optional.empty();

        System.out.println("isPresent() on non-empty Optional: " + optionalProduct.isPresent()); // true
        System.out.println("isEmpty() on non-empty Optional: " + optionalProduct.isEmpty()); // false

        System.out.println("isPresent() on empty Optional: " + emptyOptional.isPresent()); // false
        System.out.println("isEmpty() on empty Optional: " + emptyOptional.isEmpty()); // true
    }
}
```
### **Output**
```
isPresent() on non-empty Optional: true
isEmpty() on non-empty Optional: false
isPresent() on empty Optional: false
isEmpty() on empty Optional: true
```
- `isEmpty()` returns **`true`** when the `Optional` is empty.

---

## **E-Commerce Use Cases**

### **1. Checking If Product Exists in Inventory**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 5);
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product)); // Returns Optional.empty() if product not found
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        String product = "Smartphone";
        if (inventory.getStock(product).isEmpty()) {
            System.out.println(product + " is out of stock.");
        } else {
            System.out.println(product + " is available.");
        }
    }
}
```
### **Output**
```
Smartphone is out of stock.
```
- Uses `isEmpty()` to check if a product is **missing from inventory**.

---

### **2. Checking If Customer Has Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customer = new Customer(null);

        if (customer.getLoyaltyPoints().isEmpty()) {
            System.out.println("Customer has no loyalty points.");
        } else {
            System.out.println("Loyalty Points: " + customer.getLoyaltyPoints().get());
        }
    }
}
```
### **Output**
```
Customer has no loyalty points.
```
- Uses `isEmpty()` to check if **loyalty points are missing**.

---

### **3. Handling Missing Product Description**
```java
import java.util.Optional;

class Product {
    private String description;

    public Product(String description) {
        this.description = description;
    }

    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }
}

public class ProductDescriptionExample {
    public static void main(String[] args) {
        Product product = new Product(null);

        if (product.getDescription().isEmpty()) {
            System.out.println("No product description available.");
        } else {
            System.out.println("Product Description: " + product.getDescription().get());
        }
    }
}
```
### **Output**
```
No product description available.
```
- Uses `isEmpty()` to check if **product description is missing**.

---

## **Alternative: Using `ifPresentOrElse()` for Cleaner Code**
Instead of `isEmpty()`, we can use `ifPresentOrElse()` (Java 9+).

```java
import java.util.Optional;

public class IfPresentOrElseExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.empty();

        optionalProduct.ifPresentOrElse(
                product -> System.out.println("Product: " + product),
                () -> System.out.println("No product available")
        );
    }
}
```
### **Output**
```
No product available
```
- **`ifPresentOrElse()`** is **better** than `isEmpty()` for handling **both cases together**.

---

## **Common Methods Used with `isEmpty()`**
| Method | Description | Example |
|--------|-------------|---------|
| `isPresent()` | Checks if a value is present | `opt.isPresent()` → `true` or `false` |
| `isEmpty()` | Checks if no value is present | `opt.isEmpty()` → `true` or `false` |
| `ifPresent(Consumer)` | Executes a function if a value exists | `opt.ifPresent(System.out::println);` |
| `orElse(defaultValue)` | Returns the value if present, otherwise returns a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures safe handling of missing values. |
| **Improves Readability** | Avoids explicit `null` checks. |
| **Works Well with Functional Programming** | Encourages usage of `ifPresent()`, `map()`, and `filter()`. |

---

## **When to Use `isEmpty()`?**
✅ **When you want to check if a value is missing before performing an operation**.  
✅ **When working with nullable values in APIs or database results**.  
✅ **When handling missing values in Collections and Streams**.  

❌ **Use `ifPresentOrElse()` if handling both cases together**.

---

## **Conclusion**
`Optional.isEmpty()` provides a **cleaner way to check for missing values**, helping to **avoid null checks** and **prevent `NullPointerException`**. It is widely used in **e-commerce applications** for **checking product availability, validating user data, and handling missing product details**.

# What is Optional.filter()?
### **Introduction**
The **`Optional.filter()`** method in Java **(introduced in Java 8)** is used to **apply a condition** (predicate) to the value inside an `Optional`. If the condition is met, the same `Optional` is returned; otherwise, an **empty `Optional`** is returned.

### **Key Points**
- **Filters values inside `Optional`** based on a given condition.
- If the condition **matches**, the same `Optional` is returned.
- If the condition **fails**, it returns **`Optional.empty()`**.
- Prevents **`NullPointerException`** and eliminates explicit `if` checks.

---

## **Syntax**
```java
Optional<T> result = optionalValue.filter(predicate);
```
- **T** → Type of value stored inside `Optional`.
- **predicate** → A function that tests a condition on the stored value.

### **Equivalent to**
```java
if (optionalValue.isPresent() && predicate.test(optionalValue.get())) {
    return optionalValue;
} else {
    return Optional.empty();
}
```

---

## **Example 1: Filtering an Optional Value**
```java
import java.util.Optional;

public class OptionalFilterExample {
    public static void main(String[] args) {
        Optional<Integer> optionalNumber = Optional.of(10);

        Optional<Integer> result = optionalNumber.filter(num -> num > 5);

        System.out.println("Filtered Value: " + result.orElse(-1));
    }
}
```
### **Output**
```
Filtered Value: 10
```
- The condition **`num > 5`** is **true**, so the original `Optional` is returned.

---

## **Example 2: Condition Not Met**
```java
import java.util.Optional;

public class OptionalFilterExample {
    public static void main(String[] args) {
        Optional<Integer> optionalNumber = Optional.of(3);

        Optional<Integer> result = optionalNumber.filter(num -> num > 5);

        System.out.println("Filtered Value: " + result.orElse(-1));
    }
}
```
### **Output**
```
Filtered Value: -1
```
- Since `3` is **not greater than 5**, `filter()` returns **`Optional.empty()`**.

---

## **E-Commerce Use Cases**

### **1. Filtering Product Price Before Applying Discount**
```java
import java.util.Optional;

class Product {
    private String name;
    private double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Optional<Double> getDiscountedPrice() {
        return Optional.of(price)
                .filter(p -> p > 100) // Apply discount only if price > 100
                .map(p -> p * 0.9); // 10% discount
    }
}

public class DiscountExample {
    public static void main(String[] args) {
        Product expensiveProduct = new Product("Laptop", 120);
        Product cheapProduct = new Product("Mouse", 50);

        System.out.println("Discounted Price (Laptop): $" + 
                expensiveProduct.getDiscountedPrice().orElse(expensiveProduct.price));

        System.out.println("Discounted Price (Mouse): $" + 
                cheapProduct.getDiscountedPrice().orElse(cheapProduct.price));
    }
}
```
### **Output**
```
Discounted Price (Laptop): $108.0
Discounted Price (Mouse): $50.0
```
- **Laptop gets a discount** because its price is **above 100**.
- **Mouse price remains the same** since it **fails the filter condition**.

---

### **2. Checking If Product Is Available in Inventory**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 10);
        stock.put("Tablet", 0); // Out of stock
    }

    public Optional<Integer> getStock(String product) {
        return Optional.ofNullable(stock.get(product))
                .filter(qty -> qty > 0); // Only return if stock is > 0
    }
}

public class InventoryCheckExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        System.out.println("Laptop Stock: " + inventory.getStock("Laptop").orElse(0));
        System.out.println("Tablet Stock: " + inventory.getStock("Tablet").orElse(0));
    }
}
```
### **Output**
```
Laptop Stock: 10
Tablet Stock: 0
```
- **Filters out `0` stock products** and returns `Optional.empty()` instead.

---

### **3. Validating Customer's Loyalty Points Eligibility**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getEligibleLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints)
                .filter(points -> points >= 100); // Only consider points >= 100
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer eligibleCustomer = new Customer(150);
        Customer ineligibleCustomer = new Customer(80);

        System.out.println("Eligible Customer Points: " + 
                eligibleCustomer.getEligibleLoyaltyPoints().orElse(0));

        System.out.println("Ineligible Customer Points: " + 
                ineligibleCustomer.getEligibleLoyaltyPoints().orElse(0));
    }
}
```
### **Output**
```
Eligible Customer Points: 150
Ineligible Customer Points: 0
```
- Customers with **less than 100 points** are **not eligible** for a reward.

---

## **Combining `filter()` with `map()`**
```java
import java.util.Optional;

class User {
    private String email;

    public User(String email) {
        this.email = email;
    }

    public Optional<String> getVerifiedEmail() {
        return Optional.ofNullable(email)
                .filter(e -> e.contains("@"))
                .map(String::toLowerCase);
    }
}

public class EmailValidationExample {
    public static void main(String[] args) {
        User validUser = new User("Customer@Store.com");
        User invalidUser = new User("InvalidEmail");

        System.out.println("Valid User Email: " + validUser.getVerifiedEmail().orElse("Invalid Email"));
        System.out.println("Invalid User Email: " + invalidUser.getVerifiedEmail().orElse("Invalid Email"));
    }
}
```
### **Output**
```
Valid User Email: customer@store.com
Invalid User Email: Invalid Email
```
- **`filter()`** ensures the email **contains `@`** before **lowercasing**.

---

## **Common Methods Used with `Optional.filter()`**
| Method | Description | Example |
|--------|-------------|---------|
| `filter(Predicate)` | Filters value based on condition | `opt.filter(val -> val > 10)` |
| `map(Function)` | Transforms the value if present | `opt.map(String::toUpperCase)` |
| `flatMap(Function)` | Flattens nested `Optional` | `opt.flatMap(Product::getDiscountedPrice)` |
| `orElse(defaultValue)` | Returns value if present, otherwise default | `opt.orElse("Default")` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Filters values safely without explicit null checks. |
| **Improves Readability** | Avoids `if-else` conditions for filtering logic. |
| **Encourages Functional Programming** | Works well with `map()` and `flatMap()`. |

---

## **When to Use `Optional.filter()`?**
✅ **When applying a condition on an optional value before using it.**  
✅ **When avoiding unnecessary `if` statements.**  
✅ **When working with nullable values from APIs, databases, or user input.**  

❌ **Avoid excessive chaining**, as it can reduce code readability.  

---

## **Conclusion**
`Optional.filter()` is a **powerful method** for safely **filtering values inside an `Optional`**. It is widely used in **e-commerce applications** for handling **discounts, inventory stock checks, customer rewards, and user validation**.

# What is Optional.map()?
## **Introduction**
The **`Optional.map()`** method in Java **(introduced in Java 8)** is used to **transform** the value inside an `Optional`, if it is present. If the `Optional` is empty, `map()` simply returns an **empty `Optional`**, ensuring **null safety**.

---

## **Key Points**
- **Transforms the value inside `Optional`** using a mapping function.
- If a value is **present**, `map()` applies the function and returns a **new `Optional`** containing the transformed value.
- If the `Optional` is **empty**, `map()` returns **`Optional.empty()`** without calling the function.
- Helps in **avoiding NullPointerException (NPE)** while transforming values.
- Commonly used in **functional programming** for **method chaining**.

---

## **Syntax**
```java
Optional<R> transformedValue = optionalValue.map(function);
```
- **`optionalValue`** → The `Optional` containing the original value.
- **`function`** → A function that transforms the original value into another value.
- **`transformedValue`** → A new `Optional` containing the transformed result.

### **Equivalent to:**
```java
if (optionalValue.isPresent()) {
    return Optional.of(function.apply(optionalValue.get()));
} else {
    return Optional.empty();
}
```

---

## **Example 1: Converting a Product Name to Uppercase**
```java
import java.util.Optional;

public class OptionalMapExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("laptop");

        Optional<String> upperCaseProduct = optionalProduct.map(String::toUpperCase);

        System.out.println("Product Name: " + upperCaseProduct.orElse("No Product Available"));
    }
}
```
### **Output**
```
Product Name: LAPTOP
```
- The `map(String::toUpperCase)` transforms `"laptop"` to `"LAPTOP"`, keeping it inside `Optional`.

---

## **Example 2: Handling an Empty Optional with `map()`**
```java
import java.util.Optional;

public class OptionalMapEmptyExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        Optional<String> transformed = emptyOptional.map(String::toUpperCase);

        System.out.println("Transformed Value: " + transformed.orElse("No Value Found"));
    }
}
```
### **Output**
```
Transformed Value: No Value Found
```
- Since the `Optional` is **empty**, `map()` **does not call `toUpperCase()`** and simply returns `Optional.empty()`.

---

## **E-Commerce Use Cases**

### **1. Getting Product Name in Uppercase**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Product product = new Product("smartphone");

        Optional<String> uppercaseName = product.getName().map(String::toUpperCase);

        System.out.println("Product Name: " + uppercaseName.orElse("Product Not Found"));
    }
}
```
### **Output**
```
Product Name: SMARTPHONE
```
- Ensures **null safety** when retrieving product names.

---

### **2. Fetching Discounted Price for Products**
```java
import java.util.Optional;

class Product {
    private String name;
    private Double price;

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Optional<Double> getDiscountedPrice() {
        return Optional.ofNullable(price).map(p -> p * 0.9); // 10% discount
    }
}

public class DiscountExample {
    public static void main(String[] args) {
        Product product = new Product("Laptop", 1200.0);

        Optional<Double> discountedPrice = product.getDiscountedPrice();

        System.out.println("Discounted Price: $" + discountedPrice.orElse(0.0));
    }
}
```
### **Output**
```
Discounted Price: $1080.0
```
- Ensures **null safety** before applying discounts.

---

### **3. Getting Product Stock Level**
```java
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Inventory {
    private Map<String, Integer> stock = new HashMap<>();

    public Inventory() {
        stock.put("Laptop", 10);
        stock.put("Tablet", null); // Unavailable stock
    }

    public Optional<String> getStockMessage(String product) {
        return Optional.ofNullable(stock.get(product))
                .map(qty -> "Stock Available: " + qty)
                .or(() -> Optional.of("Out of Stock"));
    }
}

public class InventoryExample {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        System.out.println(inventory.getStockMessage("Laptop").get());
        System.out.println(inventory.getStockMessage("Tablet").get());
    }
}
```
### **Output**
```
Stock Available: 10
Out of Stock
```
- Ensures **null safety** when checking inventory levels.

---

### **4. Extracting Customer Email Domain**
```java
import java.util.Optional;

class Customer {
    private String email;

    public Customer(String email) {
        this.email = email;
    }

    public Optional<String> getEmailDomain() {
        return Optional.ofNullable(email)
                .map(e -> e.substring(e.indexOf("@") + 1));
    }
}

public class EmailExample {
    public static void main(String[] args) {
        Customer customer = new Customer("customer@store.com");

        Optional<String> domain = customer.getEmailDomain();

        System.out.println("Email Domain: " + domain.orElse("No Email Provided"));
    }
}
```
### **Output**
```
Email Domain: store.com
```
- Extracts the email domain **safely**, avoiding `NullPointerException`.

---

## **Difference Between `map()` and `flatMap()`**
| Feature | `map()` | `flatMap()` |
|---------|--------|------------|
| Returns | `Optional<Optional<R>>` (nested) | `Optional<R>` (flattened) |
| Used When | Function returns a **normal value** | Function returns **Optional** |
| Example | `opt.map(String::length)` | `opt.flatMap(Product::getDiscountedPrice)` |

### **Example of `flatMap()`**
```java
product.getOptionalDiscount().flatMap(Product::getDiscountedPrice);
```
- `flatMap()` **unwraps nested `Optional` values**.

---

## **Common Methods Used with `Optional.map()`**
| Method | Description | Example |
|--------|-------------|---------|
| `map(Function)` | Transforms value if present | `opt.map(String::toUpperCase)` |
| `flatMap(Function)` | Flattens nested `Optional` | `opt.flatMap(Product::getDiscountedPrice)` |
| `orElse(defaultValue)` | Returns value if present, otherwise default | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures transformations happen only if a value exists. |
| **Improves Readability** | Avoids explicit null checks while transforming values. |
| **Encourages Functional Programming** | Works well with method chaining (`map()`, `filter()`, `flatMap()`). |

---

## **When to Use `Optional.map()`?**
✅ **When applying transformations to an optional value**.  
✅ **When extracting fields from an object inside `Optional`**.  
✅ **When avoiding unnecessary `if` conditions**.  

❌ **Avoid using `map()` if the function returns `Optional`. Use `flatMap()` instead.**  

---

## **Conclusion**
`Optional.map()` is a **powerful method** for **safely transforming values inside `Optional`**, avoiding **null checks** and **NullPointerException**. It is widely used in **e-commerce applications** for handling **product details, inventory stock, discount calculations, and user information**.

## **Advanced Complex Use Cases of `Optional.map()` in Java**
---
### **1. Chaining `map()`, `flatMap()`, and `filter()` for Order Processing**
#### **Scenario:**  
In an e-commerce system, orders contain products, and each product may or may not have a **discounted price**. We need to fetch the **discounted price of a product inside an order safely**.

#### **Solution:**
We will use `map()`, `flatMap()`, and `filter()` to handle this scenario elegantly.

```java
import java.util.Optional;

class Discount {
    private Double discountedPrice;

    public Discount(Double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Optional<Double> getDiscountedPrice() {
        return Optional.ofNullable(discountedPrice);
    }
}

class Product {
    private String name;
    private Discount discount;

    public Product(String name, Discount discount) {
        this.name = name;
        this.discount = discount;
    }

    public Optional<Discount> getDiscount() {
        return Optional.ofNullable(discount);
    }
}

class Order {
    private Product product;

    public Order(Product product) {
        this.product = product;
    }

    public Optional<Product> getProduct() {
        return Optional.ofNullable(product);
    }
}

public class OrderProcessing {
    public static void main(String[] args) {
        Order orderWithDiscount = new Order(new Product("Laptop", new Discount(900.0)));
        Order orderWithoutDiscount = new Order(new Product("Smartphone", null));

        // Fetch discounted price safely using map(), flatMap()
        Optional<Double> discountedPrice = orderWithDiscount.getProduct()
                .flatMap(Product::getDiscount) // Extract Discount object safely
                .flatMap(Discount::getDiscountedPrice) // Extract price safely
                .filter(price -> price < 1000); // Only return discount if price < 1000

        System.out.println("Discounted Price: " + discountedPrice.orElse(0.0));

        // Fetch discounted price when no discount is available
        Optional<Double> noDiscountPrice = orderWithoutDiscount.getProduct()
                .flatMap(Product::getDiscount)
                .flatMap(Discount::getDiscountedPrice)
                .filter(price -> price < 1000);

        System.out.println("No Discount Available: " + noDiscountPrice.orElse(0.0));
    }
}
```

### **Output**
```
Discounted Price: 900.0
No Discount Available: 0.0
```
#### **Why this is advanced?**
- **Nested Optionals:** Handling multiple levels of `Optional` (`Product` → `Discount` → `DiscountedPrice`).
- **Null Safety:** Even if **discount is null**, no `NullPointerException` occurs.
- **Method Chaining:** Uses `flatMap()` for nested `Optional` and `filter()` to apply conditions.

---

### **2. Using `map()` to Calculate Final Price Including Tax & Discount**
#### **Scenario:**  
A customer purchases a product. The **product may have a discount** and the **customer's country determines the tax rate**.  
We must calculate the **final price safely**.

#### **Solution:**
Use `Optional.map()` to apply the discount and tax safely.

```java
import java.util.Optional;

class TaxService {
    public static Optional<Double> getTaxRate(String country) {
        return Optional.ofNullable(country)
                .map(c -> switch (c.toLowerCase()) {
                    case "usa" -> 0.07; // 7% tax
                    case "uk" -> 0.2; // 20% tax
                    case "india" -> 0.18; // 18% tax
                    default -> null;
                });
    }
}

class Product {
    private String name;
    private Double basePrice;
    private Optional<Double> discountPrice;

    public Product(String name, Double basePrice, Double discountPrice) {
        this.name = name;
        this.basePrice = basePrice;
        this.discountPrice = Optional.ofNullable(discountPrice);
    }

    public Optional<Double> getFinalPrice(String country) {
        return discountPrice
                .or(() -> Optional.of(basePrice)) // Use discounted price if available
                .flatMap(price -> TaxService.getTaxRate(country).map(tax -> price + (price * tax))); // Apply tax
    }
}

public class TaxCalculationExample {
    public static void main(String[] args) {
        Product productWithDiscount = new Product("Laptop", 1200.0, 1000.0);
        Product productWithoutDiscount = new Product("Tablet", 600.0, null);

        System.out.println("Final Price in USA: $" + productWithDiscount.getFinalPrice("USA").orElse(0.0));
        System.out.println("Final Price in UK: $" + productWithoutDiscount.getFinalPrice("UK").orElse(0.0));
    }
}
```

### **Output**
```
Final Price in USA: $1070.0
Final Price in UK: $720.0
```
#### **Why this is advanced?**
- **Conditional Discount Handling:** Uses `or()` to use either **discounted price** or **base price**.
- **Applying Tax Safely:** Uses `map()` to apply tax only when a valid rate is found.
- **Avoids `null` everywhere!**

---

### **3. Extracting Product Reviews and Calculating Average Rating**
#### **Scenario:**  
A product has **multiple reviews**. Each review may or may not have a **rating**.  
We need to calculate the **average rating safely**.

#### **Solution:**
Use `map()` and `flatMap()` to extract ratings and calculate the average.

```java
import java.util.List;
import java.util.Optional;

class Review {
    private Optional<Integer> rating;

    public Review(Integer rating) {
        this.rating = Optional.ofNullable(rating);
    }

    public Optional<Integer> getRating() {
        return rating;
    }
}

class Product {
    private String name;
    private List<Review> reviews;

    public Product(String name, List<Review> reviews) {
        this.name = name;
        this.reviews = reviews;
    }

    public Optional<Double> getAverageRating() {
        return reviews.stream()
                .map(Review::getRating) // Extract Optional<Integer>
                .flatMap(Optional::stream) // Remove empty Optionals
                .mapToInt(Integer::intValue)
                .average();
    }
}

public class AverageRatingExample {
    public static void main(String[] args) {
        Product productWithReviews = new Product("Smartphone", List.of(
                new Review(5),
                new Review(null), // No rating given
                new Review(4)
        ));

        System.out.println("Average Rating: " + productWithReviews.getAverageRating().orElse(0.0));
    }
}
```

### **Output**
```
Average Rating: 4.5
```
#### **Why this is advanced?**
- **Filtering Null Ratings:** `Optional::stream` removes empty `Optional`s.
- **Functional Approach:** Uses `mapToInt().average()` for concise logic.

---

### **4. Handling Optional Data from External APIs**
#### **Scenario:**  
An API returns user data, but **email and phone number may be missing**.  
We must retrieve **either email or phone number**.

```java
import java.util.Optional;

class User {
    private Optional<String> email;
    private Optional<String> phoneNumber;

    public User(String email, String phoneNumber) {
        this.email = Optional.ofNullable(email);
        this.phoneNumber = Optional.ofNullable(phoneNumber);
    }

    public Optional<String> getContactInfo() {
        return email.or(() -> phoneNumber); // Use email if available, otherwise use phone
    }
}

public class UserContactExample {
    public static void main(String[] args) {
        User userWithEmail = new User("user@example.com", null);
        User userWithPhone = new User(null, "123-456-7890");

        System.out.println("User Contact: " + userWithEmail.getContactInfo().orElse("No contact info available"));
        System.out.println("User Contact: " + userWithPhone.getContactInfo().orElse("No contact info available"));
    }
}
```

### **Output**
```
User Contact: user@example.com
User Contact: 123-456-7890
```
#### **Why this is advanced?**
- **`or()` for Fallback Logic:** Ensures we get at least one contact method.

---

## **Conclusion**
- `Optional.map()` is **powerful** when used in **real-world scenarios**.
- **Combining `map()`, `flatMap()`, `or()`, and `filter()`** creates **robust** and **null-safe** applications.
- Avoids **`null` checks** and makes code **more readable and maintainable**.

Would you like more **real-world use cases**? 🚀

# What is Optional.flatMap()?

## **Introduction**
The **`Optional.flatMap()`** method in Java **(introduced in Java 8)** is used to **flatten** nested `Optional` values. It works similarly to `map()`, but the key difference is:
- **`map()`** transforms the value and wraps it inside an `Optional<Optional<T>>` (nested `Optional`).
- **`flatMap()`** **removes the extra `Optional` layer** and returns `Optional<T>` directly.

---

## **Key Points**
- **Used to avoid `Optional<Optional<T>>` (nested Optionals).**
- **Unwraps the inner `Optional`** if present.
- **Returns `Optional.empty()` if the outer `Optional` is empty.**
- Helps in **chaining multiple `Optional`-returning methods**.

---

## **Syntax**
```java
Optional<R> transformedValue = optionalValue.flatMap(function);
```
- **`optionalValue`** → The outer `Optional`.
- **`function`** → A function that transforms and returns an `Optional<T>`.
- **`transformedValue`** → The flattened `Optional<T>`.

### **Equivalent to**
```java
if (optionalValue.isPresent()) {
    return optionalValue.get().map(function);
} else {
    return Optional.empty();
}
```

---

## **Difference Between `map()` and `flatMap()`**
| Feature | `map()` | `flatMap()` |
|---------|--------|------------|
| Returns | `Optional<Optional<R>>` (nested) | `Optional<R>` (flattened) |
| Used When | Function returns a **normal value** | Function returns **Optional** |
| Example | `opt.map(String::length)` | `opt.flatMap(Product::getDiscountedPrice)` |

---

## **Example 1: Using `flatMap()` to Avoid Nested Optional**
### **Scenario:**  
A **Product** may or may not have a **Discount**. Each discount has a **discounted price**.  
We need to **safely retrieve the discounted price**.

```java
import java.util.Optional;

class Discount {
    private Double discountedPrice;

    public Discount(Double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public Optional<Double> getDiscountedPrice() {
        return Optional.ofNullable(discountedPrice);
    }
}

class Product {
    private String name;
    private Optional<Discount> discount;

    public Product(String name, Discount discount) {
        this.name = name;
        this.discount = Optional.ofNullable(discount);
    }

    public Optional<Discount> getDiscount() {
        return discount;
    }
}

public class FlatMapExample {
    public static void main(String[] args) {
        Product productWithDiscount = new Product("Laptop", new Discount(900.0));
        Product productWithoutDiscount = new Product("Mouse", null);

        // Using flatMap() to safely extract discounted price
        Optional<Double> discountedPrice = productWithDiscount.getDiscount()
                .flatMap(Discount::getDiscountedPrice);

        System.out.println("Discounted Price: " + discountedPrice.orElse(0.0));

        // Product with no discount
        Optional<Double> noDiscountPrice = productWithoutDiscount.getDiscount()
                .flatMap(Discount::getDiscountedPrice);

        System.out.println("No Discount Available: " + noDiscountPrice.orElse(0.0));
    }
}
```

### **Output**
```
Discounted Price: 900.0
No Discount Available: 0.0
```
#### **Why is this an advanced example?**
- **Avoids nested `Optional<Optional<T>>`.**
- **Ensures safe retrieval** of discounted prices even if `Discount` is `null`.

---

## **E-Commerce Use Cases**

### **1. Retrieving Customer Address Safely**
```java
import java.util.Optional;

class Address {
    private String city;

    public Address(String city) {
        this.city = city;
    }

    public Optional<String> getCity() {
        return Optional.ofNullable(city);
    }
}

class Customer {
    private String name;
    private Optional<Address> address;

    public Customer(String name, Address address) {
        this.name = name;
        this.address = Optional.ofNullable(address);
    }

    public Optional<Address> getAddress() {
        return address;
    }
}

public class CustomerAddressExample {
    public static void main(String[] args) {
        Customer customerWithAddress = new Customer("Alice", new Address("New York"));
        Customer customerWithoutAddress = new Customer("Bob", null);

        Optional<String> city = customerWithAddress.getAddress()
                .flatMap(Address::getCity);

        System.out.println("City: " + city.orElse("No Address Provided"));

        Optional<String> noCity = customerWithoutAddress.getAddress()
                .flatMap(Address::getCity);

        System.out.println("City: " + noCity.orElse("No Address Provided"));
    }
}
```

### **Output**
```
City: New York
City: No Address Provided
```
- **Ensures safe retrieval of city** from an **optional address**.

---

### **2. Fetching Order Tracking Details**
```java
import java.util.Optional;

class Tracking {
    private String trackingNumber;

    public Tracking(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public Optional<String> getTrackingNumber() {
        return Optional.ofNullable(trackingNumber);
    }
}

class Order {
    private Optional<Tracking> tracking;

    public Order(Tracking tracking) {
        this.tracking = Optional.ofNullable(tracking);
    }

    public Optional<Tracking> getTracking() {
        return tracking;
    }
}

public class OrderTrackingExample {
    public static void main(String[] args) {
        Order shippedOrder = new Order(new Tracking("123ABC"));
        Order unshippedOrder = new Order(null);

        Optional<String> trackingNumber = shippedOrder.getTracking()
                .flatMap(Tracking::getTrackingNumber);

        System.out.println("Tracking Number: " + trackingNumber.orElse("Tracking not available"));

        Optional<String> noTracking = unshippedOrder.getTracking()
                .flatMap(Tracking::getTrackingNumber);

        System.out.println("Tracking Number: " + noTracking.orElse("Tracking not available"));
    }
}
```

### **Output**
```
Tracking Number: 123ABC
Tracking Number: Tracking not available
```
- **Ensures safe access to tracking details** even if an order **has no tracking information**.

---

### **3. Handling Optional Data from External APIs**
#### **Scenario:**  
An API returns user data, but **email and phone number may be missing**.  
We must retrieve **either email or phone number**.

```java
import java.util.Optional;

class User {
    private Optional<String> email;
    private Optional<String> phoneNumber;

    public User(String email, String phoneNumber) {
        this.email = Optional.ofNullable(email);
        this.phoneNumber = Optional.ofNullable(phoneNumber);
    }

    public Optional<String> getPreferredContact() {
        return email.flatMap(e -> Optional.of(e)).or(() -> phoneNumber); // Use email first, then phone
    }
}

public class UserContactExample {
    public static void main(String[] args) {
        User userWithEmail = new User("user@example.com", null);
        User userWithPhone = new User(null, "123-456-7890");

        System.out.println("User Contact: " + userWithEmail.getPreferredContact().orElse("No contact info available"));
        System.out.println("User Contact: " + userWithPhone.getPreferredContact().orElse("No contact info available"));
    }
}
```

### **Output**
```
User Contact: user@example.com
User Contact: 123-456-7890
```
- **Ensures the preferred contact is chosen safely**.

---

## **Common Methods Used with `Optional.flatMap()`**
| Method | Description | Example |
|--------|-------------|---------|
| `flatMap(Function)` | Flattens nested `Optional` | `opt.flatMap(Product::getDiscountedPrice)` |
| `orElse(defaultValue)` | Returns value if present, otherwise default | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |

---

## **When to Use `flatMap()`?**
✅ **When working with multiple `Optional` values that need to be unwrapped.**  
✅ **When avoiding nested `Optional<Optional<T>>`.**  
✅ **When handling nullable objects inside another nullable object.**  

❌ **Avoid using `flatMap()` if the function returns a normal value (Use `map()`).**  

## **Conclusion**
- `Optional.flatMap()` **avoids nested Optionals** and ensures **cleaner, null-safe code**.
- It is **widely used** in **e-commerce applications** for handling **discounts, order tracking, customer addresses, and API responses**.

Would you like **more real-world use cases**? 🚀

# What is Optional.ifPresent()?
## **Introduction**
The **`Optional.ifPresent()`** method in Java **(introduced in Java 8)** is used to execute a block of code **only if a value is present** inside an `Optional` object. If the `Optional` is **empty**, it does nothing.

---

## **Key Points**
- **Eliminates explicit `isPresent()` checks**.
- **Executes a lambda function only when a value exists**.
- **Prevents NullPointerException (NPE)** without explicit null checks.
- Helps in **functional programming** by **reducing boilerplate code**.

---

## **Syntax**
```java
optionalValue.ifPresent(consumer);
```
- **`optionalValue`** → The `Optional<T>` object.
- **`consumer`** → A function (lambda) that processes the value.

### **Equivalent to:**
```java
if (optionalValue.isPresent()) {
    consumer.accept(optionalValue.get());
}
```

---

## **Example 1: Using `ifPresent()` Instead of `isPresent()`**
```java
import java.util.Optional;

public class OptionalIfPresentExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");

        // Traditional approach (not recommended)
        if (optionalProduct.isPresent()) {
            System.out.println("Product: " + optionalProduct.get());
        }

        // Using ifPresent() (recommended)
        optionalProduct.ifPresent(product -> System.out.println("Product: " + product));
    }
}
```
### **Output**
```
Product: Laptop
Product: Laptop
```
- **Avoids `isPresent()` checks** and makes the code **cleaner**.

---

## **Example 2: Handling Empty Optional Using `ifPresent()`**
```java
import java.util.Optional;

public class OptionalEmptyIfPresentExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        emptyOptional.ifPresent(product -> System.out.println("Product: " + product)); // Does nothing
    }
}
```
### **Output**
```
(No output, since Optional is empty)
```
- **Prevents NullPointerException (NPE)** safely.

---

## **E-Commerce Use Cases**

### **1. Logging Product Details If Available**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductLoggingExample {
    public static void main(String[] args) {
        Product product = new Product("Smartphone");

        product.getName().ifPresent(name -> System.out.println("Product Name: " + name));
    }
}
```
### **Output**
```
Product Name: Smartphone
```
- Logs **only if product name exists**.

---

### **2. Sending Email Notification If Customer Has an Email**
```java
import java.util.Optional;

class Customer {
    private String email;

    public Customer(String email) {
        this.email = email;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }
}

public class EmailNotificationExample {
    public static void main(String[] args) {
        Customer customerWithEmail = new Customer("customer@example.com");
        Customer customerWithoutEmail = new Customer(null);

        customerWithEmail.getEmail().ifPresent(email -> System.out.println("Sending email to: " + email));
        customerWithoutEmail.getEmail().ifPresent(email -> System.out.println("Sending email to: " + email));
    }
}
```
### **Output**
```
Sending email to: customer@example.com
```
- **Avoids checking `if (email != null)` explicitly**.

---

### **3. Applying Discounts If Available**
```java
import java.util.Optional;

class Product {
    private String name;
    private Optional<Double> discount;

    public Product(String name, Double discount) {
        this.name = name;
        this.discount = Optional.ofNullable(discount);
    }

    public Optional<Double> getDiscount() {
        return discount;
    }
}

public class DiscountApplicationExample {
    public static void main(String[] args) {
        Product productWithDiscount = new Product("Laptop", 10.0);
        Product productWithoutDiscount = new Product("Mouse", null);

        productWithDiscount.getDiscount().ifPresent(disc -> 
            System.out.println("Applying " + disc + "% discount")
        );

        productWithoutDiscount.getDiscount().ifPresent(disc -> 
            System.out.println("Applying " + disc + "% discount")
        );
    }
}
```
### **Output**
```
Applying 10.0% discount
```
- **Only applies discount if it's present**.

---

### **4. Logging Order Tracking Number If Available**
```java
import java.util.Optional;

class Order {
    private Optional<String> trackingNumber;

    public Order(String trackingNumber) {
        this.trackingNumber = Optional.ofNullable(trackingNumber);
    }

    public Optional<String> getTrackingNumber() {
        return trackingNumber;
    }
}

public class OrderTrackingExample {
    public static void main(String[] args) {
        Order shippedOrder = new Order("123ABC");
        Order unshippedOrder = new Order(null);

        shippedOrder.getTrackingNumber().ifPresent(tracking -> 
            System.out.println("Tracking Number: " + tracking)
        );

        unshippedOrder.getTrackingNumber().ifPresent(tracking -> 
            System.out.println("Tracking Number: " + tracking)
        );
    }
}
```
### **Output**
```
Tracking Number: 123ABC
```
- **Prevents errors if tracking is missing**.

---

## **Combining `ifPresent()` with `orElse()`**
### **Scenario:**  
If the **product name is available, print it**. Otherwise, print `"Unknown Product"`.

```java
import java.util.Optional;

public class IfPresentOrElseExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.empty();

        optionalProduct.ifPresentOrElse(
            product -> System.out.println("Product: " + product),
            () -> System.out.println("Unknown Product")
        );
    }
}
```
### **Output**
```
Unknown Product
```
- **`ifPresentOrElse()`** (Java 9+) combines both cases **cleanly**.

---

## **Common Methods Used with `Optional.ifPresent()`**
| Method | Description | Example |
|--------|-------------|---------|
| `isPresent()` | Checks if a value is present | `opt.isPresent()` → `true` or `false` |
| `ifPresent(Consumer)` | Executes a function if a value exists | `opt.ifPresent(System.out::println);` |
| `ifPresentOrElse(Consumer, Runnable)` | Executes a function if value exists, otherwise executes a fallback | `opt.ifPresentOrElse(System.out::println, () -> System.out.println("No value"));` |
| `orElse(defaultValue)` | Returns value if present, otherwise a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures code runs **only if value exists**. |
| **Improves Readability** | Eliminates `if (value != null)` checks. |
| **Works Well with Functional Programming** | Encourages **lambda functions** for better code. |

---

## **When to Use `ifPresent()`?**
✅ **When executing an action only if a value is available.**  
✅ **When avoiding explicit `if` conditions.**  
✅ **When using functional programming (method chaining, streams, etc.).**  

❌ **Do NOT use `ifPresent()` when a default value is needed. Use `orElse()` instead.**  

---

## **Conclusion**
- `Optional.ifPresent()` is **a clean, functional alternative** to explicit `null` checks.
- It is **widely used** in **e-commerce applications** for handling **product details, order tracking, email notifications, and discount applications**.

Would you like **more real-world use cases**? 🚀

# What is Optional.ifPresentOrElse()?
### **Introduction**
The **`Optional.ifPresentOrElse()`** method in Java **(introduced in Java 9)** is used to perform an action **if a value is present** and execute a **different action if the value is absent**. It combines the functionality of **`ifPresent()`** and **`orElse()`** into a single, streamlined method.

---

## **Key Points**
- **Executes one action if a value is present** and another if it is absent.
- Eliminates the need for **explicit `isPresent()` checks**.
- Encourages **functional programming** by using **lambda expressions**.
- Provides a **clearer alternative** to complex `if-else` statements.

---

## **Syntax**
```java
optionalValue.ifPresentOrElse(consumerAction, emptyAction);
```
- **`optionalValue`** → The `Optional<T>` object.
- **`consumerAction`** → Action to perform if value is present.
- **`emptyAction`** → Action to perform if value is absent.

### **Equivalent to:**
```java
if (optionalValue.isPresent()) {
    consumerAction.accept(optionalValue.get());
} else {
    emptyAction.run();
}
```

---

## **Example 1: Using `ifPresentOrElse()` with Product Names**
```java
import java.util.Optional;

public class OptionalIfPresentOrElseExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");

        optionalProduct.ifPresentOrElse(
            product -> System.out.println("Product: " + product),
            () -> System.out.println("No Product Available")
        );
    }
}
```
### **Output**
```
Product: Laptop
```
- If the `Optional` contains a value, it prints the product name; otherwise, it prints a fallback message.

---

## **Example 2: Handling Empty Optional Using `ifPresentOrElse()`**
```java
import java.util.Optional;

public class OptionalEmptyIfPresentOrElseExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        emptyOptional.ifPresentOrElse(
            product -> System.out.println("Product: " + product),
            () -> System.out.println("No Product Available")
        );
    }
}
```
### **Output**
```
No Product Available
```
- Provides a fallback action when the `Optional` is empty.

---

## **E-Commerce Use Cases**

### **1. Displaying Product Details or Default Message**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductDetailsExample {
    public static void main(String[] args) {
        Product availableProduct = new Product("Smartphone");
        Product unavailableProduct = new Product(null);

        availableProduct.getName().ifPresentOrElse(
            name -> System.out.println("Product Name: " + name),
            () -> System.out.println("Product Name Not Available")
        );

        unavailableProduct.getName().ifPresentOrElse(
            name -> System.out.println("Product Name: " + name),
            () -> System.out.println("Product Name Not Available")
        );
    }
}
```
### **Output**
```
Product Name: Smartphone
Product Name Not Available
```
- Handles **both present and absent product names** seamlessly.

---

### **2. Sending Notification Based on Customer Email Presence**
```java
import java.util.Optional;

class Customer {
    private String email;

    public Customer(String email) {
        this.email = email;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }
}

public class EmailNotificationExample {
    public static void main(String[] args) {
        Customer customerWithEmail = new Customer("customer@example.com");
        Customer customerWithoutEmail = new Customer(null);

        customerWithEmail.getEmail().ifPresentOrElse(
            email -> System.out.println("Sending email to: " + email),
            () -> System.out.println("No email provided, sending SMS notification")
        );

        customerWithoutEmail.getEmail().ifPresentOrElse(
            email -> System.out.println("Sending email to: " + email),
            () -> System.out.println("No email provided, sending SMS notification")
        );
    }
}
```
### **Output**
```
Sending email to: customer@example.com
No email provided, sending SMS notification
```
- **Adapts the notification method based on email availability**.

---

### **3. Processing Discounted Products**
```java
import java.util.Optional;

class Product {
    private Optional<Double> discount;

    public Product(Double discount) {
        this.discount = Optional.ofNullable(discount);
    }

    public Optional<Double> getDiscount() {
        return discount;
    }
}

public class DiscountProcessingExample {
    public static void main(String[] args) {
        Product discountedProduct = new Product(10.0);
        Product nonDiscountedProduct = new Product(null);

        discountedProduct.getDiscount().ifPresentOrElse(
            discount -> System.out.println("Applying " + discount + "% discount"),
            () -> System.out.println("No discount available")
        );

        nonDiscountedProduct.getDiscount().ifPresentOrElse(
            discount -> System.out.println("Applying " + discount + "% discount"),
            () -> System.out.println("No discount available")
        );
    }
}
```
### **Output**
```
Applying 10.0% discount
No discount available
```
- Ensures **discounts are applied only if present**.

---

### **4. Tracking Order Status**
```java
import java.util.Optional;

class Order {
    private Optional<String> trackingNumber;

    public Order(String trackingNumber) {
        this.trackingNumber = Optional.ofNullable(trackingNumber);
    }

    public Optional<String> getTrackingNumber() {
        return trackingNumber;
    }
}

public class OrderStatusExample {
    public static void main(String[] args) {
        Order shippedOrder = new Order("123ABC");
        Order unshippedOrder = new Order(null);

        shippedOrder.getTrackingNumber().ifPresentOrElse(
            tracking -> System.out.println("Tracking Number: " + tracking),
            () -> System.out.println("Order not yet shipped")
        );

        unshippedOrder.getTrackingNumber().ifPresentOrElse(
            tracking -> System.out.println("Tracking Number: " + tracking),
            () -> System.out.println("Order not yet shipped")
        );
    }
}
```
### **Output**
```
Tracking Number: 123ABC
Order not yet shipped
```
- **Provides real-time updates** based on tracking availability.

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Eliminates `if-else` Complexity** | Simplifies code by combining presence and absence actions. |
| **Encourages Functional Programming** | Uses lambda expressions for concise actions. |
| **Prevents NullPointerException** | Ensures safe execution regardless of value presence. |

---

## **When to Use `ifPresentOrElse()`?**
✅ **When needing different actions for present and absent values.**  
✅ **When simplifying `if-else` statements.**  
✅ **When using functional programming with lambda expressions.**  

❌ **Avoid for simple presence checks. Use `ifPresent()` instead.**  
## **Conclusion**
`Optional.ifPresentOrElse()` provides a **streamlined approach** to handle **both present and absent values** efficiently. It is widely used in **e-commerce applications** for **product details, customer notifications, discount processing, and order tracking**.

Would you like more **real-world use cases** or explore other `Optional` methods? 🚀

# What is Optional.orElse()?

## **Introduction**
The **`Optional.orElse()`** method in Java **(introduced in Java 8)** is used to **return the value inside an `Optional` if present**; otherwise, it returns a **default value**. This helps **avoid `null` values** and ensures **fallback mechanisms** in case the value is missing.

---

## **Key Points**
- **Returns the stored value if present**.
- **Returns a default value if the `Optional` is empty**.
- **Prevents `NullPointerException` (NPE)** by ensuring a fallback value.
- **Useful for handling missing data in APIs, databases, and e-commerce applications**.

---

## **Syntax**
```java
T result = optionalValue.orElse(defaultValue);
```
- **`optionalValue`** → The `Optional<T>` object.
- **`defaultValue`** → The value returned if `Optional` is empty.

### **Equivalent to:**
```java
if (optionalValue.isPresent()) {
    return optionalValue.get();
} else {
    return defaultValue;
}
```

---

## **Example 1: Using `orElse()` to Provide a Default Value**
```java
import java.util.Optional;

public class OptionalOrElseExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");

        String product = optionalProduct.orElse("Default Product");

        System.out.println("Product: " + product);
    }
}
```
### **Output**
```
Product: Laptop
```
- Since `Optional` contains `"Laptop"`, `orElse()` returns it.

---

## **Example 2: Handling Empty Optional Using `orElse()`**
```java
import java.util.Optional;

public class OptionalEmptyOrElseExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        String product = emptyOptional.orElse("Default Product");

        System.out.println("Product: " + product);
    }
}
```
### **Output**
```
Product: Default Product
```
- Since `Optional` is **empty**, `orElse()` returns `"Default Product"`.

---

## **E-Commerce Use Cases**

### **1. Retrieving Product Name or Default**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Product availableProduct = new Product("Smartphone");
        Product unavailableProduct = new Product(null);

        String productName1 = availableProduct.getName().orElse("No Product Found");
        String productName2 = unavailableProduct.getName().orElse("No Product Found");

        System.out.println("Product 1: " + productName1);
        System.out.println("Product 2: " + productName2);
    }
}
```
### **Output**
```
Product 1: Smartphone
Product 2: No Product Found
```
- Ensures a **default product name is used** if unavailable.

---

### **2. Providing Default Price If Missing**
```java
import java.util.Optional;

class Product {
    private Optional<Double> price;

    public Product(Double price) {
        this.price = Optional.ofNullable(price);
    }

    public Optional<Double> getPrice() {
        return price;
    }
}

public class ProductPriceExample {
    public static void main(String[] args) {
        Product productWithPrice = new Product(1200.0);
        Product productWithoutPrice = new Product(null);

        double price1 = productWithPrice.getPrice().orElse(100.0); // Default price: $100
        double price2 = productWithoutPrice.getPrice().orElse(100.0);

        System.out.println("Price 1: $" + price1);
        System.out.println("Price 2: $" + price2);
    }
}
```
### **Output**
```
Price 1: $1200.0
Price 2: $100.0
```
- Ensures **a fallback price** when the product **has no defined price**.

---

### **3. Assigning Default Customer Loyalty Points**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customerWithPoints = new Customer(150);
        Customer customerWithoutPoints = new Customer(null);

        int points1 = customerWithPoints.getLoyaltyPoints().orElse(0); // Default: 0 points
        int points2 = customerWithoutPoints.getLoyaltyPoints().orElse(0);

        System.out.println("Customer 1 Points: " + points1);
        System.out.println("Customer 2 Points: " + points2);
    }
}
```
### **Output**
```
Customer 1 Points: 150
Customer 2 Points: 0
```
- **Ensures customers always have a loyalty point value**.

---

### **4. Providing Default Shipping Address If Not Set**
```java
import java.util.Optional;

class Address {
    private String city;

    public Address(String city) {
        this.city = city;
    }

    public Optional<String> getCity() {
        return Optional.ofNullable(city);
    }
}

class Customer {
    private Optional<Address> address;

    public Customer(Address address) {
        this.address = Optional.ofNullable(address);
    }

    public Optional<Address> getAddress() {
        return address;
    }
}

public class ShippingAddressExample {
    public static void main(String[] args) {
        Customer customerWithAddress = new Customer(new Address("New York"));
        Customer customerWithoutAddress = new Customer(null);

        String address1 = customerWithAddress.getAddress()
                .flatMap(Address::getCity)
                .orElse("Default City");

        String address2 = customerWithoutAddress.getAddress()
                .flatMap(Address::getCity)
                .orElse("Default City");

        System.out.println("Shipping Address 1: " + address1);
        System.out.println("Shipping Address 2: " + address2);
    }
}
```
### **Output**
```
Shipping Address 1: New York
Shipping Address 2: Default City
```
- Ensures **every customer has a valid shipping address**.

---

## **Difference Between `orElse()` and `orElseGet()`**
| Method | When It Executes the Default Value | Use Case |
|--------|-----------------------------------|----------|
| `orElse(value)` | Always evaluates the default value, even if not used | When the default value is lightweight |
| `orElseGet(Supplier)` | Only evaluates the default when `Optional` is empty | When computing the default is expensive |

### **Example**
```java
public class OrElseVsOrElseGet {
    public static void main(String[] args) {
        Optional<String> opt = Optional.empty();

        System.out.println("Using orElse:");
        String result1 = opt.orElse(getDefault());

        System.out.println("Using orElseGet:");
        String result2 = opt.orElseGet(() -> getDefault());
    }

    public static String getDefault() {
        System.out.println("Generating default value...");
        return "Default Value";
    }
}
```
### **Output**
```
Using orElse:
Generating default value...
Using orElseGet:
Generating default value...
```
- **`orElse()` evaluates `getDefault()` even if the `Optional` is non-empty**.
- **`orElseGet()` only calls `getDefault()` when `Optional` is empty**.

---

## **Common Methods Used with `orElse()`**
| Method | Description | Example |
|--------|-------------|---------|
| `orElse(defaultValue)` | Returns the value if present, otherwise returns a default | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow(Supplier)` | Throws an exception if value is missing | `opt.orElseThrow(() -> new Exception("Not found"))` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents NullPointerException** | Ensures a default value when needed. |
| **Improves Readability** | Eliminates explicit `if-else` null checks. |
| **Works Well with Functional Programming** | Easily integrates with `map()`, `filter()`, etc. |

---

## **Conclusion**
- `Optional.orElse()` ensures a **fallback value** when **data is missing**.
- It is widely used in **e-commerce applications** for handling **missing product details, customer information, pricing, and shipping addresses**.

Would you like **more real-world scenarios**? 🚀

# What is Optional.orElseGet()?

## **Introduction**
The **`Optional.orElseGet()`** method in Java **(introduced in Java 8)** is used to **return the value inside an `Optional` if present**; otherwise, it executes a **supplier function** to generate a **default value**. It is similar to `orElse()`, but **`orElseGet()` only executes the supplier when the `Optional` is empty**, making it more efficient for **expensive default value computations**.

---

## **Key Points**
- **Returns the stored value if present**.
- **Executes a supplier function** (lazy execution) **only if `Optional` is empty**.
- **More efficient than `orElse()`** when the default value is **expensive to compute**.
- **Prevents `NullPointerException` (NPE)** while ensuring a valid return.

---

## **Syntax**
```java
T result = optionalValue.orElseGet(supplierFunction);
```
- **`optionalValue`** → The `Optional<T>` object.
- **`supplierFunction`** → A function (`() -> defaultValue`) executed **only if `Optional` is empty**.

### **Equivalent to:**
```java
if (optionalValue.isPresent()) {
    return optionalValue.get();
} else {
    return supplierFunction.get();
}
```

---

## **Difference Between `orElse()` and `orElseGet()`**
| Feature | `orElse(defaultValue)` | `orElseGet(Supplier)` |
|---------|------------------|-------------------|
| **When It Executes Default Value** | **Always**, even if `Optional` contains a value | **Only when `Optional` is empty** |
| **Use Case** | When the default value is **lightweight** | When computing the default is **expensive** |
| **Performance Impact** | Might be inefficient for costly operations | **More efficient** since it executes lazily |

### **Example: Performance Difference**
```java
import java.util.Optional;

public class OrElseVsOrElseGetExample {
    public static void main(String[] args) {
        Optional<String> optional = Optional.of("Laptop");

        System.out.println("Using orElse:");
        String result1 = optional.orElse(getDefault()); // Executes getDefault() always

        System.out.println("Using orElseGet:");
        String result2 = optional.orElseGet(() -> getDefault()); // Executes getDefault() only if empty
    }

    public static String getDefault() {
        System.out.println("Generating default value...");
        return "Default Product";
    }
}
```
### **Output**
```
Using orElse:
Generating default value...
Using orElseGet:
```
- **`orElse()` always evaluates `getDefault()`**, even when `Optional` contains a value.
- **`orElseGet()` does not execute `getDefault()`** if `Optional` is non-empty.

---

## **Example 1: Using `orElseGet()` with a Default Product Name**
```java
import java.util.Optional;

public class OptionalOrElseGetExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.empty();

        String product = optionalProduct.orElseGet(() -> "Default Product");

        System.out.println("Product: " + product);
    }
}
```
### **Output**
```
Product: Default Product
```
- Uses **lazy execution** to generate the product name **only if `Optional` is empty**.

---

## **E-Commerce Use Cases**

### **1. Providing Default Product Price If Missing**
```java
import java.util.Optional;

class Product {
    private Optional<Double> price;

    public Product(Double price) {
        this.price = Optional.ofNullable(price);
    }

    public Optional<Double> getPrice() {
        return price;
    }
}

public class ProductPriceExample {
    public static void main(String[] args) {
        Product productWithPrice = new Product(1200.0);
        Product productWithoutPrice = new Product(null);

        double price1 = productWithPrice.getPrice().orElseGet(() -> getDefaultPrice());
        double price2 = productWithoutPrice.getPrice().orElseGet(() -> getDefaultPrice());

        System.out.println("Price 1: $" + price1);
        System.out.println("Price 2: $" + price2);
    }

    public static double getDefaultPrice() {
        System.out.println("Fetching default price...");
        return 100.0;
    }
}
```
### **Output**
```
Price 1: $1200.0
Fetching default price...
Price 2: $100.0
```
- **Fetching the default price happens only for the missing price**.

---

### **2. Assigning Default Customer Loyalty Points If Missing**
```java
import java.util.Optional;

class Customer {
    private Integer loyaltyPoints;

    public Customer(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public Optional<Integer> getLoyaltyPoints() {
        return Optional.ofNullable(loyaltyPoints);
    }
}

public class CustomerLoyaltyExample {
    public static void main(String[] args) {
        Customer customerWithPoints = new Customer(150);
        Customer customerWithoutPoints = new Customer(null);

        int points1 = customerWithPoints.getLoyaltyPoints().orElseGet(() -> fetchDefaultLoyaltyPoints());
        int points2 = customerWithoutPoints.getLoyaltyPoints().orElseGet(() -> fetchDefaultLoyaltyPoints());

        System.out.println("Customer 1 Points: " + points1);
        System.out.println("Customer 2 Points: " + points2);
    }

    public static int fetchDefaultLoyaltyPoints() {
        System.out.println("Fetching default loyalty points...");
        return 10;
    }
}
```
### **Output**
```
Customer 1 Points: 150
Fetching default loyalty points...
Customer 2 Points: 10
```
- **Prevents unnecessary function execution** when points are available.

---

### **3. Assigning Default Shipping Address If Not Set**
```java
import java.util.Optional;

class Address {
    private String city;

    public Address(String city) {
        this.city = city;
    }

    public Optional<String> getCity() {
        return Optional.ofNullable(city);
    }
}

class Customer {
    private Optional<Address> address;

    public Customer(Address address) {
        this.address = Optional.ofNullable(address);
    }

    public Optional<Address> getAddress() {
        return address;
    }
}

public class ShippingAddressExample {
    public static void main(String[] args) {
        Customer customerWithAddress = new Customer(new Address("New York"));
        Customer customerWithoutAddress = new Customer(null);

        String address1 = customerWithAddress.getAddress()
                .flatMap(Address::getCity)
                .orElseGet(() -> fetchDefaultCity());

        String address2 = customerWithoutAddress.getAddress()
                .flatMap(Address::getCity)
                .orElseGet(() -> fetchDefaultCity());

        System.out.println("Shipping Address 1: " + address1);
        System.out.println("Shipping Address 2: " + address2);
    }

    public static String fetchDefaultCity() {
        System.out.println("Fetching default shipping address...");
        return "Default City";
    }
}
```
### **Output**
```
Shipping Address 1: New York
Fetching default shipping address...
Shipping Address 2: Default City
```
- **Retrieves a default city only if the address is missing**.

---

## **Common Methods Used with `orElseGet()`**
| Method | Description | Example |
|--------|-------------|---------|
| `orElse(defaultValue)` | Returns value if present, otherwise a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow(Supplier)` | Throws an exception if value is missing | `opt.orElseThrow(() -> new Exception("Not found"))` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Lazy Execution** | Only computes default value if `Optional` is empty. |
| **Prevents Unnecessary Function Calls** | Unlike `orElse()`, does not execute default when value is present. |
| **Improves Performance** | Especially useful for expensive computations. |

---

## **Conclusion**
- `Optional.orElseGet()` ensures **lazy execution** of default values.
- It is widely used in **e-commerce applications** for handling **missing product prices, customer loyalty points, and shipping addresses**.
- **Use it when computing default values is expensive**.

Would you like more **real-world scenarios**? 🚀

# What is Optional.orElseThrow()?
## **Introduction**
The **`Optional.orElseThrow()`** method in Java **(introduced in Java 8)** is used to **return the value inside an `Optional` if present**, but if the `Optional` is **empty**, it **throws an exception**. It is useful when **missing values should not be allowed** and must trigger an error.

---

## **Key Points**
- **Returns the stored value if present**.
- **Throws an exception if `Optional` is empty**.
- **Prevents `NullPointerException (NPE)`** by handling missing values explicitly.
- **Useful for enforcing non-nullable values** in business logic.

## **Syntax**
```java
T result = optionalValue.orElseThrow();
```
or  
```java
T result = optionalValue.orElseThrow(Supplier<? extends Throwable> exceptionSupplier);
```
- **`optionalValue`** → The `Optional<T>` object.
- **`exceptionSupplier`** → A function that provides a custom exception **if `Optional` is empty**.

### **Equivalent to:**
```java
if (optionalValue.isPresent()) {
    return optionalValue.get();
} else {
    throw new CustomException();
}
```

---

## **Difference Between `orElseThrow()`, `orElse()`, and `orElseGet()`**
| Feature | `orElse(defaultValue)` | `orElseGet(Supplier)` | `orElseThrow()` |
|---------|------------------|------------------|------------------|
| **Returns value if present** | ✅ Yes | ✅ Yes | ✅ Yes |
| **Computes default value** | Always | Only if `Optional` is empty | ❌ (Throws exception) |
| **Throws exception if empty** | ❌ No | ❌ No | ✅ Yes |
| **Use Case** | When a **default value** is acceptable | When **computing a default is expensive** | When **a missing value is an error** |

---

## **Example 1: Using `orElseThrow()` to Ensure a Non-Null Value**
```java
import java.util.Optional;

public class OptionalOrElseThrowExample {
    public static void main(String[] args) {
        Optional<String> optionalProduct = Optional.of("Laptop");

        String product = optionalProduct.orElseThrow();

        System.out.println("Product: " + product);
    }
}
```
### **Output**
```
Product: Laptop
```
- Since `Optional` contains `"Laptop"`, `orElseThrow()` returns it.

---

## **Example 2: Handling Empty Optional Using `orElseThrow()`**
```java
import java.util.Optional;

public class OptionalEmptyOrElseThrowExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        String product = emptyOptional.orElseThrow();

        System.out.println("Product: " + product);
    }
}
```
### **Output**
```
Exception in thread "main" java.util.NoSuchElementException: No value present
```
- **Since `Optional` is empty, `orElseThrow()` throws `NoSuchElementException`**.

---

## **Example 3: Using `orElseThrow()` with a Custom Exception**
```java
import java.util.Optional;

public class OptionalOrElseThrowCustomExample {
    public static void main(String[] args) {
        Optional<String> emptyOptional = Optional.empty();

        String product = emptyOptional.orElseThrow(() -> new RuntimeException("Product not found"));

        System.out.println("Product: " + product);
    }
}
```
### **Output**
```
Exception in thread "main" java.lang.RuntimeException: Product not found
```
- **Throws a custom exception when `Optional` is empty**.

---

## **E-Commerce Use Cases**

### **1. Fetching Product Name and Enforcing It Exists**
```java
import java.util.Optional;

class Product {
    private String name;

    public Product(String name) {
        this.name = name;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
}

public class ProductExample {
    public static void main(String[] args) {
        Product availableProduct = new Product("Smartphone");
        Product unavailableProduct = new Product(null);

        String productName1 = availableProduct.getName().orElseThrow(() -> new IllegalArgumentException("Product name missing"));
        System.out.println("Product 1: " + productName1);

        String productName2 = unavailableProduct.getName().orElseThrow(() -> new IllegalArgumentException("Product name missing"));
        System.out.println("Product 2: " + productName2);
    }
}
```
### **Output**
```
Product 1: Smartphone
Exception in thread "main" java.lang.IllegalArgumentException: Product name missing
```
- Ensures **all products must have a name**.

---

### **2. Ensuring Product Price Exists**
```java
import java.util.Optional;

class Product {
    private Optional<Double> price;

    public Product(Double price) {
        this.price = Optional.ofNullable(price);
    }

    public Optional<Double> getPrice() {
        return price;
    }
}

public class ProductPriceExample {
    public static void main(String[] args) {
        Product productWithPrice = new Product(1200.0);
        Product productWithoutPrice = new Product(null);

        double price1 = productWithPrice.getPrice().orElseThrow(() -> new IllegalStateException("Price not available"));
        System.out.println("Price 1: $" + price1);

        double price2 = productWithoutPrice.getPrice().orElseThrow(() -> new IllegalStateException("Price not available"));
        System.out.println("Price 2: $" + price2);
    }
}
```
### **Output**
```
Price 1: $1200.0
Exception in thread "main" java.lang.IllegalStateException: Price not available
```
- **Throws an exception when the price is missing**.

---

### **3. Ensuring Customer Email Exists for Order Processing**
```java
import java.util.Optional;

class Customer {
    private String email;

    public Customer(String email) {
        this.email = email;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }
}

public class EmailValidationExample {
    public static void main(String[] args) {
        Customer customerWithEmail = new Customer("customer@example.com");
        Customer customerWithoutEmail = new Customer(null);

        String email1 = customerWithEmail.getEmail().orElseThrow(() -> new IllegalArgumentException("Email is required"));
        System.out.println("Customer Email: " + email1);

        String email2 = customerWithoutEmail.getEmail().orElseThrow(() -> new IllegalArgumentException("Email is required"));
        System.out.println("Customer Email: " + email2);
    }
}
```
### **Output**
```
Customer Email: customer@example.com
Exception in thread "main" java.lang.IllegalArgumentException: Email is required
```
- Ensures **all customers have an email for order confirmation**.

---

## **Common Methods Used with `orElseThrow()`**
| Method | Description | Example |
|--------|-------------|---------|
| `orElse(defaultValue)` | Returns value if present, otherwise a default value | `opt.orElse("Default")` |
| `orElseGet(Supplier)` | Calls a function if value is missing | `opt.orElseGet(() -> fetchDefault())` |
| `orElseThrow(Supplier)` | Throws an exception if value is missing | `opt.orElseThrow(() -> new Exception("Not found"))` |

---

## **Advantages**
| Feature | Benefit |
|---------|---------|
| **Prevents Silent Errors** | Ensures required values **must be present**. |
| **Encourages Defensive Programming** | Helps enforce **non-null constraints**. |
| **Avoids Unexpected NullPointerException** | Allows throwing **meaningful exceptions** instead. |

---

## **When to Use `orElseThrow()`?**
✅ **When a missing value should cause an error (e.g., product price, order details, customer email).**  
✅ **When avoiding silent failures and enforcing constraints.**  
✅ **When integrating with APIs or databases where a field must always be present.**  

❌ **Do NOT use `orElseThrow()` when a default value is acceptable. Use `orElse()` instead.**  

## **Conclusion**
- `Optional.orElseThrow()` ensures **critical values must always exist**.
- It is widely used in **e-commerce applications** for handling **mandatory product details, pricing, customer emails, and order tracking**.
- **Use it to enforce business rules and avoid silent failures.**

Would you like **more real-world scenarios**? 🚀


# What is Intermediate Operations in Java Stream API?
## **Introduction**
The **Stream API** (introduced in Java 8) provides a functional programming approach to process collections (like Lists, Sets, and Maps) efficiently. **Intermediate operations** are operations that **process a stream** and return another **modified stream**. These operations are **lazy**, meaning they are **not executed until a terminal operation is invoked**.

---

## **Key Characteristics of Intermediate Operations**
1. **Lazy Evaluation:** They do not execute immediately but wait for a **terminal operation** to trigger execution.
2. **Return a New Stream:** They transform the current stream and return another stream.
3. **Can Be Chained:** Multiple intermediate operations can be linked together before a terminal operation is applied.
4. **Do Not Modify the Original Collection:** Streams **do not alter** the source collection.

---

## **List of Intermediate Operations**
| Operation | Description | Example Use Case |
|-----------|-------------|-----------------|
| **`filter(Predicate)`** | Filters elements based on a condition | Get all orders above $100 |
| **`map(Function)`** | Transforms each element | Convert product names to uppercase |
| **`flatMap(Function)`** | Flattens nested collections | Convert list of orders into individual items |
| **`distinct()`** | Removes duplicate elements | Get unique product categories |
| **`sorted()`** | Sorts elements in **natural order** | Sort customers by name |
| **`sorted(Comparator)`** | Sorts elements using a **custom comparator** | Sort products by price |
| **`limit(n)`** | Limits the number of elements | Get top 5 expensive products |
| **`skip(n)`** | Skips the first `n` elements | Get all products except the first 3 |
| **`peek(Consumer)`** | Allows debugging/logging within stream | Log product names while filtering |

---

## **1. `filter(Predicate<T>)` - Filtering Elements**
- Used to **retain elements** that match a condition.
- **Example:** Get all products that cost more than **$500**.

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class FilterExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0),
                new Product("Smartphone", 700.0));

        List<Product> expensiveProducts = products.stream()
                .filter(product -> product.price > 500)
                .collect(Collectors.toList());

        expensiveProducts.forEach(p -> System.out.println(p.name));
    }
}
```
### **Output**
```
Laptop
Smartphone
```
✅ **Filters products where `price > 500`**.

---

## **2. `map(Function<T, R>)` - Transforming Elements**
- **Modifies each element** and returns a transformed stream.
- **Example:** Convert **product names to uppercase**.

```java
import java.util.List;
import java.util.stream.Collectors;

public class MapExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Smartphone");

        List<String> upperCaseProducts = products.stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(upperCaseProducts);
    }
}
```
### **Output**
```
[LAPTOP, MOUSE, KEYBOARD, SMARTPHONE]
```
✅ **Transforms each product name to uppercase**.

---

## **3. `flatMap(Function<T, Stream<R>>)` - Flattening Nested Collections**
- **Flattens multiple lists into a single stream**.
- **Example:** Convert **a list of orders into individual items**.

```java
import java.util.List;
import java.util.stream.Collectors;

class Order {
    List<String> items;

    public Order(List<String> items) {
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }
}

public class FlatMapExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(List.of("Laptop", "Mouse")),
                new Order(List.of("Keyboard", "Monitor")),
                new Order(List.of("Smartphone", "Headphones"))
        );

        List<String> allItems = orders.stream()
                .flatMap(order -> order.getItems().stream()) // Flatten the list
                .collect(Collectors.toList());

        System.out.println(allItems);
    }
}
```
### **Output**
```
[Laptop, Mouse, Keyboard, Monitor, Smartphone, Headphones]
```
✅ **Converts multiple lists into a single list**.

---

## **4. `distinct()` - Removing Duplicates**
- Removes **duplicate elements** from a stream.
- **Example:** Get a list of **unique product categories**.

```java
import java.util.List;
import java.util.stream.Collectors;

public class DistinctExample {
    public static void main(String[] args) {
        List<String> categories = List.of("Electronics", "Furniture", "Electronics", "Books", "Books");

        List<String> uniqueCategories = categories.stream()
                .distinct()
                .collect(Collectors.toList());

        System.out.println(uniqueCategories);
    }
}
```
### **Output**
```
[Electronics, Furniture, Books]
```
✅ **Removes duplicate categories**.

---

## **5. `sorted()` and `sorted(Comparator)` - Sorting Elements**
### **Natural Order Sorting**
```java
import java.util.List;
import java.util.stream.Collectors;

public class SortedExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Smartphone");

        List<String> sortedProducts = products.stream()
                .sorted()
                .collect(Collectors.toList());

        System.out.println(sortedProducts);
    }
}
```
### **Output**
```
[Keyboard, Laptop, Mouse, Smartphone]
```
✅ **Sorts in ascending order**.

---

### **Custom Sorting (Sorting Products by Price)**
```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class CustomSortingExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0),
                new Product("Smartphone", 700.0));

        List<Product> sortedProducts = products.stream()
                .sorted((p1, p2) -> Double.compare(p1.price, p2.price)) // Sorting by price
                .collect(Collectors.toList());

        sortedProducts.forEach(p -> System.out.println(p.name + ": $" + p.price));
    }
}
```
### **Output**
```
Mouse: $30.0
Keyboard: $70.0
Smartphone: $700.0
Laptop: $1200.0
```
✅ **Sorts products by price (ascending order).**

---

## **6. `limit(n)` - Limiting the Number of Elements**
- Used to get **only the first `n` elements**.
- **Example:** Get the **top 3 most expensive products**.

```java
List<Product> topProducts = products.stream()
        .sorted((p1, p2) -> Double.compare(p2.price, p1.price)) // Sorting in descending order
        .limit(3) // Taking top 3
        .collect(Collectors.toList());
```

---

## **7. `skip(n)` - Skipping Elements**
- Used to **ignore the first `n` elements**.
- **Example:** Get all products **except the first 2**.

```java
List<Product> remainingProducts = products.stream()
        .skip(2) // Skipping first 2 elements
        .collect(Collectors.toList());
```

---

## **8. `peek(Consumer<T>)` - Debugging Streams**
- Used for **debugging/logging inside stream pipelines**.

```java
List<Product> result = products.stream()
        .peek(p -> System.out.println("Filtering: " + p.name))
        .filter(p -> p.price > 500)
        .peek(p -> System.out.println("Filtered: " + p.name))
        .collect(Collectors.toList());
```
✅ **Helps debug each step of stream processing**.

---

## **Conclusion**
Intermediate operations **process and transform data** within a Stream **without modifying the original collection**. They are **lazy** and only executed when a **terminal operation** is applied.

## **Comparison of All Intermediate Operations in Java 8 Stream API**

Java 8 Stream API provides **intermediate operations** that transform a stream into another stream. These operations are **lazy**, meaning they **don't execute immediately** but **wait until a terminal operation is called**.

---

## **Table: Comparison of Intermediate Operations**

| **Operation** | **Description** | **Type (Stateful/Stateless)** | **Preserves Order?** | **Parallel Stream Support?** | **Example Usage** |
|--------------|----------------|-----------------|----------------|----------------------|----------------|
| **`filter(Predicate<T>)`** | Filters elements based on a condition | Stateless | ✅ Yes | ✅ Yes | `stream.filter(x -> x > 10)` |
| **`map(Function<T, R>)`** | Transforms elements (one-to-one mapping) | Stateless | ✅ Yes | ✅ Yes | `stream.map(x -> x * 2)` |
| **`flatMap(Function<T, Stream<R>>)`** | Flattens nested structures (one-to-many mapping) | Stateless | ✅ Yes | ✅ Yes | `list.stream().flatMap(Collection::stream)` |
| **`distinct()`** | Removes duplicate elements (based on `equals()`) | Stateful | ✅ Yes | ❌ Slower in parallel | `stream.distinct()` |
| **`sorted()`** | Sorts elements in **natural order** | Stateful | ✅ Yes | ❌ Slower in parallel | `stream.sorted()` |
| **`sorted(Comparator<T>)`** | Sorts using a **custom comparator** | Stateful | ✅ Yes | ❌ Slower in parallel | `stream.sorted(Comparator.reverseOrder())` |
| **`peek(Consumer<T>)`** | Performs an operation (like `System.out.println()`) without modifying the stream | Stateless | ✅ Yes | ✅ Yes | `stream.peek(System.out::println)` |
| **`limit(long n)`** | Limits the number of elements | Stateful | ✅ Yes | ❌ Can be inefficient in parallel | `stream.limit(5)` |
| **`skip(long n)`** | Skips the first `n` elements | Stateful | ✅ Yes | ❌ Slower in parallel | `stream.skip(2)` |

---

## **Detailed Explanation of Each Operation**
### **1. `filter(Predicate<T>)`**
- **Filters elements** based on a condition.
- **Example**: Get even numbers.
```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
List<Integer> evenNumbers = numbers.stream()
                                   .filter(n -> n % 2 == 0)
                                   .collect(Collectors.toList());
System.out.println(evenNumbers); // [2, 4, 6]
```

---

### **2. `map(Function<T, R>)`**
- **Transforms each element** (one-to-one mapping).
- **Example**: Convert numbers to their squares.
```java
List<Integer> squaredNumbers = numbers.stream()
                                      .map(n -> n * n)
                                      .collect(Collectors.toList());
System.out.println(squaredNumbers); // [1, 4, 9, 16, 25, 36]
```

---

### **3. `flatMap(Function<T, Stream<R>>)`**
- **Flattens nested structures** (one-to-many mapping).
- **Example**: Convert a list of lists into a single list.
```java
List<List<Integer>> nestedList = Arrays.asList(
    Arrays.asList(1, 2, 3),
    Arrays.asList(4, 5),
    Arrays.asList(6)
);
List<Integer> flatList = nestedList.stream()
                                   .flatMap(List::stream)
                                   .collect(Collectors.toList());
System.out.println(flatList); // [1, 2, 3, 4, 5, 6]
```

---

### **4. `distinct()`**
- **Removes duplicates**.
- **Example**: Remove duplicate numbers.
```java
List<Integer> numbers = Arrays.asList(1, 2, 2, 3, 4, 4, 5);
List<Integer> uniqueNumbers = numbers.stream()
                                     .distinct()
                                     .collect(Collectors.toList());
System.out.println(uniqueNumbers); // [1, 2, 3, 4, 5]
```

---

### **5. `sorted()` (Natural Order)**
- **Sorts elements in natural order** (e.g., numbers in ascending order).
- **Example**: Sort a list of names.
```java
List<String> names = Arrays.asList("Charlie", "Alice", "Bob");
List<String> sortedNames = names.stream()
                                .sorted()
                                .collect(Collectors.toList());
System.out.println(sortedNames); // [Alice, Bob, Charlie]
```

---

### **6. `sorted(Comparator<T>)` (Custom Order)**
- **Sorts elements based on a custom comparator**.
- **Example**: Sort numbers in descending order.
```java
List<Integer> sortedDesc = numbers.stream()
                                  .sorted(Comparator.reverseOrder())
                                  .collect(Collectors.toList());
System.out.println(sortedDesc); // [5, 4, 3, 2, 1]
```

---

### **7. `peek(Consumer<T>)`**
- **Useful for debugging** (`System.out.println()` inside streams).
- **Example**: Print elements while processing.
```java
List<Integer> peekedNumbers = numbers.stream()
                                     .peek(n -> System.out.println("Processing: " + n))
                                     .map(n -> n * 2)
                                     .collect(Collectors.toList());
```
**Output:**
```
Processing: 1
Processing: 2
Processing: 3
Processing: 4
Processing: 5
Processing: 6
```

---

### **8. `limit(long n)`**
- **Limits the number of elements** in the stream.
- **Example**: Get the first 3 elements.
```java
List<Integer> limitedNumbers = numbers.stream()
                                      .limit(3)
                                      .collect(Collectors.toList());
System.out.println(limitedNumbers); // [1, 2, 3]
```

---

### **9. `skip(long n)`**
- **Skips the first `n` elements**.
- **Example**: Skip the first 3 elements.
```java
List<Integer> skippedNumbers = numbers.stream()
                                      .skip(3)
                                      .collect(Collectors.toList());
System.out.println(skippedNumbers); // [4, 5, 6]
```

---

## **Best Practices for Using Intermediate Operations**
| **Scenario** | **Best Operation to Use** |
|-------------|--------------------------|
| **Filtering elements** | `filter()` |
| **Transforming data** | `map()` |
| **Flattening nested structures** | `flatMap()` |
| **Removing duplicates** | `distinct()` |
| **Sorting elements** | `sorted()` |
| **Debugging inside streams** | `peek()` |
| **Limiting the number of elements** | `limit()` |
| **Skipping elements** | `skip()` |

---

## **Key Takeaways**
- **Intermediate operations return a Stream** and **don't execute until a terminal operation** is called.
- **Some operations are stateful (`distinct()`, `sorted()`, `limit()`)** and require **storing elements**, making them **less efficient in parallel processing**.
- **`map()` and `flatMap()` are used for transformations**, but `flatMap()` is **useful for nested structures**.
- **Parallel Stream** may impact performance in **stateful operations like `sorted()` and `distinct()`**.

# What is Terminal Operations in Java Stream API?

## **Introduction**
In Java **Stream API** (introduced in Java 8), **Terminal Operations** are the **final operations** performed on a stream that **trigger execution of intermediate operations** and **produce a result**. Unlike **Intermediate Operations**, which return a transformed Stream, **Terminal Operations return a result (e.g., List, Integer, Optional, or void).**

---

## **Key Characteristics of Terminal Operations**
1. **Trigger Execution** → They force the processing of intermediate operations.
2. **Consume the Stream** → After a terminal operation is invoked, the stream **cannot be reused**.
3. **Return Final Result** → They return a **collection, primitive value, Optional, or perform an action**.

---

## **List of Terminal Operations**
| Operation | Description | Example Use Case |
|-----------|-------------|-----------------|
| **`collect(Collector)`** | Collects elements into a list, set, or map | Convert a stream into a `List` |
| **`forEach(Consumer)`** | Iterates over elements | Print all products |
| **`count()`** | Counts elements in the stream | Get total number of orders |
| **`min(Comparator)`** | Finds the minimum element | Get cheapest product |
| **`max(Comparator)`** | Finds the maximum element | Get most expensive product |
| **`findFirst()`** | Gets the first element | Find the first customer order |
| **`findAny()`** | Gets any random element (parallel execution) | Find any in-stock product |
| **`allMatch(Predicate)`** | Checks if **all elements** match condition | Are all orders above $50? |
| **`anyMatch(Predicate)`** | Checks if **any element** matches condition | Is any product out of stock? |
| **`noneMatch(Predicate)`** | Checks if **no elements** match condition | Are no items out of stock? |
| **`reduce(BinaryOperator)`** | Reduces elements into a single value | Sum of all product prices |
| **`toArray()`** | Converts stream into an array | Convert list of products to an array |

---

## **1. `collect(Collector<T, A, R>)` - Collecting Elements**
- Used to convert a stream into a collection (List, Set, Map, etc.).
- Example: Convert a **Stream of product names** into a **List**.

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectExample {
    public static void main(String[] args) {
        List<String> products = Stream.of("Laptop", "Mouse", "Keyboard", "Smartphone")
                .collect(Collectors.toList());

        System.out.println(products);
    }
}
```
### **Output**
```
[Laptop, Mouse, Keyboard, Smartphone]
```

---

## **2. `forEach(Consumer<T>)` - Iterating Over Elements**
- Used to perform an action (e.g., print, update) on each element.
- Example: Print all product names.

```java
import java.util.List;

public class ForEachExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Smartphone");

        products.stream()
                .forEach(product -> System.out.println("Product: " + product));
    }
}
```
### **Output**
```
Product: Laptop
Product: Mouse
Product: Keyboard
Product: Smartphone
```

---

## **3. `count()` - Counting Elements**
- Returns the number of elements in the stream.
- Example: Count the number of **products above $500**.

```java
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class CountExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0),
                new Product("Smartphone", 700.0));

        long expensiveProducts = products.stream()
                .filter(product -> product.price > 500)
                .count();

        System.out.println("Number of expensive products: " + expensiveProducts);
    }
}
```
### **Output**
```
Number of expensive products: 2
```

---

## **4. `min(Comparator<T>)` and `max(Comparator<T>)` - Finding Min & Max Elements**
- Finds the minimum and maximum elements based on a comparator.
- Example: Find the **cheapest and most expensive product**.

```java
import java.util.List;
import java.util.Optional;

public class MinMaxExample {
    public static void main(String[] args) {
        List<Double> prices = List.of(1200.0, 30.0, 70.0, 700.0);

        Optional<Double> minPrice = prices.stream().min(Double::compare);
        Optional<Double> maxPrice = prices.stream().max(Double::compare);

        System.out.println("Cheapest Price: $" + minPrice.orElse(0.0));
        System.out.println("Most Expensive Price: $" + maxPrice.orElse(0.0));
    }
}
```
### **Output**
```
Cheapest Price: $30.0
Most Expensive Price: $1200.0
```

---

## **5. `findFirst()` and `findAny()` - Getting an Element**
- Returns the first element (`findFirst()`) or any element (`findAny()`).
- Example: Find the **first available product**.

```java
import java.util.List;
import java.util.Optional;

public class FindFirstExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Smartphone");

        Optional<String> firstProduct = products.stream().findFirst();

        System.out.println("First product: " + firstProduct.orElse("No product found"));
    }
}
```
### **Output**
```
First product: Laptop
```

---

## **6. `anyMatch()`, `allMatch()`, `noneMatch()` - Checking Conditions**
- Used to check if elements match a given condition.
- Example: Check if **any product costs more than $1000**.

```java
import java.util.List;

public class MatchExample {
    public static void main(String[] args) {
        List<Double> prices = List.of(1200.0, 30.0, 70.0, 700.0);

        boolean anyExpensive = prices.stream().anyMatch(price -> price > 1000);
        boolean allExpensive = prices.stream().allMatch(price -> price > 1000);
        boolean noneExpensive = prices.stream().noneMatch(price -> price > 2000);

        System.out.println("Any product > $1000: " + anyExpensive);
        System.out.println("All products > $1000: " + allExpensive);
        System.out.println("No products > $2000: " + noneExpensive);
    }
}
```
### **Output**
```
Any product > $1000: true
All products > $1000: false
No products > $2000: true
```

---

## **7. `reduce(BinaryOperator<T>)` - Reducing Elements**
- Used to combine all elements into a single value.
- Example: Calculate the **total price of all products**.

```java
import java.util.List;

public class ReduceExample {
    public static void main(String[] args) {
        List<Double> prices = List.of(1200.0, 30.0, 70.0, 700.0);

        double totalPrice = prices.stream()
                .reduce(0.0, Double::sum);

        System.out.println("Total Price: $" + totalPrice);
    }
}
```
### **Output**
```
Total Price: $2000.0
```

---

## **Conclusion**
Terminal Operations **trigger the execution of Stream processing** and **produce final results**. They **cannot be chained** like intermediate operations.

## **Comparison of All Terminal Operations in Java 8 Stream API**

Terminal operations **consume the stream and produce a result** (a value, collection, or side-effect). Once a terminal operation is executed, the stream is **closed and cannot be reused**.

---

## **Table: Comparison of Terminal Operations in Java 8**

| **Operation** | **Description** | **Return Type** | **Short-Circuiting?** | **Parallel Stream Support?** | **Example Usage** |
|--------------|----------------|-----------------|----------------|----------------------|----------------|
| **`forEach(Consumer<T>)`** | Performs an action on each element | `void` | ❌ No | ✅ Yes | `stream.forEach(System.out::println);` |
| **`forEachOrdered(Consumer<T>)`** | Same as `forEach()`, but maintains order | `void` | ❌ No | ❌ Slower in parallel | `stream.forEachOrdered(System.out::println);` |
| **`collect(Collector<T, A, R>)`** | Converts stream elements into a collection | `List`, `Set`, `Map`, etc. | ❌ No | ✅ Yes | `stream.collect(Collectors.toList());` |
| **`toArray()`** | Converts stream elements into an array | `Object[]` | ❌ No | ✅ Yes | `stream.toArray();` |
| **`reduce(BinaryOperator<T>)`** | Reduces elements to a **single result** (sum, product, etc.) | `Optional<T>` | ❌ No | ✅ Yes | `stream.reduce(Integer::sum);` |
| **`count()`** | Counts the number of elements in the stream | `long` | ❌ No | ✅ Yes | `stream.count();` |
| **`min(Comparator<T>)`** | Finds the **minimum** element based on a comparator | `Optional<T>` | ❌ No | ✅ Yes | `stream.min(Comparator.naturalOrder());` |
| **`max(Comparator<T>)`** | Finds the **maximum** element based on a comparator | `Optional<T>` | ❌ No | ✅ Yes | `stream.max(Comparator.naturalOrder());` |
| **`findFirst()`** | Retrieves the **first element** in the stream | `Optional<T>` | ✅ Yes | ❌ Slower in parallel | `stream.findFirst();` |
| **`findAny()`** | Retrieves **any element** from the stream | `Optional<T>` | ✅ Yes | ✅ Yes | `stream.findAny();` |
| **`anyMatch(Predicate<T>)`** | Returns `true` if **any element** matches the condition | `boolean` | ✅ Yes | ✅ Yes | `stream.anyMatch(x -> x > 10);` |
| **`allMatch(Predicate<T>)`** | Returns `true` if **all elements** match the condition | `boolean` | ✅ Yes | ✅ Yes | `stream.allMatch(x -> x > 10);` |
| **`noneMatch(Predicate<T>)`** | Returns `true` if **no elements** match the condition | `boolean` | ✅ Yes | ✅ Yes | `stream.noneMatch(x -> x > 10);` |

---

## **Detailed Explanation of Each Operation**
### **1. `forEach(Consumer<T>)`**
- **Executes an action** (printing, logging, modifying) on **each element**.
- **Does not guarantee order in parallel streams**.
```java
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
names.stream().forEach(System.out::println);
```
🔹 **Use `forEachOrdered()` if order is required.**

---

### **2. `collect(Collector<T, A, R>)`**
- **Converts stream elements into a `List`, `Set`, `Map` or other collection**.
```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
List<Integer> squaredNumbers = numbers.stream()
                                      .map(n -> n * n)
                                      .collect(Collectors.toList());
System.out.println(squaredNumbers); // [1, 4, 9, 16, 25]
```

---

### **3. `toArray()`**
- **Converts the stream elements into an array**.
```java
Integer[] numArray = numbers.stream().toArray(Integer[]::new);
System.out.println(Arrays.toString(numArray)); // [1, 2, 3, 4, 5]
```

---

### **4. `reduce(BinaryOperator<T>)`**
- **Reduces elements to a single value** (sum, product, concatenation).
```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
int sum = numbers.stream().reduce(0, Integer::sum);
System.out.println(sum); // 15
```

---

### **5. `count()`**
- **Counts the number of elements** in the stream.
```java
long count = numbers.stream().count();
System.out.println(count); // 5
```

---

### **6. `min(Comparator<T>)` and `max(Comparator<T>)`**
- **Finds the minimum/maximum element based on a comparator**.
```java
Optional<Integer> min = numbers.stream().min(Integer::compareTo);
Optional<Integer> max = numbers.stream().max(Integer::compareTo);
System.out.println(min.get()); // 1
System.out.println(max.get()); // 5
```

---

### **7. `findFirst()`**
- **Returns the first element** (works best with ordered streams).
```java
Optional<Integer> first = numbers.stream().findFirst();
System.out.println(first.get()); // 1
```

---

### **8. `findAny()`**
- **Returns any element** (efficient in parallel processing).
```java
Optional<Integer> any = numbers.stream().findAny();
System.out.println(any.get()); // Random element
```

---

### **9. `anyMatch(Predicate<T>)`, `allMatch(Predicate<T>)`, `noneMatch(Predicate<T>)`**
- **Returns boolean based on a condition check**.
```java
boolean anyEven = numbers.stream().anyMatch(n -> n % 2 == 0); // true
boolean allEven = numbers.stream().allMatch(n -> n % 2 == 0); // false
boolean noneNegative = numbers.stream().noneMatch(n -> n < 0); // true
```

---

## **Performance Considerations**
| **Operation** | **Performance Considerations** |
|--------------|--------------------------------|
| **`forEach()` / `forEachOrdered()`** | **Fastest** for side-effects (printing, logging). **Use `forEachOrdered()` only if order matters.** |
| **`collect()`** | **Best for creating collections** (faster than manual loops). |
| **`reduce()`** | Efficient for **mathematical reductions** (sum, product, min, max). |
| **`count()`** | Fast for **sequential streams**, but **slower in parallel** (stateful). |
| **`findFirst()` vs. `findAny()`** | **Use `findAny()` for parallel streams** (random selection is faster). |
| **`min()` / `max()`** | **Parallel-friendly**, but can slow down on large datasets. |
| **`anyMatch()` / `allMatch()` / `noneMatch()`** | **Short-circuiting** operations → **exit early if condition is met**. |

---

## **Best Practices for Using Terminal Operations**
| **Scenario** | **Best Terminal Operation** |
|-------------|--------------------------|
| **Perform an action on each element** | `forEach()` |
| **Collect elements into a `List`, `Set`, or `Map`** | `collect()` |
| **Find the first element** | `findFirst()` |
| **Find any element (parallel processing)** | `findAny()` |
| **Count elements** | `count()` |
| **Reduce elements to a single value** | `reduce()` |
| **Check if any element matches a condition** | `anyMatch()` |
| **Check if all elements match a condition** | `allMatch()` |
| **Check if no elements match a condition** | `noneMatch()` |

---

## **Key Takeaways**
- **Terminal operations close the stream after execution**.
- **Some operations (like `findAny()` and `anyMatch()`) short-circuit for better performance**.
- **Parallel stream may impact order-sensitive operations (`forEachOrdered()`, `findFirst()`)**.
- **Use `collect()` for collections, `reduce()` for reductions, and `count()` for element counting**.

### **Difference Between Intermediate and Terminal Operations in Java Stream API**

| Feature               | Intermediate Operations                            | Terminal Operations                          |
|-----------------------|-------------------------------------------------|--------------------------------------------|
| **Execution**         | **Lazy** (Executes only when a terminal operation is called) | **Eager** (Executes immediately and produces a result) |
| **Return Type**       | Returns a **Stream** (allows method chaining) | Returns a **final value** (List, Set, int, boolean, etc.) |
| **Effect on Stream**  | Does **not** consume the stream | **Consumes** the stream, making it unusable afterward |
| **Multiple Calls**    | Can be **called multiple times** in a pipeline | Ends the stream pipeline; cannot be called again |
| **Purpose**          | Transforms, filters, or sorts the stream | Collects, aggregates, or processes the stream |
| **Examples**         | `filter()`, `map()`, `flatMap()`, `sorted()`, `distinct()` | `collect()`, `forEach()`, `count()`, `reduce()`, `findFirst()` |
| **Chaining Allowed?** | Yes, returns a new stream | No, produces a result and stops further processing |

# What is Streams API?

The **Stream API** in Java 8 provides a powerful way to process collections (like Lists, Sets, and Maps) in a **functional programming style**. It allows for **sequential** and **parallel** execution of operations such as **filtering, mapping, reducing, sorting, and collecting**.

Stream API **does not modify the original data structure**; instead, it creates a **pipeline** of computations that process the data.

---

## **Key Characteristics of Stream API**
✅ **Functional Programming Style** → Uses **lambda expressions** and **method references**.  
✅ **Lazy Evaluation** → Operations are **not executed immediately** but only when a **terminal operation** is encountered.  
✅ **Parallel Processing** → Supports **parallel execution** for large datasets using **parallelStream()**.  
✅ **Stateless Operations** → Does not alter the **original collection**.  
✅ **Efficient Processing** → Performs **internal iteration** instead of external iteration (like `for` loops).  

---

## **1. How to Create a Stream?**  

### **1.1. Using `stream()` on Collections**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        Stream<String> nameStream = names.stream(); // Creating a stream from a list
        nameStream.forEach(System.out::println);
    }
}
```
### **Explanation**
- `.stream()` creates a **stream** from the `List<String>`.
- `.forEach(System.out::println)` **prints** each element.

---

### **1.2. Using `Stream.of()`**
```java
import java.util.stream.Stream;

public class StreamExample2 {
    public static void main(String[] args) {
        Stream<String> nameStream = Stream.of("Alice", "Bob", "Charlie");
        nameStream.forEach(System.out::println);
    }
}
```
### **Explanation**
- `Stream.of(...)` creates a stream **directly from values**.

---

### **1.3. Using `Arrays.stream()`**
```java
import java.util.Arrays;
import java.util.stream.IntStream;

public class StreamExample3 {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};

        IntStream numStream = Arrays.stream(numbers);
        numStream.forEach(System.out::println);
    }
}
```
### **Explanation**
- `Arrays.stream(numbers)` creates a stream from an **array**.

---

### **1.4. Using `Stream.iterate()` for Infinite Streams**
```java
import java.util.stream.Stream;

public class StreamExample4 {
    public static void main(String[] args) {
        Stream.iterate(1, n -> n + 1)
              .limit(5) 
              .forEach(System.out::println);
    }
}
```
### **Explanation**
- `Stream.iterate(1, n -> n + 1)` generates **infinite numbers**.
- `limit(5)` restricts output to **first 5 numbers**.

---

## **2. Stream Operations**  

Stream operations are **classified** into:  
### **📌 Intermediate Operations** (Transformations)  
- `filter(Predicate)` → Filters elements.  
- `map(Function)` → Transforms elements.  
- `sorted(Comparator)` → Sorts elements.  
- `distinct()` → Removes duplicates.  
- `limit(n)` → Limits the number of elements.  
- `skip(n)` → Skips first `n` elements.  

### **📌 Terminal Operations** (Produces a Result)  
- `forEach(Consumer)` → Performs an action on each element.  
- `count()` → Counts elements.  
- `collect(Collector)` → Converts stream to a collection.  
- `reduce(BinaryOperator)` → Performs a reduction.  
- `findFirst()` → Returns the first element.  
- `findAny()` → Returns any element.  
- `allMatch(Predicate)`, `anyMatch(Predicate)`, `noneMatch(Predicate)` → Checks conditions.  

---

## **3. Stream API Examples**

### **3.1. Filtering Elements Using `filter()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample5 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 15, 25);

        List<Integer> evenNumbers = numbers.stream()
                                           .filter(n -> n % 2 == 0)
                                           .collect(Collectors.toList());

        System.out.println("Even Numbers: " + evenNumbers); // Output: [10, 20, 30]
    }
}
```
### **Explanation**
- `filter(n -> n % 2 == 0)` → Selects only **even numbers**.

---

### **3.2. Transforming Elements Using `map()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample6 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("alice", "bob", "charlie");

        List<String> capitalizedNames = names.stream()
                                             .map(String::toUpperCase)
                                             .collect(Collectors.toList());

        System.out.println(capitalizedNames); // Output: [ALICE, BOB, CHARLIE]
    }
}
```
### **Explanation**
- `map(String::toUpperCase)` → Converts all names to **uppercase**.

---

### **3.3. Sorting Elements Using `sorted()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample7 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 1, 3, 2, 4);

        List<Integer> sortedNumbers = numbers.stream()
                                             .sorted()
                                             .collect(Collectors.toList());

        System.out.println(sortedNumbers); // Output: [1, 2, 3, 4, 5]
    }
}
```
### **Explanation**
- `sorted()` **sorts elements** in **ascending order**.

---

### **3.4. Finding the First Element Using `findFirst()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamExample8 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        Optional<String> firstName = names.stream().findFirst();

        firstName.ifPresent(name -> System.out.println("First Name: " + name)); // Output: Alice
    }
}
```
### **Explanation**
- `findFirst()` returns **first element** in a stream.

---

### **3.5. Summing Numbers Using `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class StreamExample9 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        int sum = numbers.stream().reduce(0, Integer::sum);

        System.out.println("Sum: " + sum); // Output: 15
    }
}
```
### **Explanation**
- `reduce(0, Integer::sum)` sums **all elements**.

---

## **4. Parallel Streams for Performance**
```java
import java.util.Arrays;
import java.util.List;

public class StreamExample10 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        numbers.parallelStream().forEach(n -> 
            System.out.println(Thread.currentThread().getName() + " - " + n)
        );
    }
}
```
### **Explanation**
- **`parallelStream()`** processes elements **in parallel**.

---

## **Conclusion**
The **Stream API** in Java 8 is a **powerful** tool for **processing collections efficiently**.  
✅ **Functional and Declarative** (Less Code)  
✅ **Supports Parallel Execution**  
✅ **Improves Performance**  

Would you like **more advanced scenarios**? 🚀

# What is filter()?
## **Introduction**
The **`filter(Predicate<T>)`** method in Java **Stream API** (introduced in Java 8) is an **intermediate operation** used to **select elements** from a Stream based on a **specified condition**. It returns a **new stream containing only the elements that satisfy the given predicate**.

---

## **Key Points**
- **Used for filtering data based on a condition.**
- **Returns a new Stream** with elements that pass the test.
- **Does not modify the original collection**.
- **Lazy operation** (only executed when a terminal operation is invoked).
- **Commonly used in retail and e-commerce for filtering products, orders, and customers**.

---

## **Syntax**
```java
Stream<T> filteredStream = originalStream.filter(predicate);
```
- **`originalStream`** → The original stream.
- **`predicate`** → A condition that determines which elements are retained.
- **`filteredStream`** → A new stream containing only elements that match the condition.

---

## **Example 1: Filtering Products Above $500**
```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class FilterExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0),
                new Product("Smartphone", 700.0));

        List<Product> expensiveProducts = products.stream()
                .filter(product -> product.price > 500)
                .collect(Collectors.toList());

        expensiveProducts.forEach(p -> System.out.println(p.name));
    }
}
```
### **Output**
```
Laptop
Smartphone
```
- **Filters products where `price > 500`**.

---

## **Retail Use Cases of `filter()`**

### **1. Filtering In-Stock Products**
```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class FilterStockExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 10),
                new Product("Mouse", 0),  // Out of stock
                new Product("Keyboard", 5),
                new Product("Smartphone", 0)); // Out of stock

        List<Product> inStockProducts = products.stream()
                .filter(product -> product.stock > 0)
                .collect(Collectors.toList());

        inStockProducts.forEach(p -> System.out.println(p.name));
    }
}
```
### **Output**
```
Laptop
Keyboard
```
- **Filters only products that have stock (`stock > 0`).**

---

### **2. Finding Orders Above a Certain Amount**
```java
import java.util.List;
import java.util.stream.Collectors;

class Order {
    int orderId;
    double amount;

    public Order(int orderId, double amount) {
        this.orderId = orderId;
        this.amount = amount;
    }
}

public class FilterOrdersExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(101, 250.0),
                new Order(102, 1500.0),
                new Order(103, 99.99),
                new Order(104, 750.0));

        List<Order> highValueOrders = orders.stream()
                .filter(order -> order.amount > 500)
                .collect(Collectors.toList());

        highValueOrders.forEach(o -> System.out.println("Order ID: " + o.orderId + ", Amount: $" + o.amount));
    }
}
```
### **Output**
```
Order ID: 102, Amount: $1500.0
Order ID: 104, Amount: $750.0
```
- **Filters only orders greater than `$500`.**

---

### **3. Filtering Customers with VIP Status**
```java
import java.util.List;
import java.util.stream.Collectors;

class Customer {
    String name;
    boolean isVIP;

    public Customer(String name, boolean isVIP) {
        this.name = name;
        this.isVIP = isVIP;
    }
}

public class FilterVIPCustomersExample {
    public static void main(String[] args) {
        List<Customer> customers = List.of(
                new Customer("Alice", true),
                new Customer("Bob", false),
                new Customer("Charlie", true),
                new Customer("David", false));

        List<Customer> vipCustomers = customers.stream()
                .filter(customer -> customer.isVIP)
                .collect(Collectors.toList());

        vipCustomers.forEach(c -> System.out.println("VIP Customer: " + c.name));
    }
}
```
### **Output**
```
VIP Customer: Alice
VIP Customer: Charlie
```
- **Filters only customers who have VIP status (`isVIP == true`).**

---

### **4. Filtering Products in a Specific Category**
```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class FilterCategoryExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", "Electronics"),
                new Product("Chair", "Furniture"),
                new Product("Smartphone", "Electronics"),
                new Product("Table", "Furniture"));

        List<Product> electronics = products.stream()
                .filter(product -> product.category.equals("Electronics"))
                .collect(Collectors.toList());

        electronics.forEach(p -> System.out.println("Electronics Product: " + p.name));
    }
}
```
### **Output**
```
Electronics Product: Laptop
Electronics Product: Smartphone
```
- **Filters only products in the "Electronics" category.**

---

## **Combining Multiple `filter()` Conditions**
- You can apply **multiple `filter()` operations** to refine results.
- Example: Get **in-stock products** that cost **more than $100**.

```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;
    int stock;

    public Product(String name, double price, int stock) {
        this.name = name;
        this.price = price;
        this.stock = stock;
    }
}

public class MultiFilterExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0, 10),
                new Product("Mouse", 30.0, 0),
                new Product("Keyboard", 150.0, 5),
                new Product("Smartphone", 700.0, 0));

        List<Product> expensiveInStockProducts = products.stream()
                .filter(p -> p.price > 100)
                .filter(p -> p.stock > 0)
                .collect(Collectors.toList());

        expensiveInStockProducts.forEach(p -> System.out.println(p.name));
    }
}
```
### **Output**
```
Laptop
Keyboard
```
- **Filters only products that have `stock > 0` and `price > 100`.**

---

## **Performance Considerations**
- **Filter First**: Apply `filter()` **before** using `map()`, `sorted()`, or `collect()` to avoid unnecessary computations.
- **Parallel Streams**: Use `.parallelStream()` for large datasets to improve performance.

---

## **Conclusion**
The `filter()` method is **one of the most powerful and frequently used Stream operations**. It is widely used in **retail and e-commerce applications** to:
- Filter **in-stock** products.
- Find **high-value orders**.
- Identify **VIP customers**.
- Select **products from a specific category**.

Would you like **more real-world scenarios or database integration examples**? 🚀

## **Advanced Complex Use Cases of `filter()` in Java Stream API (Retail & E-Commerce)**

The **`filter()`** method in Java's **Stream API** is incredibly powerful for complex real-world applications, especially in **retail and e-commerce**. Let's explore **advanced filtering scenarios** involving **multiple conditions, database integration, real-time inventory management, and nested object filtering**.

---

## **1. Filtering Products Based on Multiple Attributes (Stock, Price, Category)**
### **Scenario**  
A retail store wants to display **only available electronics products that cost more than $500**.

```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;
    double price;
    int stock;

    public Product(String name, String category, double price, int stock) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.stock = stock;
    }
}

public class AdvancedFilterExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", "Electronics", 1200.0, 5),
                new Product("Headphones", "Electronics", 250.0, 15),
                new Product("Smartphone", "Electronics", 800.0, 0),
                new Product("Chair", "Furniture", 100.0, 10),
                new Product("Keyboard", "Electronics", 150.0, 20)
        );

        List<Product> filteredProducts = products.stream()
                .filter(p -> p.category.equals("Electronics"))
                .filter(p -> p.price > 500)
                .filter(p -> p.stock > 0)
                .collect(Collectors.toList());

        filteredProducts.forEach(p -> System.out.println("Available Electronics: " + p.name));
    }
}
```
### **Output**
```
Available Electronics: Laptop
```
### **Why is this advanced?**
- **Filters based on multiple attributes** (`category`, `price`, `stock`).
- **Uses multiple `filter()` conditions** for better readability.
- **Efficient filtering** before collecting results.

---

## **2. Filtering Orders Based on Order Date and Total Amount**
### **Scenario**  
An online store wants to retrieve **only the recent high-value orders** (**last 30 days and amount > $1000**).

```java
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

class Order {
    int orderId;
    double amount;
    LocalDate orderDate;

    public Order(int orderId, double amount, LocalDate orderDate) {
        this.orderId = orderId;
        this.amount = amount;
        this.orderDate = orderDate;
    }
}

public class OrderFilterExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(101, 250.0, LocalDate.now().minusDays(5)),   // recent but low amount
                new Order(102, 1500.0, LocalDate.now().minusDays(10)), // recent and high amount
                new Order(103, 750.0, LocalDate.now().minusDays(40)),  // old order
                new Order(104, 1200.0, LocalDate.now().minusDays(3))   // recent and high amount
        );

        LocalDate last30Days = LocalDate.now().minusDays(30);

        List<Order> highValueRecentOrders = orders.stream()
                .filter(order -> order.orderDate.isAfter(last30Days))
                .filter(order -> order.amount > 1000)
                .collect(Collectors.toList());

        highValueRecentOrders.forEach(o -> 
            System.out.println("Recent High-Value Order ID: " + o.orderId));
    }
}
```
### **Output**
```
Recent High-Value Order ID: 102
Recent High-Value Order ID: 104
```
### **Why is this advanced?**
- **Filters based on dynamic date range** (`last 30 days`).
- **Applies multiple conditions** (`date + order amount`).
- **Useful for analytics and revenue tracking**.

---

## **3. Filtering Customers with Nested Address Details**
### **Scenario**  
A retailer wants to **filter customers who live in a specific city ("New York") and have placed at least one order**.

```java
import java.util.List;
import java.util.stream.Collectors;

class Address {
    String city;

    public Address(String city) {
        this.city = city;
    }
}

class Customer {
    String name;
    Address address;
    int totalOrders;

    public Customer(String name, Address address, int totalOrders) {
        this.name = name;
        this.address = address;
        this.totalOrders = totalOrders;
    }
}

public class CustomerFilterExample {
    public static void main(String[] args) {
        List<Customer> customers = List.of(
                new Customer("Alice", new Address("New York"), 3),
                new Customer("Bob", new Address("Los Angeles"), 0),
                new Customer("Charlie", new Address("New York"), 1),
                new Customer("David", new Address("Chicago"), 5)
        );

        List<Customer> nyCustomers = customers.stream()
                .filter(c -> c.address.city.equals("New York"))
                .filter(c -> c.totalOrders > 0)
                .collect(Collectors.toList());

        nyCustomers.forEach(c -> System.out.println("Customer in NY with orders: " + c.name));
    }
}
```
### **Output**
```
Customer in NY with orders: Alice
Customer in NY with orders: Charlie
```
### **Why is this advanced?**
- **Filters based on a nested object (`Address` inside `Customer`)**.
- **Combines filtering with business logic (`totalOrders > 0`)**.

---

## **4. Dynamic Filtering Using User Input (Lambdas)**
### **Scenario**  
Allow users to **filter products dynamically** based on **a user-defined condition**.

```java
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;
    String category;

    public Product(String name, double price, String category) {
        this.name = name;
        this.price = price;
        this.category = category;
    }
}

public class DynamicFilterExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0, "Electronics"),
                new Product("Mouse", 25.0, "Electronics"),
                new Product("Table", 150.0, "Furniture"),
                new Product("Sofa", 500.0, "Furniture")
        );

        // User-defined filter: Get only Electronics products above $100
        Predicate<Product> electronicsFilter = p -> p.category.equals("Electronics") && p.price > 100;

        List<Product> filteredProducts = products.stream()
                .filter(electronicsFilter)
                .collect(Collectors.toList());

        filteredProducts.forEach(p -> System.out.println("Filtered Product: " + p.name));
    }
}
```
### **Output**
```
Filtered Product: Laptop
```
### **Why is this advanced?**
- **Allows dynamic filtering using user-defined predicates**.
- **Can be modified at runtime to apply different filters**.

---

## **5. Filtering Data from an External API or Database**
### **Scenario**  
A retailer fetches **product data from an external API or database** and **filters only discounted products**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class API {
    public static List<Product> fetchProducts() {
        return Arrays.asList(
                new Product("Laptop", 1200.0, 1000.0), // Discounted
                new Product("Smartphone", 800.0, 800.0), // No discount
                new Product("Headphones", 250.0, 200.0) // Discounted
        );
    }
}

class Product {
    String name;
    double originalPrice;
    double salePrice;

    public Product(String name, double originalPrice, double salePrice) {
        this.name = name;
        this.originalPrice = originalPrice;
        this.salePrice = salePrice;
    }
}

public class APIProductFilterExample {
    public static void main(String[] args) {
        List<Product> discountedProducts = API.fetchProducts().stream()
                .filter(p -> p.originalPrice > p.salePrice)
                .collect(Collectors.toList());

        discountedProducts.forEach(p -> System.out.println("Discounted: " + p.name));
    }
}
```
### **Output**
```
Discounted: Laptop
Discounted: Headphones
```
### **Why is this advanced?**
- **Simulates fetching real-time data from an API/database**.
- **Applies discount logic dynamically**.

---

## **Conclusion**
These advanced examples demonstrate how **`filter()`** can be used effectively in **e-commerce and retail applications** for:
- **Complex multi-attribute filtering**
- **Date-based filtering**
- **Nested object filtering**
- **Dynamic filtering with user input**
- **Filtering data from APIs or databases**


# What is map(), mapToInt(), mapToLong(), mapToDouble()?
## **Introduction**
The **Stream API** in Java (introduced in Java 8) provides the **`map()`** method and its specialized versions (`mapToInt()`, `mapToLong()`, and `mapToDouble()`) to **transform** each element of a stream into a new value. These methods are commonly used for **data transformations, mathematical operations, and type conversions**.

---

## **Key Points**
- **`map(Function<T, R>)`** → Converts elements in a stream from one type to another.
- **`mapToInt(ToIntFunction<T>)`** → Converts stream elements to `int` values.
- **`mapToLong(ToLongFunction<T>)`** → Converts stream elements to `long` values.
- **`mapToDouble(ToDoubleFunction<T>)`** → Converts stream elements to `double` values.
- Used for **numeric calculations, data extraction, and performance optimization**.
- **Improves efficiency** by avoiding unnecessary boxing/unboxing.

---

# **1. `map(Function<T, R>)` - General Transformation**
### **What It Does?**
The **`map()`** method applies a **function** to each element in the stream and returns a **new stream** with transformed elements.

### **Example 1: Converting Product Names to Uppercase**
```java
import java.util.List;
import java.util.stream.Collectors;

public class MapExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Smartphone");

        List<String> upperCaseProducts = products.stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(upperCaseProducts);
    }
}
```
### **Output**
```
[LAPTOP, MOUSE, KEYBOARD, SMARTPHONE]
```
### **Why use `map()`?**
- Transforms **each element** in the stream.
- Keeps the same **number of elements**, just **modified values**.
- Avoids **loops** and makes the code more readable.

---

### **Example 2: Extracting Product Names from an Object List**
```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ExtractNames {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0)
        );

        List<String> productNames = products.stream()
                .map(product -> product.name) // Extracts names
                .collect(Collectors.toList());

        System.out.println(productNames);
    }
}
```
### **Output**
```
[Laptop, Mouse, Keyboard]
```
### **Why use `map()` here?**
- Extracts **specific attributes** (`name`) from **complex objects**.
- Creates a **stream of Strings** from a **stream of Products**.

---

# **2. `mapToInt(ToIntFunction<T>)` - Converting Stream to `IntStream`**
### **What It Does?**
The **`mapToInt()`** method converts elements to `int` values and returns an **`IntStream`**, which is more efficient for numerical operations.

### **Example 1: Converting Prices to Integer**
```java
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class MapToIntExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.50),
                new Product("Mouse", 29.99),
                new Product("Keyboard", 69.49)
        );

        int totalPrice = products.stream()
                .mapToInt(p -> (int) p.price) // Converts to int
                .sum(); // Sum of all prices

        System.out.println("Total Price: $" + totalPrice);
    }
}
```
### **Output**
```
Total Price: $1298
```
### **Why use `mapToInt()`?**
- Returns an **`IntStream`** instead of `Stream<Integer>`, improving performance.
- Eliminates **boxing/unboxing overhead**.

---

### **Example 2: Counting Number of Characters in Each Product Name**
```java
import java.util.List;
import java.util.stream.IntStream;

public class MapToIntExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard");

        IntStream nameLengths = products.stream()
                .mapToInt(String::length); // Get name lengths

        nameLengths.forEach(System.out::println);
    }
}
```
### **Output**
```
6
5
8
```
- **Extracts integer values directly** instead of storing them as `Integer` objects.

---

# **3. `mapToLong(ToLongFunction<T>)` - Converting Stream to `LongStream`**
### **What It Does?**
The **`mapToLong()`** method works like `mapToInt()`, but converts elements to **`long` values**.

### **Example 1: Calculating Total Sales of All Products**
```java
import java.util.List;

class Product {
    String name;
    long unitsSold;

    public Product(String name, long unitsSold) {
        this.name = name;
        this.unitsSold = unitsSold;
    }
}

public class MapToLongExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 10000),
                new Product("Mouse", 50000),
                new Product("Keyboard", 25000)
        );

        long totalSales = products.stream()
                .mapToLong(p -> p.unitsSold)
                .sum(); // Sum all units sold

        System.out.println("Total Units Sold: " + totalSales);
    }
}
```
### **Output**
```
Total Units Sold: 85000
```
### **Why use `mapToLong()`?**
- Handles **large numbers** efficiently (`long` vs `int`).
- Reduces **memory overhead**.

---

# **4. `mapToDouble(ToDoubleFunction<T>)` - Converting Stream to `DoubleStream`**
### **What It Does?**
The **`mapToDouble()`** method converts elements to **`double` values**, useful for **financial and statistical calculations**.

### **Example 1: Calculating Total Revenue from Products**
```java
import java.util.List;

class Product {
    String name;
    double price;
    int quantitySold;

    public Product(String name, double price, int quantitySold) {
        this.name = name;
        this.price = price;
        this.quantitySold = quantitySold;
    }
}

public class MapToDoubleExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0, 5),
                new Product("Mouse", 30.0, 20),
                new Product("Keyboard", 70.0, 15)
        );

        double totalRevenue = products.stream()
                .mapToDouble(p -> p.price * p.quantitySold)
                .sum(); // Total revenue calculation

        System.out.println("Total Revenue: $" + totalRevenue);
    }
}
```
### **Output**
```
Total Revenue: $8850.0
```
### **Why use `mapToDouble()`?**
- Performs **precise calculations** involving decimals.
- Improves efficiency compared to using `map()` and `Double`.

---

## **Performance Considerations**
| Method | Returns | Use Case |
|--------|---------|----------|
| `map(Function<T, R>)` | `Stream<R>` | Data transformation (e.g., converting product objects to names) |
| `mapToInt(ToIntFunction<T>)` | `IntStream` | Optimized numeric calculations with `int` |
| `mapToLong(ToLongFunction<T>)` | `LongStream` | Handling large numbers (e.g., total sales) |
| `mapToDouble(ToDoubleFunction<T>)` | `DoubleStream` | Precision calculations (e.g., revenue, prices) |

---

## **Conclusion**
- **`map()`** is best for **general transformations**.
- **`mapToInt()`, `mapToLong()`, and `mapToDouble()`** are optimized for **numeric computations**.
- Use **primitive streams (`IntStream`, `LongStream`, `DoubleStream`)** to **reduce memory usage**.

Would you like **more advanced use cases** or **database integration examples**? 🚀

# What is flatMap(), flatMapToInt(), flatMapToLong(), flatMapToDouble()?
#### **Introduction**  
The **`flatMap()`** and its specialized versions (**`flatMapToInt()`**, **`flatMapToLong()`**, and **`flatMapToDouble()`**) in Java **Stream API** (introduced in Java 8) are used to **flatten nested structures (Streams inside Streams)**.  

---

### **Key Points**  
- **`flatMap(Function<T, Stream<R>>)`** → Transforms each element into a stream and flattens it.  
- **`flatMapToInt(ToIntFunction<T>)`** → Returns an **`IntStream`**.  
- **`flatMapToLong(ToLongFunction<T>)`** → Returns a **`LongStream`**.  
- **`flatMapToDouble(ToDoubleFunction<T>)`** → Returns a **`DoubleStream`**.  
- **Useful for handling lists of lists, nested objects, and numeric data processing.**  

---

### **1. `flatMap()` - Flattening Nested Streams**  
#### **Scenario**: Convert a list of orders into individual items.  
```java
import java.util.List;
import java.util.stream.Collectors;

class Order {
    List<String> items;

    public Order(List<String> items) {
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }
}

public class FlatMapExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(List.of("Laptop", "Mouse")),
                new Order(List.of("Keyboard", "Monitor")),
                new Order(List.of("Smartphone", "Headphones"))
        );

        List<String> allItems = orders.stream()
                .flatMap(order -> order.getItems().stream()) // Flatten list of lists
                .collect(Collectors.toList());

        System.out.println(allItems);
    }
}
```
#### **Output**  
```
[Laptop, Mouse, Keyboard, Monitor, Smartphone, Headphones]
```
- **Extracts individual items from nested `List<List<String>>` into a `List<String>`**.  

---

### **2. `flatMapToInt()` - Flattening to IntStream**  
#### **Scenario**: Convert a list of employee salaries into an `IntStream`.  
```java
import java.util.List;
import java.util.stream.IntStream;

class Employee {
    List<Integer> salaries;

    public Employee(List<Integer> salaries) {
        this.salaries = salaries;
    }

    public List<Integer> getSalaries() {
        return salaries;
    }
}

public class FlatMapToIntExample {
    public static void main(String[] args) {
        List<Employee> employees = List.of(
                new Employee(List.of(50000, 60000)),
                new Employee(List.of(70000, 80000)),
                new Employee(List.of(90000, 100000))
        );

        IntStream salaryStream = employees.stream()
                .flatMapToInt(emp -> emp.getSalaries().stream().mapToInt(Integer::intValue));

        salaryStream.forEach(System.out::println);
    }
}
```
#### **Output**  
```
50000
60000
70000
80000
90000
100000
```
- **Extracts integer salary values from a nested list and converts them into an `IntStream`**.  

---

### **3. `flatMapToLong()` - Flattening to LongStream**  
#### **Scenario**: Convert a list of store sales into a `LongStream` for performance.  
```java
import java.util.List;
import java.util.stream.LongStream;

class Store {
    List<Long> sales;

    public Store(List<Long> sales) {
        this.sales = sales;
    }

    public List<Long> getSales() {
        return sales;
    }
}

public class FlatMapToLongExample {
    public static void main(String[] args) {
        List<Store> stores = List.of(
                new Store(List.of(1000000L, 1500000L)),
                new Store(List.of(2000000L, 2500000L))
        );

        LongStream salesStream = stores.stream()
                .flatMapToLong(store -> store.getSales().stream().mapToLong(Long::longValue));

        salesStream.forEach(System.out::println);
    }
}
```
#### **Output**  
```
1000000
1500000
2000000
2500000
```
- **Flattens a list of sales amounts into a single `LongStream` for efficient processing.**  

---

### **4. `flatMapToDouble()` - Flattening to DoubleStream**  
#### **Scenario**: Convert a list of product ratings into a `DoubleStream`.  
```java
import java.util.List;
import java.util.stream.DoubleStream;

class Product {
    List<Double> ratings;

    public Product(List<Double> ratings) {
        this.ratings = ratings;
    }

    public List<Double> getRatings() {
        return ratings;
    }
}

public class FlatMapToDoubleExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product(List.of(4.5, 3.9)),
                new Product(List.of(4.8, 4.2))
        );

        DoubleStream ratingsStream = products.stream()
                .flatMapToDouble(product -> product.getRatings().stream().mapToDouble(Double::doubleValue));

        ratingsStream.forEach(System.out::println);
    }
}
```
#### **Output**  
```
4.5
3.9
4.8
4.2
```
- **Flattens product ratings into a `DoubleStream` for efficient numerical operations.**  

---

### **Key Differences Between `map()` and `flatMap()`**
| Feature | `map()` | `flatMap()` |
|---------|--------|------------|
| Returns | `Stream<Stream<R>>` (Nested Stream) | `Stream<R>` (Flattened Stream) |
| Used For | Transforming elements individually | Flattening nested lists/collections |
| Example | `stream.map(List::size)` → `Stream<Integer>` | `stream.flatMap(List::stream)` → `Stream<R>` |

---

### **Performance Considerations**
| Method | Returns | Use Case |
|--------|---------|----------|
| `flatMap(Function<T, Stream<R>>)` | `Stream<R>` | Flattening nested lists of objects |
| `flatMapToInt(ToIntFunction<T>)` | `IntStream` | Handling primitive `int` efficiently |
| `flatMapToLong(ToLongFunction<T>)` | `LongStream` | Processing large `long` values like sales data |
| `flatMapToDouble(ToDoubleFunction<T>)` | `DoubleStream` | Numeric processing (e.g., prices, ratings) |

---

### **Conclusion**
- **`flatMap()`** is ideal for handling **nested lists and stream flattening**.  
- **Primitive versions (`flatMapToInt()`, `flatMapToLong()`, `flatMapToDouble()`)** are useful for **efficient numerical processing**.  
- **Improves performance** by avoiding unnecessary boxing/unboxing.  

### **Advanced Use Cases Combining `map()` and `flatMap()` in Java Stream API**  

Combining `map()` and `flatMap()` enables **complex data transformations**, especially in **e-commerce, retail, and finance** scenarios. These examples show **nested data processing, extracting attributes, and aggregating numerical values efficiently**.

---

### **1. Extracting Customer Names & Flattening Purchased Items**
#### **Scenario:**  
A store wants to retrieve a list of **customer names** and **all unique purchased items** from their orders.

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Order {
    List<String> items;

    public Order(List<String> items) {
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }
}

class Customer {
    String name;
    List<Order> orders;

    public Customer(String name, List<Order> orders) {
        this.name = name;
        this.orders = orders;
    }

    public String getName() {
        return name;
    }

    public List<Order> getOrders() {
        return orders;
    }
}

public class CustomerOrdersExample {
    public static void main(String[] args) {
        List<Customer> customers = List.of(
                new Customer("Alice", List.of(
                        new Order(List.of("Laptop", "Mouse")),
                        new Order(List.of("Keyboard", "Monitor")))),

                new Customer("Bob", List.of(
                        new Order(List.of("Smartphone", "Headphones")))),

                new Customer("Charlie", List.of(
                        new Order(List.of("Mouse", "Laptop")))) // Duplicate items
        );

        // Extracting customer names
        List<String> customerNames = customers.stream()
                .map(Customer::getName)
                .collect(Collectors.toList());

        // Flattening purchased items into a unique list
        Set<String> allPurchasedItems = customers.stream()
                .flatMap(c -> c.getOrders().stream()) // Stream<Order>
                .flatMap(order -> order.getItems().stream()) // Flatten to Stream<String>
                .collect(Collectors.toSet()); // Unique items

        System.out.println("Customer Names: " + customerNames);
        System.out.println("All Purchased Items: " + allPurchasedItems);
    }
}
```
#### **Output**  
```
Customer Names: [Alice, Bob, Charlie]
All Purchased Items: [Laptop, Mouse, Keyboard, Monitor, Smartphone, Headphones]
```
- **`map()` extracts customer names.**  
- **`flatMap()` first flattens orders, then flattens individual items.**  

---

### **2. Calculating Total Sales by Category**
#### **Scenario:**  
An online store wants to **group sales by category**, calculate total revenue, and return a **map of categories to total sales**.

```java
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;
    double price;
    int quantitySold;

    public Product(String name, String category, double price, int quantitySold) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantitySold = quantitySold;
    }

    public String getCategory() {
        return category;
    }

    public double getTotalSales() {
        return price * quantitySold;
    }
}

public class SalesByCategoryExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", "Electronics", 1200.0, 5),
                new Product("Mouse", "Electronics", 30.0, 20),
                new Product("Sofa", "Furniture", 500.0, 2),
                new Product("Table", "Furniture", 200.0, 3),
                new Product("Headphones", "Electronics", 100.0, 10)
        );

        // Grouping total sales by category
        Map<String, Double> salesByCategory = products.stream()
                .collect(Collectors.groupingBy(
                        Product::getCategory, 
                        Collectors.summingDouble(Product::getTotalSales)
                ));

        System.out.println("Sales by Category: " + salesByCategory);
    }
}
```
#### **Output**  
```
Sales by Category: {Electronics=7300.0, Furniture=1300.0}
```
- **`map()` extracts categories and calculates total sales per product.**  
- **`Collectors.groupingBy()` aggregates sales per category.**  

---

### **3. Extracting and Flattening Transaction Data from Multiple Accounts**
#### **Scenario:**  
A bank wants to **flatten all transactions from multiple customer accounts** and **calculate the average transaction amount**.

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

class Transaction {
    double amount;
    String type;

    public Transaction(double amount, String type) {
        this.amount = amount;
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }
}

class Account {
    List<Transaction> transactions;

    public Account(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }
}

class Customer {
    String name;
    List<Account> accounts;

    public Customer(String name, List<Account> accounts) {
        this.name = name;
        this.accounts = accounts;
    }

    public List<Account> getAccounts() {
        return accounts;
    }
}

public class BankTransactionExample {
    public static void main(String[] args) {
        List<Customer> customers = List.of(
                new Customer("Alice", List.of(
                        new Account(List.of(new Transaction(200.0, "Deposit"), new Transaction(100.0, "Withdraw"))),
                        new Account(List.of(new Transaction(300.0, "Deposit"))))),
                
                new Customer("Bob", List.of(
                        new Account(List.of(new Transaction(400.0, "Deposit"), new Transaction(500.0, "Withdraw")))))
        );

        // Flatten all transactions
        List<Transaction> allTransactions = customers.stream()
                .flatMap(c -> c.getAccounts().stream()) // Stream<Account>
                .flatMap(a -> a.getTransactions().stream()) // Flatten to Stream<Transaction>
                .collect(Collectors.toList());

        // Calculate the average transaction amount
        double averageTransaction = allTransactions.stream()
                .mapToDouble(Transaction::getAmount)
                .average()
                .orElse(0.0);

        System.out.println("All Transactions: " + allTransactions.size());
        System.out.println("Average Transaction Amount: $" + averageTransaction);
    }
}
```
#### **Output**  
```
All Transactions: 5
Average Transaction Amount: $300.0
```
- **`flatMap()` first flattens accounts, then transactions.**  
- **`mapToDouble()` extracts amounts and calculates the average.**  

---

### **4. Filtering and Transforming Employee Data**
#### **Scenario:**  
An HR system needs to **extract employee names** and **flatten all skill sets across departments**.

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    List<String> skills;

    public Employee(String name, List<String> skills) {
        this.name = name;
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public List<String> getSkills() {
        return skills;
    }
}

class Department {
    List<Employee> employees;

    public Department(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }
}

public class EmployeeSkillsExample {
    public static void main(String[] args) {
        List<Department> departments = List.of(
                new Department(List.of(
                        new Employee("Alice", List.of("Java", "Spring")),
                        new Employee("Bob", List.of("Python", "Django")))),

                new Department(List.of(
                        new Employee("Charlie", List.of("JavaScript", "React")),
                        new Employee("David", List.of("Java", "Angular"))))
        );

        // Extract employee names
        List<String> employeeNames = departments.stream()
                .flatMap(d -> d.getEmployees().stream())
                .map(Employee::getName)
                .collect(Collectors.toList());

        // Extract all unique skills
        Set<String> allSkills = departments.stream()
                .flatMap(d -> d.getEmployees().stream())
                .flatMap(e -> e.getSkills().stream())
                .collect(Collectors.toSet());

        System.out.println("Employee Names: " + employeeNames);
        System.out.println("All Unique Skills: " + allSkills);
    }
}
```
#### **Output**
```
Employee Names: [Alice, Bob, Charlie, David]
All Unique Skills: [Java, Spring, Python, Django, JavaScript, React, Angular]
```

Would you like **more use cases with database integration or APIs?** 🚀

# What is distinct()?

#### **Introduction**
The **`distinct()`** method in Java **Stream API** is used to **remove duplicate elements** from a stream. It ensures that the resulting stream contains **only unique elements**, similar to how a **Set** removes duplicates in a collection.

---

### **Key Points**
- **Removes duplicate elements** based on the `equals()` method.
- **Works on all types of Streams** (Integer, String, Objects, etc.).
- **Returns a new Stream** with only distinct elements.
- **Maintains the order of first occurrences**.
- **Does not modify the original collection**.

---

### **Syntax**
```java
Stream<T> uniqueStream = originalStream.distinct();
```

---

## **1. Removing Duplicates from a List of Numbers**
#### **Example: Removing duplicate integers**
```java
import java.util.List;
import java.util.stream.Collectors;

public class DistinctNumbersExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 3, 2, 1, 5, 6, 6, 7);

        List<Integer> uniqueNumbers = numbers.stream()
                .distinct()
                .collect(Collectors.toList());

        System.out.println(uniqueNumbers);
    }
}
```
#### **Output**
```
[1, 2, 3, 4, 5, 6, 7]
```
- **Removes duplicates** while **maintaining order**.

---

## **2. Removing Duplicates from a List of Strings**
#### **Example: Unique product categories**
```java
import java.util.List;
import java.util.stream.Collectors;

public class DistinctStringsExample {
    public static void main(String[] args) {
        List<String> categories = List.of("Electronics", "Furniture", "Electronics", "Books", "Books");

        List<String> uniqueCategories = categories.stream()
                .distinct()
                .collect(Collectors.toList());

        System.out.println(uniqueCategories);
    }
}
```
#### **Output**
```
[Electronics, Furniture, Books]
```
- **Filters out duplicate product categories**.

---

## **3. Removing Duplicates from a List of Objects**
By default, `distinct()` uses **`equals()` and `hashCode()`** to compare objects.

#### **Scenario**:  
Filter **unique products** based on their `name`.

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Objects;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name); // Compare only by name
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

public class DistinctObjectsExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Laptop", 1000.0), // Duplicate by name
                new Product("Keyboard", 70.0),
                new Product("Mouse", 25.0) // Duplicate by name
        );

        List<Product> uniqueProducts = products.stream()
                .distinct()
                .collect(Collectors.toList());

        uniqueProducts.forEach(p -> System.out.println(p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Laptop - $1200.0
Mouse - $30.0
Keyboard - $70.0
```
- **`distinct()` considers two objects equal if their `equals()` method returns `true`.**
- **Only the first occurrence of each unique `name` is retained.**

---

## **4. Removing Duplicates Based on a Custom Property**
If you want to filter unique elements **based on a specific attribute** (e.g., price instead of name), `distinct()` alone **won't work**. You need to use **`collect()` with `Collectors.toMap()`**.

#### **Scenario**:  
Find **unique products based on price**.

```java
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class DistinctByPriceExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Laptop", 1000.0), // Different price
                new Product("Keyboard", 70.0),
                new Product("Mouse", 30.0) // Same price
        );

        List<Product> uniqueByPrice = products.stream()
                .collect(Collectors.toMap(
                        Product::getPrice, // Key: Price
                        p -> p,            // Value: Product
                        (existing, replacement) -> existing)) // Keep first occurrence
                .values()
                .stream()
                .collect(Collectors.toList());

        uniqueByPrice.forEach(p -> System.out.println(p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Laptop - $1200.0
Mouse - $30.0
Keyboard - $70.0
```
- **Filters based on price instead of name.**
- **Uses `Collectors.toMap()` to remove duplicates by a custom field.**

---

## **5. Combining `distinct()` with Other Stream Operations**
You can use `distinct()` together with `sorted()`, `filter()`, and `map()`.

#### **Scenario**:  
Get **distinct product names**, sorted alphabetically.

```java
import java.util.List;
import java.util.stream.Collectors;

public class CombinedDistinctExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Laptop", "Keyboard", "Mouse");

        List<String> distinctSortedProducts = products.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        System.out.println(distinctSortedProducts);
    }
}
```
#### **Output**
```
[Keyboard, Laptop, Mouse]
```
- **Combines `distinct()`, `sorted()`, and `collect()`.**

---

## **Performance Considerations**
| **Aspect**       | **Behavior** |
|-----------------|-------------|
| **Time Complexity** | `O(n²)` (if using `equals()`) |
| **Performance with Large Data** | May be slow with large collections |
| **Best for Unique Elements** | Works well with `List<String>` and `List<Integer>` |
| **For Objects** | Ensure proper `equals()` and `hashCode()` methods |

---

## **Summary**
| **Feature** | **Behavior** |
|------------|-------------|
| **Removes Duplicates** | Retains only unique elements based on `equals()` |
| **Preserves Order** | Keeps the first occurrence of each unique element |
| **Works on Objects?** | Yes, but must override `equals()` and `hashCode()` |
| **Best for Filtering?** | Simple lists (`String`, `Integer`), but custom logic needed for objects |
| **Performance Tip** | Use **`toMap()`** for filtering by custom fields |

### **Advanced Use Cases Combining `distinct()` with Other Stream Operations**

The `distinct()` method can be combined with `filter()`, `map()`, `flatMap()`, `sorted()`, and `collect()` to handle **complex data processing scenarios** in **e-commerce, banking, HR, and analytics**.

---

### **1. Removing Duplicate Customers and Filtering VIP Customers**
#### **Scenario**  
An e-commerce store wants to:
1. **Remove duplicate customers** (customers with the same email).  
2. **Filter VIP customers** (those who have spent more than $1000).  
3. **Sort them by total spent (highest first).**  
4. **Extract only customer names.**  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.Objects;

class Customer {
    String name;
    String email;
    double totalSpent;

    public Customer(String name, String email, double totalSpent) {
        this.name = name;
        this.email = email;
        this.totalSpent = totalSpent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(email, customer.email); // Compare by email
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}

public class DistinctCustomersExample {
    public static void main(String[] args) {
        List<Customer> customers = List.of(
                new Customer("Alice", "alice@example.com", 1200.0),
                new Customer("Bob", "bob@example.com", 450.0),
                new Customer("Charlie", "charlie@example.com", 1500.0),
                new Customer("Alice Dup", "alice@example.com", 900.0), // Duplicate email
                new Customer("Eve", "eve@example.com", 800.0),
                new Customer("Frank", "frank@example.com", 2000.0)
        );

        List<String> vipCustomers = customers.stream()
                .distinct() // Remove duplicate emails
                .filter(c -> c.totalSpent > 1000) // Filter VIP customers
                .sorted(Comparator.comparing(Customer::totalSpent).reversed()) // Sort by spending
                .map(c -> c.name) // Extract names
                .collect(Collectors.toList());

        System.out.println("VIP Customers: " + vipCustomers);
    }
}
```
#### **Output**
```
VIP Customers: [Frank, Charlie, Alice]
```
- **Removes duplicates**, **filters**, **sorts**, and **maps names** in a single stream pipeline.

---

### **2. Extracting Unique Purchased Items Across Orders**
#### **Scenario**  
A retail store wants to:
1. **Extract all purchased products across orders**.  
2. **Flatten the nested lists**.  
3. **Remove duplicates**.  
4. **Sort alphabetically**.  

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Order {
    List<String> items;

    public Order(List<String> items) {
        this.items = items;
    }

    public List<String> getItems() {
        return items;
    }
}

public class DistinctProductsExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(List.of("Laptop", "Mouse")),
                new Order(List.of("Keyboard", "Monitor")),
                new Order(List.of("Smartphone", "Headphones", "Laptop")),
                new Order(List.of("Mouse", "Tablet"))
        );

        Set<String> uniqueSortedProducts = orders.stream()
                .flatMap(order -> order.getItems().stream()) // Flatten nested lists
                .distinct() // Remove duplicates
                .sorted() // Sort alphabetically
                .collect(Collectors.toSet());

        System.out.println("Unique Purchased Items: " + uniqueSortedProducts);
    }
}
```
#### **Output**
```
Unique Purchased Items: [Headphones, Keyboard, Laptop, Monitor, Mouse, Smartphone, Tablet]
```
- **Uses `flatMap()` to handle nested lists, `distinct()` to remove duplicates, and `sorted()` to order the products**.

---

### **3. Finding Unique Employees Across Departments**
#### **Scenario**  
An HR system wants to:
1. **Extract all employees from multiple departments**.  
2. **Ensure no duplicate employees exist**.  
3. **Sort by department first, then by employee name**.  

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.Objects;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(name, employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

public class DistinctEmployeesExample {
    public static void main(String[] args) {
        List<Employee> employees = List.of(
                new Employee("Alice", "IT"),
                new Employee("Bob", "HR"),
                new Employee("Charlie", "IT"),
                new Employee("Alice", "Finance"), // Duplicate name
                new Employee("David", "HR"),
                new Employee("Eve", "Finance")
        );

        List<Employee> uniqueSortedEmployees = employees.stream()
                .distinct() // Remove duplicate names
                .sorted(Comparator.comparing(Employee::department).thenComparing(Employee::name)) // Sort by department and name
                .collect(Collectors.toList());

        uniqueSortedEmployees.forEach(e -> 
            System.out.println(e.department + " - " + e.name));
    }
}
```
#### **Output**
```
Finance - Eve
HR - Bob
HR - David
IT - Alice
IT - Charlie
```
- **Uses `distinct()` to remove duplicate employees, then sorts by department and name**.

---

### **4. Extracting Unique Transaction Amounts and Calculating the Sum**
#### **Scenario**  
A bank needs to:
1. **Extract unique transaction amounts**.  
2. **Sort them in descending order**.  
3. **Calculate the total sum of distinct transactions**.  

```java
import java.util.List;
import java.util.stream.Collectors;

class Transaction {
    double amount;

    public Transaction(double amount) {
        this.amount = amount;
    }
}

public class DistinctTransactionsExample {
    public static void main(String[] args) {
        List<Transaction> transactions = List.of(
                new Transaction(500.0),
                new Transaction(200.0),
                new Transaction(1000.0),
                new Transaction(500.0), // Duplicate
                new Transaction(300.0),
                new Transaction(1000.0) // Duplicate
        );

        List<Double> uniqueSortedTransactions = transactions.stream()
                .map(t -> t.amount) // Extract amounts
                .distinct() // Remove duplicates
                .sorted((a, b) -> Double.compare(b, a)) // Sort descending
                .collect(Collectors.toList());

        double totalDistinctSum = uniqueSortedTransactions.stream()
                .mapToDouble(Double::doubleValue)
                .sum();

        System.out.println("Unique Sorted Transactions: " + uniqueSortedTransactions);
        System.out.println("Total Sum of Distinct Transactions: $" + totalDistinctSum);
    }
}
```
#### **Output**
```
Unique Sorted Transactions: [1000.0, 500.0, 300.0, 200.0]
Total Sum of Distinct Transactions: $2000.0
```
- **Uses `distinct()` to remove duplicate transactions, `sorted()` for descending order, and `mapToDouble().sum()` for total calculation**.

---

### **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Time Complexity** | `O(n²)` (if using `equals()`), `O(n log n)` (with `sorted()`) |
| **Best for Large Data?** | May be slow if duplicates are frequent |
| **Efficiency Tip** | Use `Collectors.toMap()` for filtering by unique fields |
| **Immutable Streams** | Returns a new stream, does not modify the original collection |

---

### **Summary**
| **Feature** | **Usage** |
|------------|-------------|
| **Removing Duplicates** | `distinct()` |
| **Filtering & Sorting** | `filter().distinct().sorted()` |
| **Flattening Nested Lists** | `flatMap().distinct()` |
| **Removing Object Duplicates** | Override `equals()` & `hashCode()` |
| **Multi-Field Sorting** | `sorted(thenComparing())` |

Would you like **more real-world cases with API or database integration?** 🚀

# What is sorted()?

#### **Introduction**
The **`sorted()`** method in Java **Stream API** is used to **sort elements** in a stream. It provides sorting in **natural order** (for numbers and strings) or allows **custom sorting using a comparator**.

---

### **Key Points**
- **Sorts the elements of a stream** before applying a terminal operation.
- **Two Variants:**
  1. **`sorted()`** → Natural ordering (e.g., ascending order for numbers, alphabetical order for strings).
  2. **`sorted(Comparator<T>)`** → Custom ordering using a comparator.
- **Returns a new Stream** with sorted elements.
- **Does not modify the original collection.**
- **Stable sorting** (preserves order for equal elements).

---

### **Syntax**
```java
Stream<T> sortedStream = originalStream.sorted();
Stream<T> sortedStream = originalStream.sorted(Comparator<T>);
```

---

## **1. Sorting Numbers in Natural Order**
```java
import java.util.List;
import java.util.stream.Collectors;

public class SortedNumbersExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(5, 3, 8, 1, 6);

        List<Integer> sortedNumbers = numbers.stream()
                .sorted() // Natural order (ascending)
                .collect(Collectors.toList());

        System.out.println(sortedNumbers);
    }
}
```
#### **Output**
```
[1, 3, 5, 6, 8]
```
- **Sorts numbers in ascending order by default.**

---

## **2. Sorting Strings in Natural Order**
```java
import java.util.List;
import java.util.stream.Collectors;

public class SortedStringsExample {
    public static void main(String[] args) {
        List<String> names = List.of("John", "Alice", "Bob", "Eve");

        List<String> sortedNames = names.stream()
                .sorted() // Natural order (Alphabetical)
                .collect(Collectors.toList());

        System.out.println(sortedNames);
    }
}
```
#### **Output**
```
[Alice, Bob, Eve, John]
```
- **Sorts strings alphabetically (case-sensitive).**

---

## **3. Sorting in Descending Order**
```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

public class SortedDescendingExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(5, 3, 8, 1, 6);

        List<Integer> sortedDescending = numbers.stream()
                .sorted(Comparator.reverseOrder()) // Descending order
                .collect(Collectors.toList());

        System.out.println(sortedDescending);
    }
}
```
#### **Output**
```
[8, 6, 5, 3, 1]
```
- **Uses `Comparator.reverseOrder()` for descending sorting.**

---

## **4. Sorting Objects Using a Custom Comparator**
By default, `sorted()` cannot sort objects unless we **define a comparator**.

#### **Scenario**:  
Sort **products** by **price (ascending and descending).**

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class SortedObjectsExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0),
                new Product("Smartphone", 800.0)
        );

        // Sorting by price in ascending order
        List<Product> sortedByPriceAsc = products.stream()
                .sorted(Comparator.comparing(p -> p.price))
                .collect(Collectors.toList());

        // Sorting by price in descending order
        List<Product> sortedByPriceDesc = products.stream()
                .sorted(Comparator.comparing(Product::getPrice).reversed())
                .collect(Collectors.toList());

        System.out.println("Sorted by Price (Ascending):");
        sortedByPriceAsc.forEach(p -> System.out.println(p.name + " - $" + p.price));

        System.out.println("\nSorted by Price (Descending):");
        sortedByPriceDesc.forEach(p -> System.out.println(p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Sorted by Price (Ascending):
Mouse - $30.0
Keyboard - $70.0
Smartphone - $800.0
Laptop - $1200.0

Sorted by Price (Descending):
Laptop - $1200.0
Smartphone - $800.0
Keyboard - $70.0
Mouse - $30.0
```
- **Uses `Comparator.comparing(Product::getPrice)` to sort by a specific field.**
- **Applies `.reversed()` for descending order.**

---

## **5. Sorting by Multiple Fields**
#### **Scenario**:  
Sort **products by category first, then by price.**

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }
}

public class MultiFieldSortingExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", "Electronics", 1200.0),
                new Product("Mouse", "Electronics", 30.0),
                new Product("Keyboard", "Electronics", 70.0),
                new Product("Sofa", "Furniture", 500.0),
                new Product("Table", "Furniture", 300.0)
        );

        List<Product> sortedByCategoryThenPrice = products.stream()
                .sorted(Comparator.comparing(Product::getCategory)
                        .thenComparing(Product::getPrice)) // Sort by category, then by price
                .collect(Collectors.toList());

        System.out.println("Sorted by Category & Price:");
        sortedByCategoryThenPrice.forEach(p -> 
            System.out.println(p.category + " - " + p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Sorted by Category & Price:
Electronics - Mouse - $30.0
Electronics - Keyboard - $70.0
Electronics - Laptop - $1200.0
Furniture - Table - $300.0
Furniture - Sofa - $500.0
```
- **Sorts by category first, then by price within each category.**
- **Uses `thenComparing()` for multi-field sorting.**

---

## **6. Sorting with Case-Insensitive Comparison**
```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

public class CaseInsensitiveSortingExample {
    public static void main(String[] args) {
        List<String> names = List.of("john", "Alice", "bob", "eve");

        List<String> sortedNames = names.stream()
                .sorted(String.CASE_INSENSITIVE_ORDER)
                .collect(Collectors.toList());

        System.out.println(sortedNames);
    }
}
```
#### **Output**
```
[Alice, bob, eve, john]
```
- **Sorts names without considering case sensitivity.**

---

## **Performance Considerations**
| **Aspect**         | **Behavior** |
|-------------------|-------------|
| **Time Complexity** | `O(n log n)` (uses TimSort, efficient for large data) |
| **Sorting Stability** | Stable (preserves order of equal elements) |
| **Immutable Streams** | Returns a new stream, does not modify original collection |
| **Memory Usage** | Sorting large data sets may increase memory consumption |

---

## **Summary**
| **Feature** | **Behavior** |
|------------|-------------|
| **Default Sorting** | Uses **natural order** (ascending for numbers, alphabetical for strings) |
| **Custom Sorting** | Uses **Comparator** to define sorting logic |
| **Descending Order** | Uses `Comparator.reverseOrder()` |
| **Sorting Objects** | Uses `Comparator.comparing()` for sorting by fields |
| **Multi-Field Sorting** | Uses `thenComparing()` to sort by multiple attributes |
| **Case-Insensitive Sorting** | Uses `String.CASE_INSENSITIVE_ORDER` |

### **Advanced Use Cases Combining `sorted()` with Other Stream Operations**

Combining `sorted()` with **`filter()`**, **`map()`**, **`distinct()`**, **`flatMap()`**, and **`collect()`** allows powerful data processing pipelines in Java Stream API. Here are complex examples demonstrating sorting in real-world scenarios.

---

### **1. Sorting Customers by Total Purchases and Filtering VIP Customers**
#### **Scenario**  
A retail store wants to:
1. **Filter** customers who have spent more than **$500**.  
2. **Sort** them by **total purchase amount in descending order**.  
3. **Extract customer names** for display.  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Customer {
    String name;
    double totalSpent;

    public Customer(String name, double totalSpent) {
        this.name = name;
        this.totalSpent = totalSpent;
    }

    public double getTotalSpent() {
        return totalSpent;
    }
}

public class SortedCustomersExample {
    public static void main(String[] args) {
        List<Customer> customers = List.of(
                new Customer("Alice", 1200.0),
                new Customer("Bob", 450.0),
                new Customer("Charlie", 800.0),
                new Customer("David", 300.0),
                new Customer("Eve", 1000.0)
        );

        List<String> vipCustomers = customers.stream()
                .filter(c -> c.getTotalSpent() > 500) // Filter VIP customers
                .sorted(Comparator.comparing(Customer::getTotalSpent).reversed()) // Sort by total spent (desc)
                .map(c -> c.name) // Extract names
                .collect(Collectors.toList());

        System.out.println("VIP Customers: " + vipCustomers);
    }
}
```
#### **Output**
```
VIP Customers: [Alice, Eve, Charlie]
```
- **Combines `filter()`, `sorted()`, and `map()`** to process customer data.

---

### **2. Sorting Orders by Purchase Date and Flattening Product Lists**
#### **Scenario**  
An e-commerce store wants to:
1. **Sort orders** by date (newest first).  
2. **Extract and flatten purchased items** from orders.  
3. **Remove duplicates** and collect the unique products.  

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.time.LocalDate;

class Order {
    LocalDate date;
    List<String> items;

    public Order(LocalDate date, List<String> items) {
        this.date = date;
        this.items = items;
    }
}

public class SortedOrdersExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(LocalDate.of(2024, 1, 10), List.of("Laptop", "Mouse")),
                new Order(LocalDate.of(2024, 1, 5), List.of("Keyboard", "Monitor")),
                new Order(LocalDate.of(2024, 1, 12), List.of("Smartphone", "Headphones", "Laptop"))
        );

        Set<String> uniqueSortedItems = orders.stream()
                .sorted(Comparator.comparing(o -> o.date, Comparator.reverseOrder())) // Sort by date (newest first)
                .flatMap(order -> order.items.stream()) // Flatten product lists
                .distinct() // Remove duplicates
                .collect(Collectors.toSet()); // Collect unique items

        System.out.println("Unique Products Ordered: " + uniqueSortedItems);
    }
}
```
#### **Output**
```
Unique Products Ordered: [Smartphone, Headphones, Laptop, Mouse, Keyboard, Monitor]
```
- **Combines `sorted()`, `flatMap()`, and `distinct()`** to process order data efficiently.

---

### **3. Sorting Employees by Department and Salary**
#### **Scenario**  
An HR system needs to:
1. **Sort employees by department** (alphabetical).  
2. **Within each department, sort by salary in descending order**.  
3. **Extract employee names for display**.  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }
}

public class SortedEmployeesExample {
    public static void main(String[] args) {
        List<Employee> employees = List.of(
                new Employee("Alice", "IT", 90000),
                new Employee("Bob", "HR", 70000),
                new Employee("Charlie", "IT", 100000),
                new Employee("David", "Finance", 85000),
                new Employee("Eve", "HR", 75000)
        );

        List<String> sortedEmployees = employees.stream()
                .sorted(Comparator.comparing(Employee::department)
                        .thenComparing(Employee::salary, Comparator.reverseOrder())) // Sort by department, then by salary (desc)
                .map(e -> e.name + " (" + e.department + ") - $" + e.salary) // Extract names
                .collect(Collectors.toList());

        System.out.println("Sorted Employees:\n" + sortedEmployees);
    }
}
```
#### **Output**
```
Sorted Employees:
[David (Finance) - $85000, Bob (HR) - $70000, Eve (HR) - $75000, Charlie (IT) - $100000, Alice (IT) - $90000]
```
- **Uses `thenComparing()`** for multi-level sorting.

---

### **4. Sorting Transactions and Calculating Cumulative Sum**
#### **Scenario**  
A bank needs to:
1. **Sort transactions by amount (largest first).**  
2. **Calculate cumulative transaction total.**  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.stream.IntStream;

class Transaction {
    String id;
    double amount;

    public Transaction(String id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class SortedTransactionsExample {
    public static void main(String[] args) {
        List<Transaction> transactions = List.of(
                new Transaction("TXN001", 1500.0),
                new Transaction("TXN002", 3000.0),
                new Transaction("TXN003", 500.0),
                new Transaction("TXN004", 2000.0)
        );

        List<String> sortedWithCumulativeTotal = IntStream.range(0, transactions.size())
                .mapToObj(i -> transactions.stream()
                        .sorted(Comparator.comparing(Transaction::amount).reversed()) // Sort transactions by amount (desc)
                        .collect(Collectors.toList()))
                .flatMap(List::stream)
                .collect(Collectors.toList())
                .stream()
                .map(t -> t.id + " - $" + t.amount)
                .collect(Collectors.toList());

        System.out.println("Sorted Transactions:\n" + sortedWithCumulativeTotal);
    }
}
```
#### **Output**
```
Sorted Transactions:
[TXN002 - $3000.0, TXN004 - $2000.0, TXN001 - $1500.0, TXN003 - $500.0]
```
- **Uses `mapToObj()` and `flatMap()` to create a cumulative transaction list.**

---

### **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Time Complexity** | `O(n log n)` (TimSort, efficient for large data) |
| **Sorting Stability** | Stable (preserves order of equal elements) |
| **Chaining Operations** | Efficient when filtering before sorting |
| **Parallel Streams** | Can improve sorting for large datasets |

---

### **Summary**
| **Feature** | **Behavior** |
|------------|-------------|
| **Sorting Numbers** | `sorted()` for ascending, `sorted(Comparator.reverseOrder())` for descending |
| **Sorting Objects** | `sorted(Comparator.comparing(Field))` for field-based sorting |
| **Sorting by Multiple Fields** | `thenComparing()` for multi-level sorting |
| **Filtering & Sorting** | `filter()` before `sorted()` improves performance |
| **Flattening & Sorting** | `flatMap()` before `sorted()` helps with nested structures |

Would you like **more real-world cases with database interactions?** 🚀

# What is skip()?

#### **Introduction**  
The **`skip(n)`** method in Java **Stream API** is used to **discard the first `n` elements** from a stream and return the remaining elements. It is useful for **pagination, filtering unwanted records, and limiting large datasets**.

---

### **Key Points**
- **Removes the first `n` elements** from a stream.
- **Returns a new Stream** with remaining elements.
- **Does not modify the original collection.**
- **If `n` is greater than the stream size**, an **empty stream is returned**.
- **Commonly used with `limit(n)` for pagination.**

---

### **Syntax**
```java
Stream<T> remainingStream = originalStream.skip(n);
```
- **`originalStream`** → The original Stream.
- **`n`** → Number of elements to skip.
- **`remainingStream`** → New stream with skipped elements.

---

## **1. Skipping First `n` Elements from a List**
#### **Example: Skipping the First 3 Numbers**
```java
import java.util.List;
import java.util.stream.Collectors;

public class SkipExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 20, 30, 40, 50, 60);

        List<Integer> remainingNumbers = numbers.stream()
                .skip(3) // Skip first 3 elements
                .collect(Collectors.toList());

        System.out.println(remainingNumbers);
    }
}
```
#### **Output**
```
[40, 50, 60]
```
- **Removes first 3 elements (`10, 20, 30`)** and keeps the rest.

---

## **2. Skipping More Elements Than Available**
#### **Example: Skipping More Than List Size**
```java
import java.util.List;
import java.util.stream.Collectors;

public class SkipEmptyExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 20, 30);

        List<Integer> remainingNumbers = numbers.stream()
                .skip(5) // Skip more than available elements
                .collect(Collectors.toList());

        System.out.println(remainingNumbers);
    }
}
```
#### **Output**
```
[]
```
- **Since `skip(5)` is greater than the list size (3), the result is an empty list.**

---

## **3. Skipping Elements in a Sorted List**
#### **Scenario**:  
Sort the numbers first and **skip the smallest 2**.
```java
import java.util.List;
import java.util.stream.Collectors;

public class SkipSortedExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(50, 20, 40, 10, 30);

        List<Integer> result = numbers.stream()
                .sorted() // Sort numbers first
                .skip(2) // Skip smallest 2 numbers
                .collect(Collectors.toList());

        System.out.println(result);
    }
}
```
#### **Output**
```
[30, 40, 50]
```
- **Sorts first, then skips the two smallest numbers (`10, 20`).**

---

## **4. Skipping First `n` Products in an E-Commerce Store**
#### **Scenario**:  
Skip the **first 2 cheapest products** after sorting.

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class SkipProductsExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1200.0),
                new Product("Mouse", 30.0),
                new Product("Keyboard", 70.0),
                new Product("Monitor", 250.0)
        );

        List<Product> expensiveProducts = products.stream()
                .sorted(Comparator.comparing(Product::price)) // Sort by price (ascending)
                .skip(2) // Skip cheapest 2 products
                .collect(Collectors.toList());

        expensiveProducts.forEach(p -> System.out.println(p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Monitor - $250.0
Laptop - $1200.0
```
- **Sorts by price, then removes the first two cheapest items (`Mouse` and `Keyboard`).**

---

## **5. Using `skip()` for Pagination**
#### **Scenario**:  
An API needs to **fetch page 2 of products** (3 products per page).
```java
import java.util.List;
import java.util.stream.Collectors;

public class SkipPaginationExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Monitor", "Phone", "Tablet");

        int pageNumber = 2; // Page number (1-based)
        int pageSize = 3; // Number of products per page

        List<String> paginatedProducts = products.stream()
                .skip((pageNumber - 1) * pageSize) // Skip products from previous pages
                .limit(pageSize) // Limit to page size
                .collect(Collectors.toList());

        System.out.println("Page " + pageNumber + " Products: " + paginatedProducts);
    }
}
```
#### **Output**
```
Page 2 Products: [Monitor, Phone, Tablet]
```
- **Calculates `skip(n) = (pageNumber - 1) * pageSize` to get the correct page.**
- **Uses `limit(pageSize)` to fetch only the required number of items.**

---

## **6. Skipping First Few Transactions in a Bank Statement**
#### **Scenario**:  
A bank **wants to ignore the first 5 transactions** and show only the latest ones.
```java
import java.util.List;
import java.util.stream.Collectors;

class Transaction {
    String id;
    double amount;

    public Transaction(String id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class SkipTransactionsExample {
    public static void main(String[] args) {
        List<Transaction> transactions = List.of(
                new Transaction("TXN001", 100.0),
                new Transaction("TXN002", 250.0),
                new Transaction("TXN003", 500.0),
                new Transaction("TXN004", 800.0),
                new Transaction("TXN005", 150.0),
                new Transaction("TXN006", 400.0),
                new Transaction("TXN007", 700.0)
        );

        List<Transaction> latestTransactions = transactions.stream()
                .skip(5) // Skip first 5 transactions
                .collect(Collectors.toList());

        latestTransactions.forEach(t -> System.out.println(t.id + " - $" + t.amount));
    }
}
```
#### **Output**
```
TXN006 - $400.0
TXN007 - $700.0
```
- **Skips first 5 transactions and returns the latest ones.**

---

## **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Time Complexity** | `O(n)`, since it must iterate over `n` elements |
| **Memory Usage** | Low, since it processes elements lazily |
| **Works on Infinite Streams?** | Yes, useful for streaming large datasets |
| **Best Use Cases** | Pagination, batch processing, filtering old data |

---

## **Summary**
| **Feature** | **Behavior** |
|------------|-------------|
| **Removes First `n` Elements** | `skip(n)` |
| **Returns Remaining Elements** | New Stream |
| **Does Not Modify Original Collection** | Immutable |
| **Used with Pagination** | `skip(pageSize * (page - 1))` |
| **Used with Sorting** | `sorted().skip(n)` to remove smallest values |
| **Best for Large Streams** | Works well for skipping unnecessary elements |

### **Advanced Use Cases Combining `skip()` with Other Stream Operations**  

The **`skip()`** method is powerful when combined with other Stream API methods like **`filter()`**, **`map()`**, **`flatMap()`**, **`sorted()`**, **`limit()`**, and **`collect()`**. Below are **complex real-world scenarios** using `skip()` effectively.

## **1. Paginating Sorted Orders with Filters**
#### **Scenario**  
An e-commerce application:
1. **Filters** only **completed** orders.  
2. **Sorts orders** by **amount (highest first)**.  
3. **Uses pagination** (skip previous pages, limit results per page).  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Order {
    int id;
    double amount;
    String status;

    public Order(int id, double amount, String status) {
        this.id = id;
        this.amount = amount;
        this.status = status;
    }
}

public class SkipOrdersExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(101, 1200.0, "Completed"),
                new Order(102, 500.0, "Pending"),
                new Order(103, 750.0, "Completed"),
                new Order(104, 300.0, "Cancelled"),
                new Order(105, 900.0, "Completed"),
                new Order(106, 1500.0, "Completed"),
                new Order(107, 400.0, "Completed")
        );

        int page = 2; // Page number
        int pageSize = 2; // Items per page

        List<Order> paginatedOrders = orders.stream()
                .filter(o -> o.status.equals("Completed")) // Filter only completed orders
                .sorted(Comparator.comparing(Order::amount).reversed()) // Sort by amount (desc)
                .skip((page - 1) * pageSize) // Skip previous pages
                .limit(pageSize) // Limit to page size
                .collect(Collectors.toList());

        paginatedOrders.forEach(o -> 
            System.out.println("Order ID: " + o.id + ", Amount: $" + o.amount));
    }
}
```
#### **Output**
```
Order ID: 1200.0
Order ID: 900.0
```
- **Filters**, **sorts**, and **paginates** completed orders in a single pipeline.

## **2. Skipping the First Two Expensive Products and Extracting Names**
#### **Scenario**  
A store wants to:
1. **Filter only products above $500**.  
2. **Sort them by price (descending).**  
3. **Skip the first two most expensive products**.  
4. **Extract only product names**.  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class SkipExpensiveProductsExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1500.0),
                new Product("Smartphone", 1200.0),
                new Product("Tablet", 800.0),
                new Product("Monitor", 600.0),
                new Product("Mouse", 50.0)
        );

        List<String> affordableProducts = products.stream()
                .filter(p -> p.price > 500) // Filter products above $500
                .sorted(Comparator.comparing(Product::price).reversed()) // Sort by price (desc)
                .skip(2) // Skip the two most expensive
                .map(p -> p.name) // Extract names
                .collect(Collectors.toList());

        System.out.println("Affordable Products: " + affordableProducts);
    }
}
```
#### **Output**
```
Affordable Products: [Tablet, Monitor]
```
- **Filters, sorts, skips, and extracts product names efficiently.**

---

## **3. Extracting Unique Employee Names After Skipping First Few**
#### **Scenario**  
An HR system:
1. **Flattens a list of employees from multiple departments**.  
2. **Removes duplicates**.  
3. **Sorts by name**.  
4. **Skips the first 3 names** (for paging).  

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class SkipUniqueEmployeesExample {
    public static void main(String[] args) {
        List<List<Employee>> departments = List.of(
                List.of(new Employee("Alice", "IT"), new Employee("Bob", "HR")),
                List.of(new Employee("Charlie", "Finance"), new Employee("Alice", "IT")), // Duplicate
                List.of(new Employee("David", "HR"), new Employee("Eve", "Finance"))
        );

        List<String> uniqueSortedEmployees = departments.stream()
                .flatMap(List::stream) // Flatten department lists
                .map(e -> e.name) // Extract names
                .distinct() // Remove duplicate names
                .sorted() // Sort alphabetically
                .skip(3) // Skip first 3 names
                .collect(Collectors.toList());

        System.out.println("Remaining Employees: " + uniqueSortedEmployees);
    }
}
```
#### **Output**
```
Remaining Employees: [David, Eve]
```
- **Flattens, removes duplicates, sorts, and skips names in a single pipeline.**


## **4. Skipping the Oldest Transactions After Sorting**
#### **Scenario**  
A bank wants to:
1. **Sort transactions by date (oldest first)**.  
2. **Skip the first 5 transactions**.  
3. **Return only the latest ones.**  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.time.LocalDate;

class Transaction {
    String id;
    LocalDate date;
    double amount;

    public Transaction(String id, LocalDate date, double amount) {
        this.id = id;
        this.date = date;
        this.amount = amount;
    }
}

public class SkipOldTransactionsExample {
    public static void main(String[] args) {
        List<Transaction> transactions = List.of(
                new Transaction("TXN001", LocalDate.of(2024, 1, 5), 100.0),
                new Transaction("TXN002", LocalDate.of(2024, 1, 10), 200.0),
                new Transaction("TXN003", LocalDate.of(2024, 1, 15), 300.0),
                new Transaction("TXN004", LocalDate.of(2024, 1, 20), 400.0),
                new Transaction("TXN005", LocalDate.of(2024, 1, 25), 500.0),
                new Transaction("TXN006", LocalDate.of(2024, 2, 1), 600.0),
                new Transaction("TXN007", LocalDate.of(2024, 2, 5), 700.0)
        );

        List<Transaction> latestTransactions = transactions.stream()
                .sorted(Comparator.comparing(Transaction::date)) // Sort by date (oldest first)
                .skip(5) // Skip first 5 transactions
                .collect(Collectors.toList());

        latestTransactions.forEach(t -> 
            System.out.println(t.id + " - Date: " + t.date + " - Amount: $" + t.amount));
    }
}
```
#### **Output**
```
TXN006 - Date: 2024-02-01 - Amount: $600.0
TXN007 - Date: 2024-02-05 - Amount: $700.0
```
- **Sorts transactions chronologically and skips the first five.**

---

### **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Time Complexity** | `O(n)` (since it must iterate over `n` elements) |
| **Memory Usage** | Low, since it processes elements lazily |
| **Best Use Cases** | Pagination, batch processing, filtering old data |
| **Works on Infinite Streams?** | Yes, useful for processing large datasets |

---

### **Summary**
| **Feature** | **Usage** |
|------------|-------------|
| **Skipping Elements** | `skip(n)` |
| **Pagination** | `skip(pageSize * (page - 1))` |
| **Skipping in Sorted Data** | `sorted().skip(n)` |
| **Combining with Filtering** | `filter().sorted().skip(n)` |
| **Flattening Nested Data** | `flatMap().distinct().sorted().skip(n)` |

Would you like **more real-world database integration examples?** 🚀

# What is limit()?
### **`limit()` in Java Stream API**  

#### **Introduction**  
The **`limit(n)`** method in Java **Stream API** is used to **restrict** the number of elements in a stream. It is particularly useful for **pagination, batch processing, and optimizing performance** by **processing only a fixed number of elements**.

---

### **Key Points**
- **Keeps only the first `n` elements** of a stream.
- **Returns a new Stream** with at most `n` elements.
- **Does not modify the original collection.**
- **Works well with `sorted()`, `filter()`, and `skip()` for pagination.**
- **If `n` is greater than the stream size**, returns the full stream.
- **Works on infinite streams**, preventing them from running indefinitely.

---

### **Syntax**
```java
Stream<T> limitedStream = originalStream.limit(n);
```
- **`originalStream`** → The original Stream.
- **`n`** → Number of elements to keep.
- **`limitedStream`** → New stream with at most `n` elements.

---

## **1. Limiting a List to First `n` Elements**
#### **Example: Keeping Only the First 3 Numbers**
```java
import java.util.List;
import java.util.stream.Collectors;

public class LimitExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 20, 30, 40, 50, 60);

        List<Integer> limitedNumbers = numbers.stream()
                .limit(3) // Keep only first 3 elements
                .collect(Collectors.toList());

        System.out.println(limitedNumbers);
    }
}
```
#### **Output**
```
[10, 20, 30]
```
- **Extracts only the first 3 numbers from the list.**

---

## **2. Limiting More Than Available Elements**
#### **Example: Using `limit(n)` When `n` is Greater Than the List Size**
```java
import java.util.List;
import java.util.stream.Collectors;

public class LimitMoreThanSizeExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 20, 30);

        List<Integer> limitedNumbers = numbers.stream()
                .limit(5) // Limit more than available elements
                .collect(Collectors.toList());

        System.out.println(limitedNumbers);
    }
}
```
#### **Output**
```
[10, 20, 30]
```
- **If `limit(n)` is larger than the list size, it returns all elements.**

---

## **3. Limiting After Sorting**
#### **Scenario**:  
Sort the numbers first and **take the top 3 smallest values**.
```java
import java.util.List;
import java.util.stream.Collectors;

public class LimitSortedExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(50, 20, 40, 10, 30);

        List<Integer> topNumbers = numbers.stream()
                .sorted() // Sort numbers first
                .limit(3) // Keep only the smallest 3 numbers
                .collect(Collectors.toList());

        System.out.println(topNumbers);
    }
}
```
#### **Output**
```
[10, 20, 30]
```
- **Sorts first, then keeps the smallest 3 numbers.**

---

## **4. Using `limit()` in an E-Commerce Store**
#### **Scenario**:  
A store wants to **show only the top 3 cheapest products**.
```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class LimitProductsExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1500.0),
                new Product("Smartphone", 1200.0),
                new Product("Tablet", 800.0),
                new Product("Monitor", 600.0),
                new Product("Mouse", 50.0)
        );

        List<Product> cheapestProducts = products.stream()
                .sorted(Comparator.comparing(Product::price)) // Sort by price (ascending)
                .limit(3) // Keep the top 3 cheapest
                .collect(Collectors.toList());

        cheapestProducts.forEach(p -> System.out.println(p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Mouse - $50.0
Monitor - $600.0
Tablet - $800.0
```
- **Sorts products by price and keeps only the cheapest 3.**

---

## **5. Using `limit()` for Pagination**
#### **Scenario**:  
Fetch **page 1 of products** (3 products per page).
```java
import java.util.List;
import java.util.stream.Collectors;

public class LimitPaginationExample {
    public static void main(String[] args) {
        List<String> products = List.of("Laptop", "Mouse", "Keyboard", "Monitor", "Phone", "Tablet");

        int pageNumber = 1; // Page number (1-based)
        int pageSize = 3; // Number of products per page

        List<String> paginatedProducts = products.stream()
                .skip((pageNumber - 1) * pageSize) // Skip previous pages
                .limit(pageSize) // Limit to page size
                .collect(Collectors.toList());

        System.out.println("Page " + pageNumber + " Products: " + paginatedProducts);
    }
}
```
#### **Output**
```
Page 1 Products: [Laptop, Mouse, Keyboard]
```
- **Calculates `skip(n) = (pageNumber - 1) * pageSize` to get the correct page.**
- **Uses `limit(pageSize)` to fetch only the required number of items.**

---

## **6. Limiting an Infinite Stream**
#### **Scenario**:  
Generate an **infinite sequence of numbers**, but **keep only the first 5**.
```java
import java.util.stream.Stream;

public class LimitInfiniteStreamExample {
    public static void main(String[] args) {
        Stream.iterate(1, n -> n + 1) // Infinite numbers starting from 1
                .limit(5) // Stop after 5 numbers
                .forEach(System.out::println);
    }
}
```
#### **Output**
```
1
2
3
4
5
```
- **Prevents infinite execution by limiting the stream.**

---

## **7. Limiting After Filtering**
#### **Scenario**:  
Show the first **2 employees** earning more than **$50,000**.
```java
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class LimitFilteredExample {
    public static void main(String[] args) {
        List<Employee> employees = List.of(
                new Employee("Alice", 60000.0),
                new Employee("Bob", 45000.0),
                new Employee("Charlie", 70000.0),
                new Employee("David", 30000.0),
                new Employee("Eve", 80000.0)
        );

        List<Employee> topEarners = employees.stream()
                .filter(e -> e.salary > 50000) // Filter salaries > 50,000
                .limit(2) // Keep only first 2 matches
                .collect(Collectors.toList());

        topEarners.forEach(e -> System.out.println(e.name + " - $" + e.salary));
    }
}
```
#### **Output**
```
Alice - $60000.0
Charlie - $70000.0
```
- **Filters first, then keeps only the first 2 matching employees.**

---

## **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Time Complexity** | `O(n)` (iterates over `n` elements) |
| **Memory Usage** | Low, as elements are processed lazily |
| **Works on Infinite Streams?** | Yes, prevents infinite execution |
| **Best Use Cases** | Pagination, limiting large data, performance optimization |

---

## **Summary**
| **Feature** | **Usage** |
|------------|-------------|
| **Limiting Elements** | `limit(n)` |
| **Pagination** | `skip(pageSize * (page - 1)).limit(pageSize)` |
| **Sorting Before Limiting** | `sorted().limit(n)` |
| **Limiting Filtered Data** | `filter().limit(n)` |
| **Using with Infinite Streams** | `Stream.iterate().limit(n)` |

### **Advanced Use Cases Combining `limit()` with Other Stream Operations**

The **`limit()`** method becomes extremely powerful when combined with other Stream API methods like **`filter()`**, **`map()`**, **`flatMap()`**, **`sorted()`**, **`distinct()`**, **`skip()`**, and **`collect()`**. Below are **complex real-world scenarios** where `limit()` optimizes data processing.

---

## **1. Fetching the Top 5 Most Expensive Products After Filtering and Sorting**
#### **Scenario**  
An e-commerce company:
1. **Filters** only products with **stock > 0**.  
2. **Sorts them by price (highest first).**  
3. **Limits to the top 5 most expensive available products.**  
4. **Extracts product names for display.**  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    double price;
    int stock;

    public Product(String name, double price, int stock) {
        this.name = name;
        this.price = price;
        this.stock = stock;
    }
}

public class TopProductsExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1500.0, 10),
                new Product("Smartphone", 1200.0, 5),
                new Product("Tablet", 800.0, 0), // Out of stock
                new Product("Monitor", 600.0, 3),
                new Product("Mouse", 50.0, 20),
                new Product("Keyboard", 70.0, 15),
                new Product("Smartwatch", 250.0, 2),
                new Product("TV", 1800.0, 4)
        );

        List<String> topProducts = products.stream()
                .filter(p -> p.stock > 0) // Filter in-stock products
                .sorted(Comparator.comparing(Product::price).reversed()) // Sort by price (descending)
                .limit(5) // Limit to top 5 expensive products
                .map(p -> p.name) // Extract names
                .collect(Collectors.toList());

        System.out.println("Top 5 Expensive Available Products: " + topProducts);
    }
}
```
#### **Output**
```
Top 5 Expensive Available Products: [TV, Laptop, Smartphone, Monitor, Smartwatch]
```
- **Filters in-stock products, sorts by price, limits to 5, and extracts names efficiently.**

---

## **2. Finding the 3 Earliest Transactions Above $500**
#### **Scenario**  
A banking application:
1. **Filters** transactions greater than **$500**.  
2. **Sorts transactions by date (earliest first).**  
3. **Limits to the first 3 matching transactions.**  

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.time.LocalDate;

class Transaction {
    String id;
    LocalDate date;
    double amount;

    public Transaction(String id, LocalDate date, double amount) {
        this.id = id;
        this.date = date;
        this.amount = amount;
    }
}

public class EarliestTransactionsExample {
    public static void main(String[] args) {
        List<Transaction> transactions = List.of(
                new Transaction("TXN001", LocalDate.of(2024, 1, 10), 1000.0),
                new Transaction("TXN002", LocalDate.of(2024, 1, 5), 250.0), // Below $500
                new Transaction("TXN003", LocalDate.of(2024, 1, 15), 800.0),
                new Transaction("TXN004", LocalDate.of(2024, 1, 3), 600.0),
                new Transaction("TXN005", LocalDate.of(2024, 1, 20), 1500.0),
                new Transaction("TXN006", LocalDate.of(2024, 1, 25), 200.0) // Below $500
        );

        List<Transaction> firstTransactions = transactions.stream()
                .filter(t -> t.amount > 500) // Filter transactions above $500
                .sorted(Comparator.comparing(Transaction::date)) // Sort by date (earliest first)
                .limit(3) // Take only the first 3 matching transactions
                .collect(Collectors.toList());

        firstTransactions.forEach(t -> 
            System.out.println(t.id + " - Date: " + t.date + " - Amount: $" + t.amount));
    }
}
```
#### **Output**
```
TXN004 - Date: 2024-01-03 - Amount: $600.0
TXN001 - Date: 2024-01-10 - Amount: $1000.0
TXN003 - Date: 2024-01-15 - Amount: $800.0
```
- **Filters transactions above $500, sorts by date, and limits the results.**

---

## **3. Extracting Unique Employee Names After Sorting and Limiting**
#### **Scenario**  
An HR system:
1. **Flattens** a list of employees from multiple departments.  
2. **Removes duplicates**.  
3. **Sorts alphabetically**.  
4. **Limits to first 5 employees.**  

```java
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class UniqueEmployeesExample {
    public static void main(String[] args) {
        List<List<Employee>> departments = List.of(
                List.of(new Employee("Alice", "IT"), new Employee("Bob", "HR")),
                List.of(new Employee("Charlie", "Finance"), new Employee("Alice", "IT")), // Duplicate
                List.of(new Employee("David", "HR"), new Employee("Eve", "Finance"))
        );

        List<String> limitedEmployees = departments.stream()
                .flatMap(List::stream) // Flatten department lists
                .map(e -> e.name) // Extract names
                .distinct() // Remove duplicate names
                .sorted() // Sort alphabetically
                .limit(5) // Keep only first 5 unique names
                .collect(Collectors.toList());

        System.out.println("Top 5 Unique Employees: " + limitedEmployees);
    }
}
```
#### **Output**
```
Top 5 Unique Employees: [Alice, Bob, Charlie, David, Eve]
```
- **Flattens, removes duplicates, sorts, and limits names in a single pipeline.**

---

## **4. Fetching the First 5 Distinct Usernames from Logs**
#### **Scenario**  
A system log contains usernames of users logging in multiple times.  
1. **Extracts usernames** from logs.  
2. **Removes duplicate usernames**.  
3. **Sorts alphabetically**.  
4. **Limits to first 5 distinct usernames.**  

```java
import java.util.List;
import java.util.stream.Collectors;

public class UniqueUsernamesExample {
    public static void main(String[] args) {
        List<String> logs = List.of(
                "Alice", "Bob", "Charlie", "Alice", "David", "Eve", "Frank", "Charlie", "George", "Bob"
        );

        List<String> uniqueUsers = logs.stream()
                .distinct() // Remove duplicate usernames
                .sorted() // Sort alphabetically
                .limit(5) // Keep first 5 unique users
                .collect(Collectors.toList());

        System.out.println("Top 5 Unique Users: " + uniqueUsers);
    }
}
```
#### **Output**
```
Top 5 Unique Users: [Alice, Bob, Charlie, David, Eve]
```
- **Handles large system logs efficiently by removing duplicates and limiting output.**

---

## **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Time Complexity** | `O(n)` (processes `n` elements) |
| **Memory Usage** | Low, as elements are processed lazily |
| **Works on Infinite Streams?** | Yes, prevents infinite execution |
| **Best Use Cases** | Pagination, limiting large data, performance optimization |

---

### **Summary**
| **Feature** | **Usage** |
|------------|-------------|
| **Limiting Elements** | `limit(n)` |
| **Pagination** | `skip(pageSize * (page - 1)).limit(pageSize)` |
| **Sorting Before Limiting** | `sorted().limit(n)` |
| **Using with Filtering** | `filter().limit(n)` |
| **Using with Infinite Streams** | `Stream.iterate().limit(n)` |

Would you like **more real-world database integration examples?** 🚀

# What is peek()?
#### **Introduction**  
The **`peek()`** method in Java **Stream API** is an **intermediate operation** that allows performing a side effect (e.g., debugging, logging, modifying elements) **without modifying the stream itself**. It is mainly used for **debugging, monitoring, and logging transformations** in a stream pipeline.

---

### **Key Points**
- **Used for debugging** and performing side-effects while processing a stream.
- **Does not modify** the elements in the stream.
- **Returns the same stream** after performing the operation.
- **Does not trigger execution** (lazy evaluation) unless followed by a **terminal operation**.
- **Useful for logging, debugging, or tracking intermediate results.**

---

### **Syntax**
```java
Stream<T> modifiedStream = originalStream.peek(action);
```
- **`originalStream`** → The original stream.
- **`action`** → A consumer function (`Consumer<T>`) that performs an operation on each element.
- **`modifiedStream`** → The same stream after performing the action.

---

## **1. Using `peek()` for Debugging**
#### **Scenario**  
Track how a stream processes elements **step-by-step**.

```java
import java.util.List;
import java.util.stream.Collectors;

public class PeekDebugExample {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie", "David");

        List<String> processedNames = names.stream()
                .peek(n -> System.out.println("Original: " + n)) // Logging original names
                .map(String::toUpperCase) // Convert to uppercase
                .peek(n -> System.out.println("After Map: " + n)) // Logging after transformation
                .sorted() // Sort alphabetically
                .peek(n -> System.out.println("After Sort: " + n)) // Logging after sorting
                .collect(Collectors.toList()); // Collect final results

        System.out.println("Final List: " + processedNames);
    }
}
```
#### **Output**
```
Original: Alice
Original: Bob
Original: Charlie
Original: David
After Map: ALICE
After Map: BOB
After Map: CHARLIE
After Map: DAVID
After Sort: ALICE
After Sort: BOB
After Sort: CHARLIE
After Sort: DAVID
Final List: [ALICE, BOB, CHARLIE, DAVID]
```
- **Uses `peek()` at multiple stages** to **log the transformation process**.

---

## **2. Using `peek()` for Logging and Filtering**
#### **Scenario**  
Log elements **before and after filtering**.

```java
import java.util.List;
import java.util.stream.Collectors;

public class PeekFilterExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 25, 30, 45, 60, 75, 90);

        List<Integer> filteredNumbers = numbers.stream()
                .peek(n -> System.out.println("Before Filter: " + n)) // Log original elements
                .filter(n -> n % 2 == 0) // Keep only even numbers
                .peek(n -> System.out.println("After Filter: " + n)) // Log after filtering
                .collect(Collectors.toList());

        System.out.println("Final List: " + filteredNumbers);
    }
}
```
#### **Output**
```
Before Filter: 10
Before Filter: 25
Before Filter: 30
Before Filter: 45
Before Filter: 60
Before Filter: 75
Before Filter: 90
After Filter: 10
After Filter: 30
After Filter: 60
After Filter: 90
Final List: [10, 30, 60, 90]
```
- **Logs elements before and after filtering**, helping in debugging **why some elements are removed**.

---

## **3. Using `peek()` to Track Product Price Updates**
#### **Scenario**  
Track **product price increases** in an e-commerce store.

```java
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class PeekProductExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1000.0),
                new Product("Mouse", 25.0),
                new Product("Keyboard", 50.0),
                new Product("Monitor", 300.0)
        );

        List<Product> updatedProducts = products.stream()
                .peek(p -> System.out.println("Before Price Update: " + p.name + " - $" + p.price))
                .map(p -> new Product(p.name, p.price * 1.1)) // Increase price by 10%
                .peek(p -> System.out.println("After Price Update: " + p.name + " - $" + p.price))
                .collect(Collectors.toList());

        System.out.println("Final Product List:");
        updatedProducts.forEach(p -> System.out.println(p.name + " - $" + p.price));
    }
}
```
#### **Output**
```
Before Price Update: Laptop - $1000.0
Before Price Update: Mouse - $25.0
Before Price Update: Keyboard - $50.0
Before Price Update: Monitor - $300.0
After Price Update: Laptop - $1100.0
After Price Update: Mouse - $27.5
After Price Update: Keyboard - $55.0
After Price Update: Monitor - $330.0
Final Product List:
Laptop - $1100.0
Mouse - $27.5
Keyboard - $55.0
Monitor - $330.0
```
- **Logs price updates** for **each product before and after the transformation**.

---

## **4. Using `peek()` for Pagination Debugging**
#### **Scenario**  
An API fetches paginated orders, and we **track pagination steps**.

```java
import java.util.List;
import java.util.stream.Collectors;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class PeekPaginationExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(101, 500.0),
                new Order(102, 1500.0),
                new Order(103, 750.0),
                new Order(104, 1200.0),
                new Order(105, 900.0),
                new Order(106, 2000.0),
                new Order(107, 300.0)
        );

        int page = 2;
        int pageSize = 2;

        List<Order> paginatedOrders = orders.stream()
                .peek(o -> System.out.println("Before Pagination: " + o.id + " - $" + o.amount))
                .sorted((o1, o2) -> Double.compare(o2.amount, o1.amount)) // Sort by amount (desc)
                .skip((page - 1) * pageSize) // Skip previous pages
                .limit(pageSize) // Limit to page size
                .peek(o -> System.out.println("After Pagination: " + o.id + " - $" + o.amount))
                .collect(Collectors.toList());

        System.out.println("Final Paginated Orders:");
        paginatedOrders.forEach(o -> System.out.println(o.id + " - $" + o.amount));
    }
}
```
#### **Output**
```
Before Pagination: 101 - $500.0
Before Pagination: 102 - $1500.0
Before Pagination: 103 - $750.0
Before Pagination: 104 - $1200.0
Before Pagination: 105 - $900.0
Before Pagination: 106 - $2000.0
Before Pagination: 107 - $300.0
After Pagination: 104 - $1200.0
After Pagination: 105 - $900.0
Final Paginated Orders:
104 - $1200.0
105 - $900.0
```
- **Logs elements before and after pagination.**

---

## **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Execution** | Lazy (Runs only if a terminal operation follows) |
| **Best Used For** | Debugging, logging, tracking transformations |
| **Performance Impact** | Minimal when used properly |
| **Avoid For** | Modifying elements (use `map()` instead) |

---

### **Summary**
| **Feature** | **Usage** |
|------------|-------------|
| **Debugging Elements** | `stream.peek(System.out::println)` |
| **Logging Before & After Transformation** | `peek() before and after map()` |
| **Tracking Filtering** | `peek() before and after filter()` |
| **Monitoring Sorting & Pagination** | `sorted().skip().limit().peek()` |

Would you like **more complex use cases with databases or API logs?** 🚀

# What is forEach()?.
#### **Introduction**  
The **`forEach()`** method in Java **Stream API** is a **terminal operation** that iterates over each element in a stream and **performs an action** on each element. It is commonly used for **printing, modifying, and executing logic on stream elements**.

---

### **Key Points**
- **Terminal operation** (Consumes the stream; cannot be reused).
- **Performs an action** (`Consumer<T>` function) on each element.
- **Works on parallel streams**, but order may not be guaranteed.
- **Does not return a new stream**, unlike intermediate operations.
- **Does not modify original collection** but can be used for side-effects.

---

### **Syntax**
```java
stream.forEach(action);
```
- **`action`** → A function (`Consumer<T>`) executed for each element.

---

## **1. Printing Elements in a Stream**
#### **Example: Printing a List of Names**
```java
import java.util.List;

public class ForEachExample {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie", "David");

        names.stream()
             .forEach(name -> System.out.println(name));
    }
}
```
#### **Output**
```
Alice
Bob
Charlie
David
```
- **Iterates over each element and prints it**.

---

## **2. Using `forEach()` with Method Reference**
#### **Example: Simplifying Using Method References**
```java
import java.util.List;

public class ForEachMethodReferenceExample {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie");

        names.stream()
             .forEach(System.out::println); // Using method reference
    }
}
```
#### **Output**
```
Alice
Bob
Charlie
```
- **Uses `System.out::println` as a method reference** instead of a lambda.

---

## **3. Iterating Over Numbers and Applying Operations**
#### **Example: Doubling Numbers**
```java
import java.util.List;

public class ForEachNumbersExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 20, 30, 40, 50);

        numbers.stream()
               .forEach(n -> System.out.println(n * 2));
    }
}
```
#### **Output**
```
20
40
60
80
100
```
- **Multiplies each element by 2 before printing**.

---

## **4. Logging Elements Before Filtering**
#### **Scenario**  
Log elements before and after filtering.

```java
import java.util.List;

public class ForEachFilterExample {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(10, 25, 30, 45, 60, 75, 90);

        numbers.stream()
               .forEach(n -> System.out.println("Before Filter: " + n));

        List<Integer> filteredNumbers = numbers.stream()
                .filter(n -> n % 2 == 0) // Keep only even numbers
                .forEach(n -> System.out.println("After Filter: " + n));
    }
}
```
#### **Output**
```
Before Filter: 10
Before Filter: 25
Before Filter: 30
Before Filter: 45
Before Filter: 60
Before Filter: 75
Before Filter: 90
After Filter: 10
After Filter: 30
After Filter: 60
After Filter: 90
```
- **Logs elements before and after filtering**.

---

## **5. Updating Product Prices in an E-Commerce Store**
#### **Scenario**  
Increase all product prices by **10%**.
```java
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ForEachProductExample {
    public static void main(String[] args) {
        List<Product> products = List.of(
                new Product("Laptop", 1000.0),
                new Product("Mouse", 25.0),
                new Product("Keyboard", 50.0),
                new Product("Monitor", 300.0)
        );

        products.stream()
                .forEach(p -> {
                    double newPrice = p.price * 1.1;
                    System.out.println(p.name + " - Updated Price: $" + newPrice);
                });
    }
}
```
#### **Output**
```
Laptop - Updated Price: $1100.0
Mouse - Updated Price: $27.5
Keyboard - Updated Price: $55.0
Monitor - Updated Price: $330.0
```
- **Processes each product and updates the price**.

---

## **6. Processing Orders and Sending Notifications**
#### **Scenario**  
A system needs to **process orders** and **send notifications**.
```java
import java.util.List;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class ForEachOrdersExample {
    public static void main(String[] args) {
        List<Order> orders = List.of(
                new Order(101, 500.0),
                new Order(102, 1500.0),
                new Order(103, 750.0)
        );

        orders.stream()
              .forEach(o -> System.out.println("Processing Order ID: " + o.id + " - Amount: $" + o.amount));
    }
}
```
#### **Output**
```
Processing Order ID: 101 - Amount: $500.0
Processing Order ID: 102 - Amount: $1500.0
Processing Order ID: 103 - Amount: $750.0
```
- **Simulates processing each order and logging the status**.

---

## **7. Using `forEachOrdered()` with Parallel Streams**
#### **Scenario**  
Ensure elements are **processed in order** when using **parallel streams**.
```java
import java.util.List;

public class ForEachOrderedExample {
    public static void main(String[] args) {
        List<String> names = List.of("Alice", "Bob", "Charlie", "David");

        System.out.println("Using forEach() with parallel stream:");
        names.parallelStream()
             .forEach(System.out::println); // Order not guaranteed

        System.out.println("\nUsing forEachOrdered() with parallel stream:");
        names.parallelStream()
             .forEachOrdered(System.out::println); // Order is guaranteed
    }
}
```
#### **Output (Unordered for `forEach()`, Ordered for `forEachOrdered()`)**
```
Using forEach() with parallel stream:
Bob
Charlie
Alice
David

Using forEachOrdered() with parallel stream:
Alice
Bob
Charlie
David
```
- **`forEach()` does not guarantee order in parallel streams**.
- **`forEachOrdered()` maintains order even in parallel processing**.

---

## **Performance Considerations**
| **Aspect** | **Behavior** |
|------------|-------------|
| **Execution** | Terminal operation (Consumes the stream) |
| **Modification** | Cannot modify elements, only perform side effects |
| **Best Used For** | Debugging, logging, sending notifications |
| **Parallel Processing** | `forEach()` may be unordered; use `forEachOrdered()` to maintain order |

---

## **Summary**
| **Feature** | **Usage** |
|------------|-------------|
| **Basic Iteration** | `stream.forEach(System.out::println)` |
| **Logging Before & After Operations** | `peek()` for debugging, `forEach()` for final processing |
| **Processing Large Datasets** | Use `parallelStream().forEach()` for speed |
| **Ensuring Order in Parallel Streams** | `forEachOrdered()` |

Would you like **more real-world database or API examples?** 🚀

# What is reduce()?
The `reduce()` method in Java 8 Stream API is used to perform a **reduction** on the elements of a stream. A reduction operation takes a sequence of input elements and combines them into a single result.

It is part of **terminal operations** in the Stream API, meaning that once `reduce()` is used, the stream is consumed and cannot be reused.

---
## **Syntax of `reduce()`**
The `reduce()` method has three variants:

1. **`reduce(BinaryOperator<T> accumulator)`**
   ```java
   Optional<T> reduce(BinaryOperator<T> accumulator)
   ```
   - Performs a **reduction** on the elements using an associative **accumulator** function.
   - Returns an `Optional<T>` to handle empty streams safely.

2. **`reduce(T identity, BinaryOperator<T> accumulator)`**
   ```java
   T reduce(T identity, BinaryOperator<T> accumulator)
   ```
   - Uses an **identity value** as the initial value.
   - Ensures a non-`Optional` return type.

3. **`reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner)`**
   ```java
   <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner)
   ```
   - Used in **parallel processing**.
   - Works with a **mutable reduction**.
   - `combiner` is used to combine results from multiple threads.

---

## **1. Using `reduce(BinaryOperator<T> accumulator)`**
This version of `reduce()` applies a binary operation to combine elements and returns an `Optional<T>`.

### **Example: Sum of all numbers**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ReduceExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        Optional<Integer> sum = numbers.stream().reduce((a, b) -> a + b);

        sum.ifPresent(System.out::println); // Output: 150
    }
}
```
### **Explanation**
- `reduce((a, b) -> a + b)` takes two elements and sums them repeatedly.
- Since the stream is not empty, `Optional<Integer>` contains a value.
- `sum.ifPresent(System.out::println);` prints the result.

---
## **2. Using `reduce(T identity, BinaryOperator<T> accumulator)`**
This version uses an **identity element**, which acts as a default value.

### **Example: Sum of all numbers with identity value**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        int sum = numbers.stream().reduce(0, (a, b) -> a + b);

        System.out.println(sum); // Output: 150
    }
}
```
### **Explanation**
- `0` is the **identity value** (neutral element for addition).
- If the stream is empty, it returns `0` instead of `Optional<Integer>`.
- `reduce(0, (a, b) -> a + b)` ensures a non-null return.

### **Example: Finding Maximum Number**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(3, 9, 7, 1, 5);

        int max = numbers.stream().reduce(Integer.MIN_VALUE, (a, b) -> a > b ? a : b);

        System.out.println(max); // Output: 9
    }
}
```
---
## **3. Using `reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner)`**
This version is designed for **parallel processing** and is used in concurrent environments.

### **Example: Finding the Sum using Parallel Stream**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample4 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(2, 4, 6, 8, 10);

        int sum = numbers.parallelStream()
                         .reduce(0, (partialSum, num) -> partialSum + num, Integer::sum);

        System.out.println(sum); // Output: 30
    }
}
```
### **Explanation**
- **Identity:** `0`
- **Accumulator:** `(partialSum, num) -> partialSum + num`
- **Combiner:** `Integer::sum`, which merges results from multiple threads.

---
## **More Examples of `reduce()`**
### **Example: Concatenating Strings**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample5 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Java", "Stream", "Reduce");

        String result = words.stream().reduce("", (a, b) -> a + b);

        System.out.println(result); // Output: JavaStreamReduce
    }
}
```
---
### **Example: Count the total characters in a list of words**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample6 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Java", "Reduce", "Stream");

        int totalChars = words.stream().reduce(0, (sum, word) -> sum + word.length(), Integer::sum);

        System.out.println(totalChars); // Output: 16
    }
}
```
---
### **Example: Multiply all elements**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample7 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        int product = numbers.stream().reduce(1, (a, b) -> a * b);

        System.out.println(product); // Output: 120
    }
}
```
---
### **Example: Find the longest string**
```java
import java.util.Arrays;
import java.util.List;

public class ReduceExample8 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("apple", "banana", "cherry", "blueberry");

        String longest = words.stream()
                              .reduce("", (a, b) -> a.length() > b.length() ? a : b);

        System.out.println(longest); // Output: blueberry
    }
}
```
---
## **Key Points About `reduce()`**
1. **Associative**: The operation should be **associative**, meaning `(a ⊕ b) ⊕ c` should be equal to `a ⊕ (b ⊕ c)`.
2. **Identity Element**:
   - The identity should be a neutral element (`0` for sum, `1` for multiplication).
   - If no identity is provided, the result is `Optional<T>`.
3. **Parallel Processing**:
   - When using `reduce(identity, accumulator, combiner)`, ensure `combiner` is **compatible** with the `accumulator`.
4. **Null Safety**: Using `Optional<T>` prevents `NullPointerException` in empty streams.

---
## **Conclusion**
The `reduce()` method in Java 8 Stream API provides a powerful way to perform **aggregation, accumulation, and transformation** on a stream. It can be used for operations like sum, product, min, max, concatenation, and more.

By choosing the appropriate variant, you can efficiently process data in both **sequential and parallel** streams.

Would you like additional examples or explanations? 🚀

Sure! Let's explore more complex examples of `reduce()` by combining it with other Stream operations like `map()`, `filter()`, `flatMap()`, and `collect()`. These examples will demonstrate advanced use cases and real-world applications of `reduce()`.

---

## **1. Sum of Square of Even Numbers**
### **Combining `filter()`, `map()`, and `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class ComplexReduceExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        int sumOfSquares = numbers.stream()
                                  .filter(n -> n % 2 == 0)  // Select only even numbers
                                  .map(n -> n * n)          // Square each number
                                  .reduce(0, Integer::sum); // Sum them up

        System.out.println(sumOfSquares); // Output: 220 (4+16+36+64+100)
    }
}
```
### **Explanation**
- `filter(n -> n % 2 == 0)`: Selects only even numbers.
- `map(n -> n * n)`: Squares each even number.
- `reduce(0, Integer::sum)`: Sums up all squared values.

---

## **2. Concatenation of Uppercase Words**
### **Combining `map()`, `filter()`, and `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class ComplexReduceExample2 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("hello", "WORLD", "java", "STREAM", "Reduce");

        String result = words.stream()
                             .filter(word -> word.equals(word.toUpperCase())) // Select uppercase words
                             .reduce("", (a, b) -> a + b); // Concatenate

        System.out.println(result); // Output: WORLDSTREAM
    }
}
```
### **Explanation**
- `filter(word -> word.equals(word.toUpperCase()))`: Selects words that are completely uppercase.
- `reduce("", (a, b) -> a + b)`: Concatenates them into a single string.

---

## **3. Find the Longest Word Length**
### **Combining `map()`, `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class ComplexReduceExample3 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("elephant", "cat", "hippopotamus", "dog", "giraffe");

        int longestLength = words.stream()
                                 .map(String::length)  // Convert words to their lengths
                                 .reduce(0, Integer::max); // Find the maximum length

        System.out.println(longestLength); // Output: 12 (hippopotamus)
    }
}
```
### **Explanation**
- `map(String::length)`: Converts each word into its length.
- `reduce(0, Integer::max)`: Finds the maximum length.

---

## **4. Product of Odd Numbers Greater Than 3**
### **Combining `filter()`, `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class ComplexReduceExample4 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 3, 5, 7, 9, 10);

        int product = numbers.stream()
                             .filter(n -> n % 2 != 0 && n > 3) // Select odd numbers greater than 3
                             .reduce(1, (a, b) -> a * b); // Multiply them

        System.out.println(product); // Output: 315 (5 * 7 * 9)
    }
}
```
### **Explanation**
- `filter(n -> n % 2 != 0 && n > 3)`: Selects odd numbers greater than 3.
- `reduce(1, (a, b) -> a * b)`: Multiplies them together.

---

## **5. Counting Words Without Using `count()`**
### **Combining `map()`, `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class ComplexReduceExample5 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("apple", "banana", "cherry", "date");

        int count = words.stream()
                         .map(word -> 1)  // Convert each word to 1
                         .reduce(0, Integer::sum); // Sum up 1s

        System.out.println(count); // Output: 4
    }
}
```
### **Explanation**
- `map(word -> 1)`: Maps each word to the number `1`.
- `reduce(0, Integer::sum)`: Sums all `1`s to count words.

---

## **6. Finding the Most Frequent Character**
### **Using `flatMap()`, `collect()`, and `reduce()`**
```java
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ComplexReduceExample6 {
    public static void main(String[] args) {
        String text = "banana";

        Optional<Map.Entry<Character, Long>> mostFrequentChar = text.chars()
                .mapToObj(c -> (char) c) // Convert int stream to Character stream
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())) // Count occurrences
                .entrySet()
                .stream()
                .reduce((a, b) -> a.getValue() > b.getValue() ? a : b); // Find max frequency

        mostFrequentChar.ifPresent(entry ->
                System.out.println("Most Frequent Character: " + entry.getKey())); // Output: a
    }
}
```
### **Explanation**
1. `text.chars().mapToObj(c -> (char) c)`: Converts `String` into a `Stream<Character>`.
2. `collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))`: Counts occurrences of each character.
3. `.reduce((a, b) -> a.getValue() > b.getValue() ? a : b)`: Finds the character with the maximum count.

---

## **7. Finding the Average Salary of Employees**
### **Combining `map()`, `reduce()`, and `count()`**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ComplexReduceExample7 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 50000),
            new Employee("Bob", 60000),
            new Employee("Charlie", 70000),
            new Employee("David", 80000)
        );

        double totalSalary = employees.stream()
                                      .map(emp -> emp.salary)
                                      .reduce(0.0, Double::sum); // Sum of salaries

        long count = employees.stream().count(); // Count employees

        double averageSalary = count > 0 ? totalSalary / count : 0;

        System.out.println("Average Salary: " + averageSalary); // Output: 65000.0
    }
}
```
### **Explanation**
1. `map(emp -> emp.salary)`: Extracts salary.
2. `reduce(0.0, Double::sum)`: Sums up salaries.
3. `.count()`: Counts employees.
4. Computes average salary: `totalSalary / count`.

---

### **Conclusion**
These advanced examples show how `reduce()` can be used with `filter()`, `map()`, `flatMap()`, and `collect()` to solve **real-world** problems. 

Here are some **real-world eCommerce examples** using `reduce()` in Java 8 Streams. These examples demonstrate how `reduce()` can be used to process **orders, invoices, customer transactions, and product analytics**.

---

## **1. Total Revenue Calculation**
### **Calculate the total revenue from a list of orders**
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class EcommerceReduceExample1 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, 120.50),
            new Order(2, 350.75),
            new Order(3, 200.25),
            new Order(4, 500.00)
        );

        double totalRevenue = orders.stream()
                                    .map(order -> order.amount)  // Extract amounts
                                    .reduce(0.0, Double::sum);   // Sum all amounts

        System.out.println("Total Revenue: $" + totalRevenue); // Output: $1171.50
    }
}
```
### **Explanation**
- Uses `map(order -> order.amount)` to extract order amounts.
- `reduce(0.0, Double::sum)` sums all the order amounts.

---

## **2. Find the Most Expensive Product**
### **Find the product with the highest price**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class EcommerceReduceExample2 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75),
            new Product("Monitor", 300.00)
        );

        Product expensiveProduct = products.stream()
                                           .reduce((p1, p2) -> p1.price > p2.price ? p1 : p2)
                                           .orElse(null);

        if (expensiveProduct != null) {
            System.out.println("Most Expensive Product: " + expensiveProduct.name + " - $" + expensiveProduct.price);
        }
    }
}
```
### **Explanation**
- Uses `reduce((p1, p2) -> p1.price > p2.price ? p1 : p2)` to find the most expensive product.

---

## **3. Calculate Average Order Value (AOV)**
### **Find the average order value for an eCommerce platform**
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class EcommerceReduceExample3 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, 250.50),
            new Order(2, 400.75),
            new Order(3, 150.25),
            new Order(4, 600.00)
        );

        double totalRevenue = orders.stream()
                                    .map(order -> order.amount)
                                    .reduce(0.0, Double::sum);

        long totalOrders = orders.stream().count();

        double averageOrderValue = totalOrders > 0 ? totalRevenue / totalOrders : 0;

        System.out.println("Average Order Value: $" + averageOrderValue);
    }
}
```
### **Explanation**
- Uses `reduce(0.0, Double::sum)` to calculate total revenue.
- Uses `count()` to get the number of orders.
- Computes **AOV** as `totalRevenue / totalOrders`.

---

## **4. Find the Most Frequent Product Category**
### **Identify the most frequently purchased product category**
```java
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class EcommerceReduceExample4 {
    public static void main(String[] args) {
        List<Product> purchases = Arrays.asList(
            new Product("Laptop", "Electronics"),
            new Product("Phone", "Electronics"),
            new Product("Shoes", "Fashion"),
            new Product("Tablet", "Electronics"),
            new Product("T-shirt", "Fashion"),
            new Product("Watch", "Fashion"),
            new Product("Headphones", "Electronics")
        );

        Optional<Map.Entry<String, Long>> mostFrequentCategory = purchases.stream()
            .map(product -> product.category)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .entrySet()
            .stream()
            .reduce((a, b) -> a.getValue() > b.getValue() ? a : b);

        mostFrequentCategory.ifPresent(category ->
            System.out.println("Most Frequent Category: " + category.getKey() + " (" + category.getValue() + " purchases)")
        );
    }
}
```
### **Explanation**
1. Uses `map(product -> product.category)` to extract product categories.
2. Groups by category and counts occurrences.
3. Uses `reduce((a, b) -> a.getValue() > b.getValue() ? a : b)` to find the most frequent category.

---

## **5. Find Customer with Highest Spending**
### **Determine the customer who spent the most**
```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    double totalSpent;

    public Customer(String name, double totalSpent) {
        this.name = name;
        this.totalSpent = totalSpent;
    }
}

public class EcommerceReduceExample5 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", 1200.50),
            new Customer("Bob", 950.75),
            new Customer("Charlie", 1800.25),
            new Customer("David", 1500.00)
        );

        Customer topCustomer = customers.stream()
                                        .reduce((c1, c2) -> c1.totalSpent > c2.totalSpent ? c1 : c2)
                                        .orElse(null);

        if (topCustomer != null) {
            System.out.println("Top Customer: " + topCustomer.name + " - $" + topCustomer.totalSpent);
        }
    }
}
```
### **Explanation**
- Uses `reduce((c1, c2) -> c1.totalSpent > c2.totalSpent ? c1 : c2)` to find the highest spender.

---

## **6. Find the Total Quantity of All Items Sold**
### **Calculate the total number of items sold in an eCommerce store**
```java
import java.util.Arrays;
import java.util.List;

class Item {
    String name;
    int quantitySold;

    public Item(String name, int quantitySold) {
        this.name = name;
        this.quantitySold = quantitySold;
    }
}

public class EcommerceReduceExample6 {
    public static void main(String[] args) {
        List<Item> items = Arrays.asList(
            new Item("Laptop", 5),
            new Item("Phone", 10),
            new Item("Tablet", 7),
            new Item("Headphones", 15)
        );

        int totalItemsSold = items.stream()
                                  .map(item -> item.quantitySold)
                                  .reduce(0, Integer::sum);

        System.out.println("Total Items Sold: " + totalItemsSold); // Output: 37
    }
}
```
### **Explanation**
- Uses `map(item -> item.quantitySold)` to extract quantities.
- Uses `reduce(0, Integer::sum)` to sum all quantities.

---

### **Conclusion**
These **real-world eCommerce examples** of `reduce()` show how it can be used to:
✅ Calculate total revenue, AOV, and most sold items.  
✅ Identify top-spending customers and most popular product categories.  
✅ Process large-scale transactional data efficiently.

Would you like more **custom eCommerce scenarios**, such as inventory analysis, fraud detection, or cart abandonment rates? 🚀

# What is sum()?
The `sum()` method in Java 8 Stream API is used to calculate the **sum of numerical values** in a stream. It is a terminal operation that returns a **primitive** value rather than an `Optional`. This makes it useful for **aggregation operations**.

The `sum()` method is available in three specialized streams:
1. **`IntStream`** → `sum()` for `int` values.
2. **`LongStream`** → `sum()` for `long` values.
3. **`DoubleStream`** → `sum()` for `double` values.

---
## **1. Syntax of `sum()`**
Each primitive stream interface provides a `sum()` method:
```java
int sum();       // For IntStream
long sum();      // For LongStream
double sum();    // For DoubleStream
```

---
## **2. `sum()` with `IntStream`**
### **Example: Sum of integers in a list**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class SumExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        int total = numbers.stream().mapToInt(Integer::intValue).sum();

        System.out.println("Sum: " + total); // Output: 150
    }
}
```
### **Explanation**
1. `mapToInt(Integer::intValue)`: Converts `Stream<Integer>` to `IntStream`.
2. `sum()`: Computes the sum of all numbers.

---
## **3. `sum()` with `LongStream`**
### **Example: Sum of large numbers**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.LongStream;

public class SumExample2 {
    public static void main(String[] args) {
        List<Long> sales = Arrays.asList(100000L, 200000L, 300000L, 400000L);

        long totalSales = sales.stream().mapToLong(Long::longValue).sum();

        System.out.println("Total Sales: $" + totalSales); // Output: 1000000
    }
}
```
### **Explanation**
- `mapToLong(Long::longValue)`: Converts `Stream<Long>` to `LongStream`.
- `sum()`: Computes the sum of all sales values.

---
## **4. `sum()` with `DoubleStream`**
### **Example: Sum of product prices**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;

public class SumExample3 {
    public static void main(String[] args) {
        List<Double> prices = Arrays.asList(199.99, 499.50, 149.75, 99.25);

        double totalPrice = prices.stream().mapToDouble(Double::doubleValue).sum();

        System.out.println("Total Price: $" + totalPrice); // Output: 948.49
    }
}
```
### **Explanation**
- `mapToDouble(Double::doubleValue)`: Converts `Stream<Double>` to `DoubleStream`.
- `sum()`: Computes the total sum of all prices.

---
## **5. `sum()` with `IntStream.range()`**
### **Example: Sum of first 10 natural numbers**
```java
import java.util.stream.IntStream;

public class SumExample4 {
    public static void main(String[] args) {
        int sum = IntStream.rangeClosed(1, 10).sum();
        System.out.println("Sum of first 10 numbers: " + sum); // Output: 55
    }
}
```
### **Explanation**
- `IntStream.rangeClosed(1, 10)`: Generates numbers from 1 to 10.
- `sum()`: Computes their sum.

---
## **6. Sum of Even Numbers**
### **Example: Using `filter()`**
```java
import java.util.Arrays;
import java.util.List;

public class SumExample5 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        int sumEven = numbers.stream()
                             .mapToInt(Integer::intValue)
                             .filter(n -> n % 2 == 0) // Select even numbers
                             .sum();

        System.out.println("Sum of Even Numbers: " + sumEven); // Output: 30
    }
}
```
### **Explanation**
- `filter(n -> n % 2 == 0)`: Selects even numbers.
- `sum()`: Computes their sum.

---
## **7. Sum of Employee Salaries**
### **Example: Using `sum()` in an eCommerce system**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class SumExample6 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 50000),
            new Employee("Bob", 60000),
            new Employee("Charlie", 70000),
            new Employee("David", 80000)
        );

        double totalSalary = employees.stream()
                                      .mapToDouble(emp -> emp.salary)
                                      .sum();

        System.out.println("Total Salary: $" + totalSalary); // Output: 260000
    }
}
```
### **Explanation**
- `mapToDouble(emp -> emp.salary)`: Extracts salaries.
- `sum()`: Computes total salary.

---
## **8. Sum of Product Prices in an Order**
### **Example: Calculating the total cart value**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class SumExample7 {
    public static void main(String[] args) {
        List<Product> cart = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        double totalCartValue = cart.stream()
                                    .mapToDouble(product -> product.price)
                                    .sum();

        System.out.println("Total Cart Value: $" + totalCartValue); // Output: 2452.24
    }
}
```
### **Explanation**
- `mapToDouble(product -> product.price)`: Extracts product prices.
- `sum()`: Computes total cart value.

---
## **9. Sum of Squares**
### **Example: Sum of squares of numbers**
```java
import java.util.Arrays;
import java.util.List;

public class SumExample8 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        int sumOfSquares = numbers.stream()
                                  .mapToInt(n -> n * n)
                                  .sum();

        System.out.println("Sum of Squares: " + sumOfSquares); // Output: 55
    }
}
```
### **Explanation**
- `mapToInt(n -> n * n)`: Computes squares.
- `sum()`: Adds all squares.

---
## **10. Alternative: Using `reduce()` Instead of `sum()`**
### **Example: Sum using `reduce()`**
```java
import java.util.Arrays;
import java.util.List;

public class SumExample9 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        int total = numbers.stream().reduce(0, Integer::sum);

        System.out.println("Sum: " + total); // Output: 15
    }
}
```
---
## **Conclusion**
The `sum()` method in Java 8 Streams is **efficient** and **concise** for summing numerical values. It simplifies computations in:
✅ Arrays  
✅ Collections  
✅ Employee salaries  
✅ Sales reports  
✅ Cart totals  

Would you like more **custom examples** or **performance comparisons**? 🚀

# What is min()?
The `min()` method in Java 8 Stream API is used to **find the minimum element** from a stream based on a **Comparator**. It is a **terminal operation**, meaning that after calling `min()`, the stream cannot be used further.

The `min()` method returns an `Optional<T>`, which helps handle cases where the stream may be empty.

---

## **Syntax of `min()`**  
The `min()` method is available in the Stream API with the following syntax:

```java
Optional<T> min(Comparator<? super T> comparator);
```
- The method takes a **Comparator** to determine the smallest element.
- It returns an `Optional<T>` to avoid `NullPointerException` when the stream is empty.

For **primitive streams**, Java provides specialized methods:
- `IntStream.min()`
- `LongStream.min()`
- `DoubleStream.min()`

These return a primitive value wrapped inside an `OptionalInt`, `OptionalLong`, or `OptionalDouble`.

---

## **Finding Minimum Value from a List of Numbers**  

### **Example: Finding the Minimum Integer in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MinExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 5, 20, 15, 3, 25);

        Optional<Integer> minNumber = numbers.stream().min(Integer::compareTo);

        minNumber.ifPresent(min -> System.out.println("Minimum Number: " + min)); // Output: 3
    }
}
```
### **Explanation**
1. `stream().min(Integer::compareTo)`: Finds the minimum integer in the list.
2. `Optional<Integer>` ensures safe handling if the list is empty.
3. `ifPresent(min -> System.out.println(min))`: Prints the result only if a value is present.

---

## **Finding Minimum in a List of Custom Objects**  

### **Example: Finding the Employee with the Lowest Salary**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class MinExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        Optional<Employee> minSalaryEmployee = employees.stream()
                                                        .min((e1, e2) -> Double.compare(e1.salary, e2.salary));

        minSalaryEmployee.ifPresent(emp -> 
            System.out.println("Employee with Lowest Salary: " + emp.name + " - $" + emp.salary)
        );
    }
}
```
### **Explanation**
- Uses `Double.compare(e1.salary, e2.salary)` to compare salaries.
- Finds the employee with the **lowest** salary.
- Uses `Optional<Employee>` to safely handle empty lists.

---

## **Finding Minimum Value in a Primitive Stream**  

### **Example: Finding the Minimum in an `IntStream`**
```java
import java.util.stream.IntStream;
import java.util.OptionalInt;

public class MinExample3 {
    public static void main(String[] args) {
        OptionalInt minValue = IntStream.of(12, 4, 19, 7, 25).min();

        minValue.ifPresent(min -> System.out.println("Minimum Value: " + min)); // Output: 4
    }
}
```
### **Explanation**
- `IntStream.of(...).min()`: Finds the smallest integer.
- Uses `OptionalInt` to handle empty cases.

---

## **Finding the Minimum Length String in a List**  

### **Example: Finding the Shortest Word in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MinExample4 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Java", "Stream", "Reduce", "API");

        Optional<String> shortestWord = words.stream()
                                             .min((w1, w2) -> Integer.compare(w1.length(), w2.length()));

        shortestWord.ifPresent(word -> System.out.println("Shortest Word: " + word)); // Output: API
    }
}
```
### **Explanation**
- Compares strings based on `length()`.
- Finds the shortest word in the list.

---

## **Finding the Cheapest Product in an eCommerce Application**  

### **Example: Finding the Product with the Lowest Price**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class MinExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75),
            new Product("Monitor", 300.00)
        );

        Optional<Product> cheapestProduct = products.stream()
                                                    .min((p1, p2) -> Double.compare(p1.price, p2.price));

        cheapestProduct.ifPresent(product -> 
            System.out.println("Cheapest Product: " + product.name + " - $" + product.price)
        );
    }
}
```
### **Explanation**
- Uses `Double.compare(p1.price, p2.price)` to compare product prices.
- Finds the cheapest product in the list.

---

## **Finding the Earliest Date in a List of Transactions**  

### **Example: Finding the Oldest Transaction**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.time.LocalDate;

class Transaction {
    String id;
    LocalDate date;

    public Transaction(String id, LocalDate date) {
        this.id = id;
        this.date = date;
    }
}

public class MinExample6 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", LocalDate.of(2023, 5, 10)),
            new Transaction("TXN2", LocalDate.of(2022, 3, 15)),
            new Transaction("TXN3", LocalDate.of(2021, 8, 20)),
            new Transaction("TXN4", LocalDate.of(2023, 1, 5))
        );

        Optional<Transaction> earliestTransaction = transactions.stream()
                                                                .min((t1, t2) -> t1.date.compareTo(t2.date));

        earliestTransaction.ifPresent(transaction -> 
            System.out.println("Earliest Transaction: " + transaction.id + " on " + transaction.date)
        );
    }
}
```
### **Explanation**
- Uses `t1.date.compareTo(t2.date)` to compare transaction dates.
- Finds the **earliest transaction** in the list.

---

## **Alternative Approach: Using `reduce()` Instead of `min()`**  

### **Example: Finding the Minimum Value Using `reduce()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MinExample7 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(15, 22, 8, 19, 5, 30);

        Optional<Integer> minNumber = numbers.stream().reduce((a, b) -> a < b ? a : b);

        minNumber.ifPresent(min -> System.out.println("Minimum Number: " + min)); // Output: 5
    }
}
```
### **Explanation**
- Uses `reduce((a, b) -> a < b ? a : b)` to find the smallest number.

---

## **Conclusion**  
The `min()` method in Java 8 Stream API is a **powerful tool** for:
✅ Finding the **smallest number** in a list.  
✅ Identifying **lowest prices, salaries, and dates**.  
✅ Sorting data **efficiently** with a **Comparator**.  

Would you like additional **custom examples**? 🚀

# What is max()?
The `max()` method in Java 8 Stream API is a **terminal operation** used to find the **maximum element** in a stream based on a given **Comparator**. It is useful for finding **the highest number, most expensive product, highest salary, or latest date** in a dataset.

The `max()` method returns an `Optional<T>`, which helps handle cases where the stream may be empty.

---

## **Syntax of `max()`**  
The `max()` method is available in the Stream API with the following syntax:

```java
Optional<T> max(Comparator<? super T> comparator);
```
- The method takes a **Comparator** to determine the largest element.
- It returns an `Optional<T>` to prevent `NullPointerException` when the stream is empty.

For **primitive streams**, Java provides specialized methods:
- `IntStream.max()`
- `LongStream.max()`
- `DoubleStream.max()`

These return a primitive value wrapped inside an `OptionalInt`, `OptionalLong`, or `OptionalDouble`.

---

## **Finding Maximum Value from a List of Numbers**  

### **Example: Finding the Maximum Integer in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MaxExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 5, 20, 15, 3, 25);

        Optional<Integer> maxNumber = numbers.stream().max(Integer::compareTo);

        maxNumber.ifPresent(max -> System.out.println("Maximum Number: " + max)); // Output: 25
    }
}
```
### **Explanation**
1. `stream().max(Integer::compareTo)`: Finds the maximum integer in the list.
2. `Optional<Integer>` ensures safe handling if the list is empty.
3. `ifPresent(max -> System.out.println(max))`: Prints the result only if a value is present.

---

## **Finding Maximum in a List of Custom Objects**  

### **Example: Finding the Employee with the Highest Salary**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class MaxExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        Optional<Employee> maxSalaryEmployee = employees.stream()
                                                        .max((e1, e2) -> Double.compare(e1.salary, e2.salary));

        maxSalaryEmployee.ifPresent(emp -> 
            System.out.println("Employee with Highest Salary: " + emp.name + " - $" + emp.salary)
        );
    }
}
```
### **Explanation**
- Uses `Double.compare(e1.salary, e2.salary)` to compare salaries.
- Finds the employee with the **highest** salary.
- Uses `Optional<Employee>` to safely handle empty lists.

---

## **Finding Maximum Value in a Primitive Stream**  

### **Example: Finding the Maximum in an `IntStream`**
```java
import java.util.stream.IntStream;
import java.util.OptionalInt;

public class MaxExample3 {
    public static void main(String[] args) {
        OptionalInt maxValue = IntStream.of(12, 4, 19, 7, 25).max();

        maxValue.ifPresent(max -> System.out.println("Maximum Value: " + max)); // Output: 25
    }
}
```
### **Explanation**
- `IntStream.of(...).max()`: Finds the largest integer.
- Uses `OptionalInt` to handle empty cases.

---

## **Finding the Longest String in a List**  

### **Example: Finding the Longest Word in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MaxExample4 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Java", "Stream", "Reduce", "API");

        Optional<String> longestWord = words.stream()
                                            .max((w1, w2) -> Integer.compare(w1.length(), w2.length()));

        longestWord.ifPresent(word -> System.out.println("Longest Word: " + word)); // Output: Stream
    }
}
```
### **Explanation**
- Compares strings based on `length()`.
- Finds the longest word in the list.

---

## **Finding the Most Expensive Product in an eCommerce Application**  

### **Example: Finding the Product with the Highest Price**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class MaxExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75),
            new Product("Monitor", 300.00)
        );

        Optional<Product> mostExpensiveProduct = products.stream()
                                                         .max((p1, p2) -> Double.compare(p1.price, p2.price));

        mostExpensiveProduct.ifPresent(product -> 
            System.out.println("Most Expensive Product: " + product.name + " - $" + product.price)
        );
    }
}
```
### **Explanation**
- Uses `Double.compare(p1.price, p2.price)` to compare product prices.
- Finds the most expensive product in the list.

---

## **Finding the Latest Date in a List of Transactions**  

### **Example: Finding the Most Recent Transaction**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.time.LocalDate;

class Transaction {
    String id;
    LocalDate date;

    public Transaction(String id, LocalDate date) {
        this.id = id;
        this.date = date;
    }
}

public class MaxExample6 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", LocalDate.of(2023, 5, 10)),
            new Transaction("TXN2", LocalDate.of(2022, 3, 15)),
            new Transaction("TXN3", LocalDate.of(2021, 8, 20)),
            new Transaction("TXN4", LocalDate.of(2023, 12, 5))
        );

        Optional<Transaction> latestTransaction = transactions.stream()
                                                             .max((t1, t2) -> t1.date.compareTo(t2.date));

        latestTransaction.ifPresent(transaction -> 
            System.out.println("Latest Transaction: " + transaction.id + " on " + transaction.date)
        );
    }
}
```
### **Explanation**
- Uses `t1.date.compareTo(t2.date)` to compare transaction dates.
- Finds the **most recent transaction** in the list.

---

## **Alternative Approach: Using `reduce()` Instead of `max()`**  

### **Example: Finding the Maximum Value Using `reduce()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MaxExample7 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(15, 22, 8, 19, 5, 30);

        Optional<Integer> maxNumber = numbers.stream().reduce((a, b) -> a > b ? a : b);

        maxNumber.ifPresent(max -> System.out.println("Maximum Number: " + max)); // Output: 30
    }
}
```
### **Explanation**
- Uses `reduce((a, b) -> a > b ? a : b)` to find the largest number.

---

## **Conclusion**  
The `max()` method in Java 8 Stream API is **useful** for:
✅ Finding the **largest number** in a list.  
✅ Identifying **highest prices, salaries, and dates**.  
✅ Sorting data **efficiently** with a **Comparator**.  

Would you like additional **custom examples**? 🚀

# What is average()?
The `average()` method in Java 8 Stream API is a **terminal operation** that calculates the **average (mean) of numerical values** in a stream. It is primarily used with **primitive streams** (`IntStream`, `LongStream`, and `DoubleStream`).

The method returns an `OptionalDouble`, which ensures safe handling in case the stream is empty.

---

## **Syntax of `average()`**  
Each primitive stream interface provides an `average()` method:

```java
OptionalDouble average(); // For IntStream
OptionalDouble average(); // For LongStream
OptionalDouble average(); // For DoubleStream
```
- The method calculates the **arithmetic mean** of the elements in the stream.
- It returns an `OptionalDouble` to handle empty streams safely.

---

## **Finding the Average of a List of Numbers**  

### **Example: Calculating the Average of Integers**
```java
import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.List;

public class AverageExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        OptionalDouble avg = numbers.stream()
                                    .mapToInt(Integer::intValue) // Convert to IntStream
                                    .average();

        avg.ifPresent(value -> System.out.println("Average: " + value)); // Output: 30.0
    }
}
```
### **Explanation**
1. `mapToInt(Integer::intValue)`: Converts `Stream<Integer>` to `IntStream`.
2. `average()`: Computes the arithmetic mean.
3. `OptionalDouble` ensures safe handling if the list is empty.

---

## **Finding the Average of Employee Salaries**  

### **Example: Calculating the Average Salary**
```java
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class AverageExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        OptionalDouble avgSalary = employees.stream()
                                            .mapToDouble(emp -> emp.salary)
                                            .average();

        avgSalary.ifPresent(salary -> 
            System.out.println("Average Salary: $" + salary)); // Output: 66250.0
    }
}
```
### **Explanation**
- `mapToDouble(emp -> emp.salary)`: Extracts salaries.
- `average()`: Computes the mean salary.
- `OptionalDouble` prevents errors in empty lists.

---

## **Finding the Average Price of Products**  

### **Example: Calculating the Average Price of Items in a Shopping Cart**
```java
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class AverageExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75),
            new Product("Monitor", 300.00)
        );

        OptionalDouble avgPrice = products.stream()
                                          .mapToDouble(p -> p.price)
                                          .average();

        avgPrice.ifPresent(price -> 
            System.out.println("Average Product Price: $" + price)); // Output: 688.06
    }
}
```
### **Explanation**
- `mapToDouble(p -> p.price)`: Extracts product prices.
- `average()`: Computes the average price of products.

---

## **Finding the Average Length of Strings in a List**  

### **Example: Calculating the Average Word Length**
```java
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class AverageExample4 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Java", "Stream", "API", "Programming");

        OptionalDouble avgLength = words.stream()
                                        .mapToInt(String::length)
                                        .average();

        avgLength.ifPresent(length -> 
            System.out.println("Average Word Length: " + length)); // Output: 6.25
    }
}
```
### **Explanation**
- `mapToInt(String::length)`: Converts words into their lengths.
- `average()`: Finds the mean length.

---

## **Finding the Average Order Value in eCommerce**  

### **Example: Calculating the Average Order Value (AOV)**
```java
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class AverageExample5 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, 250.50),
            new Order(2, 400.75),
            new Order(3, 150.25),
            new Order(4, 600.00)
        );

        OptionalDouble avgOrderValue = orders.stream()
                                             .mapToDouble(order -> order.amount)
                                             .average();

        avgOrderValue.ifPresent(value -> 
            System.out.println("Average Order Value: $" + value)); // Output: 350.375
    }
}
```
### **Explanation**
- `mapToDouble(order -> order.amount)`: Extracts order amounts.
- `average()`: Finds the mean order value.

---

## **Finding the Average Temperature Over a Week**  

### **Example: Calculating the Average Temperature**
```java
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class AverageExample6 {
    public static void main(String[] args) {
        List<Double> temperatures = Arrays.asList(22.5, 24.3, 19.8, 21.6, 23.0, 25.4, 20.1);

        OptionalDouble avgTemperature = temperatures.stream()
                                                    .mapToDouble(Double::doubleValue)
                                                    .average();

        avgTemperature.ifPresent(temp -> 
            System.out.println("Average Temperature: " + temp + "°C")); // Output: 22.38°C
    }
}
```
### **Explanation**
- `mapToDouble(Double::doubleValue)`: Extracts temperature values.
- `average()`: Finds the mean temperature.

---

## **Alternative Approach: Using `reduce()` Instead of `average()`**  

### **Example: Calculating the Average Using `reduce()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class AverageExample7 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        OptionalDouble avg = numbers.stream()
                                    .mapToInt(Integer::intValue)
                                    .reduce((sum, num) -> sum + num)
                                    .stream()
                                    .average();

        avg.ifPresent(value -> System.out.println("Average: " + value)); // Output: 30.0
    }
}
```
### **Explanation**
- Uses `reduce()` to sum elements.
- Uses `stream().average()` to compute the mean.

---

## **Conclusion**  
The `average()` method in Java 8 Stream API is **useful** for:
✅ Calculating **mean values** in **numeric datasets**.  
✅ Finding **average salaries, product prices, order values, and temperatures**.  
✅ Handling **empty lists safely** with `OptionalDouble`.  

Would you like additional **custom examples**? 🚀

# What is count()?
The `count()` method in Java 8 Stream API is a **terminal operation** that returns the **total number of elements** in a stream. It is useful for counting items in a list, counting occurrences after applying filters, and determining the size of datasets efficiently.

Unlike methods like `findFirst()` or `max()`, the `count()` method does not modify the stream but simply provides a numerical **count**.

---

## **Syntax of `count()`**  
The `count()` method is available in the Stream API with the following syntax:

```java
long count();
```
- Returns a `long` value representing the number of elements in the stream.
- Since it is a **terminal operation**, it consumes the stream and cannot be used further.

For primitive streams, `count()` works with:
- `IntStream.count()`
- `LongStream.count()`
- `DoubleStream.count()`

---

## **Counting Elements in a List**  

### **Example: Counting Elements in a List of Numbers**
```java
import java.util.Arrays;
import java.util.List;

public class CountExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        long count = numbers.stream().count();

        System.out.println("Total Count: " + count); // Output: 5
    }
}
```
### **Explanation**
- `numbers.stream().count()`: Returns the total count of elements in the list.

---

## **Counting Elements After Applying `filter()`**  

### **Example: Counting Even Numbers in a List**
```java
import java.util.Arrays;
import java.util.List;

public class CountExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 15, 20, 25, 30, 35);

        long evenCount = numbers.stream()
                                .filter(n -> n % 2 == 0)
                                .count();

        System.out.println("Total Even Numbers: " + evenCount); // Output: 3
    }
}
```
### **Explanation**
1. `filter(n -> n % 2 == 0)`: Selects only even numbers.
2. `count()`: Counts the number of even numbers.

---

## **Counting Employees in an eCommerce Application**  

### **Example: Counting Employees with a Salary Greater Than 60,000**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class CountExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        long highSalaryCount = employees.stream()
                                        .filter(emp -> emp.salary > 60000)
                                        .count();

        System.out.println("Employees with Salary > 60000: " + highSalaryCount); // Output: 2
    }
}
```
### **Explanation**
- Uses `filter(emp -> emp.salary > 60000)` to select employees with a high salary.
- `count()` returns the number of such employees.

---

## **Counting Products Below a Certain Price in an eCommerce Store**  

### **Example: Counting Affordable Products in a Product List**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class CountExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75),
            new Product("Monitor", 300.00)
        );

        long affordableProductCount = products.stream()
                                              .filter(p -> p.price < 500)
                                              .count();

        System.out.println("Affordable Products: " + affordableProductCount); // Output: 2
    }
}
```
### **Explanation**
- Uses `filter(p -> p.price < 500)` to select affordable products.
- `count()` returns the number of such products.

---

## **Counting Words in a List**  

### **Example: Counting Words Longer Than 5 Characters**
```java
import java.util.Arrays;
import java.util.List;

public class CountExample5 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Java", "Stream", "Reduce", "Programming");

        long longWordsCount = words.stream()
                                   .filter(word -> word.length() > 5)
                                   .count();

        System.out.println("Words Longer Than 5 Characters: " + longWordsCount); // Output: 2
    }
}
```
### **Explanation**
- Uses `filter(word -> word.length() > 5)` to count long words.

---

## **Counting Orders with High Value in an eCommerce Store**  

### **Example: Counting Orders Above $500**
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class CountExample6 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, 250.50),
            new Order(2, 400.75),
            new Order(3, 150.25),
            new Order(4, 600.00),
            new Order(5, 850.00)
        );

        long highValueOrders = orders.stream()
                                     .filter(order -> order.amount > 500)
                                     .count();

        System.out.println("High-Value Orders: " + highValueOrders); // Output: 2
    }
}
```
### **Explanation**
- `filter(order -> order.amount > 500)`: Filters high-value orders.
- `count()` returns the number of high-value orders.

---

## **Alternative Approach: Using `collect(Collectors.counting())` Instead of `count()`**  

### **Example: Counting Elements Using `collect()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CountExample7 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        long count = numbers.stream().collect(Collectors.counting());

        System.out.println("Total Count: " + count); // Output: 5
    }
}
```
### **Explanation**
- Uses `collect(Collectors.counting())`, which is another way to count elements.

---

## **Performance Considerations**  
1. **Using `count()` is efficient** because it processes the stream lazily and avoids unnecessary computations.
2. **If the exact count is not required**, consider using **`limit()`** or **`findAny()`** for better performance in large datasets.

---

## **Conclusion**  
The `count()` method in Java 8 Stream API is **powerful and efficient** for:
✅ Counting **total elements** in a collection.  
✅ Counting **filtered** items.  
✅ Counting **words, numbers, employees, and transactions**.  
✅ Using `Optional` to avoid null issues.  

Would you like **custom examples** based on your use case? 🚀

# What is findFirst()?
The `findFirst()` method in Java 8 Stream API is a **terminal operation** that returns the **first element** in a stream. It is useful when you need to retrieve the **first available element** from a dataset.

It returns an **`Optional<T>`**, which helps prevent `NullPointerException` if the stream is empty.

---

## **Syntax of `findFirst()`**  
The `findFirst()` method is available in the Stream API with the following syntax:

```java
Optional<T> findFirst();
```
- Returns an `Optional<T>` that may contain the **first element** of the stream.
- If the stream is **empty**, it returns `Optional.empty()`.
- Works with both **ordered** and **unordered** streams.

---

## **Finding the First Element in a List**  

### **Example: Getting the First Element of a List of Numbers**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindFirstExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        Optional<Integer> firstNumber = numbers.stream().findFirst();

        firstNumber.ifPresent(num -> System.out.println("First Number: " + num)); // Output: 10
    }
}
```
### **Explanation**
1. `stream().findFirst()`: Returns the first element.
2. `Optional<Integer>` prevents `NullPointerException`.
3. `ifPresent(num -> System.out.println(num))`: Prints only if a value is present.

---

## **Using `findFirst()` with Filtering**  

### **Example: Finding the First Even Number in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindFirstExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(15, 27, 18, 22, 30);

        Optional<Integer> firstEven = numbers.stream()
                                             .filter(n -> n % 2 == 0)
                                             .findFirst();

        firstEven.ifPresent(num -> System.out.println("First Even Number: " + num)); // Output: 18
    }
}
```
### **Explanation**
1. `filter(n -> n % 2 == 0)`: Selects even numbers.
2. `findFirst()`: Returns the first even number.

---

## **Finding the First Employee with a High Salary**  

### **Example: Finding the First Employee with Salary Greater Than 60,000**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class FindFirstExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 55000),
            new Employee("Bob", 75000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        Optional<Employee> highSalaryEmployee = employees.stream()
                                                         .filter(emp -> emp.salary > 60000)
                                                         .findFirst();

        highSalaryEmployee.ifPresent(emp -> 
            System.out.println("First Employee with Salary > 60000: " + emp.name)
        ); // Output: Bob
    }
}
```
### **Explanation**
- `filter(emp -> emp.salary > 60000)`: Selects employees earning more than 60,000.
- `findFirst()`: Retrieves the **first employee** who meets the condition.

---

## **Using `findFirst()` with `sorted()`**  

### **Example: Finding the Lowest Priced Product in an eCommerce Store**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class FindFirstExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75),
            new Product("Monitor", 300.00)
        );

        Optional<Product> cheapestProduct = products.stream()
                                                    .sorted((p1, p2) -> Double.compare(p1.price, p2.price))
                                                    .findFirst();

        cheapestProduct.ifPresent(product -> 
            System.out.println("Cheapest Product: " + product.name + " - $" + product.price)
        ); // Output: Monitor - $300.00
    }
}
```
### **Explanation**
- Uses `sorted((p1, p2) -> Double.compare(p1.price, p2.price))` to **sort** products by price.
- `findFirst()` retrieves the **cheapest product**.

---

## **Finding the First Transaction with High Value**  

### **Example: Finding the First High-Value Transaction**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Transaction {
    String id;
    double amount;

    public Transaction(String id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class FindFirstExample5 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", 200.50),
            new Transaction("TXN2", 1500.75),
            new Transaction("TXN3", 5000.00),
            new Transaction("TXN4", 750.00)
        );

        Optional<Transaction> firstHighValueTransaction = transactions.stream()
                                                                      .filter(txn -> txn.amount > 1000)
                                                                      .findFirst();

        firstHighValueTransaction.ifPresent(txn -> 
            System.out.println("First High-Value Transaction: " + txn.id + " - $" + txn.amount)
        ); // Output: TXN2 - $1500.75
    }
}
```
### **Explanation**
- `filter(txn -> txn.amount > 1000)`: Selects high-value transactions.
- `findFirst()`: Retrieves the **first high-value transaction**.

---

## **Handling Empty Streams with `findFirst()`**  

### **Example: Handling an Empty Stream Safely**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindFirstExample6 {
    public static void main(String[] args) {
        List<Integer> emptyList = Arrays.asList();

        Optional<Integer> firstElement = emptyList.stream().findFirst();

        if (firstElement.isPresent()) {
            System.out.println("First Element: " + firstElement.get());
        } else {
            System.out.println("No elements found!"); // Output: No elements found!
        }
    }
}
```
### **Explanation**
- Uses an **empty list**.
- `Optional.isPresent()` prevents errors if no elements exist.

---

## **Using `findFirst()` vs `findAny()`**  

| Feature         | `findFirst()` | `findAny()` |
|---------------|-------------|-------------|
| **Order Sensitive** | Yes | No |
| **Parallel Performance** | Slower (waits for order) | Faster (returns any element) |
| **Best Used When** | Order matters (e.g., first record) | Order doesn’t matter (e.g., any match) |

### **Example: Difference Between `findFirst()` and `findAny()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindFirstVsFindAny {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        Optional<Integer> first = numbers.stream().findFirst();
        Optional<Integer> any = numbers.parallelStream().findAny();

        first.ifPresent(num -> System.out.println("First: " + num)); // Output: 10
        any.ifPresent(num -> System.out.println("Any: " + num)); // Output: Random value (due to parallel stream)
    }
}
```
### **Explanation**
- `findFirst()` returns **first element**.
- `findAny()` can return **any element (in parallel streams).**

---

## **Conclusion**  
The `findFirst()` method in Java 8 Stream API is **useful** for:
✅ Finding the **first element** in a stream.  
✅ Retrieving **filtered** results.  
✅ Handling **empty lists safely**.  
✅ Using **sorted()** to fetch **smallest/largest values**.  

Would you like additional **custom examples**? 🚀

Here are some **advanced** examples of `findFirst()` in an **eCommerce system**, combining **other Stream operations** like `filter()`, `map()`, `sorted()`, `flatMap()`, `collect()`, and `reduce()`. These examples simulate **real-world scenarios** such as order processing, customer transactions, and product analytics.

---

## **1. Finding the First High-Value Order in a Sorted List**
### **Scenario**: Find the first **high-value order** (above $1000) **sorted by amount**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class EcommerceExample1 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(101, 500.00),
            new Order(102, 2500.00),
            new Order(103, 1500.00),
            new Order(104, 800.00)
        );

        Optional<Order> highValueOrder = orders.stream()
                                               .filter(o -> o.amount > 1000)
                                               .sorted((o1, o2) -> Double.compare(o2.amount, o1.amount)) // Sort descending
                                               .findFirst();

        highValueOrder.ifPresent(o -> 
            System.out.println("First High-Value Order: ID " + o.id + " - $" + o.amount)
        ); // Output: First High-Value Order: ID 102 - $2500.00
    }
}
```
### **Key Operations**
- **`filter(o -> o.amount > 1000)`** → Selects only high-value orders.
- **`sorted((o1, o2) -> Double.compare(o2.amount, o1.amount))`** → Sorts orders by amount in **descending** order.
- **`findFirst()`** → Retrieves the **highest** order above $1000.

---

## **2. Finding the First Out-of-Stock Product in a Category**
### **Scenario**: Find the **first product** in the **"Electronics"** category that is **out of stock**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Product {
    String name;
    String category;
    int stock;

    public Product(String name, String category, int stock) {
        this.name = name;
        this.category = category;
        this.stock = stock;
    }
}

public class EcommerceExample2 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 5),
            new Product("Phone", "Electronics", 0),
            new Product("Tablet", "Electronics", 10),
            new Product("Shoes", "Fashion", 0)
        );

        Optional<Product> outOfStockProduct = products.stream()
                                                      .filter(p -> p.category.equals("Electronics") && p.stock == 0)
                                                      .findFirst();

        outOfStockProduct.ifPresent(p -> 
            System.out.println("First Out-of-Stock Electronics Product: " + p.name)
        ); // Output: Phone
    }
}
```
### **Key Operations**
- **`filter(p -> p.category.equals("Electronics") && p.stock == 0)`** → Finds **out-of-stock** electronics products.
- **`findFirst()`** → Retrieves the **first** matching product.

---

## **3. Finding the First Customer Who Made a Purchase Over $5000**
### **Scenario**: Find the first **customer** who made a purchase **over $5000**, sorted by name.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Customer {
    String name;
    double totalSpent;

    public Customer(String name, double totalSpent) {
        this.name = name;
        this.totalSpent = totalSpent;
    }
}

public class EcommerceExample3 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", 4500),
            new Customer("Bob", 7000),
            new Customer("Charlie", 6000),
            new Customer("David", 3000)
        );

        Optional<Customer> highSpender = customers.stream()
                                                  .filter(c -> c.totalSpent > 5000)
                                                  .sorted((c1, c2) -> c1.name.compareTo(c2.name)) // Sort alphabetically
                                                  .findFirst();

        highSpender.ifPresent(c -> 
            System.out.println("First High-Spender: " + c.name + " - $" + c.totalSpent)
        ); // Output: Bob - $7000
    }
}
```
### **Key Operations**
- **`filter(c -> c.totalSpent > 5000)`** → Filters **high-spending** customers.
- **`sorted((c1, c2) -> c1.name.compareTo(c2.name))`** → Sorts alphabetically.
- **`findFirst()`** → Retrieves the **first high spender**.

---

## **4. Finding the First Pending Order for a Customer**
### **Scenario**: Find the first **pending order** for customer `"John Doe"`.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Order {
    int id;
    String customerName;
    String status;

    public Order(int id, String customerName, String status) {
        this.id = id;
        this.customerName = customerName;
        this.status = status;
    }
}

public class EcommerceExample4 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, "John Doe", "Shipped"),
            new Order(2, "Alice Smith", "Pending"),
            new Order(3, "John Doe", "Pending"),
            new Order(4, "Bob Williams", "Delivered")
        );

        Optional<Order> firstPendingOrder = orders.stream()
                                                  .filter(o -> o.customerName.equals("John Doe") && o.status.equals("Pending"))
                                                  .findFirst();

        firstPendingOrder.ifPresent(o -> 
            System.out.println("First Pending Order for John Doe: ID " + o.id)
        ); // Output: First Pending Order for John Doe: ID 3
    }
}
```
### **Key Operations**
- **`filter(o -> o.customerName.equals("John Doe") && o.status.equals("Pending"))`** → Finds pending orders for `"John Doe"`.
- **`findFirst()`** → Retrieves **first pending order**.

---

## **5. Finding the First Customer Who Bought a Specific Product**
### **Scenario**: Find the **first customer** who **purchased a "Laptop"**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Purchase {
    String customerName;
    String productName;

    public Purchase(String customerName, String productName) {
        this.customerName = customerName;
        this.productName = productName;
    }
}

public class EcommerceExample5 {
    public static void main(String[] args) {
        List<Purchase> purchases = Arrays.asList(
            new Purchase("Alice", "Phone"),
            new Purchase("Bob", "Laptop"),
            new Purchase("Charlie", "Tablet"),
            new Purchase("David", "Laptop")
        );

        Optional<Purchase> firstLaptopBuyer = purchases.stream()
                                                       .filter(p -> p.productName.equals("Laptop"))
                                                       .findFirst();

        firstLaptopBuyer.ifPresent(p -> 
            System.out.println("First Customer Who Bought a Laptop: " + p.customerName)
        ); // Output: Bob
    }
}
```
### **Key Operations**
- **`filter(p -> p.productName.equals("Laptop"))`** → Finds purchases of `"Laptop"`.
- **`findFirst()`** → Retrieves **first laptop buyer**.

---

## **Conclusion**
These advanced **eCommerce scenarios** using `findFirst()` show how it can be combined with:
✅ **Sorting (`sorted()`)** → Find highest/lowest values first.  
✅ **Filtering (`filter()`)** → Select specific conditions.  
✅ **Mapping (`map()`)** → Extract relevant fields.  
✅ **Flat Mapping (`flatMap()`)** → Handle nested lists.  

Would you like **more advanced custom scenarios**? 🚀

# What is findAny()?

The `findAny()` method in Java 8 Stream API is a **terminal operation** that returns **any** element from a stream. It is particularly useful in **parallel streams**, where it can return the **first available** element without guaranteeing **order**.  

It returns an **`Optional<T>`**, which helps avoid `NullPointerException` if the stream is empty.

---

## **Syntax of `findAny()`**  

The `findAny()` method is available in the Stream API with the following syntax:  

```java
Optional<T> findAny();
```
- Returns an `Optional<T>` containing **any** element from the stream.
- If the stream is **empty**, it returns `Optional.empty()`.
- In **parallel streams**, it is optimized to return the first encountered element for performance.

---

## **Finding Any Element in a List**  

### **Example: Getting Any Element from a List of Numbers**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindAnyExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        Optional<Integer> anyNumber = numbers.stream().findAny();

        anyNumber.ifPresent(num -> System.out.println("Any Number: " + num)); // Output: 10 (or any other number)
    }
}
```
### **Explanation**
1. `stream().findAny()` retrieves **any** element.
2. `Optional<Integer>` ensures safe handling if the list is empty.
3. `ifPresent(num -> System.out.println(num))` prints the element if present.

---

## **Using `findAny()` in Parallel Streams**  

### **Example: Finding Any Employee in a Parallel Stream**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class FindAnyExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        Optional<Employee> anyEmployee = employees.parallelStream().findAny();

        anyEmployee.ifPresent(emp -> 
            System.out.println("Any Employee: " + emp.name + " - $" + emp.salary)
        ); 
    }
}
```
### **Explanation**
- `parallelStream().findAny()` returns **any** employee.
- In a **parallel stream**, it may return **different results** on different executions.

---

## **Using `findAny()` with `filter()`**  

### **Example: Finding Any Even Number in a List**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindAnyExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(15, 27, 18, 22, 30);

        Optional<Integer> anyEven = numbers.stream()
                                           .filter(n -> n % 2 == 0)
                                           .findAny();

        anyEven.ifPresent(num -> System.out.println("Any Even Number: " + num)); // Output: Any even number
    }
}
```
### **Explanation**
1. `filter(n -> n % 2 == 0)`: Selects even numbers.
2. `findAny()`: Retrieves **any** even number.

---

## **Finding Any Available Product in an eCommerce Store**  

### **Example: Finding Any Product That is In Stock**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class FindAnyExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 5),
            new Product("Phone", 0),
            new Product("Tablet", 10),
            new Product("Monitor", 8)
        );

        Optional<Product> availableProduct = products.stream()
                                                     .filter(p -> p.stock > 0)
                                                     .findAny();

        availableProduct.ifPresent(p -> 
            System.out.println("Available Product: " + p.name)
        );
    }
}
```
### **Explanation**
- **`filter(p -> p.stock > 0)`** → Finds **in-stock** products.
- **`findAny()`** → Retrieves **any** available product.

---

## **Finding Any Customer Who Made a Purchase Above $1000**  

### **Example: Finding Any High-Spender Customer**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Customer {
    String name;
    double totalSpent;

    public Customer(String name, double totalSpent) {
        this.name = name;
        this.totalSpent = totalSpent;
    }
}

public class FindAnyExample5 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", 4500),
            new Customer("Bob", 7000),
            new Customer("Charlie", 6000),
            new Customer("David", 3000)
        );

        Optional<Customer> highSpender = customers.stream()
                                                  .filter(c -> c.totalSpent > 5000)
                                                  .findAny();

        highSpender.ifPresent(c -> 
            System.out.println("Any High-Spender: " + c.name + " - $" + c.totalSpent)
        );
    }
}
```
### **Explanation**
- **`filter(c -> c.totalSpent > 5000)`** → Finds **high-spenders**.
- **`findAny()`** → Retrieves **any** matching customer.

---

## **Difference Between `findFirst()` and `findAny()`**  

| Feature         | `findFirst()` | `findAny()` |
|---------------|-------------|-------------|
| **Order Sensitive?** | Yes | No |
| **Parallel Performance** | Slower (maintains order) | Faster (returns first encountered) |
| **Best Used When** | Order matters | Order doesn’t matter |

---

## **Example: `findFirst()` vs `findAny()` in a Parallel Stream**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindFirstVsFindAny {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        Optional<Integer> first = numbers.stream().findFirst();
        Optional<Integer> any = numbers.parallelStream().findAny();

        first.ifPresent(num -> System.out.println("First: " + num)); // Output: 10
        any.ifPresent(num -> System.out.println("Any: " + num)); // Output: Could be any number
    }
}
```
### **Explanation**
- `findFirst()` returns **first element** in an ordered stream.
- `findAny()` may return **any element**, especially in **parallel streams**.

---

## **Conclusion**  
The `findAny()` method in Java 8 Stream API is **useful** for:  
✅ Retrieving **any element** efficiently.  
✅ Improving **parallel performance**.  
✅ Handling **filtered** results.  
✅ Preventing `NullPointerException` with `Optional<T>`.  

Would you like **more complex scenarios**? 🚀

Here are **advanced and complex scenarios** using `findAny()` in an **eCommerce** context, **combining multiple Stream operations** like `filter()`, `map()`, `sorted()`, `flatMap()`, and `collect()`.


## **1. Find Any Customer Who Ordered a Product in a Given Price Range**
### **Scenario**: Find **any customer** who has ordered a **product** priced **between $500 and $1000**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Customer {
    String name;
    List<Order> orders;

    public Customer(String name, List<Order> orders) {
        this.name = name;
        this.orders = orders;
    }
}

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class EcommerceFindAnyExample1 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Order(101, 400), new Order(102, 900))),
            new Customer("Bob", Arrays.asList(new Order(103, 1200))),
            new Customer("Charlie", Arrays.asList(new Order(104, 750), new Order(105, 300))),
            new Customer("David", Arrays.asList(new Order(106, 150)))
        );

        Optional<Customer> customer = customers.stream()
                                               .filter(c -> c.orders.stream().anyMatch(o -> o.amount >= 500 && o.amount <= 1000))
                                               .findAny();

        customer.ifPresent(c -> System.out.println("Customer who ordered in range: " + c.name));
    }
}
```
### **Key Operations**
- **`filter(c -> c.orders.stream().anyMatch(...))`** → Finds customers who have at least one order in the price range.
- **`findAny()`** → Retrieves **any** matching customer.

---

## **2. Find Any Product with a Discount from a Specific Category**
### **Scenario**: Find **any product** in the `"Electronics"` category that has a **discount of 20% or more**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Product {
    String name;
    String category;
    double price;
    double discountPercentage;

    public Product(String name, String category, double price, double discountPercentage) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.discountPercentage = discountPercentage;
    }
}

public class EcommerceFindAnyExample2 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99, 10),
            new Product("Phone", "Electronics", 800.50, 20),
            new Product("Shoes", "Fashion", 150.75, 30),
            new Product("Tablet", "Electronics", 450.75, 25)
        );

        Optional<Product> discountedProduct = products.stream()
                                                      .filter(p -> p.category.equals("Electronics") && p.discountPercentage >= 20)
                                                      .findAny();

        discountedProduct.ifPresent(p -> System.out.println("Discounted Product: " + p.name));
    }
}
```
### **Key Operations**
- **`filter(p -> p.category.equals("Electronics") && p.discountPercentage >= 20)`** → Finds electronics with at least a 20% discount.
- **`findAny()`** → Retrieves **any** matching product.

---

## **3. Find Any Order That Contains Multiple Items and Exceeds $1000**
### **Scenario**: Find **any order** that contains **multiple items** and the **total amount exceeds $1000**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Order {
    int id;
    List<Item> items;

    public Order(int id, List<Item> items) {
        this.id = id;
        this.items = items;
    }

    public double getTotalAmount() {
        return items.stream().mapToDouble(i -> i.price * i.quantity).sum();
    }
}

class Item {
    String name;
    double price;
    int quantity;

    public Item(String name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
}

public class EcommerceFindAnyExample3 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, Arrays.asList(new Item("Laptop", 1200, 1))),
            new Order(2, Arrays.asList(new Item("Phone", 500, 1), new Item("Tablet", 600, 1))),
            new Order(3, Arrays.asList(new Item("Monitor", 200, 2), new Item("Keyboard", 50, 3)))
        );

        Optional<Order> highValueOrder = orders.stream()
                                               .filter(o -> o.items.size() > 1 && o.getTotalAmount() > 1000)
                                               .findAny();

        highValueOrder.ifPresent(o -> System.out.println("Order ID with multiple items exceeding $1000: " + o.id));
    }
}
```
### **Key Operations**
- **`filter(o -> o.items.size() > 1 && o.getTotalAmount() > 1000)`** → Finds orders with multiple items and total value > $1000.
- **`findAny()`** → Retrieves **any** matching order.

---

## **4. Find Any Customer Who Purchased at Least One Item in Each Category**
### **Scenario**: Find **any customer** who has bought **at least one product from each category** (`Electronics`, `Fashion`, `Grocery`).

```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

class Customer {
    String name;
    List<Purchase> purchases;

    public Customer(String name, List<Purchase> purchases) {
        this.name = name;
        this.purchases = purchases;
    }
}

class Purchase {
    String productName;
    String category;

    public Purchase(String productName, String category) {
        this.productName = productName;
        this.category = category;
    }
}

public class EcommerceFindAnyExample4 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Purchase("Laptop", "Electronics"), new Purchase("T-shirt", "Fashion"))),
            new Customer("Bob", Arrays.asList(new Purchase("Phone", "Electronics"), new Purchase("Shoes", "Fashion"), new Purchase("Milk", "Grocery")))),
            new Customer("Charlie", Arrays.asList(new Purchase("Tablet", "Electronics"), new Purchase("Jacket", "Fashion")))
        );

        Set<String> requiredCategories = Set.of("Electronics", "Fashion", "Grocery");

        Optional<Customer> fullCategoryCustomer = customers.stream()
                                                           .filter(c -> c.purchases.stream()
                                                                                   .map(p -> p.category)
                                                                                   .collect(Collectors.toSet())
                                                                                   .containsAll(requiredCategories))
                                                           .findAny();

        fullCategoryCustomer.ifPresent(c -> System.out.println("Customer who bought from all categories: " + c.name));
    }
}
```
### **Key Operations**
- **`filter(c -> c.purchases.stream().map(p -> p.category).collect(Collectors.toSet()).containsAll(requiredCategories))`** → Finds customers who purchased in **all** categories.
- **`findAny()`** → Retrieves **any** matching customer.

---

## **Conclusion**
These advanced **eCommerce scenarios** using `findAny()` show how it can be combined with:
✅ **Filtering (`filter()`)** → Select specific conditions.  
✅ **Mapping (`map()`)** → Extract relevant fields.  
✅ **Flat Mapping (`flatMap()`)** → Handle nested lists.  
✅ **Sorting (`sorted()`)** → Optimize data retrieval.  
✅ **Reducing (`reduce()`)** → Aggregate values.  

Would you like **even more complex, real-world cases**? 🚀

# What is allMatch()?

The `allMatch()` method in Java 8 Stream API is a **terminal operation** that checks **whether all elements in the stream satisfy a given condition** (predicate). It returns a **boolean** value:
- `true` → if **all** elements match the condition.
- `false` → if **at least one** element does not match.
- **Short-circuits** (stops processing early) if an element **fails the condition**.

---

## **Syntax of `allMatch()`**  

```java
boolean allMatch(Predicate<? super T> predicate);
```
- **Returns `true`** if all elements satisfy the condition.
- **Returns `false`** if at least one element does not match.
- **Stops early** (short-circuits) if a mismatch is found.

---

## **Checking If All Numbers Are Positive**  

### **Example: Checking if All Numbers Are Positive**
```java
import java.util.Arrays;
import java.util.List;

public class AllMatchExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        boolean allPositive = numbers.stream().allMatch(n -> n > 0);

        System.out.println("Are all numbers positive? " + allPositive); // Output: true
    }
}
```
### **Explanation**
1. **`allMatch(n -> n > 0)`** → Checks if all numbers are positive.
2. **Returns `true`** since all numbers are greater than `0`.

---

## **Checking If All Employees Have a Salary Above a Threshold**  

### **Example: Checking If All Employees Earn More Than $50,000**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class AllMatchExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        boolean allAboveThreshold = employees.stream().allMatch(emp -> emp.salary > 50000);

        System.out.println("Do all employees earn more than $50,000? " + allAboveThreshold); // Output: true
    }
}
```
### **Explanation**
- **`allMatch(emp -> emp.salary > 50000)`** → Checks if all employees have salaries above `$50,000`.
- **Returns `true`** since every employee meets the condition.

---

## **Checking If All Products Are In Stock**  

### **Example: Checking If All Products Have Stock Available**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class AllMatchExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 5),
            new Product("Phone", 8),
            new Product("Tablet", 12),
            new Product("Monitor", 0)  // Out of stock
        );

        boolean allInStock = products.stream().allMatch(p -> p.stock > 0);

        System.out.println("Are all products in stock? " + allInStock); // Output: false
    }
}
```
### **Explanation**
- **`allMatch(p -> p.stock > 0)`** → Checks if all products have stock.
- **Returns `false`** because `"Monitor"` is out of stock.

---

## **Checking If All Customers Have Placed At Least One Order**  

### **Example: Checking If Every Customer Has Orders**
```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    List<Order> orders;

    public Customer(String name, List<Order> orders) {
        this.name = name;
        this.orders = orders;
    }
}

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class AllMatchExample4 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Order(101, 400), new Order(102, 900))),
            new Customer("Bob", Arrays.asList(new Order(103, 1200))),
            new Customer("Charlie", Arrays.asList()) // No orders
        );

        boolean allHaveOrders = customers.stream().allMatch(c -> !c.orders.isEmpty());

        System.out.println("Have all customers placed at least one order? " + allHaveOrders); // Output: false
    }
}
```
### **Explanation**
- **`allMatch(c -> !c.orders.isEmpty())`** → Checks if every customer has at least **one order**.
- **Returns `false`** because `"Charlie"` has **no orders**.

---

## **Checking If All Transactions Are Successful**  

### **Example: Checking If All Transactions Have Status `"Success"`**
```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    String id;
    String status;

    public Transaction(String id, String status) {
        this.id = id;
        this.status = status;
    }
}

public class AllMatchExample5 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", "Success"),
            new Transaction("TXN2", "Success"),
            new Transaction("TXN3", "Failed"),
            new Transaction("TXN4", "Success")
        );

        boolean allSuccessful = transactions.stream().allMatch(t -> t.status.equals("Success"));

        System.out.println("Are all transactions successful? " + allSuccessful); // Output: false
    }
}
```
### **Explanation**
- **`allMatch(t -> t.status.equals("Success"))`** → Checks if all transactions were **successful**.
- **Returns `false`** because `"TXN3"` **failed**.

---

## **Checking If All Words Contain a Specific Character**  

### **Example: Checking If All Words Contain the Letter `"e"`**
```java
import java.util.Arrays;
import java.util.List;

public class AllMatchExample6 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Elephant", "Stream", "Reduce", "Tree");

        boolean allContainE = words.stream().allMatch(word -> word.toLowerCase().contains("e"));

        System.out.println("Do all words contain 'e'? " + allContainE); // Output: true
    }
}
```
### **Explanation**
- **`allMatch(word -> word.toLowerCase().contains("e"))`** → Checks if **all words contain "e"**.
- **Returns `true`** because all words have `"e"`.

---

## **Difference Between `allMatch()`, `anyMatch()`, and `noneMatch()`**  

| Method        | Returns `true` If... | Stops Early? | Use Case |
|--------------|---------------------|-------------|----------|
| **`allMatch()`** | **All** elements match condition | **Yes** | Validate all items meet a rule |
| **`anyMatch()`** | **At least one** element matches | **Yes** | Check if any item meets a rule |
| **`noneMatch()`** | **No** elements match | **Yes** | Verify no items meet a rule |

---

## **Conclusion**  

The `allMatch()` method is **useful for:**  
✅ **Checking constraints** (e.g., all orders are above $50).  
✅ **Validating data** (e.g., all transactions are successful).  
✅ **Ensuring consistency** (e.g., all customers have placed an order).  
✅ **Efficient processing** (stops early if a mismatch is found).  

Would you like more **complex scenarios**? 🚀

Here are **advanced real-world eCommerce scenarios** using `allMatch()`, combined with **filtering, mapping, flat-mapping, sorting, and reducing**.

---

## **1. Check If All Customers Have At Least One Order Above $100**
### **Scenario**: Ensure that **every customer** has placed at least **one order** that is **greater than $100**.
```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    List<Order> orders;

    public Customer(String name, List<Order> orders) {
        this.name = name;
        this.orders = orders;
    }
}

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class EcommerceAllMatchExample1 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Order(101, 150), new Order(102, 200))),
            new Customer("Bob", Arrays.asList(new Order(103, 50), new Order(104, 300))), // FAILS
            new Customer("Charlie", Arrays.asList(new Order(105, 500)))
        );

        boolean allHaveLargeOrders = customers.stream()
                                              .allMatch(c -> c.orders.stream().anyMatch(o -> o.amount > 100));

        System.out.println("Do all customers have at least one order above $100? " + allHaveLargeOrders); // Output: false
    }
}
```
### **Key Operations**
- **`allMatch(c -> c.orders.stream().anyMatch(o -> o.amount > 100))`** → Checks if every customer has at least **one** order above $100.
- **Returns `false`** because `"Bob"` has an order below $100.

---

## **2. Check If All Products in the Inventory Are Above a Minimum Stock Level**
### **Scenario**: Verify that **every product** in inventory has at least **5 units in stock**.
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class EcommerceAllMatchExample2 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 10),
            new Product("Phone", 20),
            new Product("Tablet", 3), // FAILS
            new Product("Monitor", 7)
        );

        boolean allAboveStockLimit = products.stream().allMatch(p -> p.stock >= 5);

        System.out.println("Are all products above stock limit? " + allAboveStockLimit); // Output: false
    }
}
```
### **Key Operations**
- **`allMatch(p -> p.stock >= 5)`** → Ensures that every product has at least `5` units in stock.
- **Returns `false`** because the `"Tablet"` has **only 3** units.

---

## **3. Check If All Orders Contain Only In-Stock Products**
### **Scenario**: Ensure that **every order** contains **only products that are in stock**.
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    List<Product> products;

    public Order(int id, List<Product> products) {
        this.id = id;
        this.products = products;
    }
}

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class EcommerceAllMatchExample3 {
    public static void main(String[] args) {
        Product laptop = new Product("Laptop", 5);
        Product phone = new Product("Phone", 0); // OUT OF STOCK
        Product tablet = new Product("Tablet", 8);

        List<Order> orders = Arrays.asList(
            new Order(1, Arrays.asList(laptop, tablet)),
            new Order(2, Arrays.asList(phone, laptop)) // FAILS
        );

        boolean allOrdersHaveStock = orders.stream()
                                           .allMatch(o -> o.products.stream().allMatch(p -> p.stock > 0));

        System.out.println("Do all orders contain only in-stock products? " + allOrdersHaveStock); // Output: false
    }
}
```
### **Key Operations**
- **`allMatch(o -> o.products.stream().allMatch(p -> p.stock > 0))`** → Ensures that **every order** contains **only available products**.
- **Returns `false`** because `"Phone"` is **out of stock**.

---

## **4. Check If All Transactions Are Verified and Above a Certain Amount**
### **Scenario**: Ensure that **all transactions** are **verified** and have an amount **greater than $500**.
```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    String id;
    double amount;
    boolean isVerified;

    public Transaction(String id, double amount, boolean isVerified) {
        this.id = id;
        this.amount = amount;
        this.isVerified = isVerified;
    }
}

public class EcommerceAllMatchExample4 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", 600, true),
            new Transaction("TXN2", 800, true),
            new Transaction("TXN3", 200, true), // FAILS
            new Transaction("TXN4", 1000, true)
        );

        boolean allVerifiedAboveLimit = transactions.stream()
                                                    .allMatch(t -> t.isVerified && t.amount > 500);

        System.out.println("Are all transactions verified and above $500? " + allVerifiedAboveLimit); // Output: false
    }
}
```
### **Key Operations**
- **`allMatch(t -> t.isVerified && t.amount > 500)`** → Ensures that every transaction is **verified** and above **$500**.
- **Returns `false`** because `"TXN3"` has an amount **below $500**.

---

## **5. Check If All Customers Have Purchased at Least One Product in Each Category**
### **Scenario**: Ensure that every **customer has purchased at least one item** from **each required category** (`Electronics`, `Fashion`, `Grocery`).
```java
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Customer {
    String name;
    List<Purchase> purchases;

    public Customer(String name, List<Purchase> purchases) {
        this.name = name;
        this.purchases = purchases;
    }
}

class Purchase {
    String productName;
    String category;

    public Purchase(String productName, String category) {
        this.productName = productName;
        this.category = category;
    }
}

public class EcommerceAllMatchExample5 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Purchase("Laptop", "Electronics"), new Purchase("Shoes", "Fashion")))), // FAILS
            new Customer("Bob", Arrays.asList(new Purchase("Phone", "Electronics"), new Purchase("T-shirt", "Fashion"), new Purchase("Milk", "Grocery"))),
            new Customer("Charlie", Arrays.asList(new Purchase("Tablet", "Electronics"), new Purchase("Jacket", "Fashion"), new Purchase("Bread", "Grocery")))
        );

        Set<String> requiredCategories = Set.of("Electronics", "Fashion", "Grocery");

        boolean allCustomersBoughtAllCategories = customers.stream()
                                                           .allMatch(c -> c.purchases.stream()
                                                                                     .map(p -> p.category)
                                                                                     .collect(Collectors.toSet())
                                                                                     .containsAll(requiredCategories));

        System.out.println("Did all customers buy from all categories? " + allCustomersBoughtAllCategories); // Output: false
    }
}
```
### **Key Operations**
- **`allMatch(c -> c.purchases.stream().map(p -> p.category).collect(Collectors.toSet()).containsAll(requiredCategories))`** → Checks if every customer bought from **all categories**.
- **Returns `false`** because `"Alice"` did not buy from `"Grocery"`.

---

## **Conclusion**
These advanced **eCommerce scenarios** using `allMatch()` demonstrate how it can be used with:
✅ **Filtering (`filter()`)** → Select specific conditions.  
✅ **Mapping (`map()`)** → Extract relevant fields.  
✅ **Flat Mapping (`flatMap()`)** → Handle nested lists.  
✅ **Grouping (`collect()`)** → Process categorized data.  

Would you like **even more complex cases**? 🚀

# What is noneMatch()?

The `noneMatch()` method in Java 8 Stream API is a **terminal operation** that checks if **none of the elements** in a stream **match** a given condition (predicate).  

It returns:  
- **`true`** → If **no** elements match the condition.  
- **`false`** → If **at least one** element matches the condition.  
- **Short-circuits** → Stops processing early if **any** element satisfies the condition.  

---

## **Syntax of `noneMatch()`**  

```java
boolean noneMatch(Predicate<? super T> predicate);
```
- **Returns `true`** if **no** elements satisfy the condition.  
- **Returns `false`** if **at least one** element satisfies the condition.  
- **Stops early** if a matching element is found (for efficiency).  

---

## **Checking If No Numbers Are Negative**  

### **Example: Checking if No Numbers Are Negative**
```java
import java.util.Arrays;
import java.util.List;

public class NoneMatchExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        boolean noNegatives = numbers.stream().noneMatch(n -> n < 0);

        System.out.println("Are there no negative numbers? " + noNegatives); // Output: true
    }
}
```
### **Explanation**
1. **`noneMatch(n -> n < 0)`** → Checks if there are **no negative numbers**.  
2. **Returns `true`** since **all** numbers are **non-negative**.

---

## **Checking If No Employees Earn Below Minimum Wage**  

### **Example: Checking If All Employees Earn at Least $50,000**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class NoneMatchExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 55000),
            new Employee("Charlie", 80000),
            new Employee("David", 60000)
        );

        boolean noLowSalaries = employees.stream().noneMatch(emp -> emp.salary < 50000);

        System.out.println("Do all employees earn at least $50,000? " + noLowSalaries); // Output: true
    }
}
```
### **Explanation**
- **`noneMatch(emp -> emp.salary < 50000)`** → Ensures **no employee** earns below **$50,000**.
- **Returns `true`** because **all employees** meet the condition.

---

## **Checking If No Products Are Out of Stock**  

### **Example: Checking If All Products Are Available in Stock**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class NoneMatchExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 5),
            new Product("Phone", 8),
            new Product("Tablet", 12),
            new Product("Monitor", 0)  // Out of stock
        );

        boolean noOutOfStock = products.stream().noneMatch(p -> p.stock == 0);

        System.out.println("Are all products in stock? " + noOutOfStock); // Output: false
    }
}
```
### **Explanation**
- **`noneMatch(p -> p.stock == 0)`** → Ensures **no product is out of stock**.
- **Returns `false`** because `"Monitor"` is **out of stock**.

---

## **Checking If No Customer Has Made an Order Above $10,000**  

### **Example: Checking If All Orders Are Below $10,000**
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class NoneMatchExample4 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(101, 5000),
            new Order(102, 7000),
            new Order(103, 9000)
        );

        boolean noLargeOrders = orders.stream().noneMatch(o -> o.amount > 10000);

        System.out.println("Are all orders below $10,000? " + noLargeOrders); // Output: true
    }
}
```
### **Explanation**
- **`noneMatch(o -> o.amount > 10000)`** → Ensures **no order exceeds $10,000**.
- **Returns `true`** because **all orders** are **below** the limit.

---

## **Checking If No Transactions Are Marked as "Failed"**  

### **Example: Checking If All Transactions Are Successful**
```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    String id;
    String status;

    public Transaction(String id, String status) {
        this.id = id;
        this.status = status;
    }
}

public class NoneMatchExample5 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", "Success"),
            new Transaction("TXN2", "Success"),
            new Transaction("TXN3", "Pending")
        );

        boolean noFailedTransactions = transactions.stream().noneMatch(t -> t.status.equals("Failed"));

        System.out.println("Are there no failed transactions? " + noFailedTransactions); // Output: true
    }
}
```
### **Explanation**
- **`noneMatch(t -> t.status.equals("Failed"))`** → Ensures **no transaction** has failed.
- **Returns `true`** since **all transactions are either `"Success"` or `"Pending"`**.

---

## **Checking If No Strings Contain a Specific Word**  

### **Example: Checking If No Words Contain "error"**
```java
import java.util.Arrays;
import java.util.List;

public class NoneMatchExample6 {
    public static void main(String[] args) {
        List<String> messages = Arrays.asList("Operation completed", "Process started", "Execution successful");

        boolean noErrors = messages.stream().noneMatch(msg -> msg.contains("error"));

        System.out.println("Are there no error messages? " + noErrors); // Output: true
    }
}
```
### **Explanation**
- **`noneMatch(msg -> msg.contains("error"))`** → Ensures no **error messages** exist.
- **Returns `true`** since none of the messages contain `"error"`.

---

## **Difference Between `allMatch()`, `anyMatch()`, and `noneMatch()`**  

| Method        | Returns `true` If... | Stops Early? | Use Case |
|--------------|---------------------|-------------|----------|
| **`allMatch()`** | **All** elements match condition | **Yes** | Validate all items meet a rule |
| **`anyMatch()`** | **At least one** element matches | **Yes** | Check if any item meets a rule |
| **`noneMatch()`** | **No** elements match | **Yes** | Verify no items meet a rule |

---

## **Conclusion**  

The `noneMatch()` method in Java 8 Stream API is **useful for:**  
✅ **Ensuring constraints** (e.g., No orders above $10,000).  
✅ **Validating data** (e.g., No transactions failed).  
✅ **Checking absence of conditions** (e.g., No employees with salary below $50,000).  
✅ **Efficient processing** (stops early if a matching element is found).  

Would you like **more complex scenarios**? 🚀

Here are **advanced real-world eCommerce scenarios** using `noneMatch()`, combined with **filtering, mapping, flat-mapping, sorting, and reducing**.

---

## **1. Check If No Customers Have Unverified Accounts**
### **Scenario**: Ensure that **no customers** have **unverified** accounts before allowing a bulk order.
```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    boolean isVerified;

    public Customer(String name, boolean isVerified) {
        this.name = name;
        this.isVerified = isVerified;
    }
}

public class EcommerceNoneMatchExample1 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", true),
            new Customer("Bob", true),
            new Customer("Charlie", false) // FAILS
        );

        boolean allVerified = customers.stream().noneMatch(c -> !c.isVerified);

        System.out.println("Are all customers verified? " + allVerified); // Output: false
    }
}
```
### **Key Operations**
- **`noneMatch(c -> !c.isVerified)`** → Ensures **no customer is unverified**.
- **Returns `false`** because `"Charlie"` has an **unverified account**.

---

## **2. Check If No Orders Are Marked as Fraudulent**
### **Scenario**: Ensure that **no orders** are marked as `"Fraudulent"` before processing payments.
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    String status;

    public Order(int id, String status) {
        this.id = id;
        this.status = status;
    }
}

public class EcommerceNoneMatchExample2 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(101, "Completed"),
            new Order(102, "Pending"),
            new Order(103, "Fraudulent") // FAILS
        );

        boolean noFraudOrders = orders.stream().noneMatch(o -> o.status.equals("Fraudulent"));

        System.out.println("Are there no fraudulent orders? " + noFraudOrders); // Output: false
    }
}
```
### **Key Operations**
- **`noneMatch(o -> o.status.equals("Fraudulent"))`** → Ensures **no fraudulent orders exist**.
- **Returns `false`** because `"Order 103"` is marked as **fraudulent**.

---

## **3. Check If No Products Have Expired**
### **Scenario**: Ensure that **no products** in inventory have an **expired date** before allowing sales.
```java
import java.util.Arrays;
import java.util.List;
import java.time.LocalDate;

class Product {
    String name;
    LocalDate expiryDate;

    public Product(String name, LocalDate expiryDate) {
        this.name = name;
        this.expiryDate = expiryDate;
    }
}

public class EcommerceNoneMatchExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Milk", LocalDate.of(2024, 4, 1)),
            new Product("Juice", LocalDate.of(2024, 3, 15)),
            new Product("Yogurt", LocalDate.of(2023, 12, 10)) // FAILS
        );

        boolean noExpiredProducts = products.stream()
                                            .noneMatch(p -> p.expiryDate.isBefore(LocalDate.now()));

        System.out.println("Are there no expired products? " + noExpiredProducts); // Output: false
    }
}
```
### **Key Operations**
- **`noneMatch(p -> p.expiryDate.isBefore(LocalDate.now()))`** → Ensures **no expired products exist**.
- **Returns `false`** because `"Yogurt"` **expired on December 10, 2023**.

---

## **4. Check If No Customers Have Negative Account Balance**
### **Scenario**: Ensure that **no customers** have a **negative balance** before issuing refunds.
```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    double accountBalance;

    public Customer(String name, double accountBalance) {
        this.name = name;
        this.accountBalance = accountBalance;
    }
}

public class EcommerceNoneMatchExample4 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", 1000.00),
            new Customer("Bob", 200.50),
            new Customer("Charlie", -50.75) // FAILS
        );

        boolean noNegativeBalance = customers.stream().noneMatch(c -> c.accountBalance < 0);

        System.out.println("Are there no negative account balances? " + noNegativeBalance); // Output: false
    }
}
```
### **Key Operations**
- **`noneMatch(c -> c.accountBalance < 0)`** → Ensures **no customer** has a **negative balance**.
- **Returns `false`** because `"Charlie"` has **negative balance (-$50.75)**.

---

## **5. Check If No High-Value Transactions Are Pending**
### **Scenario**: Ensure that **no transactions** above **$5000** are still in `"Pending"` status.
```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    String id;
    double amount;
    String status;

    public Transaction(String id, double amount, String status) {
        this.id = id;
        this.amount = amount;
        this.status = status;
    }
}

public class EcommerceNoneMatchExample5 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", 4500, "Completed"),
            new Transaction("TXN2", 7000, "Pending"), // FAILS
            new Transaction("TXN3", 3000, "Completed")
        );

        boolean noHighValuePending = transactions.stream()
                                                 .noneMatch(t -> t.amount > 5000 && t.status.equals("Pending"));

        System.out.println("Are there no high-value pending transactions? " + noHighValuePending); // Output: false
    }
}
```
### **Key Operations**
- **`noneMatch(t -> t.amount > 5000 && t.status.equals("Pending"))`** → Ensures **no pending high-value transactions** exist.
- **Returns `false`** because `"TXN2"` is `"Pending"` with **$7000**.

---

## **6. Check If No Reviews Contain Offensive Words**
### **Scenario**: Ensure that **no customer reviews** contain **offensive words** before publishing.
```java
import java.util.Arrays;
import java.util.List;

public class EcommerceNoneMatchExample6 {
    public static void main(String[] args) {
        List<String> reviews = Arrays.asList(
            "Great product! Very useful.",
            "Worst experience ever!",
            "Amazing service. Highly recommended!"
        );

        List<String> offensiveWords = Arrays.asList("worst", "bad", "terrible");

        boolean noOffensiveReviews = reviews.stream()
                                            .noneMatch(review -> offensiveWords.stream()
                                                                               .anyMatch(review.toLowerCase()::contains));

        System.out.println("Are there no offensive reviews? " + noOffensiveReviews); // Output: false
    }
}
```
### **Key Operations**
- **`noneMatch(review -> offensiveWords.stream().anyMatch(review.toLowerCase()::contains))`** → Checks if **no reviews contain offensive words**.
- **Returns `false`** because `"Worst experience ever!"` contains `"worst"`.

---

## **Conclusion**
These advanced **eCommerce scenarios** using `noneMatch()` demonstrate how it can be combined with:  
✅ **Filtering (`filter()`)** → Select specific conditions.  
✅ **Mapping (`map()`)** → Extract relevant fields.  
✅ **Flat Mapping (`flatMap()`)** → Handle nested lists.  
✅ **Parallel Streams (`parallelStream()`)** → Improve performance.  

Would you like **even more complex cases**? 🚀

# What is anyMatch()?

The `anyMatch()` method in Java 8 Stream API is a **terminal operation** that checks whether **at least one element** in the stream satisfies a given condition (**predicate**).  

It returns:  
- **`true`** → If **at least one** element **matches** the condition.  
- **`false`** → If **no** elements match the condition.  
- **Short-circuits** → Stops processing early if **one matching element is found**.

---

## **Syntax of `anyMatch()`**  

```java
boolean anyMatch(Predicate<? super T> predicate);
```
- **Returns `true`** if **at least one** element satisfies the condition.  
- **Returns `false`** if **no elements** satisfy the condition.  
- **Stops early** if a matching element is found (**short-circuiting**).  

---

## **Checking If Any Number Is Even**  

### **Example: Checking If There Is At Least One Even Number**
```java
import java.util.Arrays;
import java.util.List;

public class AnyMatchExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 3, 5, 7, 10);

        boolean hasEven = numbers.stream().anyMatch(n -> n % 2 == 0);

        System.out.println("Is there any even number? " + hasEven); // Output: true
    }
}
```
### **Explanation**
1. **`anyMatch(n -> n % 2 == 0)`** → Checks if **any number** is **even**.  
2. **Returns `true`** since `10` is even.  

---

## **Checking If Any Employee Earns Above $100,000**  

### **Example: Checking If There Is At Least One High-Earning Employee**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class AnyMatchExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 95000),
            new Employee("Charlie", 120000) // MATCH
        );

        boolean hasHighEarner = employees.stream().anyMatch(emp -> emp.salary > 100000);

        System.out.println("Is there any high-earning employee? " + hasHighEarner); // Output: true
    }
}
```
### **Explanation**
- **`anyMatch(emp -> emp.salary > 100000)`** → Checks if **any employee** earns above **$100,000**.
- **Returns `true`** since `"Charlie"` earns **$120,000**.

---

## **Checking If Any Product Is Out of Stock**  

### **Example: Checking If Any Product Has 0 Stock**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
}

public class AnyMatchExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 5),
            new Product("Phone", 0), // MATCH
            new Product("Tablet", 10)
        );

        boolean anyOutOfStock = products.stream().anyMatch(p -> p.stock == 0);

        System.out.println("Is there any out-of-stock product? " + anyOutOfStock); // Output: true
    }
}
```
### **Explanation**
- **`anyMatch(p -> p.stock == 0)`** → Checks if **any product is out of stock**.
- **Returns `true`** because `"Phone"` has **0 stock**.

---

## **Checking If Any Order Exceeds $5000**  

### **Example: Checking If Any Customer Has an Order Above $5000**
```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class AnyMatchExample4 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(101, 3500),
            new Order(102, 4800),
            new Order(103, 6000) // MATCH
        );

        boolean anyLargeOrder = orders.stream().anyMatch(o -> o.amount > 5000);

        System.out.println("Is there any order above $5000? " + anyLargeOrder); // Output: true
    }
}
```
### **Explanation**
- **`anyMatch(o -> o.amount > 5000)`** → Checks if **any order** is above **$5000**.
- **Returns `true`** because `"Order 103"` is **$6000**.

---

## **Checking If Any Transaction Failed**  

### **Example: Checking If Any Transaction Has `"Failed"` Status**
```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    String id;
    String status;

    public Transaction(String id, String status) {
        this.id = id;
        this.status = status;
    }
}

public class AnyMatchExample5 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", "Success"),
            new Transaction("TXN2", "Pending"),
            new Transaction("TXN3", "Failed") // MATCH
        );

        boolean anyFailed = transactions.stream().anyMatch(t -> t.status.equals("Failed"));

        System.out.println("Is there any failed transaction? " + anyFailed); // Output: true
    }
}
```
### **Explanation**
- **`anyMatch(t -> t.status.equals("Failed"))`** → Checks if **any transaction** has failed.
- **Returns `true`** because `"TXN3"` is `"Failed"`.

---

## **Checking If Any Customer Has Negative Account Balance**  

### **Example: Checking If Any Customer Has a Negative Balance**
```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    double accountBalance;

    public Customer(String name, double accountBalance) {
        this.name = name;
        this.accountBalance = accountBalance;
    }
}

public class AnyMatchExample6 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", 1000.00),
            new Customer("Bob", -50.75), // MATCH
            new Customer("Charlie", 200.50)
        );

        boolean hasNegativeBalance = customers.stream().anyMatch(c -> c.accountBalance < 0);

        System.out.println("Is there any negative balance? " + hasNegativeBalance); // Output: true
    }
}
```
### **Explanation**
- **`anyMatch(c -> c.accountBalance < 0)`** → Checks if **any customer** has a **negative balance**.
- **Returns `true`** because `"Bob"` has **-50.75 balance**.

---

## **Difference Between `allMatch()`, `anyMatch()`, and `noneMatch()`**  

| Method        | Returns `true` If... | Stops Early? | Use Case |
|--------------|---------------------|-------------|----------|
| **`allMatch()`** | **All** elements match condition | **Yes** | Validate all items meet a rule |
| **`anyMatch()`** | **At least one** element matches | **Yes** | Check if any item meets a rule |
| **`noneMatch()`** | **No** elements match | **Yes** | Verify no items meet a rule |

---

## **Conclusion**  

The `anyMatch()` method in Java 8 Stream API is **useful for:**  
✅ **Checking constraints** (e.g., At least one order above $5000).  
✅ **Detecting errors** (e.g., At least one transaction failed).  
✅ **Ensuring conditions exist** (e.g., At least one product is out of stock).  
✅ **Efficient processing** (stops early if a matching element is found).  

Would you like **more complex scenarios**? 🚀

Here are **advanced real-world eCommerce scenarios** using `anyMatch()`, combined with **filtering, mapping, flat-mapping, sorting, and reducing**.

---

## **1. Check If Any Customer Has Purchased a Specific Product**
### **Scenario**: Verify if **at least one customer** has purchased a `"Laptop"`.

```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    List<Purchase> purchases;

    public Customer(String name, List<Purchase> purchases) {
        this.name = name;
        this.purchases = purchases;
    }
}

class Purchase {
    String productName;

    public Purchase(String productName) {
        this.productName = productName;
    }
}

public class EcommerceAnyMatchExample1 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Purchase("Phone"), new Purchase("Tablet"))),
            new Customer("Bob", Arrays.asList(new Purchase("Laptop"))), // MATCH
            new Customer("Charlie", Arrays.asList(new Purchase("Monitor")))
        );

        boolean laptopPurchased = customers.stream()
                                           .anyMatch(c -> c.purchases.stream()
                                                                     .anyMatch(p -> p.productName.equals("Laptop")));

        System.out.println("Has any customer purchased a Laptop? " + laptopPurchased); // Output: true
    }
}
```
### **Key Operations**
- **`anyMatch(c -> c.purchases.stream().anyMatch(p -> p.productName.equals("Laptop")))`**  
  → Checks if **any customer** has purchased a `"Laptop"`.
- **Returns `true`** because `"Bob"` has purchased a **Laptop**.

---

## **2. Check If Any Order Contains Multiple High-Value Items**
### **Scenario**: Verify if **any order** contains **two or more** items with a price **above $500**.

```java
import java.util.Arrays;
import java.util.List;

class Order {
    int id;
    List<Item> items;

    public Order(int id, List<Item> items) {
        this.id = id;
        this.items = items;
    }
}

class Item {
    String name;
    double price;

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class EcommerceAnyMatchExample2 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1, Arrays.asList(new Item("Monitor", 200), new Item("Keyboard", 50))),
            new Order(2, Arrays.asList(new Item("Laptop", 1200), new Item("Phone", 800))), // MATCH
            new Order(3, Arrays.asList(new Item("Tablet", 400), new Item("Mouse", 30)))
        );

        boolean hasHighValueOrder = orders.stream()
                                          .anyMatch(o -> o.items.stream()
                                                                .filter(i -> i.price > 500)
                                                                .count() >= 2);

        System.out.println("Does any order contain multiple high-value items? " + hasHighValueOrder); // Output: true
    }
}
```
### **Key Operations**
- **`filter(i -> i.price > 500).count() >= 2`**  
  → Finds orders with **two or more** items above **$500**.
- **Returns `true`** because `"Order 2"` has **two** high-value items.

---

## **3. Check If Any Customer Has an Overdue Payment**
### **Scenario**: Verify if **any customer** has an **unpaid bill older than 30 days**.

```java
import java.util.Arrays;
import java.util.List;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

class Customer {
    String name;
    List<Bill> bills;

    public Customer(String name, List<Bill> bills) {
        this.name = name;
        this.bills = bills;
    }
}

class Bill {
    double amount;
    LocalDate dueDate;
    boolean isPaid;

    public Bill(double amount, LocalDate dueDate, boolean isPaid) {
        this.amount = amount;
        this.dueDate = dueDate;
        this.isPaid = isPaid;
    }
}

public class EcommerceAnyMatchExample3 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList(new Bill(200, LocalDate.now().minusDays(10), false))),
            new Customer("Bob", Arrays.asList(new Bill(500, LocalDate.now().minusDays(35), false))), // MATCH
            new Customer("Charlie", Arrays.asList(new Bill(300, LocalDate.now().minusDays(20), true)))
        );

        boolean hasOverdueBill = customers.stream()
                                          .anyMatch(c -> c.bills.stream()
                                                                .anyMatch(b -> !b.isPaid && 
                                                                               ChronoUnit.DAYS.between(b.dueDate, LocalDate.now()) > 30));

        System.out.println("Is there any customer with an overdue bill? " + hasOverdueBill); // Output: true
    }
}
```
### **Key Operations**
- **`!b.isPaid && ChronoUnit.DAYS.between(b.dueDate, LocalDate.now()) > 30`**  
  → Finds unpaid bills **overdue for more than 30 days**.
- **Returns `true`** because `"Bob"` has an overdue bill.

---

## **4. Check If Any High-Risk Transaction Exists**
### **Scenario**: Verify if **any transaction** above **$5000** is still in `"Pending"` status.

```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    String id;
    double amount;
    String status;

    public Transaction(String id, double amount, String status) {
        this.id = id;
        this.amount = amount;
        this.status = status;
    }
}

public class EcommerceAnyMatchExample4 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("TXN1", 4500, "Completed"),
            new Transaction("TXN2", 7000, "Pending"), // MATCH
            new Transaction("TXN3", 3000, "Completed")
        );

        boolean hasHighRiskTransaction = transactions.stream()
                                                     .anyMatch(t -> t.amount > 5000 && t.status.equals("Pending"));

        System.out.println("Is there any high-risk transaction? " + hasHighRiskTransaction); // Output: true
    }
}
```
### **Key Operations**
- **`anyMatch(t -> t.amount > 5000 && t.status.equals("Pending"))`**  
  → Finds **high-value** transactions still in **Pending** status.
- **Returns `true`** because `"TXN2"` is `"Pending"` with **$7000**.

---

## **5. Check If Any Customer Has Used a Specific Discount Code**
### **Scenario**: Verify if **any customer** has applied the `"NEWYEAR2024"` discount code.

```java
import java.util.Arrays;
import java.util.List;

class Customer {
    String name;
    List<String> discountCodesUsed;

    public Customer(String name, List<String> discountCodesUsed) {
        this.name = name;
        this.discountCodesUsed = discountCodesUsed;
    }
}

public class EcommerceAnyMatchExample5 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", Arrays.asList("SUMMER2023", "FESTIVE50")),
            new Customer("Bob", Arrays.asList("NEWYEAR2024")), // MATCH
            new Customer("Charlie", Arrays.asList("WELCOME10"))
        );

        boolean usedNewYearDiscount = customers.stream()
                                               .anyMatch(c -> c.discountCodesUsed.contains("NEWYEAR2024"));

        System.out.println("Has any customer used NEWYEAR2024 discount? " + usedNewYearDiscount); // Output: true
    }
}
```
### **Key Operations**
- **`anyMatch(c -> c.discountCodesUsed.contains("NEWYEAR2024"))`**  
  → Checks if **any customer** used the `"NEWYEAR2024"` discount.
- **Returns `true`** because `"Bob"` used the discount.

---

## **Conclusion**
These advanced **eCommerce scenarios** using `anyMatch()` demonstrate how it can be combined with:  
✅ **Filtering (`filter()`)** → Select specific conditions.  
✅ **Mapping (`map()`)** → Extract relevant fields.  
✅ **Flat Mapping (`flatMap()`)** → Handle nested lists.  
✅ **Parallel Streams (`parallelStream()`)** → Improve performance.  

Would you like **even more complex cases**? 🚀

# What is Terminal Operations - collect()?

The `collect()` method in Java 8 Stream API is a **terminal operation** that **transforms a stream** into a **different form**, such as a **List, Set, Map, String, or a custom data structure**. It is widely used for **grouping, partitioning, counting, summing, and summarizing** data.

---

## **Syntax of `collect()`**  

```java
<R, A> R collect(Collector<? super T, A, R> collector);
```
- `T` → The **type** of stream elements.  
- `R` → The **result** type (e.g., `List<T>`, `Set<T>`, `Map<K,V>`).  
- `Collector` → A **collector** that defines how elements are collected.  

The most commonly used collectors are in **`java.util.stream.Collectors`**.

---

## **Common Collectors in `Collectors` Class**  

| Method | Description |
|--------|-------------|
| `toList()` | Collects elements into a `List<T>`. |
| `toSet()` | Collects elements into a `Set<T>`. |
| `toMap(keyMapper, valueMapper)` | Collects elements into a `Map<K, V>`. |
| `joining()` | Joins elements into a single `String`. |
| `counting()` | Counts the number of elements. |
| `summingInt(ToIntFunction<T>)` | Computes the sum of integer values. |
| `averagingDouble(ToDoubleFunction<T>)` | Computes the average of double values. |
| `groupingBy(Function<T, K>)` | Groups elements into a `Map<K, List<T>>`. |
| `partitioningBy(Predicate<T>)` | Partitions elements into a `Map<Boolean, List<T>>`. |

---

## **Basic Examples of `collect()`**

### **1. Collecting Elements into a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        List<String> collectedNames = names.stream().collect(Collectors.toList());

        System.out.println("Collected Names: " + collectedNames); // Output: [Alice, Bob, Charlie]
    }
}
```
### **Explanation**
- `.collect(Collectors.toList())` **converts the stream** into a `List<String>`.

---

### **2. Collecting Elements into a Set**
```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectExample2 {
    public static void main(String[] args) {
        Set<Integer> numbers = Arrays.asList(10, 20, 30, 10, 20).stream()
                                  .collect(Collectors.toSet());

        System.out.println("Collected Set: " + numbers); // Output: [10, 20, 30] (No duplicates)
    }
}
```
### **Explanation**
- `.collect(Collectors.toSet())` **removes duplicates** and collects elements into a `Set<Integer>`.

---

### **3. Collecting Elements into a Map**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class CollectExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        Map<String, Double> productPriceMap = products.stream()
                                                      .collect(Collectors.toMap(p -> p.name, p -> p.price));

        System.out.println("Product Price Map: " + productPriceMap);
        // Output: {Laptop=1200.99, Phone=800.5, Tablet=450.75}
    }
}
```
### **Explanation**
- `.collect(Collectors.toMap(p -> p.name, p -> p.price))` → Collects data into a `Map<String, Double>`.

---

## **Advanced Examples of `collect()`**

### **4. Joining Strings Using `joining()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectExample4 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        String joinedNames = names.stream().collect(Collectors.joining(", "));

        System.out.println("Joined Names: " + joinedNames); // Output: Alice, Bob, Charlie
    }
}
```
### **Explanation**
- `.collect(Collectors.joining(", "))` **joins all elements** into a `String`, separated by `", "`.

---

### **5. Counting Elements Using `counting()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectExample5 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        long count = numbers.stream().collect(Collectors.counting());

        System.out.println("Count: " + count); // Output: 5
    }
}
```
### **Explanation**
- `.collect(Collectors.counting())` **counts** the elements in the stream.

---

### **6. Summing Values Using `summingInt()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectExample6 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        int sum = numbers.stream().collect(Collectors.summingInt(n -> n));

        System.out.println("Sum: " + sum); // Output: 150
    }
}
```
### **Explanation**
- `.collect(Collectors.summingInt(n -> n))` **calculates the sum** of all elements.

---

## **Grouping and Partitioning**

### **7. Grouping Elements Using `groupingBy()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String department;
    String name;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class CollectExample7 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR")
        );

        Map<String, List<Employee>> groupedByDepartment = employees.stream()
                                                                   .collect(Collectors.groupingBy(e -> e.department));

        System.out.println("Grouped Employees: " + groupedByDepartment);
    }
}
```
### **Explanation**
- `.collect(Collectors.groupingBy(e -> e.department))` **groups employees by department**.

---

### **8. Partitioning Elements Using `partitioningBy()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectExample8 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 15, 20, 25, 30);

        Map<Boolean, List<Integer>> partitioned = numbers.stream()
                                                         .collect(Collectors.partitioningBy(n -> n % 2 == 0));

        System.out.println("Partitioned Numbers: " + partitioned);
        // Output: {false=[15, 25], true=[10, 20, 30]}
    }
}
```
### **Explanation**
- `.collect(Collectors.partitioningBy(n -> n % 2 == 0))` **partitions numbers into even and odd groups**.

---

## **Conclusion**
The `collect()` method in Java 8 **Stream API** is used for:  
✅ **Transforming Streams** into Lists, Sets, and Maps.  
✅ **Grouping and Partitioning Data** effectively.  
✅ **Summarizing Data** (counting, summing, averaging).  
✅ **Efficient Data Processing** using **Collectors**.  

Would you like **more complex scenarios**? 🚀

# What is Collectors.toArray()?

The `Collectors.toArray()` method in Java 8 **Stream API** is a **terminal operation** that **collects stream elements into an array**. Unlike `Collectors.toList()` or `Collectors.toSet()`, which return collections, `toArray()` returns an **array**.

There are **two versions** of `toArray()`:
1. **`toArray()`** → Returns an `Object[]` array.
2. **`toArray(IntFunction<T[]> generator)`** → Returns a **typed array** (e.g., `String[]`, `Integer[]`).

---

## **1. Syntax of `toArray()`**  

### **1.1. Default Version (Returns `Object[]`)**
```java
Object[] toArray();
```
- Returns an `Object[]` containing all elements from the stream.
- If you need a **specific type**, you must **cast** the array manually.

---

### **1.2. Custom Array Generator (Returns Typed Array)**
```java
T[] toArray(IntFunction<T[]> generator);
```
- Takes a **function** that creates an **array** of the **same type**.
- Returns a **typed array** (`String[]`, `Integer[]`, etc.).

---

## **2. Basic Examples of `toArray()`**

### **2.1. Using `toArray()` to Convert a Stream into `Object[]`**
```java
import java.util.Arrays;
import java.util.List;

public class ToArrayExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        Object[] nameArray = names.stream().toArray();

        System.out.println(Arrays.toString(nameArray)); // Output: [Alice, Bob, Charlie]
    }
}
```
### **Explanation**
- `.toArray()` converts the **stream** into an `Object[]` array.
- **Downside**: Needs **manual casting** for type-specific operations.

---

### **2.2. Using `toArray(String[]::new)` to Convert to `String[]`**
```java
import java.util.Arrays;
import java.util.List;

public class ToArrayExample2 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        String[] nameArray = names.stream().toArray(String[]::new);

        System.out.println(Arrays.toString(nameArray)); // Output: [Alice, Bob, Charlie]
    }
}
```
### **Explanation**
- `.toArray(String[]::new)` → Creates a **String[]** array instead of `Object[]`.
- No **manual casting** is needed.

---

### **2.3. Using `toArray(Integer[]::new)` to Convert to `Integer[]`**
```java
import java.util.Arrays;
import java.util.List;

public class ToArrayExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        Integer[] numberArray = numbers.stream().toArray(Integer[]::new);

        System.out.println(Arrays.toString(numberArray)); // Output: [1, 2, 3, 4, 5]
    }
}
```
### **Explanation**
- **`toArray(Integer[]::new)`** → Creates a **typed** `Integer[]` array.

---

## **3. Advanced Use Cases of `toArray()`**

### **3.1. Filtering and Converting to an Array**
```java
import java.util.Arrays;
import java.util.List;

public class ToArrayExample4 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 10, 15, 20, 25, 30);

        Integer[] evenNumbers = numbers.stream()
                                       .filter(n -> n % 2 == 0) // Keep even numbers
                                       .toArray(Integer[]::new);

        System.out.println(Arrays.toString(evenNumbers)); // Output: [10, 20, 30]
    }
}
```
### **Explanation**
- **`filter(n -> n % 2 == 0)`** → Keeps only even numbers.
- **`toArray(Integer[]::new)`** → Converts the result into an `Integer[]`.

---

### **3.2. Sorting and Converting to an Array**
```java
import java.util.Arrays;
import java.util.List;

public class ToArrayExample5 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(30, 10, 20, 5, 25);

        Integer[] sortedNumbers = numbers.stream()
                                         .sorted() // Sort elements
                                         .toArray(Integer[]::new);

        System.out.println(Arrays.toString(sortedNumbers)); // Output: [5, 10, 20, 25, 30]
    }
}
```
### **Explanation**
- **`sorted()`** → Sorts elements in **ascending order**.
- **`toArray(Integer[]::new)`** → Converts the **sorted result** into an `Integer[]`.

---

### **3.3. Removing Duplicates and Converting to an Array**
```java
import java.util.Arrays;
import java.util.List;

public class ToArrayExample6 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Alice", "Bob");

        String[] uniqueNames = names.stream()
                                    .distinct() // Remove duplicates
                                    .toArray(String[]::new);

        System.out.println(Arrays.toString(uniqueNames)); // Output: [Alice, Bob, Charlie]
    }
}
```
### **Explanation**
- **`distinct()`** → Removes **duplicate names**.
- **`toArray(String[]::new)`** → Converts the result into a `String[]`.

---

## **4. Real-World Scenarios Using `toArray()`**

### **4.1. Extracting Product Names from a List**
```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ToArrayExample7 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        String[] productNames = products.stream()
                                        .map(p -> p.name) // Extract names
                                        .toArray(String[]::new);

        System.out.println(Arrays.toString(productNames)); // Output: [Laptop, Phone, Tablet]
    }
}
```
### **Explanation**
- **`map(p -> p.name)`** → Extracts product names.
- **`toArray(String[]::new)`** → Converts names into a `String[]`.

---

### **4.2. Checking If Any Employee Has a Salary Above $100,000 and Extracting Their Names**
```java
import java.util.Arrays;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToArrayExample8 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 95000),
            new Employee("Charlie", 120000)
        );

        String[] highEarners = employees.stream()
                                        .filter(emp -> emp.salary > 100000) // Keep high earners
                                        .map(emp -> emp.name) // Extract names
                                        .toArray(String[]::new);

        System.out.println(Arrays.toString(highEarners)); // Output: [Charlie]
    }
}
```
### **Explanation**
- **`filter(emp -> emp.salary > 100000)`** → Finds **high-earning employees**.
- **`map(emp -> emp.name)`** → Extracts their **names**.
- **`toArray(String[]::new)`** → Converts names into a `String[]`.

---

## **5. Conclusion**
The `toArray()` method in Java **Stream API** is useful for:  
✅ **Converting Streams to Arrays** (`Object[]`, `String[]`, `Integer[]`).  
✅ **Filtering, Sorting, and Extracting Data** into an array.  
✅ **Optimizing Performance** (better than manual looping).  

Would you like **more complex scenarios**? 🚀

# What is Collectors.toCollection()?

The `Collectors.toCollection()` method in Java 8 Stream API is a terminal operation that collects elements of a stream into a specific collection type, such as:  

- `ArrayList`  
- `LinkedList`  
- `HashSet`  
- `TreeSet`  
- `PriorityQueue`  

Unlike `Collectors.toList()` or `Collectors.toSet()`, which return default implementations (`ArrayList` and `HashSet` respectively), `Collectors.toCollection()` allows you to control the type of collection.  

## Syntax of `Collectors.toCollection()`  

```java
<T, C extends Collection<T>> Collector<T, ?, C> toCollection(Supplier<C> collectionFactory);
```

- `T` → The type of elements in the stream.  
- `C` → The collection type (e.g., `ArrayList<T>`, `LinkedList<T>`).  
- `Supplier<C>` → A factory method (`() -> new CollectionType<>()`) that generates an empty collection.  

## Collecting Stream Elements into Different Collections  

### Collecting Into an `ArrayList`  

```java
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ToCollectionExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        ArrayList<String> collectedNames = names.stream()
                                                .collect(Collectors.toCollection(ArrayList::new));

        System.out.println("ArrayList: " + collectedNames);
    }
}
```

### Collecting Into a `LinkedList`  

```java
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ToCollectionExample2 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        LinkedList<String> linkedNames = names.stream()
                                              .collect(Collectors.toCollection(LinkedList::new));

        System.out.println("LinkedList: " + linkedNames);
    }
}
```

### Collecting Into a `HashSet` (Removing Duplicates)  

```java
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class ToCollectionExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 10, 20);

        HashSet<Integer> uniqueNumbers = numbers.stream()
                                                .collect(Collectors.toCollection(HashSet::new));

        System.out.println("HashSet: " + uniqueNumbers);
    }
}
```

### Collecting Into a `TreeSet` (Sorted Order)  

```java
import java.util.Arrays;
import java.util.TreeSet;
import java.util.List;
import java.util.stream.Collectors;

public class ToCollectionExample4 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(30, 10, 20, 50, 40);

        TreeSet<Integer> sortedNumbers = numbers.stream()
                                                .collect(Collectors.toCollection(TreeSet::new));

        System.out.println("TreeSet: " + sortedNumbers);
    }
}
```

### Collecting Into a `PriorityQueue` (Heap Data Structure)  

```java
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.List;
import java.util.stream.Collectors;

public class ToCollectionExample5 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(30, 10, 20, 50, 40);

        PriorityQueue<Integer> minHeap = numbers.stream()
                                                .collect(Collectors.toCollection(PriorityQueue::new));

        System.out.println("PriorityQueue: " + minHeap);
    }
}
```

## Advanced Use Cases of `toCollection()`  

### Collecting Employees into a `LinkedList`  

```java
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToCollectionExample6 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000)
        );

        LinkedList<Employee> employeeList = employees.stream()
                                                     .collect(Collectors.toCollection(LinkedList::new));

        employeeList.forEach(e -> System.out.println(e.name + ": $" + e.salary));
    }
}
```

### Collecting High-Salary Employees into a `TreeSet` (Sorted Order)  

```java
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String toString() {
        return name + ": $" + salary;
    }
}

public class ToCollectionExample7 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000)
        );

        TreeSet<Employee> highEarners = employees.stream()
                                                 .filter(e -> e.salary > 60000)
                                                 .collect(Collectors.toCollection(() -> 
                                                     new TreeSet<>((e1, e2) -> Double.compare(e2.salary, e1.salary))));

        System.out.println(highEarners);
    }
}
```

## Comparison: `toCollection()` vs `toList()` vs `toSet()`  

| Feature | `toList()` | `toSet()` | `toCollection(Supplier)` |
|---------|----------|----------|--------------------------|
| Returns | `ArrayList` | `HashSet` | Any collection type |
| Duplicate Handling | Allows duplicates | Removes duplicates | Depends on collection |
| Ordering | Maintains order | Unordered | Depends on collection |
| Performance | Fast insertion | Fast lookup | Depends on collection |

## Conclusion  

The `Collectors.toCollection()` method is useful for:  

- Custom Collection Selection (e.g., `LinkedList`, `TreeSet`)  
- Sorting & Uniqueness (`TreeSet`, `HashSet`)  
- Specialized Data Structures (`PriorityQueue`)  
- Efficient Data Processing for large datasets  

# What is Collectors.toList()?

The `Collectors.toList()` method in Java 8 Stream API is a **terminal operation** that **collects** elements of a stream into a `List`. It provides a simple and efficient way to convert a stream into a `List` without needing to manually add elements one by one.  

Unlike `Collectors.toCollection(ArrayList::new)`, which allows you to choose the list type, `Collectors.toList()` does not **guarantee** the specific implementation of `List` (though in practice, it usually returns an `ArrayList`).  

## **Syntax of `Collectors.toList()`**  

```java
List<T> toList();
```

- `T` → The type of elements in the stream.  
- Returns a `List<T>` containing all elements in the order they were processed.  

## **Collecting Stream Elements into a List**  

### **Collecting a Stream into a List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToListExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        List<String> collectedNames = names.stream().collect(Collectors.toList());

        System.out.println("Collected List: " + collectedNames);
    }
}
```

### **Explanation**  

- `.collect(Collectors.toList())` converts the **stream** into a `List<String>`.  
- The output list preserves the **order of elements** from the original stream.  

## **Using `toList()` with Different Data Types**  

### **Collecting Integers into a List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToListExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        List<Integer> numberList = numbers.stream().collect(Collectors.toList());

        System.out.println("Collected List: " + numberList);
    }
}
```

### **Explanation**  

- `.collect(Collectors.toList())` collects **Integer elements** into a `List<Integer>`.  
- The resulting list preserves the **order of the numbers**.  

## **Filtering and Collecting with `toList()`**  

### **Filtering Even Numbers and Collecting into a List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToListExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 10, 15, 20, 25, 30);

        List<Integer> evenNumbers = numbers.stream()
                                           .filter(n -> n % 2 == 0)
                                           .collect(Collectors.toList());

        System.out.println("Even Numbers: " + evenNumbers);
    }
}
```

### **Explanation**  

- `.filter(n -> n % 2 == 0)` keeps only **even numbers**.  
- `.collect(Collectors.toList())` collects them into a `List<Integer>`.  

## **Sorting and Collecting with `toList()`**  

### **Sorting a List of Names**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToListExample4 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Charlie", "Alice", "Bob");

        List<String> sortedNames = names.stream()
                                        .sorted()
                                        .collect(Collectors.toList());

        System.out.println("Sorted List: " + sortedNames);
    }
}
```

### **Explanation**  

- `.sorted()` sorts elements in **natural order** (`A → Z`).  
- `.collect(Collectors.toList())` collects the sorted elements into a `List<String>`.  

## **Removing Duplicates and Collecting into a List**  

### **Removing Duplicate Elements from a List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToListExample5 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 10, 20, 40);

        List<Integer> uniqueNumbers = numbers.stream()
                                             .distinct()
                                             .collect(Collectors.toList());

        System.out.println("Unique Numbers: " + uniqueNumbers);
    }
}
```

### **Explanation**  

- `.distinct()` removes **duplicate values** from the stream.  
- `.collect(Collectors.toList())` collects the **unique values** into a `List<Integer>`.  

## **Mapping and Collecting with `toList()`**  

### **Extracting Names from a List of Employees**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToListExample6 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000)
        );

        List<String> employeeNames = employees.stream()
                                              .map(emp -> emp.name)
                                              .collect(Collectors.toList());

        System.out.println("Employee Names: " + employeeNames);
    }
}
```

### **Explanation**  

- `.map(emp -> emp.name)` extracts only the **employee names**.  
- `.collect(Collectors.toList())` collects them into a `List<String>`.  

## **Filtering, Mapping, and Collecting**  

### **Finding High-Salary Employees and Extracting Their Names**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToListExample7 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        List<String> highEarners = employees.stream()
                                            .filter(emp -> emp.salary > 60000)
                                            .map(emp -> emp.name)
                                            .collect(Collectors.toList());

        System.out.println("High-Earning Employees: " + highEarners);
    }
}
```

### **Explanation**  

- `.filter(emp -> emp.salary > 60000)` selects **high-earning employees**.  
- `.map(emp -> emp.name)` extracts only the **names**.  
- `.collect(Collectors.toList())` collects them into a `List<String>`.  

## **Comparison: `toList()` vs `toSet()` vs `toCollection()`**  

| Feature | `toList()` | `toSet()` | `toCollection(Supplier)` |
|---------|----------|----------|--------------------------|
| Returns | `ArrayList` | `HashSet` | Any collection type |
| Duplicate Handling | Allows duplicates | Removes duplicates | Depends on collection |
| Ordering | Maintains order | Unordered | Depends on collection |
| Performance | Fast insertion | Fast lookup | Depends on collection |

## **Conclusion**  

The `Collectors.toList()` method in Java 8 Stream API is useful for:  

- Collecting elements into a `List` while preserving order  
- Filtering and mapping data efficiently  
- Sorting and removing duplicates before collecting  
- Transforming objects into lists of specific attributes  

This method is one of the **most commonly used** operations in the Stream API for working with lists.

# What is Collectors.toSet()?
The `Collectors.toSet()` method in Java 8 Stream API is a **terminal operation** that collects elements of a stream into a **`Set`**, which automatically removes duplicates.  

Unlike `Collectors.toList()`, which collects elements into a `List` that allows duplicates, `Collectors.toSet()` ensures that the resulting collection has **only unique elements**.  

However, `Collectors.toSet()` does not guarantee a specific `Set` implementation (it typically returns a `HashSet`), meaning that the order of elements **is not preserved**.

---

## **Syntax of `Collectors.toSet()`**  

```java
Set<T> toSet();
```

- `T` → The type of elements in the stream.  
- Returns a `Set<T>` containing all **unique elements** in an unspecified order.  

---

## **Collecting Stream Elements into a Set**  

### **Collecting a Stream into a `Set`**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ToSetExample1 {
    public static void main(String[] args) {
        Set<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Alice")
                                  .stream()
                                  .collect(Collectors.toSet());

        System.out.println("Collected Set: " + names);
    }
}
```

### **Explanation**  

- `.collect(Collectors.toSet())` converts the **stream** into a `Set<String>`.  
- **Duplicate values are automatically removed**.  
- **Order is not guaranteed** because `Set` does not maintain insertion order.  

---

## **Using `toSet()` with Different Data Types**  

### **Collecting Integers into a Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ToSetExample2 {
    public static void main(String[] args) {
        Set<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 3, 2)
                                     .stream()
                                     .collect(Collectors.toSet());

        System.out.println("Collected Set: " + numbers);
    }
}
```

### **Explanation**  

- `.collect(Collectors.toSet())` collects **Integer elements** into a `Set<Integer>`.  
- The **duplicates are removed** automatically.  

---

## **Filtering and Collecting with `toSet()`**  

### **Filtering Even Numbers and Collecting into a Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ToSetExample3 {
    public static void main(String[] args) {
        Set<Integer> evenNumbers = Arrays.asList(5, 10, 15, 20, 25, 30, 10)
                                         .stream()
                                         .filter(n -> n % 2 == 0)
                                         .collect(Collectors.toSet());

        System.out.println("Even Numbers: " + evenNumbers);
    }
}
```

### **Explanation**  

- `.filter(n -> n % 2 == 0)` keeps only **even numbers**.  
- `.collect(Collectors.toSet())` collects them into a **unique Set**.  

---

## **Sorting and Collecting with `toSet()`**  

### **Sorting a List of Names Before Collecting into a Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.stream.Collectors;

public class ToSetExample4 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Charlie", "Alice", "Bob", "Alice");

        Set<String> sortedNames = names.stream()
                                       .collect(Collectors.toCollection(TreeSet::new));

        System.out.println("Sorted Set: " + sortedNames);
    }
}
```

### **Explanation**  

- `Collectors.toSet()` does **not guarantee order**, so we use `toCollection(TreeSet::new)`.  
- **`TreeSet` maintains sorted order** while ensuring uniqueness.  

---

## **Mapping and Collecting with `toSet()`**  

### **Extracting Names from a List of Employees**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToSetExample5 {
    public static void main(String[] args) {
        Set<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000),
            new Employee("Alice", 70000) // Duplicate
        ).stream()
        .collect(Collectors.toSet());

        employees.forEach(e -> System.out.println(e.name + ": $" + e.salary));
    }
}
```

### **Explanation**  

- `.collect(Collectors.toSet())` ensures that **duplicate objects** are removed **based on reference equality**.  
- To remove duplicates based on **field values**, override `equals()` and `hashCode()` in `Employee`.  

---

## **Filtering, Mapping, and Collecting**  

### **Finding High-Salary Employees and Collecting Their Names into a Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToSetExample6 {
    public static void main(String[] args) {
        Set<String> highEarners = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000),
            new Employee("Alice", 70000) // Duplicate
        ).stream()
        .filter(emp -> emp.salary > 60000)
        .map(emp -> emp.name)
        .collect(Collectors.toSet());

        System.out.println("High-Earning Employees: " + highEarners);
    }
}
```

### **Explanation**  

- `.filter(emp -> emp.salary > 60000)` selects **high-earning employees**.  
- `.map(emp -> emp.name)` extracts only the **names**.  
- `.collect(Collectors.toSet())` ensures **uniqueness** in names.  

---

## **Comparison: `toSet()` vs `toList()` vs `toCollection()`**  

| Feature | `toList()` | `toSet()` | `toCollection(Supplier)` |
|---------|----------|----------|--------------------------|
| Returns | `ArrayList` | `HashSet` | Any collection type |
| Duplicate Handling | Allows duplicates | Removes duplicates | Depends on collection |
| Ordering | Maintains order | Unordered | Depends on collection |
| Performance | Fast insertion | Fast lookup | Depends on collection |

---

## **Conclusion**  

The `Collectors.toSet()` method in Java 8 Stream API is useful for:  

- **Removing duplicates** from a stream  
- **Efficient storage** of unique elements  
- **Faster lookups** compared to `List`  
- **Grouping data** without worrying about order  

This method is ideal for cases where **order does not matter**, and **uniqueness is required**.

# What is Collectors.toMap()?

The `Collectors.toMap()` method in Java 8 Stream API is a **terminal operation** that collects elements of a stream into a **Map**. It allows **key-value mapping** from stream elements, making it useful for **converting collections into maps** based on specific attributes.  

Unlike `Collectors.toList()` and `Collectors.toSet()`, which return `List` and `Set`, `Collectors.toMap()` requires **two functions** to define:  

- How **keys** are extracted  
- How **values** are extracted  

---

## **Syntax of `Collectors.toMap()`**  

### **1. Basic Syntax (Key-Value Mapping)**  
```java
<R, K, V> Collector<T, ?, Map<K, V>> toMap(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper);
```
- `T` → Type of elements in the stream  
- `K` → Key type in the resulting `Map`  
- `V` → Value type in the resulting `Map`  
- `keyMapper` → Function to extract the key  
- `valueMapper` → Function to extract the value  

### **2. Handling Key Collisions (Duplicate Keys)**
```java
<R, K, V> Collector<T, ?, Map<K, V>> toMap(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper, BinaryOperator<V> mergeFunction);
```
- `mergeFunction` → Defines how to handle **duplicate keys**  

### **3. Using a Custom Map Implementation**
```java
<R, K, V, M extends Map<K, V>> Collector<T, ?, M> toMap(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper, BinaryOperator<V> mergeFunction, Supplier<M> mapSupplier);
```
- `mapSupplier` → Provides a **specific Map implementation** (e.g., `TreeMap`)  

---

## **Basic Examples of `toMap()`**  

### **Collecting a List into a Map (Basic Key-Value Mapping)**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ToMapExample1 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        Map<String, Double> productPriceMap = products.stream()
                                                      .collect(Collectors.toMap(p -> p.name, p -> p.price));

        System.out.println("Product Price Map: " + productPriceMap);
    }
}
```

### **Explanation**  
- `.toMap(p -> p.name, p -> p.price)` collects products into a **Map**  
- **Key** → `Product name`  
- **Value** → `Product price`  
- **Output**: `{Laptop=1200.99, Phone=800.5, Tablet=450.75}`  

---

## **Handling Duplicate Keys Using a Merge Function**  

### **Example: Handling Employees with the Same Department**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class ToMapExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT") // Same department as Alice
        );

        Map<String, String> departmentMap = employees.stream()
                                                     .collect(Collectors.toMap(
                                                         e -> e.department, // Key: Department
                                                         e -> e.name,       // Value: Employee Name
                                                         (name1, name2) -> name1 + ", " + name2 // Merge duplicate keys
                                                     ));

        System.out.println("Department Map: " + departmentMap);
    }
}
```

### **Explanation**  
- `.toMap(e -> e.department, e -> e.name, (name1, name2) -> name1 + ", " + name2)`  
- If a **duplicate key** is found (`IT` department appears twice), it **merges values** using `name1 + ", " + name2"`.  
- **Output**: `{IT=Alice, Charlie, HR=Bob}`  

---

## **Using a Custom Map Implementation (TreeMap for Sorting)**  

### **Example: Sorting Product Names Alphabetically**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ToMapExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        Map<String, Double> sortedProductMap = products.stream()
                                                       .collect(Collectors.toMap(
                                                           p -> p.name,
                                                           p -> p.price,
                                                           (existing, replacement) -> existing, // No duplicate handling needed
                                                           TreeMap::new // Use TreeMap for sorting
                                                       ));

        System.out.println("Sorted Product Map: " + sortedProductMap);
    }
}
```

### **Explanation**  
- **Uses `TreeMap`** to keep **product names sorted alphabetically**  
- **Output**: `{Laptop=1200.99, Phone=800.5, Tablet=450.75}`  

---

## **Mapping Employee Names to Salary Using `toMap()`**  

### **Example: Storing Employee Salary in a Map**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToMapExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000)
        );

        Map<String, Double> salaryMap = employees.stream()
                                                 .collect(Collectors.toMap(e -> e.name, e -> e.salary));

        System.out.println("Employee Salary Map: " + salaryMap);
    }
}
```

### **Explanation**  
- Maps `Employee.name` → `Employee.salary`  
- **Output**: `{Alice=70000, Bob=50000, Charlie=80000}`  

---

## **Comparison: `toMap()` vs `toList()` vs `toSet()`**  

| Feature | `toList()` | `toSet()` | `toMap()` |
|---------|----------|----------|----------|
| **Result Type** | `List<T>` | `Set<T>` | `Map<K, V>` |
| **Duplicates** | Allowed | Removed | Keys must be unique |
| **Ordering** | Maintains order | Unordered | Unordered (unless `TreeMap` is used) |
| **Use Case** | Collect items in a list | Collect unique items | Key-value storage |

---

## **Conclusion**  

The `Collectors.toMap()` method in Java 8 is useful for:  

- **Transforming Lists into Key-Value Maps**  
- **Handling Duplicate Keys with a Merge Function**  
- **Sorting Maps Using a Custom Implementation (`TreeMap`)**  
- **Efficiently Mapping Objects to Their Attributes**  

This method is **highly flexible** and widely used for **data transformation** in Java Streams.

## **Advanced and Complex Examples of `Collectors.toMap()` in Java 8**

Here are **advanced real-world scenarios** using `Collectors.toMap()`, combined with **filtering, grouping, and custom merging strategies**.

---

## **1. Converting a List of Orders into a Map of Order ID to Amount**
### **Scenario**: Convert a list of orders into a `Map` where **Order ID** is the key and **Order Amount** is the value.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Order {
    int id;
    double amount;

    public Order(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }
}

public class ToMapAdvancedExample1 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(101, 250.75),
            new Order(102, 500.50),
            new Order(103, 1200.00)
        );

        Map<Integer, Double> orderMap = orders.stream()
                                              .collect(Collectors.toMap(o -> o.id, o -> o.amount));

        System.out.println("Order Map: " + orderMap);
    }
}
```

### **Explanation**
- **Key** → Order ID (`o.id`)  
- **Value** → Order Amount (`o.amount`)  
- **Output**: `{101=250.75, 102=500.5, 103=1200.0}`  

---

## **2. Handling Duplicate Keys Using a Merge Function**
### **Scenario**: Convert a list of orders into a `Map` where **Customer Name** is the key and **Total Amount Spent** is the value.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Order {
    String customerName;
    double amount;

    public Order(String customerName, double amount) {
        this.customerName = customerName;
        this.amount = amount;
    }
}

public class ToMapAdvancedExample2 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order("Alice", 250.75),
            new Order("Bob", 500.50),
            new Order("Alice", 1200.00) // Alice has multiple orders
        );

        Map<String, Double> customerSpending = orders.stream()
                                                     .collect(Collectors.toMap(
                                                         o -> o.customerName,
                                                         o -> o.amount,
                                                         Double::sum // Merge function to sum duplicate values
                                                     ));

        System.out.println("Customer Spending Map: " + customerSpending);
    }
}
```

### **Explanation**
- **Key** → Customer Name (`o.customerName`)  
- **Value** → Order Amount (`o.amount`)  
- **Merge Function** → If a customer has multiple orders, their **amounts are summed**.  
- **Output**: `{Alice=1450.75, Bob=500.5}`  

---

## **3. Creating a TreeMap for Sorted Results**
### **Scenario**: Convert a list of employees into a **sorted `TreeMap`** where **Department** is the key and **Employee Name** is the value.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class ToMapAdvancedExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "Finance"),
            new Employee("David", "IT")
        );

        Map<String, String> departmentEmployeeMap = employees.stream()
                                                             .collect(Collectors.toMap(
                                                                 e -> e.department,
                                                                 e -> e.name,
                                                                 (name1, name2) -> name1 + ", " + name2, // Merge multiple employees in the same department
                                                                 TreeMap::new // Use TreeMap for sorting
                                                             ));

        System.out.println("Sorted Department Map: " + departmentEmployeeMap);
    }
}
```

### **Explanation**
- **Key** → Department (`e.department`)  
- **Value** → Employee Name (`e.name`)  
- **Merge Function** → Combines employees **in the same department** into a single string.  
- **Custom Map Implementation** → Uses `TreeMap` for **sorted keys**.  
- **Output**: `{Finance=Charlie, HR=Bob, IT=Alice, David}`  

---

## **4. Mapping Employee Names to Salaries With Duplicate Handling**
### **Scenario**: Convert a list of employees into a **Map** where **Employee Name** is the key and **Salary** is the value. If there is a duplicate name, keep the highest salary.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToMapAdvancedExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000),
            new Employee("Alice", 75000) // Duplicate name with higher salary
        );

        Map<String, Double> highestSalaryMap = employees.stream()
                                                        .collect(Collectors.toMap(
                                                            e -> e.name,
                                                            e -> e.salary,
                                                            Double::max // Keep the highest salary for duplicate names
                                                        ));

        System.out.println("Highest Salary Map: " + highestSalaryMap);
    }
}
```

### **Explanation**
- **Key** → Employee Name (`e.name`)  
- **Value** → Employee Salary (`e.salary`)  
- **Merge Function** → Keeps the **maximum salary** for duplicate names.  
- **Output**: `{Alice=75000.0, Bob=50000.0, Charlie=80000.0}`  

---

## **5. Grouping Products by Category Using `toMap()`**
### **Scenario**: Convert a list of products into a `Map` where **Category** is the key and **Product Names** are stored in a `List<String>`.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.ArrayList;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class ToMapAdvancedExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics"),
            new Product("Phone", "Electronics"),
            new Product("Shoes", "Fashion"),
            new Product("Tablet", "Electronics"),
            new Product("Jacket", "Fashion")
        );

        Map<String, List<String>> categoryProductMap = products.stream()
                                                               .collect(Collectors.toMap(
                                                                   p -> p.category,
                                                                   p -> new ArrayList<>(List.of(p.name)), // Convert name to List<String>
                                                                   (existingList, newList) -> { 
                                                                       existingList.addAll(newList); 
                                                                       return existingList;
                                                                   }
                                                               ));

        System.out.println("Category to Product Map: " + categoryProductMap);
    }
}
```

### **Explanation**
- **Key** → Category (`p.category`)  
- **Value** → List of Product Names (`List<String>`)  
- **Merge Function** → If a category exists, **append the product name** to the existing list.  
- **Output**: `{Electronics=[Laptop, Phone, Tablet], Fashion=[Shoes, Jacket]}`  

---

## **Conclusion**
These **advanced examples** show how `Collectors.toMap()` can be used for:  

- **Transforming Lists into Key-Value Maps**  
- **Merging Duplicate Keys (Summing, Appending, or Keeping Maximum Values)**  
- **Sorting Maps Using `TreeMap`**  
- **Grouping Elements into Lists Inside a Map**  

This method is **powerful** for **data transformation and aggregation** in Java Streams. Would you like **even more complex cases**? 🚀

# What is Collectors.toUnmodifiableList()?
The `Collectors.toUnmodifiableList()` method in **Java 10** is a **terminal operation** in the Stream API that **collects elements into an immutable (unmodifiable) `List`**.  

Unlike `Collectors.toList()`, which returns a **mutable** `List`, `Collectors.toUnmodifiableList()` creates a **read-only** list that **cannot be modified** after creation.  

This method ensures **data integrity** by preventing accidental modifications to the collected list.

---

## **Syntax of `Collectors.toUnmodifiableList()`**  

```java
List<T> toUnmodifiableList();
```

- `T` → The type of elements in the stream.  
- Returns an **unmodifiable `List<T>`**, meaning you **cannot add, remove, or modify** elements after collection.  
- Throws **`UnsupportedOperationException`** if modification is attempted.

---

## **Difference Between `toList()` and `toUnmodifiableList()`**  

| Feature | `toList()` | `toUnmodifiableList()` |
|---------|-----------|------------------------|
| **Modifiability** | Mutable | Immutable (Read-only) |
| **Modification Methods (`add()`, `remove()`)** | Allowed | Throws `UnsupportedOperationException` |
| **Java Version** | Java 8+ | Java 10+ |
| **Thread-Safety** | Not thread-safe | Thread-safe due to immutability |

---

## **Basic Examples of `toUnmodifiableList()`**  

### **Collecting a Stream into an Unmodifiable List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToUnmodifiableListExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        List<String> unmodifiableList = names.stream()
                                             .collect(Collectors.toUnmodifiableList());

        System.out.println("Unmodifiable List: " + unmodifiableList);

        // Attempting modification will throw UnsupportedOperationException
        unmodifiableList.add("David");  // Throws Exception
    }
}
```

### **Explanation**  
- `.collect(Collectors.toUnmodifiableList())` converts the **stream** into an **immutable list**.  
- **Throws `UnsupportedOperationException`** if `add()`, `remove()`, or `set()` is called.  

---

## **Using `toUnmodifiableList()` with Different Data Types**  

### **Collecting Integers into an Unmodifiable List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToUnmodifiableListExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        List<Integer> unmodifiableNumbers = numbers.stream()
                                                   .collect(Collectors.toUnmodifiableList());

        System.out.println("Unmodifiable List: " + unmodifiableNumbers);

        unmodifiableNumbers.remove(0); // Throws Exception
    }
}
```

### **Explanation**  
- **Immutable list** ensures that numbers **cannot be modified after creation**.  
- **Any modification attempts throw `UnsupportedOperationException`**.

---

## **Filtering and Collecting into an Unmodifiable List**  

### **Filtering Even Numbers and Collecting into an Unmodifiable List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToUnmodifiableListExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 10, 15, 20, 25, 30);

        List<Integer> evenNumbers = numbers.stream()
                                           .filter(n -> n % 2 == 0)
                                           .collect(Collectors.toUnmodifiableList());

        System.out.println("Even Numbers: " + evenNumbers);

        evenNumbers.add(40); // Throws Exception
    }
}
```

### **Explanation**  
- `.filter(n -> n % 2 == 0)` selects **only even numbers**.  
- `.collect(Collectors.toUnmodifiableList())` ensures **immutable results**.  

---

## **Sorting and Collecting into an Unmodifiable List**  

### **Sorting a List Before Collecting into an Unmodifiable List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ToUnmodifiableListExample4 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Charlie", "Alice", "Bob");

        List<String> sortedNames = names.stream()
                                        .sorted()
                                        .collect(Collectors.toUnmodifiableList());

        System.out.println("Sorted Unmodifiable List: " + sortedNames);

        sortedNames.remove(0); // Throws Exception
    }
}
```

### **Explanation**  
- **Sorted before collection** to ensure the list is **ordered but immutable**.  
- **No modifications are allowed after collection**.

---

## **Mapping and Collecting into an Unmodifiable List**  

### **Extracting Names from a List of Employees**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToUnmodifiableListExample5 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000)
        );

        List<String> employeeNames = employees.stream()
                                              .map(emp -> emp.name)
                                              .collect(Collectors.toUnmodifiableList());

        System.out.println("Employee Names: " + employeeNames);

        employeeNames.add("David"); // Throws Exception
    }
}
```

### **Explanation**  
- `.map(emp -> emp.name)` extracts **employee names**.  
- `.collect(Collectors.toUnmodifiableList())` ensures **list is immutable**.

---

## **Advanced Use Cases of `toUnmodifiableList()`**  

### **Finding High-Salary Employees and Collecting Their Names into an Unmodifiable List**  

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToUnmodifiableListExample6 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        List<String> highEarners = employees.stream()
                                            .filter(emp -> emp.salary > 60000)
                                            .map(emp -> emp.name)
                                            .collect(Collectors.toUnmodifiableList());

        System.out.println("High-Earning Employees: " + highEarners);

        highEarners.remove("Alice"); // Throws Exception
    }
}
```

### **Explanation**  
- `.filter(emp -> emp.salary > 60000)` selects **high-earning employees**.  
- `.collect(Collectors.toUnmodifiableList())` makes the resulting list **immutable**.  

---

## **Key Advantages of `toUnmodifiableList()`**  

- **Prevents accidental modifications**  
- **Ensures thread safety** (no external modifications possible)  
- **Better performance** in cases where an unmodifiable collection is required  
- **Ideal for returning API results that should not be modified**  

---

## **Comparison: `toList()` vs `toUnmodifiableList()`**  

| Feature | `toList()` | `toUnmodifiableList()` |
|---------|----------|------------------|
| **Modifiable** | Yes | No |
| **Java Version** | Java 8+ | Java 10+ |
| **Performance** | Fast | Slightly better for read-only operations |
| **Best Used For** | Mutable Lists | Read-only Lists |

---

## **Conclusion**  

The `Collectors.toUnmodifiableList()` method is useful for:  

- **Creating immutable lists that prevent modifications**  
- **Ensuring data integrity and thread safety**  
- **Collecting elements from a stream while avoiding accidental mutations**  
- **Providing better performance for read-only use cases**  

This method is a great choice when you want to **protect collections** from modification while leveraging the power of Java Streams.

# What is Collectors.toUnmodifiableSet()?
The `Collectors.toUnmodifiableSet()` method in **Java 10** is a **terminal operation** in the Stream API that **collects elements into an immutable (unmodifiable) `Set`**.  

Unlike `Collectors.toSet()`, which returns a **mutable** `Set`, `Collectors.toUnmodifiableSet()` creates a **read-only** set that **cannot be modified** after creation.  

This method ensures **data integrity** by preventing accidental modifications to the collected set.

---

## **Syntax of `Collectors.toUnmodifiableSet()`**  

```java
Set<T> toUnmodifiableSet();
```

- `T` → The type of elements in the stream.  
- Returns an **unmodifiable `Set<T>`**, meaning you **cannot add, remove, or modify** elements after collection.  
- Throws **`UnsupportedOperationException`** if modification is attempted.  

---

## **Difference Between `toSet()` and `toUnmodifiableSet()`**  

| Feature | `toSet()` | `toUnmodifiableSet()` |
|---------|-----------|------------------------|
| **Modifiability** | Mutable | Immutable (Read-only) |
| **Modification Methods (`add()`, `remove()`)** | Allowed | Throws `UnsupportedOperationException` |
| **Java Version** | Java 8+ | Java 10+ |
| **Duplicate Handling** | Removes duplicates | Removes duplicates |
| **Thread-Safety** | Not thread-safe | Thread-safe due to immutability |

---

## **Basic Examples of `toUnmodifiableSet()`**  

### **Collecting a Stream into an Unmodifiable Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ToUnmodifiableSetExample1 {
    public static void main(String[] args) {
        Set<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Alice")
                                  .stream()
                                  .collect(Collectors.toUnmodifiableSet());

        System.out.println("Unmodifiable Set: " + names);

        // Attempting modification will throw UnsupportedOperationException
        names.add("David");  // Throws Exception
    }
}
```

### **Explanation**  
- `.collect(Collectors.toUnmodifiableSet())` converts the **stream** into an **immutable set**.  
- **Duplicate values are automatically removed**.  
- **Throws `UnsupportedOperationException`** if `add()`, `remove()`, or `clear()` is called.  

---

## **Using `toUnmodifiableSet()` with Different Data Types**  

### **Collecting Integers into an Unmodifiable Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ToUnmodifiableSetExample2 {
    public static void main(String[] args) {
        Set<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50, 30, 20)
                                     .stream()
                                     .collect(Collectors.toUnmodifiableSet());

        System.out.println("Unmodifiable Set: " + numbers);

        numbers.remove(10); // Throws Exception
    }
}
```

### **Explanation**  
- **Ensures uniqueness** by removing duplicates.  
- **Immutable set**, so modifications **are not allowed**.  

---

## **Filtering and Collecting into an Unmodifiable Set**  

### **Filtering Even Numbers and Collecting into an Unmodifiable Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ToUnmodifiableSetExample3 {
    public static void main(String[] args) {
        Set<Integer> evenNumbers = Arrays.asList(5, 10, 15, 20, 25, 30, 10)
                                         .stream()
                                         .filter(n -> n % 2 == 0)
                                         .collect(Collectors.toUnmodifiableSet());

        System.out.println("Even Numbers: " + evenNumbers);

        evenNumbers.add(40); // Throws Exception
    }
}
```

### **Explanation**  
- `.filter(n -> n % 2 == 0)` selects **only even numbers**.  
- `.collect(Collectors.toUnmodifiableSet())` ensures **immutable results**.  

---

## **Sorting and Collecting into an Unmodifiable Set**  

### **Sorting a Set Before Collecting into an Unmodifiable Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.stream.Collectors;

public class ToUnmodifiableSetExample4 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Charlie", "Alice", "Bob", "Alice");

        Set<String> sortedNames = names.stream()
                                       .collect(Collectors.toUnmodifiableSet());

        System.out.println("Unmodifiable Set: " + sortedNames);

        sortedNames.remove("Charlie"); // Throws Exception
    }
}
```

### **Explanation**  
- `toUnmodifiableSet()` does **not guarantee order**, so if sorting is needed, use **`toCollection(TreeSet::new)`**.  
- **No modifications are allowed after collection**.

---

## **Mapping and Collecting into an Unmodifiable Set**  

### **Extracting Names from a List of Employees**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToUnmodifiableSetExample5 {
    public static void main(String[] args) {
        Set<String> employeeNames = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000)
        ).stream()
        .map(emp -> emp.name)
        .collect(Collectors.toUnmodifiableSet());

        System.out.println("Employee Names: " + employeeNames);

        employeeNames.add("David"); // Throws Exception
    }
}
```

### **Explanation**  
- `.map(emp -> emp.name)` extracts **employee names**.  
- `.collect(Collectors.toUnmodifiableSet())` ensures **set is immutable**.

---

## **Advanced Use Cases of `toUnmodifiableSet()`**  

### **Finding High-Salary Employees and Collecting Their Names into an Unmodifiable Set**  

```java
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToUnmodifiableSetExample6 {
    public static void main(String[] args) {
        Set<String> highEarners = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        ).stream()
        .filter(emp -> emp.salary > 60000)
        .map(emp -> emp.name)
        .collect(Collectors.toUnmodifiableSet());

        System.out.println("High-Earning Employees: " + highEarners);

        highEarners.remove("Alice"); // Throws Exception
    }
}
```

### **Explanation**  
- `.filter(emp -> emp.salary > 60000)` selects **high-earning employees**.  
- `.collect(Collectors.toUnmodifiableSet())` makes the resulting set **immutable**.  

---

## **Key Advantages of `toUnmodifiableSet()`**  

- **Prevents accidental modifications**  
- **Ensures thread safety** (no external modifications possible)  
- **Better performance** in cases where an unmodifiable collection is required  
- **Ideal for returning API results that should not be modified**  

---

## **Comparison: `toSet()` vs `toUnmodifiableSet()`**  

| Feature | `toSet()` | `toUnmodifiableSet()` |
|---------|----------|------------------|
| **Modifiable** | Yes | No |
| **Java Version** | Java 8+ | Java 10+ |
| **Performance** | Fast | Slightly better for read-only operations |
| **Best Used For** | Mutable Sets | Read-only Sets |

---

## **Conclusion**  

The `Collectors.toUnmodifiableSet()` method is useful for:  

- **Creating immutable sets that prevent modifications**  
- **Ensuring data integrity and thread safety**  
- **Collecting elements from a stream while avoiding accidental mutations**  
- **Providing better performance for read-only use cases**  

This method is a great choice when you want to **protect collections** from modification while leveraging the power of Java Streams.

# What is Collectors.toUnmodifiableMap()?
The `Collectors.toUnmodifiableMap()` method in **Java 10** is a **terminal operation** in the Stream API that **collects elements into an immutable (unmodifiable) `Map`**.  

Unlike `Collectors.toMap()`, which returns a **mutable** `Map`, `Collectors.toUnmodifiableMap()` creates a **read-only** map that **cannot be modified** after creation.  

This method ensures **data integrity** by preventing accidental modifications to the collected map.

---

## **Syntax of `Collectors.toUnmodifiableMap()`**  

### **1. Basic Syntax (Key-Value Mapping)**
```java
<R, K, V> Collector<T, ?, Map<K, V>> toUnmodifiableMap(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper);
```
- `T` → Type of elements in the stream  
- `K` → Key type in the resulting `Map`  
- `V` → Value type in the resulting `Map`  
- `keyMapper` → Function to extract the key  
- `valueMapper` → Function to extract the value  

### **2. Handling Key Collisions (Duplicate Keys)**
```java
<R, K, V> Collector<T, ?, Map<K, V>> toUnmodifiableMap(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends V> valueMapper, BinaryOperator<V> mergeFunction);
```
- `mergeFunction` → Defines how to handle **duplicate keys**  

---

## **Difference Between `toMap()` and `toUnmodifiableMap()`**  

| Feature | `toMap()` | `toUnmodifiableMap()` |
|---------|-----------|------------------------|
| **Modifiability** | Mutable | Immutable (Read-only) |
| **Modification Methods (`put()`, `remove()`)** | Allowed | Throws `UnsupportedOperationException` |
| **Java Version** | Java 8+ | Java 10+ |
| **Duplicate Key Handling** | Must specify merge function | Must specify merge function |
| **Thread-Safety** | Not thread-safe | Thread-safe due to immutability |

---

## **Basic Examples of `toUnmodifiableMap()`**  

### **Collecting a List into an Unmodifiable Map (Basic Key-Value Mapping)**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ToUnmodifiableMapExample1 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        Map<String, Double> productPriceMap = products.stream()
                                                      .collect(Collectors.toUnmodifiableMap(p -> p.name, p -> p.price));

        System.out.println("Product Price Map: " + productPriceMap);

        productPriceMap.put("Smartwatch", 300.00); // Throws Exception
    }
}
```

### **Explanation**
- **Key** → Product Name (`p.name`)  
- **Value** → Product Price (`p.price`)  
- **Throws `UnsupportedOperationException`** if modification is attempted.  

---

## **Handling Duplicate Keys Using a Merge Function**  

### **Example: Handling Employees with the Same Department**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class ToUnmodifiableMapExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT") // Same department as Alice
        );

        Map<String, String> departmentMap = employees.stream()
                                                     .collect(Collectors.toUnmodifiableMap(
                                                         e -> e.department, // Key: Department
                                                         e -> e.name,       // Value: Employee Name
                                                         (name1, name2) -> name1 + ", " + name2 // Merge function
                                                     ));

        System.out.println("Department Map: " + departmentMap);
    }
}
```

### **Explanation**
- **Key** → Department (`e.department`)  
- **Value** → Employee Name (`e.name`)  
- **Merge Function** → If a **duplicate key** is found, it **merges values** into a string.  
- **Throws `UnsupportedOperationException`** if modification is attempted.  

---

## **Using a Custom Map Implementation (TreeMap for Sorting)**  

### **Example: Sorting Product Names Alphabetically**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.TreeMap;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

public class ToUnmodifiableMapExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        Map<String, Double> sortedProductMap = products.stream()
                                                       .collect(Collectors.toUnmodifiableMap(
                                                           p -> p.name,
                                                           p -> p.price,
                                                           (existing, replacement) -> existing // No duplicate handling needed
                                                       ));

        System.out.println("Sorted Product Map: " + sortedProductMap);
    }
}
```

### **Explanation**
- **Stores products in an immutable map**  
- **Throws `UnsupportedOperationException`** if modification is attempted  

---

## **Mapping Employee Names to Salaries With Duplicate Handling**  

### **Example: Storing Employee Salary in a Map**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class ToUnmodifiableMapExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 80000),
            new Employee("Alice", 75000) // Duplicate name with higher salary
        );

        Map<String, Double> highestSalaryMap = employees.stream()
                                                        .collect(Collectors.toUnmodifiableMap(
                                                            e -> e.name,
                                                            e -> e.salary,
                                                            Double::max // Keep the highest salary for duplicate names
                                                        ));

        System.out.println("Highest Salary Map: " + highestSalaryMap);
    }
}
```

### **Explanation**
- **Keeps only the highest salary for duplicate employees**  
- **Throws `UnsupportedOperationException`** if modification is attempted  

---

## **Key Advantages of `toUnmodifiableMap()`**  

- **Prevents accidental modifications**  
- **Ensures thread safety** (no external modifications possible)  
- **Better performance** in cases where an unmodifiable collection is required  
- **Ideal for returning API results that should not be modified**  

---

## **Comparison: `toMap()` vs `toUnmodifiableMap()`**  

| Feature | `toMap()` | `toUnmodifiableMap()` |
|---------|----------|------------------|
| **Modifiable** | Yes | No |
| **Java Version** | Java 8+ | Java 10+ |
| **Performance** | Fast | Slightly better for read-only operations |
| **Best Used For** | Mutable Maps | Read-only Maps |

---

## **Conclusion**  

The `Collectors.toUnmodifiableMap()` method is useful for:  

- **Creating immutable maps that prevent modifications**  
- **Ensuring data integrity and thread safety**  
- **Collecting elements from a stream while avoiding accidental mutations**  
- **Providing better performance for read-only use cases**  

This method is a great choice when you want to **protect collections** from modification while leveraging the power of Java Streams.

# What is Collectors.groupingBy()?

The `Collectors.groupingBy()` method in Java 8 Stream API is a **terminal operation** used for **grouping elements** from a stream into a `Map`.  

It is useful for **categorizing, counting, summing, and aggregating data** efficiently. This method is equivalent to the SQL `GROUP BY` operation.

---

## **Syntax of `Collectors.groupingBy()`**  

### **1. Basic Syntax (Grouping by a Key)**
```java
Map<K, List<T>> groupingBy(Function<? super T, ? extends K> classifier);
```
- `T` → Type of elements in the stream  
- `K` → Key type in the resulting `Map`  
- `classifier` → Function to extract the grouping key  

### **2. Grouping with Downstream Collector (Aggregation)**
```java
Map<K, D> groupingBy(Function<? super T, ? extends K> classifier, Collector<? super T, ?, D> downstream);
```
- Allows **post-processing**, such as **counting, summing, or averaging** the grouped data  

### **3. Using a Custom Map Implementation**
```java
Map<K, D> groupingBy(Function<? super T, ? extends K> classifier, Supplier<M> mapSupplier, Collector<? super T, ?, D> downstream);
```
- Uses a **specific Map type** (e.g., `TreeMap`)  

---

## **Basic Examples of `groupingBy()`**  

### **Grouping a List of Strings by Their First Letter**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingByExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Anna", "Brian");

        Map<Character, List<String>> groupedNames = names.stream()
                                                         .collect(Collectors.groupingBy(name -> name.charAt(0)));

        System.out.println("Grouped Names: " + groupedNames);
    }
}
```
### **Explanation**  
- Groups names based on their **first letter** (`charAt(0)`).  
- **Output**: `{A=[Alice, Anna], B=[Bob, Brian], C=[Charlie]}`  

---

### **Grouping Employees by Department**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class GroupingByExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR"),
            new Employee("Eve", "Finance")
        );

        Map<String, List<Employee>> employeesByDepartment = employees.stream()
                                                                     .collect(Collectors.groupingBy(emp -> emp.department));

        System.out.println("Employees Grouped by Department: " + employeesByDepartment);
    }
}
```
### **Explanation**  
- Groups employees based on their **department**.  
- **Output**: `{IT=[Alice, Charlie], HR=[Bob, David], Finance=[Eve]}`  

---

## **Advanced Use Cases of `groupingBy()`**

### **Counting Elements in Each Group (`counting()`)**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingByExample3 {
    public static void main(String[] args) {
        List<String> items = Arrays.asList("Apple", "Banana", "Orange", "Apple", "Banana", "Apple");

        Map<String, Long> itemCounts = items.stream()
                                            .collect(Collectors.groupingBy(item -> item, Collectors.counting()));

        System.out.println("Item Counts: " + itemCounts);
    }
}
```
### **Explanation**  
- Groups items by name and **counts** occurrences.  
- **Output**: `{Apple=3, Banana=2, Orange=1}`  

---

### **Summing Values in Each Group (`summingInt()`)**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Transaction {
    String category;
    int amount;

    public Transaction(String category, int amount) {
        this.category = category;
        this.amount = amount;
    }
}

public class GroupingByExample4 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("Food", 50),
            new Transaction("Electronics", 200),
            new Transaction("Food", 30),
            new Transaction("Electronics", 150)
        );

        Map<String, Integer> totalAmountByCategory = transactions.stream()
                                                                 .collect(Collectors.groupingBy(
                                                                     t -> t.category,
                                                                     Collectors.summingInt(t -> t.amount)
                                                                 ));

        System.out.println("Total Amount by Category: " + totalAmountByCategory);
    }
}
```
### **Explanation**  
- Groups transactions by **category** and **sums** the amounts.  
- **Output**: `{Food=80, Electronics=350}`  

---

### **Finding Maximum Value in Each Group (`maxBy()`)**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

class Employee {
    String department;
    double salary;

    public Employee(String department, double salary) {
        this.department = department;
        this.salary = salary;
    }
}

public class GroupingByExample5 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("IT", 70000),
            new Employee("HR", 50000),
            new Employee("IT", 90000),
            new Employee("HR", 60000)
        );

        Map<String, Optional<Employee>> highestSalaryByDepartment = employees.stream()
                                                                             .collect(Collectors.groupingBy(
                                                                                 e -> e.department,
                                                                                 Collectors.maxBy((e1, e2) -> Double.compare(e1.salary, e2.salary))
                                                                             ));

        System.out.println("Highest Salary by Department: " + highestSalaryByDepartment);
    }
}
```
### **Explanation**  
- Groups employees by **department** and finds the **highest salary**.  
- **Output**: `{IT=90000, HR=60000}`  

---

### **Using a Custom `Map` Implementation (`TreeMap` for Sorting)**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class GroupingByExample6 {
    public static void main(String[] args) {
        List<String> cities = Arrays.asList("New York", "Los Angeles", "Chicago", "Houston", "Phoenix");

        Map<Character, List<String>> sortedGroupedCities = cities.stream()
                                                                 .collect(Collectors.groupingBy(
                                                                     city -> city.charAt(0),
                                                                     TreeMap::new, // Use TreeMap for sorted keys
                                                                     Collectors.toList()
                                                                 ));

        System.out.println("Cities Grouped and Sorted by First Letter: " + sortedGroupedCities);
    }
}
```
### **Explanation**  
- Uses **`TreeMap`** to **sort** the grouped cities by **first letter**.  
- **Output**: `{C=[Chicago], H=[Houston], L=[Los Angeles], N=[New York], P=[Phoenix]}`  

---

## **Comparison: `groupingBy()` vs Other Collectors**  

| Feature | `groupingBy()` | `partitioningBy()` | `toMap()` |
|---------|--------------|----------------|--------|
| **Groups by** | Key function | Boolean condition | Unique keys |
| **Output Type** | `Map<K, List<T>>` | `Map<Boolean, List<T>>` | `Map<K, V>` |
| **Supports Aggregation** | Yes | No | No |
| **Allows Custom Map** | Yes | No | Yes |

---

## **Conclusion**  

The `Collectors.groupingBy()` method is useful for:  

- **Categorizing elements** based on attributes  
- **Counting, summing, and finding min/max in groups**  
- **Sorting grouped data using custom `Map` implementations**  
- **Mimicking SQL `GROUP BY` in Java Streams**  

This method is a powerful tool for handling **large-scale data processing efficiently** in Java Streams.

Here are **advanced real-world scenarios** using `Collectors.groupingBy()`, combined with **aggregation, sorting, partitioning, nested grouping, and custom collectors**.

---

## **1. Grouping Orders by Customer and Summing Order Amounts**
### **Scenario**: Convert a list of orders into a `Map` where **Customer Name** is the key and the **Total Order Amount** is the value.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Order {
    String customer;
    double amount;

    public Order(String customer, double amount) {
        this.customer = customer;
        this.amount = amount;
    }
}

public class GroupingByAdvancedExample1 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order("Alice", 250.75),
            new Order("Bob", 500.50),
            new Order("Alice", 1200.00),
            new Order("Charlie", 450.25)
        );

        Map<String, Double> totalSpentByCustomer = orders.stream()
                                                         .collect(Collectors.groupingBy(
                                                             o -> o.customer,
                                                             Collectors.summingDouble(o -> o.amount)
                                                         ));

        System.out.println("Total Spent by Customer: " + totalSpentByCustomer);
    }
}
```
### **Explanation**
- Groups **orders by customer**.
- **Sums order amounts** for each customer.
- **Output**: `{Alice=1450.75, Bob=500.5, Charlie=450.25}`.

---

## **2. Grouping Products by Category and Counting Them**
### **Scenario**: Convert a list of products into a `Map` where **Category** is the key and the **Count of Products** in that category is the value.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class GroupingByAdvancedExample2 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics"),
            new Product("Phone", "Electronics"),
            new Product("Shoes", "Fashion"),
            new Product("Tablet", "Electronics"),
            new Product("Jacket", "Fashion")
        );

        Map<String, Long> productCountByCategory = products.stream()
                                                           .collect(Collectors.groupingBy(
                                                               p -> p.category,
                                                               Collectors.counting()
                                                           ));

        System.out.println("Product Count by Category: " + productCountByCategory);
    }
}
```
### **Explanation**
- Groups **products by category**.
- **Counts** how many products are in each category.
- **Output**: `{Electronics=3, Fashion=2}`.

---

## **3. Grouping Employees by Department and Finding Maximum Salary**
### **Scenario**: Convert a list of employees into a `Map` where **Department** is the key and the **Employee with the Highest Salary** is the value.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }
}

public class GroupingByAdvancedExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 90000),
            new Employee("David", "HR", 60000)
        );

        Map<String, Optional<Employee>> highestSalaryByDepartment = employees.stream()
                                                                             .collect(Collectors.groupingBy(
                                                                                 e -> e.department,
                                                                                 Collectors.maxBy((e1, e2) -> Double.compare(e1.salary, e2.salary))
                                                                             ));

        System.out.println("Highest Salary by Department: " + highestSalaryByDepartment);
    }
}
```
### **Explanation**
- Groups **employees by department**.
- **Finds the employee with the highest salary** in each department.
- **Output**: `{IT=Charlie ($90000), HR=David ($60000)}`.

---

## **4. Nested Grouping: Grouping Students by Grade, Then by Section**
### **Scenario**: Convert a list of students into a `Map` where **Grade** is the primary key and **Section** is the secondary key.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int grade;
    String section;

    public Student(String name, int grade, String section) {
        this.name = name;
        this.grade = grade;
        this.section = section;
    }
}

public class GroupingByAdvancedExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 10, "A"),
            new Student("Bob", 10, "B"),
            new Student("Charlie", 11, "A"),
            new Student("David", 11, "B"),
            new Student("Eve", 10, "A")
        );

        Map<Integer, Map<String, List<Student>>> studentsByGradeAndSection = students.stream()
                                                                                    .collect(Collectors.groupingBy(
                                                                                        s -> s.grade,
                                                                                        Collectors.groupingBy(s -> s.section)
                                                                                    ));

        System.out.println("Students Grouped by Grade and Section: " + studentsByGradeAndSection);
    }
}
```
### **Explanation**
- Groups **students by grade** first.
- Then **groups them by section** inside each grade.
- **Output**:
```java
{
    10={A=[Alice, Eve], B=[Bob]},
    11={A=[Charlie], B=[David]}
}
```

---

## **5. Partitioning Employees into High and Low Salary Groups**
### **Scenario**: Partition employees into **high-salary** (above 60,000) and **low-salary** (60,000 or below).

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class GroupingByAdvancedExample5 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000),
            new Employee("David", 60000)
        );

        Map<Boolean, List<Employee>> salaryPartition = employees.stream()
                                                                .collect(Collectors.partitioningBy(e -> e.salary > 60000));

        System.out.println("Employees Partitioned by Salary: " + salaryPartition);
    }
}
```
### **Explanation**
- **Partitioning** instead of grouping (true = high salary, false = low salary).
- **Output**:
```java
{
    true=[Alice ($70000), Charlie ($120000)],
    false=[Bob ($50000), David ($60000)]
}
```

---

## **6. Using a `TreeMap` to Sort Grouped Data**
### **Scenario**: Group employees by department and use a **TreeMap** to ensure department names are **sorted**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class GroupingByAdvancedExample6 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "Finance"),
            new Employee("David", "IT")
        );

        Map<String, List<Employee>> sortedEmployeesByDepartment = employees.stream()
                                                                          .collect(Collectors.groupingBy(
                                                                              e -> e.department,
                                                                              TreeMap::new,
                                                                              Collectors.toList()
                                                                          ));

        System.out.println("Employees Grouped by Department (Sorted): " + sortedEmployeesByDepartment);
    }
}
```
### **Explanation**
- Uses **`TreeMap`** to **sort departments alphabetically**.
- **Output**: `{Finance=[Charlie], HR=[Bob], IT=[Alice, David]}`.


# What is Collectors.groupingByConcurrent()?
The `Collectors.groupingByConcurrent()` method in Java 8 Stream API is a **terminal operation** that **performs concurrent grouping** of stream elements into a `ConcurrentMap`.  

It is similar to `Collectors.groupingBy()`, but optimized for **parallel streams**, making it useful in **multi-threaded environments** for efficient data processing.

---

## **Syntax of `Collectors.groupingByConcurrent()`**  

### **1. Basic Syntax (Concurrent Grouping by a Key)**
```java
ConcurrentMap<K, List<T>> groupingByConcurrent(Function<? super T, ? extends K> classifier);
```
- `T` → Type of elements in the stream.  
- `K` → Key type in the resulting `ConcurrentMap`.  
- `classifier` → Function to extract the grouping key.  
- **Returns** a `ConcurrentMap<K, List<T>>`.  

### **2. Grouping with a Downstream Collector (Aggregation)**
```java
ConcurrentMap<K, D> groupingByConcurrent(Function<? super T, ? extends K> classifier, Collector<? super T, ?, D> downstream);
```
- Allows **counting, summing, averaging, finding max/min values, etc.**  

### **3. Using a Custom Concurrent Map Implementation**
```java
ConcurrentMap<K, D> groupingByConcurrent(Function<? super T, ? extends K> classifier, Supplier<M> mapSupplier, Collector<? super T, ?, D> downstream);
```
- Uses a **specific `ConcurrentMap` type** (e.g., `ConcurrentSkipListMap`).  

---

## **Difference Between `groupingBy()` and `groupingByConcurrent()`**  

| Feature | `groupingBy()` | `groupingByConcurrent()` |
|---------|--------------|------------------------|
| **Map Type** | `HashMap` (or user-defined) | `ConcurrentHashMap` |
| **Thread-Safety** | Not thread-safe | Thread-safe |
| **Parallel Processing** | Inefficient | Optimized for parallel streams |
| **Best Use Case** | Single-threaded processing | Multi-threaded or parallel processing |

---

## **Basic Examples of `groupingByConcurrent()`**  

### **Grouping Names by First Letter Concurrently**  
```java
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class GroupingByConcurrentExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Anna", "Brian");

        ConcurrentMap<Character, List<String>> groupedNames = names.parallelStream()
                                                                   .collect(Collectors.groupingByConcurrent(name -> name.charAt(0)));

        System.out.println("Concurrent Grouped Names: " + groupedNames);
    }
}
```
### **Explanation**  
- Groups names by **first letter**.  
- Uses **parallel stream** for faster execution.  
- **Output**: `{A=[Alice, Anna], B=[Bob, Brian], C=[Charlie]}`.  

---

## **Advanced Use Cases of `groupingByConcurrent()`**

### **Counting Elements in Each Group Concurrently**
```java
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class GroupingByConcurrentExample2 {
    public static void main(String[] args) {
        List<String> items = Arrays.asList("Apple", "Banana", "Orange", "Apple", "Banana", "Apple");

        ConcurrentMap<String, Long> itemCounts = items.parallelStream()
                                                      .collect(Collectors.groupingByConcurrent(item -> item, Collectors.counting()));

        System.out.println("Concurrent Item Counts: " + itemCounts);
    }
}
```
### **Explanation**  
- Groups items **concurrently** by name.  
- **Counts** occurrences in each group.  
- **Output**: `{Apple=3, Banana=2, Orange=1}`.  

---

### **Summing Values in Each Group Concurrently**
```java
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

class Transaction {
    String category;
    int amount;

    public Transaction(String category, int amount) {
        this.category = category;
        this.amount = amount;
    }
}

public class GroupingByConcurrentExample3 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("Food", 50),
            new Transaction("Electronics", 200),
            new Transaction("Food", 30),
            new Transaction("Electronics", 150)
        );

        ConcurrentMap<String, Integer> totalAmountByCategory = transactions.parallelStream()
                                                                          .collect(Collectors.groupingByConcurrent(
                                                                              t -> t.category,
                                                                              Collectors.summingInt(t -> t.amount)
                                                                          ));

        System.out.println("Concurrent Total Amount by Category: " + totalAmountByCategory);
    }
}
```
### **Explanation**  
- Groups transactions by **category** and **sums** the amounts **concurrently**.  
- **Output**: `{Food=80, Electronics=350}`.  

---

### **Finding Maximum Salary in Each Department Concurrently**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

class Employee {
    String department;
    double salary;

    public Employee(String department, double salary) {
        this.department = department;
        this.salary = salary;
    }
}

public class GroupingByConcurrentExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("IT", 70000),
            new Employee("HR", 50000),
            new Employee("IT", 90000),
            new Employee("HR", 60000)
        );

        ConcurrentMap<String, Optional<Employee>> highestSalaryByDepartment = employees.parallelStream()
                                                                                       .collect(Collectors.groupingByConcurrent(
                                                                                           e -> e.department,
                                                                                           Collectors.maxBy((e1, e2) -> Double.compare(e1.salary, e2.salary))
                                                                                       ));

        System.out.println("Concurrent Highest Salary by Department: " + highestSalaryByDepartment);
    }
}
```
### **Explanation**  
- Groups employees **by department** and finds **max salary per department** **concurrently**.  
- **Output**: `{IT=90000, HR=60000}`.  

---

### **Nested Grouping: Grouping Orders by Customer, Then by Category Concurrently**
```java
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

class Order {
    String customer;
    String category;
    double amount;

    public Order(String customer, String category, double amount) {
        this.customer = customer;
        this.category = category;
        this.amount = amount;
    }
}

public class GroupingByConcurrentExample5 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order("Alice", "Electronics", 500.0),
            new Order("Alice", "Groceries", 200.0),
            new Order("Bob", "Electronics", 150.0),
            new Order("Bob", "Groceries", 100.0),
            new Order("Alice", "Electronics", 300.0)
        );

        ConcurrentMap<String, ConcurrentMap<String, List<Order>>> ordersByCustomerAndCategory = orders.parallelStream()
                                                                                                      .collect(Collectors.groupingByConcurrent(
                                                                                                          o -> o.customer,
                                                                                                          Collectors.groupingByConcurrent(o -> o.category)
                                                                                                      ));

        System.out.println("Concurrent Orders Grouped by Customer and Category: " + ordersByCustomerAndCategory);
    }
}
```
### **Explanation**  
- Groups **orders by customer** and then **by category** **concurrently**.  
- **Output**: 
```java
{
    Alice={Electronics=[500.0, 300.0], Groceries=[200.0]},
    Bob={Electronics=[150.0], Groceries=[100.0]}
}
```

---

## **Key Advantages of `groupingByConcurrent()`**  

- **Improved Performance with Parallel Streams**  
- **Thread-Safe Processing** using `ConcurrentHashMap`  
- **Efficient for Large Datasets**  
- **Supports Aggregation (counting, summing, max/min)**  

---

## **Comparison: `groupingBy()` vs `groupingByConcurrent()`**  

| Feature | `groupingBy()` | `groupingByConcurrent()` |
|---------|--------------|------------------|
| **Thread-Safety** | No | Yes |
| **Parallel Stream Support** | Not optimized | Optimized |
| **Performance** | Slower in multi-threading | Faster in parallel execution |
| **Best Used For** | Single-threaded processing | Multi-threaded environments |

---

## **Conclusion**  

The `Collectors.groupingByConcurrent()` method is useful for:  

- **High-performance concurrent data grouping**  
- **Parallel stream processing for large datasets**  
- **Thread-safe operations on shared data**  

Here is a **tabular difference** between `Collectors.groupingBy()` and `Collectors.groupingByConcurrent()`:

| Feature | `groupingBy()` | `groupingByConcurrent()` |
|---------|--------------|--------------------------|
| **Thread-Safety** | Not thread-safe | Thread-safe (uses `ConcurrentHashMap`) |
| **Parallel Stream Support** | Not optimized | Optimized for parallel streams |
| **Performance** | Slower for large data with parallel processing | Faster due to concurrent execution |
| **Underlying Map** | Uses `HashMap` by default | Uses `ConcurrentHashMap` |
| **Best Use Case** | Single-threaded processing | Multi-threaded or parallel stream processing |
| **Supports Aggregation** | Yes | Yes |
| **Ordering of Elements** | Preserves insertion order (depends on the map implementation) | No guaranteed order (due to `ConcurrentHashMap`) |
| **Memory Overhead** | Lower memory overhead | Slightly higher due to concurrency mechanisms |
| **Usage in High-Volume Data Processing** | Less efficient in multi-threaded environments | More efficient for concurrent workloads |
| **Custom Map Implementation** | Can specify a custom `Map` type (e.g., `TreeMap`) | Can specify a custom `ConcurrentMap` type (e.g., `ConcurrentSkipListMap`) |

### **Key Takeaways**
- Use **`groupingBy()`** when working with **single-threaded** streams or if you need **ordered results**.
- Use **`groupingByConcurrent()`** when working with **parallel streams** for better **performance and thread safety**.

# What is Collectors.partitioningBy()?
The `Collectors.partitioningBy()` method in Java 8 Stream API is a **terminal operation** that **divides stream elements into two groups** based on a **predicate condition**.  

Unlike `Collectors.groupingBy()`, which can create **multiple groups**, `partitioningBy()` always creates **two partitions**:
1. **One partition (key = `true`)** → Contains elements that satisfy the predicate.
2. **Another partition (key = `false`)** → Contains elements that do not satisfy the predicate.

This method returns a `Map<Boolean, List<T>>`, where:
- The **key `true`** holds elements that pass the condition.
- The **key `false`** holds elements that fail the condition.

---

## **Syntax of `Collectors.partitioningBy()`**  

### **1. Basic Partitioning (Using Predicate)**
```java
Map<Boolean, List<T>> partitioningBy(Predicate<? super T> predicate);
```
- **Returns a `Map<Boolean, List<T>>`** where:
  - `true` → Elements matching the condition.
  - `false` → Elements not matching the condition.

### **2. Partitioning with a Downstream Collector**
```java
Map<Boolean, D> partitioningBy(Predicate<? super T> predicate, Collector<? super T, ?, D> downstream);
```
- Allows **aggregating**, **counting**, **summing**, or **custom transformations** within partitions.

---

## **Difference Between `groupingBy()` and `partitioningBy()`**  

| Feature | `groupingBy()` | `partitioningBy()` |
|---------|--------------|------------------|
| **Number of Groups** | Can create multiple groups | Always creates **two partitions** (true/false) |
| **Return Type** | `Map<K, List<T>>` (keys depend on classifier) | `Map<Boolean, List<T>>` |
| **Predicate Usage** | Not required | Mandatory (used for true/false classification) |
| **Performance** | Slower for binary classification | Faster, as it only creates **two groups** |

---

## **Basic Examples of `partitioningBy()`**

### **1. Partitioning Numbers into Even and Odd**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PartitioningByExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 15, 20, 25, 30);

        Map<Boolean, List<Integer>> evenOddPartition = numbers.stream()
                                                              .collect(Collectors.partitioningBy(n -> n % 2 == 0));

        System.out.println("Even Numbers: " + evenOddPartition.get(true));
        System.out.println("Odd Numbers: " + evenOddPartition.get(false));
    }
}
```
### **Explanation**
- **Predicate Condition:** `n % 2 == 0` (checks if the number is even).
- **Output**:
  ```
  Even Numbers: [10, 20, 30]
  Odd Numbers: [15, 25]
  ```

---

### **2. Partitioning Employees Based on Salary Threshold**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class PartitioningByExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000),
            new Employee("David", 60000)
        );

        Map<Boolean, List<Employee>> salaryPartition = employees.stream()
                                                                .collect(Collectors.partitioningBy(e -> e.salary > 60000));

        System.out.println("High Salary Employees: " + salaryPartition.get(true));
        System.out.println("Low Salary Employees: " + salaryPartition.get(false));
    }
}
```
### **Explanation**
- **Predicate Condition:** Employees earning more than `$60,000` are classified as **high salary**.
- **Output**:
  ```
  High Salary Employees: [Alice ($70000), Charlie ($120000)]
  Low Salary Employees: [Bob ($50000), David ($60000)]
  ```

---

## **Advanced Use Cases of `partitioningBy()`**

### **3. Counting Elements in Each Partition**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PartitioningByExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 15, 20, 25, 30);

        Map<Boolean, Long> countPartition = numbers.stream()
                                                   .collect(Collectors.partitioningBy(n -> n % 2 == 0, Collectors.counting()));

        System.out.println("Count of Even Numbers: " + countPartition.get(true));
        System.out.println("Count of Odd Numbers: " + countPartition.get(false));
    }
}
```
### **Explanation**
- Uses `Collectors.counting()` to **count** elements in each partition.
- **Output**:
  ```
  Count of Even Numbers: 3
  Count of Odd Numbers: 2
  ```

---

### **4. Partitioning Students Based on Passing Grade and Collecting Names**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}

public class PartitioningByExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75),
            new Student("David", 35)
        );

        Map<Boolean, List<String>> passFailStudents = students.stream()
                                                              .collect(Collectors.partitioningBy(
                                                                  s -> s.marks >= 50, 
                                                                  Collectors.mapping(s -> s.name, Collectors.toList())
                                                              ));

        System.out.println("Passed Students: " + passFailStudents.get(true));
        System.out.println("Failed Students: " + passFailStudents.get(false));
    }
}
```
### **Explanation**
- Uses `Collectors.mapping()` to collect **only names** in each partition.
- **Output**:
  ```
  Passed Students: [Alice, Charlie]
  Failed Students: [Bob, David]
  ```

---

### **5. Finding the Average Salary in Each Partition**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class PartitioningByExample5 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000),
            new Employee("David", 60000)
        );

        Map<Boolean, Double> avgSalaryPartition = employees.stream()
                                                           .collect(Collectors.partitioningBy(
                                                               e -> e.salary > 60000, 
                                                               Collectors.averagingDouble(e -> e.salary)
                                                           ));

        System.out.println("Average Salary of High Earners: " + avgSalaryPartition.get(true));
        System.out.println("Average Salary of Low Earners: " + avgSalaryPartition.get(false));
    }
}
```
### **Explanation**
- Uses `Collectors.averagingDouble()` to find **average salary in each partition**.
- **Output**:
  ```
  Average Salary of High Earners: 95000.0
  Average Salary of Low Earners: 55000.0
  ```

---

## **Conclusion**
The `Collectors.partitioningBy()` method is useful for:
- **Efficient binary classification** (true/false).
- **Better performance than `groupingBy()` for two-group scenarios**.
- **Works well with counting, summing, and averaging operations**.
- **Ideal for pass/fail scenarios, salary classifications, even/odd numbers, etc.**.

## **Performance Comparison: `Collectors.groupingBy()` vs `Collectors.partitioningBy()`**  

### **Key Differences in Performance**
| Feature | `groupingBy()` | `partitioningBy()` |
|---------|--------------|------------------|
| **Number of Groups** | Can create multiple groups | Always creates **two partitions** (true/false) |
| **Return Type** | `Map<K, List<T>>` | `Map<Boolean, List<T>>` |
| **Predicate Usage** | Not required | Mandatory (for true/false classification) |
| **Memory Usage** | More memory usage for multiple groups | Lower memory usage (only two partitions) |
| **Processing Speed** | Slower for binary classification (must check and categorize into multiple groups) | Faster because it only needs **one boolean condition** |
| **Best Use Case** | When there are **multiple categories** | When there are **only two categories** (true/false) |

### **Performance Testing: `groupingBy()` vs `partitioningBy()`**
To compare the **performance** of both methods, we will **classify numbers** as **even or odd** using both `groupingBy()` and `partitioningBy()`, measuring execution time.

---

### **Java Code for Performance Test**
```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GroupingVsPartitioningPerformanceTest {
    public static void main(String[] args) {
        // Generate a large list of random integers
        List<Integer> numbers = IntStream.rangeClosed(1, 1_000_000).boxed().collect(Collectors.toList());

        // Measure time for groupingBy()
        long startTime1 = System.nanoTime();
        Map<String, List<Integer>> groupedNumbers = numbers.stream()
                .collect(Collectors.groupingBy(n -> n % 2 == 0 ? "Even" : "Odd"));
        long endTime1 = System.nanoTime();

        // Measure time for partitioningBy()
        long startTime2 = System.nanoTime();
        Map<Boolean, List<Integer>> partitionedNumbers = numbers.stream()
                .collect(Collectors.partitioningBy(n -> n % 2 == 0));
        long endTime2 = System.nanoTime();

        // Print execution times
        System.out.println("Execution Time for groupingBy(): " + (endTime1 - startTime1) / 1_000_000 + " ms");
        System.out.println("Execution Time for partitioningBy(): " + (endTime2 - startTime2) / 1_000_000 + " ms");
    }
}
```

---

### **Expected Results**
#### **1. Execution Time Comparison**
| Method | Expected Execution Time |
|---------|-------------------------|
| `groupingBy()` | **Slower (~5-10% slower for binary classification)** |
| `partitioningBy()` | **Faster (optimized for two groups)** |

#### **2. Memory Usage Comparison**
| Method | Memory Usage |
|---------|----------------|
| `groupingBy()` | Higher (allocates memory for multiple keys and lists) |
| `partitioningBy()` | Lower (allocates memory for only **two lists**) |

---

### **Why is `partitioningBy()` Faster?**
- **Uses a Boolean key (`true/false`)** → Simpler lookup compared to multiple keys.  
- **Optimized for two groups** → Reduces unnecessary computations.  
- **Avoids multiple conditional checks** → `groupingBy()` must check each element for multiple categories, whereas `partitioningBy()` only checks **one predicate**.  

---

### **When to Use Which?**
| **Scenario** | **Best Choice** |
|-------------|--------------|
| **Classifying elements into two groups (e.g., even/odd, pass/fail)** | `partitioningBy()` |
| **Grouping elements into multiple categories (e.g., age groups, departments, product categories)** | `groupingBy()` |
| **Memory efficiency for two groups** | `partitioningBy()` |
| **Complex aggregation, multiple groups required** | `groupingBy()` |

---

## **Conclusion**
- **If you only need two groups (true/false), use `partitioningBy()`** → Faster, more memory-efficient.  
- **If you need multiple groups, use `groupingBy()`** → More flexible but slightly slower for binary classification.  
- **For large datasets**, `partitioningBy()` performs better for **binary classification** tasks like **even/odd numbers, pass/fail students, salary-based classification, etc.**  

Would you like a **parallel stream performance comparison**? 🚀

## **Advanced Examples of `Collectors.partitioningBy()` in Java 8**  

Here are advanced real-world use cases using `Collectors.partitioningBy()` for **data classification, aggregation, parallel stream optimization, and custom downstream collectors**.

---

## **1. Partitioning Orders Based on High and Low Value**
### **Scenario:** Classify orders as **high-value (> $500)** and **low-value (≤ $500)**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Order {
    String customer;
    double amount;

    public Order(String customer, double amount) {
        this.customer = customer;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return customer + " ($" + amount + ")";
    }
}

public class PartitioningByAdvancedExample1 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order("Alice", 750.00),
            new Order("Bob", 200.00),
            new Order("Charlie", 1200.00),
            new Order("David", 400.00),
            new Order("Eve", 550.00)
        );

        Map<Boolean, List<Order>> partitionedOrders = orders.stream()
                                                            .collect(Collectors.partitioningBy(o -> o.amount > 500));

        System.out.println("High-Value Orders: " + partitionedOrders.get(true));
        System.out.println("Low-Value Orders: " + partitionedOrders.get(false));
    }
}
```
### **Explanation**
- **High-value orders (`> $500`)** are separated from **low-value orders (`≤ $500`)**.
- **Output**:
  ```
  High-Value Orders: [Alice ($750.0), Charlie ($1200.0), Eve ($550.0)]
  Low-Value Orders: [Bob ($200.0), David ($400.0)]
  ```

---

## **2. Partitioning Employees into Senior and Junior Levels**
### **Scenario:** Classify employees as **senior (experience ≥ 5 years)** and **junior (experience < 5 years)**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    int experience;

    public Employee(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    @Override
    public String toString() {
        return name + " (" + experience + " years)";
    }
}

public class PartitioningByAdvancedExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 7),
            new Employee("Bob", 3),
            new Employee("Charlie", 10),
            new Employee("David", 2),
            new Employee("Eve", 6)
        );

        Map<Boolean, List<Employee>> employeePartition = employees.stream()
                                                                 .collect(Collectors.partitioningBy(e -> e.experience >= 5));

        System.out.println("Senior Employees: " + employeePartition.get(true));
        System.out.println("Junior Employees: " + employeePartition.get(false));
    }
}
```
### **Explanation**
- **Senior employees** (5+ years of experience) are partitioned separately from **junior employees** (<5 years).
- **Output**:
  ```
  Senior Employees: [Alice (7 years), Charlie (10 years), Eve (6 years)]
  Junior Employees: [Bob (3 years), David (2 years)]
  ```

---

## **3. Partitioning Students Based on Scholarship Eligibility and Counting**
### **Scenario:** Classify students as **eligible for a scholarship (marks ≥ 75)** and **not eligible** while **counting each group**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}

public class PartitioningByAdvancedExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75),
            new Student("David", 35),
            new Student("Eve", 90)
        );

        Map<Boolean, Long> scholarshipCount = students.stream()
                                                      .collect(Collectors.partitioningBy(
                                                          s -> s.marks >= 75, Collectors.counting()));

        System.out.println("Scholarship Eligible Count: " + scholarshipCount.get(true));
        System.out.println("Not Eligible Count: " + scholarshipCount.get(false));
    }
}
```
### **Explanation**
- Uses `Collectors.counting()` to count students **eligible** and **not eligible** for the scholarship.
- **Output**:
  ```
  Scholarship Eligible Count: 3
  Not Eligible Count: 2
  ```

---

## **4. Partitioning Transactions and Summing Values**
### **Scenario:** Classify **debit** and **credit transactions** and calculate the **total amount for each type**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Transaction {
    String type; // "credit" or "debit"
    double amount;

    public Transaction(String type, double amount) {
        this.type = type;
        this.amount = amount;
    }
}

public class PartitioningByAdvancedExample4 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction("credit", 500.0),
            new Transaction("debit", 200.0),
            new Transaction("credit", 1000.0),
            new Transaction("debit", 300.0),
            new Transaction("credit", 150.0)
        );

        Map<Boolean, Double> transactionSums = transactions.stream()
                                                           .collect(Collectors.partitioningBy(
                                                               t -> t.type.equals("credit"),
                                                               Collectors.summingDouble(t -> t.amount)
                                                           ));

        System.out.println("Total Credit Amount: " + transactionSums.get(true));
        System.out.println("Total Debit Amount: " + transactionSums.get(false));
    }
}
```
### **Explanation**
- Uses `Collectors.summingDouble()` to calculate **total transaction amounts** for **credit** and **debit** transactions.
- **Output**:
  ```
  Total Credit Amount: 1650.0
  Total Debit Amount: 500.0
  ```

---

## **5. Parallel Stream Optimization for Large Data**
### **Scenario:** Optimize **performance** of partitioning employees **with parallel streams**.

```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Employee {
    int id;
    boolean remoteWorker;

    public Employee(int id, boolean remoteWorker) {
        this.id = id;
        this.remoteWorker = remoteWorker;
    }
}

public class PartitioningByAdvancedExample5 {
    public static void main(String[] args) {
        List<Employee> employees = IntStream.range(1, 1000000)
                                            .mapToObj(i -> new Employee(i, i % 2 == 0))
                                            .collect(Collectors.toList());

        long startTime = System.nanoTime();
        Map<Boolean, List<Employee>> partitionedEmployees = employees.parallelStream()
                                                                    .collect(Collectors.partitioningBy(e -> e.remoteWorker));
        long endTime = System.nanoTime();

        System.out.println("Remote Workers: " + partitionedEmployees.get(true).size());
        System.out.println("On-Site Workers: " + partitionedEmployees.get(false).size());
        System.out.println("Execution Time: " + (endTime - startTime) / 1_000_000 + " ms");
    }
}
```
### **Explanation**
- Uses **parallel streams** for **faster execution** in large datasets (1 million employees).
- **Partitions remote and on-site employees efficiently**.

---

## **Conclusion**
The `Collectors.partitioningBy()` method is **powerful for binary classification** in real-world scenarios, such as:
- **Filtering and counting elements** (students, transactions, employees).
- **Summing or averaging values** in each partition.
- **Using parallel streams for performance optimization**.

### **Benchmark Comparison: Single-Threaded vs. Parallel Execution Using `partitioningBy()`**  

To measure performance differences between **single-threaded (`stream()`)** and **parallel execution (`parallelStream()`)**, we will:
- **Partition a large dataset (1 million elements)**
- **Classify numbers into even and odd**
- **Measure execution time**

---

### **Java Code for Benchmarking**
```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PartitioningByPerformanceTest {
    public static void main(String[] args) {
        // Generate a large list of numbers
        List<Integer> numbers = IntStream.rangeClosed(1, 1_000_000).boxed().collect(Collectors.toList());

        // Measure time for sequential partitioningBy()
        long startTime1 = System.nanoTime();
        Map<Boolean, List<Integer>> partitionedSequential = numbers.stream()
                                                                  .collect(Collectors.partitioningBy(n -> n % 2 == 0));
        long endTime1 = System.nanoTime();

        // Measure time for parallel partitioningBy()
        long startTime2 = System.nanoTime();
        Map<Boolean, List<Integer>> partitionedParallel = numbers.parallelStream()
                                                                 .collect(Collectors.partitioningBy(n -> n % 2 == 0));
        long endTime2 = System.nanoTime();

        // Print execution times
        System.out.println("Sequential Execution Time: " + (endTime1 - startTime1) / 1_000_000 + " ms");
        System.out.println("Parallel Execution Time: " + (endTime2 - startTime2) / 1_000_000 + " ms");
    }
}
```

---

### **Expected Results: Performance Benchmark**
| **Method** | **Execution Time (Expected)** |
|------------|------------------------------|
| **Sequential (`stream()`)** | ~500-700 ms |
| **Parallel (`parallelStream()`)** | ~200-400 ms (**Faster by ~50%**) |

---

### **Analysis of Results**
- **Parallel stream reduces execution time** by almost **50%** in large datasets (1 million elements).
- **PartitioningBy works well with parallel execution** because it splits data into **two groups (`true` and `false`)**, making it **ideal for parallel processing**.
- **Parallel streams utilize multiple CPU cores**, improving **efficiency for large datasets**.

---

### **When to Use Parallel Streams?**
| **Dataset Size** | **Best Choice** |
|------------------|---------------|
| **Small (≤ 10,000 elements)** | Use `stream()` (sequential) |
| **Medium (~100,000 elements)** | Consider `parallelStream()` if processing is intensive |
| **Large (≥ 1,000,000 elements)** | Use `parallelStream()` for better performance |

---

### **Final Conclusion**
- **For small datasets**, `stream()` (sequential) is better **(no overhead of parallel execution)**.
- **For large datasets**, `parallelStream()` **significantly improves performance** when using `partitioningBy()`.
- **Parallel execution is highly effective for CPU-intensive operations**, like **classification, filtering, and aggregation**.


# What is Collectors.mapping()?

The `Collectors.mapping()` method in Java 8 Stream API is a **downstream collector** used with `Collectors.groupingBy()` or `Collectors.partitioningBy()`.  

It **transforms elements** before collecting them, allowing you to extract specific attributes **before grouping** or **partitioning**.

---

## **Syntax of `Collectors.mapping()`**  

### **1. Basic Syntax**
```java
<R, A, T> Collector<T, ?, A> mapping(Function<? super T, ? extends R> mapper, Collector<? super R, ?, A> downstream);
```
- **`T`** → Type of stream elements.  
- **`R`** → Type after applying the `mapper` function.  
- **`mapper`** → Function to transform stream elements.  
- **`downstream`** → Collector used for final collection (`toList()`, `toSet()`, etc.).  

---

## **Comparison: `Collectors.mapping()` vs `Collectors.toList()`**
| Feature | `Collectors.toList()` | `Collectors.mapping()` |
|---------|----------------|----------------|
| **Purpose** | Collects stream elements directly | Transforms elements **before** collecting |
| **Used With** | `collect(Collectors.toList())` | `collect(Collectors.groupingBy(..., mapping(...)))` |
| **Supports Grouping?** | No | Yes |
| **Example Output** | `List<Employee>` | `Map<String, List<String>>` (e.g., `{IT=[Alice, Bob]}`) |

---

## **Basic Examples of `Collectors.mapping()`**

### **1. Extracting Names from a List of Employees**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class MappingExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "Finance")
        );

        List<String> employeeNames = employees.stream()
                                              .collect(Collectors.mapping(e -> e.name, Collectors.toList()));

        System.out.println("Employee Names: " + employeeNames);
    }
}
```
### **Explanation**
- **Extracts only names** from `Employee` objects.
- **Output**:  
  ```
  Employee Names: [Alice, Bob, Charlie, David]
  ```

---

## **Advanced Use Cases of `Collectors.mapping()`**

### **2. Grouping Employees by Department and Extracting Names**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }

    @Override
    public String toString() {
        return name;
    }
}

public class MappingExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR"),
            new Employee("Eve", "Finance")
        );

        Map<String, List<String>> employeesByDept = employees.stream()
                                                             .collect(Collectors.groupingBy(
                                                                 e -> e.department,
                                                                 Collectors.mapping(e -> e.name, Collectors.toList())
                                                             ));

        System.out.println("Employees Grouped by Department: " + employeesByDept);
    }
}
```
### **Explanation**
- **Groups employees by department** and **extracts names**.
- **Output**:  
  ```
  Employees Grouped by Department: {IT=[Alice, Charlie], HR=[Bob, David], Finance=[Eve]}
  ```

---

### **3. Partitioning Students by Pass/Fail and Collecting Names**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}

public class MappingExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75),
            new Student("David", 35),
            new Student("Eve", 90)
        );

        Map<Boolean, List<String>> partitionedStudents = students.stream()
                                                                 .collect(Collectors.partitioningBy(
                                                                     s -> s.marks >= 50,
                                                                     Collectors.mapping(s -> s.name, Collectors.toList())
                                                                 ));

        System.out.println("Passed Students: " + partitionedStudents.get(true));
        System.out.println("Failed Students: " + partitionedStudents.get(false));
    }
}
```
### **Explanation**
- **Partitions students into "Pass" and "Fail" categories**.
- **Extracts names** instead of returning full `Student` objects.
- **Output**:
  ```
  Passed Students: [Alice, Charlie, Eve]
  Failed Students: [Bob, David]
  ```

---

### **4. Summarizing Product Prices by Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }
}

public class MappingExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99),
            new Product("Phone", "Electronics", 800.50),
            new Product("Shoes", "Fashion", 100.00),
            new Product("Jacket", "Fashion", 150.00)
        );

        Map<String, List<Double>> priceByCategory = products.stream()
                                                            .collect(Collectors.groupingBy(
                                                                p -> p.category,
                                                                Collectors.mapping(p -> p.price, Collectors.toList())
                                                            ));

        System.out.println("Prices by Category: " + priceByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Extracts only prices** instead of full product objects.
- **Output**:
  ```
  Prices by Category: {Electronics=[1200.99, 800.5], Fashion=[100.0, 150.0]}
  ```

---

## **Comparison: `groupingBy()` vs `groupingBy(..., mapping(...))`**

| Feature | `groupingBy()` | `groupingBy(..., mapping(...))` |
|---------|--------------|----------------------|
| **Returns** | `Map<K, List<T>>` | `Map<K, List<R>>` (transformed elements) |
| **Example Output** | `{IT=[Alice, Bob]}` | `{IT=[Alice, Charlie]}` |
| **Use Case** | Stores full objects | Extracts specific attributes |

---

## **Conclusion**
The `Collectors.mapping()` method is useful for:
- **Extracting attributes** before grouping/partitioning.
- **Reducing memory usage** (stores specific fields instead of entire objects).
- **Improving readability** in reports and analytics.

## **Advanced Examples of `Collectors.mapping()` in Java 8 Stream API**  

### **1. Grouping Employees by Department and Storing Names as a `Set` (Avoid Duplicates)**
#### **Scenario:** Group employees by department and store **unique** names in a `Set`.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class MappingAdvancedExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR"),
            new Employee("Alice", "IT") // Duplicate
        );

        Map<String, Set<String>> employeesByDept = employees.stream()
                                                            .collect(Collectors.groupingBy(
                                                                e -> e.department,
                                                                Collectors.mapping(e -> e.name, Collectors.toSet())
                                                            ));

        System.out.println("Employees Grouped by Department (Unique Names): " + employeesByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Stores only unique names (`Set<String>`)**.
- **Output**:
  ```
  Employees Grouped by Department (Unique Names): {IT=[Alice, Charlie], HR=[Bob, David]}
  ```

---

### **2. Partitioning Employees into Remote and Onsite Workers and Storing Names**
#### **Scenario:** Separate employees into **remote and onsite workers** and store **names**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    boolean remote;

    public Employee(String name, boolean remote) {
        this.name = name;
        this.remote = remote;
    }
}

public class MappingAdvancedExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", true),
            new Employee("Bob", false),
            new Employee("Charlie", true),
            new Employee("David", false)
        );

        Map<Boolean, List<String>> remoteWorkers = employees.stream()
                                                            .collect(Collectors.partitioningBy(
                                                                e -> e.remote,
                                                                Collectors.mapping(e -> e.name, Collectors.toList())
                                                            ));

        System.out.println("Remote Workers: " + remoteWorkers.get(true));
        System.out.println("Onsite Workers: " + remoteWorkers.get(false));
    }
}
```
### **Explanation**
- **Partitions employees into two categories (`true` for remote, `false` for onsite)**.
- **Stores only names, not full employee objects**.
- **Output**:
  ```
  Remote Workers: [Alice, Charlie]
  Onsite Workers: [Bob, David]
  ```

---

### **3. Grouping Transactions by Category and Collecting Transaction IDs**
#### **Scenario:** Group transactions by category and collect only **transaction IDs**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Transaction {
    int id;
    String category;
    double amount;

    public Transaction(int id, String category, double amount) {
        this.id = id;
        this.category = category;
        this.amount = amount;
    }
}

public class MappingAdvancedExample3 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction(101, "Electronics", 1200.00),
            new Transaction(102, "Groceries", 200.50),
            new Transaction(103, "Electronics", 800.00),
            new Transaction(104, "Groceries", 150.00),
            new Transaction(105, "Fashion", 500.00)
        );

        Map<String, List<Integer>> transactionsByCategory = transactions.stream()
                                                                        .collect(Collectors.groupingBy(
                                                                            t -> t.category,
                                                                            Collectors.mapping(t -> t.id, Collectors.toList())
                                                                        ));

        System.out.println("Transaction IDs by Category: " + transactionsByCategory);
    }
}
```
### **Explanation**
- **Groups transactions by category**.
- **Stores only transaction IDs, avoiding full `Transaction` objects**.
- **Output**:
  ```
  Transaction IDs by Category: {Electronics=[101, 103], Groceries=[102, 104], Fashion=[105]}
  ```

---

### **4. Summarizing Product Names by Category (Comma-Separated String)**
#### **Scenario:** Group products by category and store **product names as a comma-separated string**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class MappingAdvancedExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics"),
            new Product("Phone", "Electronics"),
            new Product("Shoes", "Fashion"),
            new Product("Jacket", "Fashion")
        );

        Map<String, String> productNamesByCategory = products.stream()
                                                             .collect(Collectors.groupingBy(
                                                                 p -> p.category,
                                                                 Collectors.mapping(
                                                                     p -> p.name,
                                                                     Collectors.joining(", ")
                                                                 )
                                                             ));

        System.out.println("Products by Category: " + productNamesByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Stores names as a comma-separated string using `Collectors.joining()`**.
- **Output**:
  ```
  Products by Category: {Electronics=Laptop, Phone, Fashion=Shoes, Jacket}
  ```

---

### **5. Grouping Students by Grade and Getting Maximum Marks per Grade**
#### **Scenario:** Group students by **grade** and find **maximum marks per grade**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Student {
    String name;
    String grade;
    int marks;

    public Student(String name, String grade, int marks) {
        this.name = name;
        this.grade = grade;
        this.marks = marks;
    }
}

public class MappingAdvancedExample5 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", "A", 85),
            new Student("Bob", "B", 75),
            new Student("Charlie", "A", 90),
            new Student("David", "B", 80),
            new Student("Eve", "A", 95)
        );

        Map<String, Integer> maxMarksByGrade = students.stream()
                                                       .collect(Collectors.groupingBy(
                                                           s -> s.grade,
                                                           Collectors.mapping(
                                                               s -> s.marks,
                                                               Collectors.collectingAndThen(Collectors.maxBy(Integer::compareTo), Optional::get)
                                                           )
                                                       ));

        System.out.println("Maximum Marks by Grade: " + maxMarksByGrade);
    }
}
```
### **Explanation**
- **Groups students by grade**.
- **Finds maximum marks in each grade** using `maxBy()` and `collectingAndThen()`.
- **Output**:
  ```
  Maximum Marks by Grade: {A=95, B=80}
  ```

---

## **Key Takeaways**
- **Use `Collectors.mapping()` to extract attributes** before collecting data.
- **Works well with `groupingBy()` and `partitioningBy()`** to avoid storing full objects.
- **Can combine with `Collectors.toSet()`, `Collectors.joining()`, `Collectors.maxBy()`, etc.** for custom transformations.
- **Efficient in reducing memory usage** when only a few fields are needed.


# What is Collectors.flatMapping()?
The `Collectors.flatMapping()` method, introduced in **Java 9**, is a **downstream collector** used with `Collectors.groupingBy()` or `Collectors.partitioningBy()`.  

It is similar to `Collectors.mapping()`, but with an additional advantage: it **flattens** nested collections into a **single, flat collection** before collecting.

---

## **Syntax of `Collectors.flatMapping()`**
```java
<R, A, T> Collector<T, ?, A> flatMapping(Function<? super T, ? extends Stream<? extends R>> mapper, Collector<? super R, ?, A> downstream);
```

- **`T`** → Type of stream elements.  
- **`R`** → Type after applying the `mapper` function.  
- **`mapper`** → Function that returns a **Stream<R>** instead of a single value.  
- **`downstream`** → Collector used for final collection (`toList()`, `toSet()`, etc.).  

---

## **Difference Between `Collectors.mapping()` and `Collectors.flatMapping()`**
| Feature | `Collectors.mapping()` | `Collectors.flatMapping()` |
|---------|----------------|----------------|
| **Purpose** | Extracts attributes **before** collecting | Extracts and **flattens nested collections** |
| **Mapper Function Output** | Single value per element | A **Stream** (multiple values per element) |
| **Used With** | `groupingBy()`, `partitioningBy()` | `groupingBy()`, `partitioningBy()` |
| **Example Output** | `{IT=[Alice, Charlie]}` | `{IT=[Alice, Bob, Charlie]}` (Flattened) |

---

## **Basic Example: Extracting and Flattening Values**

### **1. Flattening Employee Skills by Department**
#### **Scenario:** Employees have multiple skills. Group them by department and get a **flat list of all skills**.

```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Employee {
    String name;
    String department;
    List<String> skills;

    public Employee(String name, String department, List<String> skills) {
        this.name = name;
        this.department = department;
        this.skills = skills;
    }
}

public class FlatMappingExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", Arrays.asList("Java", "Python")),
            new Employee("Bob", "HR", Arrays.asList("Communication", "Recruitment")),
            new Employee("Charlie", "IT", Arrays.asList("Java", "DevOps"))
        );

        Map<String, Set<String>> skillsByDept = employees.stream()
                                                         .collect(Collectors.groupingBy(
                                                             e -> e.department,
                                                             Collectors.flatMapping(
                                                                 e -> e.skills.stream(),
                                                                 Collectors.toSet()
                                                             )
                                                         ));

        System.out.println("Skills by Department: " + skillsByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Extracts skills** and **flattens** them into a **single set**.
- **Avoids nested lists (`List<List<String>>`)**.
- **Output**:
  ```
  Skills by Department: {IT=[Java, Python, DevOps], HR=[Communication, Recruitment]}
  ```

---

## **Advanced Use Cases of `Collectors.flatMapping()`**

### **2. Extracting and Flattening Product Categories**
#### **Scenario:** Products belong to multiple categories. Group them by type and get **all categories**.

```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Product {
    String name;
    String type;
    List<String> categories;

    public Product(String name, String type, List<String> categories) {
        this.name = name;
        this.type = type;
        this.categories = categories;
    }
}

public class FlatMappingExample2 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", Arrays.asList("Computers", "Office")),
            new Product("Phone", "Electronics", Arrays.asList("Mobiles", "Gadgets")),
            new Product("Shirt", "Clothing", Arrays.asList("Men", "Casual"))
        );

        Map<String, Set<String>> categoriesByType = products.stream()
                                                            .collect(Collectors.groupingBy(
                                                                p -> p.type,
                                                                Collectors.flatMapping(
                                                                    p -> p.categories.stream(),
                                                                    Collectors.toSet()
                                                                )
                                                            ));

        System.out.println("Categories by Product Type: " + categoriesByType);
    }
}
```
### **Explanation**
- **Groups products by type**.
- **Flattens category lists** and removes duplicates.
- **Output**:
  ```
  Categories by Product Type: {Electronics=[Computers, Office, Mobiles, Gadgets], Clothing=[Men, Casual]}
  ```

---

### **3. Partitioning Students Based on Marks and Flattening Subjects**
#### **Scenario:** Students study multiple subjects. Partition them into **pass (≥ 50 marks)** and **fail (< 50 marks)**, and **list all subjects in each partition**.

```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Student {
    String name;
    int marks;
    List<String> subjects;

    public Student(String name, int marks, List<String> subjects) {
        this.name = name;
        this.marks = marks;
        this.subjects = subjects;
    }
}

public class FlatMappingExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85, Arrays.asList("Math", "Physics")),
            new Student("Bob", 45, Arrays.asList("History", "Geography")),
            new Student("Charlie", 75, Arrays.asList("Math", "Computer Science")),
            new Student("David", 35, Arrays.asList("History", "Civics"))
        );

        Map<Boolean, Set<String>> subjectsByPassFail = students.stream()
                                                               .collect(Collectors.partitioningBy(
                                                                   s -> s.marks >= 50,
                                                                   Collectors.flatMapping(
                                                                       s -> s.subjects.stream(),
                                                                       Collectors.toSet()
                                                                   )
                                                               ));

        System.out.println("Subjects of Passed Students: " + subjectsByPassFail.get(true));
        System.out.println("Subjects of Failed Students: " + subjectsByPassFail.get(false));
    }
}
```
### **Explanation**
- **Partitions students into pass (`true`) and fail (`false`)**.
- **Flattens their subject lists into a set**.
- **Output**:
  ```
  Subjects of Passed Students: [Math, Physics, Computer Science]
  Subjects of Failed Students: [History, Geography, Civics]
  ```

---

### **4. Extracting and Flattening Project Technologies Used in Each Department**
#### **Scenario:** Employees work on multiple projects. Group them by department and **flatten the technologies used**.

```java
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Employee {
    String name;
    String department;
    List<String> technologies;

    public Employee(String name, String department, List<String> technologies) {
        this.name = name;
        this.department = department;
        this.technologies = technologies;
    }
}

public class FlatMappingExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", Arrays.asList("Java", "AWS")),
            new Employee("Bob", "IT", Arrays.asList("Python", "AWS")),
            new Employee("Charlie", "HR", Arrays.asList("Excel", "SAP")),
            new Employee("David", "HR", Arrays.asList("SAP", "Communication Tools"))
        );

        Map<String, Set<String>> technologiesByDept = employees.stream()
                                                               .collect(Collectors.groupingBy(
                                                                   e -> e.department,
                                                                   Collectors.flatMapping(
                                                                       e -> e.technologies.stream(),
                                                                       Collectors.toSet()
                                                                   )
                                                               ));

        System.out.println("Technologies by Department: " + technologiesByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Flattens technology lists into a set**.
- **Output**:
  ```
  Technologies by Department: {IT=[Java, Python, AWS], HR=[Excel, SAP, Communication Tools]}
  ```

---

## **Key Takeaways**
- **Use `Collectors.flatMapping()` when dealing with nested collections** inside objects.
- **Flattens multiple lists into a single list** within each group.
- **Reduces unnecessary nesting**, improving performance and readability.
- **Works best with `groupingBy()` and `partitioningBy()`**.

Would you like **performance comparisons between `mapping()` and `flatMapping()`?** 🚀

## **Full Tabular Difference Between `Collectors.mapping()` and `Collectors.flatMapping()`**  

| Feature | `Collectors.mapping()` | `Collectors.flatMapping()` |
|---------|----------------|----------------|
| **Purpose** | Extracts and transforms a single value from each element before collecting. | Extracts and flattens multiple values (from a `Stream<R>`) before collecting. |
| **Mapper Function Output** | Produces a **single** transformed value per element. | Produces a **stream of values** per element (which gets flattened). |
| **Input Function Type** | `Function<T, R>` (returns **one value**) | `Function<T, Stream<R>>` (returns **a stream of values**) |
| **Used With** | `groupingBy()`, `partitioningBy()` | `groupingBy()`, `partitioningBy()` |
| **Flattening Behavior** | Does **not** flatten nested collections; collects them as `List<List<R>>`. | **Flattens nested collections**; collects as `List<R>`. |
| **Duplicates Handling** | Preserves all values, including duplicates. | Can eliminate duplicates when used with `Collectors.toSet()`. |
| **Performance Impact** | Works well when extracting a **single field**. | More efficient when dealing with **lists inside objects** (avoids `List<List<T>>`). |
| **Nested Collection Handling** | If each element has a collection (e.g., `List<String>`), it will store `List<List<String>>`. | Converts `List<List<String>>` into `List<String>`. |
| **Common Use Cases** | Extracting names, IDs, or single fields before collecting. | Flattening **skills, technologies, subjects, categories, etc.** into a **flat list or set**. |

---

### **Key Takeaways**
- **Use `mapping()`** when you need to **extract a single field** from each object.
- **Use `flatMapping()`** when you need to **extract and flatten multiple values** from each object.
- **`flatMapping()` is useful for avoiding nested collections**, making results more readable.

# What is Collectors.averaging()?
The `Collectors.averaging()` method in Java 8 Stream API is a **downstream collector** used to **calculate the average** of numerical values in a stream. It is commonly used with `groupingBy()` to find the average in different categories.

It comes in three variants:
- `Collectors.averagingInt()` → Computes the **average of `int` values**.
- `Collectors.averagingLong()` → Computes the **average of `long` values**.
- `Collectors.averagingDouble()` → Computes the **average of `double` values**.

---

## **Syntax of `Collectors.averaging()`**

### **1. Averaging `int` Values**
```java
Collector<T, ?, Double> averagingInt(ToIntFunction<? super T> mapper);
```
- **Extracts `int` values** and computes their average.
- **Returns `Double`** (not `int`) to handle fractional results.

### **2. Averaging `long` Values**
```java
Collector<T, ?, Double> averagingLong(ToLongFunction<? super T> mapper);
```
- **Extracts `long` values** and computes their average.
- **Returns `Double`** for precision.

### **3. Averaging `double` Values**
```java
Collector<T, ?, Double> averagingDouble(ToDoubleFunction<? super T> mapper);
```
- **Extracts `double` values** and computes their average.
- **Returns `Double`**.

---

## **Basic Examples of `Collectors.averaging()`**

### **1. Averaging a List of Numbers**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AveragingExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        double average = numbers.stream()
                                .collect(Collectors.averagingInt(n -> n));

        System.out.println("Average: " + average);
    }
}
```
### **Explanation**
- **Converts each `Integer` to `int`**.
- **Computes average**.
- **Output**:
  ```
  Average: 30.0
  ```

---

## **Advanced Use Cases of `Collectors.averaging()`**

### **2. Averaging Employee Salaries**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class AveragingExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        double avgSalary = employees.stream()
                                    .collect(Collectors.averagingDouble(e -> e.salary));

        System.out.println("Average Salary: " + avgSalary);
    }
}
```
### **Explanation**
- **Extracts `salary` (`double`)**.
- **Computes the average**.
- **Output**:
  ```
  Average Salary: 80000.0
  ```

---

### **3. Grouping Employees by Department and Averaging Salaries**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }
}

public class AveragingExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        Map<String, Double> avgSalaryByDept = employees.stream()
                                                       .collect(Collectors.groupingBy(
                                                           e -> e.department,
                                                           Collectors.averagingDouble(e -> e.salary)
                                                       ));

        System.out.println("Average Salary by Department: " + avgSalaryByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Computes average salary per department**.
- **Output**:
  ```
  Average Salary by Department: {IT=95000.0, HR=55000.0, Finance=80000.0}
  ```

---

### **4. Averaging Students' Marks and Partitioning by Pass/Fail**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}

public class AveragingExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75),
            new Student("David", 35),
            new Student("Eve", 90)
        );

        Map<Boolean, Double> avgMarksByPassFail = students.stream()
                                                          .collect(Collectors.partitioningBy(
                                                              s -> s.marks >= 50,
                                                              Collectors.averagingInt(s -> s.marks)
                                                          ));

        System.out.println("Average Marks of Passed Students: " + avgMarksByPassFail.get(true));
        System.out.println("Average Marks of Failed Students: " + avgMarksByPassFail.get(false));
    }
}
```
### **Explanation**
- **Partitions students into pass (`true`) and fail (`false`)**.
- **Computes the average marks in each partition**.
- **Output**:
  ```
  Average Marks of Passed Students: 83.33
  Average Marks of Failed Students: 40.0
  ```

---

### **5. Finding the Average Price of Products by Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }
}

public class AveragingExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99),
            new Product("Phone", "Electronics", 800.50),
            new Product("Shoes", "Fashion", 100.00),
            new Product("Jacket", "Fashion", 150.00),
            new Product("Tablet", "Electronics", 450.75)
        );

        Map<String, Double> avgPriceByCategory = products.stream()
                                                         .collect(Collectors.groupingBy(
                                                             p -> p.category,
                                                             Collectors.averagingDouble(p -> p.price)
                                                         ));

        System.out.println("Average Price by Category: " + avgPriceByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Computes the average price per category**.
- **Output**:
  ```
  Average Price by Category: {Electronics=817.41, Fashion=125.0}
  ```

---

## **Key Takeaways**
- **Use `averagingInt()` for `int` values, `averagingLong()` for `long`, and `averagingDouble()` for `double` values**.
- **Can be combined with `groupingBy()` to compute category-wise averages**.
- **Can be used with `partitioningBy()` to compare different groups**.
- **Returns `Double`, even for `int` and `long` values, ensuring decimal precision**.

Would you like a **performance comparison between `averaging()` and manual calculations using `sum()` and `count()`?** 🚀

# What is Collectors.summing()?

The `Collectors.summing()` method in Java 8 Stream API is a **downstream collector** used to **calculate the sum** of numerical values in a stream. It is commonly used with `groupingBy()` to find the **total sum of values in different categories**.

It comes in three variants:
- `Collectors.summingInt()` → Computes the **sum of `int` values**.
- `Collectors.summingLong()` → Computes the **sum of `long` values**.
- `Collectors.summingDouble()` → Computes the **sum of `double` values**.

---

## **Syntax of `Collectors.summing()`**

### **1. Summing `int` Values**
```java
Collector<T, ?, Integer> summingInt(ToIntFunction<? super T> mapper);
```
- **Extracts `int` values** and computes their sum.
- **Returns `Integer`**.

### **2. Summing `long` Values**
```java
Collector<T, ?, Long> summingLong(ToLongFunction<? super T> mapper);
```
- **Extracts `long` values** and computes their sum.
- **Returns `Long`**.

### **3. Summing `double` Values**
```java
Collector<T, ?, Double> summingDouble(ToDoubleFunction<? super T> mapper);
```
- **Extracts `double` values** and computes their sum.
- **Returns `Double`**.

---

## **Difference Between `Collectors.summing()` and `Stream.mapToInt().sum()`**
| Feature | `Collectors.summing()` | `Stream.mapToInt().sum()` |
|---------|----------------|----------------|
| **Method Type** | Terminal operation (collecting) | Terminal operation (numeric stream) |
| **Works With `groupingBy()`?** | ✅ Yes | ❌ No |
| **Best Use Case** | Summing values inside `groupingBy()` | Summing a full numeric stream |

---

## **Basic Examples of `Collectors.summing()`**

### **1. Summing a List of Numbers**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SummingExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 30, 40, 50);

        int sum = numbers.stream()
                         .collect(Collectors.summingInt(n -> n));

        System.out.println("Sum: " + sum);
    }
}
```
### **Explanation**
- **Converts each `Integer` to `int`**.
- **Computes the sum**.
- **Output**:
  ```
  Sum: 150
  ```

---

## **Advanced Use Cases of `Collectors.summing()`**

### **2. Summing Employee Salaries**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class SummingExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        double totalSalary = employees.stream()
                                      .collect(Collectors.summingDouble(e -> e.salary));

        System.out.println("Total Salary: " + totalSalary);
    }
}
```
### **Explanation**
- **Extracts `salary` (`double`)**.
- **Computes the total salary**.
- **Output**:
  ```
  Total Salary: 240000.0
  ```

---

### **3. Grouping Employees by Department and Summing Salaries**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }
}

public class SummingExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        Map<String, Double> totalSalaryByDept = employees.stream()
                                                         .collect(Collectors.groupingBy(
                                                             e -> e.department,
                                                             Collectors.summingDouble(e -> e.salary)
                                                         ));

        System.out.println("Total Salary by Department: " + totalSalaryByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Computes total salary per department**.
- **Output**:
  ```
  Total Salary by Department: {IT=190000.0, HR=110000.0, Finance=80000.0}
  ```

---

### **4. Partitioning Students by Pass/Fail and Summing Their Marks**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}

public class SummingExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75),
            new Student("David", 35),
            new Student("Eve", 90)
        );

        Map<Boolean, Integer> totalMarksByPassFail = students.stream()
                                                            .collect(Collectors.partitioningBy(
                                                                s -> s.marks >= 50,
                                                                Collectors.summingInt(s -> s.marks)
                                                            ));

        System.out.println("Total Marks of Passed Students: " + totalMarksByPassFail.get(true));
        System.out.println("Total Marks of Failed Students: " + totalMarksByPassFail.get(false));
    }
}
```
### **Explanation**
- **Partitions students into pass (`true`) and fail (`false`)**.
- **Computes the total marks in each partition**.
- **Output**:
  ```
  Total Marks of Passed Students: 250
  Total Marks of Failed Students: 80
  ```

---

### **5. Finding the Total Price of Products by Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }
}

public class SummingExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99),
            new Product("Phone", "Electronics", 800.50),
            new Product("Shoes", "Fashion", 100.00),
            new Product("Jacket", "Fashion", 150.00),
            new Product("Tablet", "Electronics", 450.75)
        );

        Map<String, Double> totalPriceByCategory = products.stream()
                                                           .collect(Collectors.groupingBy(
                                                               p -> p.category,
                                                               Collectors.summingDouble(p -> p.price)
                                                           ));

        System.out.println("Total Price by Category: " + totalPriceByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Computes the total price per category**.
- **Output**:
  ```
  Total Price by Category: {Electronics=2452.24, Fashion=250.0}
  ```

---

## **Key Takeaways**
- **Use `summingInt()`, `summingLong()`, or `summingDouble()` based on value type**.
- **Works well with `groupingBy()` to compute category-wise totals**.
- **Use with `partitioningBy()` to compare different groups**.
- **Returns `Integer`, `Long`, or `Double` accordingly**.

Would you like a **performance comparison between `summing()` and manual summation using `reduce()`?** 🚀

# What is Collectors.minBy()?
The `Collectors.minBy()` method in Java 8 Stream API is a **terminal collector** used to **find the minimum element** in a stream based on a given comparator. It is commonly used with **groupingBy()** to find the **minimum value within different categories**.

It **returns an `Optional<T>`**, which may be **empty** if the stream contains no elements.

---

## **Syntax of `Collectors.minBy()`**
```java
Collector<T, ?, Optional<T>> minBy(Comparator<? super T> comparator);
```
- **`T`** → Type of elements in the stream.
- **`comparator`** → The **comparison logic** to determine the minimum element.
- **Returns an `Optional<T>`**, which contains the **minimum element** or **empty if the stream is empty**.

---

## **Difference Between `Collectors.minBy()` and `Stream.min()`**
| Feature | `Collectors.minBy()` | `Stream.min()` |
|---------|----------------|----------------|
| **Method Type** | Used with `collect()` | Used directly on stream |
| **Works With `groupingBy()`?** | ✅ Yes | ❌ No |
| **Return Type** | `Optional<T>` | `Optional<T>` |
| **Best Use Case** | Finding minimum per group (e.g., lowest salary in each department) | Finding overall minimum in the stream |

---

## **Basic Example of `Collectors.minBy()`**
### **1. Finding the Minimum Value in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

public class MinByExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 5, 40, 30);

        Optional<Integer> minNumber = numbers.stream()
                                             .collect(Collectors.minBy(Comparator.naturalOrder()));

        minNumber.ifPresent(n -> System.out.println("Minimum Number: " + n));
    }
}
```
### **Explanation**
- **Finds the smallest number** in the list.
- **Uses `Comparator.naturalOrder()`** to find the **smallest element**.
- **Handles empty list safely** (returns `Optional<Integer>`).
- **Output**:
  ```
  Minimum Number: 5
  ```

---

## **Advanced Use Cases of `Collectors.minBy()`**

### **2. Finding Employee with Minimum Salary**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class MinByExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        Optional<Employee> minSalaryEmployee = employees.stream()
                                                        .collect(Collectors.minBy(Comparator.comparing(e -> e.salary)));

        minSalaryEmployee.ifPresent(e -> System.out.println("Employee with Minimum Salary: " + e.name));
    }
}
```
### **Explanation**
- **Finds employee with the lowest salary**.
- **Uses `Comparator.comparing(e -> e.salary)`** to compare employees by salary.
- **Handles empty list safely using `Optional`**.
- **Output**:
  ```
  Employee with Minimum Salary: Bob
  ```

---

### **3. Finding the Minimum Salary in Each Department**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " ($" + salary + ")";
    }
}

public class MinByExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        Map<String, Optional<Employee>> minSalaryByDept = employees.stream()
                                                                   .collect(Collectors.groupingBy(
                                                                       e -> e.department,
                                                                       Collectors.minBy(Comparator.comparing(e -> e.salary))
                                                                   ));

        System.out.println("Minimum Salary by Department: " + minSalaryByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Finds the employee with the lowest salary in each department**.
- **Returns a `Map<String, Optional<Employee>>`** where `String` is the department.
- **Output**:
  ```
  Minimum Salary by Department: {IT=Alice ($70000), HR=Bob ($50000), Finance=Eve ($80000)}
  ```

---

### **4. Finding the Cheapest Product in Each Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " ($" + price + ")";
    }
}

public class MinByExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99),
            new Product("Phone", "Electronics", 800.50),
            new Product("Shoes", "Fashion", 100.00),
            new Product("Jacket", "Fashion", 150.00),
            new Product("Tablet", "Electronics", 450.75)
        );

        Map<String, Optional<Product>> minPriceByCategory = products.stream()
                                                                    .collect(Collectors.groupingBy(
                                                                        p -> p.category,
                                                                        Collectors.minBy(Comparator.comparing(p -> p.price))
                                                                    ));

        System.out.println("Cheapest Product by Category: " + minPriceByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Finds the cheapest product in each category**.
- **Output**:
  ```
  Cheapest Product by Category: {Electronics=Tablet ($450.75), Fashion=Shoes ($100.0)}
  ```

---

## **Key Takeaways**
- **Use `minBy()` to find the minimum element in a stream** based on a comparator.
- **Works well with `groupingBy()` to find the minimum value in each group**.
- **Returns an `Optional<T>` to handle empty streams safely**.
- **Avoids `NullPointerException` because of the `Optional` wrapper**.

Would you like **performance comparisons between `Collectors.minBy()` and `Stream.min()`?** 🚀

# What is Collectors.maxBy()?

The `Collectors.maxBy()` method in Java 8 Stream API is a **terminal collector** used to **find the maximum element** in a stream based on a given comparator. It is commonly used with **groupingBy()** to find the **maximum value within different categories**.

It **returns an `Optional<T>`**, which may be **empty** if the stream contains no elements.

---

## **Syntax of `Collectors.maxBy()`**
```java
Collector<T, ?, Optional<T>> maxBy(Comparator<? super T> comparator);
```
- **`T`** → Type of elements in the stream.
- **`comparator`** → The **comparison logic** to determine the maximum element.
- **Returns an `Optional<T>`**, which contains the **maximum element** or **empty if the stream is empty**.

---

## **Difference Between `Collectors.maxBy()` and `Stream.max()`**
| Feature | `Collectors.maxBy()` | `Stream.max()` |
|---------|----------------|----------------|
| **Method Type** | Used with `collect()` | Used directly on stream |
| **Works With `groupingBy()`?** | ✅ Yes | ❌ No |
| **Return Type** | `Optional<T>` | `Optional<T>` |
| **Best Use Case** | Finding maximum per group (e.g., highest salary in each department) | Finding overall maximum in the stream |

---

## **Basic Example of `Collectors.maxBy()`**
### **1. Finding the Maximum Value in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

public class MaxByExample1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, 20, 5, 40, 30);

        Optional<Integer> maxNumber = numbers.stream()
                                             .collect(Collectors.maxBy(Comparator.naturalOrder()));

        maxNumber.ifPresent(n -> System.out.println("Maximum Number: " + n));
    }
}
```
### **Explanation**
- **Finds the largest number** in the list.
- **Uses `Comparator.naturalOrder()`** to find the **largest element**.
- **Handles empty list safely** (returns `Optional<Integer>`).
- **Output**:
  ```
  Maximum Number: 40
  ```

---

## **Advanced Use Cases of `Collectors.maxBy()`**

### **2. Finding Employee with Maximum Salary**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}

public class MaxByExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        Optional<Employee> maxSalaryEmployee = employees.stream()
                                                        .collect(Collectors.maxBy(Comparator.comparing(e -> e.salary)));

        maxSalaryEmployee.ifPresent(e -> System.out.println("Employee with Maximum Salary: " + e.name));
    }
}
```
### **Explanation**
- **Finds employee with the highest salary**.
- **Uses `Comparator.comparing(e -> e.salary)`** to compare employees by salary.
- **Handles empty list safely using `Optional`**.
- **Output**:
  ```
  Employee with Maximum Salary: Charlie
  ```

---

### **3. Finding the Maximum Salary in Each Department**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " ($" + salary + ")";
    }
}

public class MaxByExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        Map<String, Optional<Employee>> maxSalaryByDept = employees.stream()
                                                                   .collect(Collectors.groupingBy(
                                                                       e -> e.department,
                                                                       Collectors.maxBy(Comparator.comparing(e -> e.salary))
                                                                   ));

        System.out.println("Maximum Salary by Department: " + maxSalaryByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Finds the employee with the highest salary in each department**.
- **Returns a `Map<String, Optional<Employee>>`** where `String` is the department.
- **Output**:
  ```
  Maximum Salary by Department: {IT=Charlie ($120000), HR=David ($60000), Finance=Eve ($80000)}
  ```

---

### **4. Finding the Most Expensive Product in Each Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Comparator;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " ($" + price + ")";
    }
}

public class MaxByExample4 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99),
            new Product("Phone", "Electronics", 800.50),
            new Product("Shoes", "Fashion", 100.00),
            new Product("Jacket", "Fashion", 150.00),
            new Product("Tablet", "Electronics", 450.75)
        );

        Map<String, Optional<Product>> maxPriceByCategory = products.stream()
                                                                    .collect(Collectors.groupingBy(
                                                                        p -> p.category,
                                                                        Collectors.maxBy(Comparator.comparing(p -> p.price))
                                                                    ));

        System.out.println("Most Expensive Product by Category: " + maxPriceByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Finds the most expensive product in each category**.
- **Output**:
  ```
  Most Expensive Product by Category: {Electronics=Laptop ($1200.99), Fashion=Jacket ($150.0)}
  ```

---

## **Key Takeaways**
- **Use `maxBy()` to find the maximum element in a stream** based on a comparator.
- **Works well with `groupingBy()` to find the maximum value in each group**.
- **Returns an `Optional<T>` to handle empty streams safely**.
- **Avoids `NullPointerException` because of the `Optional` wrapper**.

Would you like **performance comparisons between `Collectors.maxBy()` and `Stream.max()`?** 🚀

# What is Collectors.counting()?
The `Collectors.counting()` method in Java 8 Stream API is a **terminal collector** that **counts the number of elements** in a stream. It is often used with **groupingBy() or partitioningBy()** to count elements in **different categories**.

It **returns a `Long` value** representing the number of elements.

---

## **Syntax of `Collectors.counting()`**
```java
Collector<T, ?, Long> counting();
```
- **`T`** → Type of elements in the stream.
- **Returns a `Long`**, which is the **count of elements** in the stream.

---

## **Difference Between `Collectors.counting()` and `Stream.count()`**
| Feature | `Collectors.counting()` | `Stream.count()` |
|---------|----------------|----------------|
| **Method Type** | Used with `collect()` | Used directly on stream |
| **Works With `groupingBy()`?** | ✅ Yes | ❌ No |
| **Return Type** | `Long` | `long` |
| **Best Use Case** | Counting elements **per group** (e.g., count employees per department) | Counting total elements in the stream |

---

## **Basic Example of `Collectors.counting()`**
### **1. Counting the Number of Elements in a List**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CountingExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David", "Eve");

        long count = names.stream()
                          .collect(Collectors.counting());

        System.out.println("Total Count: " + count);
    }
}
```
### **Explanation**
- **Counts the number of elements** in the list.
- **Output**:
  ```
  Total Count: 5
  ```

---

## **Advanced Use Cases of `Collectors.counting()`**

### **2. Counting Employees by Department**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class CountingExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR"),
            new Employee("Eve", "Finance")
        );

        Map<String, Long> employeeCountByDept = employees.stream()
                                                         .collect(Collectors.groupingBy(
                                                             e -> e.department,
                                                             Collectors.counting()
                                                         ));

        System.out.println("Employee Count by Department: " + employeeCountByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Counts the number of employees in each department**.
- **Output**:
  ```
  Employee Count by Department: {IT=2, HR=2, Finance=1}
  ```

---

### **3. Counting Students by Grade**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    String grade;

    public Student(String name, String grade) {
        this.name = name;
        this.grade = grade;
    }
}

public class CountingExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", "A"),
            new Student("Bob", "B"),
            new Student("Charlie", "A"),
            new Student("David", "C"),
            new Student("Eve", "B")
        );

        Map<String, Long> studentCountByGrade = students.stream()
                                                        .collect(Collectors.groupingBy(
                                                            s -> s.grade,
                                                            Collectors.counting()
                                                        ));

        System.out.println("Student Count by Grade: " + studentCountByGrade);
    }
}
```
### **Explanation**
- **Groups students by grade**.
- **Counts the number of students in each grade**.
- **Output**:
  ```
  Student Count by Grade: {A=2, B=2, C=1}
  ```

---

### **4. Partitioning Customers Based on VIP Status and Counting**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Customer {
    String name;
    boolean isVIP;

    public Customer(String name, boolean isVIP) {
        this.name = name;
        this.isVIP = isVIP;
    }
}

public class CountingExample4 {
    public static void main(String[] args) {
        List<Customer> customers = Arrays.asList(
            new Customer("Alice", true),
            new Customer("Bob", false),
            new Customer("Charlie", true),
            new Customer("David", false),
            new Customer("Eve", true)
        );

        Map<Boolean, Long> customerCountByVIP = customers.stream()
                                                         .collect(Collectors.partitioningBy(
                                                             c -> c.isVIP,
                                                             Collectors.counting()
                                                         ));

        System.out.println("VIP Customers: " + customerCountByVIP.get(true));
        System.out.println("Regular Customers: " + customerCountByVIP.get(false));
    }
}
```
### **Explanation**
- **Partitions customers into VIP (`true`) and regular (`false`)**.
- **Counts the number of customers in each category**.
- **Output**:
  ```
  VIP Customers: 3
  Regular Customers: 2
  ```

---

### **5. Counting Products by Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class CountingExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics"),
            new Product("Phone", "Electronics"),
            new Product("Shoes", "Fashion"),
            new Product("Jacket", "Fashion"),
            new Product("Tablet", "Electronics")
        );

        Map<String, Long> productCountByCategory = products.stream()
                                                           .collect(Collectors.groupingBy(
                                                               p -> p.category,
                                                               Collectors.counting()
                                                           ));

        System.out.println("Product Count by Category: " + productCountByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Counts the number of products in each category**.
- **Output**:
  ```
  Product Count by Category: {Electronics=3, Fashion=2}
  ```

---

## **Key Takeaways**
- **Use `Collectors.counting()` to count elements in a stream**.
- **Works well with `groupingBy()` to count elements in each category**.
- **Can be combined with `partitioningBy()` to count elements in true/false categories**.
- **Returns `Long`, which can handle large counts**.

Would you like **performance comparisons between `Collectors.counting()` and `Stream.count()`?** 🚀

# What is Collectors.joining()?
The `Collectors.joining()` method in Java 8 Stream API is a **terminal collector** that **concatenates stream elements into a single `String`**. It is commonly used to **combine names, categories, or any textual data** into a formatted string.

It provides three **overloaded versions**:
1. `Collectors.joining()` → Joins elements **without any delimiter**.
2. `Collectors.joining(delimiter)` → Joins elements **with a delimiter** (e.g., `", "`, `" | "`).
3. `Collectors.joining(delimiter, prefix, suffix)` → Joins elements with a **delimiter**, **prefix**, and **suffix**.

---

## **Syntax of `Collectors.joining()`**

### **1. Joining Without Delimiter**
```java
Collector<CharSequence, ?, String> joining();
```
- **Concatenates elements** without any separator.

### **2. Joining With a Delimiter**
```java
Collector<CharSequence, ?, String> joining(CharSequence delimiter);
```
- **Concatenates elements** with a separator (e.g., `", "`, `" | "`).

### **3. Joining With a Delimiter, Prefix, and Suffix**
```java
Collector<CharSequence, ?, String> joining(CharSequence delimiter, CharSequence prefix, CharSequence suffix);
```
- **Concatenates elements** with a separator, and adds a prefix & suffix.

---

## **Difference Between `Collectors.joining()` and `String.join()`**
| Feature | `Collectors.joining()` | `String.join()` |
|---------|----------------|----------------|
| **Method Type** | Used with `collect()` on streams | Direct method on `String` |
| **Works With `groupingBy()`?** | ✅ Yes | ❌ No |
| **Handles Non-String Data?** | ✅ Works with `Stream<T>` by converting to `String` | ❌ Requires `String` elements only |
| **Custom Prefix/Suffix?** | ✅ Yes | ❌ No |

---

## **Basic Example of `Collectors.joining()`**

### **1. Joining a List of Names Without a Delimiter**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoiningExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        String joinedNames = names.stream()
                                  .collect(Collectors.joining());

        System.out.println("Joined Names: " + joinedNames);
    }
}
```
### **Explanation**
- **Joins all names** **without any delimiter**.
- **Output**:
  ```
  Joined Names: AliceBobCharlie
  ```

---

## **Advanced Use Cases of `Collectors.joining()`**

### **2. Joining Names With a Comma Separator**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoiningExample2 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        String joinedNames = names.stream()
                                  .collect(Collectors.joining(", "));

        System.out.println("Joined Names: " + joinedNames);
    }
}
```
### **Explanation**
- **Joins all names with `", "` as a delimiter**.
- **Output**:
  ```
  Joined Names: Alice, Bob, Charlie
  ```

---

### **3. Joining Names With a Custom Prefix and Suffix**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoiningExample3 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        String joinedNames = names.stream()
                                  .collect(Collectors.joining(", ", "[", "]"));

        System.out.println("Joined Names: " + joinedNames);
    }
}
```
### **Explanation**
- **Joins names with a comma separator (`", "`), prefix (`"["`), and suffix (`"]"`)**.
- **Output**:
  ```
  Joined Names: [Alice, Bob, Charlie]
  ```

---

### **4. Joining Employee Names by Department**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class JoiningExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR"),
            new Employee("Eve", "Finance")
        );

        Map<String, String> employeesByDept = employees.stream()
                                                       .collect(Collectors.groupingBy(
                                                           e -> e.department,
                                                           Collectors.mapping(e -> e.name, Collectors.joining(", "))
                                                       ));

        System.out.println("Employees Grouped by Department: " + employeesByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Joins names in each department with a comma**.
- **Output**:
  ```
  Employees Grouped by Department: {IT=Alice, Charlie, HR=Bob, David, Finance=Eve}
  ```

---

### **5. Joining Product Names by Category**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;

    public Product(String name, String category) {
        this.name = name;
        this.category = category;
    }
}

public class JoiningExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics"),
            new Product("Phone", "Electronics"),
            new Product("Shoes", "Fashion"),
            new Product("Jacket", "Fashion"),
            new Product("Tablet", "Electronics")
        );

        Map<String, String> productsByCategory = products.stream()
                                                         .collect(Collectors.groupingBy(
                                                             p -> p.category,
                                                             Collectors.mapping(p -> p.name, Collectors.joining(" | "))
                                                         ));

        System.out.println("Products by Category: " + productsByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Joins product names with `" | "` separator**.
- **Output**:
  ```
  Products by Category: {Electronics=Laptop | Phone | Tablet, Fashion=Shoes | Jacket}
  ```

---

## **Performance Considerations**
- **Efficient for small lists** → `Collectors.joining()` is fast and **avoids extra memory allocation**.
- **Use `StringBuilder` for large data** → If the data is **very large**, consider using `reduce()` with `StringBuilder` for better **performance**.

---

## **Comparison: `joining()` vs Other Methods**
| Feature | `Collectors.joining()` | `String.join()` | `StringBuilder.append()` |
|---------|----------------|----------------|----------------|
| **Method Type** | Terminal collector | Utility method | String manipulation |
| **Works With `groupingBy()`?** | ✅ Yes | ❌ No | ❌ No |
| **Performance for Large Data** | ✅ Good | ✅ Good | 🔥 Best |
| **Supports Custom Prefix/Suffix?** | ✅ Yes | ❌ No | ✅ Yes |
| **Handles Non-String Data?** | ✅ Works with `Stream<T>` | ❌ Requires `String` elements only | ✅ Yes |

---

## **Key Takeaways**
- **Use `joining()` to concatenate elements in a stream into a single `String`**.
- **Works well with `groupingBy()` and `mapping()` to format grouped data**.
- **Supports custom delimiters, prefixes, and suffixes**.
- **Efficient for small to medium-sized collections** but **use `StringBuilder` for very large data**.

## **Advanced and Complex Examples of `Collectors.joining()` in Java 8 Stream API**  

Here are **real-world scenarios** using `Collectors.joining()` combined with **grouping, partitioning, and filtering** for **data processing**.

---

### **1. Joining Book Titles by Genre**
#### **Scenario**: Group books by **genre** and concatenate the **book titles** in each genre.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Book {
    String title;
    String genre;

    public Book(String title, String genre) {
        this.title = title;
        this.genre = genre;
    }
}

public class JoiningExample1 {
    public static void main(String[] args) {
        List<Book> books = Arrays.asList(
            new Book("The Hobbit", "Fantasy"),
            new Book("Harry Potter", "Fantasy"),
            new Book("Dune", "Sci-Fi"),
            new Book("Neuromancer", "Sci-Fi"),
            new Book("1984", "Dystopian")
        );

        Map<String, String> booksByGenre = books.stream()
                                               .collect(Collectors.groupingBy(
                                                   b -> b.genre,
                                                   Collectors.mapping(b -> b.title, Collectors.joining(", "))
                                               ));

        System.out.println("Books by Genre: " + booksByGenre);
    }
}
```
### **Explanation**
- **Groups books by genre**.
- **Joins book titles with a comma**.
- **Output**:
  ```
  Books by Genre: {Fantasy=The Hobbit, Harry Potter, Sci-Fi=Dune, Neuromancer, Dystopian=1984}
  ```

---

### **2. Formatting Employee Details by Department**
#### **Scenario**: Group employees by **department** and **format** their names and salaries.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }
}

public class JoiningExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 75000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 90000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        Map<String, String> employeeDetailsByDept = employees.stream()
                                                             .collect(Collectors.groupingBy(
                                                                 e -> e.department,
                                                                 Collectors.mapping(
                                                                     e -> e.name + " ($" + e.salary + ")",
                                                                     Collectors.joining(" | ")
                                                                 )
                                                             ));

        System.out.println("Employee Details by Department: " + employeeDetailsByDept);
    }
}
```
### **Explanation**
- **Groups employees by department**.
- **Formats each employee as `"Name ($Salary)"`**.
- **Output**:
  ```
  Employee Details by Department: {IT=Alice ($75000) | Charlie ($90000), HR=Bob ($50000) | David ($60000), Finance=Eve ($80000)}
  ```

---

### **3. Joining Order Items With Quantities**
#### **Scenario**: Format a **customer's order** as a string that includes **product names and quantities**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class OrderItem {
    String product;
    int quantity;

    public OrderItem(String product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }
}

public class JoiningExample3 {
    public static void main(String[] args) {
        List<OrderItem> order = Arrays.asList(
            new OrderItem("Laptop", 1),
            new OrderItem("Mouse", 2),
            new OrderItem("Keyboard", 1),
            new OrderItem("Monitor", 2)
        );

        String orderSummary = order.stream()
                                   .map(item -> item.product + " x" + item.quantity)
                                   .collect(Collectors.joining(", ", "Order Summary: [", "]"));

        System.out.println(orderSummary);
    }
}
```
### **Explanation**
- **Formats each order item as `"Product xQuantity"`**.
- **Adds a prefix (`"Order Summary: ["`) and suffix (`"]"`)**.
- **Output**:
  ```
  Order Summary: [Laptop x1, Mouse x2, Keyboard x1, Monitor x2]
  ```

---

### **4. Partitioning Students by Pass/Fail and Joining Their Names**
#### **Scenario**: Partition students into **pass/fail groups** and list their names.

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }
}

public class JoiningExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75),
            new Student("David", 35),
            new Student("Eve", 90)
        );

        Map<Boolean, String> studentsByResult = students.stream()
                                                        .collect(Collectors.partitioningBy(
                                                            s -> s.marks >= 50,
                                                            Collectors.mapping(s -> s.name, Collectors.joining(", "))
                                                        ));

        System.out.println("Passed Students: " + studentsByResult.get(true));
        System.out.println("Failed Students: " + studentsByResult.get(false));
    }
}
```
### **Explanation**
- **Partitions students based on marks (pass ≥ 50, fail < 50)**.
- **Joins names of students in each group**.
- **Output**:
  ```
  Passed Students: Alice, Charlie, Eve
  Failed Students: Bob, David
  ```

---

### **5. Filtering and Joining Names of People Over 30**
#### **Scenario**: Filter people **above age 30** and join their **names**.

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

public class JoiningExample5 {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
            new Person("Alice", 28),
            new Person("Bob", 35),
            new Person("Charlie", 40),
            new Person("David", 25),
            new Person("Eve", 32)
        );

        String namesOver30 = people.stream()
                                   .filter(p -> p.age > 30)
                                   .map(p -> p.name)
                                   .collect(Collectors.joining(" | ", "People Over 30: {", "}"));

        System.out.println(namesOver30);
    }
}
```
### **Explanation**
- **Filters people older than 30**.
- **Joins names with `" | "` separator, and `{}` brackets**.
- **Output**:
  ```
  People Over 30: {Bob | Charlie | Eve}
  ```

---

## **Key Takeaways**
- **Use `Collectors.joining()` to concatenate text data efficiently**.
- **Works well with `groupingBy()`, `partitioningBy()`, and `mapping()`**.
- **Supports custom delimiters, prefixes, and suffixes** for formatting.
- **Can be used for creating formatted reports, summaries, and UI-friendly data representations**.


# What is Collectors.collectingAndThen()?
The `Collectors.collectingAndThen()` method in Java 8 Stream API is a **composite collector** that **modifies the result of another collector** by applying a **finishing transformation**.  

It **wraps another collector** and applies a **post-processing function** to **modify the final result**.

---

## **Syntax of `Collectors.collectingAndThen()`**
```java
<T, A, R, RR> Collector<T, A, RR> collectingAndThen(Collector<T, A, R> downstream, Function<R, RR> finisher);
```
- **`T`** → Type of stream elements.  
- **`A`** → Intermediate accumulator type (used internally by the downstream collector).  
- **`R`** → Result type of the downstream collector.  
- **`RR`** → Final transformed result type.  
- **`downstream`** → The collector to **compute initial results**.  
- **`finisher`** → A function to **apply transformation** to the collected result.  

---

## **Key Use Cases of `collectingAndThen()`**
| Use Case | Description |
|----------|------------|
| **Making Collections Immutable** | Convert results into **unmodifiable lists, sets, or maps**. |
| **Applying Post-Processing** | Convert results into a **different type** (e.g., `Optional`, `BigDecimal`, `Formatted String`). |
| **Ensuring Non-Empty Results** | Provide **default values** when the stream is empty. |
| **Custom Aggregation Logic** | Apply additional **calculations or transformations** on collected data. |

---

## **Basic Example: Making a List Immutable**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;

public class CollectingAndThenExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        List<String> immutableList = names.stream()
                                          .collect(Collectors.collectingAndThen(
                                              Collectors.toList(), 
                                              Collections::unmodifiableList
                                          ));

        System.out.println("Immutable List: " + immutableList);

        // Throws UnsupportedOperationException if we try to modify it
        // immutableList.add("David");
    }
}
```
### **Explanation**
- **Collects elements into a `List`**.
- **Applies `Collections.unmodifiableList()` to make it immutable**.
- **Output**:
  ```
  Immutable List: [Alice, Bob, Charlie]
  ```

---

## **Advanced Use Cases of `collectingAndThen()`**

### **1. Getting the Maximum Salary Employee**
#### **Scenario**: Find the **employee with the highest salary** but return a **default employee** if the list is empty.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;
import java.util.stream.Collectors;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " ($" + salary + ")";
    }
}

public class CollectingAndThenExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        Employee highestPaid = employees.stream()
                                        .collect(Collectors.collectingAndThen(
                                            Collectors.maxBy(Comparator.comparing(e -> e.salary)),
                                            opt -> opt.orElse(new Employee("Default", 0))
                                        ));

        System.out.println("Highest Paid Employee: " + highestPaid);
    }
}
```
### **Explanation**
- **Finds the employee with the highest salary** using `maxBy()`.
- **Uses `orElse()` to provide a default value** if no employees exist.
- **Output**:
  ```
  Highest Paid Employee: Charlie ($120000)
  ```

---

### **2. Counting Employees and Converting to String**
#### **Scenario**: Count **employees** and return a **formatted string**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    String name;

    public Employee(String name) {
        this.name = name;
    }
}

public class CollectingAndThenExample3 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice"),
            new Employee("Bob"),
            new Employee("Charlie")
        );

        String employeeCountMessage = employees.stream()
                                               .collect(Collectors.collectingAndThen(
                                                   Collectors.counting(),
                                                   count -> "Total Employees: " + count
                                               ));

        System.out.println(employeeCountMessage);
    }
}
```
### **Explanation**
- **Counts employees** using `counting()`.
- **Converts the result into a human-readable message**.
- **Output**:
  ```
  Total Employees: 3
  ```

---

### **3. Finding the Youngest Student and Converting to Name Only**
#### **Scenario**: Find the **youngest student** and return **only their name**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;
import java.util.stream.Collectors;

class Student {
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

public class CollectingAndThenExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 22),
            new Student("Bob", 20),
            new Student("Charlie", 24)
        );

        String youngestStudentName = students.stream()
                                             .collect(Collectors.collectingAndThen(
                                                 Collectors.minBy(Comparator.comparing(s -> s.age)),
                                                 opt -> opt.map(s -> s.name).orElse("No students")
                                             ));

        System.out.println("Youngest Student: " + youngestStudentName);
    }
}
```
### **Explanation**
- **Finds the youngest student** using `minBy()`.
- **Extracts only the student's name**.
- **Output**:
  ```
  Youngest Student: Bob
  ```

---

### **4. Creating an Immutable Set of Products**
#### **Scenario**: Collect a **unique set of product names** and make it **immutable**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Collections;
import java.util.stream.Collectors;

class Product {
    String name;

    public Product(String name) {
        this.name = name;
    }
}

public class CollectingAndThenExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop"),
            new Product("Phone"),
            new Product("Tablet"),
            new Product("Laptop")  // Duplicate
        );

        Set<String> uniqueProductNames = products.stream()
                                                 .map(p -> p.name)
                                                 .collect(Collectors.collectingAndThen(
                                                     Collectors.toSet(),
                                                     Collections::unmodifiableSet
                                                 ));

        System.out.println("Unique Products: " + uniqueProductNames);
    }
}
```
### **Explanation**
- **Collects unique product names into a `Set`**.
- **Applies `Collections.unmodifiableSet()` to make it immutable**.
- **Output**:
  ```
  Unique Products: [Laptop, Phone, Tablet]
  ```

---

## **Key Takeaways**
- **`collectingAndThen()` allows post-processing of collected data**.
- **Common use cases include immutability, formatting, and default values**.
- **Works well with `groupingBy()`, `counting()`, `minBy()`, and `maxBy()`**.
- **Prevents `NullPointerException` by wrapping results in `Optional`**.

Would you like **performance comparisons between `collectingAndThen()` and manual transformation (`map()`)?** 🚀

## **Advanced Examples of `Collectors.collectingAndThen()` in Java 8 Stream API**  

Here are **complex real-world scenarios** using `Collectors.collectingAndThen()` combined with **grouping, partitioning, and advanced post-processing** for **data transformation**.

---

### **1. Finding the Most Expensive Product in Each Category and Formatting Output**
#### **Scenario**: Group products by **category**, find the **most expensive product**, and return only the **product name**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Product {
    String name;
    String category;
    double price;

    public Product(String name, String category, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " ($" + price + ")";
    }
}

public class CollectingAndThenExample1 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", "Electronics", 1200.99),
            new Product("Phone", "Electronics", 800.50),
            new Product("Shoes", "Fashion", 100.00),
            new Product("Jacket", "Fashion", 150.00),
            new Product("Tablet", "Electronics", 450.75)
        );

        Map<String, String> maxPriceByCategory = products.stream()
                                                         .collect(Collectors.groupingBy(
                                                             p -> p.category,
                                                             Collectors.collectingAndThen(
                                                                 Collectors.maxBy(Comparator.comparing(p -> p.price)),
                                                                 opt -> opt.map(p -> p.name).orElse("No products")
                                                             )
                                                         ));

        System.out.println("Most Expensive Product by Category: " + maxPriceByCategory);
    }
}
```
### **Explanation**
- **Groups products by category**.
- **Finds the most expensive product in each category**.
- **Returns only the product name, avoiding `Optional<Product>`**.
- **Output**:
  ```
  Most Expensive Product by Category: {Electronics=Laptop, Fashion=Jacket}
  ```

---

### **2. Summing Sales Amount by Region and Formatting as Currency**
#### **Scenario**: Calculate total **sales amount** per region and format the result as **currency**.

```java
import java.util.*;
import java.util.stream.Collectors;
import java.text.NumberFormat;

class Sale {
    String region;
    double amount;

    public Sale(String region, double amount) {
        this.region = region;
        this.amount = amount;
    }
}

public class CollectingAndThenExample2 {
    public static void main(String[] args) {
        List<Sale> sales = Arrays.asList(
            new Sale("North", 5000.75),
            new Sale("North", 3000.50),
            new Sale("South", 7000.00),
            new Sale("South", 2000.25),
            new Sale("West", 6000.00)
        );

        Map<String, String> totalSalesByRegion = sales.stream()
                                                      .collect(Collectors.groupingBy(
                                                          s -> s.region,
                                                          Collectors.collectingAndThen(
                                                              Collectors.summingDouble(s -> s.amount),
                                                              total -> NumberFormat.getCurrencyInstance().format(total)
                                                          )
                                                      ));

        System.out.println("Total Sales by Region: " + totalSalesByRegion);
    }
}
```
### **Explanation**
- **Groups sales by region**.
- **Sums the total amount per region**.
- **Formats the amount as currency (`$5,000.75`)**.
- **Output**:
  ```
  Total Sales by Region: {North=$8,000.75, South=$9,000.25, West=$6,000.00}
  ```

---

### **3. Finding the Highest-Rated Movie in Each Genre**
#### **Scenario**: Group movies by **genre**, find the **highest-rated movie**, and return **name and rating**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Movie {
    String title;
    String genre;
    double rating;

    public Movie(String title, String genre, double rating) {
        this.title = title;
        this.genre = genre;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return title + " (" + rating + ")";
    }
}

public class CollectingAndThenExample3 {
    public static void main(String[] args) {
        List<Movie> movies = Arrays.asList(
            new Movie("Inception", "Sci-Fi", 8.8),
            new Movie("Interstellar", "Sci-Fi", 8.6),
            new Movie("The Dark Knight", "Action", 9.0),
            new Movie("Avengers", "Action", 8.0),
            new Movie("Titanic", "Romance", 7.8)
        );

        Map<String, String> bestMovieByGenre = movies.stream()
                                                     .collect(Collectors.groupingBy(
                                                         m -> m.genre,
                                                         Collectors.collectingAndThen(
                                                             Collectors.maxBy(Comparator.comparing(m -> m.rating)),
                                                             opt -> opt.map(m -> m.title + " (" + m.rating + ")").orElse("No movies")
                                                         )
                                                     ));

        System.out.println("Best Movie by Genre: " + bestMovieByGenre);
    }
}
```
### **Explanation**
- **Groups movies by genre**.
- **Finds the movie with the highest rating**.
- **Formats the result as `"Title (Rating)"`**.
- **Output**:
  ```
  Best Movie by Genre: {Sci-Fi=Inception (8.8), Action=The Dark Knight (9.0), Romance=Titanic (7.8)}
  ```

---

### **4. Finding the Most Frequent Word in a Text**
#### **Scenario**: Count **word occurrences** in a text and return the **most common word**.

```java
import java.util.*;
import java.util.stream.Collectors;

public class CollectingAndThenExample4 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("apple", "banana", "apple", "orange", "banana", "banana", "apple");

        String mostFrequentWord = words.stream()
                                       .collect(Collectors.collectingAndThen(
                                           Collectors.groupingBy(w -> w, Collectors.counting()),
                                           map -> map.entrySet().stream()
                                                     .max(Map.Entry.comparingByValue())
                                                     .map(Map.Entry::getKey)
                                                     .orElse("No words")
                                       ));

        System.out.println("Most Frequent Word: " + mostFrequentWord);
    }
}
```
### **Explanation**
- **Counts occurrences of each word**.
- **Finds the word with the highest frequency**.
- **Output**:
  ```
  Most Frequent Word: apple
  ```

---

### **5. Converting a List of Student Names to an Immutable Sorted Set**
#### **Scenario**: Collect student names into a **sorted, immutable set**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Student {
    String name;

    public Student(String name) {
        this.name = name;
    }
}

public class CollectingAndThenExample5 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Charlie"),
            new Student("Alice"),
            new Student("Bob"),
            new Student("Alice")  // Duplicate
        );

        Set<String> sortedNames = students.stream()
                                          .map(s -> s.name)
                                          .collect(Collectors.collectingAndThen(
                                              Collectors.toCollection(TreeSet::new),
                                              Collections::unmodifiableSet
                                          ));

        System.out.println("Sorted, Immutable Student Names: " + sortedNames);
    }
}
```
### **Explanation**
- **Collects student names into a `TreeSet` (sorted, unique)**.
- **Makes the result immutable using `Collections.unmodifiableSet()`**.
- **Output**:
  ```
  Sorted, Immutable Student Names: [Alice, Bob, Charlie]
  ```

---

## **Key Takeaways**
- **`collectingAndThen()` allows post-processing of collected data**.
- **Common use cases include immutability, formatting, and custom calculations**.
- **Works well with `groupingBy()`, `counting()`, `minBy()`, and `maxBy()`**.
- **Useful for ensuring non-empty results (`orElse()` for defaults)**.


# What is Comparator?

In **Java 8**, `Comparator` is an **interface** used for **custom sorting** of objects. It is part of the `java.util` package and provides multiple **default** and **static methods** that make sorting more flexible and concise.

---

## **Key Features of `Comparator` in Java 8**
| Feature | Description |
|---------|------------|
| **Functional Interface** | `Comparator<T>` is a **functional interface**, so it can be implemented using **lambda expressions**. |
| **Static Methods** | Java 8 introduces **`Comparator.comparing()`** and other static methods for easy sorting. |
| **Default Methods** | New methods like **`thenComparing()`** allow **multi-level sorting**. |
| **Reverse Sorting** | `reversed()` method provides an easy way to sort in **descending order**. |
| **Null Handling** | `nullsFirst()` and `nullsLast()` help handle `null` values safely in sorting. |

---

## **Basic Syntax of `Comparator` in Java 8**
```java
Comparator<T> comparator = Comparator.comparing(T::getField);
```
- **`Comparator.comparing()`** is used to compare objects based on a specific field.
- **Lambdas** simplify comparator logic, making it more readable.

---

## **Difference Between `Comparable` and `Comparator`**
| Feature | `Comparable` | `Comparator` |
|---------|-------------|-------------|
| **Where to Implement** | Implemented inside the class (`compareTo` method) | Implemented as a separate class or lambda |
| **Sorting Logic** | Natural ordering (default sorting) | Custom ordering (multiple sorting options) |
| **Affects Original Class?** | Yes (modifies class itself) | No (used externally) |
| **Multiple Sorting** | No | Yes (`thenComparing()` allows multi-level sorting) |
| **Modification Requirement** | Requires modifying class source code | Works without modifying class |

---

## **Basic Example: Sorting Employees by Salary**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " ($" + salary + ")";
    }
}

public class ComparatorExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        employees.sort(Comparator.comparing(e -> e.salary));

        System.out.println("Employees Sorted by Salary: " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparing(e -> e.salary)`** for sorting.
- **Sorts employees by salary in ascending order**.
- **Output**:
  ```
  Employees Sorted by Salary: [Bob ($50000), Alice ($70000), Charlie ($120000)]
  ```

---

## **Advanced Use Cases of `Comparator`**

### **1. Sorting by Multiple Fields (`thenComparing()`)**
#### **Scenario**: Sort employees **by department**, then **by salary**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " (" + department + " - $" + salary + ")";
    }
}

public class ComparatorExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        employees.sort(Comparator.comparing(Employee::getDepartment)
                                 .thenComparing(Employee::getSalary));

        System.out.println("Sorted Employees: " + employees);
    }
}
```
### **Explanation**
- **First sorts by `department`**.
- **Then sorts by `salary` within each department**.
- **Output**:
  ```
  Sorted Employees: [Eve (Finance - $80000), Bob (HR - $50000), David (HR - $60000), Alice (IT - $70000), Charlie (IT - $120000)]
  ```

---

### **2. Sorting in Reverse Order**
#### **Scenario**: Sort **employees by salary in descending order**.
```java
employees.sort(Comparator.comparing(Employee::getSalary).reversed());
```
### **Explanation**
- **Uses `.reversed()` to sort in descending order**.

---

### **3. Sorting with `null` Handling (`nullsFirst()`, `nullsLast()`)**
#### **Scenario**: Sort **employees by name**, but place `null` names **at the end**.
```java
employees.sort(Comparator.comparing(Employee::getName, Comparator.nullsLast(String::compareTo)));
```
### **Explanation**
- **Ensures `null` values appear at the end** instead of causing errors.

---

### **4. Sorting Objects Using Lambda**
#### **Scenario**: Sort a list of **students by marks** using **lambda expression**.
```java
import java.util.Arrays;
import java.util.List;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }

    @Override
    public String toString() {
        return name + " (" + marks + ")";
    }
}

public class ComparatorExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75)
        );

        students.sort((s1, s2) -> Integer.compare(s2.marks, s1.marks));

        System.out.println("Sorted Students by Marks (Descending): " + students);
    }
}
```
### **Explanation**
- **Uses lambda expression** for comparison.
- **Sorts students by marks in descending order**.
- **Output**:
  ```
  Sorted Students by Marks (Descending): [Alice (85), Charlie (75), Bob (45)]
  ```

---

### **5. Custom Sorting for Case-Insensitive Names**
#### **Scenario**: Sort names **ignoring case**.
```java
employees.sort(Comparator.comparing(e -> e.name.toLowerCase()));
```
### **Explanation**
- **Ensures case-insensitive sorting**.

---

### **6. Using Streams to Sort (`sorted()`)**
#### **Scenario**: Sort a **list of transactions by amount**.
```java
List<Transaction> sortedTransactions = transactions.stream()
                                                   .sorted(Comparator.comparing(Transaction::getAmount))
                                                   .collect(Collectors.toList());
```
### **Explanation**
- **Sorts inside a stream pipeline**.
- **Returns a new sorted list**.

---

## **Comparator vs `sorted()` in Stream**
| Feature | `Collections.sort()` / `List.sort()` | `Stream.sorted()` |
|---------|----------------------------|----------------|
| **Modifies Original List?** | ✅ Yes | ❌ No |
| **Returns New List?** | ❌ No | ✅ Yes |
| **Performance** | ✅ Faster | ⚠️ Slightly slower |
| **Best Use Case** | Sorting existing lists | Sorting while streaming |

---

## **Key Takeaways**
- **Java 8 introduces `Comparator.comparing()` and `thenComparing()` for easy sorting**.
- **Lambdas simplify `Comparator` implementation**.
- **Use `reversed()` for descending order**.
- **Use `nullsFirst()` and `nullsLast()` to handle `null` values safely**.
- **Streams can sort dynamically using `sorted()`**.

Would you like **performance comparisons between `Comparator` and `Comparable`?** 🚀

# What is Comparator.comparing()?

The `Comparator.comparing()` method in **Java 8** is a **static method** that helps create a `Comparator` using a **key extractor function**. It provides an **elegant and readable way** to sort objects based on a specific field.

---

## **Syntax of `Comparator.comparing()`**
```java
Comparator<T> comparing(Function<? super T, ? extends U> keyExtractor);
```
- **`T`** → Type of elements to be compared.
- **`U`** → Type of the field used for comparison.
- **`keyExtractor`** → A **function that extracts a key** from the object for comparison.

### **Variants of `Comparator.comparing()`**
| Method | Description |
|---------|------------|
| `Comparator.comparing(Function<T, U> keyExtractor)` | Compares objects based on a **single field**. |
| `Comparator.comparing(Function<T, U> keyExtractor, Comparator<U> keyComparator)` | Uses a **custom comparator** for comparison. |
| `Comparator.comparingInt(ToIntFunction<T> keyExtractor)` | Optimized for **int fields**. |
| `Comparator.comparingLong(ToLongFunction<T> keyExtractor)` | Optimized for **long fields**. |
| `Comparator.comparingDouble(ToDoubleFunction<T> keyExtractor)` | Optimized for **double fields**. |

---

## **Basic Example: Sorting Employees by Salary**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " ($" + salary + ")";
    }
}

public class ComparingExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000),
            new Employee("Bob", 50000),
            new Employee("Charlie", 120000)
        );

        employees.sort(Comparator.comparing(e -> e.salary));

        System.out.println("Employees Sorted by Salary: " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparing(e -> e.salary)`** for sorting.
- **Sorts employees by salary in ascending order**.
- **Output**:
  ```
  Employees Sorted by Salary: [Bob ($50000), Alice ($70000), Charlie ($120000)]
  ```

---

## **Advanced Use Cases of `Comparator.comparing()`**

### **1. Sorting by Multiple Fields (`thenComparing()`)**
#### **Scenario**: Sort employees **by department**, then **by salary**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " (" + department + " - $" + salary + ")";
    }
}

public class ComparingExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        employees.sort(Comparator.comparing(Employee::getDepartment)
                                 .thenComparing(Employee::getSalary));

        System.out.println("Sorted Employees: " + employees);
    }
}
```
### **Explanation**
- **First sorts by `department`**.
- **Then sorts by `salary` within each department**.
- **Output**:
  ```
  Sorted Employees: [Eve (Finance - $80000), Bob (HR - $50000), David (HR - $60000), Alice (IT - $70000), Charlie (IT - $120000)]
  ```

---

### **2. Sorting in Reverse Order**
#### **Scenario**: Sort **employees by salary in descending order**.
```java
employees.sort(Comparator.comparing(Employee::getSalary).reversed());
```
### **Explanation**
- **Uses `.reversed()` to sort in descending order**.

---

### **3. Sorting with `null` Handling (`nullsFirst()`, `nullsLast()`)**
#### **Scenario**: Sort **employees by name**, but place `null` names **at the end**.
```java
employees.sort(Comparator.comparing(Employee::getName, Comparator.nullsLast(String::compareTo)));
```
### **Explanation**
- **Ensures `null` values appear at the end** instead of causing errors.

---

### **4. Sorting Objects Using Lambda**
#### **Scenario**: Sort a list of **students by marks** using **lambda expression**.
```java
import java.util.Arrays;
import java.util.List;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }

    @Override
    public String toString() {
        return name + " (" + marks + ")";
    }
}

public class ComparingExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75)
        );

        students.sort((s1, s2) -> Integer.compare(s2.marks, s1.marks));

        System.out.println("Sorted Students by Marks (Descending): " + students);
    }
}
```
### **Explanation**
- **Uses lambda expression** for comparison.
- **Sorts students by marks in descending order**.
- **Output**:
  ```
  Sorted Students by Marks (Descending): [Alice (85), Charlie (75), Bob (45)]
  ```

---

### **5. Custom Sorting for Case-Insensitive Names**
#### **Scenario**: Sort names **ignoring case**.
```java
employees.sort(Comparator.comparing(e -> e.name.toLowerCase()));
```
### **Explanation**
- **Ensures case-insensitive sorting**.

---

### **6. Using Streams to Sort (`sorted()`)**
#### **Scenario**: Sort a **list of transactions by amount**.
```java
List<Transaction> sortedTransactions = transactions.stream()
                                                   .sorted(Comparator.comparing(Transaction::getAmount))
                                                   .collect(Collectors.toList());
```
### **Explanation**
- **Sorts inside a stream pipeline**.
- **Returns a new sorted list**.

---

## **Performance Considerations**
- **For simple sorting** → Use `Comparator.comparing()`.
- **For multiple fields** → Use `thenComparing()`.
- **For primitive types** → Use `comparingInt()`, `comparingLong()`, or `comparingDouble()` for better performance.

---

## **Key Takeaways**
- **Java 8 introduces `Comparator.comparing()` for easy sorting**.
- **Lambdas simplify `Comparator` implementation**.
- **Use `thenComparing()` for multi-field sorting**.
- **Use `reversed()` for descending order**.
- **Use `nullsFirst()` and `nullsLast()` to handle `null` values safely**.
- **Streams can sort dynamically using `sorted()`**.

Would you like **performance comparisons between `Comparator.comparing()` and `Comparable`?** 🚀

# What is Comparator.comparingInt()?
The **`Comparator.comparingInt()`** method in **Java 8** is a specialized version of **`Comparator.comparing()`** designed for **primitive `int` values**. It provides better **performance** because it **avoids unnecessary boxing/unboxing** when working with `int` fields.

---

## **Syntax of `Comparator.comparingInt()`**
```java
Comparator<T> comparingInt(ToIntFunction<? super T> keyExtractor);
```
- **`T`** → The type of objects to compare.
- **`keyExtractor`** → A **function** that extracts an `int` field from an object.
- **Returns a `Comparator<T>`** that can be used for sorting.

---

## **Why Use `comparingInt()` Instead of `comparing()`?**
| Feature | `Comparator.comparing()` | `Comparator.comparingInt()` |
|---------|-----------------|------------------|
| **Works With** | Any data type | Only `int` values |
| **Autoboxing** | Converts `int` to `Integer` (boxing overhead) | Uses `int` directly (no boxing) |
| **Performance** | Slower due to boxing/unboxing | Faster for `int` fields |

---

## **Basic Example: Sorting Employees by Age (int field)**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Employee {
    String name;
    int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return name + " (" + age + " years)";
    }
}

public class ComparingIntExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 30),
            new Employee("Bob", 25),
            new Employee("Charlie", 35)
        );

        employees.sort(Comparator.comparingInt(e -> e.age));

        System.out.println("Employees Sorted by Age: " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparingInt(e -> e.age)`** for sorting.
- **Sorts employees by age in ascending order**.
- **Output**:
  ```
  Employees Sorted by Age: [Bob (25 years), Alice (30 years), Charlie (35 years)]
  ```

---

## **Advanced Use Cases of `Comparator.comparingInt()`**

### **1. Sorting by Multiple Fields Using `thenComparingInt()`**
#### **Scenario**: Sort employees **by department**, then **by age**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    int age;

    public Employee(String name, String department, int age) {
        this.name = name;
        this.department = department;
        this.age = age;
    }

    @Override
    public String toString() {
        return name + " (" + department + " - " + age + " years)";
    }
}

public class ComparingIntExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 30),
            new Employee("Bob", "HR", 25),
            new Employee("Charlie", "IT", 35),
            new Employee("David", "HR", 40),
            new Employee("Eve", "Finance", 28)
        );

        employees.sort(Comparator.comparing(Employee::getDepartment)
                                 .thenComparingInt(Employee::getAge));

        System.out.println("Sorted Employees: " + employees);
    }
}
```
### **Explanation**
- **First sorts by `department`**.
- **Then sorts by `age` within each department**.
- **Output**:
  ```
  Sorted Employees: [Eve (Finance - 28 years), Bob (HR - 25 years), David (HR - 40 years), Alice (IT - 30 years), Charlie (IT - 35 years)]
  ```

---

### **2. Sorting Students by Marks in Descending Order**
#### **Scenario**: Sort **students by marks in descending order**.

```java
import java.util.Arrays;
import java.util.List;

class Student {
    String name;
    int marks;

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }

    @Override
    public String toString() {
        return name + " (" + marks + ")";
    }
}

public class ComparingIntExample3 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 85),
            new Student("Bob", 45),
            new Student("Charlie", 75)
        );

        students.sort(Comparator.comparingInt(s -> s.marks).reversed());

        System.out.println("Sorted Students by Marks (Descending): " + students);
    }
}
```
### **Explanation**
- **Sorts students by marks in descending order** using `.reversed()`.
- **Output**:
  ```
  Sorted Students by Marks (Descending): [Alice (85), Charlie (75), Bob (45)]
  ```

---

### **3. Sorting Transactions by Amount**
#### **Scenario**: Sort a list of **transactions by amount**.

```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    int id;
    int amount;

    public Transaction(int id, int amount) {
        this.id = id;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction " + id + " ($" + amount + ")";
    }
}

public class ComparingIntExample4 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction(101, 500),
            new Transaction(102, 200),
            new Transaction(103, 800)
        );

        transactions.sort(Comparator.comparingInt(t -> t.amount));

        System.out.println("Sorted Transactions: " + transactions);
    }
}
```
### **Explanation**
- **Sorts transactions in ascending order by amount**.
- **Output**:
  ```
  Sorted Transactions: [Transaction 102 ($200), Transaction 101 ($500), Transaction 103 ($800)]
  ```

---

### **4. Handling `null` Values in Sorting**
#### **Scenario**: Sort **employees by age** but handle `null` values.

```java
employees.sort(Comparator.comparingInt(Employee::getAge, Comparator.nullsLast(Integer::compareTo)));
```
### **Explanation**
- **Ensures `null` values appear at the end** instead of causing errors.

---

### **5. Using Streams to Sort (`sorted()`)**
#### **Scenario**: Sort a **list of products by price**.
```java
List<Product> sortedProducts = products.stream()
                                       .sorted(Comparator.comparingInt(Product::getPrice))
                                       .collect(Collectors.toList());
```
### **Explanation**
- **Sorts inside a stream pipeline**.
- **Returns a new sorted list**.

---

## **Performance Considerations**
- **Use `comparingInt()` for better performance on `int` fields**.
- **For non-primitive fields, use `comparing()`**.
- **Use `thenComparingInt()` for multi-field sorting**.

---

## **Key Takeaways**
- **Java 8 introduces `Comparator.comparingInt()` for sorting `int` fields efficiently**.
- **Avoids boxing overhead compared to `comparing()`**.
- **Use `thenComparingInt()` for multi-field sorting**.
- **Use `.reversed()` for descending order**.
- **Use `nullsFirst()` and `nullsLast()` to handle `null` values safely**.
- **Streams can sort dynamically using `sorted()`**.

Would you like **performance comparisons between `comparingInt()` and `comparing()`?** 🚀

# What is Comparator.comparingLong()?

The **`Comparator.comparingLong()`** method in **Java 8** is a specialized version of **`Comparator.comparing()`** optimized for **primitive `long` values**. It **avoids unnecessary boxing/unboxing**, making sorting **more efficient** when dealing with `long` fields.

---

## **Syntax of `Comparator.comparingLong()`**
```java
Comparator<T> comparingLong(ToLongFunction<? super T> keyExtractor);
```
- **`T`** → Type of the objects to compare.
- **`keyExtractor`** → A **function that extracts a `long` field** from an object.
- **Returns a `Comparator<T>`** that can be used for sorting.

---

## **Why Use `comparingLong()` Instead of `comparing()`?**
| Feature | `Comparator.comparing()` | `Comparator.comparingLong()` |
|---------|-----------------|------------------|
| **Works With** | Any data type | Only `long` values |
| **Autoboxing** | Converts `long` to `Long` (boxing overhead) | Uses `long` directly (no boxing) |
| **Performance** | Slower due to boxing/unboxing | Faster for `long` fields |

---

## **Basic Example: Sorting Employees by Experience (long field)**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Employee {
    String name;
    long experience;

    public Employee(String name, long experience) {
        this.name = name;
        this.experience = experience;
    }

    @Override
    public String toString() {
        return name + " (" + experience + " years)";
    }
}

public class ComparingLongExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 10L),
            new Employee("Bob", 5L),
            new Employee("Charlie", 15L)
        );

        employees.sort(Comparator.comparingLong(e -> e.experience));

        System.out.println("Employees Sorted by Experience: " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparingLong(e -> e.experience)`** for sorting.
- **Sorts employees by experience in ascending order**.
- **Output**:
  ```
  Employees Sorted by Experience: [Bob (5 years), Alice (10 years), Charlie (15 years)]
  ```

---

## **Advanced Use Cases of `Comparator.comparingLong()`**

### **1. Sorting by Multiple Fields Using `thenComparingLong()`**
#### **Scenario**: Sort **employees by department**, then **by experience**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    long experience;

    public Employee(String name, String department, long experience) {
        this.name = name;
        this.department = department;
        this.experience = experience;
    }

    @Override
    public String toString() {
        return name + " (" + department + " - " + experience + " years)";
    }
}

public class ComparingLongExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 10),
            new Employee("Bob", "HR", 5),
            new Employee("Charlie", "IT", 15),
            new Employee("David", "HR", 20),
            new Employee("Eve", "Finance", 8)
        );

        employees.sort(Comparator.comparing(Employee::getDepartment)
                                 .thenComparingLong(Employee::getExperience));

        System.out.println("Sorted Employees: " + employees);
    }
}
```
### **Explanation**
- **First sorts by `department`**.
- **Then sorts by `experience` within each department**.
- **Output**:
  ```
  Sorted Employees: [Eve (Finance - 8 years), Bob (HR - 5 years), David (HR - 20 years), Alice (IT - 10 years), Charlie (IT - 15 years)]
  ```

---

### **2. Sorting Transactions by Transaction ID**
#### **Scenario**: Sort **transactions by transaction ID (long value)**.

```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    long transactionId;
    double amount;

    public Transaction(long transactionId, double amount) {
        this.transactionId = transactionId;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction ID: " + transactionId + " ($" + amount + ")";
    }
}

public class ComparingLongExample3 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction(1005L, 500.75),
            new Transaction(1002L, 200.50),
            new Transaction(1008L, 800.25)
        );

        transactions.sort(Comparator.comparingLong(t -> t.transactionId));

        System.out.println("Sorted Transactions by ID: " + transactions);
    }
}
```
### **Explanation**
- **Sorts transactions by transaction ID in ascending order**.
- **Output**:
  ```
  Sorted Transactions by ID: [Transaction ID: 1002 ($200.5), Transaction ID: 1005 ($500.75), Transaction ID: 1008 ($800.25)]
  ```

---

### **3. Sorting Orders by Delivery Time (Epoch Time - `long`)**
#### **Scenario**: Sort **orders based on delivery time (timestamp in milliseconds)**.

```java
import java.util.Arrays;
import java.util.List;

class Order {
    long deliveryTime; // Epoch timestamp in milliseconds
    String product;

    public Order(long deliveryTime, String product) {
        this.deliveryTime = deliveryTime;
        this.product = product;
    }

    @Override
    public String toString() {
        return product + " (Delivery: " + deliveryTime + ")";
    }
}

public class ComparingLongExample4 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(1627801200000L, "Laptop"),
            new Order(1627737600000L, "Phone"),
            new Order(1627894000000L, "Tablet")
        );

        orders.sort(Comparator.comparingLong(o -> o.deliveryTime));

        System.out.println("Orders Sorted by Delivery Time: " + orders);
    }
}
```
### **Explanation**
- **Sorts orders based on delivery time (timestamp values)**.
- **Output**:
  ```
  Orders Sorted by Delivery Time: [Phone (Delivery: 1627737600000), Laptop (Delivery: 1627801200000), Tablet (Delivery: 1627894000000)]
  ```

---

### **4. Sorting Employees by Salary Using `comparingLong()`**
#### **Scenario**: Sort **employees by salary (`long` field)**.

```java
employees.sort(Comparator.comparingLong(Employee::getSalary));
```
### **Explanation**
- **Sorts employees based on salary using `comparingLong()`**.

---

### **5. Using Streams to Sort (`sorted()`)**
#### **Scenario**: Sort **list of products by sales volume (`long`)**.
```java
List<Product> sortedProducts = products.stream()
                                       .sorted(Comparator.comparingLong(Product::getSalesVolume))
                                       .collect(Collectors.toList());
```
### **Explanation**
- **Sorts inside a stream pipeline**.
- **Returns a new sorted list**.

---

## **Performance Considerations**
- **Use `comparingLong()` for better performance on `long` fields**.
- **For non-primitive fields, use `comparing()`**.
- **Use `thenComparingLong()` for multi-field sorting**.

---

## **Key Takeaways**
- **Java 8 introduces `Comparator.comparingLong()` for sorting `long` fields efficiently**.
- **Avoids boxing overhead compared to `comparing()`**.
- **Use `thenComparingLong()` for multi-field sorting**.
- **Use `.reversed()` for descending order**.
- **Use `nullsFirst()` and `nullsLast()` to handle `null` values safely**.
- **Streams can sort dynamically using `sorted()`**.

Would you like **performance comparisons between `comparingLong()` and `comparing()`?** 🚀

# What is Comparator.comparingDouble()?

The **`Comparator.comparingDouble()`** method in **Java 8** is a specialized version of **`Comparator.comparing()`** designed for **primitive `double` values**. It provides a **more efficient way** to compare and sort objects based on a `double` field by **avoiding unnecessary boxing/unboxing**.

---

## **Syntax of `Comparator.comparingDouble()`**
```java
Comparator<T> comparingDouble(ToDoubleFunction<? super T> keyExtractor);
```
- **`T`** → Type of the objects to compare.
- **`keyExtractor`** → A function that extracts a **`double` value** from an object.
- **Returns a `Comparator<T>`** that can be used for sorting.

---

## **Why Use `comparingDouble()` Instead of `comparing()`?**
| Feature | `Comparator.comparing()` | `Comparator.comparingDouble()` |
|---------|-----------------|------------------|
| **Works With** | Any data type | Only `double` values |
| **Autoboxing** | Converts `double` to `Double` (boxing overhead) | Uses `double` directly (no boxing) |
| **Performance** | Slower due to boxing/unboxing | Faster for `double` fields |

---

## **Basic Example: Sorting Employees by Salary (double field)**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Employee {
    String name;
    double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " ($" + salary + ")";
    }
}

public class ComparingDoubleExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", 70000.50),
            new Employee("Bob", 50000.75),
            new Employee("Charlie", 120000.25)
        );

        employees.sort(Comparator.comparingDouble(e -> e.salary));

        System.out.println("Employees Sorted by Salary: " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparingDouble(e -> e.salary)`** for sorting.
- **Sorts employees by salary in ascending order**.
- **Output**:
  ```
  Employees Sorted by Salary: [Bob ($50000.75), Alice ($70000.5), Charlie ($120000.25)]
  ```

---

## **Advanced Use Cases of `Comparator.comparingDouble()`**

### **1. Sorting by Multiple Fields Using `thenComparingDouble()`**
#### **Scenario**: Sort employees **by department**, then **by salary**.

```java
import java.util.*;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " (" + department + " - $" + salary + ")";
    }
}

public class ComparingDoubleExample2 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000.50),
            new Employee("Bob", "HR", 50000.75),
            new Employee("Charlie", "IT", 120000.25),
            new Employee("David", "HR", 60000.90),
            new Employee("Eve", "Finance", 80000.20)
        );

        employees.sort(Comparator.comparing(Employee::getDepartment)
                                 .thenComparingDouble(Employee::getSalary));

        System.out.println("Sorted Employees: " + employees);
    }
}
```
### **Explanation**
- **First sorts by `department`**.
- **Then sorts by `salary` within each department**.
- **Output**:
  ```
  Sorted Employees: [Eve (Finance - $80000.2), Bob (HR - $50000.75), David (HR - $60000.9), Alice (IT - $70000.5), Charlie (IT - $120000.25)]
  ```

---

### **2. Sorting Products by Price**
#### **Scenario**: Sort **products by price (double value)**.

```java
import java.util.Arrays;
import java.util.List;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " ($" + price + ")";
    }
}

public class ComparingDoubleExample3 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        products.sort(Comparator.comparingDouble(p -> p.price));

        System.out.println("Sorted Products by Price: " + products);
    }
}
```
### **Explanation**
- **Sorts products in ascending order by price**.
- **Output**:
  ```
  Sorted Products by Price: [Tablet ($450.75), Phone ($800.5), Laptop ($1200.99)]
  ```

---

### **3. Sorting Students by GPA in Descending Order**
#### **Scenario**: Sort **students by GPA in descending order**.

```java
import java.util.Arrays;
import java.util.List;

class Student {
    String name;
    double gpa;

    public Student(String name, double gpa) {
        this.name = name;
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return name + " (GPA: " + gpa + ")";
    }
}

public class ComparingDoubleExample4 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 3.85),
            new Student("Bob", 2.75),
            new Student("Charlie", 3.50)
        );

        students.sort(Comparator.comparingDouble(s -> s.gpa).reversed());

        System.out.println("Sorted Students by GPA (Descending): " + students);
    }
}
```
### **Explanation**
- **Sorts students by GPA in descending order** using `.reversed()`.
- **Output**:
  ```
  Sorted Students by GPA (Descending): [Alice (GPA: 3.85), Charlie (GPA: 3.5), Bob (GPA: 2.75)]
  ```

---

### **4. Sorting Transactions by Amount**
#### **Scenario**: Sort **transactions by amount**.

```java
import java.util.Arrays;
import java.util.List;

class Transaction {
    double amount;

    public Transaction(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction ($" + amount + ")";
    }
}

public class ComparingDoubleExample5 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction(500.75),
            new Transaction(200.50),
            new Transaction(800.25)
        );

        transactions.sort(Comparator.comparingDouble(t -> t.amount));

        System.out.println("Sorted Transactions by Amount: " + transactions);
    }
}
```
### **Explanation**
- **Sorts transactions by amount in ascending order**.
- **Output**:
  ```
  Sorted Transactions by Amount: [Transaction ($200.5), Transaction ($500.75), Transaction ($800.25)]
  ```

---

### **5. Using Streams to Sort (`sorted()`)**
#### **Scenario**: Sort **list of orders by total price (`double`)**.
```java
List<Order> sortedOrders = orders.stream()
                                 .sorted(Comparator.comparingDouble(Order::getTotalPrice))
                                 .collect(Collectors.toList());
```
### **Explanation**
- **Sorts inside a stream pipeline**.
- **Returns a new sorted list**.

---

## **Performance Considerations**
- **Use `comparingDouble()` for better performance on `double` fields**.
- **For non-primitive fields, use `comparing()`**.
- **Use `thenComparingDouble()` for multi-field sorting**.

---

## **Key Takeaways**
- **Java 8 introduces `Comparator.comparingDouble()` for sorting `double` fields efficiently**.
- **Avoids boxing overhead compared to `comparing()`**.
- **Use `thenComparingDouble()` for multi-field sorting**.
- **Use `.reversed()` for descending order**.
- **Use `nullsFirst()` and `nullsLast()` to handle `null` values safely**.
- **Streams can sort dynamically using `sorted()`**.

Would you like **performance comparisons between `comparingDouble()` and `comparing()`?** 🚀

# What is Comparator.naturalOrder()?

The **`Comparator.naturalOrder()`** method in **Java 8** is a **static method** that returns a **Comparator** that sorts elements in their **natural ordering**.  

Natural ordering means:  
- **Numbers** (`Integer`, `Double`, `Long`, etc.) → Sorted in **ascending order**.  
- **Strings** → Sorted **alphabetically** (lexicographically).  
- **Dates** → Sorted **chronologically**.  

It **only works with types that implement the `Comparable` interface**, such as **String, Integer, Double, LocalDate, etc.**  

---

## **Syntax of `Comparator.naturalOrder()`**
```java
Comparator<T> naturalOrder();
```
- **Returns a `Comparator<T>`** that sorts elements in their **natural order**.
- **Works only with `Comparable` types**.

---

## **Why Use `Comparator.naturalOrder()`?**
| Feature | `Comparator.naturalOrder()` | `Comparator.comparing()` |
|---------|-----------------|------------------|
| **Works With** | `Comparable` types | Any type |
| **Best Use Case** | Direct sorting of `Comparable` objects | Sorting based on a specific field |
| **Performance** | Efficient | Slightly more overhead |
| **Usability** | Simple and clean | More flexible |

---

## **Basic Example: Sorting a List of Strings**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class NaturalOrderExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Charlie", "Alice", "Bob");

        names.sort(Comparator.naturalOrder());

        System.out.println("Sorted Names: " + names);
    }
}
```
### **Explanation**
- **Uses `Comparator.naturalOrder()`** to sort **Strings alphabetically**.
- **Output**:
  ```
  Sorted Names: [Alice, Bob, Charlie]
  ```

---

## **Advanced Use Cases of `Comparator.naturalOrder()`**

### **1. Sorting a List of Numbers**
#### **Scenario**: Sort a list of **integers in natural order**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

public class NaturalOrderExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 1, 8, 3, 9, 2);

        numbers.sort(Comparator.naturalOrder());

        System.out.println("Sorted Numbers: " + numbers);
    }
}
```
### **Explanation**
- **Uses `Comparator.naturalOrder()`** to sort **integers in ascending order**.
- **Output**:
  ```
  Sorted Numbers: [1, 2, 3, 5, 8, 9]
  ```

---

### **2. Sorting a List of Dates**
#### **Scenario**: Sort a list of **LocalDate values in natural order**.
```java
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

public class NaturalOrderExample3 {
    public static void main(String[] args) {
        List<LocalDate> dates = Arrays.asList(
            LocalDate.of(2023, 5, 10),
            LocalDate.of(2021, 3, 15),
            LocalDate.of(2022, 8, 20)
        );

        dates.sort(Comparator.naturalOrder());

        System.out.println("Sorted Dates: " + dates);
    }
}
```
### **Explanation**
- **Uses `Comparator.naturalOrder()`** to sort **dates chronologically**.
- **Output**:
  ```
  Sorted Dates: [2021-03-15, 2022-08-20, 2023-05-10]
  ```

---

### **3. Sorting Employees by Name (Natural Order)**
#### **Scenario**: Sort employees **by name alphabetically**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

class Employee {
    String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

public class NaturalOrderExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Charlie"),
            new Employee("Alice"),
            new Employee("Bob")
        );

        employees.sort(Comparator.comparing(e -> e.name, Comparator.naturalOrder()));

        System.out.println("Sorted Employees by Name: " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparing(Employee::getName, Comparator.naturalOrder())`**.
- **Sorts employees alphabetically by name**.
- **Output**:
  ```
  Sorted Employees by Name: [Alice, Bob, Charlie]
  ```

---

### **4. Sorting with Streams**
#### **Scenario**: Sort a **list of products by price** using streams.
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " ($" + price + ")";
    }
}

public class NaturalOrderExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        List<Product> sortedProducts = products.stream()
                                               .sorted(Comparator.comparing(p -> p.price, Comparator.naturalOrder()))
                                               .collect(Collectors.toList());

        System.out.println("Sorted Products by Price: " + sortedProducts);
    }
}
```
### **Explanation**
- **Sorts inside a stream pipeline using `sorted()`**.
- **Returns a new sorted list**.
- **Output**:
  ```
  Sorted Products by Price: [Tablet ($450.75), Phone ($800.5), Laptop ($1200.99)]
  ```

---

## **Comparing `naturalOrder()` with Other Comparator Methods**
| Method | Use Case | Works With |
|--------|---------|------------|
| `Comparator.naturalOrder()` | Sort elements in **default order** | `Comparable` types (`Integer`, `String`, `LocalDate`, etc.) |
| `Comparator.reverseOrder()` | Sort in **reverse order** | `Comparable` types |
| `Comparator.comparing()` | Sort **by a specific field** | Any object |
| `Comparator.comparingInt()` | Sort **by an `int` field** | `int` primitive |
| `Comparator.comparingDouble()` | Sort **by a `double` field** | `double` primitive |
| `Comparator.nullsFirst()` | Puts `null` values **before** non-null values | Any type |
| `Comparator.nullsLast()` | Puts `null` values **after** non-null values | Any type |

---

## **Performance Considerations**
- **Use `naturalOrder()` for sorting `Comparable` objects directly**.
- **For sorting by fields, use `comparing()` with `naturalOrder()`**.
- **Avoid unnecessary `null` issues by using `nullsFirst()` or `nullsLast()`**.

---

## **Key Takeaways**
- **`Comparator.naturalOrder()` sorts elements based on their natural sorting**.
- **Only works with `Comparable` types**.
- **Can be used inside `comparing()` to sort objects by fields**.
- **Use `reverseOrder()` to sort in descending order**.
- **Use `sorted()` in streams for dynamic sorting**.

Would you like **performance comparisons between `naturalOrder()` and `reverseOrder()`?** 🚀

# What is Comparator.reverseOrder()?

The **`Comparator.reverseOrder()`** method in **Java 8** is a **static method** that returns a `Comparator` that sorts elements in their **reverse natural order**.

Reverse natural order means:  
- **Numbers** (`Integer`, `Double`, `Long`, etc.) → Sorted in **descending order** (highest to lowest).  
- **Strings** → Sorted in **reverse alphabetical order** (Z to A).  
- **Dates** → Sorted in **reverse chronological order** (latest first).  

It **only works with types that implement the `Comparable` interface**, such as **String, Integer, Double, LocalDate, etc.**  

---

## **Syntax of `Comparator.reverseOrder()`**
```java
Comparator<T> reverseOrder();
```
- **Returns a `Comparator<T>`** that sorts elements in **reverse natural order**.
- **Works only with `Comparable` types**.

---

## **Why Use `Comparator.reverseOrder()`?**
| Feature | `Comparator.naturalOrder()` | `Comparator.reverseOrder()` |
|---------|-----------------|------------------|
| **Sorts In** | Ascending order (default) | Descending order |
| **Best Use Case** | Sorting in increasing order | Sorting in decreasing order |
| **Works With** | `Comparable` types | `Comparable` types |
| **Performance** | Efficient | Efficient |

---

## **Basic Example: Sorting a List of Strings in Reverse Order**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ReverseOrderExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Charlie", "Alice", "Bob");

        names.sort(Comparator.reverseOrder());

        System.out.println("Sorted Names in Reverse Order: " + names);
    }
}
```
### **Explanation**
- **Uses `Comparator.reverseOrder()`** to sort **Strings in reverse alphabetical order**.
- **Output**:
  ```
  Sorted Names in Reverse Order: [Charlie, Bob, Alice]
  ```

---

## **Advanced Use Cases of `Comparator.reverseOrder()`**

### **1. Sorting a List of Numbers in Descending Order**
#### **Scenario**: Sort a list of **integers in descending order**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

public class ReverseOrderExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 1, 8, 3, 9, 2);

        numbers.sort(Comparator.reverseOrder());

        System.out.println("Sorted Numbers in Reverse Order: " + numbers);
    }
}
```
### **Explanation**
- **Uses `Comparator.reverseOrder()`** to sort **integers in descending order**.
- **Output**:
  ```
  Sorted Numbers in Reverse Order: [9, 8, 5, 3, 2, 1]
  ```

---

### **2. Sorting a List of Dates in Reverse Order**
#### **Scenario**: Sort a list of **LocalDate values in reverse chronological order**.
```java
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

public class ReverseOrderExample3 {
    public static void main(String[] args) {
        List<LocalDate> dates = Arrays.asList(
            LocalDate.of(2023, 5, 10),
            LocalDate.of(2021, 3, 15),
            LocalDate.of(2022, 8, 20)
        );

        dates.sort(Comparator.reverseOrder());

        System.out.println("Sorted Dates in Reverse Order: " + dates);
    }
}
```
### **Explanation**
- **Uses `Comparator.reverseOrder()`** to sort **dates in reverse chronological order**.
- **Output**:
  ```
  Sorted Dates in Reverse Order: [2023-05-10, 2022-08-20, 2021-03-15]
  ```

---

### **3. Sorting Employees by Name in Reverse Order**
#### **Scenario**: Sort employees **by name in reverse alphabetical order**.
```java
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;

class Employee {
    String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

public class ReverseOrderExample4 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Charlie"),
            new Employee("Alice"),
            new Employee("Bob")
        );

        employees.sort(Comparator.comparing(e -> e.name, Comparator.reverseOrder()));

        System.out.println("Sorted Employees by Name (Reverse Order): " + employees);
    }
}
```
### **Explanation**
- **Uses `Comparator.comparing(Employee::getName, Comparator.reverseOrder())`**.
- **Sorts employees in reverse alphabetical order**.
- **Output**:
  ```
  Sorted Employees by Name (Reverse Order): [Charlie, Bob, Alice]
  ```

---

### **4. Sorting with Streams**
#### **Scenario**: Sort a **list of products by price in descending order** using streams.
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Product {
    String name;
    double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " ($" + price + ")";
    }
}

public class ReverseOrderExample5 {
    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
            new Product("Laptop", 1200.99),
            new Product("Phone", 800.50),
            new Product("Tablet", 450.75)
        );

        List<Product> sortedProducts = products.stream()
                                               .sorted(Comparator.comparing(p -> p.price, Comparator.reverseOrder()))
                                               .collect(Collectors.toList());

        System.out.println("Sorted Products by Price (Descending): " + sortedProducts);
    }
}
```
### **Explanation**
- **Sorts inside a stream pipeline using `sorted()`**.
- **Returns a new sorted list**.
- **Output**:
  ```
  Sorted Products by Price (Descending): [Laptop ($1200.99), Phone ($800.5), Tablet ($450.75)]
  ```

---

## **Comparing `reverseOrder()` with Other Comparator Methods**
| Method | Use Case | Works With |
|--------|---------|------------|
| `Comparator.naturalOrder()` | Sort elements in **default order** | `Comparable` types (`Integer`, `String`, `LocalDate`, etc.) |
| `Comparator.reverseOrder()` | Sort in **reverse order** | `Comparable` types |
| `Comparator.comparing()` | Sort **by a specific field** | Any object |
| `Comparator.comparingInt()` | Sort **by an `int` field** | `int` primitive |
| `Comparator.comparingDouble()` | Sort **by a `double` field** | `double` primitive |
| `Comparator.nullsFirst()` | Puts `null` values **before** non-null values | Any type |
| `Comparator.nullsLast()` | Puts `null` values **after** non-null values | Any type |

---

## **Performance Considerations**
- **Use `reverseOrder()` for sorting `Comparable` objects in descending order**.
- **For sorting by fields, use `comparing()` with `reverseOrder()`**.
- **Avoid unnecessary `null` issues by using `nullsFirst()` or `nullsLast()`**.

---

## **Key Takeaways**
- **`Comparator.reverseOrder()` sorts elements in reverse natural order**.
- **Only works with `Comparable` types**.
- **Can be used inside `comparing()` to sort objects by fields in descending order**.
- **Use `naturalOrder()` for ascending order**.
- **Use `sorted()` in streams for dynamic sorting**.

# What is Comparator.thenComparing()?

The **`Comparator.thenComparing()`** method in **Java 8** is a **default method** of the `Comparator` interface that allows **multi-level sorting**.  

It is used **after the first sorting criteria** to **define additional sorting rules** when the primary sorting results in a tie (i.e., two elements are equal in the first comparison).  

---

## **Syntax of `Comparator.thenComparing()`**
```java
Comparator<T> thenComparing(Comparator<? super T> other);
```
- **Works after an initial comparator**.
- **Defines secondary sorting** if the primary sorting results in a tie.
- **Can chain multiple `thenComparing()` for deeper sorting levels**.

### **Overloaded Variants**
| Method | Description |
|---------|------------|
| `thenComparing(Comparator<? super T> other)` | Uses another `Comparator<T>` for secondary sorting. |
| `thenComparing(Function<? super T, ? extends U> keyExtractor)` | Extracts a field for secondary sorting using natural order. |
| `thenComparing(Function<? super T, ? extends U> keyExtractor, Comparator<? super U> keyComparator)` | Uses a custom `Comparator<U>` for secondary sorting. |
| `thenComparingInt(ToIntFunction<? super T> keyExtractor)` | Optimized for **int** fields. |
| `thenComparingLong(ToLongFunction<? super T> keyExtractor)` | Optimized for **long** fields. |
| `thenComparingDouble(ToDoubleFunction<? super T> keyExtractor)` | Optimized for **double** fields. |

---

## **Why Use `thenComparing()`?**
| Feature | `Comparator.comparing()` | `Comparator.thenComparing()` |
|---------|-----------------|------------------|
| **Single vs. Multi-Level Sorting** | Used for primary sorting | Used for secondary sorting |
| **Sorts By** | One field | Multiple fields (fallback sorting) |
| **Best Use Case** | Simple sorting (e.g., sort by age) | Complex sorting (e.g., sort by department, then age) |

---

## **Basic Example: Sorting Employees by Department, then Salary**
```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Employee {
    String name;
    String department;
    double salary;

    public Employee(String name, String department, double salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + " (" + department + " - $" + salary + ")";
    }
}

public class ThenComparingExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT", 70000),
            new Employee("Bob", "HR", 50000),
            new Employee("Charlie", "IT", 120000),
            new Employee("David", "HR", 60000),
            new Employee("Eve", "Finance", 80000)
        );

        employees.sort(Comparator.comparing(Employee::getDepartment)
                                 .thenComparing(Employee::getSalary));

        System.out.println("Sorted Employees: " + employees);
    }
}
```
### **Explanation**
1. **First sorts employees by department (`Comparator.comparing(Employee::getDepartment)`)**.
2. **If two employees belong to the same department, sorts by salary (`thenComparing(Employee::getSalary)`)**.
3. **Output**:
   ```
   Sorted Employees: [Eve (Finance - $80000), Bob (HR - $50000), David (HR - $60000), Alice (IT - $70000), Charlie (IT - $120000)]
   ```

---

## **Advanced Use Cases of `thenComparing()`**

### **1. Sorting Students by Grade, then Name**
#### **Scenario**: Sort **students by grade**, then **alphabetically by name**.
```java
import java.util.Arrays;
import java.util.List;

class Student {
    String name;
    char grade;

    public Student(String name, char grade) {
        this.name = name;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return name + " (Grade: " + grade + ")";
    }
}

public class ThenComparingExample2 {
    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
            new Student("Alice", 'B'),
            new Student("Bob", 'A'),
            new Student("Charlie", 'A'),
            new Student("David", 'C'),
            new Student("Eve", 'B')
        );

        students.sort(Comparator.comparing(s -> s.grade)
                                .thenComparing(s -> s.name));

        System.out.println("Sorted Students: " + students);
    }
}
```
### **Explanation**
- **First sorts by grade (`A` → `B` → `C`)**.
- **If two students have the same grade, sorts alphabetically by name**.
- **Output**:
  ```
  Sorted Students: [Bob (Grade: A), Charlie (Grade: A), Alice (Grade: B), Eve (Grade: B), David (Grade: C)]
  ```

---

### **2. Sorting Transactions by Date, then Amount**
#### **Scenario**: Sort **transactions by date**, then **by amount**.
```java
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

class Transaction {
    LocalDate date;
    double amount;

    public Transaction(LocalDate date, double amount) {
        this.date = date;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction on " + date + " ($" + amount + ")";
    }
}

public class ThenComparingExample3 {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
            new Transaction(LocalDate.of(2023, 5, 10), 500.75),
            new Transaction(LocalDate.of(2023, 5, 10), 200.50),
            new Transaction(LocalDate.of(2022, 8, 20), 800.25),
            new Transaction(LocalDate.of(2021, 3, 15), 900.00)
        );

        transactions.sort(Comparator.comparing(t -> t.date)
                                    .thenComparingDouble(t -> t.amount));

        System.out.println("Sorted Transactions: " + transactions);
    }
}
```
### **Explanation**
- **Sorts transactions by date first**.
- **For the same date, sorts transactions by amount in ascending order**.
- **Output**:
  ```
  Sorted Transactions: [Transaction on 2021-03-15 ($900.0), Transaction on 2022-08-20 ($800.25), Transaction on 2023-05-10 ($200.5), Transaction on 2023-05-10 ($500.75)]
  ```

---

### **3. Sorting Orders by Priority, then Price (Descending)**
#### **Scenario**: Sort **orders by priority (`HIGH` > `MEDIUM` > `LOW`)**, then **by price (descending)**.
```java
import java.util.Arrays;
import java.util.List;

class Order {
    String priority;
    double price;

    public Order(String priority, double price) {
        this.priority = priority;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order (" + priority + ", $" + price + ")";
    }
}

public class ThenComparingExample4 {
    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order("MEDIUM", 500.0),
            new Order("HIGH", 1200.0),
            new Order("LOW", 300.0),
            new Order("HIGH", 900.0)
        );

        orders.sort(Comparator.comparing((Order o) -> o.priority)
                              .reversed()
                              .thenComparingDouble(o -> o.price)
                              .reversed());

        System.out.println("Sorted Orders: " + orders);
    }
}
```
### **Explanation**
- **Sorts by priority (`HIGH` > `MEDIUM` > `LOW`)**.
- **For same priority, sorts by price in descending order**.
- **Output**:
  ```
  Sorted Orders: [Order (HIGH, $1200.0), Order (HIGH, $900.0), Order (MEDIUM, $500.0), Order (LOW, $300.0)]
  ```

---

## **Key Takeaways**
- **Use `thenComparing()` for multi-level sorting**.
- **Optimized versions `thenComparingInt()`, `thenComparingLong()`, and `thenComparingDouble()` improve performance**.
- **Works well with `reversed()` for custom sorting orders**.
- **Ideal for sorting complex objects with multiple criteria**.

# What is the difference between Parallel Stream vs Sequential Stream?
# **Parallel Stream vs. Sequential Stream in Java 8**
Java 8 introduced the **Stream API**, which allows functional-style operations on collections. The **Stream API** provides two modes of operation:
1. **Sequential Stream** → Processes elements **one at a time, in order**.
2. **Parallel Stream** → Processes elements **concurrently using multiple threads**.

---

## **Key Differences Between Parallel Stream and Sequential Stream**
| Feature | **Sequential Stream** | **Parallel Stream** |
|---------|------------------|------------------|
| **Processing** | Processes elements **one-by-one (single thread)** | Splits elements across **multiple threads** (parallel execution) |
| **Performance** | Slower for large datasets | Faster for CPU-intensive operations with large datasets |
| **Order** | **Maintains order** of elements | **Does not guarantee order** of execution |
| **Threading** | Uses **main thread** only | Uses **ForkJoinPool (multiple threads)** |
| **Use Case** | Best for **small or order-sensitive** tasks | Best for **large, independent computations** |

---

## **Basic Example: Sequential vs. Parallel Stream**
```java
import java.util.Arrays;
import java.util.List;

public class StreamExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David");

        // Sequential Stream
        System.out.println("Sequential Stream:");
        names.stream()
             .forEach(name -> System.out.println(Thread.currentThread().getName() + " - " + name));

        // Parallel Stream
        System.out.println("\nParallel Stream:");
        names.parallelStream()
             .forEach(name -> System.out.println(Thread.currentThread().getName() + " - " + name));
    }
}
```
### **Explanation**
- **Sequential Stream (`stream()`)** → Uses **main thread**.
- **Parallel Stream (`parallelStream()`)** → Uses **multiple threads**.
- **Output Example (Parallel Stream order may vary)**:
  ```
  Sequential Stream:
  main - Alice
  main - Bob
  main - Charlie
  main - David

  Parallel Stream:
  ForkJoinPool.commonPool-worker-1 - Charlie
  ForkJoinPool.commonPool-worker-2 - Bob
  main - Alice
  ForkJoinPool.commonPool-worker-3 - David
  ```

---

## **Performance Test: Parallel vs. Sequential Stream**
#### **Scenario**: Find the sum of squares of numbers **from 1 to 1 million**.
```java
import java.util.stream.LongStream;

public class StreamExample2 {
    public static void main(String[] args) {
        long startTime, endTime;

        // Sequential Stream
        startTime = System.currentTimeMillis();
        long sumSequential = LongStream.rangeClosed(1, 1_000_000)
                                       .map(x -> x * x)
                                       .sum();
        endTime = System.currentTimeMillis();
        System.out.println("Sequential Sum: " + sumSequential + " | Time Taken: " + (endTime - startTime) + "ms");

        // Parallel Stream
        startTime = System.currentTimeMillis();
        long sumParallel = LongStream.rangeClosed(1, 1_000_000)
                                     .parallel()
                                     .map(x -> x * x)
                                     .sum();
        endTime = System.currentTimeMillis();
        System.out.println("Parallel Sum: " + sumParallel + " | Time Taken: " + (endTime - startTime) + "ms");
    }
}
```
### **Explanation**
- **Sequential Stream** → Processes numbers **one at a time**.
- **Parallel Stream** → Splits workload across **multiple threads**.
- **Expected Output** (Time may vary based on CPU):
  ```
  Sequential Sum: 333333833333500000 | Time Taken: 30ms
  Parallel Sum: 333333833333500000 | Time Taken: 10ms
  ```
  - **Parallel Stream is 2-3 times faster** on large data.

---

## **When to Use Parallel Stream?**
| Best Use Cases | Not Recommended |
|---------------|----------------|
| **Processing large datasets** (millions of elements) | **Small datasets** (thread overhead makes it slower) |
| **CPU-intensive tasks** (e.g., complex calculations) | **I/O operations** (parallelism won’t help) |
| **Order doesn't matter** | **Order-sensitive operations** (like writing to a file) |
| **Independent computations** | **Operations with shared resources** (synchronization issues) |

---

## **Advanced Use Cases of Parallel Stream**

### **1. Counting Employees by Department Using Parallel Stream**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Employee {
    String name;
    String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
}

public class ParallelStreamExample1 {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
            new Employee("Alice", "IT"),
            new Employee("Bob", "HR"),
            new Employee("Charlie", "IT"),
            new Employee("David", "HR"),
            new Employee("Eve", "Finance")
        );

        Map<String, Long> departmentCount = employees.parallelStream()
                                                     .collect(Collectors.groupingBy(e -> e.department, Collectors.counting()));

        System.out.println("Employee Count by Department: " + departmentCount);
    }
}
```
### **Explanation**
- **Uses `parallelStream()`** for faster **grouping and counting**.
- **Output**:
  ```
  Employee Count by Department: {IT=2, HR=2, Finance=1}
  ```

---

### **2. Sorting Large Data Using Parallel Stream**
```java
import java.util.Arrays;
import java.util.List;

public class ParallelStreamExample2 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 2, 8, 1, 4, 7, 3, 6);

        List<Integer> sortedNumbers = numbers.parallelStream()
                                             .sorted()
                                             .toList();

        System.out.println("Sorted Numbers: " + sortedNumbers);
    }
}
```
### **Explanation**
- **Uses `parallelStream().sorted()`** for efficient sorting.
- **Parallel sorting works best for large datasets**.

---

### **3. Filtering Large Datasets Using Parallel Stream**
```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParallelStreamExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = IntStream.rangeClosed(1, 1000000).boxed().collect(Collectors.toList());

        long startTime = System.currentTimeMillis();
        List<Integer> evenNumbers = numbers.parallelStream()
                                           .filter(n -> n % 2 == 0)
                                           .collect(Collectors.toList());
        long endTime = System.currentTimeMillis();

        System.out.println("Filtered " + evenNumbers.size() + " even numbers | Time Taken: " + (endTime - startTime) + "ms");
    }
}
```
### **Explanation**
- **Uses `parallelStream().filter()`** for faster filtering of **large datasets**.

---

## **Key Takeaways**
| Feature | **Sequential Stream** | **Parallel Stream** |
|---------|------------------|------------------|
| **Processing** | **One-by-one (single thread)** | **Multiple threads (concurrent execution)** |
| **Performance** | **Good for small datasets** | **Faster for large datasets** |
| **Order** | **Maintains original order** | **Does not guarantee order** |
| **Threading** | Uses **main thread** | Uses **ForkJoinPool (multiple threads)** |
| **Use Case** | **Order-sensitive tasks** | **CPU-intensive, large data tasks** |

---

## **When to Use Parallel Streams?**
✅ **Use Parallel Streams When:**
- **Working with large datasets** (e.g., millions of elements).
- **Performing CPU-intensive computations** (e.g., numerical processing).
- **Order does not matter**.

❌ **Avoid Parallel Streams When:**
- **Working with small datasets** (overhead is higher than benefits).
- **Performing I/O operations** (e.g., reading/writing files).
- **Operations require strict ordering**.

Would you like a **performance benchmark comparing Parallel Stream vs. ForkJoinPool?** 🚀

# Explain Internal Working of Parallel Stream?

Java 8 **Parallel Stream** is a powerful feature that allows **multi-threaded** processing of streams. It divides data into multiple chunks and processes them in parallel using the **ForkJoinPool framework**. This makes it **faster for large datasets and CPU-intensive operations**.

---

## **How Parallel Stream Works Internally?**
### **1. Splitting the Data**
- **The input data (Collection, Array, Stream) is split into multiple parts**.
- Uses **Spliterator (Splittable Iterator)** to divide the elements into **subtasks**.

### **2. Processing in Parallel**
- Each chunk of data is processed **simultaneously** by different **threads**.
- Uses **ForkJoinPool**, a specialized thread pool designed for recursive parallel tasks.

### **3. Merging the Results**
- Once processing is done, **results from different threads are merged back together**.
- The **order of elements may not be preserved**.

---

## **Step-by-Step Internal Execution of Parallel Stream**
### **1. Uses a Common ForkJoinPool**
- **Parallel Streams** run on the **ForkJoinPool.commonPool()**.
- **Default thread count** = `number of CPU cores`.

```java
System.out.println("Parallel Stream Default Threads: " + ForkJoinPool.commonPool().getParallelism());
```
- On a **quad-core processor**, it prints:
  ```
  Parallel Stream Default Threads: 3
  ```

### **2. Uses Spliterator to Divide Data**
- `Spliterator` is a special iterator that **splits** the stream into smaller parts.
- Parallel Stream **recursively divides the data** using `trySplit()`.

### **3. Uses ForkJoin Framework to Process Data**
- **ForkJoinPool assigns subtasks to multiple threads**.
- Uses **work-stealing algorithm**, meaning **idle threads take unprocessed tasks**.

---

## **Example: How Parallel Stream Splits and Processes Data?**
```java
import java.util.Arrays;
import java.util.List;

public class ParallelStreamExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David", "Eve");

        names.parallelStream()
             .forEach(name -> System.out.println(Thread.currentThread().getName() + " - " + name));
    }
}
```
### **How It Works Internally?**
1. **Spliterator** divides the list into multiple parts.
2. **ForkJoinPool assigns different parts to different threads**.
3. **Threads process the elements in parallel**.
4. **Results are merged** (but order is not guaranteed).

### **Expected Output (Parallel Execution)**
```
ForkJoinPool.commonPool-worker-1 - Alice
ForkJoinPool.commonPool-worker-2 - Bob
main - Charlie
ForkJoinPool.commonPool-worker-3 - David
ForkJoinPool.commonPool-worker-1 - Eve
```

---

## **How ForkJoinPool Works in Parallel Stream?**
### **1. Default Pool Size = CPU Cores - 1**
- Uses `Runtime.getRuntime().availableProcessors()` to get CPU count.
- Example: On a **4-core CPU**, `ForkJoinPool` will create **3 worker threads**.

### **2. Uses Work-Stealing Algorithm**
- **Threads “steal” work** from other threads if they finish early.
- Ensures **efficient CPU utilization**.

### **3. Uses Recursive Task Execution**
- Splits data into **smaller independent tasks** and executes them in parallel.
- Example: **Processing 1 million numbers** → ForkJoinPool will divide the workload **evenly**.

---

## **Performance Example: Parallel vs Sequential Stream**
#### **Scenario**: Finding the sum of squares of numbers **from 1 to 1 million**.
```java
import java.util.stream.LongStream;

public class ParallelStreamExample2 {
    public static void main(String[] args) {
        long startTime, endTime;

        // Sequential Stream
        startTime = System.currentTimeMillis();
        long sumSequential = LongStream.rangeClosed(1, 1_000_000)
                                       .map(x -> x * x)
                                       .sum();
        endTime = System.currentTimeMillis();
        System.out.println("Sequential Sum: " + sumSequential + " | Time Taken: " + (endTime - startTime) + "ms");

        // Parallel Stream
        startTime = System.currentTimeMillis();
        long sumParallel = LongStream.rangeClosed(1, 1_000_000)
                                     .parallel()
                                     .map(x -> x * x)
                                     .sum();
        endTime = System.currentTimeMillis();
        System.out.println("Parallel Sum: " + sumParallel + " | Time Taken: " + (endTime - startTime) + "ms");
    }
}
```
### **Expected Output**
```
Sequential Sum: 333333833333500000 | Time Taken: 30ms
Parallel Sum: 333333833333500000 | Time Taken: 10ms
```
- **Parallel Stream is 2-3 times faster** due to multi-threading.

---

## **Best Practices for Using Parallel Stream**
✅ **Use Parallel Stream When**:
1. **Large Datasets** (millions of elements).
2. **CPU-Intensive Tasks** (mathematical computations).
3. **Independent Operations** (no shared resources).

❌ **Avoid Parallel Stream When**:
1. **Small Datasets** (thread overhead is higher than benefit).
2. **I/O Operations** (parallelism won't help).
3. **Order-Sensitive Operations** (like writing to a file).

---

## **Advanced Example: Parallel Sorting of Large Data**
```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ParallelStreamExample3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(5, 2, 8, 1, 4, 7, 3, 6);

        List<Integer> sortedNumbers = numbers.parallelStream()
                                             .sorted()
                                             .collect(Collectors.toList());

        System.out.println("Sorted Numbers: " + sortedNumbers);
    }
}
```
### **Explanation**
- Uses **parallel stream to sort numbers efficiently**.
- **Parallel sorting works best for large datasets**.

---

## **How to Control Parallel Stream Thread Pool?**
### **1. Using `ForkJoinPool` Manually**
By default, `ForkJoinPool.commonPool()` is used. You can customize it:
```java
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

public class CustomParallelStream {
    public static void main(String[] args) {
        ForkJoinPool customPool = new ForkJoinPool(4); // Set 4 threads

        customPool.submit(() -> {
            IntStream.range(1, 10)
                     .parallel()
                     .forEach(i -> System.out.println(Thread.currentThread().getName() + " - " + i));
        }).join();
    }
}
```
### **Explanation**
- **Creates a custom ForkJoinPool** with **4 threads**.
- **Executes the parallel stream inside this custom thread pool**.

---

## **Key Takeaways**
| Feature | **Sequential Stream** | **Parallel Stream** |
|---------|------------------|------------------|
| **Processing** | **Single-threaded** | **Multi-threaded (ForkJoinPool)** |
| **Performance** | **Good for small tasks** | **Faster for large tasks** |
| **Order** | **Preserves order** | **Does not guarantee order** |
| **Splitting Mechanism** | **Processes elements one by one** | **Uses `Spliterator` to divide data** |
| **Threading** | Uses **main thread** | Uses **ForkJoinPool worker threads** |
| **Use Case** | **Order-sensitive tasks** | **CPU-intensive, large data tasks** |

---

## **When to Use Parallel Stream?**
✅ **Use Parallel Streams When**:
- **Processing large datasets** (e.g., millions of elements).
- **Performing CPU-intensive computations** (e.g., numerical processing).
- **Order does not matter**.

❌ **Avoid Parallel Streams When**:
- **Working with small datasets** (overhead is higher than benefits).
- **Performing I/O operations** (e.g., reading/writing files).
- **Operations require strict ordering**.

Would you like a **deep dive into ForkJoinPool performance optimizations?** 🚀

# What is Task Splitting using SplitIterator?

The **`Spliterator` (Splittable Iterator)** in Java 8 is a **high-performance iterator** designed for **parallel processing**. It enables **task splitting**, allowing large datasets to be processed concurrently using **Parallel Streams** and **ForkJoinPool**.

---

## **How `Spliterator` Works?**
1. **Splits data into multiple smaller parts (subtasks)**.
2. **Processes parts in parallel** using multiple threads.
3. **Combines results** after computation.

🔹 **Unlike `Iterator`, `Spliterator` can split tasks into smaller independent parts, making parallel processing efficient.**

---

## **Key Methods of `Spliterator`**
| Method | Description |
|---------|------------|
| `boolean tryAdvance(Consumer<? super T> action)` | Processes one element at a time. |
| `void forEachRemaining(Consumer<? super T> action)` | Processes all remaining elements. |
| `Spliterator<T> trySplit()` | **Divides the Spliterator into smaller parts** for parallel processing. |
| `long estimateSize()` | Returns an estimated number of elements. |
| `int characteristics()` | Returns characteristics like `ORDERED`, `DISTINCT`, `SIZED`, etc. |

---

## **Basic Example: Splitting a List Using `trySplit()`**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Spliterator;

public class SpliteratorExample1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David", "Eve", "Frank");

        Spliterator<String> spliterator1 = names.spliterator();
        Spliterator<String> spliterator2 = spliterator1.trySplit();

        System.out.println("Processing from Spliterator 1:");
        spliterator1.forEachRemaining(System.out::println);

        System.out.println("\nProcessing from Spliterator 2:");
        spliterator2.forEachRemaining(System.out::println);
    }
}
```
### **Explanation**
1. **Splits the list into two parts** using `trySplit()`.
2. **Each `Spliterator` processes part of the list separately**.
3. **Output**:
   ```
   Processing from Spliterator 1:
   Charlie
   David
   Eve
   Frank

   Processing from Spliterator 2:
   Alice
   Bob
   ```
---

## **How `Spliterator` Works Internally in Parallel Streams**
### **Step-by-Step Execution**
1. **Spliterator Splits Data** → `trySplit()` divides elements recursively.
2. **ForkJoinPool Executes Tasks** → Processes each part in parallel.
3. **Results Are Merged** → After processing, results are combined.

### **Example: Using Parallel Stream with Spliterator**
```java
import java.util.Arrays;
import java.util.List;

public class ParallelStreamSpliterator {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David", "Eve", "Frank");

        names.parallelStream()
             .forEach(name -> System.out.println(Thread.currentThread().getName() + " - " + name));
    }
}
```
### **Expected Output (Order Not Guaranteed)**
```
ForkJoinPool.commonPool-worker-1 - Alice
main - Bob
ForkJoinPool.commonPool-worker-2 - Charlie
ForkJoinPool.commonPool-worker-3 - David
ForkJoinPool.commonPool-worker-1 - Eve
main - Frank
```
- **Parallel stream uses Spliterator to split tasks**.
- **Different threads process parts in parallel**.

---

## **Custom `Spliterator` Implementation**
### **Scenario**: Custom Spliterator to process **words of a sentence** separately.
```java
import java.util.Spliterator;
import java.util.function.Consumer;

class WordSpliterator implements Spliterator<String> {
    private final String[] words;
    private int currentIndex = 0;

    public WordSpliterator(String sentence) {
        this.words = sentence.split(" ");
    }

    @Override
    public boolean tryAdvance(Consumer<? super String> action) {
        if (currentIndex < words.length) {
            action.accept(words[currentIndex]);
            currentIndex++;
            return true;
        }
        return false;
    }

    @Override
    public Spliterator<String> trySplit() {
        int remainingSize = words.length - currentIndex;
        if (remainingSize < 2) {
            return null;
        }

        int splitPoint = currentIndex + remainingSize / 2;
        Spliterator<String> newSpliterator = new WordSpliterator(
            String.join(" ", words, currentIndex, splitPoint)
        );
        currentIndex = splitPoint;
        return newSpliterator;
    }

    @Override
    public long estimateSize() {
        return words.length - currentIndex;
    }

    @Override
    public int characteristics() {
        return ORDERED | SIZED | SUBSIZED;
    }
}

public class CustomSpliteratorExample {
    public static void main(String[] args) {
        String sentence = "Spliterator is useful for parallel processing of streams";
        Spliterator<String> spliterator = new WordSpliterator(sentence);
        spliterator.forEachRemaining(System.out::println);
    }
}
```
### **Explanation**
1. **Implements `Spliterator<String>`** to process words in a sentence.
2. **Overrides `tryAdvance()`** to process words **one by one**.
3. **Overrides `trySplit()`** to **divide the words into separate Spliterators**.
4. **Processes words in chunks**.

---

## **Using `Spliterator` to Process Large Data in Parallel**
```java
import java.util.Spliterator;
import java.util.stream.Stream;

public class LargeDatasetSpliterator {
    public static void main(String[] args) {
        Stream<Integer> numberStream = Stream.iterate(1, n -> n + 1).limit(1000);
        Spliterator<Integer> spliterator = numberStream.spliterator();

        Spliterator<Integer> spliterator2 = spliterator.trySplit();
        
        System.out.println("Processing from Spliterator 1:");
        spliterator.forEachRemaining(System.out::println);

        System.out.println("\nProcessing from Spliterator 2:");
        spliterator2.forEachRemaining(System.out::println);
    }
}
```
### **Explanation**
- **Uses `Spliterator.trySplit()` to split 1000 numbers into two halves**.
- **Each half is processed separately**.

---

## **Characteristics of `Spliterator`**
| Characteristic | Description |
|---------------|------------|
| `ORDERED` | Elements have a defined order (e.g., Lists). |
| `DISTINCT` | All elements are unique. |
| `SORTED` | Elements are sorted (e.g., TreeSet). |
| `SIZED` | Knows the exact number of elements. |
| `SUBSIZED` | If split, each sub-Spliterator is also sized. |
| `IMMUTABLE` | Data cannot be modified during traversal. |
| `NONNULL` | No null elements in the collection. |

### **Example: Checking Characteristics**
```java
import java.util.Arrays;
import java.util.List;
import java.util.Spliterator;

public class SpliteratorCharacteristics {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        Spliterator<String> spliterator = names.spliterator();

        System.out.println("Characteristics: " + spliterator.characteristics());
    }
}
```
### **Expected Output**
```
Characteristics: 16464 (ORDERED | SIZED | SUBSIZED | IMMUTABLE)
```

---

## **Comparison: `Iterator` vs `Spliterator`**
| Feature | `Iterator` | `Spliterator` |
|---------|-----------|--------------|
| **Traversing** | One-by-one | Can split & traverse in parallel |
| **Parallel Support** | No | Yes |
| **Processing Mode** | Sequential | Sequential & Parallel |
| **Order Guarantee** | Yes | Not always guaranteed |

---

## **Key Takeaways**
- **`Spliterator` allows parallel processing by splitting tasks**.
- **`trySplit()` recursively divides data for efficient parallel execution**.
- **Used in Parallel Streams to optimize performance**.
- **Custom `Spliterator` can be created for specialized data processing**.
- **Parallel execution is ideal for large datasets and CPU-bound tasks**.

# What is Task Submission using ForkJoinPool?

`ForkJoinPool` is **Java’s built-in thread pool** for parallel task execution. It is optimized for **task splitting and parallel execution**, making it ideal for **divide-and-conquer algorithms**.

---

## **What is `ForkJoinPool`?**
- **A special thread pool** designed for **parallel computing**.
- Uses **"work-stealing"** → Idle threads "steal" tasks from busy threads.
- **Uses Fork-Join Framework** → Splits large tasks into smaller subtasks, processes them in parallel, and merges results.

🔹 **Ideal for recursive tasks and parallel streams.**

---

## **Internal Working of `ForkJoinPool`**
### **1. Splitting the Task**
- **Divides large tasks into smaller tasks (subtasks).**
- Uses **`RecursiveTask<V>`** (returns a result) or **`RecursiveAction`** (no result).

### **2. Processing Subtasks in Parallel**
- Each subtask is assigned to a **worker thread** in the pool.
- **Work-stealing** ensures efficient thread usage.

### **3. Merging Results**
- **Combines results of subtasks** to get the final output.

---

## **Basic Example: ForkJoinPool vs. Normal Execution**
```java
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

public class ForkJoinExample1 {
    public static void main(String[] args) {
        ForkJoinPool customPool = new ForkJoinPool(4); // Custom ForkJoinPool with 4 threads

        customPool.submit(() -> {
            IntStream.range(1, 10)
                     .parallel()
                     .forEach(i -> System.out.println(Thread.currentThread().getName() + " - " + i));
        }).join();
        
        customPool.shutdown();
    }
}
```
### **Expected Output (Order May Vary)**
```
ForkJoinPool-1-worker-3 - 1
ForkJoinPool-1-worker-1 - 2
ForkJoinPool-1-worker-2 - 3
ForkJoinPool-1-worker-3 - 4
ForkJoinPool-1-worker-1 - 5
ForkJoinPool-1-worker-2 - 6
ForkJoinPool-1-worker-3 - 7
ForkJoinPool-1-worker-1 - 8
ForkJoinPool-1-worker-2 - 9
```
### **Explanation**
- **Creates a custom ForkJoinPool with 4 worker threads.**
- **Executes tasks in parallel** using `parallelStream()`.
- **Threads process different numbers concurrently**.

---

## **Types of ForkJoinPool Tasks**
| Task Type | Description | Example |
|-----------|------------|---------|
| **RecursiveTask<V>** | Returns a result (e.g., sum, search) | Finding sum of numbers |
| **RecursiveAction** | No return value (e.g., sorting, printing) | Printing a list in parallel |

---

## **Example 1: Finding Sum Using `RecursiveTask`**
#### **Scenario**: Compute sum of numbers **from 1 to 1 million** using `ForkJoinPool`.

```java
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;

class SumTask extends RecursiveTask<Long> {
    private final int start, end;
    private static final int THRESHOLD = 10000;

    public SumTask(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        if ((end - start) <= THRESHOLD) { // Base case: Direct computation
            long sum = 0;
            for (int i = start; i <= end; i++) {
                sum += i;
            }
            return sum;
        } else { // Recursive case: Split task
            int mid = (start + end) / 2;
            SumTask leftTask = new SumTask(start, mid);
            SumTask rightTask = new SumTask(mid + 1, end);

            leftTask.fork(); // Execute leftTask asynchronously
            long rightResult = rightTask.compute(); // Compute rightTask synchronously
            long leftResult = leftTask.join(); // Wait for leftTask result

            return leftResult + rightResult; // Combine results
        }
    }
}

public class ForkJoinSumExample {
    public static void main(String[] args) {
        ForkJoinPool pool = new ForkJoinPool();

        SumTask task = new SumTask(1, 1_000_000);
        long result = pool.invoke(task);

        System.out.println("Sum: " + result);
        pool.shutdown();
    }
}
```
### **Explanation**
1. **Divides the range [1, 1,000,000] into smaller tasks** (splitting at `THRESHOLD = 10,000`).
2. **Uses `fork()` to execute left subtask asynchronously**.
3. **Right subtask is computed in the current thread (`compute()`)**.
4. **Merges results from left and right subtasks (`join()`)**.

### **Expected Output**
```
Sum: 500000500000
```
🔹 **`ForkJoinPool` is 2-3x faster than normal loops for large computations!**

---

## **Example 2: Parallel Printing Using `RecursiveAction`**
#### **Scenario**: Print numbers **in parallel** using `ForkJoinPool`.

```java
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.ForkJoinPool;

class PrintTask extends RecursiveAction {
    private final int start, end;
    private static final int THRESHOLD = 5;

    public PrintTask(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected void compute() {
        if ((end - start) <= THRESHOLD) {
            for (int i = start; i <= end; i++) {
                System.out.println(Thread.currentThread().getName() + " - " + i);
            }
        } else {
            int mid = (start + end) / 2;
            PrintTask leftTask = new PrintTask(start, mid);
            PrintTask rightTask = new PrintTask(mid + 1, end);

            invokeAll(leftTask, rightTask);
        }
    }
}

public class ForkJoinPrintExample {
    public static void main(String[] args) {
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(new PrintTask(1, 20));
        pool.shutdown();
    }
}
```
### **Explanation**
1. **Splits the range `[1, 20]` into smaller parts of `THRESHOLD = 5`**.
2. **Processes each subtask in parallel** using `invokeAll()`.
3. **No return value (printing operation)**.

### **Expected Output**
```
ForkJoinPool-1-worker-3 - 1
ForkJoinPool-1-worker-1 - 2
ForkJoinPool-1-worker-2 - 3
ForkJoinPool-1-worker-3 - 4
ForkJoinPool-1-worker-1 - 5
...
```
🔹 **Parallel execution using multiple threads**.

---

## **Comparison: `ForkJoinPool` vs. Parallel Stream**
| Feature | `ForkJoinPool` | `Parallel Stream` |
|---------|---------------|------------------|
| **Thread Control** | **Customizable** thread pool size | Uses **common ForkJoinPool** |
| **Best For** | Recursive tasks (sorting, searching) | Simple data processing (filter, map) |
| **Efficiency** | **Better control over workload distribution** | **Easier syntax, less control** |
| **Data Type** | Works well with **recursive algorithms** | Works best with **collections** |

---

## **Best Practices for Using `ForkJoinPool`**
✅ **Use ForkJoinPool When**:
- **Recursive computations** (merging sorted arrays, tree traversal).
- **Custom thread pool management** is needed.
- **CPU-intensive parallel tasks**.

❌ **Avoid ForkJoinPool When**:
- **Simple collection operations** (use **Parallel Stream** instead).
- **Tasks involve I/O operations** (better to use `ExecutorService`).
- **Order-sensitive computations** (parallelism may affect ordering).

---

## **Key Takeaways**
- **`ForkJoinPool` optimizes parallel task execution using the work-stealing algorithm**.
- **Uses `RecursiveTask` (returns a value) and `RecursiveAction` (no return value)**.
- **Splits large tasks into smaller parallel tasks**.
- **Better control over threading compared to Parallel Streams**.