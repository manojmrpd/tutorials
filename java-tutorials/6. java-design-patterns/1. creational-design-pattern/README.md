# Java Creational Design Patterns

## Table of Contents
1. [What is Singleton Design Pattern](#what-is-singleton-design-pattern)
    - [What is the Singleton Design Pattern](#what-is-the-singleton-design-pattern)
    - [When to use Singleton Design Pattern](#when-to-use-singleton-design-pattern)
    - [Initialization Types of Singleton](#initialization-types-of-singleton)
    - [Key Components of Singleton Design Pattern](#key-components-of-singleton-design-pattern)
    - [Implementation of Singleton Design Pattern](#implementation-of-singleton-design-pattern)
    - [Different Ways to Implement Singleton Design Pattern](#different-ways-to-implement-singleton-design-pattern)
    - [Use Case of Singleton Design Pattern](#use-case-of-singleton-method-design-pattern)
    - [Realtime Full Example of Singleton Design Pattern](#realtime-full-example-of-singleton-design-pattern)
    - [Pros and Cons of Singleton Design Pattern](#pros-and-cons-of-singleton-design-pattern)

2. [What is Prototype Design Pattern](#what-is-prototype-design-pattern)
    - [What is the Prototype Design Pattern](#what-is-the-prototype-design-pattern)
    - [When to use Prototype Design Pattern](#when-to-use-prototype-design-pattern)
    - [Key Components of Prototype Design Pattern](#key-components-of-prototype-design-pattern)
    - [Different Ways to Implement Prototype Design Pattern](#different-ways-to-implement-prototype-design-pattern)
    - [Prototype Design Pattern Full Example in Java](#prototype-design-pattern-full-example-in-java)
    - [Use Case of Prototype Design Pattern](#use-case-of-prototype-design-pattern)
    - [Realtime Full Example of Prototype Design Pattern](#realtime-full-example-of-prototype-design-pattern)
    - [Pros and Cons of Prototype Design Pattern](#pros-and-cons-of-prototype-design-pattern)

2. [What is Factory Design Pattern](#what-is-factory-design-pattern)
    - [What is the Factory Design Pattern](#what-is-the-factory-design-pattern)
    - [When to use Factory Design Pattern](#when-to-use-factory-design-pattern)
    - [Key Components of Factory Design Pattern](#key-components-of-factory-design-pattern)
    - [Different Ways to Implement Factory Design Pattern](#different-ways-to-implement-factory-design-pattern)
    - [Factory Design Pattern Full Example in Java](#factory-design-pattern-full-example-in-java)
    - [Use Case of Factory Design Pattern](#use-case-of-factory-design-pattern)
    - [Realtime Full Example of Factory Design Pattern](#realtime-full-example-of-factory-design-pattern)
    - [Pros and Cons of Factory Design Pattern](#pros-and-cons-of-factory-design-pattern)

2. [What is Abstract Factory Design Pattern](#what-is-abstract-factory-design-pattern)
    - [What is the Abstract Factory Design Pattern](#what-is-the-abstract-factory-design-pattern)
    - [When to use Abstract Factory Design Pattern](#when-to-use-prototabstract-factoryype-design-pattern)
    - [Key Components of Abstract Factory Design Pattern](#key-components-of-abstract-factory-design-pattern)
    - [Different Ways to Implement Abstract Factory Design Pattern](#different-ways-to-implement-abstract-factory-design-pattern)
    - [Abstract Factory Design Pattern Full Example in Java](#abstract-factory-design-pattern-full-example-in-java)
    - [Use Case of Abstract Factory Design Pattern](#use-case-of-abstract-factory-design-pattern)
    - [Realtime Full Example of Abstract Factory Design Pattern](#realtime-full-example-of-abstract-factory-design-pattern)
    - [Pros and Cons of Abstract Factory Design Pattern](#pros-and-cons-of-abstract-factory-design-pattern)


2. [What is Builder Design Pattern](#what-is-builder-design-pattern)
    - [What is the Builder Design Pattern](#what-is-the-builder-design-pattern)
    - [When to use Builder Design Pattern](#when-to-use-builder-design-pattern)
    - [Key Components of Builder Design Pattern](#key-components-of-builder-design-pattern)
    - [Different Ways to Implement Builder Design Pattern](#different-ways-to-implement-protbuilderotype-design-pattern)
    - [Builder Design Pattern Full Example in Java](#builder-design-pattern-full-example-in-java)
    - [Use Case of Builder Design Pattern](#use-case-of-builder-design-pattern)
    - [Realtime Full Example of Builder Design Pattern](#realtime-full-example-of-builder-design-pattern)
    - [Pros and Cons of Builder Design Pattern](#pros-and-cons-of-builder-design-pattern)


# Creational Design Patterns
Creational design patterns abstract the instantiation process. They help make a system independent of how its objects are created, composed, and represented. A class creational pattern uses inheritance to vary the class that’s instantiated, whereas an object creational pattern will delegate instantiation to another object.


Creational patterns give a lot of flexibility in what gets created, who creates it, how it gets created, and, when.

There are two recurring themes in these patterns:

- They all encapsulate knowledge about which concrete class the system uses.
- They hide how instances of these classes are created and put together.

## Types of Creational Design Patterns
- Singleton Design Pattern
- Prototype Design Pattern
- Factory Design Pattern
- Abstract Factory Design Pattern
- Builder Design Patterns

# What is Singleton Design Pattern
### What is the Singleton Design Pattern

The Singleton Design Pattern is a creational design pattern used to ensure that a class has only one instance and provides a global point of access to that instance. This is particularly useful when exactly one object is needed to coordinate actions across the system.

### When to Use Singleton Design Pattern

The Singleton Design Pattern should be used in scenarios where:

1. **Single Instance is Required:** When you need to control the access to a shared resource like a database connection or a file.
2. **Global Point of Access:** When you need to provide a single point of access to a resource that is used throughout the application.
3. **State Management:** When you need to manage state across various parts of an application, ensuring consistency.
4. **Configuration Management:** When the configuration settings should be centralized and accessed globally.

### Initialization Types of Singleton

1. **Eager Initialization:**
   - The instance is created at the time of class loading.
   - Pros: Simple to implement, thread-safe.
   - Cons: Instance is created even if it might not be used.

   ```java
   public class Singleton {
       private static final Singleton instance = new Singleton();
       private Singleton() {}
       public static Singleton getInstance() {
           return instance;
       }
   }
   ```

2. **Lazy Initialization:**
   - The instance is created only when it is needed for the first time.
   - Pros: Saves resources by creating the instance only when needed.
   - Cons: Not thread-safe in its basic form.

   ```java
   public class Singleton {
       private static Singleton instance;
       private Singleton() {}
       public static Singleton getInstance() {
           if (instance == null) {
               instance = new Singleton();
           }
           return instance;
       }
   }
   ```

3. **Thread-safe Lazy Initialization:**
   - Ensures that the instance is created in a thread-safe manner.
   - Pros: Thread-safe, lazy initialization.
   - Cons: May reduce performance due to synchronization overhead.

   ```java
   public class Singleton {
       private static Singleton instance;
       private Singleton() {}
       public static synchronized Singleton getInstance() {
           if (instance == null) {
               instance = new Singleton();
           }
           return instance;
       }
   }
   ```

4. **Bill Pugh Singleton Design:**
   - Uses an inner static helper class to create the instance.
   - Pros: Thread-safe without synchronization, lazy initialization.
   - Cons: More complex implementation.

   ```java
   public class Singleton {
       private Singleton() {}
       private static class SingletonHelper {
           private static final Singleton INSTANCE = new Singleton();
       }
       public static Singleton getInstance() {
           return SingletonHelper.INSTANCE;
       }
   }
   ```

### Key Components of Singleton Design Pattern

1. **Private Constructor:**
   - Prevents instantiation of the class from outside.

2. **Private Static Instance:**
   - Holds the single instance of the class.

3. **Public Static Method:**
   - Provides global access to the instance and returns it.

### Implementation of Singleton Design Pattern

Here’s a detailed implementation in Java:

```java
public class Singleton {
    // Private static instance of the class
    private static Singleton instance;

    // Private constructor to prevent instantiation
    private Singleton() {}

    // Public static method to provide access to the instance
    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
```

### Different Ways to Implement Singleton Design Pattern

1. **Eager Initialization:**

   ```java
   public class Singleton {
       private static final Singleton instance = new Singleton();
       private Singleton() {}
       public static Singleton getInstance() {
           return instance;
       }
   }
   ```

2. **Lazy Initialization:**

   ```java
   public class Singleton {
       private static Singleton instance;
       private Singleton() {}
       public static Singleton getInstance() {
           if (instance == null) {
               instance = new Singleton();
           }
           return instance;
       }
   }
   ```

3. **Thread-safe Lazy Initialization:**

   ```java
   public class Singleton {
       private static Singleton instance;
       private Singleton() {}
       public static synchronized Singleton getInstance() {
           if (instance == null) {
               instance = new Singleton();
           }
           return instance;
       }
   }
   ```

4. **Bill Pugh Singleton Design:**

   ```java
   public class Singleton {
       private Singleton() {}
       private static class SingletonHelper {
           private static final Singleton INSTANCE = new Singleton();
       }
       public static Singleton getInstance() {
           return SingletonHelper.INSTANCE;
       }
   }
   ```

### Use Case of Singleton Design Pattern

- **Logging:** Ensures a single instance of a logger class that logs messages for the entire application.
- **Configuration Settings:** Manages application-wide configuration settings in a single instance.
- **Database Connections:** Manages a single instance of a database connection to ensure efficient resource utilization.
- **Cache:** Maintains a single instance of a cache to store frequently accessed data.

### Realtime Full Example of Singleton Design Pattern

Consider a real-time example where a Singleton is used to manage a connection to a database. This ensures that only one instance of the database connection exists throughout the application lifecycle, which can help manage resources efficiently.

**Step 1: Create a Singleton class**

```java
public class DatabaseConnection {
    // Private static instance of the same class that is the only instance of the class.
    private static DatabaseConnection instance;
    
    // Private constructor to prevent instantiation from other classes.
    private DatabaseConnection() {
        // Initialize the database connection here.
        System.out.println("Database Connection Initialized");
    }

    // Public static method that provides access to the instance.
    public static DatabaseConnection getInstance() {
        if (instance == null) {
            synchronized (DatabaseConnection.class) {
                if (instance == null) {
                    instance = new DatabaseConnection();
                }
            }
        }
        return instance;
    }

    // Method to perform a database query.
    public void query(String sql) {
        // Code to execute a database query.
        System.out.println("Executing query: " + sql);
    }
}
```

**Step 2: Use the Singleton in your application**

```java
public class SingletonPatternDemo {
    public static void main(String[] args) {
        // Attempt to get an instance of DatabaseConnection
        DatabaseConnection connection1 = DatabaseConnection.getInstance();
        
        // Perform a database query
        connection1.query("SELECT * FROM users");

        // Attempt to get another instance of DatabaseConnection
        DatabaseConnection connection2 = DatabaseConnection.getInstance();
        
        // Perform another database query
        connection2.query("SELECT * FROM orders");

        // Verify that both connection1 and connection2 are the same instance
        System.out.println(connection1 == connection2);  // Should print 'true'
    }
}
```

### Explanation

1. **Singleton Class (`DatabaseConnection`)**
    - **Instance Variable:** A private static variable `instance` holds the single instance of the class.
    - **Private Constructor:** Prevents direct instantiation from outside the class.
    - **Static `getInstance` Method:** Provides global access to the instance, with double-checked locking to ensure thread safety.
    - **Query Method:** Simulates performing a database query.

2. **Client Code (`SingletonPatternDemo`)**
    - **Get Instance:** Calls `getInstance` to retrieve the singleton instance.
    - **Perform Queries:** Uses the singleton instance to perform database queries.
    - **Verify Singleton:** Checks if the retrieved instances are the same to confirm the singleton behavior.


### Pros and Cons of Singleton Design Pattern

**Pros:**

1. **Controlled Access to the Instance:**
   - Ensures only one instance is created, providing a controlled access point.

2. **Reduced Namespace Pollution:**
   - Avoids global variables and provides a single access point to a shared resource.

3. **Permits Refinement of Operations and Representation:**
   - Allows changing the method of creating the instance without affecting the rest of the application.

**Cons:**

1. **Global State:**
   - Makes the state globally accessible, which can lead to issues in multi-threaded environments.

2. **Hidden Dependencies:**
   - Can make it difficult to track dependencies since the singleton can be accessed from anywhere in the application.

3. **Testing Difficulties:**
   - Singleton instances can make unit testing challenging because they maintain state across tests, leading to test interdependencies.

# What is Prototype Design Pattern

### What is the Prototype Design Pattern?

The Prototype Design Pattern is a creational design pattern used to create new objects by copying an existing object, known as a prototype. This pattern is particularly useful when the cost of creating a new object is high and when instances of a class have only a few different states. Instead of creating a new instance through a constructor, the client calls the clone method on an existing instance.

### When to Use Prototype Design Pattern

The Prototype Design Pattern is useful in the following scenarios:

1. **Object Creation is Costly:** When creating a new object directly is resource-intensive or time-consuming. For example, if an object involves a complex setup or heavy computational processes during initialization, cloning an existing object can be more efficient.

2. **Object Initialization is Complex:** When the object's state is complex and initializing it requires significant effort. By cloning a pre-initialized object, you can bypass the complex initialization process.

3. **Avoiding Subclassing:** When the classes to instantiate are specified at runtime and you want to avoid creating a hierarchy of factories. The Prototype pattern allows for adding new prototypes without altering the code that uses them, reducing the need for a subclass explosion.

4. **Improving Performance:** When cloning is faster than creating a new instance using a constructor. This is especially true in systems where the overhead of creating and initializing new objects is substantial.

### Key Components of Prototype Design Pattern

1. **Prototype Interface:**
   - Defines a method for cloning itself.
   - Ensures that all concrete prototypes implement a clone method.

2. **Concrete Prototype:**
   - Implements the cloning method defined in the Prototype interface.
   - This class is the one that clients will clone to create new instances.

3. **Client:**
   - Creates a new object by asking a prototype to clone itself.
   - Uses the clone method to create a copy of the prototype rather than creating a new instance through a constructor.

### Different Ways to Implement Prototype Design Pattern

1. **Shallow Copy:**
   - Copies the fields of an object as is.
   - Only the references to objects are copied, not the objects themselves.
   - Suitable for simple objects that do not contain references to other objects.

2. **Deep Copy:**
   - Copies the fields of an object and also recursively copies all objects referenced by the fields.
   - Ensures a complete duplication of the object and its related objects.
   - More complex and slower compared to shallow copy, but necessary for objects that contain references to other mutable objects.

### Prototype Design Pattern Full Example in Java

Here is a detailed implementation demonstrating both shallow and deep copy:

**Prototype Interface:**

```java
public interface Prototype extends Cloneable {
    Prototype clone();
}
```

**Concrete Prototype (Shallow Copy):**

```java
import java.util.List;

public class ShallowPrototype implements Prototype {
    private String field1;
    private List<String> field2;

    public ShallowPrototype(String field1, List<String> field2) {
        this.field1 = field1;
        this.field2 = field2;
    }

    @Override
    public Prototype clone() {
        try {
            return (ShallowPrototype) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Getters and setters
    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public List<String> getField2() {
        return field2;
    }

    public void setField2(List<String> field2) {
        this.field2 = field2;
    }
}
```

**Concrete Prototype (Deep Copy):**

```java
import java.util.ArrayList;
import java.util.List;

public class DeepPrototype implements Prototype {
    private String field1;
    private List<String> field2;

    public DeepPrototype(String field1, List<String> field2) {
        this.field1 = field1;
        this.field2 = new ArrayList<>(field2);
    }

    @Override
    public Prototype clone() {
        List<String> clonedField2 = new ArrayList<>(field2);
        return new DeepPrototype(field1, clonedField2);
    }

    // Getters and setters
    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public List<String> getField2() {
        return field2;
    }

    public void setField2(List<String> field2) {
        this.field2 = field2;
    }
}
```

**Client:**

```java
import java.util.ArrayList;
import java.util.List;

public class PrototypeDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Item 1");
        list.add("Item 2");

        ShallowPrototype shallowPrototype = new ShallowPrototype("Shallow", list);
        ShallowPrototype shallowClone = (ShallowPrototype) shallowPrototype.clone();
        
        DeepPrototype deepPrototype = new DeepPrototype("Deep", list);
        DeepPrototype deepClone = (DeepPrototype) deepPrototype.clone();
        
        // Demonstrating shallow and deep copies
        list.add("Item 3");

        System.out.println("Original shallow prototype list: " + shallowPrototype.getField2());
        System.out.println("Cloned shallow prototype list: " + shallowClone.getField2());

        System.out.println("Original deep prototype list: " + deepPrototype.getField2());
        System.out.println("Cloned deep prototype list: " + deepClone.getField2());
    }
}
```

### Use Case of Prototype Design Pattern

- **Prototyping Complex Objects:** When creating complex objects whose initialization may involve multiple steps or complex logic. For example, if you have a system that needs to create various configurations of an object, the Prototype pattern can simplify this process.
- **Performance Optimization:** When creating a new object is resource-intensive, cloning a prototype can be a more efficient alternative. This is particularly useful in game development, where creating new objects on the fly can be performance-critical.
- **Dynamic Object Creation:** When the system needs to create new instances of a class dynamically at runtime. For example, a document editor might use the Prototype pattern to create different types of document elements (text, images, tables) based on a user's actions.

### Pros and Cons of Prototype Design Pattern

**Pros:**

1. **Reduces Subclassing:**
   - The Prototype pattern can reduce the need for subclassing by using clones of existing objects. This can simplify the class hierarchy and reduce the number of subclasses needed.

2. **Improves Performance:**
   - Cloning can be more efficient than creating a new object from scratch, especially when initialization is costly. This can lead to performance improvements in systems where object creation is a bottleneck.

3. **Dynamic System:**
   - The pattern supports the creation of new instances dynamically at runtime. This can be useful in systems where the types and configurations of objects are not known until runtime.

**Cons:**

1. **Cloning Complexity:**
   - Implementing a deep clone can be complex, especially for objects with multiple references to other objects. Ensuring that all objects are properly cloned can be challenging and error-prone.

2. **Copy Issues:**
   - Shallow copies can introduce problems if the cloned object contains references to mutable objects. Changes to the referenced objects in one clone can affect all clones, leading to unintended side effects.

3. **Maintenance:**
   - Managing clones and ensuring proper cloning can increase maintenance complexity. This is particularly true in systems with complex object relationships and dependencies.

### Realtime Full Example of Prototype Design Pattern
Sure! Let's consider a real-time example of the Prototype Design Pattern using a scenario in a game development context where we have different types of characters (e.g., heroes, monsters) that need to be created multiple times with similar attributes.

### Scenario

In a game, we have different types of characters like heroes and monsters. Each character has various attributes like health, attack power, and special abilities. Instead of creating each character from scratch, we can use the Prototype Design Pattern to clone existing character templates and then customize them as needed.

### Implementation in Java

**1. Prototype Interface:**

```java
public interface Character extends Cloneable {
    Character clone();
    void displayInfo();
}
```

**2. Concrete Prototype - Hero:**

```java
public class Hero implements Character {
    private String name;
    private int health;
    private int attackPower;
    private String specialAbility;

    public Hero(String name, int health, int attackPower, String specialAbility) {
        this.name = name;
        this.health = health;
        this.attackPower = attackPower;
        this.specialAbility = specialAbility;
    }

    @Override
    public Character clone() {
        try {
            return (Hero) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void displayInfo() {
        System.out.println("Hero [name=" + name + ", health=" + health + ", attackPower=" + attackPower + ", specialAbility=" + specialAbility + "]");
    }
    
    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public String getSpecialAbility() {
        return specialAbility;
    }

    public void setSpecialAbility(String specialAbility) {
        this.specialAbility = specialAbility;
    }
}
```

**3. Concrete Prototype - Monster:**

```java
public class Monster implements Character {
    private String type;
    private int health;
    private int attackPower;

    public Monster(String type, int health, int attackPower) {
        this.type = type;
        this.health = health;
        this.attackPower = attackPower;
    }

    @Override
    public Character clone() {
        try {
            return (Monster) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void displayInfo() {
        System.out.println("Monster [type=" + type + ", health=" + health + ", attackPower=" + attackPower + "]");
    }

    // Getters and setters
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }
}
```

**4. Client:**

```java
public class Game {
    public static void main(String[] args) {
        // Creating initial prototypes
        Hero heroPrototype = new Hero("Archer", 100, 20, "Arrow Rain");
        Monster monsterPrototype = new Monster("Goblin", 50, 10);
        
        // Cloning heroes
        Hero hero1 = (Hero) heroPrototype.clone();
        hero1.setName("Archer - Clone 1");
        
        Hero hero2 = (Hero) heroPrototype.clone();
        hero2.setName("Archer - Clone 2");
        
        // Cloning monsters
        Monster monster1 = (Monster) monsterPrototype.clone();
        monster1.setType("Goblin - Clone 1");
        
        Monster monster2 = (Monster) monsterPrototype.clone();
        monster2.setType("Goblin - Clone 2");
        
        // Displaying cloned characters
        hero1.displayInfo();
        hero2.displayInfo();
        monster1.displayInfo();
        monster2.displayInfo();
    }
}
```   

In conclusion, the Prototype Design Pattern is a powerful tool for creating new objects by cloning existing ones. It can improve performance, simplify object creation, and reduce subclassing. However, it requires careful implementation to avoid issues related to shallow and deep copying. Proper understanding and application of this pattern can lead to more efficient and maintainable software systems.

# What is Factory Design Pattern
### What is the Factory Design Pattern?

The Factory Design Pattern is a creational design pattern that provides an interface for creating objects in a super class, but allows subclasses to alter the type of objects that will be created. It defines a method for creating an object but allows subclasses to decide which class to instantiate. This pattern promotes loose coupling by eliminating the need to bind application-specific classes into the code.

### When to Use Factory Design Pattern

The Factory Design Pattern is useful when:

1. **Object Creation Requires Logic:** When the creation of an object involves complex logic or computations, centralizing this logic in a factory simplifies code maintenance.
2. **Object Management:** When you want to manage the lifecycle and creation of objects in a centralized way.
3. **Encapsulation of Object Creation:** When the specific class of objects must be hidden from the client to promote loose coupling.
4. **Family of Related Objects:** When you need to create families of related or dependent objects without specifying their concrete classes.

### Key Components of Factory Design Pattern

1. **Product:** The interface or abstract class defining the object type to be created.
2. **ConcreteProduct:** The specific implementations of the Product interface.
3. **Creator/Factory:** The abstract class or interface declaring the factory method that returns a Product object.
4. **ConcreteCreator/ConcreteFactory:** The classes that implement the factory method to create and return instances of ConcreteProduct.

### Different Ways to Implement Factory Design Pattern

1. **Simple Factory (Static Factory Method):** A class with a static method that returns instances of different classes based on input.
2. **Factory Method:** Uses inheritance: the creation of objects is delegated to subclasses that implement the factory method to create objects.
3. **Abstract Factory:** A factory of factories; a super-factory that creates other factories. Each factory is responsible for creating related objects.

### Factory Design Pattern Full Example in Java

**Product Interface:**

```java
public interface Shape {
    void draw();
}
```

**Concrete Products:**

```java
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing a Circle");
    }
}

public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing a Rectangle");
    }
}

public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing a Square");
    }
}
```

**Factory Interface:**

```java
public abstract class ShapeFactory {
    abstract Shape createShape(String shapeType);
}
```

**Concrete Factory:**

```java
public class ConcreteShapeFactory extends ShapeFactory {
    @Override
    Shape createShape(String shapeType) {
        if (shapeType == null) {
            return null;
        }
        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle();
        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle();
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            return new Square();
        }
        return null;
    }
}
```

**Client:**

```java
public class FactoryPatternDemo {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ConcreteShapeFactory();

        // Create a Circle
        Shape shape1 = shapeFactory.createShape("CIRCLE");
        shape1.draw();

        // Create a Rectangle
        Shape shape2 = shapeFactory.createShape("RECTANGLE");
        shape2.draw();

        // Create a Square
        Shape shape3 = shapeFactory.createShape("SQUARE");
        shape3.draw();
    }
}
```

### Use Case of Factory Design Pattern

- **UI Component Creation:** When developing a graphical user interface, different UI components (buttons, text fields, etc.) need to be created based on user actions or configurations. The factory pattern can simplify this process by centralizing the creation logic.
- **Document Handling:** In applications that support multiple document formats (PDF, DOCX, TXT), factories can be used to create appropriate document objects based on file extensions or user preferences.

### Real-time Full Example of Factory Design Pattern

Consider an application that needs to handle different types of notification services (e.g., Email, SMS, Push Notification).

**Notification Interface:**

```java
public interface Notification {
    void notifyUser();
}
```

**Concrete Notifications:**

```java
public class EmailNotification implements Notification {
    @Override
    public void notifyUser() {
        System.out.println("Sending an email notification");
    }
}

public class SMSNotification implements Notification {
    @Override
    public void notifyUser() {
        System.out.println("Sending an SMS notification");
    }
}

public class PushNotification implements Notification {
    @Override
    public void notifyUser() {
        System.out.println("Sending a push notification");
    }
}
```

**Factory Interface:**

```java
public abstract class NotificationFactory {
    abstract Notification createNotification(String notificationType);
}
```

**Concrete Factory:**

```java
public class ConcreteNotificationFactory extends NotificationFactory {
    @Override
    Notification createNotification(String notificationType) {
        if (notificationType == null) {
            return null;
        }
        if (notificationType.equalsIgnoreCase("EMAIL")) {
            return new EmailNotification();
        } else if (notificationType.equalsIgnoreCase("SMS")) {
            return new SMSNotification();
        } else if (notificationType.equalsIgnoreCase("PUSH")) {
            return new PushNotification();
        }
        return null;
    }
}
```

**Client:**

```java
public class NotificationService {
    public static void main(String[] args) {
        NotificationFactory notificationFactory = new ConcreteNotificationFactory();

        // Create an Email Notification
        Notification notification1 = notificationFactory.createNotification("EMAIL");
        notification1.notifyUser();

        // Create an SMS Notification
        Notification notification2 = notificationFactory.createNotification("SMS");
        notification2.notifyUser();

        // Create a Push Notification
        Notification notification3 = notificationFactory.createNotification("PUSH");
        notification3.notifyUser();
    }
}
```

### Pros and Cons of Factory Design Pattern

**Pros:**

1. **Encapsulation:** Encapsulates the object creation logic, making the code more maintainable and easier to manage.
2. **Loose Coupling:** Promotes loose coupling by reducing dependencies between client code and the concrete classes.
3. **Scalability:** Adding new types of objects is easy; you only need to create a new class and modify the factory method if necessary.

**Cons:**

1. **Complexity:** Adds an extra layer of complexity with additional classes and interfaces.
2. **Single Responsibility Principle Violation:** The factory class may grow complex and violate the Single Responsibility Principle if it handles the creation of many different types of objects.
3. **Maintenance Overhead:** Requires maintaining additional classes and ensuring the factory method is kept up to date with all possible product types.

The Factory Design Pattern is a powerful tool for managing object creation, providing a flexible and scalable solution for complex systems requiring various types of objects. Properly understanding and applying this pattern can lead to more maintainable and extensible codebases.

# What is Abstract Factory Design Pattern
### What is the Abstract Factory Design Pattern?

The Abstract Factory Design Pattern is a creational design pattern that provides an interface for creating families of related or dependent objects without specifying their concrete classes. It involves a super-factory, which creates other factories, also known as Factory of Factories.

### When to Use Abstract Factory Design Pattern

The Abstract Factory Design Pattern is useful when:

1. **Families of Related Objects:** You need to create families of related objects or products that work together.
2. **Independence of Product Variants:** You want to provide a way to manage and switch between different families of products without changing the code that uses these products.
3. **Enforcing Consistency:** Ensuring that related objects are created together and are consistent with each other.

### Key Components of Abstract Factory Design Pattern

1. **AbstractFactory:** Declares a set of methods for creating abstract products.
2. **ConcreteFactory:** Implements the creation methods for the specific product family.
3. **AbstractProduct:** Declares an interface for a type of product object.
4. **ConcreteProduct:** Implements the AbstractProduct interface.
5. **Client:** Uses the AbstractFactory and AbstractProduct interfaces to create and work with product objects.

### Different Ways to Implement Abstract Factory Design Pattern

1. **Interface-Based Implementation:** Define interfaces for factories and products, with concrete classes implementing these interfaces.
2. **Abstract Class-Based Implementation:** Use abstract classes for factories and products, with concrete classes extending these abstract classes.

### Abstract Factory Design Pattern Full Example in Java

**Abstract Product Interfaces:**

```java
public interface Button {
    void paint();
}

public interface Checkbox {
    void paint();
}
```

**Concrete Product Implementations:**

```java
public class WindowsButton implements Button {
    @Override
    public void paint() {
        System.out.println("Rendering a button in a Windows style");
    }
}

public class MacButton implements Button {
    @Override
    public void paint() {
        System.out.println("Rendering a button in a Mac style");
    }
}

public class WindowsCheckbox implements Checkbox {
    @Override
    public void paint() {
        System.out.println("Rendering a checkbox in a Windows style");
    }
}

public class MacCheckbox implements Checkbox {
    @Override
    public void paint() {
        System.out.println("Rendering a checkbox in a Mac style");
    }
}
```

**Abstract Factory Interface:**

```java
public interface GUIFactory {
    Button createButton();
    Checkbox createCheckbox();
}
```

**Concrete Factory Implementations:**

```java
public class WindowsFactory implements GUIFactory {
    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new WindowsCheckbox();
    }
}

public class MacFactory implements GUIFactory {
    @Override
    public Button createButton() {
        return new MacButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new MacCheckbox();
    }
}
```

**Client Code:**

```java
public class Application {
    private Button button;
    private Checkbox checkbox;

    public Application(GUIFactory factory) {
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }

    public void paint() {
        button.paint();
        checkbox.paint();
    }
}

public class ApplicationConfigurator {
    public static void main(String[] args) {
        Application app;
        GUIFactory factory;
        
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("mac")) {
            factory = new MacFactory();
        } else {
            factory = new WindowsFactory();
        }
        
        app = new Application(factory);
        app.paint();
    }
}
```

### Use Case of Abstract Factory Design Pattern

- **Cross-Platform UI:** Creating UI components for different operating systems (Windows, Mac, Linux) while ensuring consistency within the UI of each platform.
- **Product Families:** Managing and creating product families in e-commerce systems where different products share common traits but vary across categories.

### Real-time Full Example of Abstract Factory Design Pattern

Consider a real-time example of a vehicle manufacturing system where different factories produce different types of vehicles (cars and trucks) with various components (engines and tires).

**Abstract Product Interfaces:**

```java
public interface Engine {
    void design();
}

public interface Tire {
    void manufacture();
}
```

**Concrete Product Implementations:**

```java
public class CarEngine implements Engine {
    @Override
    public void design() {
        System.out.println("Designing car engine");
    }
}

public class TruckEngine implements Engine {
    @Override
    public void design() {
        System.out.println("Designing truck engine");
    }
}

public class CarTire implements Tire {
    @Override
    public void manufacture() {
        System.out.println("Manufacturing car tire");
    }
}

public class TruckTire implements Tire {
    @Override
    public void manufacture() {
        System.out.println("Manufacturing truck tire");
    }
}
```

**Abstract Factory Interface:**

```java
public interface VehicleFactory {
    Engine createEngine();
    Tire createTire();
}
```

**Concrete Factory Implementations:**

```java
public class CarFactory implements VehicleFactory {
    @Override
    public Engine createEngine() {
        return new CarEngine();
    }

    @Override
    public Tire createTire() {
        return new CarTire();
    }
}

public class TruckFactory implements VehicleFactory {
    @Override
    public Engine createEngine() {
        return new TruckEngine();
    }

    @Override
    public Tire createTire() {
        return new TruckTire();
    }
}
```

**Client Code:**

```java
public class VehicleApplication {
    private Engine engine;
    private Tire tire;

    public VehicleApplication(VehicleFactory factory) {
        engine = factory.createEngine();
        tire = factory.createTire();
    }

    public void produce() {
        engine.design();
        tire.manufacture();
    }

    public static void main(String[] args) {
        VehicleFactory carFactory = new CarFactory();
        VehicleApplication carApp = new VehicleApplication(carFactory);
        carApp.produce();

        VehicleFactory truckFactory = new TruckFactory();
        VehicleApplication truckApp = new VehicleApplication(truckFactory);
        truckApp.produce();
    }
}
```

### Pros and Cons of Abstract Factory Design Pattern

**Pros:**

1. **Enforces Consistency:** Ensures that related objects are created together and are consistent with each other.
2. **Promotes Loose Coupling:** Isolates the client code from the concrete classes.
3. **Scalability:** Adding new families of products is easy; you just need to add a new factory class and the corresponding products.

**Cons:**

1. **Complexity:** Adds an extra layer of complexity with additional classes and interfaces.
2. **Extensibility Issues:** Adding new products to the abstract factory is difficult because it requires changes to the abstract factory interface and all its concrete implementations.
3. **Class Explosion:** Can lead to a proliferation of classes, especially when the number of products increases.

The Abstract Factory Design Pattern is highly beneficial in scenarios where systems need to create families of related objects consistently, such as cross-platform UI components or manufacturing systems. Proper understanding and application of this pattern can lead to more maintainable, scalable, and flexible software systems.

# What is Builder Design Pattern
### What is the Builder Design Pattern?

The Builder Design Pattern is a creational design pattern that provides a way to construct complex objects step by step. Unlike other creational patterns, which focus on the creation process, the Builder Pattern focuses on how a product is assembled. It allows the creation of different representations of an object using the same construction process.

### When to Use Builder Design Pattern

The Builder Design Pattern is useful when:

1. **Complex Object Construction:** An object is complex to construct and requires multiple steps or configuration options.
2. **Immutable Objects:** When you need to create immutable objects with many properties.
3. **Readable Code:** To improve code readability by clearly separating the construction and representation of an object.
4. **Single Responsibility Principle:** To adhere to the Single Responsibility Principle by having a separate class to build the object.

### Key Components of Builder Design Pattern

1. **Product:** The complex object that is being constructed.
2. **Builder:** The interface or abstract class that defines the building steps.
3. **ConcreteBuilder:** The class that implements the Builder interface and provides specific implementations of the building steps.
4. **Director:** The class that constructs the object using the Builder interface. It defines the order in which to call the building steps.

### Different Ways to Implement Builder Design Pattern

1. **Traditional Builder Pattern:** Uses a separate Director class to manage the construction process.
2. **Builder Pattern with Method Chaining:** Simplifies the creation process by using method chaining and returning the builder object from each method.
3. **Inner Static Builder Class:** Uses a static inner builder class within the Product class for more concise and readable code.

### Builder Design Pattern Full Example in Java

**Product Class:**

```java
public class Computer {
    // required parameters
    private String HDD;
    private String RAM;

    // optional parameters
    private boolean isGraphicsCardEnabled;
    private boolean isBluetoothEnabled;

    private Computer(ComputerBuilder builder) {
        this.HDD = builder.HDD;
        this.RAM = builder.RAM;
        this.isGraphicsCardEnabled = builder.isGraphicsCardEnabled;
        this.isBluetoothEnabled = builder.isBluetoothEnabled;
    }

    // Getters and toString method

    public String getHDD() {
        return HDD;
    }

    public String getRAM() {
        return RAM;
    }

    public boolean isGraphicsCardEnabled() {
        return isGraphicsCardEnabled;
    }

    public boolean isBluetoothEnabled() {
        return isBluetoothEnabled;
    }

    @Override
    public String toString() {
        return "Computer [HDD=" + HDD + ", RAM=" + RAM + ", isGraphicsCardEnabled=" + isGraphicsCardEnabled
                + ", isBluetoothEnabled=" + isBluetoothEnabled + "]";
    }

    // Builder Class
    public static class ComputerBuilder {
        // required parameters
        private String HDD;
        private String RAM;

        // optional parameters
        private boolean isGraphicsCardEnabled;
        private boolean isBluetoothEnabled;

        public ComputerBuilder(String HDD, String RAM) {
            this.HDD = HDD;
            this.RAM = RAM;
        }

        public ComputerBuilder setGraphicsCardEnabled(boolean isGraphicsCardEnabled) {
            this.isGraphicsCardEnabled = isGraphicsCardEnabled;
            return this;
        }

        public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
            this.isBluetoothEnabled = isBluetoothEnabled;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }
}
```

**Client Code:**

```java
public class BuilderPatternTest {
    public static void main(String[] args) {
        Computer computer = new Computer.ComputerBuilder("500 GB", "8 GB")
                                .setGraphicsCardEnabled(true)
                                .setBluetoothEnabled(true)
                                .build();

        System.out.println(computer);
    }
}
```

### Use Case of Builder Design Pattern

- **Configurable Objects:** Used for creating objects that can be configured in multiple ways, such as complex UI components, or vehicle configuration systems where various optional features can be added.
- **Immutable Objects:** Helpful in creating immutable objects with many parameters without writing a complex constructor with multiple parameters.

### Realtime Full Example of Builder Design Pattern

Consider a real-time example of creating a meal in a fast-food restaurant where a meal can have various items like burgers, drinks, and sides.

**Product Class:**

```java
public class Meal {
    private String burger;
    private String drink;
    private String side;

    private Meal(MealBuilder builder) {
        this.burger = builder.burger;
        this.drink = builder.drink;
        this.side = builder.side;
    }

    @Override
    public String toString() {
        return "Meal [burger=" + burger + ", drink=" + drink + ", side=" + side + "]";
    }

    public static class MealBuilder {
        private String burger;
        private String drink;
        private String side;

        public MealBuilder setBurger(String burger) {
            this.burger = burger;
            return this;
        }

        public MealBuilder setDrink(String drink) {
            this.drink = drink;
            return this;
        }

        public MealBuilder setSide(String side) {
            this.side = side;
            return this;
        }

        public Meal build() {
            return new Meal(this);
        }
    }
}
```

**Client Code:**

```java
public class MealBuilderTest {
    public static void main(String[] args) {
        Meal meal = new Meal.MealBuilder()
                        .setBurger("Cheeseburger")
                        .setDrink("Coke")
                        .setSide("Fries")
                        .build();

        System.out.println(meal);

        Meal vegetarianMeal = new Meal.MealBuilder()
                               .setBurger("Veggie Burger")
                               .setDrink("Lemonade")
                               .setSide("Salad")
                               .build();

        System.out.println(vegetarianMeal);
    }
}
```

### Pros and Cons of Builder Design Pattern

**Pros:**

1. **Readable Code:** Enhances code readability by clearly separating the construction and representation of an object.
2. **Immutability:** Facilitates the creation of immutable objects.
3. **Fluent Interface:** The method chaining approach (fluent interface) makes the client code much easier to write and understand.
4. **Single Responsibility Principle:** Helps adhere to the Single Responsibility Principle by separating the construction logic into a separate class.

**Cons:**

1. **Complexity:** Introduces additional classes and complexity in the code.
2. **Boilerplate Code:** Requires writing boilerplate code for builder classes, especially for objects with many fields.

The Builder Design Pattern is a powerful tool for constructing complex objects, providing flexibility, readability, and maintainability. It's particularly useful in scenarios where objects need to be created with numerous optional parameters or configurations. Properly understanding and applying this pattern can significantly improve the design and structure of your software systems.
