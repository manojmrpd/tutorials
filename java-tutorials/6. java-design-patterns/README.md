# Java Design Patterns
### 1. Creational Design Pattern
1. [What is Singleton Design Pattern](#what-is-singleton-design-pattern)
2. [What is Prototype Design Pattern](#what-is-prototype-design-pattern)
3. [What is Factory Design Pattern](#what-is-factory-design-pattern)
4. [What is Abstract Factory Design Pattern](#what-is-abstract-factory-design-pattern)
5. [What is Builder Design Pattern](#what-is-builder-design-pattern)

### 2. Structural Design Pattern
1. [What is Adapter Design Pattern](#what-is-adapter-design-pattern)
2. [What is Decorator Design Pattern](#what-is-decorator-design-pattern)
3. [What is Proxy Design Pattern](#what-is-proxy-design-pattern)
4. [What is Bridge Design Pattern](#what-is-bridge-design-pattern)
5. [What is Composite Design Pattern](#what-is-composite-design-pattern)
6. [What is Flyweight Design Pattern](#what-is-flyweight-design-pattern)

### 3. Behavioural Design Pattern
1. [What is Chain of Responsibility Design Pattern](#what-is-chain-of-Responsibility-design-pattern)
2. [What is Command Design Pattern](#what-is-command-design-pattern)
3. [What is Interpreter Design Pattern](#what-is-interpreter-design-pattern)
4. [What is Mediator Design Pattern](#what-is-mediator-design-pattern)
5. [What is Memento Design Pattern](#what-is-memento-design-pattern)
6. [What is Observer Design Pattern](#what-is-observer-design-pattern)
7. [What is State Design Pattern](#what-is-state-design-pattern)
8. [What is Strategy Design Pattern](#what-is-strategy-design-pattern)
9. [What is Template Design Pattern](#what-is-template-design-pattern)
10. [What is Visitor Design Pattern](#what-is-visitor-design-pattern)

### 4. SOLID Principles
1. [What is Java SOLID Principles?](#what-is-java-solid-principles)
2. [What is Single Responsibility Principle (SRP)?](#what-is-single-responsibility-principle-srp)
3. [What is Open-Closed Principle (OCP)?](#what-is-open-closed-principle-ocp)
4. [What is Liskov Substitution Principle (LSP)?](#what-is-liskov-substitution-principle-lsp)
5. [What is Interface Segregation Principle (ISP)?](#what-is-interface-segregation-principle-isp)
6. [What is Dependency Inversion Principle (DIP)?](#what-is-dependency-inversion-principle-dip)