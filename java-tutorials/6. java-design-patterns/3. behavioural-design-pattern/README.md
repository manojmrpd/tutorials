# Java Behavioural Design Pattern

## Table of Contents

1. [What is Chain of Responsibility Design Pattern](#what-is-chain-of-Responsibility-design-pattern)
    - [What is the Chain of Responsibility Design Pattern](#what-is-the-chain-of-Responsibility-design-pattern)
    - [When to use Chain of Responsibility Design Pattern](#when-to-use-chain-of-Responsibility-design-pattern)
    - [Key Components of Chain of Responsibility Design Pattern](#key-components-of-chain-of-Responsibility-design-pattern)
    - [Different Ways to Implement Chain of Responsibility Design Pattern](#different-ways-to-implement-chain-of-Responsibility-design-pattern)
    - [Chain of Responsibility Design Pattern Full Example in Java](#chain-of-Responsibility-design-pattern-full-example-in-java)
    - [Use Case of Chain of Responsibility Design Pattern](#use-case-of-chain-of-Responsibility-design-pattern)
    - [Realtime Full Example of Chain of Responsibility Design Pattern](#realtime-full-example-of-chain-of-Responsibility-design-pattern)
    - [Pros and Cons of Chain of Responsibility Design Pattern](#pros-and-cons-of-chain-of-Responsibility-design-pattern)

2. [What is Command Design Pattern](#what-is-command-design-pattern)
    - [What is the Command Design Pattern](#what-is-the-command-design-pattern)
    - [When to use Command Design Pattern](#when-to-use-command-design-pattern)
    - [Key Components of Command Design Pattern](#key-components-of-command-design-pattern)
    - [Different Ways to Command Decorator Design Pattern](#different-ways-to-implement-command-design-pattern)
    - [Command Design Pattern Full Example in Java](#command-design-pattern-full-example-in-java)
    - [Use Case of Command Design Pattern](#use-case-of-command-design-pattern)
    - [Realtime Full Example of Command Design Pattern](#realtime-full-example-of-command-design-pattern)
    - [Pros and Cons of Command Design Pattern](#pros-and-cons-of-command-design-pattern)

3. [What is Interpreter Design Pattern](#what-is-interpreter-design-pattern)
    - [What is the Interpreter Design Pattern](#what-is-the-interpreter-design-pattern)
    - [When to use Interpreter Design Pattern](#when-to-use-interpreter-design-pattern)
    - [Key Components of Interpreter Design Pattern](#key-components-of-interpreter-design-pattern)
    - [Different Ways to Implement Interpreter Design Pattern](#different-ways-to-implement-interpreter-design-pattern)
    - [Interpreter Design Pattern Full Example in Java](#interpreter-design-pattern-full-example-in-java)
    - [Use Case of Interpreter Design Pattern](#use-case-of-interpreter-design-pattern)
    - [Realtime Full Example of Interpreter Design Pattern](#realtime-full-example-of-interpreter-design-pattern)
    - [Pros and Cons of Interpreter Design Pattern](#pros-and-cons-of-interpreter-design-pattern)

4. [What is Mediator Design Pattern](#what-is-mediator-design-pattern)
    - [What is the Mediator Design Pattern](#what-is-the-mediator-design-pattern)
    - [When to use Mediator Design Pattern](#when-to-use-mediator-design-pattern)
    - [Key Components of Mediator Design Pattern](#key-components-of-mediator-design-pattern)
    - [Different Ways to Implement Mediator Design Pattern](#different-ways-to-implement-mediator-design-pattern)
    - [Mediator Design Pattern Full Example in Java](#mediator-design-pattern-full-example-in-java)
    - [Use Case of Mediator Design Pattern](#use-case-of-mediator-design-pattern)
    - [Realtime Full Example of Mediator Design Pattern](#realtime-full-example-of-mediator-design-pattern)
    - [Pros and Cons of Mediator Design Pattern](#pros-and-cons-of-mediator-design-pattern)

5. [What is Memento Design Pattern](#what-is-memento-design-pattern)
    - [What is the Memento Design Pattern](#what-is-the-memento-design-pattern)
    - [When to use Memento Design Pattern](#when-to-use-memento-design-pattern)
    - [Key Components of Memento Design Pattern](#key-components-of-memento-design-pattern)
    - [Different Ways to Implement Memento Design Pattern](#different-ways-to-implement-memento-design-pattern)
    - [Memento Design Pattern Full Example in Java](#memento-design-pattern-full-example-in-java)
    - [Use Case of Memento Design Pattern](#use-case-of-memento-design-pattern)
    - [Realtime Full Example of Memento Design Pattern](#realtime-full-example-of-memento-design-pattern)
    - [Pros and Cons of Memento Design Pattern](#pros-and-cons-of-memento-design-pattern)

6. [What is Observer Design Pattern](#what-is-observer-design-pattern)
    - [What is the Observer Design Pattern](#what-is-the-observer-design-pattern)
    - [When to use Observer Design Pattern](#when-to-use-observer-design-pattern)
    - [Key Components of Observer Design Pattern](#key-components-of-observer-design-pattern)
    - [Different Ways to Implement Observer Design Pattern](#different-ways-to-implement-observer-design-pattern)
    - [Observer Design Pattern Full Example in Java](#observer-design-pattern-full-example-in-java)
    - [Use Case of Observer Design Pattern](#use-case-of-observer-design-pattern)
    - [Realtime Full Example of Observer Design Pattern](#realtime-full-example-of-observer-design-pattern)
    - [Pros and Cons of Observer Design Pattern](#pros-and-cons-of-observer-design-pattern)


7. [What is State Design Pattern](#what-is-state-design-pattern)
    - [What is the State Design Pattern](#what-is-the-state-design-pattern)
    - [When to use State Design Pattern](#when-to-use-state-design-pattern)
    - [Key Components of State Design Pattern](#key-components-of-state-design-pattern)
    - [Different Ways to Implement State Design Pattern](#different-ways-to-implement-state-design-pattern)
    - [State Design Pattern Full Example in Java](#state-design-pattern-full-example-in-java)
    - [Use Case of State Design Pattern](#use-case-of-state-design-pattern)
    - [Realtime Full Example of State Design Pattern](#realtime-full-example-of-state-design-pattern)
    - [Pros and Cons of State Design Pattern](#pros-and-cons-of-state-design-pattern)

8. [What is Strategy Design Pattern](#what-is-strategy-design-pattern)
    - [What is the Strategy Design Pattern](#what-is-the-strategy-design-pattern)
    - [When to use Strategy Design Pattern](#when-to-use-strategy-design-pattern)
    - [Key Components of Strategy Design Pattern](#key-components-of-strategy-design-pattern)
    - [Different Ways to Implement Strategy Design Pattern](#different-ways-to-implement-strategy-design-pattern)
    - [Strategy Design Pattern Full Example in Java](#strategy-design-pattern-full-example-in-java)
    - [Use Case of Strategy Design Pattern](#use-case-of-strategy-design-pattern)
    - [Realtime Full Example of Strategy Design Pattern](#realtime-full-example-of-strategy-design-pattern)
    - [Pros and Cons of Strategy Design Pattern](#pros-and-cons-of-strategy-design-pattern)

9. [What is Template Design Pattern](#what-is-template-design-pattern)
    - [What is the Template Design Pattern](#what-is-the-template-design-pattern)
    - [When to use Template Design Pattern](#when-to-use-template-design-pattern)
    - [Key Components of Template Design Pattern](#key-components-of-template-design-pattern)
    - [Different Ways to Implement Template Design Pattern](#different-ways-to-implement-template-design-pattern)
    - [Template Design Pattern Full Example in Java](#template-design-pattern-full-example-in-java)
    - [Use Case of Template Design Pattern](#use-case-of-template-design-pattern)
    - [Realtime Full Example of Template Design Pattern](#realtime-full-example-of-template-design-pattern)
    - [Pros and Cons of Template Design Pattern](#pros-and-cons-of-template-design-pattern)

10. [What is Visitor Design Pattern](#what-is-visitor-design-pattern)
    - [What is the Visitor Design Pattern](#what-is-the-visitor-design-pattern)
    - [When to use FlywVisitoreight Design Pattern](#when-to-use-visitor-design-pattern)
    - [Key Components of Visitor Design Pattern](#key-components-of-visitor-design-pattern)
    - [Different Ways to Implement Visitor Design Pattern](#different-ways-to-implement-visitor-design-pattern)
    - [Visitor Design Pattern Full Example in Java](#visitor-design-pattern-full-example-in-java)
    - [Use Case of Visitor Design Pattern](#use-case-of-visitor-design-pattern)
    - [Realtime Full Example of Visitor Design Pattern](#realtime-full-example-of-visitor-design-pattern)
    - [Pros and Cons of Visitor Design Pattern](#pros-and-cons-of-visitor-design-pattern)

# What is Chain of Responsibility Design Pattern

### What is the Chain of Responsibility Design Pattern
The Chain of Responsibility Design Pattern is a behavioral pattern that allows an object to pass a request along a chain of handlers. Each handler in the chain has the opportunity to process the request or pass it on to the next handler in the chain. This pattern decouples the sender of the request from the receiver, giving multiple objects the chance to handle the request without the sender needing to know which object will ultimately process it.

### When to use Chain of Responsibility Design Pattern
Use the Chain of Responsibility Design Pattern when
- You want to decouple the sender and receiver of a request.
- There are multiple objects that can handle a request, and the handler isn't known in advance.
- You want to allow handlers to process requests in a specific order.

### Key Components of Chain of Responsibility Design Pattern
1. **Handler**: Defines an interface for handling requests. It also implements the successor link, which allows the handler to pass the request to the next handler in the chain.
2. **ConcreteHandler**: Implements the Handler interface and is responsible for processing requests it can handle. If it can't handle the request, it forwards the request to its successor.
3. **Client**: Initiates requests to the chain of handlers.

### Different Ways to Implement Chain of Responsibility Design Pattern
There are several ways to implement the Chain of Responsibility pattern, including:
- Using a linked list where each handler has a reference to the next handler in the chain.
- Using a tree structure where each handler has references to its child handlers.

### Chain of Responsibility Design Pattern Full Example in Java
Here's a simple example of the Chain of Responsibility pattern in Java:

```java
// Handler interface
public interface Handler {
    void setNext(Handler handler);
    void handleRequest(Request request);
}

// ConcreteHandler
public class ConcreteHandler1 implements Handler {
    private Handler nextHandler;
    
    public void setNext(Handler handler) {
        this.nextHandler = handler;
    }
    
    public void handleRequest(Request request) {
        if (request.getType() == RequestType.TYPE1) {
            System.out.println("Request handled by ConcreteHandler1");
        } else if (nextHandler != null) {
            nextHandler.handleRequest(request);
        }
    }
}

public class ConcreteHandler2 implements Handler {
    private Handler nextHandler;
    
    public void setNext(Handler handler) {
        this.nextHandler = handler;
    }
    
    public void handleRequest(Request request) {
        if (request.getType() == RequestType.TYPE2) {
            System.out.println("Request handled by ConcreteHandler2");
        } else if (nextHandler != null) {
            nextHandler.handleRequest(request);
        }
    }
}

// Request and RequestType
public class Request {
    private RequestType type;
    
    public Request(RequestType type) {
        this.type = type;
    }
    
    public RequestType getType() {
        return type;
    }
}

public enum RequestType {
    TYPE1, TYPE2
}

// Client
public class Client {
    public static void main(String[] args) {
        Handler handler1 = new ConcreteHandler1();
        Handler handler2 = new ConcreteHandler2();
        handler1.setNext(handler2);
        
        handler1.handleRequest(new Request(RequestType.TYPE1)); // Output: Request handled by ConcreteHandler1
        handler1.handleRequest(new Request(RequestType.TYPE2)); // Output: Request handled by ConcreteHandler2
        handler1.handleRequest(new Request(RequestType.TYPE3)); // No handler found
    }
}
```

### Use Case of Chain of Responsibility Design Pattern
A common use case is in an approval process, where different levels of management need to approve a request. Each level of management can be represented by a handler, and if one level can't approve the request, it gets passed to the next level.

### Real-time Example of Chain of Responsibility Design Pattern in Detail
Consider a software system that processes incoming support tickets. The system has three levels of support: basic, advanced, and expert. Each level corresponds to a handler in the chain. When a ticket comes in, the system first checks if the basic support can handle it. If not, it gets passed to the advanced support, and so on.

### Pros and Cons of Chain of Responsibility Design Pattern
**Pros**:
- Decouples senders and receivers of requests.
- Allows for dynamic addition or removal of handlers.
- Can be used to implement complex processing pipelines.

**Cons**:
- Requests may go unhandled if the chain is not properly configured.
- Can be less efficient if the chain is long and each handler takes significant time to process the request.
# What is Command Design Pattern

### What is the Command Design Pattern?
The Command Design Pattern is a behavioral pattern that encapsulates a request as an object, thereby allowing for parameterization of clients with different requests, queuing of requests, and logging of requests. It also supports the undoable operations.

### When to use Command Design Pattern?
Use the Command Design Pattern when you want to:
- Parameterize objects based on an action to be performed.
- Queue requests or operations.
- Implement undo functionality.

### Key Components of Command Design Pattern
1. **Command**: Interface that declares a method for executing a command.
2. **ConcreteCommand**: Implements the Command interface and defines the binding between the action and the receiver.
3. **Invoker**: Asks the command to carry out the request.
4. **Receiver**: Knows how to perform the operations associated with the command.

### Different Ways to Implement Command Design Pattern
The Command Design Pattern can be implemented in various ways, including:
1. **Simple Command**: The command directly implements the execution logic.
2. **Macro Command**: A command that executes a sequence of sub-commands.
3. **Undoable Command**: A command that supports undo functionality by storing state information.

### Command Design Pattern Full Example in Java
Here's a simple example of the Command pattern in Java:

```java
// Command interface
public interface Command {
    void execute();
}

// Receiver
public class Light {
    public void turnOn() {
        System.out.println("Light is on");
    }

    public void turnOff() {
        System.out.println("Light is off");
    }
}

// ConcreteCommand
public class TurnOnCommand implements Command {
    private Light light;

    public TurnOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.turnOn();
    }
}

// Invoker
public class Switch {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void executeCommand() {
        command.execute();
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        Light light = new Light();
        Command turnOnCommand = new TurnOnCommand(light);

        Switch lightSwitch = new Switch();
        lightSwitch.setCommand(turnOnCommand);
        lightSwitch.executeCommand();
    }
}
```

### Use Case of Command Design Pattern
A common use case is in a restaurant ordering system. Each order can be represented as a command, and the kitchen can execute the orders (commands) in the order they were received.

### Real-time Example of Command Design Pattern in Detail
Consider a remote control for a television. The remote control has buttons for different operations such as turn on, turn off, increase volume, decrease volume, etc. Each button press can be represented as a command, and the television can execute these commands accordingly.

### Pros and Cons of Command Design Pattern
**Pros**:
- Decouples the sender and receiver of a request.
- Allows for parameterization of clients with different requests.
- Supports undo operations.
- Allows for queuing of requests.

**Cons**:
- Can introduce a large number of command classes if not used carefully.
- May increase complexity, especially for simple operations.
# What is Interpreter Design Pattern

### What is the Interpreter Design Pattern?
The Interpreter Design Pattern is a behavioral pattern that defines a grammar for a language and provides a way to evaluate sentences in that language. It is used to interpret sentences or expressions of a language and is commonly used in domain-specific languages (DSLs) or in cases where complex rules need to be interpreted.

### When to use Interpreter Design Pattern?
Use the Interpreter Design Pattern when you have a language to interpret, a grammar for the language, and you want to be able to evaluate expressions in the language.

### Key Components of Interpreter Design Pattern
1. **AbstractExpression**: Declares an interpret() operation that all concrete expressions must implement.
2. **TerminalExpression**: Implements the interpret() operation for terminal symbols in the grammar.
3. **NonterminalExpression**: Represents a rule in the grammar, typically composed of other expressions.
4. **Context**: Contains information that is global to the interpreter and is passed to expressions as they interpret.

### Different Ways to Implement Interpreter Design Pattern
The Interpreter Design Pattern can be implemented in various ways, including:
1. **Recursive Descent Parser**: A parser that works by recursively calling methods corresponding to the grammar rules.
2. **Abstract Syntax Tree (AST)**: Represents the structure of a parsed program in the form of a tree.

### Interpreter Design Pattern Full Example in Java
Here's a simple example of the Interpreter pattern in Java, interpreting a simple arithmetic expression:

```java
// AbstractExpression
interface Expression {
    int interpret();
}

// TerminalExpression
class NumberExpression implements Expression {
    private int number;

    public NumberExpression(int number) {
        this.number = number;
    }

    public int interpret() {
        return number;
    }
}

// NonterminalExpression
class AdditionExpression implements Expression {
    private Expression left;
    private Expression right;

    public AdditionExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public int interpret() {
        return left.interpret() + right.interpret();
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        // 1 + 2
        Expression expression = new AdditionExpression(new NumberExpression(1), new NumberExpression(2));
        System.out.println(expression.interpret()); // Output: 3
    }
}
```

### Use Case of Interpreter Design Pattern
A common use case is in the development of a query language interpreter. For example, interpreting a SQL query into a form that can be executed by a database.

### Real-time Example of Interpreter Design Pattern in Detail
Consider a software system that processes mathematical expressions provided as strings. The system can parse these strings into an abstract syntax tree (AST) using the Interpreter pattern and then evaluate the expressions represented by the AST.

### Pros and Cons of Interpreter Design Pattern
**Pros**:
- Easily extendable to support new expressions or rules.
- Can be used to implement complex grammars.
- Promotes a flexible and maintainable design for interpreting languages or rules.

**Cons**:
- Can be complex to implement, especially for large or complex grammars.
- May not be suitable for all types of problems, especially those that require high performance or efficiency.
# What is Mediator Design Pattern

### What is the Mediator Design Pattern?
The Mediator Design Pattern is a behavioral pattern that defines an object (the mediator) that coordinates communication between multiple objects (colleagues) without them needing to explicitly reference each other. It promotes loose coupling between the objects by keeping them from referring to each other explicitly and allows for easier maintenance and extension of the system.

### When to use Mediator Design Pattern?
Use the Mediator Design Pattern when you have a set of objects that communicate in complex ways and you want to reduce the dependencies between them. It's especially useful when you have a large number of objects and the communication logic between them becomes hard to maintain.

### Key Components of Mediator Design Pattern
1. **Mediator**: Defines an interface for communicating with colleague objects.
2. **ConcreteMediator**: Implements the Mediator interface and coordinates communication between colleague objects.
3. **Colleague**: Defines an interface for communicating with other colleagues through the mediator.
4. **ConcreteColleague**: Implements the Colleague interface and communicates with other colleagues through the mediator.

### Different Ways to Implement Mediator Design Pattern
The Mediator Design Pattern can be implemented in various ways, including:
1. **Centralized Mediator**: Uses a single mediator object that all colleagues reference.
2. **Distributed Mediator**: Uses multiple mediator objects that collaborate to mediate communication between colleagues.

### Mediator Design Pattern Full Example in Java
Here's a simple example of the Mediator pattern in Java, where a chat room acts as a mediator between users:

```java
// Mediator
interface ChatRoom {
    void sendMessage(String message, User user);
}

// ConcreteMediator
class ChatRoomImpl implements ChatRoom {
    public void sendMessage(String message, User user) {
        System.out.println(user.getName() + " sends message: " + message);
    }
}

// Colleague
class User {
    private String name;
    private ChatRoom chatRoom;

    public User(String name, ChatRoom chatRoom) {
        this.name = name;
        this.chatRoom = chatRoom;
    }

    public String getName() {
        return name;
    }

    public void sendMessage(String message) {
        chatRoom.sendMessage(message, this);
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoomImpl();
        User user1 = new User("Alice", chatRoom);
        User user2 = new User("Bob", chatRoom);

        user1.sendMessage("Hi, Bob!");
        user2.sendMessage("Hello, Alice!");
    }
}
```

### Use Case of Mediator Design Pattern
A common use case is in a graphical user interface (GUI) framework, where multiple components (such as buttons, text fields, etc.) need to communicate with each other. The mediator can manage the interactions between these components.

### Real-time Example of Mediator Design Pattern in Detail
Consider an air traffic control system where multiple flights need to communicate their positions and intentions to each other. The mediator (air traffic controller) coordinates the communication between the flights, ensuring safe and efficient operation of the airspace.

### Pros and Cons of Mediator Design Pattern
**Pros**:
- Reduces dependencies between objects, making the system easier to maintain and extend.
- Centralizes control of communication logic, simplifying complex interactions.
- Promotes reusability of individual components.

**Cons**:
- Can introduce a single point of failure if the mediator becomes too complex or overloaded.
- May increase the complexity of the system, especially if not used appropriately.
# What is Memento Design Pattern

### What is the Memento Design Pattern?
The Memento Design Pattern is a behavioral pattern that allows an object to capture its internal state without exposing its internal structure. This captured state can then be stored externally and restored to the object later, effectively restoring the object to its previous state. The pattern is often used to implement undo mechanisms and to restore objects to a previous state.

### When to use Memento Design Pattern?
Use the Memento Design Pattern when you want to:
- Save and restore the state of an object without revealing its internal structure.
- Create checkpoints in the object's state to support undo operations.
- Manage the history of an object's state changes.

### Key Components of Memento Design Pattern
1. **Originator**: The object whose state needs to be saved and restored. It creates a memento containing a snapshot of its state and can restore its state from a memento.
2. **Memento**: A token object that stores the state of the originator. It only allows the originator to access its state.
3. **Caretaker**: Responsible for keeping the memento. It does not operate on or examine the contents of the memento.

### Different Ways to Implement Memento Design Pattern
The Memento Design Pattern can be implemented in various ways, including:
1. **Narrow interface**: The memento provides a narrow interface to the originator to access its state, ensuring that only the originator can access its state.
2. **Wide interface**: The memento provides a wide interface to the originator, allowing other objects to access and modify its state. However, this violates the principle of encapsulation.

### Memento Design Pattern Full Example in Java
Here's a simple example of the Memento pattern in Java, where a text editor can save and restore its state:

```java
// Memento
class EditorMemento {
    private final String content;

    public EditorMemento(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}

// Originator
class TextEditor {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public EditorMemento save() {
        return new EditorMemento(content);
    }

    public void restore(EditorMemento memento) {
        content = memento.getContent();
    }
}

// Caretaker
class History {
    private List<EditorMemento> mementos = new ArrayList<>();

    public void push(EditorMemento memento) {
        mementos.add(memento);
    }

    public EditorMemento pop() {
        int lastIndex = mementos.size() - 1;
        EditorMemento memento = mementos.get(lastIndex);
        mementos.remove(lastIndex);
        return memento;
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        TextEditor editor = new TextEditor();
        History history = new History();

        editor.setContent("Hello");
        history.push(editor.save());

        editor.setContent("Hello World");
        history.push(editor.save());

        editor.setContent("Hello World!");
        System.out.println("Current Content: " + editor.getContent());

        editor.restore(history.pop());
        System.out.println("Restored Content: " + editor.getContent());

        editor.restore(history.pop());
        System.out.println("Restored Content: " + editor.getContent());
    }
}
```

### Use Case of Memento Design Pattern
A common use case is in text editors or graphic editors where users can undo and redo operations. Each time a user makes a change, a memento is created and stored in a history. When the user wants to undo an operation, the editor restores the state from the previous memento.

### Real-time Example of Memento Design Pattern in Detail
Consider a drawing application where users can create shapes and move them around. The application can use the Memento pattern to save the state of each shape (position, size, color, etc.) when it is created or modified. If the user wants to undo an operation, the application can restore the state of the shape from the previous memento.

### Pros and Cons of Memento Design Pattern
**Pros**:
- Allows an object to capture and externalize its state.
- Supports undo and redo operations.
- Simplifies the originator's code by encapsulating the state in a memento.

**Cons**:
- Can consume a lot of memory if mementos are not managed properly.
- Caretaker must manage mementos appropriately to avoid memory leaks.

# What is Observer Design Pattern

### What is the Observer Design Pattern?
The Observer Design Pattern is a behavioral pattern that defines a one-to-many dependency between objects, so that when one object changes state, all its dependents are notified and updated automatically. It is also known as the publish-subscribe pattern.

### When to use Observer Design Pattern?
Use the Observer Design Pattern when you want to:
- Establish a one-to-many dependency between objects, where changes to one object trigger updates to its dependents.
- Avoid tight coupling between objects, allowing for easier modification and extension.

### Key Components of Observer Design Pattern
1. **Subject (Observable)**: Maintains a list of observers, provides methods to add and remove observers, and notifies observers of changes in state.
2. **Observer**: Defines an update() method that is called by the subject to notify the observer of a change in state.
3. **ConcreteSubject (ConcreteObservable)**: Extends the subject and implements the specific logic for adding, removing, and notifying observers.
4. **ConcreteObserver**: Implements the observer interface and defines the specific actions to take in response to updates from the subject.

### Different Ways to Implement Observer Design Pattern
The Observer Design Pattern can be implemented in various ways, including:
1. **Push Model**: The subject sends detailed information about the change to its observers.
2. **Pull Model**: The subject sends a simple notification to its observers, and observers pull the information they need from the subject.

### Observer Design Pattern Full Example in Java
Here's a simple example of the Observer pattern in Java, where a subject (WeatherStation) notifies its observers (Display) of changes in weather:

```java
// Observer
interface Observer {
    void update(float temperature);
}

// Subject
interface Subject {
    void registerObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();
}

// Concrete Observer
class Display implements Observer {
    public void update(float temperature) {
        System.out.println("Temperature updated: " + temperature);
    }
}

// Concrete Subject
class WeatherStation implements Subject {
    private List<Observer> observers = new ArrayList<>();
    private float temperature;

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(temperature);
        }
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
        notifyObservers();
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        WeatherStation weatherStation = new WeatherStation();
        Display display = new Display();

        weatherStation.registerObserver(display);
        weatherStation.setTemperature(25.5f); // Output: Temperature updated: 25.5
    }
}
```

### Use Case of Observer Design Pattern
A common use case is in graphical user interface (GUI) frameworks, where components (observers) need to be notified of changes in other components (subjects), such as in the Model-View-Controller (MVC) pattern.

### Real-time Example of Observer Design Pattern in Detail
Consider a stock market application where users can track the prices of various stocks. Each stock price can be a subject, and the user interface components displaying the prices can be observers. When the price of a stock changes, all the components displaying that stock's price are notified and updated.

### Pros and Cons of Observer Design Pattern
**Pros**:
- Supports the principle of loose coupling between objects.
- Allows for one-to-many communication between objects.
- Provides a well-defined interface for updating dependents.

**Cons**:
- Can result in memory leaks if observers are not properly removed from the subject.
- Subjects may notify observers even when there are no meaningful changes, leading to unnecessary updates.
# What is State Design Pattern

### What is the State Design Pattern?
The State Design Pattern is a behavioral pattern that allows an object to change its behavior when its internal state changes. It encapsulates states into separate classes and delegates the state-specific behavior to these classes. This pattern is useful when an object's behavior depends on its state and the behavior needs to change dynamically at runtime.

### When to use State Design Pattern?
Use the State Design Pattern when:
- An object's behavior changes depending on its internal state, and it needs to change at runtime.
- State-specific behavior is defined in individual classes, making it easier to maintain and extend the code.
- You want to avoid using conditional statements to determine an object's behavior based on its state.

### Key Components of State Design Pattern
1. **Context**: Defines the interface of interest to clients. It maintains an instance of a ConcreteState subclass that defines the current state.
2. **State**: Defines an interface for encapsulating the behavior associated with a particular state of the Context.
3. **ConcreteState**: Each subclass implements a behavior associated with a state of the Context.

### Different Ways to Implement State Design Pattern
The State Design Pattern can be implemented in various ways, including:
1. **Context Switching**: Context changes its current state to another state based on some conditions or events.
2. **State Pattern**: Each state is represented by a class and the context has a reference to the current state.

### State Design Pattern Full Example in Java
Here's a simple example of the State pattern in Java, where a fan can be off, low, medium, or high:

```java
// State
interface State {
    void pull(CeilingFanPullChain wrapper);
}

// Concrete States
class Off implements State {
    public void pull(CeilingFanPullChain wrapper) {
        wrapper.set_state(new Low());
        System.out.println("   low speed");
    }
}

class Low implements State {
    public void pull(CeilingFanPullChain wrapper) {
        wrapper.set_state(new Medium());
        System.out.println("   medium speed");
    }
}

class Medium implements State {
    public void pull(CeilingFanPullChain wrapper) {
        wrapper.set_state(new High());
        System.out.println("   high speed");
    }
}

class High implements State {
    public void pull(CeilingFanPullChain wrapper) {
        wrapper.set_state(new Off());
        System.out.println("   turning off");
    }
}

// Context
class CeilingFanPullChain {
    private State current;

    public CeilingFanPullChain() {
        current = new Off();
    }

    public void set_state(State s) {
        current = s;
    }

    public void pull() {
        current.pull(this);
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        CeilingFanPullChain chain = new CeilingFanPullChain();
        while (true) {
            chain.pull();
        }
    }
}
```

### Use Case of State Design Pattern
A common use case is in vending machines, where the machine's behavior depends on its current state (e.g., no change, not enough change, item out of stock).

### Real-time Example of State Design Pattern in Detail
Consider an online chat application where the user can be in different states like online, offline, away, or busy. Each state has different behavior, such as receiving messages, notifications, etc.

### Pros and Cons of State Design Pattern
**Pros**:
- Encapsulates state-specific behavior in individual classes, making it easier to maintain and extend.
- Eliminates conditional statements for state-based behavior, improving readability.
- Promotes the Open/Closed Principle, as new states can be added without modifying existing code.

**Cons**:
- Can introduce many small classes if the states and transitions are complex.
- May be overkill for simpler state-dependent behavior.

# What is Strategy Design Pattern

### What is the Strategy Design Pattern?
The Strategy Design Pattern is a behavioral pattern that enables an algorithm's behavior to be selected at runtime. It defines a family of algorithms, encapsulates each algorithm, and makes them interchangeable. This pattern allows the client to choose the algorithm needed without changing its context.

### When to use Strategy Design Pattern?
Use the Strategy Design Pattern when:
- You want to define a class that will have one behavior that is similar to other behaviors in a list.
- You need to use one of several behaviors dynamically.
- You have many related classes that differ only in their behavior.
- You need to isolate the implementation details of an algorithm from the code that uses it.

### Key Components of Strategy Design Pattern
1. **Strategy**: Interface or abstract class that defines the algorithm interface.
2. **ConcreteStrategy**: Implements the algorithm using the Strategy interface.
3. **Context**: Uses the Strategy interface to call the algorithm defined in ConcreteStrategy.

### Different Ways to Implement Strategy Design Pattern
The Strategy Design Pattern can be implemented in various ways, including:
1. **Using Interface**: Define a Strategy interface and have multiple classes implementing this interface for different strategies.
2. **Using Abstract Class**: Define an abstract class as the base strategy and have concrete classes extend this abstract class.

### Strategy Design Pattern Full Example in Java
Here's a simple example of the Strategy pattern in Java, where different sorting algorithms are used:

```java
// Strategy
interface SortingStrategy {
    void sort(int[] numbers);
}

// Concrete Strategies
class BubbleSort implements SortingStrategy {
    public void sort(int[] numbers) {
        // Implement bubble sort algorithm
    }
}

class QuickSort implements SortingStrategy {
    public void sort(int[] numbers) {
        // Implement quick sort algorithm
    }
}

// Context
class Sorter {
    private SortingStrategy strategy;

    public Sorter(SortingStrategy strategy) {
        this.strategy = strategy;
    }

    public void setStrategy(SortingStrategy strategy) {
        this.strategy = strategy;
    }

    public void sort(int[] numbers) {
        strategy.sort(numbers);
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        int[] numbers = {5, 1, 3, 6, 2, 4};
        Sorter sorter = new Sorter(new BubbleSort());
        sorter.sort(numbers); // Use bubble sort

        sorter.setStrategy(new QuickSort());
        sorter.sort(numbers); // Use quick sort
    }
}
```

### Use Case of Strategy Design Pattern
A common use case is in payment processing systems, where different payment methods (e.g., credit card, PayPal, bank transfer) can be used interchangeably based on user choice.

### Real-time Example of Strategy Design Pattern in Detail
Consider a navigation application where the user can choose different routes based on preferences such as fastest route, shortest route, or scenic route. Each route-finding algorithm represents a strategy.

### Pros and Cons of Strategy Design Pattern
**Pros**:
- Encapsulates algorithms into separate classes, making it easy to switch between them.
- Allows the algorithm to vary independently from the context using it.
- Promotes the Open/Closed Principle, as new strategies can be added without changing existing code.

**Cons**:
- Clients must be aware of the strategies and select the appropriate one, which can add complexity.
- Increases the number of classes in the codebase, especially if there are many strategies.

# What is Template Design Pattern

### What is the Template Design Pattern?
The Template Design Pattern is a behavioral pattern that defines the skeleton of an algorithm in a method, deferring some steps to subclasses. It allows subclasses to redefine certain steps of the algorithm without changing its structure, promoting code reuse and reducing duplication.

### When to use Template Design Pattern?
Use the Template Design Pattern when you want to:
- Implement the invariant parts of an algorithm once and let subclasses implement the variant parts.
- Avoid duplication in code by moving common behavior to a superclass.
- Define a template method that can be customized by subclasses to provide specific behavior.

### Key Components of Template Design Pattern
1. **AbstractClass**: Defines abstract methods that represent steps of the algorithm. It also contains a template method that defines the algorithm's structure.
2. **ConcreteClass**: Implements the abstract methods defined in the AbstractClass to provide specific behavior for the algorithm's steps.

### Different Ways to Implement Template Design Pattern
The Template Design Pattern can be implemented in various ways, including:
1. **Abstract Class**: Use an abstract class to define the template method and abstract methods for the steps of the algorithm.
2. **Interface**: Use an interface to define the template method and provide default implementations for some steps using default methods (Java 8+).

### Template Design Pattern Full Example in Java
Here's a simple example of the Template pattern in Java, where a beverage preparation algorithm is defined:

```java
// AbstractClass
abstract class Beverage {
    final void prepareBeverage() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            addCondiments();
        }
    }

    abstract void brew();

    abstract void addCondiments();

    void boilWater() {
        System.out.println("Boiling water");
    }

    void pourInCup() {
        System.out.println("Pouring into cup");
    }

    // Hook method
    boolean customerWantsCondiments() {
        return true;
    }
}

// ConcreteClass
class Coffee extends Beverage {
    void brew() {
        System.out.println("Dripping coffee through filter");
    }

    void addCondiments() {
        System.out.println("Adding sugar and milk");
    }
}

// ConcreteClass
class Tea extends Beverage {
    void brew() {
        System.out.println("Steeping the tea");
    }

    void addCondiments() {
        System.out.println("Adding lemon");
    }

    // Override hook method
    boolean customerWantsCondiments() {
        return false;
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        Beverage coffee = new Coffee();
        coffee.prepareBeverage();

        Beverage tea = new Tea();
        tea.prepareBeverage();
    }
}
```

### Use Case of Template Design Pattern
A common use case is in software frameworks, where the framework provides a template method that clients can override to customize behavior, such as in web frameworks for request processing.

### Real-time Example of Template Design Pattern in Detail
Consider a document processing application where different types of documents (e.g., reports, letters) follow a similar structure but have different content. The application can use the Template pattern to define a common process for creating documents, with subclasses implementing the specific content for each type of document.

### Pros and Cons of Template Design Pattern
**Pros**:
- Promotes code reuse by defining a common algorithm in a superclass.
- Allows subclasses to customize behavior without changing the algorithm's structure.
- Encapsulates common behavior in a single place, making it easier to maintain.

**Cons**:
- Subclasses are tightly coupled to the superclass, which can make the code harder to maintain if the superclass changes.
- May lead to a complex hierarchy of subclasses if not used carefully.

# What is Visitor Design Pattern

### What is the Visitor Design Pattern?
The Visitor Design Pattern is a behavioral pattern that allows adding new operations to existing classes without modifying them. It is used to separate the algorithm from the object structure on which it operates. The pattern consists of defining a visitor class that implements the operations to be performed on the elements of an object structure, and then passing this visitor object to the elements to perform the operation.

### When to use Visitor Design Pattern?
Use the Visitor Design Pattern when you want to:
- Add new operations to existing classes without modifying them.
- Keep related operations together and separate unrelated ones.
- Define operations that need to be applied to multiple unrelated classes.

### Key Components of Visitor Design Pattern
1. **Visitor**: Declares a visit method for each class of ConcreteElement in the object structure.
2. **ConcreteVisitor**: Implements each operation declared by the Visitor interface. Provides the implementation for the operations defined for each ConcreteElement class.
3. **Element**: Defines an accept method that accepts a visitor as an argument.
4. **ConcreteElement**: Implements the accept method defined in Element. Calls the visit method of the visitor, passing itself as an argument.
5. **ObjectStructure**: Represents a collection of elements. Provides a way to iterate over its elements.

### Different Ways to Implement Visitor Design Pattern
The Visitor Design Pattern can be implemented in various ways, including:
1. **Single Dispatch**: The visitor pattern as described above, where the type of the visitor determines the method to call.
2. **Double Dispatch**: In languages that support method overloading, the method to call is determined by both the type of the visitor and the type of the element.

### Visitor Design Pattern Full Example in Java
Here's a simple example of the Visitor pattern in Java, where a visitor calculates the total price of items in a shopping cart:

```java
// Visitor
interface ShoppingCartVisitor {
    double visit(ItemElement item);
}

// ConcreteVisitor
class ShoppingCartVisitorImpl implements ShoppingCartVisitor {
    public double visit(ItemElement item) {
        double cost = item.getPrice();
        System.out.println("Item: " + item.getName() + ", Cost = " + cost);
        return cost;
    }
}

// Element
interface ItemElement {
    double accept(ShoppingCartVisitor visitor);
}

// ConcreteElement
class Book implements ItemElement {
    private String name;
    private double price;

    public Book(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double accept(ShoppingCartVisitor visitor) {
        return visitor.visit(this);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

// ConcreteElement
class Fruit implements ItemElement {
    private String name;
    private double pricePerKg;
    private double weight;

    public Fruit(String name, double pricePerKg, double weight) {
        this.name = name;
        this.pricePerKg = pricePerKg;
        this.weight = weight;
    }

    public double accept(ShoppingCartVisitor visitor) {
        return visitor.visit(this);
    }

    public String getName() {
        return name;
    }

    public double getPricePerKg() {
        return pricePerKg;
    }

    public double getWeight() {
        return weight;
    }
}

// Client
public class Client {
    public static void main(String[] args) {
        ItemElement[] items = new ItemElement[] {
                new Book("Book 1", 100),
                new Fruit("Apple", 2, 1.5),
                new Fruit("Banana", 1, 2)
        };

        ShoppingCartVisitor visitor = new ShoppingCartVisitorImpl();
        double totalPrice = calculatePrice(items, visitor);
        System.out.println("Total Price = " + totalPrice);
    }

    private static double calculatePrice(ItemElement[] items, ShoppingCartVisitor visitor) {
        double totalPrice = 0;
        for (ItemElement item : items) {
            totalPrice += item.accept(visitor);
        }
        return totalPrice;
    }
}
```

### Use Case of Visitor Design Pattern
A common use case is in compilers, where different elements of the abstract syntax tree can be visited by a visitor to perform various operations such as type checking, optimization, and code generation.

### Real-time Example of Visitor Design Pattern in Detail
Consider a software application that processes different shapes (e.g., circles, rectangles) and needs to calculate various properties (e.g., area, perimeter) of these shapes. The application can use the Visitor pattern to define a visitor that calculates these properties for each type of shape.

### Pros and Cons of Visitor Design Pattern
**Pros**:
- Adds new operations to existing classes without modifying them, promoting the Open/Closed Principle.
- Keeps related behavior together, making the code easier to maintain and understand.
- Allows for double dispatch in languages that support method overloading.

**Cons**:
- Can make the code more complex by introducing new classes and methods.
- Requires changes to the element interface when adding new ConcreteElement classes, which can be cumbersome in some cases.

