# Java SOLID Principles
## Table of Contents
1. [What is Java SOLID Principles?](#what-is-java-solid-principles)
2. [What is Single Responsibility Principle (SRP)?](#what-is-single-responsibility-principle-srp)
3. [What is Open-Closed Principle (OCP)?](#what-is-open-closed-principle-ocp)
4. [What is Liskov Substitution Principle (LSP)?](#what-is-liskov-substitution-principle-lsp)
5. [What is Interface Segregation Principle (ISP)?](#what-is-interface-segregation-principle-isp)
6. [What is Dependency Inversion Principle (DIP)?](#what-is-dependency-inversion-principle-dip)


# What is Java SOLID Principles?
### **Java SOLID Principles**
SOLID is an acronym that represents five core principles of object-oriented programming and design. These principles, introduced by Robert C. Martin (Uncle Bob), help developers create maintainable, scalable, and testable software. Let's go through each principle in detail with examples.

---

## **1. Single Responsibility Principle (SRP)**
**Definition**: A class should have only one reason to change, meaning it should have only one responsibility.

### **Example**
**Bad Design:**
```java
class Report {
    public void generateReport() {
        // Code to generate report
    }

    public void printReport() {
        // Code to print report
    }

    public void saveToDatabase() {
        // Code to save report in database
    }
}
```
**Problems:**
- This class has multiple responsibilities: generating a report, printing, and saving it to the database.
- Any change in printing logic would affect other functionalities.

**Good Design:**
```java
class ReportGenerator {
    public void generateReport() {
        // Code to generate report
    }
}

class ReportPrinter {
    public void printReport() {
        // Code to print report
    }
}

class ReportRepository {
    public void saveToDatabase() {
        // Code to save report in database
    }
}
```
**Benefits:**
- Each class has a single responsibility.
- Changes in one functionality don’t affect others.

---

## **2. Open-Closed Principle (OCP)**
**Definition**: A class should be open for extension but closed for modification.

### **Example**
**Bad Design (Violating OCP):**
```java
class DiscountCalculator {
    public double calculateDiscount(String customerType, double amount) {
        if (customerType.equals("Regular")) {
            return amount * 0.1;
        } else if (customerType.equals("Premium")) {
            return amount * 0.2;
        }
        return 0;
    }
}
```
**Problems:**
- If we introduce a new customer type (e.g., "VIP"), we need to modify the class.
- Violates OCP because modification is required for extensions.

**Good Design (Following OCP):**
```java
interface DiscountStrategy {
    double getDiscount(double amount);
}

class RegularCustomerDiscount implements DiscountStrategy {
    public double getDiscount(double amount) {
        return amount * 0.1;
    }
}

class PremiumCustomerDiscount implements DiscountStrategy {
    public double getDiscount(double amount) {
        return amount * 0.2;
    }
}

class DiscountCalculator {
    public double calculateDiscount(DiscountStrategy strategy, double amount) {
        return strategy.getDiscount(amount);
    }
}
```
**Benefits:**
- Adding new discount types requires creating a new class instead of modifying existing code.
- Supports scalability and maintainability.

---

## **3. Liskov Substitution Principle (LSP)**
**Definition**: Subtypes must be substitutable for their base types without altering the correctness of the program.

### **Example**
**Bad Design (Violating LSP):**
```java
class Rectangle {
    protected int width, height;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getArea() {
        return width * height;
    }
}

class Square extends Rectangle {
    @Override
    public void setWidth(int width) {
        this.width = this.height = width;
    }

    @Override
    public void setHeight(int height) {
        this.width = this.height = height;
    }
}
```
**Problems:**
- `Square` violates LSP because it changes the behavior of `Rectangle` methods.
- If a function relies on `Rectangle`, it may break when given a `Square`.

**Good Design (Following LSP):**
```java
interface Shape {
    int getArea();
}

class Rectangle implements Shape {
    protected int width, height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getArea() {
        return width * height;
    }
}

class Square implements Shape {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    public int getArea() {
        return side * side;
    }
}
```
**Benefits:**
- `Rectangle` and `Square` now properly adhere to LSP.
- They both implement `Shape` and can be used interchangeably.

---

## **4. Interface Segregation Principle (ISP)**
**Definition**: A class should not be forced to implement interfaces it does not use.

### **Example**
**Bad Design (Violating ISP):**
```java
interface Worker {
    void work();
    void eat();
}

class Robot implements Worker {
    public void work() {
        System.out.println("Robot is working...");
    }

    public void eat() {
        throw new UnsupportedOperationException("Robots do not eat!");
    }
}
```
**Problems:**
- `Robot` is forced to implement `eat()`, which it does not need.
- Leads to unnecessary code and exceptions.

**Good Design (Following ISP):**
```java
interface Workable {
    void work();
}

interface Eatable {
    void eat();
}

class HumanWorker implements Workable, Eatable {
    public void work() {
        System.out.println("Human is working...");
    }

    public void eat() {
        System.out.println("Human is eating...");
    }
}

class RobotWorker implements Workable {
    public void work() {
        System.out.println("Robot is working...");
    }
}
```
**Benefits:**
- Classes only implement what they need.
- More flexibility and cleaner design.

---

## **5. Dependency Inversion Principle (DIP)**
**Definition**: High-level modules should not depend on low-level modules. Both should depend on abstractions.

### **Example**
**Bad Design (Violating DIP):**
```java
class WiredKeyboard {
    public void connect() {
        System.out.println("Wired keyboard connected.");
    }
}

class Computer {
    private WiredKeyboard keyboard;

    public Computer() {
        this.keyboard = new WiredKeyboard();
    }

    public void start() {
        keyboard.connect();
        System.out.println("Computer started.");
    }
}
```
**Problems:**
- `Computer` is tightly coupled with `WiredKeyboard`.
- If we want to switch to a wireless keyboard, we need to modify `Computer`.

**Good Design (Following DIP):**
```java
interface Keyboard {
    void connect();
}

class WiredKeyboard implements Keyboard {
    public void connect() {
        System.out.println("Wired keyboard connected.");
    }
}

class WirelessKeyboard implements Keyboard {
    public void connect() {
        System.out.println("Wireless keyboard connected.");
    }
}

class Computer {
    private Keyboard keyboard;

    public Computer(Keyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void start() {
        keyboard.connect();
        System.out.println("Computer started.");
    }
}
```
**Benefits:**
- `Computer` depends on the `Keyboard` interface, not a specific implementation.
- Easily switch between different keyboard types.

---

## **Conclusion**
Following the SOLID principles ensures:
✅ Better maintainability  
✅ Easier testing  
✅ Scalable and flexible code  
✅ Reduction in bugs and coupling  

By applying these principles, you can design software that is more modular, robust, and adaptable to changes over time.

# What is Single Responsibility Principle (SRP)?
## **Single Responsibility Principle (SRP)**
### **Definition**
The **Single Responsibility Principle (SRP)** states that **a class should have only one reason to change**, meaning it should **only have one responsibility**. 

This principle helps keep the code clean, maintainable, and easier to debug by ensuring that each class has a single, well-defined purpose.

---

## **Why is SRP Important?**
When a class has multiple responsibilities, the following problems can occur:
1. **Increased complexity** – More responsibilities in a class make it harder to understand.
2. **High coupling** – Unrelated functionalities become dependent on each other.
3. **Difficult to modify** – Changes in one functionality might unintentionally affect others.
4. **Reduced reusability** – A class with mixed responsibilities is harder to reuse in different contexts.
5. **Testing becomes harder** – Unit tests need to cover multiple functionalities, increasing the risk of breaking unrelated parts of the code.

---

## **Real-Time Example 1: Employee Management System**
### **Bad Design (Violating SRP)**
```java
class Employee {
    private String name;
    private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    // Responsibility 1: Managing employee details
    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    // Responsibility 2: Calculating salary
    public double calculateBonus() {
        return salary * 0.1; // 10% bonus calculation
    }

    // Responsibility 3: Storing employee data in database
    public void saveToDatabase() {
        System.out.println("Saving employee details to database...");
    }
}
```
### **Problems**
- **Multiple responsibilities**: This class is handling **employee details**, **salary calculation**, and **database storage**.
- **Difficult to modify**: If we change the salary calculation logic, we also risk affecting how employee data is stored in the database.
- **Low reusability**: We cannot reuse `calculateBonus()` in another class without also bringing unnecessary database-related logic.

---

### **Good Design (Following SRP)**
We split the responsibilities into separate classes.

```java
// Single responsibility: Managing employee details
class Employee {
    private String name;
    private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }
}

// Single responsibility: Handling salary calculations
class SalaryCalculator {
    public double calculateBonus(Employee employee) {
        return employee.getSalary() * 0.1;
    }
}

// Single responsibility: Handling database operations
class EmployeeRepository {
    public void saveToDatabase(Employee employee) {
        System.out.println("Saving employee details to database...");
    }
}
```
### **Benefits**
✔ **Each class has a single responsibility**  
✔ **Easier to modify**: If bonus calculation logic changes, we only modify `SalaryCalculator`.  
✔ **Improved reusability**: `SalaryCalculator` can be used independently in another system.  
✔ **Better testability**: We can write separate unit tests for each functionality.  

---

## **Real-Time Example 2: Order Processing System**
### **Bad Design (Violating SRP)**
```java
class Order {
    private String orderId;
    private double amount;

    public Order(String orderId, double amount) {
        this.orderId = orderId;
        this.amount = amount;
    }

    // Responsibility 1: Handling order details
    public String getOrderId() {
        return orderId;
    }

    public double getAmount() {
        return amount;
    }

    // Responsibility 2: Processing payments
    public void processPayment() {
        System.out.println("Processing payment for Order ID: " + orderId);
    }

    // Responsibility 3: Sending order confirmation email
    public void sendOrderConfirmation() {
        System.out.println("Sending order confirmation email for Order ID: " + orderId);
    }
}
```
### **Problems**
- The `Order` class is responsible for **order management**, **payment processing**, and **email notifications**.
- Changes in one area may impact others.
- The code is **less reusable** and **harder to test**.

---

### **Good Design (Following SRP)**
**Refactoring the code to follow SRP:**
```java
// Single responsibility: Managing order details
class Order {
    private String orderId;
    private double amount;

    public Order(String orderId, double amount) {
        this.orderId = orderId;
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public double getAmount() {
        return amount;
    }
}

// Single responsibility: Handling payments
class PaymentProcessor {
    public void processPayment(Order order) {
        System.out.println("Processing payment for Order ID: " + order.getOrderId());
    }
}

// Single responsibility: Sending email notifications
class EmailService {
    public void sendOrderConfirmation(Order order) {
        System.out.println("Sending order confirmation email for Order ID: " + order.getOrderId());
    }
}
```
### **Benefits**
✔ **Easier Maintenance**: Changes in payment logic won’t affect order management.  
✔ **High Reusability**: `PaymentProcessor` and `EmailService` can be used for other purposes.  
✔ **Better Testing**: Each class can be tested separately.  

---

## **Real-Life Industry Use Cases**
### **1. Banking Application**
- **Bad Approach:** A single `BankAccount` class handling account management, transaction processing, and reporting.  
- **Good Approach:**  
  - `BankAccount`: Manages account details.  
  - `TransactionProcessor`: Handles transactions.  
  - `ReportGenerator`: Creates account statements.  

### **2. E-Commerce Website**
- **Bad Approach:** A single `ShoppingCart` class manages items, calculates totals, processes payments, and sends notifications.  
- **Good Approach:**  
  - `ShoppingCart`: Manages items.  
  - `CartTotalCalculator`: Calculates the total price.  
  - `PaymentProcessor`: Handles payments.  
  - `EmailNotification`: Sends purchase confirmations.  

---

## **Conclusion**
Following the **Single Responsibility Principle (SRP)** leads to:
✅ **More modular and reusable code**  
✅ **Easier debugging and maintenance**  
✅ **Improved testability**  
✅ **Reduced dependencies and better scalability**  

## **More Real-Time Use Cases of the Single Responsibility Principle (SRP)**

The **Single Responsibility Principle (SRP)** states that a class should have **only one reason to change**. In other words, a class should have **only one responsibility**.

Here are more **real-time industry use cases** where SRP is either followed or violated and how we can design the system correctly.

---

## **Use Case 1: Hospital Management System (Patient Record Handling)**
### **Bad Design (Violating SRP)**
A **Patient** class is responsible for multiple things:
- Storing patient details
- Calculating medical bill
- Saving records to the database

```java
class Patient {
    private String name;
    private int age;
    private double medicalBill;

    public Patient(String name, int age, double medicalBill) {
        this.name = name;
        this.age = age;
        this.medicalBill = medicalBill;
    }

    // Responsibility 1: Retrieving patient details
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    // Responsibility 2: Calculating medical bill
    public double calculateBill() {
        return medicalBill * 1.05; // 5% tax added
    }

    // Responsibility 3: Saving to database
    public void saveToDatabase() {
        System.out.println("Saving patient data to database...");
    }
}
```

### **Problem**
🚨 **Violates SRP because:**
1. The class **stores patient data**, **calculates bills**, and **handles database storage**.
2. If the hospital changes **how bills are calculated**, we must modify this class.
3. If we introduce a **new database**, we must modify this class.

---

### **Good Design (Following SRP)**
We **split responsibilities** into separate classes.

```java
// Responsibility 1: Managing patient details
class Patient {
    private String name;
    private int age;

    public Patient(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

// Responsibility 2: Handling bill calculations
class BillingService {
    public double calculateBill(double baseAmount) {
        return baseAmount * 1.05; // 5% tax added
    }
}

// Responsibility 3: Handling database operations
class PatientRepository {
    public void saveToDatabase(Patient patient) {
        System.out.println("Saving patient data to database...");
    }
}
```

### **Benefits**
✔ **Each class has a single responsibility.**  
✔ **Easy to modify** – Changing the billing logic does not affect patient data storage.  
✔ **Better testability** – We can unit test each functionality separately.  

---

## **Use Case 2: E-Commerce Order Processing**
### **Bad Design (Violating SRP)**
An **Order** class is responsible for:
- Storing order details
- Calculating total price
- Processing payments
- Sending confirmation emails

```java
class Order {
    private String orderId;
    private double amount;

    public Order(String orderId, double amount) {
        this.orderId = orderId;
        this.amount = amount;
    }

    // Responsibility 1: Storing order details
    public String getOrderId() {
        return orderId;
    }

    public double getAmount() {
        return amount;
    }

    // Responsibility 2: Calculating total price
    public double calculateTotalPrice() {
        return amount * 1.1; // 10% tax added
    }

    // Responsibility 3: Processing payments
    public void processPayment() {
        System.out.println("Processing payment...");
    }

    // Responsibility 4: Sending confirmation email
    public void sendConfirmationEmail() {
        System.out.println("Sending confirmation email...");
    }
}
```

### **Problem**
🚨 **Violates SRP because:**
1. The `Order` class **mixes multiple concerns** (order management, pricing, payment, email).
2. If we change the **pricing logic**, we must modify this class.
3. If we introduce **new payment methods**, we must modify this class.
4. It makes **unit testing complex**.

---

### **Good Design (Following SRP)**
We **separate responsibilities**.

```java
// Responsibility 1: Storing order details
class Order {
    private String orderId;
    private double amount;

    public Order(String orderId, double amount) {
        this.orderId = orderId;
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public double getAmount() {
        return amount;
    }
}

// Responsibility 2: Calculating price
class PriceCalculator {
    public double calculateTotalPrice(double amount) {
        return amount * 1.1; // 10% tax added
    }
}

// Responsibility 3: Handling payments
class PaymentProcessor {
    public void processPayment(Order order) {
        System.out.println("Processing payment for Order ID: " + order.getOrderId());
    }
}

// Responsibility 4: Sending email notifications
class EmailService {
    public void sendConfirmationEmail(Order order) {
        System.out.println("Sending confirmation email for Order ID: " + order.getOrderId());
    }
}
```

### **Benefits**
✔ **Each class has a single responsibility.**  
✔ **Easier to maintain** – Changing one functionality does not affect others.  
✔ **Better reusability** – We can reuse `PaymentProcessor` for other transactions.  

---

## **Use Case 3: Logging in a Web Application**
### **Bad Design (Violating SRP)**
A **LoginService** class handles:
- User authentication
- Logging login attempts
- Sending security alerts

```java
class LoginService {
    public boolean authenticate(String username, String password) {
        System.out.println("Authenticating user...");
        return username.equals("admin") && password.equals("password");
    }

    public void logLoginAttempt(String username, boolean success) {
        System.out.println("Logging login attempt for " + username);
    }

    public void sendSecurityAlert(String username) {
        System.out.println("Sending security alert for suspicious login attempt: " + username);
    }
}
```

### **Problem**
🚨 **Violates SRP because:**
1. `LoginService` handles **authentication, logging, and security alerts**.
2. If we change the **authentication logic**, we must modify this class.
3. If we change **logging behavior**, we must modify this class.

---

### **Good Design (Following SRP)**
We **split the responsibilities**.

```java
// Responsibility 1: User Authentication
class AuthenticationService {
    public boolean authenticate(String username, String password) {
        System.out.println("Authenticating user...");
        return username.equals("admin") && password.equals("password");
    }
}

// Responsibility 2: Logging login attempts
class LoginLogger {
    public void logLoginAttempt(String username, boolean success) {
        System.out.println("Logging login attempt for " + username);
    }
}

// Responsibility 3: Sending security alerts
class SecurityAlertService {
    public void sendSecurityAlert(String username) {
        System.out.println("Sending security alert for suspicious login attempt: " + username);
    }
}
```

### **Benefits**
✔ **Each class has a single responsibility.**  
✔ **Easy to modify** – Changes in authentication logic don’t affect logging.  
✔ **Better security** – `SecurityAlertService` can be enhanced separately.  

---

## **Conclusion**
By following **SRP**, we ensure:
✅ **Modular and reusable code**  
✅ **Easier debugging and maintenance**  
✅ **Better separation of concerns**  
✅ **Improved testability**  

By **splitting responsibilities**, we create **scalable, maintainable, and flexible** software. 🚀

By keeping classes focused on **one responsibility**, we ensure **better code organization and long-term maintainability** in software development. 🚀

# What is Open-Closed Principle (OCP)? 
## **Open-Closed Principle (OCP)**
### **Definition**
The **Open-Closed Principle (OCP)** states that a **class should be open for extension but closed for modification**. This means that:
1. **Open for extension** → The functionality of a class should be extendable.
2. **Closed for modification** → Existing code should not be changed when adding new features.

This principle encourages the use of **abstraction, inheritance, and polymorphism** to add new behavior without modifying existing code.

---

## **Why is OCP Important?**
If we frequently modify existing code to introduce new functionality, we may face issues like:
- **Increased risk of breaking existing functionality.**
- **Reduced maintainability** because every modification requires testing of the whole system.
- **Code fragility**, where a small change in one area affects multiple parts.

By applying OCP, we ensure:
✔ **Stable codebase**  
✔ **Scalability** – Easily extend behavior without altering existing code  
✔ **Better maintainability**  

---

## **Real-Time Example 1: Payment Processing System**
### **Bad Design (Violating OCP)**
Consider an e-commerce application where payments can be made using different methods such as **Credit Card** and **PayPal**.

```java
class PaymentProcessor {
    public void processPayment(String paymentType) {
        if (paymentType.equals("CreditCard")) {
            System.out.println("Processing Credit Card payment...");
        } else if (paymentType.equals("PayPal")) {
            System.out.println("Processing PayPal payment...");
        }
    }
}
```
### **Problems**
🚨 **Violates OCP**:  
- If a new payment method (e.g., Bitcoin) is introduced, we must modify `processPayment()`, risking breaking existing functionality.  
- **Code is not scalable**: Every new payment method requires modifying this class.

---

### **Good Design (Following OCP)**
We refactor the code using **polymorphism** and **abstraction**.

```java
// Step 1: Create an abstract Payment class
abstract class Payment {
    public abstract void pay();
}

// Step 2: Implement different payment methods
class CreditCardPayment extends Payment {
    public void pay() {
        System.out.println("Processing Credit Card payment...");
    }
}

class PayPalPayment extends Payment {
    public void pay() {
        System.out.println("Processing PayPal payment...");
    }
}

// Step 3: Modify the PaymentProcessor to work with any payment type
class PaymentProcessor {
    public void processPayment(Payment payment) {
        payment.pay();
    }
}

// Step 4: Using the PaymentProcessor in a real-world scenario
public class Main {
    public static void main(String[] args) {
        PaymentProcessor processor = new PaymentProcessor();

        Payment creditCard = new CreditCardPayment();
        processor.processPayment(creditCard); // Processing Credit Card payment...

        Payment paypal = new PayPalPayment();
        processor.processPayment(paypal); // Processing PayPal payment...
    }
}
```
### **Benefits**
✔ **Open for extension**: We can add new payment methods (Bitcoin, Apple Pay, etc.) without modifying `PaymentProcessor`.  
✔ **Closed for modification**: The existing class remains unchanged.  
✔ **Follows OCP** by using **abstraction and polymorphism**.  

---

## **Real-Time Example 2: Notification System**
### **Bad Design (Violating OCP)**
A system that sends **email and SMS notifications**:
```java
class NotificationService {
    public void sendNotification(String type, String message) {
        if (type.equals("Email")) {
            System.out.println("Sending Email: " + message);
        } else if (type.equals("SMS")) {
            System.out.println("Sending SMS: " + message);
        }
    }
}
```
### **Problems**
- **Not extensible**: Adding **Push Notification** or **WhatsApp notification** requires modifying this class.
- **Violates OCP**: Changes require modifying existing code.

---

### **Good Design (Following OCP)**
We refactor the code using **interface-based design**.

```java
// Step 1: Create an interface
interface Notification {
    void send(String message);
}

// Step 2: Implement multiple notification types
class EmailNotification implements Notification {
    public void send(String message) {
        System.out.println("Sending Email: " + message);
    }
}

class SMSNotification implements Notification {
    public void send(String message) {
        System.out.println("Sending SMS: " + message);
    }
}

// Step 3: Use a general NotificationService
class NotificationService {
    public void sendNotification(Notification notification, String message) {
        notification.send(message);
    }
}

// Step 4: Using the NotificationService
public class Main {
    public static void main(String[] args) {
        NotificationService service = new NotificationService();

        Notification email = new EmailNotification();
        service.sendNotification(email, "Hello via Email");

        Notification sms = new SMSNotification();
        service.sendNotification(sms, "Hello via SMS");
    }
}
```
### **Benefits**
✔ **Open for extension**: We can add **Push Notifications, WhatsApp, or Slack** without modifying `NotificationService`.  
✔ **Closed for modification**: Existing code remains untouched.  
✔ **Highly scalable and maintainable**.  

---

## **Real-Life Industry Use Cases**
### **1. ATM Withdrawal System**
- **Bad Approach:** A single `ATMWithdrawal` class that handles different types of accounts (Savings, Checking, Business).  
- **Good Approach:**  
  - `WithdrawalProcessor` (abstract class/interface)  
  - `SavingsWithdrawal`, `CheckingWithdrawal`, and `BusinessWithdrawal` implement `WithdrawalProcessor`.  
  - The system remains **open for extension and closed for modification** when new account types are introduced.

### **2. Report Generation System**
- **Bad Approach:** A `ReportGenerator` class that generates PDF and Excel reports within the same method.  
- **Good Approach:**  
  - `Report` (abstract class)  
  - `PDFReport` and `ExcelReport` implement `Report`  
  - If we need a new format (e.g., `HTMLReport`), we add it without modifying the existing system.

---

## **Conclusion**
The **Open-Closed Principle (OCP)** ensures:
✅ **Scalable code**  
✅ **Easy maintenance**  
✅ **Avoids modification of existing, working code**  
✅ **Supports future extensions with minimal effort**  

By following OCP, developers can build **flexible and future-proof software**. 🚀

# What is Liskov Substitution Principle (LSP)? 
## **Liskov Substitution Principle (LSP)**
### **Definition**
The **Liskov Substitution Principle (LSP)** states that **a subclass should be substitutable for its base class without altering the correctness of the program**.

In other words, **if class B is a subclass of class A, then objects of class A should be replaceable with objects of class B without affecting the program behavior**.

This principle was introduced by **Barbara Liskov** and ensures that inheritance is used correctly.

---

## **Why is LSP Important?**
If LSP is violated, we may face:
1. **Unexpected behaviors** when substituting subclasses.
2. **Incorrect inheritance hierarchy**, leading to **code that is hard to maintain**.
3. **Bugs and broken functionality** due to **incompatible subclass implementations**.

To **satisfy LSP**, subclasses must:
✔ **Not weaken or remove functionality of the base class**  
✔ **Preserve base class behavior**  
✔ **Follow the same contract as the base class**  

---

## **Real-Time Example 1: Rectangle and Square Problem**
### **Bad Design (Violating LSP)**
Let’s consider a classic example of **Rectangle and Square**.

```java
class Rectangle {
    protected int width, height;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getArea() {
        return width * height;
    }
}
```

Now, we create a **Square** class that extends **Rectangle**.

```java
class Square extends Rectangle {
    @Override
    public void setWidth(int width) {
        this.width = this.height = width;
    }

    @Override
    public void setHeight(int height) {
        this.width = this.height = height;
    }
}
```

### **Problem**
🚨 **LSP is violated because:**  
- The `Square` class **modifies the behavior** of `setWidth()` and `setHeight()`.
- If we substitute a `Rectangle` with a `Square`, the program will **behave unexpectedly**.

### **Example of Unexpected Behavior**
```java
public class Main {
    public static void main(String[] args) {
        Rectangle rect = new Square();
        rect.setWidth(4);
        rect.setHeight(5);
        
        System.out.println("Expected Area: 4 * 5 = 20");
        System.out.println("Actual Area: " + rect.getArea()); // Prints 25 (wrong result)
    }
}
```
**Expected Output:** `20`  
**Actual Output:** `25` (incorrect behavior)

### **Why is this a problem?**
- A `Rectangle` should allow independent changes to `width` and `height`, but `Square` enforces equal values.
- **A Square is not truly a Rectangle in this case.**
- Any function expecting a `Rectangle` will not work correctly when given a `Square`.

---

### **Good Design (Following LSP)**
Instead of forcing `Square` to inherit from `Rectangle`, we **extract a common interface**:

```java
interface Shape {
    int getArea();
}

class Rectangle implements Shape {
    protected int width, height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getArea() {
        return width * height;
    }
}

class Square implements Shape {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    public int getArea() {
        return side * side;
    }
}
```
Now, both `Rectangle` and `Square` implement `Shape`, but they **do not inherit from each other**.

### **Benefits**
✔ **No unexpected behavior when substituting**  
✔ **Preserves LSP by ensuring consistency**  
✔ **More flexible and maintainable design**  

---

## **Real-Time Example 2: Bird Hierarchy**
### **Bad Design (Violating LSP)**
Let’s say we have a `Bird` class with a `fly()` method.

```java
class Bird {
    public void fly() {
        System.out.println("Bird is flying...");
    }
}
```
Now, we create a `Penguin` class, which **extends `Bird`**, but **penguins cannot fly**.

```java
class Penguin extends Bird {
    @Override
    public void fly() {
        throw new UnsupportedOperationException("Penguins cannot fly!");
    }
}
```
### **Problem**
🚨 **LSP is violated because:**
- If a function expects a `Bird`, it might call `fly()` and crash if given a `Penguin`.

```java
public class Main {
    public static void makeBirdFly(Bird bird) {
        bird.fly();
    }

    public static void main(String[] args) {
        Bird sparrow = new Bird();
        makeBirdFly(sparrow);  // Works fine

        Bird penguin = new Penguin();
        makeBirdFly(penguin);  // Throws an exception!
    }
}
```
### **Solution (Following LSP)**
We should **separate flying and non-flying birds** using interfaces.

```java
interface Bird {
    void eat();
}

interface Flyable {
    void fly();
}

class Sparrow implements Bird, Flyable {
    public void eat() {
        System.out.println("Sparrow is eating...");
    }

    public void fly() {
        System.out.println("Sparrow is flying...");
    }
}

class Penguin implements Bird {
    public void eat() {
        System.out.println("Penguin is eating...");
    }
}
```

### **Benefits**
✔ `Penguin` **does not override** `fly()`, preserving LSP.  
✔ **Now, functions expecting `Flyable` will only receive birds that can fly.**  
✔ **Correct use of interfaces prevents runtime errors.**  

---

## **Real-Life Industry Use Cases**
### **1. Vehicle Hierarchy**
- **Bad Approach:** A `Vehicle` base class with `startEngine()`, but bicycles don’t have engines.  
- **Good Approach:**  
  - `Vehicle` → Common properties  
  - `MotorizedVehicle extends Vehicle` → Has `startEngine()`  
  - `Bicycle extends Vehicle` → Does **not** have `startEngine()`  

### **2. User Roles in a System**
- **Bad Approach:** A `User` base class with `generateReports()`, but normal users don’t generate reports.  
- **Good Approach:**  
  - `User` → Common properties  
  - `AdminUser extends User` → Has `generateReports()`  
  - `RegularUser extends User` → No `generateReports()`  

---

## **Conclusion**
The **Liskov Substitution Principle (LSP)** ensures:
✅ **Correct inheritance hierarchy**  
✅ **No unexpected behaviors when substituting subclasses**  
✅ **Prevents incorrect usage of polymorphism**  
✅ **Encourages interface segregation and correct abstraction**  

By following LSP, we **design better, bug-free, and maintainable software**. 🚀

## **More Real-Time Use Cases and Explanation of Liskov Substitution Principle (LSP)**

The **Liskov Substitution Principle (LSP)** ensures that **subtypes can replace base types without altering the correctness of the program**. Let’s explore more **real-time industry use cases** where LSP is either followed or violated and how we can design the system correctly.

---

## **Use Case 1: Banking System - Accounts Management**
### **Bad Design (Violating LSP)**
A bank has different types of accounts, such as:
- **Savings Account** (earns interest)
- **Fixed Deposit Account** (cannot withdraw before maturity)

A naive approach would be to create a base class `BankAccount` and extend it for different account types.

```java
class BankAccount {
    protected double balance;

    public void deposit(double amount) {
        balance += amount;
    }

    public void withdraw(double amount) {
        balance -= amount;
    }
}
```

Now, we extend it for **Fixed Deposit Account**.

```java
class FixedDepositAccount extends BankAccount {
    @Override
    public void withdraw(double amount) {
        throw new UnsupportedOperationException("Withdrawal not allowed for Fixed Deposit Account!");
    }
}
```

### **Problem**
🚨 **LSP is violated because:**
- If a function expects a `BankAccount` and tries to call `withdraw()`, it will throw an exception for `FixedDepositAccount`.

```java
public class Main {
    public static void withdrawFunds(BankAccount account, double amount) {
        account.withdraw(amount);
    }

    public static void main(String[] args) {
        BankAccount savings = new BankAccount();
        withdrawFunds(savings, 500);  // Works fine

        BankAccount fd = new FixedDepositAccount();
        withdrawFunds(fd, 500);  // Throws exception!
    }
}
```

### **Solution (Following LSP)**
We **split the behavior** using interfaces:

```java
interface Withdrawable {
    void withdraw(double amount);
}

class SavingsAccount extends BankAccount implements Withdrawable {
    public void withdraw(double amount) {
        balance -= amount;
    }
}

class FixedDepositAccount extends BankAccount {
    // No withdraw method, since it is not withdrawable
}
```

Now, we can check at **compile-time** if an account supports withdrawal:

```java
public class Main {
    public static void withdrawFunds(Withdrawable account, double amount) {
        account.withdraw(amount);
    }

    public static void main(String[] args) {
        Withdrawable savings = new SavingsAccount();
        withdrawFunds(savings, 500);  // Works fine

        FixedDepositAccount fd = new FixedDepositAccount();
        // Now, we cannot call withdrawFunds() on fd, ensuring correctness
    }
}
```

### **Benefits**
✔ `FixedDepositAccount` does **not override `withdraw()` incorrectly**.  
✔ **Prevents runtime errors** caused by invalid operations.  
✔ **Clear separation of behavior using interfaces.**  

---

## **Use Case 2: E-Commerce System - Product Discount**
### **Bad Design (Violating LSP)**
A system applies **discounts** based on the type of product.

```java
class Product {
    protected double price;

    public Product(double price) {
        this.price = price;
    }

    public double getDiscountedPrice() {
        return price;
    }
}
```

A **Seasonal Discount Product** applies a discount.

```java
class SeasonalDiscountProduct extends Product {
    public SeasonalDiscountProduct(double price) {
        super(price);
    }

    @Override
    public double getDiscountedPrice() {
        return price * 0.9; // 10% discount
    }
}
```

Now, we introduce **Non-Discountable Product**, such as luxury items, which should **not allow** discounts.

```java
class NonDiscountableProduct extends Product {
    @Override
    public double getDiscountedPrice() {
        throw new UnsupportedOperationException("This product does not allow discounts!");
    }
}
```

### **Problem**
🚨 **LSP is violated because:**
- If a function expects `Product` and calls `getDiscountedPrice()`, it **crashes for `NonDiscountableProduct`**.

```java
public class Main {
    public static void main(String[] args) {
        Product product = new SeasonalDiscountProduct(100);
        System.out.println(product.getDiscountedPrice());  // Works fine

        Product luxuryItem = new NonDiscountableProduct(100);
        System.out.println(luxuryItem.getDiscountedPrice());  // Throws exception!
    }
}
```

### **Solution (Following LSP)**
We separate **discountable** and **non-discountable** products using interfaces.

```java
interface Discountable {
    double getDiscountedPrice();
}

class Product {
    protected double price;

    public Product(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}

class SeasonalDiscountProduct extends Product implements Discountable {
    public SeasonalDiscountProduct(double price) {
        super(price);
    }

    public double getDiscountedPrice() {
        return price * 0.9;
    }
}

class NonDiscountableProduct extends Product {
    // No getDiscountedPrice method, ensuring LSP compliance
}
```

### **Benefits**
✔ Prevents **calling `getDiscountedPrice()` on non-discountable products**.  
✔ **Compile-time safety** – prevents runtime crashes.  
✔ **Clear separation of behavior using interfaces.**  

---

## **Use Case 3: Employee Management System**
### **Bad Design (Violating LSP)**
A system calculates **salary** for different types of employees.

```java
class Employee {
    protected double baseSalary;

    public Employee(double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public double calculateSalary() {
        return baseSalary;
    }
}
```

Now, we introduce **Interns** who **do not receive a fixed salary but get a stipend**.

```java
class Intern extends Employee {
    @Override
    public double calculateSalary() {
        throw new UnsupportedOperationException("Interns do not receive fixed salary!");
    }
}
```

### **Problem**
🚨 **LSP is violated because:**
- If a function expects `Employee` and calls `calculateSalary()`, it crashes for `Intern`.

```java
public class Main {
    public static void main(String[] args) {
        Employee fullTimeEmployee = new Employee(5000);
        System.out.println(fullTimeEmployee.calculateSalary()); // Works fine

        Employee intern = new Intern(0);
        System.out.println(intern.calculateSalary()); // Throws exception!
    }
}
```

### **Solution (Following LSP)**
We create a separate interface for **payable employees**.

```java
interface Payable {
    double calculateSalary();
}

class Employee implements Payable {
    protected double baseSalary;

    public Employee(double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public double calculateSalary() {
        return baseSalary;
    }
}

class Intern {
    private double stipend;

    public Intern(double stipend) {
        this.stipend = stipend;
    }

    public double getStipend() {
        return stipend;
    }
}
```

### **Benefits**
✔ Prevents calling `calculateSalary()` on interns.  
✔ **Interns now have a separate behavior** that doesn’t violate LSP.  
✔ **Compile-time safety and maintainability.**  

---

## **Conclusion**
The **Liskov Substitution Principle (LSP)** ensures:
✅ **Correct inheritance hierarchy**  
✅ **No unexpected behavior when substituting subclasses**  
✅ **Prevents incorrect usage of polymorphism**  
✅ **Encourages interface segregation and correct abstraction**  

By following LSP, we **design better, bug-free, and maintainable software**. 🚀

## **More Real-Time Use Cases of the Open-Closed Principle (OCP)**

The **Open-Closed Principle (OCP)** states that a class should be **open for extension but closed for modification**. This means:
- **Open for extension** → We should be able to add new functionality without modifying existing code.
- **Closed for modification** → Existing, tested, and working code should remain unchanged.

### **Why is OCP Important?**
- Reduces **code breakage** when adding new features.
- Improves **scalability** and **maintainability**.
- Avoids modifying **existing logic**, reducing the risk of introducing new bugs.

---

## **Use Case 1: Payment Processing System**
### **Bad Design (Violating OCP)**
An **e-commerce application** allows **Credit Card** and **PayPal** payments.

```java
class PaymentProcessor {
    public void processPayment(String paymentType) {
        if (paymentType.equals("CreditCard")) {
            System.out.println("Processing Credit Card payment...");
        } else if (paymentType.equals("PayPal")) {
            System.out.println("Processing PayPal payment...");
        }
    }
}
```
### **Problems**
🚨 **Violates OCP because:**
1. If we introduce **Bitcoin payments**, we must modify `processPayment()`, increasing the risk of breaking existing functionality.
2. The `if-else` structure makes the class **harder to maintain**.

---

### **Good Design (Following OCP)**
We use **polymorphism** and **abstraction**.

```java
// Step 1: Create an interface for payment
interface Payment {
    void pay();
}

// Step 2: Implement payment methods
class CreditCardPayment implements Payment {
    public void pay() {
        System.out.println("Processing Credit Card payment...");
    }
}

class PayPalPayment implements Payment {
    public void pay() {
        System.out.println("Processing PayPal payment...");
    }
}

// Step 3: Use abstraction in PaymentProcessor
class PaymentProcessor {
    public void processPayment(Payment payment) {
        payment.pay();
    }
}

// Step 4: Using the PaymentProcessor
public class Main {
    public static void main(String[] args) {
        PaymentProcessor processor = new PaymentProcessor();

        Payment creditCard = new CreditCardPayment();
        processor.processPayment(creditCard); // Processing Credit Card payment...

        Payment paypal = new PayPalPayment();
        processor.processPayment(paypal); // Processing PayPal payment...
    }
}
```
### **Benefits**
✔ **Adding new payment methods (Bitcoin, Apple Pay) does not modify existing code.**  
✔ **No `if-else` structure; follows OCP.**  
✔ **Highly maintainable and scalable.**  

---

## **Use Case 2: Notification System**
### **Bad Design (Violating OCP)**
A **notification system** sends **Email and SMS notifications**.

```java
class NotificationService {
    public void sendNotification(String type, String message) {
        if (type.equals("Email")) {
            System.out.println("Sending Email: " + message);
        } else if (type.equals("SMS")) {
            System.out.println("Sending SMS: " + message);
        }
    }
}
```

### **Problems**
🚨 **Violates OCP because:**
1. If we introduce **Push Notification** or **WhatsApp Notification**, we must modify `sendNotification()`.
2. The `if-else` structure leads to **poor maintainability**.

---

### **Good Design (Following OCP)**
We **use an interface** to support multiple notification types.

```java
// Step 1: Create an interface
interface Notification {
    void send(String message);
}

// Step 2: Implement different notification types
class EmailNotification implements Notification {
    public void send(String message) {
        System.out.println("Sending Email: " + message);
    }
}

class SMSNotification implements Notification {
    public void send(String message) {
        System.out.println("Sending SMS: " + message);
    }
}

// Step 3: Use a general NotificationService
class NotificationService {
    public void sendNotification(Notification notification, String message) {
        notification.send(message);
    }
}

// Step 4: Using the NotificationService
public class Main {
    public static void main(String[] args) {
        NotificationService service = new NotificationService();

        Notification email = new EmailNotification();
        service.sendNotification(email, "Hello via Email");

        Notification sms = new SMSNotification();
        service.sendNotification(sms, "Hello via SMS");
    }
}
```
### **Benefits**
✔ **New notification types (WhatsApp, Slack, Push) can be added without modifying NotificationService.**  
✔ **No need to modify existing code.**  
✔ **Better maintainability and scalability.**  

---

## **Use Case 3: Employee Payroll System**
### **Bad Design (Violating OCP)**
A **payroll system** calculates salaries for different employee types.

```java
class Payroll {
    public double calculateSalary(String employeeType, double baseSalary) {
        if (employeeType.equals("FullTime")) {
            return baseSalary;
        } else if (employeeType.equals("Contractor")) {
            return baseSalary * 0.9; // Contractors get 10% less
        }
        return 0;
    }
}
```

### **Problems**
🚨 **Violates OCP because:**
1. If we add a new **Intern** employee type, we must modify `calculateSalary()`.
2. **Every change requires modifying existing logic**, increasing risk.

---

### **Good Design (Following OCP)**
We create **separate classes** for each employee type.

```java
// Step 1: Create an interface
interface Employee {
    double calculateSalary(double baseSalary);
}

// Step 2: Implement employee types
class FullTimeEmployee implements Employee {
    public double calculateSalary(double baseSalary) {
        return baseSalary;
    }
}

class ContractorEmployee implements Employee {
    public double calculateSalary(double baseSalary) {
        return baseSalary * 0.9; // 10% less salary
    }
}

// Step 3: Modify Payroll to use abstraction
class Payroll {
    public double calculateSalary(Employee employee, double baseSalary) {
        return employee.calculateSalary(baseSalary);
    }
}

// Step 4: Using the Payroll system
public class Main {
    public static void main(String[] args) {
        Payroll payroll = new Payroll();

        Employee fullTime = new FullTimeEmployee();
        System.out.println("Full-time salary: " + payroll.calculateSalary(fullTime, 5000));

        Employee contractor = new ContractorEmployee();
        System.out.println("Contractor salary: " + payroll.calculateSalary(contractor, 5000));
    }
}
```
### **Benefits**
✔ **Adding a new employee type (Intern) does not require modifying `Payroll`.**  
✔ **More modular and scalable.**  
✔ **No `if-else` structure.**  

---

## **Use Case 4: Report Generation System**
### **Bad Design (Violating OCP)**
A **report system** generates **PDF and Excel reports**.

```java
class ReportGenerator {
    public void generateReport(String type) {
        if (type.equals("PDF")) {
            System.out.println("Generating PDF Report...");
        } else if (type.equals("Excel")) {
            System.out.println("Generating Excel Report...");
        }
    }
}
```

### **Problems**
🚨 **Violates OCP because:**
1. Adding **HTML reports** requires modifying `generateReport()`.
2. **Frequent modifications introduce risks**.

---

### **Good Design (Following OCP)**
We create an **interface** for report generation.

```java
// Step 1: Create an interface
interface Report {
    void generate();
}

// Step 2: Implement different report types
class PDFReport implements Report {
    public void generate() {
        System.out.println("Generating PDF Report...");
    }
}

class ExcelReport implements Report {
    public void generate() {
        System.out.println("Generating Excel Report...");
    }
}

// Step 3: Use a general ReportGenerator
class ReportGenerator {
    public void generateReport(Report report) {
        report.generate();
    }
}

// Step 4: Using the ReportGenerator
public class Main {
    public static void main(String[] args) {
        ReportGenerator generator = new ReportGenerator();

        Report pdf = new PDFReport();
        generator.generateReport(pdf);

        Report excel = new ExcelReport();
        generator.generateReport(excel);
    }
}
```
### **Benefits**
✔ **Adding a new report type (HTML, CSV) does not modify `ReportGenerator`.**  
✔ **More flexible and maintainable.**  

---

## **Conclusion**
By following **OCP**, we create:
✅ **Extensible and scalable systems**  
✅ **Maintainable and reusable code**  
✅ **Less risk of breaking existing functionality**  

This principle helps in **building software that adapts to future requirements without modifying core functionality**. 🚀

# What is Interface Segregation Principle (ISP)?
## **Definition**
The **Interface Segregation Principle (ISP)** states that **a class should not be forced to implement interfaces it does not use**. Instead of having **large, monolithic interfaces**, we should split them into **smaller, more specific interfaces**.

### **Key Idea of ISP**
- **Avoid bloated interfaces** that force implementing classes to include unnecessary methods.
- **Prefer multiple small interfaces** that are **specific** to the functionalities they provide.
- **Improves flexibility and maintainability** by ensuring that classes only implement what they actually need.

---

## **Why is ISP Important?**
Violating ISP leads to:
🚨 **Unnecessary dependencies** – Classes depend on methods they don’t use.  
🚨 **Code rigidity** – If an interface changes, all implementing classes must change, even if they don’t use the new methods.  
🚨 **Difficult maintenance** – Every class implementing a large interface must handle unused methods.  

By applying ISP, we ensure:
✅ **More modular code** – Each interface serves a **single purpose**.  
✅ **Easier maintenance** – Classes don’t need to implement unnecessary methods.  
✅ **Flexible and scalable design** – Adding new functionality doesn’t break existing code.  

---

## **Real-Time Example 1: Workplace Roles (Bad Design)**
### **Bad Design (Violating ISP)**
Imagine a company where employees have different responsibilities. We create a **large `Employee` interface** that includes all possible employee functions.

```java
interface Employee {
    void work();
    void attendMeetings();
    void manageTeam();
    void conductInterviews();
}
```

Now, we have two types of employees:  
1️⃣ **Developers** – Work and attend meetings but don’t manage teams or conduct interviews.  
2️⃣ **Managers** – Work, attend meetings, manage teams, and conduct interviews.

Let’s say **both Developer and Manager must implement the interface**:

```java
class Developer implements Employee {
    public void work() {
        System.out.println("Developer is writing code...");
    }

    public void attendMeetings() {
        System.out.println("Developer is attending meetings...");
    }

    public void manageTeam() {
        throw new UnsupportedOperationException("Developers don't manage teams!");
    }

    public void conductInterviews() {
        throw new UnsupportedOperationException("Developers don't conduct interviews!");
    }
}
```

```java
class Manager implements Employee {
    public void work() {
        System.out.println("Manager is managing projects...");
    }

    public void attendMeetings() {
        System.out.println("Manager is attending meetings...");
    }

    public void manageTeam() {
        System.out.println("Manager is managing the team...");
    }

    public void conductInterviews() {
        System.out.println("Manager is conducting interviews...");
    }
}
```

### **Problems**
🚨 **Violates ISP because:**
- `Developer` is **forced to implement `manageTeam()` and `conductInterviews()`**, even though **developers don’t do these tasks**.
- Any change in `Employee` (e.g., adding a new method) **affects both Developer and Manager**, even if it's irrelevant to them.

---

### **Good Design (Following ISP)**
We **split the `Employee` interface** into **smaller, specific interfaces**.

```java
// Step 1: Create specific interfaces
interface Workable {
    void work();
}

interface Attendee {
    void attendMeetings();
}

interface TeamManager {
    void manageTeam();
}

interface Interviewer {
    void conductInterviews();
}

// Step 2: Implement only relevant interfaces
class Developer implements Workable, Attendee {
    public void work() {
        System.out.println("Developer is writing code...");
    }

    public void attendMeetings() {
        System.out.println("Developer is attending meetings...");
    }
}

class Manager implements Workable, Attendee, TeamManager, Interviewer {
    public void work() {
        System.out.println("Manager is managing projects...");
    }

    public void attendMeetings() {
        System.out.println("Manager is attending meetings...");
    }

    public void manageTeam() {
        System.out.println("Manager is managing the team...");
    }

    public void conductInterviews() {
        System.out.println("Manager is conducting interviews...");
    }
}
```

### **Benefits**
✔ **Developers only implement `Workable` and `Attendee` interfaces.**  
✔ **Managers implement all relevant interfaces.**  
✔ **Adding new roles (e.g., HR) is easy, without affecting existing classes.**  

---

## **Real-Time Example 2: Printer Functionality**
### **Bad Design (Violating ISP)**
We create a `Printer` interface with all functionalities.

```java
interface Printer {
    void print();
    void scan();
    void fax();
}
```

Now, different printers need to implement this interface:
1️⃣ **Basic Printer** – Only prints.  
2️⃣ **Advanced Printer** – Prints, scans, and faxes.

```java
class BasicPrinter implements Printer {
    public void print() {
        System.out.println("Printing document...");
    }

    public void scan() {
        throw new UnsupportedOperationException("Basic printer does not support scanning!");
    }

    public void fax() {
        throw new UnsupportedOperationException("Basic printer does not support faxing!");
    }
}
```

### **Problems**
🚨 **Violates ISP because:**
- `BasicPrinter` **is forced to implement `scan()` and `fax()`**, even though it does not support these features.
- Future changes to `Printer` affect **both basic and advanced printers**.

---

### **Good Design (Following ISP)**
We **split the `Printer` interface** into **smaller, focused interfaces**.

```java
// Step 1: Create specific interfaces
interface PrintFunction {
    void print();
}

interface ScanFunction {
    void scan();
}

interface FaxFunction {
    void fax();
}

// Step 2: Implement only relevant interfaces
class BasicPrinter implements PrintFunction {
    public void print() {
        System.out.println("Printing document...");
    }
}

class AdvancedPrinter implements PrintFunction, ScanFunction, FaxFunction {
    public void print() {
        System.out.println("Printing document...");
    }

    public void scan() {
        System.out.println("Scanning document...");
    }

    public void fax() {
        System.out.println("Faxing document...");
    }
}
```

### **Benefits**
✔ **BasicPrinter implements only `PrintFunction` and avoids unnecessary methods.**  
✔ **AdvancedPrinter supports all features.**  
✔ **Easier to modify, extend, and maintain.**  

---

## **Real-Time Use Case 3: Restaurant Ordering System**
### **Bad Design (Violating ISP)**
A restaurant **orders system** has a `RestaurantService` interface.

```java
interface RestaurantService {
    void takeOrder();
    void cookFood();
    void serveCustomer();
    void processPayment();
}
```

### **Problems**
🚨 **Violates ISP because:**
- A **Cashier** must implement `cookFood()` even though they don’t cook.
- A **Chef** must implement `processPayment()` even though they don’t handle money.

---

### **Good Design (Following ISP)**
We split it into **smaller interfaces**.

```java
interface OrderTaker {
    void takeOrder();
}

interface Cook {
    void cookFood();
}

interface Server {
    void serveCustomer();
}

interface Cashier {
    void processPayment();
}

// Employees implement only relevant interfaces
class Waiter implements OrderTaker, Server {
    public void takeOrder() {
        System.out.println("Taking customer order...");
    }

    public void serveCustomer() {
        System.out.println("Serving food to the customer...");
    }
}

class Chef implements Cook {
    public void cookFood() {
        System.out.println("Cooking food...");
    }
}

class CashierEmployee implements Cashier {
    public void processPayment() {
        System.out.println("Processing customer payment...");
    }
}
```

### **Benefits**
✔ **Waiters don’t need to implement `cookFood()` or `processPayment()`.**  
✔ **Chefs focus only on cooking.**  
✔ **Better separation of concerns and maintainability.**  

---

## **Conclusion**
By following **ISP**, we create:
✅ **Smaller, focused interfaces**  
✅ **More modular and reusable code**  
✅ **Easier debugging and maintenance**  
✅ **Better separation of responsibilities**  

By **splitting large interfaces into smaller, more specific ones**, we **avoid unnecessary dependencies and create more scalable systems**. 🚀

# What is Dependency Inversion Principle (DIP)?
# **Dependency Inversion Principle (DIP)**
## **Definition**
The **Dependency Inversion Principle (DIP)** states that:
1. **High-level modules should not depend on low-level modules. Both should depend on abstractions.**
2. **Abstractions should not depend on details. Details should depend on abstractions.**

### **Key Idea of DIP**
- Instead of **directly depending on concrete classes**, we should depend on **interfaces or abstract classes**.
- **Reduces coupling** → Easier to modify, extend, and test.
- **Enhances flexibility** → We can swap implementations without affecting higher-level modules.

---

## **Why is DIP Important?**
Violating DIP leads to:
🚨 **Tightly coupled code** – Hard to replace or modify dependencies.  
🚨 **Difficult unit testing** – Cannot mock dependencies easily.  
🚨 **Low scalability** – Changing implementation requires modifying multiple parts of the system.  

By applying DIP:
✅ **Loosely coupled code** – High flexibility.  
✅ **Easier testing** – Dependency Injection allows mocking.  
✅ **Extensible architecture** – New features can be added easily.  

---

## **Real-Time Example 1: Keyboard and Computer (Bad Design)**
### **Bad Design (Violating DIP)**
A `Computer` directly depends on a **specific** `WiredKeyboard` class.

```java
class WiredKeyboard {
    public void connect() {
        System.out.println("Wired Keyboard connected.");
    }
}

class Computer {
    private WiredKeyboard keyboard;

    public Computer() {
        this.keyboard = new WiredKeyboard();
    }

    public void start() {
        keyboard.connect();
        System.out.println("Computer started.");
    }
}
```

### **Problems**
🚨 **Violates DIP because:**
1. `Computer` is **tightly coupled** with `WiredKeyboard`.  
2. If we want to switch to a `WirelessKeyboard`, we must modify `Computer`.  

---

### **Good Design (Following DIP)**
We introduce **an interface (`Keyboard`)** so `Computer` depends on **abstractions**.

```java
// Step 1: Create an abstraction
interface Keyboard {
    void connect();
}

// Step 2: Implement different keyboards
class WiredKeyboard implements Keyboard {
    public void connect() {
        System.out.println("Wired Keyboard connected.");
    }
}

class WirelessKeyboard implements Keyboard {
    public void connect() {
        System.out.println("Wireless Keyboard connected.");
    }
}

// Step 3: Modify Computer to depend on the interface
class Computer {
    private Keyboard keyboard;

    // Dependency Injection through constructor
    public Computer(Keyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void start() {
        keyboard.connect();
        System.out.println("Computer started.");
    }
}

// Step 4: Using the Computer class
public class Main {
    public static void main(String[] args) {
        Keyboard wired = new WiredKeyboard();
        Computer computer1 = new Computer(wired);
        computer1.start();

        Keyboard wireless = new WirelessKeyboard();
        Computer computer2 = new Computer(wireless);
        computer2.start();
    }
}
```

### **Benefits**
✔ **Computer now works with any `Keyboard` implementation.**  
✔ **We can add `BluetoothKeyboard` without modifying `Computer`.**  
✔ **Reduces code dependency and improves flexibility.**  

---

## **Real-Time Example 2: Payment Processing System**
### **Bad Design (Violating DIP)**
A **PaymentService** directly depends on `CreditCardPayment`.

```java
class CreditCardPayment {
    public void pay() {
        System.out.println("Payment made via Credit Card.");
    }
}

class PaymentService {
    private CreditCardPayment payment;

    public PaymentService() {
        this.payment = new CreditCardPayment();
    }

    public void processPayment() {
        payment.pay();
    }
}
```

### **Problems**
🚨 **Violates DIP because:**
1. `PaymentService` is **tightly coupled** with `CreditCardPayment`.  
2. If we introduce **PayPal, Bitcoin, or Apple Pay**, we must modify `PaymentService`.  

---

### **Good Design (Following DIP)**
We introduce **an interface (`PaymentMethod`)** so `PaymentService` depends on **abstractions**.

```java
// Step 1: Create an abstraction
interface PaymentMethod {
    void pay();
}

// Step 2: Implement multiple payment methods
class CreditCardPayment implements PaymentMethod {
    public void pay() {
        System.out.println("Payment made via Credit Card.");
    }
}

class PayPalPayment implements PaymentMethod {
    public void pay() {
        System.out.println("Payment made via PayPal.");
    }
}

// Step 3: Modify PaymentService to depend on the interface
class PaymentService {
    private PaymentMethod paymentMethod;

    // Dependency Injection through constructor
    public PaymentService(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void processPayment() {
        paymentMethod.pay();
    }
}

// Step 4: Using the PaymentService
public class Main {
    public static void main(String[] args) {
        PaymentMethod creditCard = new CreditCardPayment();
        PaymentService service1 = new PaymentService(creditCard);
        service1.processPayment();

        PaymentMethod paypal = new PayPalPayment();
        PaymentService service2 = new PaymentService(paypal);
        service2.processPayment();
    }
}
```

### **Benefits**
✔ **New payment methods can be added without modifying `PaymentService`.**  
✔ **More modular and testable architecture.**  
✔ **Supports dependency injection.**  

---

## **Real-Time Example 3: Notification System**
### **Bad Design (Violating DIP)**
A **NotificationService** directly depends on `EmailNotification`.

```java
class EmailNotification {
    public void send(String message) {
        System.out.println("Sending Email: " + message);
    }
}

class NotificationService {
    private EmailNotification emailNotification;

    public NotificationService() {
        this.emailNotification = new EmailNotification();
    }

    public void notifyUser(String message) {
        emailNotification.send(message);
    }
}
```

### **Problems**
🚨 **Violates DIP because:**
1. If we want **SMS or Push notifications**, we must modify `NotificationService`.  
2. Tightly coupled design makes maintenance hard.  

---

### **Good Design (Following DIP)**
We introduce **an interface (`Notifier`)** so `NotificationService` depends on **abstractions**.

```java
// Step 1: Create an abstraction
interface Notifier {
    void send(String message);
}

// Step 2: Implement different notification methods
class EmailNotification implements Notifier {
    public void send(String message) {
        System.out.println("Sending Email: " + message);
    }
}

class SMSNotification implements Notifier {
    public void send(String message) {
        System.out.println("Sending SMS: " + message);
    }
}

// Step 3: Modify NotificationService to depend on the interface
class NotificationService {
    private Notifier notifier;

    // Dependency Injection through constructor
    public NotificationService(Notifier notifier) {
        this.notifier = notifier;
    }

    public void notifyUser(String message) {
        notifier.send(message);
    }
}

// Step 4: Using the NotificationService
public class Main {
    public static void main(String[] args) {
        Notifier email = new EmailNotification();
        NotificationService service1 = new NotificationService(email);
        service1.notifyUser("Welcome to our service!");

        Notifier sms = new SMSNotification();
        NotificationService service2 = new NotificationService(sms);
        service2.notifyUser("Your order has been shipped!");
    }
}
```

### **Benefits**
✔ **Easily add new notification types (Push, WhatsApp) without modifying `NotificationService`.**  
✔ **Follows Dependency Inversion Principle by using abstractions.**  
✔ **Better scalability and maintainability.**  

---

## **Conclusion**
By following **DIP**, we create:
✅ **Loosely coupled and extensible systems**  
✅ **Code that is easier to test and maintain**  
✅ **Flexible architectures that adapt to changes easily**  

Using **interfaces and dependency injection**, we ensure that **high-level modules remain independent of low-level details**, making our systems **more modular and scalable**. 🚀