# Java Structural Design Patterns

## Table of Contents

1. [What is Adapter Design Pattern](#what-is-adapter-design-pattern)
    - [What is the Adapter Design Pattern](#what-is-the-adapter-design-pattern)
    - [When to use Adapter Design Pattern](#when-to-use-adapter-design-pattern)
    - [Key Components of Adapter Design Pattern](#key-components-of-adapter-design-pattern)
    - [Different Ways to Implement Adapter Design Pattern](#different-ways-to-implement-adapter-design-pattern)
    - [Adapter Design Pattern Full Example in Java](#adapter-design-pattern-full-example-in-java)
    - [Use Case of Adapter Design Pattern](#use-case-of-adapter-design-pattern)
    - [Realtime Full Example of Adapter Design Pattern](#realtime-full-example-of-adapter-design-pattern)
    - [Pros and Cons of Adapter Design Pattern](#pros-and-cons-of-adapter-design-pattern)

2. [What is Decorator Design Pattern](#what-is-decorator-design-pattern)
    - [What is the Decorator Design Pattern](#what-is-the-decorator-design-pattern)
    - [When to use Decorator Design Pattern](#when-to-use-decorator-design-pattern)
    - [Key Components of Decorator Design Pattern](#key-components-of-decorator-design-pattern)
    - [Different Ways to Implement Decorator Design Pattern](#different-ways-to-implement-decorator-design-pattern)
    - [Decorator Design Pattern Full Example in Java](#decorator-design-pattern-full-example-in-java)
    - [Use Case of Decorator Design Pattern](#use-case-of-decorator-design-pattern)
    - [Realtime Full Example of Decorator Design Pattern](#realtime-full-example-of-decorator-design-pattern)
    - [Pros and Cons of Decorator Design Pattern](#pros-and-cons-of-decorator-design-pattern)

3. [What is Proxy Design Pattern](#what-is-proxy-design-pattern)
    - [What is the Proxy Design Pattern](#what-is-the-proxy-design-pattern)
    - [When to use Proxy Design Pattern](#when-to-use-proxy-design-pattern)
    - [Key Components of Proxy Design Pattern](#key-components-of-proxy-design-pattern)
    - [Different Ways to Implement Proxy Design Pattern](#different-ways-to-implement-proxy-design-pattern)
    - [Proxy Design Pattern Full Example in Java](#proxy-design-pattern-full-example-in-java)
    - [Use Case of Proxy Design Pattern](#use-case-of-proxy-design-pattern)
    - [Realtime Full Example of Proxy Design Pattern](#realtime-full-example-of-proxy-design-pattern)
    - [Pros and Cons of Proxy Design Pattern](#pros-and-cons-of-proxy-design-pattern)

4. [What is Facade Design Pattern](#what-is-facade-design-pattern)
    - [What is the Facade Design Pattern](#what-is-the-facade-design-pattern)
    - [When to use Facade Design Pattern](#when-to-use-facade-design-pattern)
    - [Key Components of Facade Design Pattern](#key-components-of-facade-design-pattern)
    - [Different Ways to Implement Facade Design Pattern](#different-ways-to-implement-facade-design-pattern)
    - [Facade Design Pattern Full Example in Java](#facade-design-pattern-full-example-in-java)
    - [Use Case of Facade Design Pattern](#use-case-of-facade-design-pattern)
    - [Realtime Full Example of Facade Design Pattern](#realtime-full-example-of-facade-design-pattern)
    - [Pros and Cons of Facade Design Pattern](#pros-and-cons-of-facade-design-pattern)

5. [What is Bridge Design Pattern](#what-is-bridge-design-pattern)
    - [What is the Bridge Design Pattern](#what-is-the-bridge-design-pattern)
    - [When to use Bridge Design Pattern](#when-to-use-bridge-design-pattern)
    - [Key Components of Bridge Design Pattern](#key-components-of-bridge-design-pattern)
    - [Different Ways to Implement Bridge Design Pattern](#different-ways-to-implement-bridge-design-pattern)
    - [Bridge Design Pattern Full Example in Java](#bridge-design-pattern-full-example-in-java)
    - [Use Case of Bridge Design Pattern](#use-case-of-bridge-design-pattern)
    - [Realtime Full Example of Bridge Design Pattern](#realtime-full-example-of-bridge-design-pattern)
    - [Pros and Cons of Bridge Design Pattern](#pros-and-cons-of-bridge-design-pattern)

5. [What is Composite Design Pattern](#what-is-composite-design-pattern)
    - [What is the Composite Design Pattern](#what-is-the-composite-design-pattern)
    - [When to use Composite Design Pattern](#when-to-use-composite-design-pattern)
    - [Key Components of Composite Design Pattern](#key-components-of-composite-design-pattern)
    - [Different Ways to Implement Composite Design Pattern](#different-ways-to-implement-composite-design-pattern)
    - [Composite Design Pattern Full Example in Java](#composite-design-pattern-full-example-in-java)
    - [Use Case of Composite Design Pattern](#use-case-of-composite-design-pattern)
    - [Realtime Full Example of Composite Design Pattern](#realtime-full-example-of-composite-design-pattern)
    - [Pros and Cons of Composite Design Pattern](#pros-and-cons-of-composite-design-pattern)

5. [What is Flyweight Design Pattern](#what-is-flyweight-design-pattern)
    - [What is the Flyweight Design Pattern](#what-is-the-flyweight-design-pattern)
    - [When to use Flyweight Design Pattern](#when-to-use-flyweight-design-pattern)
    - [Key Components of Flyweight Design Pattern](#key-components-of-flyweight-design-pattern)
    - [Different Ways to Implement Flyweight Design Pattern](#different-ways-to-implement-flyweight-design-pattern)
    - [Flyweight Design Pattern Full Example in Java](#flyweight-design-pattern-full-example-in-java)
    - [Use Case of Flyweight Design Pattern](#use-case-of-flyweight-design-pattern)
    - [Realtime Full Example of Flyweight Design Pattern](#realtime-full-example-of-flyweight-design-pattern)
    - [Pros and Cons of Flyweight Design Pattern](#pros-and-cons-of-flyweight-design-pattern)

# What is Adapter Design Pattern
### What is the Adapter Design Pattern?

The Adapter Design Pattern is a structural design pattern that allows objects with incompatible interfaces to collaborate. It acts as a bridge between two incompatible interfaces by providing a wrapper class that implements the interface of one class and delegates the calls to the other class. This pattern is useful when you want to use a class that doesn't quite fit the interface you need.

### When to Use Adapter Design Pattern

The Adapter Design Pattern is useful when:

- You want to use an existing class but its interface is not compatible with the rest of your code.
- You want to create a reusable class that cooperates with classes that don't have compatible interfaces.
- You want to decouple the client from the implementation details of an interface.

### Key Components of Adapter Design Pattern

1. **Target:** This is the interface that the client expects.
2. **Adapter:** This is the class that adapts the interface of the Adaptee to the Target interface.
3. **Adaptee:** This is the class that has the interface that needs adapting.
4. **Client:** This is the class that uses the Target interface to interact with the Adaptee.

### Different Ways to Implement Adapter Design Pattern

There are two common ways to implement the Adapter Design Pattern:

1. **Class Adapter:** This uses inheritance to adapt the interface of the Adaptee to the Target interface. In this approach, the Adapter class extends the Adaptee class and implements the Target interface.
2. **Object Adapter:** This uses composition to adapt the interface of the Adaptee to the Target interface. In this approach, the Adapter class contains an instance of the Adaptee class and implements the Target interface by delegating the calls to the Adaptee.

### Adapter Design Pattern Full Example in Java

Here is the UML diagram for the Adapter Design Pattern example with the `MediaPlayer`, `Mp3Player`, `Mp4Player`, and `MediaAdapter` classes:

```plaintext
-----------------------------------------
|              MediaPlayer              |
-----------------------------------------
| + play(audioType: String, fileName: String): void |
-----------------------------------------
             /                      \
            /                        \
           /                          \
-----------------        -----------------
|    Mp3Player   |        |    Mp4Player   |
-----------------        -----------------
| + playMp3(fileName: String): void | | + playMp4(fileName: String): void |
-----------------        -----------------
             \                      /
              \                    /
               \                  /
                    -------------
                    | MediaAdapter |
                    -------------
                    | - mp3Player: Mp3Player |
                    | - mp4Player: Mp4Player |
                    ---------------------------
                    | + play(audioType: String, fileName: String): void |
                    ------------------------------------------------------
```

In this diagram:

- `MediaPlayer` is the target interface that the client code expects.
- `Mp3Player` and `Mp4Player` are the adaptee classes, representing the existing classes with incompatible interfaces.
- `MediaAdapter` is the adapter class that implements the `MediaPlayer` interface and adapts the functionality of the `Mp3Player` and `Mp4Player` classes.
### Use Case of Adapter Design Pattern

- **Integration of New with Existing Code:** When you want to integrate new classes with existing code that expects a certain interface.
- **Legacy Code Reuse:** When you want to reuse legacy code that has an incompatible interface with the rest of your system.
- **Third-party Libraries:** When you want to use third-party libraries or APIs that have interfaces that are not compatible with your application.

### Realtime Full Example of Adapter Design Pattern

Consider a real-time scenario where you have a web application that needs to fetch data from different APIs, each with its own data format.

**Target Interface:**

```java
public interface DataProvider {
    String fetchData();
}
```

**Adaptee Classes:**

```java
public class XmlDataProvider {
    public String fetchDataAsXml() {
        return "<data>This is XML data</data>";
    }
}

public class JsonDataProvider {
    public String fetchDataAsJson() {
        return "{ \"data\": \"This is JSON data\" }";
    }
}
```

**Adapter Classes:**

```java
public class XmlAdapter implements DataProvider {
    private XmlDataProvider xmlDataProvider;

    public XmlAdapter(XmlDataProvider xmlDataProvider) {
        this.xmlDataProvider = xmlDataProvider;
    }

    @Override
    public String fetchData() {
        return xmlDataProvider.fetchDataAsXml();
    }
}

public class JsonAdapter implements DataProvider {
    private JsonDataProvider jsonDataProvider;

    public JsonAdapter(JsonDataProvider jsonDataProvider) {
        this.jsonDataProvider = jsonDataProvider;
    }

    @Override
    public String fetchData() {
        return jsonDataProvider.fetchDataAsJson();
    }
}
```

**Client Code:**

```java
public class DataConsumer {
    private DataProvider dataProvider;

    public void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String fetchData() {
        return dataProvider.fetchData();
    }

    public static void main(String[] args) {
        DataConsumer consumer = new DataConsumer();
        consumer.setDataProvider(new XmlAdapter(new XmlDataProvider()));
        System.out.println(consumer.fetchData());

        consumer.setDataProvider(new JsonAdapter(new JsonDataProvider()));
        System.out.println(consumer.fetchData());
    }
}
```

Here is the UML diagram for the Adapter Design Pattern example you provided:

```
---------------------------------------
|             DataProvider           |
---------------------------------------
| + fetchData(): String              |
---------------------------------------
             /         \
            /           \
           /             \
---------------------------- ----------------------------
|       XmlAdapter       | |       JsonAdapter       |
---------------------------- ----------------------------
| - xmlDataProvider: XmlDataProvider| | - jsonDataProvider: JsonDataProvider |
---------------------------- ----------------------------
| + XmlAdapter(XmlDataProvider)   | | + JsonAdapter(JsonDataProvider)   |
| + fetchData(): String            | | + fetchData(): String            |
---------------------------- ----------------------------
            /                 \
           /                   \
          /                     \
-------------------    -------------------
| XmlDataProvider |    | JsonDataProvider |
-------------------    -------------------
| + fetchDataAsXml(): String | | + fetchDataAsJson(): String |
-------------------    -------------------
```

In this diagram:

- `DataProvider` is the target interface that the client code expects.
- `XmlDataProvider` and `JsonDataProvider` are the adaptee classes, representing the existing classes with incompatible interfaces.
- `XmlAdapter` and `JsonAdapter` are the adapter classes that implement the `DataProvider` interface and adapt the functionality of the adaptee classes.
- `DataConsumer` is the client class that uses the adapter classes to fetch data in the expected format.

### Pros and Cons of Adapter Design Pattern

**Pros:**

1. **Flexibility:** Allows objects with incompatible interfaces to work together.
2. **Reusability:** Promotes the reuse of existing classes without modifying their code.
3. **Decoupling:** Decouples the client code from the implementation details of the adaptee.
4. **Simplicity:** Provides a simple solution to integrate new classes with existing code.

**Cons:**

1. **Complexity:** Introduces additional complexity with the need for adapter classes.
2. **Overhead:** Can add overhead, especially if many adapters are used in the application.
3. **Limited Use:** Not suitable for every scenario, especially when the interfaces are vastly different.

# What is Decorator Design Pattern
### What is the Decorator Design Pattern?

The Decorator Design Pattern is a structural design pattern that allows behavior to be added to individual objects, dynamically, without affecting the behavior of other objects from the same class. It is used to extend or alter the functionality of objects at runtime, by wrapping them in an object of a decorator class.

### When to Use Decorator Design Pattern

The Decorator Design Pattern is useful when:

- You need to add functionality to individual objects dynamically and transparently.
- You want to avoid subclassing to add functionality.
- You need to add or remove responsibilities from objects at runtime.
- You want to combine behaviors that are independent of each other.

### Key Components of Decorator Design Pattern

1. **Component:** Defines the interface for objects that can have responsibilities added to them dynamically.
2. **ConcreteComponent:** Implements the Component interface and defines the base behavior to which additional responsibilities can be added.
3. **Decorator:** Maintains a reference to a Component object and defines an interface that conforms to the Component interface.
4. **ConcreteDecorator:** Adds responsibilities to the component.

### Different Ways to Implement Decorator Design Pattern

There are two common ways to implement the Decorator Design Pattern:

1. **Composition:** In this approach, the decorator and the component implement the same interface, and the decorator contains an instance of the component. The decorator delegates all operations to the component, adding its own behavior before or after the delegation.
2. **Inheritance:** In this approach, the decorator extends the component class, adding its own behavior before or after calling the parent component's methods. However, this approach is less flexible than the composition approach because it requires subclassing for each combination of behaviors.

### Decorator Design Pattern Full Example in Java

Here is the UML diagram for the Decorator Design Pattern example you provided:

```plaintext
-----------------------------------
|              Pizza              |
-----------------------------------
| + getDescription(): String     |
| + getCost(): double            |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|  PlainPizza   |   | PizzaDecorator |
-----------------   -----------------
| + getDescription(): String | | - pizza: Pizza |
| + getCost(): double        | -----------------
-----------------            | + getDescription(): String |
                             | + getCost(): double        |
                             -----------------------------
                                        ^
                                        |
                         -----------------------------
                         |                           |
             ------------------          ------------------
             |   CheeseDecorator |          | PepperoniDecorator|
             ------------------          ------------------
             | - pizza: Pizza    |          | - pizza: Pizza    |
             ------------------          ------------------
             | + getDescription(): String | | + getDescription(): String |
             | + getCost(): double        | | + getCost(): double        |
             -----------------------------   -----------------------------
```

In this diagram:

- `Pizza` is the component interface that defines the methods for getting the description and cost of a pizza.
- `PlainPizza` is a concrete component that implements the `Pizza` interface.
- `PizzaDecorator` is the abstract decorator class that implements the `Pizza` interface and contains a reference to a `Pizza` object.
- `CheeseDecorator` and `PepperoniDecorator` are concrete decorators that extend `PizzaDecorator` and add functionality (description and cost) to the pizza object they wrap.
### Use Case of Decorator Design Pattern

- **Adding Features to Text:** Adding features like bold, italic, underline to text in a word processor.
- **Enhancing Graphical Objects:** Adding borders, shadows, and other effects to graphical objects in a graphics editor.
- **Filtering Streams:** Adding encryption, compression, or buffering to streams.
- **Dynamic Configuration:** Adding or removing options dynamically in a configuration system.

### Realtime Full Example of Decorator Design Pattern

Consider a real-time example where you have a car rental system that allows customers to rent cars with different features.

**Component Interface:**

```java
public interface Car {
    String getDescription();
    double getCost();
}
```

**Concrete Component:**

```java
public class BasicCar implements Car {
    @Override
    public String getDescription() {
        return "Basic Car";
    }

    @Override
    public double getCost() {
        return 200.00;
    }
}
```

**Decorator Classes:**

```java
public abstract class CarDecorator implements Car {
    protected Car car;

    public CarDecorator(Car car) {
        this.car = car;
    }

    @Override
    public String getDescription() {
        return car.getDescription();
    }

    @Override
    public double getCost() {
        return car.getCost();
    }
}

public class SportsPackage extends CarDecorator {
    public SportsPackage(Car car) {
        super(car);
    }

    @Override
    public String getDescription() {
        return car.getDescription() + ", Sports Package";
    }

    @Override
    public double getCost() {
        return car.getCost() + 100.00;
    }
}

public class LuxuryPackage extends CarDecorator {
    public LuxuryPackage(Car car) {
        super(car);
    }

    @Override
    public String getDescription() {
        return car.getDescription() + ", Luxury Package";
    }

    @Override
    public double getCost() {
        return car.getCost() + 500.00;
    }
}
```

**Client Code:**

```java
public class CarRentalShop {
    public static void main(String[] args) {
        Car car = new BasicCar();
        System.out.println("Description: " + car.getDescription() + ", Cost: $" + car.getCost());

        Car sportsCar = new SportsPackage(car);
        System.out.println("Description: " + sportsCar.getDescription() + ", Cost: $" + sportsCar.getCost());

        Car luxuryCar = new LuxuryPackage(car);
        System.out.println("Description: " + luxuryCar.getDescription() + ", Cost: $" + luxuryCar.getCost());

        Car luxurySportsCar = new LuxuryPackage(new SportsPackage(car));
        System.out.println("Description: " + luxurySportsCar.getDescription() + ", Cost: $" + luxurySportsCar.getCost());
    }
}
```
Here is the UML diagram for the Decorator Design Pattern example with the `Car`, `BasicCar`, `CarDecorator`, `SportsPackage`, and `LuxuryPackage` classes:

```plaintext
-----------------------------------
|              Car                |
-----------------------------------
| + getDescription(): String     |
| + getCost(): double            |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|   BasicCar    |   |  CarDecorator  |
-----------------   -----------------
| + getDescription(): String | | - car: Car     |
| + getCost(): double        | -----------------
-----------------            | + getDescription(): String |
                             | + getCost(): double        |
                             -----------------------------
                                        ^
                                        |
                   -----------------------------
                   |                           |
         ------------------        ------------------
         |   SportsPackage  |        |  LuxuryPackage  |
         ------------------        ------------------
         | - car: Car       |        | - car: Car       |
         ------------------        ------------------
         | + getDescription(): String | | + getDescription(): String |
         | + getCost(): double        | | + getCost(): double        |
         -----------------------------   -----------------------------
```

In this diagram:

- `Car` is the component interface that defines the methods for getting the description and cost of a car.
- `BasicCar` is a concrete component that implements the `Car` interface.
- `CarDecorator` is the abstract decorator class that implements the `Car` interface and contains a reference to a `Car` object.
- `SportsPackage` and `LuxuryPackage` are concrete decorators that extend `CarDecorator` and add functionality (description and cost) to the car object they wrap.

### Pros and Cons of Decorator Design Pattern

**Pros:**

1. **Flexibility:** Allows adding new behaviors or modifying existing behaviors of individual objects without affecting other objects.
2. **Open/Closed Principle:** Allows the addition of new decorators without modifying existing code, thus following the open/closed principle.
3. **Easy to Implement:** Provides a simple and easy-to-understand way to add responsibilities to objects dynamically.

**Cons:**

1. **Complexity:** Can lead to a large number of small classes if
# What is Proxy Design Pattern
### What is the Proxy Design Pattern?

The Proxy Design Pattern is a structural design pattern that provides a surrogate or placeholder for another object to control access to it. It allows you to create a class (the proxy) that has the same interface as the real object, which enables you to substitute the proxy for the real object in your code. The proxy acts as an intermediary, controlling access to the real object and allowing you to add extra functionality, such as caching, logging, or access control, without changing the real object's code.

### When to Use Proxy Design Pattern

The Proxy Design Pattern is useful when:

- You want to control access to an object, such as limiting the number of instances that can be created or checking permissions before allowing access.
- You need to add extra functionality to an object without changing its code, such as logging, caching, or lazy initialization.
- You want to defer the creation and initialization of an object until it is actually needed, which can improve performance.

### Key Components of Proxy Design Pattern

1. **Subject:** Defines the common interface for the RealSubject and Proxy so that a Proxy can be used anywhere a RealSubject is expected.
2. **RealSubject:** The real object that the Proxy represents and controls access to.
3. **Proxy:** The surrogate object that controls access to the RealSubject and may be used to add additional functionality.

### Different Ways to Implement Proxy Design Pattern

There are several ways to implement the Proxy Design Pattern:

1. **Virtual Proxy:** Defers the creation and initialization of the RealSubject until it is actually needed.
2. **Remote Proxy:** Provides a local representative for an object in a different address space, typically used in distributed systems.
3. **Protection Proxy:** Controls access to the RealSubject based on access permissions.
4. **Smart Proxy:** Adds additional functionality, such as logging or caching, to the access of the RealSubject.

### Proxy Design Pattern Full Example in Java

**Subject Interface:**

```java
public interface Image {
    void display();
}
```

**RealSubject Class:**

```java
public class RealImage implements Image {
    private final String filename;

    public RealImage(String filename) {
        this.filename = filename;
        loadImageFromDisk();
    }

    private void loadImageFromDisk() {
        System.out.println("Loading " + filename);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + filename);
    }
}
```

**Proxy Class:**

```java
public class ProxyImage implements Image {
    private RealImage realImage;
    private final String filename;

    public ProxyImage(String filename) {
        this.filename = filename;
    }

    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(filename);
        }
        realImage.display();
    }
}
```

**Client Code:**

```java
public class ProxyPatternDemo {
    public static void main(String[] args) {
        Image image1 = new ProxyImage("image1.jpg");
        Image image2 = new ProxyImage("image2.jpg");

        // Image1 will be loaded from disk
        image1.display();
        // Image1 will not be loaded from disk again
        image1.display();
        // Image2 will be loaded from disk
        image2.display();
    }
}
```
Here is the UML diagram for the Proxy Design Pattern example with the `Image`, `RealImage`, and `ProxyImage` classes:

```plaintext
-----------------------------------
|              Image              |
-----------------------------------
| + display(): void              |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|   RealImage    |   |  ProxyImage    |
-----------------   -----------------
| - filename: String | | - realImage: RealImage |
-----------------   | - filename: String       |
| + display(): void | -----------------
-----------------   | + display(): void       |
                    -------------------------
```

In this diagram:

- `Image` is the subject interface that defines the `display` method.
- `RealImage` is the real subject class that implements the `Image` interface and loads and displays the image from disk.
- `ProxyImage` is the proxy class that also implements the `Image` interface and controls access to the `RealImage` object, loading it from disk only when necessary.

### Use Case of Proxy Design Pattern

- **Lazy Initialization:** When you want to defer the creation and initialization of an object until it is actually needed.
- **Access Control:** When you want to control access to an object, such as limiting the number of instances that can be created or checking permissions before allowing access.
- **Logging:** When you want to log access to an object, such as when it is created or accessed.
- **Caching:** When you want to cache the results of expensive operations, such as database queries, to improve performance.

### Realtime Full Example of Proxy Design Pattern

Sure, let's consider a real-time example of a proxy for accessing sensitive information in a secure system. In this example, we'll create an interface for accessing the sensitive information, a concrete implementation of the interface for the real access, and a proxy class to control access to the sensitive information.

**Interface for accessing sensitive information:**

```java
public interface SensitiveInformation {
    void accessInformation();
}
```

**Concrete implementation of the interface:**

```java
public class RealSensitiveInformation implements SensitiveInformation {
    private final String username;
    private final String password;

    public RealSensitiveInformation(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public void accessInformation() {
        System.out.println("Accessing sensitive information with username: " + username + " and password: " + password);
    }
}
```

**Proxy class for controlling access to sensitive information:**

```java
public class SensitiveInformationProxy implements SensitiveInformation {
    private final String username;
    private final String password;
    private RealSensitiveInformation realSensitiveInformation;

    public SensitiveInformationProxy(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public void accessInformation() {
        if (realSensitiveInformation == null) {
            realSensitiveInformation = new RealSensitiveInformation(username, password);
        }
        realSensitiveInformation.accessInformation();
    }
}
```

**Client code using the proxy to access sensitive information:**

```java
public class ProxyPatternDemo {
    public static void main(String[] args) {
        SensitiveInformation proxy = new SensitiveInformationProxy("admin", "password");
        proxy.accessInformation();
    }
}
```

In this example, the `SensitiveInformationProxy` controls access to the `RealSensitiveInformation` object. It checks if the real object has been created and if not, creates it using the provided username and password. This way, the proxy can restrict access to the sensitive information based on certain conditions, such as authentication, before allowing the real object to be accessed.

Here is the UML diagram for the real-time example of the Proxy Design Pattern:

```plaintext
-----------------------------------
|     SensitiveInformation        |
-----------------------------------
| + accessInformation(): void    |
-----------------------------------
               ^
               |
          ------------------
          |                |
-----------------   -----------------
| RealSensitiveInformation | ProxySensitiveInformation |
-----------------   -----------------
| - username: String        | - username: String          |
| - password: String        | - password: String          |
-----------------   -----------------
| + accessInformation(): void| + accessInformation(): void |
-----------------   -----------------
```

In this diagram:

- `SensitiveInformation` is the interface that declares the `accessInformation` method for accessing sensitive information.
- `RealSensitiveInformation` is the real subject class that implements the `SensitiveInformation` interface and represents the actual access to sensitive information.
- `ProxySensitiveInformation` is the proxy class that also implements the `SensitiveInformation` interface and controls access to the `RealSensitiveInformation` object, creating it only when necessary.

### Pros and Cons of Proxy Design Pattern

**Pros:**

1. **Controlled Access:** Allows you to control access to the real object, such as limiting the number of instances that can be created or checking permissions before allowing access.
2. **Flexibility:** Allows you to add additional functionality to the real object without changing its code, such as logging, caching, or lazy initialization.
3. **Improved Performance:** Can improve performance by deferring the creation and initialization of the real object until it is actually needed.

**Cons:**

1. **Complexity:** Can add complexity to your code, especially if you have multiple proxies with different behaviors.
2. **Overhead:** Can introduce overhead, especially if the proxy needs to perform additional processing before delegating to the real object.
3. **Increased Code Size:** Can increase the size of your codebase, especially if you have many proxies with different behaviors.
# What is Facade Design Pattern
### What is the Facade Design Pattern?

The Facade Design Pattern is a structural design pattern that provides a simplified interface to a complex subsystem of classes, making it easier to use. It acts as a facade or entry point to a group of interfaces in a subsystem, providing a single unified interface for a set of interfaces in a subsystem.

### When to Use Facade Design Pattern

The Facade Design Pattern is useful when:

- You want to provide a simple interface to a complex subsystem to make it easier to use.
- You need to decouple a client from the complex subsystem to improve modularity and maintainability.
- You want to define an interface to a set of interfaces in a subsystem to make it easier to understand and use.

### Key Components of Facade Design Pattern

1. **Facade:** Provides a simple interface to the complex subsystem. It delegates client requests to appropriate objects in the subsystem.
2. **Subsystem Classes:** Classes that implement the functionality of the subsystem. The facade delegates requests to these classes.

### Different Ways to Implement Facade Design Pattern

The Facade Design Pattern can be implemented in various ways, but the most common approach is to create a single facade class that provides a simple interface to a complex subsystem. This facade class delegates requests to the appropriate classes in the subsystem.

### Facade Design Pattern Full Example in Java

**Subsystem Classes:**

```java
class CPU {
    public void freeze() { System.out.println("CPU freeze"); }
    public void jump(long position) { System.out.println("CPU jump"); }
    public void execute() { System.out.println("CPU execute"); }
}

class Memory {
    public void load(long position, byte[] data) { System.out.println("Memory load"); }
}

class HardDrive {
    public byte[] read(long lba, int size) { System.out.println("HardDrive read"); return null; }
}
```

**Facade Class:**

```java
class ComputerFacade {
    private final CPU cpu;
    private final Memory memory;
    private final HardDrive hardDrive;

    public ComputerFacade() {
        this.cpu = new CPU();
        this.memory = new Memory();
        this.hardDrive = new HardDrive();
    }

    public void start() {
        cpu.freeze();
        memory.load(0, hardDrive.read(0, 1024));
        cpu.jump(0);
        cpu.execute();
    }
}
```

**Client Code:**

```java
public class Main {
    public static void main(String[] args) {
        ComputerFacade computerFacade = new ComputerFacade();
        computerFacade.start();
    }
}
```
Here is the UML diagram for the Facade Design Pattern example you provided:

```plaintext
-----------------------------------
|          ComputerFacade         |
-----------------------------------
| - cpu: CPU                     |
| - memory: Memory               |
| - hardDrive: HardDrive         |
-----------------------------------
| + start(): void                |
-----------------------------------
            |
            | uses
            |
-----------------------------------
|               CPU               |
-----------------------------------
| + freeze(): void               |
| + jump(position: long): void  |
| + execute(): void              |
-----------------------------------
            |
            | uses
            |
-----------------------------------
|             Memory             |
-----------------------------------
| + load(position: long, data: byte[]): void |
-----------------------------------
            |
            | uses
            |
-----------------------------------
|           HardDrive            |
-----------------------------------
| + read(lba: long, size: int): byte[] |
-----------------------------------
```

In this diagram:

- `ComputerFacade` is the facade class that provides a simplified interface to start the computer. It uses the `CPU`, `Memory`, and `HardDrive` subsystem classes to perform the startup process.
- `CPU`, `Memory`, and `HardDrive` are the subsystem classes that perform specific tasks related to the computer's startup process.

### Use Case of Facade Design Pattern

- **Simplifying Complex Systems:** When you have a complex system with many interacting components and you want to provide a simplified interface to it.
- **Providing a Unified Interface:** When you want to provide a single entry point to a group of interfaces in a subsystem to make it easier to understand and use.
- **Hiding Implementation Details:** When you want to hide the complexities of a subsystem and provide a simple interface to clients.

### Realtime Full Example of Facade Design Pattern

Consider a real-time example of a home theater system with multiple components such as DVD player, projector, and lights. The Facade Design Pattern can be used to provide a simple interface to control the entire home theater system.

**Subsystem Classes:**

Assume classes for DVD player, projector, and lights are defined.

**Facade Class:**

```java
class HomeTheaterFacade {
    private final DVDPlayer dvdPlayer;
    private final Projector projector;
    private final Lights lights;

    public HomeTheaterFacade(DVDPlayer dvdPlayer, Projector projector, Lights lights) {
        this.dvdPlayer = dvdPlayer;
        this.projector = projector;
        this.lights = lights;
    }

    public void watchMovie(String movie) {
        lights.dim();
        projector.on();
        dvdPlayer.play(movie);
    }

    public void endMovie() {
        lights.on();
        projector.off();
        dvdPlayer.stop();
    }
}
```

**Client Code:**

```java
public class Main {
    public static void main(String[] args) {
        DVDPlayer dvdPlayer = new DVDPlayer();
        Projector projector = new Projector();
        Lights lights = new Lights();

        HomeTheaterFacade homeTheater = new HomeTheaterFacade(dvdPlayer, projector, lights);
        homeTheater.watchMovie("Inception");
        homeTheater.endMovie();
    }
}
```

Here is the UML diagram for the real-time example of the Facade Design Pattern for a home theater system:

```plaintext
-----------------------------------
|       HomeTheaterFacade         |
-----------------------------------
| - dvdPlayer: DVDPlayer         |
| - projector: Projector         |
| - lights: Lights               |
-----------------------------------
| + HomeTheaterFacade(dvdPlayer: DVDPlayer, projector: Projector, lights: Lights) |
| + watchMovie(movie: String): void |
| + endMovie(): void              |
-----------------------------------
            |
            | uses
            |
-----------------------------------
|           DVDPlayer            |
-----------------------------------
| + play(movie: String): void   |
| + stop(): void                |
-----------------------------------
            |
            | uses
            |
-----------------------------------
|           Projector            |
-----------------------------------
| + on(): void                  |
| + off(): void                 |
-----------------------------------
            |
            | uses
            |
-----------------------------------
|            Lights              |
-----------------------------------
| + dim(): void                 |
| + on(): void                  |
-----------------------------------
```

In this diagram:

- `HomeTheaterFacade` is the facade class that provides a simplified interface to control the home theater system. It uses the `DVDPlayer`, `Projector`, and `Lights` subsystem classes to perform actions such as watching a movie and ending the movie.
- `DVDPlayer`, `Projector`, and `Lights` are the subsystem classes that perform specific tasks related to the home theater system, such as playing a DVD, controlling the projector, and adjusting the lights.

### Pros and Cons of Facade Design Pattern

**Pros:**

1. **Simplifies Client Interface:** Provides a simple interface to a complex subsystem, making it easier to use.
2. **Decouples Clients from Subsystem:** Reduces the dependencies between clients and the subsystem, improving maintainability and flexibility.
3. **Improves Modularity:** Promotes a more modular design by encapsulating the subsystem behind a facade.

**Cons:**

1. **Limited Flexibility:** The facade may not provide all the features of the subsystem, limiting the flexibility of clients.
2. **Potential Performance Overhead:** The use of a facade may introduce some performance overhead, especially if the facade needs to delegate to multiple subsystem components.
# What is Bridge Design Pattern
### What is the Bridge Design Pattern?

The Bridge Design Pattern is a structural design pattern that decouples an abstraction from its implementation so that the two can vary independently. It allows the abstraction and the implementation to be developed independently and then composed at runtime. This pattern is useful when you want to avoid a permanent binding between an abstraction and its implementation, allowing them to vary independently.

### When to Use Bridge Design Pattern

The Bridge Design Pattern is useful when:

- You want to avoid a permanent binding between an abstraction and its implementation.
- You need to support multiple implementations of an abstraction and want to switch between them at runtime.
- You want to share an implementation among multiple objects.

### Key Components of Bridge Design Pattern

1. **Abstraction:** Defines the interface for the abstraction and maintains a reference to an object of type Implementor.
2. **Refined Abstraction:** Extends the abstraction, providing additional functionality based on the Implementor interface.
3. **Implementor:** Defines the interface for the implementation classes.
4. **Concrete Implementor:** Implements the Implementor interface and defines its concrete implementation.

### Different Ways to Implement Bridge Design Pattern

There are two common ways to implement the Bridge Design Pattern:

1. **Composition:** In this approach, the Abstraction class contains a reference to an Implementor object and delegates all calls to it.
2. **Inheritance:** In this approach, the Abstraction class and the Implementor interface are both abstract, and the ConcreteAbstraction and ConcreteImplementor classes provide the concrete implementations.

### Bridge Design Pattern Full Example in Java

**Implementor Interface:**

```java
public interface Color {
    void applyColor();
}
```

**Concrete Implementor Classes:**

```java
public class RedColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Applying red color");
    }
}

public class GreenColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("Applying green color");
    }
}
```

**Abstraction Class:**

```java
public abstract class Shape {
    protected Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract void applyColor();
}
```

**Refined Abstraction Classes:**

```java
public class Square extends Shape {
    public Square(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Square filled with color ");
        color.applyColor();
    }
}

public class Circle extends Shape {
    public Circle(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Circle filled with color ");
        color.applyColor();
    }
}
```

**Client Code:**

```java
public class BridgePatternDemo {
    public static void main(String[] args) {
        Shape redSquare = new Square(new RedColor());
        redSquare.applyColor();

        Shape greenCircle = new Circle(new GreenColor());
        greenCircle.applyColor();
    }
}
```
Here is the UML diagram for the Bridge Design Pattern example you provided:

```plaintext
-----------------------------------
|              Color              |
-----------------------------------
| + applyColor(): void           |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|    RedColor    |   |   GreenColor   |
-----------------   -----------------
| + applyColor(): void | | + applyColor(): void |
-----------------   -----------------
                ^
                |
-----------------------------------
|             Shape               |
-----------------------------------
| - color: Color                 |
-----------------------------------
| + applyColor(): void           |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|     Square      |   |     Circle      |
-----------------   -----------------
| + applyColor(): void | | + applyColor(): void |
-----------------   -----------------
```

In this diagram:

- `Color` is the implementor interface that defines the method `applyColor`.
- `RedColor` and `GreenColor` are concrete implementor classes that implement the `Color` interface.
- `Shape` is the abstraction class that has a reference to a `Color` object and defines the method `applyColor`.
- `Square` and `Circle` are refined abstraction classes that extend `Shape` and implement the `applyColor` method specific to their shape.
### Use Case of Bridge Design Pattern

- **Graphic User Interface (GUI) Toolkits:** GUI frameworks often use the Bridge Design Pattern to provide different looks (implementations) for widgets (abstractions) that can vary independently.
- **Database Drivers:** Database drivers can use the Bridge Design Pattern to provide different implementations for different database management systems while keeping a common interface for database access.

### Realtime Full Example of Bridge Design Pattern

Consider a real-time example of a vehicle manufacturing system where vehicles can be assembled with different types of engines.

**Implementor Interface:**

```java
public interface Engine {
    void start();
}

```

**Concrete Implementor Classes:**

```java
public class ElectricEngine implements Engine {
    @Override
    public void start() {
        System.out.println("Electric engine started");
    }
}

public class DieselEngine implements Engine {
    @Override
    public void start() {
        System.out.println("Diesel engine started");
    }
}
```

**Abstraction Class:**

```java
public abstract class Vehicle {
    protected Engine engine;

    public Vehicle(Engine engine) {
        this.engine = engine;
    }

    public abstract void startEngine();
}
```

**Refined Abstraction Classes:**

```java
public class Car extends Vehicle {
    public Car(Engine engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.print("Car ");
        engine.start();
    }
}

public class Truck extends Vehicle {
    public Truck(Engine engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.print("Truck ");
        engine.start();
    }
}
```

**Client Code:**

```java
public class VehicleManufacturer {
    public static void main(String[] args) {
        Vehicle car = new Car(new ElectricEngine());
        car.startEngine();

        Vehicle truck = new Truck(new DieselEngine());
        truck.startEngine();
    }
}
```
Here is the UML diagram for the real-time example of the Bridge Design Pattern for a vehicle manufacturing system:

```plaintext
-----------------------------------
|             Engine              |
-----------------------------------
| + start(): void                |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
| ElectricEngine |   |  DieselEngine  |
-----------------   -----------------
| + start(): void  | | + start(): void |
-----------------   -----------------
                ^
                |
-----------------------------------
|            Vehicle              |
-----------------------------------
| - engine: Engine               |
-----------------------------------
| + startEngine(): void          |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|       Car       |   |      Truck      |
-----------------   -----------------
| + startEngine(): void | | + startEngine(): void |
-----------------   -----------------
```

In this diagram:

- `Engine` is the implementor interface that defines the method `start`.
- `ElectricEngine` and `DieselEngine` are concrete implementor classes that implement the `Engine` interface.
- `Vehicle` is the abstraction class that has a reference to an `Engine` object and defines the method `startEngine`.
- `Car` and `Truck` are refined abstraction classes that extend `Vehicle` and implement the `startEngine` method specific to their vehicle type.

### Pros and Cons of Bridge Design Pattern

**Pros:**

1. **Decouples Abstraction and Implementation:** Allows the abstraction and implementation to vary independently.
2. **Improved Extensibility:** Makes it easy to add new abstractions and implementations without changing the existing code.
3. **Improved Maintainability:** Reduces the complexity of the code by separating the abstraction and implementation into separate hierarchies.

**Cons:**

1. **Increased Complexity:** Introduces additional complexity, especially when there are multiple levels of abstraction and implementation.
2. **Overhead:** Can introduce some overhead due to the need for additional classes and indirection.
3. **Design Upfront:** Requires careful design upfront to define the abstraction and implementation hierarchies.
# What is Composite Design Pattern
### What is the Composite Design Pattern?

The Composite Design Pattern is a structural design pattern that lets you compose objects into tree-like structures to represent part-whole hierarchies. It allows clients to treat individual objects and compositions of objects uniformly. In other words, it enables you to work with complex tree structures of objects in a similar way as you would with individual objects.

### When to Use Composite Design Pattern

The Composite Design Pattern is useful when:

- You want clients to be able to treat individual objects and compositions of objects uniformly.
- You need to represent part-whole hierarchies of objects.
- You want to be able to add and remove objects dynamically from the hierarchy.

### Key Components of Composite Design Pattern

1. **Component:** Declares the interface for objects in the composition and implements default behavior for the interface common to all classes.
2. **Leaf:** Represents leaf objects in the composition. It implements behavior for primitive objects in the composition.
3. **Composite:** Represents composite objects in the composition. It stores child components and implements behavior related to objects in the composition.

### Different Ways to Implement Composite Design Pattern

There are two common ways to implement the Composite Design Pattern:

1. **Using a Single Interface:** Define a single interface for both leaf and composite objects. Leaf objects implement this interface directly, while composite objects implement it and contain a collection of child components.
2. **Using Inheritance:** Define a base class for both leaf and composite objects. Leaf objects inherit from this base class and do not contain child components, while composite objects also inherit from this base class and contain child components.

### Composite Design Pattern Full Example in Java

**Component Interface:**

```java
public interface Employee {
    void showDetails();
}
```

**Leaf Class:**

```java
public class Developer implements Employee {
    private final String name;

    public Developer(String name) {
        this.name = name;
    }

    @Override
    public void showDetails() {
        System.out.println("Developer: " + name);
    }
}
```

**Composite Class:**

```java
import java.util.ArrayList;
import java.util.List;

public class Manager implements Employee {
    private final String name;
    private final List<Employee> employees = new ArrayList<>();

    public Manager(String name) {
        this.name = name;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    @Override
    public void showDetails() {
        System.out.println("Manager: " + name);
        for (Employee employee : employees) {
            employee.showDetails();
        }
    }
}
```

**Client Code:**

```java
public class Company {
    public static void main(String[] args) {
        Employee dev1 = new Developer("John Doe");
        Employee dev2 = new Developer("Jane Smith");

        Manager manager = new Manager("Alice Brown");
        manager.addEmployee(dev1);
        manager.addEmployee(dev2);

        manager.showDetails();
    }
}
```

### Use Case of Composite Design Pattern

- **Graphic User Interface (GUI) Components:** GUI frameworks often use the Composite Design Pattern to represent complex layouts, where individual components (leaf nodes) are nested within containers (composite nodes).
- **File System Structure:** File systems can be represented using the Composite Design Pattern, where directories (composite nodes) can contain both files (leaf nodes) and other directories.

### Realtime Full Example of Composite Design Pattern
Sure, let's consider a real-time example of a company's organizational structure using the Composite Design Pattern. We'll create an interface for both individual employees and groups of employees (departments or teams), and then use the Composite pattern to represent the entire organization.

**Component Interface:**

```java
public interface Employee {
    void showDetails();
}
```

**Leaf Class (Individual Employee):**

```java
public class Developer implements Employee {
    private final String name;

    public Developer(String name) {
        this.name = name;
    }

    @Override
    public void showDetails() {
        System.out.println("Developer: " + name);
    }
}
```

**Composite Class (Group of Employees):**

```java
import java.util.ArrayList;
import java.util.List;

public class Manager implements Employee {
    private final String name;
    private final List<Employee> employees = new ArrayList<>();

    public Manager(String name) {
        this.name = name;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    @Override
    public void showDetails() {
        System.out.println("Manager: " + name);
        for (Employee employee : employees) {
            employee.showDetails();
        }
    }
}
```

**Client Code (Creating and Using the Organization Structure):**

```java
public class Organization {
    public static void main(String[] args) {
        Developer dev1 = new Developer("John Doe");
        Developer dev2 = new Developer("Jane Smith");

        Manager manager = new Manager("Alice Brown");
        manager.addEmployee(dev1);
        manager.addEmployee(dev2);

        Manager seniorManager = new Manager("Bob White");
        seniorManager.addEmployee(manager);

        seniorManager.showDetails();
    }
}
```

In this example, the `Manager` class acts as a composite, as it can contain both individual `Developer` objects and other `Manager` objects. The `showDetails` method of the `Manager` class recursively calls the `showDetails` method of each employee in its list, effectively displaying the entire organizational structure.

Here is the UML diagram for the Composite Design Pattern example:

```
-----------------------------------------
|               Employee                |
-----------------------------------------
| + showDetails(): void                |
-----------------------------------------
                      ^
                      |
          ------------------------
          |                      |
          |                      |
  -----------------      -----------------
  |    Developer   |      |     Manager    |
  -----------------      -----------------
  | - name: String |      | - name: String |
  -----------------      | - employees    |
  | + Developer()  |      -----------------
  | + showDetails()|      | + Manager()    |
  -----------------      | + addEmployee()|
                          | + showDetails()|
                          -----------------
```

In this diagram, `Employee` is the component interface implemented by both `Developer` and `Manager`. `Developer` is a leaf node in the composite structure, representing an individual employee. `Manager` is a composite that can contain both individual employees (`Developer`) and other managers (`Manager`). The `Manager` class has a list of employees to manage, and its `showDetails` method recursively calls `showDetails` on each employee in its list.

### Pros and Cons of Composite Design Pattern

**Pros:**

1. **Simplifies Client Code:** Allows clients to treat individual objects and compositions of objects uniformly, simplifying client code.
2. **Flexibility:** Provides flexibility in representing part-whole hierarchies of objects and allows for the dynamic addition and removal of objects from the hierarchy.
3. **Maintainability:** Improves maintainability by allowing you to work with complex tree structures of objects in a similar way as you would with individual objects.

**Cons:**

1. **Complexity:** Can introduce complexity, especially in cases where the hierarchy is deep or the behavior of composite and leaf objects differs significantly.
2. **Performance Overhead:** May introduce some performance overhead due to the recursive nature of operations on composite objects.

# What is Flyweight Design Pattern
### What is the Flyweight Design Pattern?

The Flyweight Design Pattern is a structural design pattern that aims to minimize memory usage and improve performance by sharing objects that are used repeatedly. It achieves this by splitting the object's state into intrinsic (shared) and extrinsic (unique) states. The intrinsic state is shared among multiple objects and is stored in the flyweight object, while the extrinsic state is passed to the flyweight objects when they are used.

### When to Use Flyweight Design Pattern

The Flyweight Design Pattern is useful when:

- You need to create a large number of similar objects that can be shared and reused.
- The objects have intrinsic (shared) and extrinsic (unique) states, and the intrinsic state can be shared among multiple objects.

### Key Components of Flyweight Design Pattern

1. **Flyweight:** Declares an interface or abstract class for flyweight objects.
2. **ConcreteFlyweight:** Implements the Flyweight interface and stores the intrinsic state of the flyweight.
3. **FlyweightFactory:** Creates and manages flyweight objects. It ensures that flyweights are shared and reused.
4. **Client:** Uses the flyweight objects. It maintains the extrinsic state of the flyweights.

### Different Ways to Implement Flyweight Design Pattern

There are two common ways to implement the Flyweight Design Pattern:

1. **Using a Factory:** In this approach, the FlyweightFactory class manages a pool of flyweight objects and returns an existing flyweight if one exists, or creates a new one if not.
2. **Using Caching:** In this approach, flyweight objects are cached and reused when needed. The client retrieves flyweight objects from the cache instead of creating new ones.

### Flyweight Design Pattern Full Example in Java

**Flyweight Interface:**

```java
public interface Shape {
    void draw(int x, int y);
}
```

**ConcreteFlyweight Class:**

```java
public class Circle implements Shape {
    private final String color;

    public Circle(String color) {
        this.color = color;
    }

    @Override
    public void draw(int x, int y) {
        System.out.println("Drawing circle with color " + color + " at (" + x + ", " + y + ")");
    }
}
```

**FlyweightFactory Class:**

```java
import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {
    private static final Map<String, Shape> circleMap = new HashMap<>();

    public static Shape getCircle(String color) {
        Shape circle = circleMap.get(color);

        if (circle == null) {
            circle = new Circle(color);
            circleMap.put(color, circle);
        }

        return circle;
    }
}
```

**Client Code:**

```java
public class Client {
    private static final String[] colors = { "Red", "Green", "Blue", "Yellow", "Black" };

    public static void main(String[] args) {
        for (int i = 0; i < 20; ++i) {
            Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());
            circle.draw(getRandomX(), getRandomY());
        }
    }

    private static String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    private static int getRandomX() {
        return (int) (Math.random() * 100);
    }

    private static int getRandomY() {
        return (int) (Math.random() * 100);
    }
}
```
Here is the UML diagram for the Flyweight Design Pattern example you provided:

```plaintext
-----------------------------------
|              Shape              |
-----------------------------------
| + draw(int, int): void         |
-----------------------------------
                ^
                |
           -------------------
           |                 |
-----------------   -----------------
|     Circle      |   |   ShapeFactory  |
-----------------   -----------------
| - color: String  | | - circleMap: Map<String, Shape> |
-----------------   -----------------
| + draw(int, int): void        | | + getCircle(String): Shape |
----------------------------------- -------------------
```

In this diagram:

- `Shape` is the flyweight interface that declares the `draw` method.
- `Circle` is a concrete flyweight class that implements the `Shape` interface and represents a circle shape with a specific color.
- `ShapeFactory` is the flyweight factory class that manages flyweight objects. It maintains a map (`circleMap`) of shared flyweight objects and provides a method (`getCircle`) to get a flyweight object based on a given color.
- The client code (`Client`) uses the `ShapeFactory` to get flyweight objects (circles) and draws them at random positions on the screen.

### Use Case of Flyweight Design Pattern

The Flyweight Design Pattern is often used in graphical applications where a large number of similar objects, such as characters in a document or pixels on a screen, need to be efficiently managed and rendered.

### Real-time Full Example of Flyweight Design Pattern

Consider a real-time example of a text editor where you have a large document with many instances of the same character. Instead of storing each character as a separate object, you can use the Flyweight Design Pattern to share instances of characters that are used repeatedly.

Here's a simplified example of how the Flyweight pattern could be applied to a text editor scenario:

**Flyweight Interface:**

```java
public interface Character {
    void draw(int positionX, int positionY);
    char getCharacter();
}
```

**Concrete Flyweight Class:**

```java
public class SharedCharacter implements Character {
    private final char character;

    public SharedCharacter(char character) {
        this.character = character;
    }

    @Override
    public void draw(int positionX, int positionY) {
        System.out.println("Drawing character '" + character + "' at position (" + positionX + ", " + positionY + ")");
    }

    @Override
    public char getCharacter() {
        return character;
    }
}
```

**Flyweight Factory:**

```java
import java.util.HashMap;
import java.util.Map;

public class CharacterFactory {
    private static final Map<Character, Character> characterMap = new HashMap<>();

    public static Character getCharacter(char character) {
        Character sharedCharacter = characterMap.get(character);

        if (sharedCharacter == null) {
            sharedCharacter = new SharedCharacter(character);
            characterMap.put(character, sharedCharacter);
        }

        return sharedCharacter;
    }
}
```

**Client Code (Text Editor):**

```java
public class TextEditor {
    private final List<Character> characters = new ArrayList<>();

    public void addCharacter(char character, int positionX, int positionY) {
        Character sharedCharacter = CharacterFactory.getCharacter(character);
        characters.add(sharedCharacter);
        sharedCharacter.draw(positionX, positionY);
    }

    public void displayCharacters() {
        for (Character character : characters) {
            System.out.print(character.getCharacter());
        }
        System.out.println();
    }

    public static void main(String[] args) {
        TextEditor textEditor = new TextEditor();
        textEditor.addCharacter('H', 0, 0);
        textEditor.addCharacter('e', 10, 0);
        textEditor.addCharacter('l', 20, 0);
        textEditor.addCharacter('l', 30, 0); // Reuses 'l' instance
        textEditor.addCharacter('o', 40, 0);
        textEditor.displayCharacters();
    }
}
```

In this example, the `CharacterFactory` class manages the creation and sharing of `Character` objects. When a character is added to the text editor, the factory is used to retrieve a shared instance if it already exists, or create a new one if it doesn't. This way, memory is saved by reusing shared instances for characters that are used multiple times in the text.

Here's the UML diagram for the Flyweight Design Pattern example in a text editor scenario:

```plaintext
 ________________________        ___________________       _______________        ____________________
|          Client         |      |   Character      |     | SharedCharacter|      | CharacterFactory  |
|------------------------|      |------------------|     |----------------|      |-------------------|
|                        |      | +draw(int,int)   |     | -character:char|      | -characterMap:Map |
|                        |      | +getCharacter(): |     |----------------|      |-------------------|
|                        |      |                  |     | +draw(int,int) |      | +getCharacter(char)|
|                        |      |                  |     | +getCharacter():|      |___________________|
|                        |      |                  |     |________________|
|                        |      |__________________|             /\
|                        |                   /\                 |
|                        |                   |                  |
|                        |                   |                  |
|                        |                   |                  |
|________________________|                   |                  |
                                              |                  |
                                              |                  |
                                              |                  |
                                              |                  |
                 _____________________________|__________________|_____________________
                |                         Character Factory                               |
                |------------------------------------------------------------------------|
                | -characterMap:Map<Character, Character>                               |
                |------------------------------------------------------------------------|
                | +getCharacter(char):Character                                           |
                |________________________________________________________________________|

```

In this diagram:
- `Client` represents the class using the Flyweight pattern to manage characters in the text editor.
- `Character` is the interface implemented by both the `SharedCharacter` and `UnsharedCharacter` classes.
- `SharedCharacter` is a concrete flyweight class that represents shared characters.
- `CharacterFactory` is the flyweight factory class responsible for managing and creating shared character instances.

Please note that the `UnsharedCharacter` class, which could represent characters with unique properties not suitable for sharing, is not included in this example.

**Flyweight Interface and ConcreteFlyweight Class:**

Same as the example above.

**FlyweightFactory Class:**

Same as the example above.

**Client Code:**

Same as the example above.

### Pros and Cons of Flyweight Design Pattern

**Pros:**

1. **Memory Efficiency:** Reduces memory usage by sharing objects that are used repeatedly.
2. **Performance Improvement:** Improves performance by reducing the number of objects that need to be created and managed.
3. **Simplicity:** Simplifies the code by separating the intrinsic and extrinsic states of objects.

**Cons:**

1. **Complexity:** Introduces complexity, especially in managing the shared objects and ensuring thread safety.
2. **Increased Code Size:** Can increase the size of the codebase, especially if the flyweight objects need to be managed in a complex manner.
