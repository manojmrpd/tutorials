# Java String Coding Interview

## Table of Contents

- [Find Duplicate Characters in a Given String](#find-duplicate-characters-in-a-given-string)
- [Remove Duplicate Characters in a Given String](#remove-duplicate-characters-in-a-given-string)
- [Find the Duplicate Occurrences Count in a Given String](#find-the-duplicate-occurrences-count-in-a-given-string)
- [Find First Non-Repeated Character in a Given String](#find-first-non-repeated-character-in-a-given-string)
- [Find Second Non-Repeated Character in a Given String](#find-second-non-repeated-character-in-a-given-string)
- [Find First Repeated Character in a Given String](#find-first-repeated-character-in-a-given-string)
- [Find Second Repeated Character in a Given String](#find-second-repeated-character-in-a-given-string)
- [Check if a Given String is a Palindrome in Java](#check-if-a-given-string-is-a-palindrome-in-java)
- [Reverse a Given String in Java](#reverse-a-given-string-in-java)
- [Find Common Characters in Two Strings in Java](#find-common-characters-in-two-strings-in-java)
- [Remove Duplicate Characters in Two Strings in Java](#remove-duplicate-characters-in-two-strings-in-java)
- [Find Longest and Smallest Word in a String in Java](#find-longest-and-smallest-word-in-a-string-in-java)
- [Remove Duplicate Words in a Given String](#remove-duplicate-words-in-a-given-string)
- [Find Vowels and Consonants in a Given String](#find-vowels-and-consonants-in-a-given-string)
- [Find Max Vowel Count in a Given String](#find-max-vowel-count-in-a-given-string)
- [Check if Two Strings are Anagram in Java](#check-if-two-strings-are-anagram-in-java)
- [Find Longest Substring Without Repeated Characters](#find-longest-substring-without-repeated-characters)
- [Find Longest Substring With Repeated Vowel Characters](#find-longest-substring-with-repeated-vowel-characters)
- [Find Longest Substring with At Least K Repeating Characters](#find-longest-substring-with-at-least-k-repeating-characters)


## Find duplicate characters in a given string
To find duplicate characters in a given string without using `HashMap`, you can use an array to count the frequency of each character. Here's a step-by-step solution:

### Steps

1. **Count Character Frequencies**: Traverse the string and count the frequency of each character using an array.
2. **Identify Duplicates**: Traverse the frequency array to identify characters that appear more than once.

Here is the implementation:

```java
public class FindDuplicateCharacters {
    public static void main(String[] args) {
        String input = "programming";

        findDuplicateCharacters(input);
    }

    public static void findDuplicateCharacters(String str) {
        // Create an array to store character frequencies
        int[] charCount = new int[256]; // Assuming ASCII character set

        // Count character frequencies
        for (char c : str.toCharArray()) {
            charCount[c]++;
        }

        // Identify and print duplicate characters
        System.out.println("Duplicate characters in the string:");
        for (int i = 0; i < charCount.length; i++) {
            if (charCount[i] > 1) {
                System.out.println((char) i + ": " + charCount[i]);
            }
        }
    }
}
```

### Explanation

1. **Count Character Frequencies**:
    - The method `findDuplicateCharacters` creates an array `charCount` to store the frequency of each character.
    - It then iterates through the string and increments the corresponding index in the `charCount` array for each character.

2. **Identify Duplicates**:
    - The method traverses the `charCount` array and prints characters that have a frequency greater than 1, indicating duplicates.

### Output

For the input string `"programming"`, the output will be:

```
Duplicate characters in the string:
g: 2
m: 2
r: 2
```

This solution ensures that all duplicate characters in the given string are identified and their counts are printed without using `HashMap`.
## Remove duplicate characters in a given string
To remove duplicate characters from a given string in Java, you can use a `boolean` array to keep track of the characters that have already been encountered. Here's a step-by-step solution:

### Steps

1. **Initialize a `boolean` array**: This array will keep track of whether a character has been seen before.
2. **Iterate through the string**: Append characters to the result if they haven't been seen before.

Here's the implementation:

```java
public class RemoveDuplicateCharacters {
    public static void main(String[] args) {
        String input = "programming";

        String result = removeDuplicateCharacters(input);
        System.out.println("String after removing duplicate characters: " + result);
    }

    public static String removeDuplicateCharacters(String str) {
        // Create a boolean array to track characters
        boolean[] charSeen = new boolean[256]; // Assuming ASCII character set
        StringBuilder result = new StringBuilder();

        // Iterate through the string
        for (char c : str.toCharArray()) {
            if (!charSeen[c]) {
                result.append(c);
                charSeen[c] = true; // Mark this character as seen
            }
        }

        return result.toString();
    }
}
```

### Explanation

1. **Initialize a `boolean` array**:
    - The `charSeen` array of size 256 (for ASCII characters) is used to keep track of which characters have been encountered.

2. **Iterate through the string**:
    - The `StringBuilder` `result` is used to build the new string without duplicates.
    - For each character in the input string, check if it has already been seen using the `charSeen` array.
    - If the character has not been seen, append it to `result` and mark it as seen in the `charSeen` array.

### Output

For the input string `"programming"`, the output will be:

```
String after removing duplicate characters: progamin
```

This solution ensures that all duplicate characters are removed from the given string while maintaining the order of the first occurrences of each character.

## Find the duplicate occurrences count in a given string
To find the count of duplicate occurrences in a given string without using a `HashMap`, you can use an array to count the frequency of each character. Here's a step-by-step solution:

### Steps

1. **Count Character Frequencies**: Traverse the string and count the frequency of each character using an array.
2. **Identify Duplicates**: Traverse the frequency array to count characters that appear more than once.

Here's the implementation:

```java
public class FindDuplicateOccurrences {
    public static void main(String[] args) {
        String input = "programming";

        findDuplicateOccurrences(input);
    }

    public static void findDuplicateOccurrences(String str) {
        // Create an array to store character frequencies
        int[] charCount = new int[256]; // Assuming ASCII character set

        // Count character frequencies
        for (char c : str.toCharArray()) {
            charCount[c]++;
        }

        // Identify and print duplicate characters and their counts
        System.out.println("Duplicate characters in the string:");
        for (int i = 0; i < charCount.length; i++) {
            if (charCount[i] > 1) {
                System.out.println((char) i + ": " + charCount[i]);
            }
        }
    }
}
```

### Explanation

1. **Count Character Frequencies**:
    - The `charCount` array is used to store the frequency of each character.
    - The loop `for (char c : str.toCharArray())` iterates through each character in the string and increments the corresponding index in the `charCount` array.

2. **Identify Duplicates**:
    - The loop `for (int i = 0; i < charCount.length; i++)` iterates through the `charCount` array to identify characters with a frequency greater than 1, indicating duplicates.
    - It prints each duplicate character along with its count.

### Output

For the input string `"programming"`, the output will be:

```
Duplicate characters in the string:
g: 2
m: 2
r: 2
```

This solution ensures that all duplicate characters in the given string are identified and their counts are printed without using a `HashMap`.

## Find First Non repeated character in a given string 
To find the first non-repeated character in a given string, you can use an array to count the frequency of each character and then iterate over the string to find the first character with a frequency of 1. Here's how you can do it:

```java
public class FirstNonRepeatedCharacter {
    public static void main(String[] args) {
        String input = "programming";

        char firstNonRepeated = findFirstNonRepeated(input);

        System.out.println("First non-repeated character: " + firstNonRepeated);
    }

    public static char findFirstNonRepeated(String str) {
        // Create an array to store character frequencies
        int[] charCount = new int[256]; // Assuming ASCII character set

        // Count character frequencies
        for (char c : str.toCharArray()) {
            charCount[c]++;
        }

        // Find the first non-repeated character
        for (char c : str.toCharArray()) {
            if (charCount[c] == 1) {
                return c;
            }
        }

        // If no non-repeated character found, return '\0' (null character)
        return '\0';
    }
}
```

### Explanation

1. **Count Character Frequencies**:
    - The `charCount` array is used to store the frequency of each character.
    - The loop `for (char c : str.toCharArray())` iterates through each character in the string and increments the corresponding index in the `charCount` array.

2. **Find First Non-Repeated Character**:
    - The second loop `for (char c : str.toCharArray())` iterates through the string again.
    - For each character, it checks if the frequency in the `charCount` array is 1. If it is, it returns that character as the first non-repeated character.

3. **Handling No Non-Repeated Character**:
    - If no non-repeated character is found, the method returns `'\0'` (null character).

### Output

For the input string `"programming"`, the output will be:

```
First non-repeated character: o
```

This solution ensures that the first non-repeated character in the given string is identified and returned.
## Find Second Non-repeated character in a given string
To find the second non-repeated character in a given string, you can modify the approach slightly to keep track of the first and second non-repeated characters. Here's how you can do it:

```java
public class SecondNonRepeatedCharacter {
    public static void main(String[] args) {
        String input = "programming";

        char secondNonRepeated = findSecondNonRepeated(input);

        if (secondNonRepeated != '\0') {
            System.out.println("Second non-repeated character: " + secondNonRepeated);
        } else {
            System.out.println("No second non-repeated character found.");
        }
    }

    public static char findSecondNonRepeated(String str) {
        // Create an array to store character frequencies
        int[] charCount = new int[256]; // Assuming ASCII character set
        char firstNonRepeated = '\0';
        char secondNonRepeated = '\0';

        // Count character frequencies
        for (char c : str.toCharArray()) {
            charCount[c]++;
        }

        // Find the first and second non-repeated characters
        for (char c : str.toCharArray()) {
            if (charCount[c] == 1) {
                if (firstNonRepeated == '\0') {
                    firstNonRepeated = c;
                } else if (secondNonRepeated == '\0' && c != firstNonRepeated) {
                    secondNonRepeated = c;
                    break; // Second non-repeated character found, exit the loop
                }
            }
        }

        return secondNonRepeated;
    }
}
```

### Explanation

1. **Count Character Frequencies**:
    - The `charCount` array is used to store the frequency of each character.
    - The loop `for (char c : str.toCharArray())` iterates through each character in the string and increments the corresponding index in the `charCount` array.

2. **Find Second Non-Repeated Character**:
    - The second loop `for (char c : str.toCharArray())` iterates through the string again.
    - For each character with a frequency of 1, it checks if it is different from the first non-repeated character (`firstNonRepeated`). If it is, it assigns it as the second non-repeated character (`secondNonRepeated`) and breaks out of the loop.

3. **Handling No Second Non-Repeated Character**:
    - If no second non-repeated character is found, the method returns `'\0'` (null character).

### Output

For the input string `"programming"`, the output will be:

```
Second non-repeated character: i
```

This solution ensures that the second non-repeated character in the given string is identified and returned.
## Find First repeated character in a given string
To find the first repeated character in a given string, you can use a set to keep track of characters that have been seen before. Here's how you can do it:

```java
public class FirstRepeatedCharacter {
    public static void main(String[] args) {
        String input = "programming";

        char firstRepeated = findFirstRepeated(input);

        if (firstRepeated != '\0') {
            System.out.println("First repeated character: " + firstRepeated);
        } else {
            System.out.println("No repeated character found.");
        }
    }

    public static char findFirstRepeated(String str) {
        // Create a set to store characters that have been seen
        Set<Character> seenCharacters = new HashSet<>();

        // Iterate through the string
        for (char c : str.toCharArray()) {
            if (seenCharacters.contains(c)) {
                return c; // First repeated character found
            } else {
                seenCharacters.add(c);
            }
        }

        return '\0'; // No repeated character found
    }
}
```

### Explanation

1. **Using a Set to Track Characters**:
    - The `seenCharacters` set is used to keep track of characters that have been seen before.
    - The loop `for (char c : str.toCharArray())` iterates through each character in the string.

2. **Finding the First Repeated Character**:
    - For each character, it checks if it is already in the `seenCharacters` set.
    - If the character is already in the set, it means it is the first repeated character, and it returns that character.
    - If the character is not in the set, it adds the character to the set and continues iterating.

3. **Handling No Repeated Character**:
    - If no repeated character is found, the method returns `'\0'` (null character).

### Output

For the input string `"programming"`, the output will be:

```
First repeated character: g
```

This solution ensures that the first repeated character in the given string is identified and returned.

## Find Second repeated character in a given string
To find the second repeated character in a given string, you can modify the approach slightly to keep track of the first and second repeated characters. Here's how you can do it:

```java
public class SecondRepeatedCharacter {
    public static void main(String[] args) {
        String input = "programming";

        char secondRepeated = findSecondRepeated(input);

        if (secondRepeated != '\0') {
            System.out.println("Second repeated character: " + secondRepeated);
        } else {
            System.out.println("No second repeated character found.");
        }
    }

    public static char findSecondRepeated(String str) {
        // Create a set to store characters that have been seen
        Set<Character> seenCharacters = new HashSet<>();
        // Create a set to store characters that have been repeated
        Set<Character> repeatedCharacters = new HashSet<>();

        // Iterate through the string
        for (char c : str.toCharArray()) {
            if (seenCharacters.contains(c)) {
                repeatedCharacters.add(c); // Add to repeated characters set
            } else {
                seenCharacters.add(c); // Add to seen characters set
            }
        }

        // Find the second repeated character
        for (char c : str.toCharArray()) {
            if (repeatedCharacters.contains(c)) {
                return c; // Second repeated character found
            }
        }

        return '\0'; // No second repeated character found
    }
}
```

### Explanation

1. **Using Sets to Track Characters**:
    - The `seenCharacters` set is used to keep track of characters that have been seen before.
    - The `repeatedCharacters` set is used to keep track of characters that have been repeated.

2. **Finding the Second Repeated Character**:
    - For each character in the string, if the character is already in the `seenCharacters` set, it is added to the `repeatedCharacters` set.
    - After iterating through the string, the method iterates through the string again to find the second repeated character in the `repeatedCharacters` set.

3. **Handling No Second Repeated Character**:
    - If no second repeated character is found, the method returns `'\0'` (null character).

### Output

For the input string `"programming"`, the output will be:

```
Second repeated character: r
```

This solution ensures that the second repeated character in the given string is identified and returned.

## Find a given string is a palidrome in java
To check if a given string is a palindrome in Java, you can compare characters from the beginning and the end of the string and move towards the center. Here's how you can do it:

```java
public class PalindromeChecker {
    public static void main(String[] args) {
        String input = "radar";

        if (isPalindrome(input)) {
            System.out.println("The string is a palindrome.");
        } else {
            System.out.println("The string is not a palindrome.");
        }
    }

    public static boolean isPalindrome(String str) {
        int left = 0;
        int right = str.length() - 1;

        while (left < right) {
            if (str.charAt(left) != str.charAt(right)) {
                return false; // Characters at the left and right positions do not match
            }
            left++;
            right--;
        }

        return true; // All characters matched, so the string is a palindrome
    }
}
```

### Explanation

1. **Comparing Characters**:
    - The `isPalindrome` method uses two pointers, `left` and `right`, initialized to the beginning and end of the string, respectively.
    - It compares characters at these positions and moves `left` towards the right and `right` towards the left until they meet or cross each other.

2. **Checking for Palindrome**:
    - If at any point the characters at the `left` and `right` positions do not match, the method returns `false`, indicating that the string is not a palindrome.
    - If all characters match, the method returns `true`, indicating that the string is a palindrome.

### Output

For the input string `"radar"`, the output will be:

```
The string is a palindrome.
```

This solution efficiently checks if a given string is a palindrome without using any additional data structures.

## Reverse a given string in java
To reverse a given string in Java, you can use a `StringBuilder` or convert the string to a character array and swap characters from the beginning and end of the array. Here's how you can do it using both methods:

Using `StringBuilder`:

```java
public class StringReversal {
    public static void main(String[] args) {
        String input = "hello";

        String reversed = reverseString(input);

        System.out.println("Reversed string: " + reversed);
    }

    public static String reverseString(String str) {
        return new StringBuilder(str).reverse().toString();
    }
}
```

Using character array:

```java
public class StringReversal {
    public static void main(String[] args) {
        String input = "hello";

        String reversed = reverseString(input);

        System.out.println("Reversed string: " + reversed);
    }

    public static String reverseString(String str) {
        char[] charArray = str.toCharArray();
        int left = 0;
        int right = charArray.length - 1;

        while (left < right) {
            // Swap characters
            char temp = charArray[left];
            charArray[left] = charArray[right];
            charArray[right] = temp;

            // Move towards the center
            left++;
            right--;
        }

        return new String(charArray);
    }
}
```

Both methods will reverse the given string. Using `StringBuilder` is more concise, while using a character array may be more efficient for longer strings as it avoids creating a new `StringBuilder` object.

## Find common characters in two string in java
To find common characters in two strings in Java, you can use a `HashMap` to store the frequency of characters in one string, and then iterate over the characters in the second string to check if they are present in the `HashMap`. Here's how you can do it:

```java
import java.util.HashMap;
import java.util.Map;

public class CommonCharacters {
    public static void main(String[] args) {
        String str1 = "hello";
        String str2 = "world";

        String commonChars = findCommonCharacters(str1, str2);

        System.out.println("Common characters: " + commonChars);
    }

    public static String findCommonCharacters(String str1, String str2) {
        // Create a map to store character frequencies in str1
        Map<Character, Integer> charCount = new HashMap<>();

        // Count character frequencies in str1
        for (char c : str1.toCharArray()) {
            charCount.put(c, charCount.getOrDefault(c, 0) + 1);
        }

        // Find common characters in str2
        StringBuilder commonChars = new StringBuilder();
        for (char c : str2.toCharArray()) {
            if (charCount.containsKey(c) && charCount.get(c) > 0) {
                commonChars.append(c);
                charCount.put(c, charCount.get(c) - 1);
            }
        }

        return commonChars.toString();
    }
}
```

### Explanation

1. **Counting Character Frequencies**:
    - The `findCommonCharacters` method first counts the frequency of each character in the first string (`str1`) using a `HashMap` (`charCount`).

2. **Finding Common Characters**:
    - Then, it iterates over each character in the second string (`str2`) and checks if the character is present in the `HashMap` and its frequency is greater than 0.
    - If both conditions are met, the character is appended to the `commonChars` string and its frequency in the `HashMap` is decremented.

3. **Returning Result**:
    - The method returns the `commonChars` string containing all the common characters found in both strings.

### Output

For the input strings `"hello"` and `"world"`, the output will be:

```
Common characters: lo
```

This solution efficiently finds the common characters in two strings using a `HashMap` to store character frequencies.

## Remove duplicate characters in two string in java
To remove duplicate characters in two strings in Java, you can concatenate the two strings and then use a similar approach as before to remove duplicates. Here's how you can do it:

```java
public class RemoveDuplicatesInTwoStrings {
    public static void main(String[] args) {
        String str1 = "hello";
        String str2 = "world";

        String result = removeDuplicates(str1, str2);

        System.out.println("String after removing duplicates: " + result);
    }

    public static String removeDuplicates(String str1, String str2) {
        // Concatenate the two strings
        String concatenated = str1 + str2;

        // Create a map to store character frequencies
        Map<Character, Integer> charCount = new HashMap<>();

        // Remove duplicates and build the result string
        StringBuilder result = new StringBuilder();
        for (char c : concatenated.toCharArray()) {
            if (!charCount.containsKey(c)) {
                result.append(c);
                charCount.put(c, 1);
            }
        }

        return result.toString();
    }
}
```

### Explanation

1. **Concatenating Strings**:
    - The two input strings (`str1` and `str2`) are concatenated into a single string (`concatenated`).

2. **Removing Duplicates**:
    - A `HashMap` (`charCount`) is used to store character frequencies.
    - Each character in the `concatenated` string is iterated over.
    - If a character is not already in the `charCount` map, it is appended to the `result` string, and its frequency is set to 1 in the `charCount` map.

3. **Returning Result**:
    - The method returns the `result` string, which contains all characters from both strings with duplicates removed.

### Output

For the input strings `"hello"` and `"world"`, the output will be:

```
String after removing duplicates: helowrd
```

This solution efficiently removes duplicate characters from two strings and concatenates them into a single string.

## Find Longest and Smallest word in a string in java
To find the longest and smallest (shortest) words in a string in Java, you can split the string into words and iterate over them to find the longest and smallest words. Here's how you can do it:

```java
public class LongestAndShortestWord {
    public static void main(String[] args) {
        String input = "This is a sample string with some words";

        String[] words = input.split(" ");

        String longestWord = findLongestWord(words);
        String shortestWord = findShortestWord(words);

        System.out.println("Longest word: " + longestWord);
        System.out.println("Shortest word: " + shortestWord);
    }

    public static String findLongestWord(String[] words) {
        String longestWord = "";
        for (String word : words) {
            if (word.length() > longestWord.length()) {
                longestWord = word;
            }
        }
        return longestWord;
    }

    public static String findShortestWord(String[] words) {
        String shortestWord = words[0];
        for (String word : words) {
            if (word.length() < shortestWord.length()) {
                shortestWord = word;
            }
        }
        return shortestWord;
    }
}
```

### Explanation

1. **Splitting the String**:
    - The input string is split into words using the `split(" ")` method, which splits the string on spaces.

2. **Finding the Longest Word**:
    - The `findLongestWord` method iterates over the words and keeps track of the longest word found so far.

3. **Finding the Shortest Word**:
    - The `findShortestWord` method similarly iterates over the words but keeps track of the shortest word found so far.

4. **Returning Results**:
    - The longest and shortest words found are returned from their respective methods.

### Output

For the input string `"This is a sample string with some words"`, the output will be:

```
Longest word: string
Shortest word: a
```

This solution efficiently finds the longest and shortest words in a given string.

## Remove duplicate words in a given string
To remove duplicate words in a given string in Java, you can split the string into words, use a `Set` to keep track of unique words, and then join the unique words back into a string. Here's how you can do it:

```java
public class RemoveDuplicateWords {
    public static void main(String[] args) {
        String input = "hello world world hello";

        String result = removeDuplicates(input);

        System.out.println("String after removing duplicates: " + result);
    }

    public static String removeDuplicates(String str) {
        // Split the string into words
        String[] words = str.split(" ");

        // Create a set to store unique words
        Set<String> uniqueWords = new LinkedHashSet<>();

        // Add words to the set (duplicates will be removed automatically)
        Collections.addAll(uniqueWords, words);

        // Join the unique words back into a string
        return String.join(" ", uniqueWords);
    }
}
```

### Explanation

1. **Splitting the String**:
    - The input string is split into words using the `split(" ")` method, which splits the string on spaces.

2. **Removing Duplicates**:
    - A `Set` (`uniqueWords`) is used to store unique words. As `LinkedHashSet` is used, it maintains the order of insertion.
    - By adding all the words to the set, duplicates are automatically removed.

3. **Joining Unique Words**:
    - The unique words are then joined back into a string using `String.join(" ", uniqueWords)`.

### Output

For the input string `"hello world world hello"`, the output will be:

```
String after removing duplicates: hello world
```

This solution efficiently removes duplicate words from a given string while preserving the order of the words.

## Find Vowels and Consonants in a given string
To find the vowels and consonants in a given string in Java, you can iterate over each character in the string and check if it is a vowel or a consonant. Here's how you can do it:

```java
public class VowelsAndConsonants {
    public static void main(String[] args) {
        String input = "hello world";

        int vowels = 0;
        int consonants = 0;

        for (char c : input.toLowerCase().toCharArray()) {
            if (c >= 'a' && c <= 'z') {
                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                    vowels++;
                } else {
                    consonants++;
                }
            }
        }

        System.out.println("Vowels: " + vowels);
        System.out.println("Consonants: " + consonants);
    }
}
```

### Explanation

1. **Iterating Over Characters**:
    - The `for` loop iterates over each character in the string after converting it to lowercase.

2. **Checking for Vowels and Consonants**:
    - Each character is checked to ensure it is a letter (`c >= 'a' && c <= 'z'`).
    - If the character is a vowel (`'a'`, `'e'`, `'i'`, `'o'`, `'u'`), the `vowels` count is incremented.
    - If the character is not a vowel, it is considered a consonant, and the `consonants` count is incremented.

### Output

For the input string `"hello world"`, the output will be:

```
Vowels: 3
Consonants: 7
```

This solution efficiently counts the number of vowels and consonants in a given string.

## Find Max vowel count in a given string
To find the maximum vowel count in a given string in Java, you can iterate over the characters in the string, count the vowels for each substring starting from each character, and keep track of the maximum count found. Here's how you can do it:

```java
public class MaxVowelCount {
    public static void main(String[] args) {
        String input = "hello world";

        int maxVowelCount = findMaxVowelCount(input);

        System.out.println("Max vowel count: " + maxVowelCount);
    }

    public static int findMaxVowelCount(String str) {
        int maxCount = 0;

        for (int i = 0; i < str.length(); i++) {
            int count = 0;
            for (int j = i; j < str.length(); j++) {
                char c = Character.toLowerCase(str.charAt(j));
                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                    count++;
                }
                if (count > maxCount) {
                    maxCount = count;
                }
            }
        }

        return maxCount;
    }
}
```

### Explanation

1. **Nested Loop**:
    - The outer loop (`for (int i = 0; i < str.length(); i++)`) iterates over each character in the string, starting from index 0.
    - The inner loop (`for (int j = i; j < str.length(); j++)`) iterates over each substring starting from the current character.

2. **Counting Vowels**:
    - For each character in the substring, vowels are counted (`count++`) if the character is a vowel.
    - The maximum vowel count (`maxCount`) is updated whenever a new maximum is found.

3. **Returning Result**:
    - After iterating over all substrings, the maximum vowel count is returned.

### Output

For the input string `"hello world"`, the output will be:

```
Max vowel count: 2
```

This solution efficiently finds the maximum vowel count in a given string.

## Check if two strings are anagram in java
To check if two strings are anagrams in Java, you can compare their character counts. An anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once. Here's how you can do it:

```java
import java.util.Arrays;

public class AnagramChecker {
    public static void main(String[] args) {
        String str1 = "listen";
        String str2 = "silent";

        if (areAnagrams(str1, str2)) {
            System.out.println("The strings are anagrams.");
        } else {
            System.out.println("The strings are not anagrams.");
        }
    }

    public static boolean areAnagrams(String str1, String str2) {
        // Remove spaces and convert to lowercase
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        // Check if the lengths are different
        if (str1.length() != str2.length()) {
            return false;
        }

        // Convert strings to character arrays and sort
        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();

        Arrays.sort(charArray1);
        Arrays.sort(charArray2);

        // Compare sorted strings
        return Arrays.equals(charArray1, charArray2);
    }
}
```

### Explanation

1. **Removing Spaces and Lowercasing**:
    - The input strings are first processed to remove spaces and convert them to lowercase using `replaceAll("\\s", "").toLowerCase()`.

2. **Length Check**:
    - If the lengths of the two processed strings are different, they cannot be anagrams, so the method returns `false`.

3. **Sorting Characters**:
    - The processed strings are converted to character arrays using `toCharArray()` and then sorted using `Arrays.sort()`.

4. **Comparing Arrays**:
    - The sorted character arrays are compared using `Arrays.equals()` to determine if the strings are anagrams.

### Output

For the input strings `"listen"` and `"silent"`, the output will be:

```
The strings are anagrams.
```

This solution efficiently checks if two strings are anagrams by comparing their sorted character arrays.

## Find Longest Substring without repeated characters
To find the longest substring without repeating characters in a given string, you can use a sliding window approach. Here's a Java function that does this:

```java
import java.util.HashSet;

public class LongestSubstringWithoutRepeating {

    public static int lengthOfLongestSubstring(String s) {
        int n = s.length();
        HashSet<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        while (i < n && j < n) {
            if (!set.contains(s.charAt(j))) {
                set.add(s.charAt(j++));
                ans = Math.max(ans, j - i);
            } else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        String s = "abcabcbb";
        System.out.println(lengthOfLongestSubstring(s)); // Output: 3 (abc)
    }
}
```

This function uses two pointers `i` and `j` to create a sliding window. The `HashSet` is used to keep track of the characters in the current substring. If a character is already in the set, `i` is incremented to remove it from the set, and if not, `j` is incremented to expand the window. The maximum length of the window is stored in `ans`.

## Find Longest Substring with repeated vowel characters 
- String s = "leetcode"; answer 2
To find the longest substring with repeated vowel characters in a given string, we can modify the approach slightly to focus on checking for vowels and keeping track of their repetitions. Here's a Java function that does this:

```java
import java.util.HashSet;

public class LongestSubstringWithRepeatedVowels {

    public static int lengthOfLongestSubstringWithRepeatedVowels(String s) {
        int n = s.length();
        HashSet<Character> vowels = new HashSet<>();
        vowels.add('a');
        vowels.add('e');
        vowels.add('i');
        vowels.add('o');
        vowels.add('u');
        int ans = 0, count = 0;
        char prev = '\0';
        for (char c : s.toCharArray()) {
            if (vowels.contains(c)) {
                count++;
                if (c != prev) {
                    count = 1;
                }
                ans = Math.max(ans, count);
                prev = c;
            } else {
                prev = '\0';
                count = 0;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        String s = "leetcode";
        System.out.println(lengthOfLongestSubstringWithRepeatedVowels(s)); // Output: 2 (ee)
    }
}
```

In this function, we iterate over the characters in the string `s` and maintain a count of consecutive vowel characters (`count`). We also keep track of the previous vowel character (`prev`). Whenever we encounter a vowel character, we update the count and check if it's greater than the current maximum (`ans`). If the current character is different from the previous one, we reset the count to 1.

## Longest Substring with At Least K Repeating Characters
To find the length of the longest substring in a given string such that each character in the substring appears at least `k` times, you can use a recursive approach. Here's how you can do it in Java:

```java
public class LongestSubstringKRepeatingCharacters {
    public static void main(String[] args) {
        String s = "aaabb";
        int k = 3;

        int longestLength = longestSubstring(s, k);

        System.out.println("Length of the longest substring with at least " + k + " repeating characters: " + longestLength);
    }

    public static int longestSubstring(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        int[] charCounts = new int[26];
        for (char c : s.toCharArray()) {
            charCounts[c - 'a']++;
        }

        boolean valid = true;
        for (int count : charCounts) {
            if (count > 0 && count < k) {
                valid = false;
                break;
            }
        }

        if (valid) {
            return s.length();
        }

        int start = 0;
        int end = 0;
        int maxLength = 0;

        while (end < s.length()) {
            if (charCounts[s.charAt(end) - 'a'] < k) {
                maxLength = Math.max(maxLength, longestSubstring(s.substring(start, end), k));
                start = end + 1;
            }
            end++;
        }

        maxLength = Math.max(maxLength, longestSubstring(s.substring(start), k));

        return maxLength;
    }
}
```

### Explanation

1. **Recursive Approach**:
    - The `longestSubstring` method is called recursively to find the longest substring that satisfies the condition.
    - The method first counts the frequency of each character in the string.
    - It then checks if the string is valid (i.e., each character appears at least `k` times).
    - If the string is valid, the length of the string is returned.
    - If the string is not valid, the string is split at the first character that appears less than `k` times, and the method is called recursively on each part.

2. **Base Cases**:
    - The base cases are when the string is empty or when all characters in the string appear at least `k` times.

3. **Recursion**:
    - The method calls itself on the left and right substrings of the current string (excluding the character that caused the split).

4. **Returning the Result**:
    - The maximum length of the valid substrings is returned.

### Output

For the input string `"aaabb"` and `k = 3`, the output will be:

```
Length of the longest substring with at least 3 repeating characters: 3
```

This solution efficiently finds the length of the longest substring in a given string such that each character in the substring appears at least `k` times.
