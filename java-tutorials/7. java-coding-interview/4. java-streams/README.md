# Java 8 Stream Coding Interview

## Table of Contents

- [Filter even numbers in a given array using java8](#filter-even-numbers-in-a-given-array-using-java8)
- [Remove duplicate characters in a given string using java8](#remove-duplicate-characters-in-a-given-string-using-java8)
- [Remove duplicate words in a given string using java8](#remove-duplicate-words-in-a-given-string-using-java8)
- [Print unique characters in a given string using java8](#print-unique-characters-in-a-given-string-using-java8)
- [Find the duplicate occurrences count in a given array using java8](#find-the-duplicate-occurrences-count-in-a-given-array-using-java8)
- [Find the duplicate occurrences count in a given string using java8](#find-the-duplicate-occurrences-count-in-a-given-string-using-java8)
- [Find First Non repeated character in a given string using java8](#find-first-non-repeated-character-in-a-given-string-using-java8)
- [Find Second Non-repeated character in a given string using java8](#find-second-non-repeated-character-in-a-given-string-using-java8)
- [Find First repeated character in a given string using java8](#find-first-repeated-character-in-a-given-string-using-java8)
- [Find Second repeated character in a given string using java8](#find-second-repeated-character-in-a-given-string-using-java8)
- [Find First longest string in a given array using java8](#find-first-longest-string-in-a-given-array-using-java8)
- [Find Second longest string in a given array using java8](#find-second-longest-string-in-a-given-array-using-java8)
- [Find a given string is a palidrome using java8](#find-a-given-string-is-a-palidrome-using-java8)
- [Reverse a given string using java8](#reverse-a-given-string-using-java8)
- [Find common characters in two string using java8](#find-common-characters-in-two-string-using-java8)
- [Find the words starting with A using java8](#find-the-words-starting-with-a-using-java8)
- [First the First largest number in a given array using java8](#first-the-first-largest-number-in-a-given-array-using-java8)
- [Find the Second largest number in a given array using java8](#find-the-second-largest-number-in-a-given-array-using-java8)
- [Find the Third largest number in a given array using java8](#find-the-third-largest-number-in-a-given-array-using-java8)
- [Find only duplicate elements with its count from the String list using java8](#find-only-duplicate-elements-with-its-count-from-the-string-list-using-java8)
- [Find the highest duplicate count from a given string using java?](#find-the-highest-duplicate-count-from-a-given-string-using-java)
- [Perform cube on list elements and filter numbers greater than 50 using java8](#perform-cube-on-list-elements-and-filter-numbers-greater-than-50-using-java8)
- [Generate the fibanocci series using java8](#generate-the-fibanocci-series-using-java8)
- [Print sum of all numbers using java8](#print-sum-of-all-numbers-using-java8)
- [Find Vowels and Consonants in a given string using java8](#find-vowels-and-consonants-in-a-given-string-using-java8)
- [Sort the elements in a given array in ascending order using java8](#sort-the-elements-in-a-given-array-in-ascending-order-using-java8)
- [Sort the elements in a given array in descending order using java8](#sort-the-elements-in-a-given-array-in-descending-order-using-java8)
- [Filter employee list whose gender is male and salary is less than 50000](#filter-employee-list-whose-gender-is-male-and-salary-is-less-than-50000)
- [Filter employee list and group based on gender using java8](#filter-employee-list-and-group-based-on-gender-using-java8)
- [Convert Map to List using java8](#convert-map-to-list-using-java8)
- [Convert List to Map using java8](#convert-list-to-map-using-java8)
- [Sort Product List using Category and Price using java8](#sort-product-list-using-category-and-price-using-java8)
- [Find Average price of a Product whose category is Fruit](#find-average-price-of-a-product-whose-category-is-fruit)
- [Given a product list, Find the count of product of particular category whose inventory is available.](#given-a-product-list-find-the-count-of-product-of-particular-category-whose-inventory-is-available)
- [Given Each Product have List of Sku, Return all the skus whose inventory is out of stock](#given-each-product-have-list-of-sku-return-all-the-skus-whose-inventory-is-out-of-stock)
- [Given Each Product have List of Sku, Return all the skus whose inventory is out of stock for each product](#given-each-product-have-list-of-sku-return-all-the-skus-whose-inventory-is-out-of-stock-for-each-product)
- [Given an Employee list, Return the Employee who is having max and min salary](#given-an-employee-list-return-the-employee-who-is-having-max-and-min-salary)
- [Given an Employee List, Return the maximum salary of an employee from each department](#given-an-employee-list-return-the-maximum-salary-of-an-employee-from-each-department)
- [Given a Product list, Return the maximum price of a product from each category](#given-a-product-list-return-the-maximum-price-of-a-product-from-each-category)
- [Sort the Map by key and value using Java 8](#sort-the-map-by-key-and-value-using-java-8)
- [Given an Employee List, Find the Nth Highest Salary from an Employee whose salary may be duplicated](#given-an-employee-list-find-the-nth-highest-salary-from-an-employee-whose-salary-may-be-duplicated)
- [Given an Employee List, Sort employees based on descending order](#given-an-employee-list-sort-employees-based-on-descending-order)
- [Given an Employee List, Get Top 3 salaried employees](#given-an-employee-list-get-top-3-salaried-employees)
- [Given an Employee List, Fetch all employees having salary less than 3rd highest salary](#given-an-employee-list-fetch-all-employees-having-salary-less-than-3rd-highest-salary)


## Filter even numbers in a given array using java8

To filter even numbers from a given array using Java 8, you can utilize the Stream API. Here is an example demonstrating how to do this:

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterEvenNumbers {
    public static void main(String[] args) {
        // Sample array of integers
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        // Convert the array to a List of integers
        List<Integer> numberList = Arrays.stream(numbers)
                                         .boxed()
                                         .collect(Collectors.toList());

        // Use the Stream API to filter even numbers
        List<Integer> evenNumbers = numberList.stream()
                                              .filter(n -> n % 2 == 0)
                                              .collect(Collectors.toList());

        // Print the list of even numbers
        System.out.println(evenNumbers);
    }
}
```

In this example:
1. The array `numbers` is converted to a stream.
2. The `boxed()` method is used to convert the primitive `int` stream to a stream of `Integer` objects.
3. The `filter()` method filters out the even numbers using the condition `n -> n % 2 == 0`.
4. The `collect(Collectors.toList())` method collects the filtered elements into a list.
5. Finally, the list of even numbers is printed.

## Remove duplicate characters in a given string using java8
To remove duplicate characters from a given string using Java 8, you can use the Stream API along with a `LinkedHashSet` to maintain the order of characters. Here is an example demonstrating how to do this:

```java
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RemoveDuplicateCharacters {
    public static void main(String[] args) {
        // Sample string
        String input = "programming";

        // Use the Stream API to remove duplicate characters
        String result = input.chars()
                             .mapToObj(c -> (char) c)
                             .distinct()
                             .map(String::valueOf)
                             .collect(Collectors.joining());

        // Print the result string without duplicate characters
        System.out.println(result);
    }
}
```

In this example:
1. The `chars()` method converts the string to an `IntStream` of character codes.
2. The `mapToObj(c -> (char) c)` method converts each character code to a `Character` object.
3. The `distinct()` method filters out duplicate characters.
4. The `map(String::valueOf)` method converts each `Character` object back to a `String`.
5. The `collect(Collectors.joining())` method concatenates the characters into a single string.
6. Finally, the result string without duplicate characters is printed.

The output for the input `"programming"` would be `"progamin"`.

## Remove duplicate words in a given string using java8
To remove duplicate words from a given string using Java 8, you can use the Stream API along with a `LinkedHashSet` to maintain the order of words. Here is an example demonstrating how to do this:

```java
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RemoveDuplicateWords {
    public static void main(String[] args) {
        // Sample string
        String input = "this is a test test string with duplicate duplicate words";

        // Use the Stream API to remove duplicate words
        String result = Arrays.stream(input.split("\\s+"))
                              .distinct()
                              .collect(Collectors.joining(" "));

        // Print the result string without duplicate words
        System.out.println(result);
    }
}
```

In this example:
1. The `split("\\s+")` method splits the input string into an array of words using whitespace as the delimiter.
2. The `Arrays.stream()` method creates a stream from the array of words.
3. The `distinct()` method filters out duplicate words.
4. The `collect(Collectors.joining(" "))` method concatenates the unique words into a single string with spaces between them.
5. Finally, the result string without duplicate words is printed.

The output for the input `"this is a test test string with duplicate duplicate words"` would be `"this is a test string with duplicate words"`.

## Print unique characters in a given string using java8
To print unique characters in a given string using Java 8, you can convert the string to a stream of characters, filter out duplicates, and then collect the unique characters into a new string. Here's an example:

```java
public class UniqueCharacters {
    public static void main(String[] args) {
        String str = "hello world";

        // Find unique characters in the string
        String uniqueChars = str.chars()
                .distinct() // Remove duplicates
                .mapToObj(c -> String.valueOf((char) c)) // Convert int to String
                .collect(Collectors.joining()); // Join the characters into a string

        // Print the unique characters
        System.out.println("Unique characters in the string: " + uniqueChars);
    }
}
```

In this example:
- `str.chars()` converts the string to an IntStream of Unicode code points.
- `distinct()` removes duplicate code points.
- `mapToObj(c -> String.valueOf((char) c))` converts each code point back to a string character.
- `collect(Collectors.joining())` joins the characters into a single string.

For the input string `"hello world"`, the output would be:
```
Unique characters in the string: helo wrd
```

## Find the duplicate occurrences count in a given array using java8
To find the count of duplicate occurrences in a given array using Java 8, you can utilize the `Stream` API along with `Collectors.groupingBy` and `Collectors.counting`. Here is an example demonstrating how to achieve this:

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DuplicateOccurrencesCount {
    public static void main(String[] args) {
        // Sample array
        Integer[] numbers = {1, 2, 3, 4, 2, 3, 5, 6, 3, 7, 8, 8, 8};

        // Convert the array to a list
        List<Integer> numberList = Arrays.asList(numbers);

        // Find the count of duplicate occurrences
        Map<Integer, Long> duplicateCountMap = numberList.stream()
                .collect(Collectors.groupingBy(n -> n, Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        // Print the duplicate occurrences count
        duplicateCountMap.forEach((key, value) -> System.out.println("Number " + key + " occurs " + value + " times"));
    }
}
```

In this example:
1. The array `numbers` is converted to a list using `Arrays.asList()`.
2. The `stream()` method is called on the list to create a stream of elements.
3. The `collect(Collectors.groupingBy(n -> n, Collectors.counting()))` method groups the elements by their value and counts the occurrences of each element.
4. The resulting map is filtered to include only those entries where the count is greater than 1, indicating duplicates.
5. The filtered map is collected into a new map using `Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))`.
6. The resulting map of duplicate counts is printed, displaying each number and its count of occurrences.

The output for the input array `{1, 2, 3, 4, 2, 3, 5, 6, 3, 7, 8, 8, 8}` would be:
```
Number 2 occurs 2 times
Number 3 occurs 3 times
Number 8 occurs 3 times
```
## Find the duplicate occurrences count in a given string using java8

To find the count of duplicate occurrences of characters in a given string using Java 8, you can use the `chars()` method to get an IntStream of characters, and then collect them into a map where the key is the character and the value is the count. Here's how you can do it:

```java
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateOccurrencesCount {
    public static void main(String[] args) {
        String str = "Hello, World!";
        
        // Find the count of duplicate occurrences of characters in the string
        Map<Character, Long> occurrences = str.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        // Print the count of duplicate occurrences
        occurrences.entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .forEach(entry -> System.out.println("Character '" + entry.getKey() + "' occurs " + entry.getValue() + " times."));
    }
}
```

In this example:
- We first convert the string into an `IntStream` of character codes using `chars()`.
- Then, we map each character code to a character using `mapToObj(c -> (char) c)`.
- Next, we collect the characters into a map using `Collectors.groupingBy` and `Collectors.counting()` to get the count of each character.
- Finally, we print the characters that have a count greater than 1, indicating duplicate occurrences.

The output will show the characters in the string that occur more than once, along with the count of their occurrences.

## Find First Non repeated character in a given string using java8
To find the first non-repeated character in a given string using Java 8, you can use the Stream API along with `Collectors.groupingBy` to count the occurrences of each character. Then, you can filter the characters to find the first one that appears only once. Here is an example demonstrating how to do this:

```java
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FirstNonRepeatedCharacter {
    public static void main(String[] args) {
        // Sample string
        String input = "swiss";

        // Find the first non-repeated character
        Character firstNonRepeatedChar = input.chars()
                                              .mapToObj(c -> (char) c)
                                              .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))
                                              .entrySet().stream()
                                              .filter(entry -> entry.getValue() == 1)
                                              .map(Map.Entry::getKey)
                                              .findFirst()
                                              .orElse(null);

        // Print the first non-repeated character
        System.out.println(firstNonRepeatedChar != null 
                           ? "First non-repeated character: " + firstNonRepeatedChar 
                           : "No non-repeated character found");
    }
}
```

In this example:
1. The `chars()` method converts the string to an `IntStream` of character codes.
2. The `mapToObj(c -> (char) c)` method converts each character code to a `Character` object.
3. The `collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))` method groups the characters by their value and counts the occurrences of each character, using a `LinkedHashMap` to maintain the insertion order.
4. The `entrySet().stream()` method creates a stream of the map's entries.
5. The `filter(entry -> entry.getValue() == 1)` method filters the entries to include only those characters that appear exactly once.
6. The `map(Map.Entry::getKey)` method extracts the character from each entry.
7. The `findFirst()` method finds the first character in the stream that meets the criteria.
8. The `orElse(null)` method returns `null` if no such character is found.
9. Finally, the first non-repeated character is printed, or a message indicating no non-repeated character was found is printed.

For the input `"swiss"`, the output would be:
```
First non-repeated character: w
```

## Find Second Non-repeated character in a given string using java8
To find the second non-repeated character in a given string using Java 8, you can extend the previous logic slightly by using the Stream API. You'll filter for characters that appear only once and then use the `skip()` method to bypass the first non-repeated character. Here is an example demonstrating how to do this:

```java
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SecondNonRepeatedCharacter {
    public static void main(String[] args) {
        // Sample string
        String input = "swiss";

        // Find the second non-repeated character
        Character secondNonRepeatedChar = input.chars()
                                               .mapToObj(c -> (char) c)
                                               .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))
                                               .entrySet().stream()
                                               .filter(entry -> entry.getValue() == 1)
                                               .skip(1) // Skip the first non-repeated character
                                               .map(Map.Entry::getKey)
                                               .findFirst()
                                               .orElse(null);

        // Print the second non-repeated character
        System.out.println(secondNonRepeatedChar != null 
                           ? "Second non-repeated character: " + secondNonRepeatedChar 
                           : "No second non-repeated character found");
    }
}
```

In this example:
1. The `chars()` method converts the string to an `IntStream` of character codes.
2. The `mapToObj(c -> (char) c)` method converts each character code to a `Character` object.
3. The `collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))` method groups the characters by their value and counts the occurrences of each character, using a `LinkedHashMap` to maintain the insertion order.
4. The `entrySet().stream()` method creates a stream of the map's entries.
5. The `filter(entry -> entry.getValue() == 1)` method filters the entries to include only those characters that appear exactly once.
6. The `skip(1)` method skips the first non-repeated character in the stream.
7. The `map(Map.Entry::getKey)` method extracts the character from each entry.
8. The `findFirst()` method finds the second non-repeated character in the stream that meets the criteria.
9. The `orElse(null)` method returns `null` if no such character is found.
10. Finally, the second non-repeated character is printed, or a message indicating no second non-repeated character was found is printed.

For the input `"swiss"`, the output would be:
```
Second non-repeated character: i
```
## Find First repeated character in a given string using java8
To find the first repeated character in a given string using Java 8, you can use the Stream API along with a `Set` to track characters that have already been seen. Here's an example demonstrating how to achieve this:

```java
import java.util.HashSet;
import java.util.Set;

public class FirstRepeatedCharacter {
    public static void main(String[] args) {
        // Sample string
        String input = "swiss";

        // Find the first repeated character
        Character firstRepeatedChar = input.chars()
                                           .mapToObj(c -> (char) c)
                                           .filter(new HashSet<Character>()::add)
                                           .findFirst()
                                           .orElse(null);

        // Print the first repeated character
        System.out.println(firstRepeatedChar != null 
                           ? "First repeated character: " + firstRepeatedChar 
                           : "No repeated character found");
    }
}
```

In this example:
1. The `chars()` method converts the string to an `IntStream` of character codes.
2. The `mapToObj(c -> (char) c)` method converts each character code to a `Character` object.
3. The `filter(new HashSet<Character>()::add)` method uses a `HashSet` to filter out characters. The `add` method of `HashSet` returns `false` if the character is already in the set, which means it's a repeated character.
4. The `findFirst()` method finds the first character in the stream that is repeated.
5. The `orElse(null)` method returns `null` if no such character is found.
6. Finally, the first repeated character is printed, or a message indicating no repeated character was found is printed.

The output for the input `"swiss"` would be:
```
First repeated character: s
```

Note: I realized there's a mistake in the logic here. The `filter(new HashSet<Character>()::add)` approach returns unique elements, not the repeated ones. We need a different approach to detect the first repeated character correctly.

Here's the corrected version:

```java
import java.util.HashSet;
import java.util.Set;

public class FirstRepeatedCharacter {
    public static void main(String[] args) {
        // Sample string
        String input = "swiss";

        // Create a set to track seen characters
        Set<Character> seen = new HashSet<>();
        Character firstRepeatedChar = null;

        // Find the first repeated character
        for (char c : input.toCharArray()) {
            if (!seen.add(c)) {
                firstRepeatedChar = c;
                break;
            }
        }

        // Print the first repeated character
        System.out.println(firstRepeatedChar != null 
                           ? "First repeated character: " + firstRepeatedChar 
                           : "No repeated character found");
    }
}
```

In this corrected example:
1. A `HashSet` named `seen` is used to keep track of characters that have already been seen.
2. Iterate over each character in the string using a `for` loop.
3. Use the `add` method of `HashSet`. If `add` returns `false`, it means the character is already in the set and is the first repeated character.
4. Print the first repeated character if found, otherwise print a message indicating no repeated character was found.

For the input `"swiss"`, the output would be:
```
First repeated character: s
```

## Find Second repeated character in a given string using java8
To find the second repeated character in a given string using Java 8, you can extend the previous logic slightly by tracking the number of times each character has been repeated and identifying the second one. Here is an example demonstrating how to do this:

```java
import java.util.HashMap;
import java.util.Map;

public class SecondRepeatedCharacter {
    public static void main(String[] args) {
        // Sample string
        String input = "swissmississippi";

        // Create a map to track character counts
        Map<Character, Integer> charCount = new HashMap<>();
        Character secondRepeatedChar = null;
        int repeatedCount = 0;

        // Find the second repeated character
        for (char c : input.toCharArray()) {
            charCount.put(c, charCount.getOrDefault(c, 0) + 1);
            if (charCount.get(c) == 2) {
                repeatedCount++;
                if (repeatedCount == 2) {
                    secondRepeatedChar = c;
                    break;
                }
            }
        }

        // Print the second repeated character
        System.out.println(secondRepeatedChar != null 
                           ? "Second repeated character: " + secondRepeatedChar 
                           : "No second repeated character found");
    }
}
```

In this example:
1. A `HashMap` named `charCount` is used to keep track of the counts of each character.
2. Iterate over each character in the string using a `for` loop.
3. For each character, update its count in the `charCount` map.
4. Check if the character count becomes 2, indicating it is a repeated character.
5. Increment the `repeatedCount` and check if it is the second repeated character.
6. Print the second repeated character if found, otherwise print a message indicating no second repeated character was found.

For the input `"swissmississippi"`, the output would be:
```
Second repeated character: i
```

## Find First longest string in a given array using java8
To find the first longest string in a given array using Java 8, you can use the `Stream` API. Here's how you can do it:

```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public class FirstLongestString {
    public static void main(String[] args) {
        String[] arr = {"java", "programming", "skills", "required"};

        // Find the first longest string
        Optional<String> firstLongest = Arrays.stream(arr)
                .max(Comparator.comparingInt(String::length));

        // Print the first longest string, or "No strings found" if the array is empty
        System.out.println(firstLongest.orElse("No strings found"));
    }
}
```

In this example:
- We use `Arrays.stream(arr)` to create a stream of strings from the array `arr`.
- We use `max(Comparator.comparingInt(String::length))` to find the longest string based on its length.
- Finally, we use `orElse("No strings found")` to handle the case where the array is empty, and print the first longest string or "No strings found" if the array is empty.

You can use the `reduce` operation to find the first longest string in a given array. Here's how you can do it:

```java
import java.util.Arrays;

public class FirstLongestString {
    public static void main(String[] args) {
        String[] arr = {"java", "programming", "skills", "required"};

        // Find the first longest string
        String firstLongest = Arrays.stream(arr)
                .reduce((first, second) -> first.length() >= second.length() ? first : second)
                .orElse("No strings found");

        // Print the first longest string
        System.out.println(firstLongest);
    }
}
```

In this example:
- We use `Arrays.stream(arr)` to create a stream of strings from the array `arr`.
- We use `reduce((first, second) -> first.length() >= second.length() ? first : second)` to find the longest string in the array.
- If the array is empty, `orElse("No strings found")` returns the specified default value ("No strings found").
- Finally, we print the first longest string.

## Find Second longest string in a given array using java8
To find the second longest string in a given array using Java 8, you can sort the array by string length in descending order and then select the second element. Here's how you can do it:

```java
import java.util.Arrays;
import java.util.Comparator;

public class SecondLongestString {
    public static void main(String[] args) {
        String[] arr = {"java", "programming", "skills", "required"};

        // Find the second longest string
        String secondLongest = Arrays.stream(arr)
                .sorted(Comparator.comparingInt(String::length).reversed())
                .skip(1)
                .findFirst()
                .orElse("No second longest string found");

        // Print the second longest string
        System.out.println(secondLongest);
    }
}
```

In this example:
- We use `Arrays.stream(arr)` to create a stream of strings from the array `arr`.
- We use `sorted(Comparator.comparingInt(String::length).reversed())` to sort the strings by length in descending order.
- We use `skip(1)` to skip the first element (which is the longest string).
- We use `findFirst()` to get the first element of the remaining stream, which is the second longest string, or "No second longest string found" if the array has less than two elements.
- Finally, we print the second longest string.

## Find a given string is a palidrome using java8
To determine if a given string is a palindrome using Java 8, you can use the Stream API to reverse the string and then check if the reversed string is equal to the original string. Here is an example demonstrating how to do this:

```java
import java.util.stream.IntStream;

public class PalindromeCheck {
    public static void main(String[] args) {
        // Sample string
        String input = "madam";

        // Check if the string is a palindrome
        boolean isPalindrome = IntStream.range(0, input.length() / 2)
                                        .allMatch(i -> input.charAt(i) == input.charAt(input.length() - i - 1));

        // Print the result
        System.out.println(isPalindrome 
                           ? "The string is a palindrome." 
                           : "The string is not a palindrome.");
    }
}
```

In this example:
1. The `IntStream.range(0, input.length() / 2)` method creates a stream of integers from 0 to half the length of the input string (not inclusive).
2. The `allMatch` method checks if all characters from the start and end of the string match each other.
3. The `input.charAt(i) == input.charAt(input.length() - i - 1)` lambda expression compares characters from the beginning and end of the string.
4. If all characters match, the string is a palindrome; otherwise, it is not.
5. The result is printed, indicating whether the string is a palindrome.

For the input `"madam"`, the output would be:
```
The string is a palindrome.
```

For the input `"hello"`, the output would be:
```
The string is not a palindrome.
```
## Reverse a given string using java8
To reverse a given string using Java 8, you can utilize the Stream API. Here's an example demonstrating how to do this:

```java
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ReverseString {
    public static void main(String[] args) {
        // Sample string
        String input = "hello";

        // Reverse the string using the Stream API
        String reversed = IntStream.rangeClosed(1, input.length())
                                   .mapToObj(i -> input.charAt(input.length() - i))
                                   .map(String::valueOf)
                                   .collect(Collectors.joining());

        // Print the reversed string
        System.out.println("Reversed string: " + reversed);
    }
}
```

In this example:
1. `IntStream.rangeClosed(1, input.length())` creates a stream of integers from 1 to the length of the input string.
2. The `mapToObj(i -> input.charAt(input.length() - i))` method maps each integer to the corresponding character from the end of the string.
3. The `map(String::valueOf)` method converts each character to a string.
4. The `collect(Collectors.joining())` method concatenates the characters into a single string.
5. Finally, the reversed string is printed.

For the input `"hello"`, the output would be:
```
Reversed string: olleh
```
## Find common characters in two string using java8
To find common characters in two strings using Java 8, you can convert the strings to character arrays and then use the `stream()` method to create streams of characters. You can then use the `filter()` method along with the `anyMatch()` method to check if any character in one stream is present in the other stream. Here's an example:

```java
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class CommonCharacters {
    public static void main(String[] args) {
        // Sample strings
        String str1 = "hello";
        String str2 = "world";

        // Convert strings to character arrays
        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();

        // Create a set from arr2 for faster lookup
        Set<Character> set2 = new HashSet<>();
        for (char c : arr2) {
            set2.add(c);
        }

        // Find common characters
        String commonChars = Arrays.stream(arr1)
                                   .distinct()
                                   .filter(set2::contains)
                                   .mapToObj(String::valueOf)
                                   .collect(Collectors.joining());

        // Print the common characters
        System.out.println("Common characters: " + commonChars);
    }
}
```

In this example:
1. The first string (`str1`) is converted to a character array (`arr1`).
2. The second string (`str2`) is converted to a character array (`arr2`).
3. A `HashSet` (`set2`) is created from `arr2` for faster lookup.
4. The `stream()` method is used on `arr1` to create a stream of characters.
5. The `distinct()` method is used to ensure each character is considered only once.
6. The `filter(set2::contains)` method filters the characters to include only those present in `set2`.
7. The `mapToObj(String::valueOf)` method converts each character to a string.
8. The `collect(Collectors.joining())` method concatenates the common characters into a single string.
9. Finally, the common characters are printed.

For the input strings `"hello"` and `"world"`, the output would be:
```
Common characters: lo
```
## Find the words starting with A using java8
To find the words starting with 'A' in a given string using Java 8, you can split the string into words and then filter the words based on whether they start with 'A'. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class WordsStartingWithA {
    public static void main(String[] args) {
        // Sample string
        String sentence = "Apple is a fruit and an Animal starts with the letter A.";

        // Split the string into words
        List<String> words = Arrays.asList(sentence.split("\\s+"));

        // Find words starting with 'A'
        List<String> wordsStartingWithA = words.stream()
                                                .filter(word -> word.startsWith("A") || word.startsWith("a"))
                                                .collect(Collectors.toList());

        // Print the words starting with 'A'
        System.out.println("Words starting with 'A': " + wordsStartingWithA);
    }
}
```

In this example:
1. The `split("\\s+")` method is used to split the input string `sentence` into words.
2. The `stream()` method is used to convert the list of words into a stream.
3. The `filter(word -> word.startsWith("A") || word.startsWith("a"))` method is used to filter out words that start with 'A' (uppercase or lowercase).
4. The `collect(Collectors.toList())` method is used to collect the filtered words into a list.
5. Finally, the list of words starting with 'A' is printed.

For the input sentence `"Apple is a fruit and an Animal starts with the letter A."`, the output would be:
```
Words starting with 'A': [Apple, an, Animal, A.]
```
## First the First largest number in a given array using java8
To find the largest number in a given array using Java 8, you can use the Stream API. Here's an example:

```java
import java.util.Arrays;

public class LargestNumberInArray {
    public static void main(String[] args) {
        // Sample array
        int[] numbers = {10, 4, 7, 19, 25, 13, 8};

        // Find the largest number
        int largestNumber = Arrays.stream(numbers)
                                  .max()
                                  .orElseThrow(); // Throws NoSuchElementException if array is empty

        // Print the largest number
        System.out.println("Largest number: " + largestNumber);
    }
}
```

In this example:
1. The `Arrays.stream(numbers)` method is used to convert the array `numbers` into an `IntStream`.
2. The `max()` method is used to find the maximum value in the stream.
3. The `orElseThrow()` method is used to get the result as an `int` or throw a `NoSuchElementException` if the array is empty.
4. Finally, the largest number is printed.

For the input array `{10, 4, 7, 19, 25, 13, 8}`, the output would be:
```
Largest number: 25
```
## Find the Second largest number in a given array using java8
To find the second largest number in a given array using Java 8, you can sort the array in descending order and then select the element at index 1. Here's an example:

```java
import java.util.Arrays;

public class SecondLargestNumberInArray {
    public static void main(String[] args) {
        // Sample array
        int[] numbers = {10, 4, 7, 19, 25, 13, 8};

        // Find the second largest number
        int secondLargestNumber = Arrays.stream(numbers)
                                        .boxed() // Convert int to Integer
                                        .sorted((a, b) -> b.compareTo(a)) // Sort in descending order
                                        .skip(1) // Skip the first element (largest number)
                                        .findFirst() // Get the second largest number
                                        .orElseThrow(); // Throws NoSuchElementException if array has less than 2 elements

        // Print the second largest number
        System.out.println("Second largest number: " + secondLargestNumber);
    }
}
```

In this example:
1. The `Arrays.stream(numbers)` method converts the `numbers` array to an `IntStream`.
2. The `boxed()` method is used to convert the `IntStream` to a `Stream<Integer>`.
3. The `sorted((a, b) -> b.compareTo(a))` method sorts the stream in descending order.
4. The `skip(1)` method skips the first element (largest number).
5. The `findFirst()` method gets the first element of the remaining stream (second largest number).
6. The `orElseThrow()` method is used to get the result as an `int` or throw a `NoSuchElementException` if the array has less than 2 elements.
7. Finally, the second largest number is printed.

For the input array `{10, 4, 7, 19, 25, 13, 8}`, the output would be:
```
Second largest number: 19
```
## Find the Third largest number in a given array using java8
To find the third largest number in a given array using Java 8, you can modify the approach used to find the second largest number. You can sort the array in descending order, skip the first two elements (largest and second largest numbers), and then select the element at index 2. Here's an example:

```java
import java.util.Arrays;

public class ThirdLargestNumberInArray {
    public static void main(String[] args) {
        // Sample array
        int[] numbers = {10, 4, 7, 19, 25, 13, 8};

        // Find the third largest number
        int thirdLargestNumber = Arrays.stream(numbers)
                                       .boxed() // Convert int to Integer
                                       .sorted((a, b) -> b.compareTo(a)) // Sort in descending order
                                       .skip(2) // Skip the first two elements (largest and second largest numbers)
                                       .findFirst() // Get the third largest number
                                       .orElseThrow(); // Throws NoSuchElementException if array has less than 3 elements

        // Print the third largest number
        System.out.println("Third largest number: " + thirdLargestNumber);
    }
}
```

In this example:
1. The `Arrays.stream(numbers)` method converts the `numbers` array to an `IntStream`.
2. The `boxed()` method is used to convert the `IntStream` to a `Stream<Integer>`.
3. The `sorted((a, b) -> b.compareTo(a))` method sorts the stream in descending order.
4. The `skip(2)` method skips the first two elements (largest and second largest numbers).
5. The `findFirst()` method gets the first element of the remaining stream (third largest number).
6. The `orElseThrow()` method is used to get the result as an `int` or throw a `NoSuchElementException` if the array has less than 3 elements.
7. Finally, the third largest number is printed.

For the input array `{10, 4, 7, 19, 25, 13, 8}`, the output would be:
```
Third largest number: 13
```
## Find only duplicate elements with its count from the String list using java8
To find only the duplicate elements along with their counts from a `List<String>` using Java 8, you can use the `Collectors.groupingBy` collector along with `Collectors.counting()`. Here's how you can do it:

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DuplicateElementsWithCount {
    public static void main(String[] args) {
        // Sample list
        List<String> names = Arrays.asList("AA", "BB", "AA", "CC");

        // Find duplicate elements with count
        Map<String, Long> duplicateElementsWithCount = names.stream()
                .collect(Collectors.groupingBy(name -> name, Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        // Print duplicate elements with count
        duplicateElementsWithCount.forEach((name, count) -> System.out.println(name + ": " + count));
    }
}
```

In this example:
1. The `Collectors.groupingBy(name -> name, Collectors.counting())` collector groups the elements by their value (name) and counts the occurrences of each element.
2. The resulting map is filtered to include only those entries where the count is greater than 1, indicating duplicates.
3. The `Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)` collector is used to collect the filtered entries into a new map, retaining the key (name) and value (count).
4. Finally, the map containing duplicate elements along with their counts is printed.

For the input list `["AA", "BB", "AA", "CC"]`, the output would be:
```
AA: 2
```
## Find the highest duplicate count from a given string using java?
To find the highest duplicate element(s) from a given list of strings in Java, you can follow a similar approach as before. Here's how you can do it:

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class HighestDuplicateElement {
    public static void main(String[] args) {
        // Sample list
        List<String> names = Arrays.asList("AA", "BB", "AA", "CC", "BB", "CC", "AA");

        // Find the highest duplicate element(s)
        Map<String, Long> counts = names.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        // Find the highest count
        long maxCount = counts.values().stream()
                .max(Long::compare)
                .orElse(0L);

        // Filter elements with the highest count
        Map<String, Long> highestDuplicates = counts.entrySet().stream()
                .filter(entry -> entry.getValue() == maxCount)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        // Print the highest duplicate element(s)
        highestDuplicates.forEach((name, count) -> System.out.println(name + ": " + count));
    }
}
```

In this example:
1. The list of names is converted to a stream.
2. The `Collectors.groupingBy(Function.identity(), Collectors.counting())` collector is used to group the names by their value and count their occurrences.
3. The `values().stream().max(Long::compare)` method is used to find the highest count among all names.
4. The names with the highest count are filtered and collected into a map using `Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)`.
5. Finally, the highest duplicate element(s) along with their count are printed.

For the input list `["AA", "BB", "AA", "CC", "BB", "CC", "AA"]`, the output would be:
```
AA: 3
```
## Perform cube on list elements and filter numbers greater than 50 using java8
To perform cube on elements of a list and filter numbers greater than 50 using Java 8, you can use the Stream API. Here's how you can do it:

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CubeAndFilter {
    public static void main(String[] args) {
        // Sample list
        List<Integer> numbers = Arrays.asList(2, 5, 10, 8, 7, 3, 6);

        // Perform cube on elements and filter numbers greater than 50
        List<Integer> result = numbers.stream()
                .map(n -> n * n * n) // Cube of each element
                .filter(n -> n > 50) // Filter numbers greater than 50
                .collect(Collectors.toList()); // Collect the result into a list

        // Print the result
        System.out.println("Numbers greater than 50 after cubing: " + result);
    }
}
```

In this example:
1. The `map(n -> n * n * n)` operation cubes each element of the list.
2. The `filter(n -> n > 50)` operation filters out numbers greater than 50.
3. The `collect(Collectors.toList())` operation collects the filtered elements into a new list.
4. Finally, the result (numbers greater than 50 after cubing) is printed.

For the input list `[2, 5, 10, 8, 7, 3, 6]`, the output would be:
```
Numbers greater than 50 after cubing: [125, 1000, 512]
```
## Generate the fibanocci series using java8
You can generate the Fibonacci series using Java 8 with the help of streams. Here's an example:

```java
import java.util.stream.Stream;

public class FibonacciSeries {
    public static void main(String[] args) {
        int n = 10; // Number of Fibonacci numbers to generate
        System.out.println("Fibonacci series:");
        Stream.iterate(new int[]{0, 1}, fib -> new int[]{fib[1], fib[0] + fib[1]})
              .limit(n)
              .forEach(fib -> System.out.print(fib[0] + " "));
    }
}
```

In this example:
- `Stream.iterate(new int[]{0, 1}, fib -> new int[]{fib[1], fib[0] + fib[1]})` generates an infinite stream of Fibonacci number pairs.
- `limit(n)` limits the stream to the first `n` elements.
- `forEach(fib -> System.out.print(fib[0] + " "))` prints each Fibonacci number in the series.

For `n = 10`, the output would be:
```
Fibonacci series:
0 1 1 2 3 5 8 13 21 34
```
## Print sum of all numbers using java8
To calculate the sum of all numbers in a list using Java 8, you can use the `sum()` method provided by the `Stream` interface. Here's an example:

```java
import java.util.Arrays;
import java.util.List;

public class SumOfNumbers {
    public static void main(String[] args) {
        // Sample list
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        // Calculate the sum of all numbers
        int sum = numbers.stream()
                         .mapToInt(Integer::intValue) // Convert Integer to int
                         .sum();

        // Print the sum
        System.out.println("Sum of all numbers: " + sum);
    }
}
```

In this example:
1. The `mapToInt(Integer::intValue)` operation converts each `Integer` object to an `int`.
2. The `sum()` operation calculates the sum of all `int` values in the stream.
3. Finally, the sum is printed.

For the input list `[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]`, the output would be:
```
Sum of all numbers: 55
```
## Find Vowels and Consonants in a given string using java8
To find vowels and consonants in a given string using Java 8, you can use the `filter` method along with `chars` to create a stream of characters, and then use `anyMatch` to check if a character is a vowel. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class VowelsAndConsonants {
    public static void main(String[] args) {
        // Sample string
        String input = "Hello, World!";

        // Convert the input string to lowercase for case-insensitive matching
        input = input.toLowerCase();

        // Define a predicate for vowels
        Predicate<Character> isVowel = c -> "aeiou".indexOf(c) != -1;

        // Create a map to store counts of vowels and consonants
        Map<String, Long> result = Arrays.stream(input.split(""))
                .filter(s -> !s.isEmpty()) // Filter out empty strings
                .map(s -> s.charAt(0)) // Convert strings to characters
                .collect(Collectors.groupingBy(
                        c -> isVowel.test(c) ? "vowels" : "consonants",
                        Collectors.counting()
                ));

        // Print the result
        System.out.println("Counts of vowels and consonants:");
        result.forEach((type, count) -> System.out.println(type + ": " + count));
    }
}
```

In this example:
- The input string is converted to lowercase to make the comparison case-insensitive.
- A predicate `isVowel` is defined to check if a character is a vowel.
- The input string is split into an array of strings, each containing one character.
- The array is converted to a stream of characters using `map` and `charAt(0)`.
- The characters are grouped into vowels and consonants using the `groupingBy` collector.
- Finally, the counts of vowels and consonants are printed.

For the input string `"Hello, World!"`, the output would be:
```
Counts of vowels and consonants:
vowels: 3
consonants: 8
```
## Sort the elements in a given array in ascending order using java8
To sort the elements in an array in ascending order using Java 8, you can use the `Arrays.sort()` method. Here's an example:

```java
import java.util.Arrays;

public class SortArray {
    public static void main(String[] args) {
        // Sample array
        int[] numbers = {5, 2, 8, 3, 1, 6, 4, 7};

        // Sort the array in ascending order
        Arrays.sort(numbers);

        // Print the sorted array
        System.out.println("Sorted array in ascending order:");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }
}
```

In this example:
- The `Arrays.sort(numbers)` method is used to sort the `numbers` array in ascending order.
- The sorted array is then printed.

For the input array `{5, 2, 8, 3, 1, 6, 4, 7}`, the output would be:
```
Sorted array in ascending order:
1 2 3 4 5 6 7 8
```

## Sort the elements in a given array in descending order using java8
To sort the elements in an array in descending order using Java 8, you can use the `Arrays.sort()` method along with a custom comparator that sorts elements in reverse order. Here's an example:

```java
import java.util.Arrays;
import java.util.Collections;

public class SortArrayDescending {
    public static void main(String[] args) {
        // Sample array
        Integer[] numbers = {5, 2, 8, 3, 1, 6, 4, 7};

        // Sort the array in descending order
        Arrays.sort(numbers, Collections.reverseOrder());

        // Print the sorted array
        System.out.println("Sorted array in descending order:");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }
}
```

In this example:
- The `Arrays.sort(numbers, Collections.reverseOrder())` method is used to sort the `numbers` array in descending order.
- The `Collections.reverseOrder()` method returns a comparator that sorts elements in reverse natural order (descending order for integers).
- The sorted array is then printed.

For the input array `{5, 2, 8, 3, 1, 6, 4, 7}`, the output would be:
```
Sorted array in descending order:
8 7 6 5 4 3 2 1
```

## Filter employee list whose gender is male and salary is less than 50000
To filter an employee list based on gender (male) and salary (less than 50000) using Java 8, you can use the `filter` method along with a predicate that checks both conditions. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterEmployees {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = Arrays.asList(
                new Employee("Alice", Gender.FEMALE, 45000),
                new Employee("Bob", Gender.MALE, 55000),
                new Employee("Charlie", Gender.MALE, 48000),
                new Employee("Diana", Gender.FEMALE, 51000),
                new Employee("Eve", Gender.FEMALE, 49000)
        );

        // Filter employees by gender (male) and salary (less than 50000)
        List<Employee> filteredEmployees = employees.stream()
                .filter(emp -> emp.getGender() == Gender.MALE && emp.getSalary() < 50000)
                .collect(Collectors.toList());

        // Print the filtered employees
        System.out.println("Male employees with salary less than 50000:");
        filteredEmployees.forEach(emp -> System.out.println("- " + emp.getName()));
    }

    static class Employee {
        private String name;
        private Gender gender;
        private int salary;

        public Employee(String name, Gender gender, int salary) {
            this.name = name;
            this.gender = gender;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public Gender getGender() {
            return gender;
        }

        public int getSalary() {
            return salary;
        }
    }

    enum Gender {
        MALE, FEMALE
    }
}
```

In this example:
- The `Employee` class represents an employee with a name, gender, and salary.
- The `Gender` enum represents the gender of an employee.
- The list of employees is filtered using the `filter` method, which takes a predicate that checks if the employee is male and has a salary less than 50000.
- The filtered employees are collected into a new list and printed.

For the sample list of employees, the output would be:
```
Male employees with salary less than 50000:
- Charlie
```

## Filter employee list and group based on gender using java8
To filter an employee list and group it based on gender using Java 8, you can use the `Collectors.groupingBy` collector along with a classifier function. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupEmployeesByGender {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = Arrays.asList(
                new Employee("Alice", 30, Gender.FEMALE),
                new Employee("Bob", 25, Gender.MALE),
                new Employee("Charlie", 35, Gender.MALE),
                new Employee("Diana", 28, Gender.FEMALE),
                new Employee("Eve", 40, Gender.FEMALE)
        );

        // Group employees by gender
        Map<Gender, List<Employee>> groupedByGender = employees.stream()
                .collect(Collectors.groupingBy(Employee::getGender));

        // Print the result
        groupedByGender.forEach((gender, empList) -> {
            System.out.println("Employees with gender " + gender + ":");
            empList.forEach(emp -> System.out.println("- " + emp.getName()));
        });
    }

    static class Employee {
        private String name;
        private int age;
        private Gender gender;

        public Employee(String name, int age, Gender gender) {
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public Gender getGender() {
            return gender;
        }
    }

    enum Gender {
        MALE, FEMALE
    }
}
```

In this example:
- The `Employee` class represents an employee with a name, age, and gender.
- The `Gender` enum represents the gender of an employee.
- The list of employees is grouped by gender using the `Collectors.groupingBy(Employee::getGender)` collector.
- The result is printed, showing the employees grouped by gender.

For the sample list of employees, the output would be:
```
Employees with gender MALE:
- Bob
- Charlie
Employees with gender FEMALE:
- Alice
- Diana
- Eve
```

## Convert Map to List using java8
To convert a `Map` to a `List` using Java 8, you can use the `Collectors.toList()` collector along with the `Map.entrySet().stream()` method. Each entry in the map can be transformed into an object and collected into a list. Here's an example:

```java
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapToList {
    public static void main(String[] args) {
        // Sample map
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");

        // Convert map to list of Map.Entry objects
        List<Map.Entry<Integer, String>> list = map.entrySet().stream()
                .collect(Collectors.toList());

        // Print the list
        list.forEach(entry -> System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue()));
    }
}
```

In this example:
- The `map.entrySet().stream()` method is used to convert the map to a stream of map entries.
- The `Collectors.toList()` collector is used to collect the map entries into a list.
- Each element in the resulting list is a `Map.Entry<Integer, String>` object representing a key-value pair from the original map.

The output would be:
```
Key: 1, Value: One
Key: 2, Value: Two
Key: 3, Value: Three
```
## Convert List to Map using java8
To convert a `List` to a `Map` using Java 8, you can use the `Collectors.toMap()` collector. You need to specify how to extract the key and value for each element of the list. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListToMap {
    public static void main(String[] args) {
        // Sample list
        List<String> list = Arrays.asList("apple", "banana", "orange");

        // Convert list to map with the length of the string as key and the string as value
        Map<Integer, String> map = list.stream()
                .collect(Collectors.toMap(String::length, str -> str));

        // Print the map
        map.forEach((key, value) -> System.out.println("Key: " + key + ", Value: " + value));
    }
}
```

In this example:
- The `String::length` method reference is used to extract the length of each string as the key for the map.
- The `str -> str` lambda function is used to specify the string itself as the value for the map.
- The `Collectors.toMap()` collector is used to collect the elements of the stream into a map.

The output would be:
```
Key: 5, Value: apple
Key: 6, Value: banana
Key: 6, Value: orange
```

Note: In this example, the keys are the lengths of the strings, so some strings may have the same length and will be mapped to the same key. If you want to avoid this, you can provide a merge function as an additional argument to `toMap()` to resolve conflicts.

## Sort Product List using Category and Price using java8
To sort a list of products by category and then by price using Java 8, you can use the `Comparator` interface along with the `sorted()` method of the `Stream` API. Here's an example:

```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortProductList {
    public static void main(String[] args) {
        // Sample list of products
        List<Product> products = Arrays.asList(
                new Product("Apple", "Fruit", 2.5),
                new Product("Orange", "Fruit", 2.0),
                new Product("Carrot", "Vegetable", 1.0),
                new Product("Broccoli", "Vegetable", 1.5),
                new Product("Chicken", "Meat", 5.0),
                new Product("Beef", "Meat", 7.0)
        );

        // Sort the list of products by category and then by price
        products.sort(Comparator.comparing(Product::getCategory).thenComparing(Product::getPrice));

        // Print the sorted list of products
        products.forEach(System.out::println);
    }

    static class Product {
        private String name;
        private String category;
        private double price;

        public Product(String name, String category, double price) {
            this.name = name;
            this.category = category;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public String getCategory() {
            return category;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return "Product{" +
                    "name='" + name + '\'' +
                    ", category='" + category + '\'' +
                    ", price=" + price +
                    '}';
        }
    }
}
```

In this example:
- The `Product` class represents a product with a name, category, and price.
- The `products` list is sorted using the `sort` method, which takes a comparator created using `Comparator.comparing()` and `thenComparing()` to specify the sorting order by category and then by price.
- Finally, the sorted list of products is printed.

The output would be:
```
Product{name='Carrot', category='Vegetable', price=1.0}
Product{name='Broccoli', category='Vegetable', price=1.5}
Product{name='Orange', category='Fruit', price=2.0}
Product{name='Apple', category='Fruit', price=2.5}
Product{name='Chicken', category='Meat', price=5.0}
Product{name='Beef', category='Meat', price=7.0}
```
## Find Average price of a Product whose category is Fruit
To find the average price of products whose category is "Fruit" using Java 8, you can filter the list of products by category, calculate the average using `Collectors.averagingDouble()`, and then retrieve the average price. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AveragePriceOfFruit {
    public static void main(String[] args) {
        // Sample list of products
        List<Product> products = Arrays.asList(
                new Product("Apple", "Fruit", 2.5),
                new Product("Orange", "Fruit", 2.0),
                new Product("Carrot", "Vegetable", 1.0),
                new Product("Broccoli", "Vegetable", 1.5),
                new Product("Chicken", "Meat", 5.0),
                new Product("Beef", "Meat", 7.0)
        );

        // Calculate the average price of products with category "Fruit"
        double averagePrice = products.stream()
                .filter(product -> product.getCategory().equals("Fruit"))
                .collect(Collectors.averagingDouble(Product::getPrice));

        // Print the average price
        System.out.println("Average price of Fruit: " + averagePrice);
    }

    static class Product {
        private String name;
        private String category;
        private double price;

        public Product(String name, String category, double price) {
            this.name = name;
            this.category = category;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public String getCategory() {
            return category;
        }

        public double getPrice() {
            return price;
        }
    }
}
```

In this example:
- The `Product` class represents a product with a name, category, and price.
- The `products` list is filtered to include only products with the category "Fruit".
- The `Collectors.averagingDouble(Product::getPrice)` collector calculates the average price of the filtered products.
- The average price is printed.

For the sample list of products, the output would be:
```
Average price of Fruit: 2.25
```
## Given a product list, Find the count of product of particular category whose inventory is available.
To find the count of products of a particular category whose inventory is available, you can filter the list of products by the category and inventory availability, and then count the number of products. Here's an example:

```java
import java.util.Arrays;
import java.util.List;

public class ProductCount {
    public static void main(String[] args) {
        // Sample list of products
        List<Product> products = Arrays.asList(
                new Product("Apple", "Fruit", true),
                new Product("Orange", "Fruit", false),
                new Product("Carrot", "Vegetable", true),
                new Product("Broccoli", "Vegetable", true),
                new Product("Chicken", "Meat", false),
                new Product("Beef", "Meat", true)
        );

        // Category and inventory availability to search for
        String category = "Fruit";
        boolean inventoryAvailable = true;

        // Count of products of a particular category with available inventory
        long count = products.stream()
                .filter(product -> product.getCategory().equals(category))
                .filter(Product::isInventoryAvailable)
                .count();

        // Print the count
        System.out.println("Count of " + category + " products with available inventory: " + count);
    }

    static class Product {
        private String name;
        private String category;
        private boolean inventoryAvailable;

        public Product(String name, String category, boolean inventoryAvailable) {
            this.name = name;
            this.category = category;
            this.inventoryAvailable = inventoryAvailable;
        }

        public String getCategory() {
            return category;
        }

        public boolean isInventoryAvailable() {
            return inventoryAvailable;
        }
    }
}
```

In this example:
- The `Product` class represents a product with a name, category, and inventory availability.
- The `products` list is filtered to include only products of the specified category with available inventory.
- The `count()` method is used to count the number of filtered products.
- The count is printed.

For the sample list of products, the output would be:
```
Count of Fruit products with available inventory: 1
```

## Given Each Product have List of Sku, Return all the skus whose inventory is out of stock
To get all the SKUs whose inventory is out of stock from a list of products, where each product contains a list of SKUs, you can use a nested stream to iterate over each product and its associated SKUs. Here's an example:

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OutOfStockSKUs {
    public static void main(String[] args) {
        // Sample list of products with SKUs
        List<Product> products = Arrays.asList(
                new Product("Product1", Arrays.asList(
                        new Sku("SKU001", 10),
                        new Sku("SKU002", 0),
                        new Sku("SKU003", 5)
                )),
                new Product("Product2", Arrays.asList(
                        new Sku("SKU004", 0),
                        new Sku("SKU005", 15),
                        new Sku("SKU006", 0)
                ))
        );

        // Get all SKUs whose inventory is out of stock
        List<Sku> outOfStockSKUs = products.stream()
                .flatMap(product -> product.getSkus().stream()) // FlatMap to stream of SKUs
                .filter(sku -> sku.getInventory() == 0)
                .collect(Collectors.toList());

        // Print the out-of-stock SKUs
        outOfStockSKUs.forEach(System.out::println);
    }

    static class Product {
        private String name;
        private List<Sku> skus;

        public Product(String name, List<Sku> skus) {
            this.name = name;
            this.skus = skus;
        }

        public String getName() {
            return name;
        }

        public List<Sku> getSkus() {
            return skus;
        }
    }

    static class Sku {
        private String skuId;
        private int inventory;

        public Sku(String skuId, int inventory) {
            this.skuId = skuId;
            this.inventory = inventory;
        }

        public String getSkuId() {
            return skuId;
        }

        public int getInventory() {
            return inventory;
        }

        @Override
        public String toString() {
            return "Sku{" +
                    "skuId='" + skuId + '\'' +
                    ", inventory=" + inventory +
                    '}';
        }
    }
}
```

In this example:
- The `Product` class represents a product with a name and a list of SKUs.
- The `Sku` class represents a SKU with an SKU ID and an inventory count.
- We use `flatMap` to convert the stream of products into a stream of SKUs, so that we can filter the SKUs based on their inventory.
- The `filter` method is used to filter out SKUs with an inventory of 0 (out of stock).
- The result is a list of SKUs whose inventory is out of stock.

The output would be:
```
Sku{skuId='SKU002', inventory=0}
Sku{skuId='SKU004', inventory=0}
Sku{skuId='SKU006', inventory=0}
```

## Given Each Product have List of Sku, Return all the skus whose inventory is out of stock for each product
If each product contains a list of SKUs, and you want to find all the SKUs whose inventory is out of stock for each product, you can modify the approach slightly to iterate over each product and its associated SKUs. Here's an example:

```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OutOfStockSKUs {
    public static void main(String[] args) {
        // Sample list of products with SKUs
        List<Product> products = Arrays.asList(
                new Product("Product1", Arrays.asList(
                        new Sku("SKU001", 10),
                        new Sku("SKU002", 0),
                        new Sku("SKU003", 5)
                )),
                new Product("Product2", Arrays.asList(
                        new Sku("SKU004", 0),
                        new Sku("SKU005", 15),
                        new Sku("SKU006", 0)
                ))
        );

        // Get SKUs whose inventory is out of stock for each product
        List<Product> productsWithOutOfStockSKUs = products.stream()
                .map(product -> {
                    List<Sku> outOfStockSkus = product.getSkus().stream()
                            .filter(sku -> sku.getInventory() == 0)
                            .collect(Collectors.toList());
                    return new Product(product.getName(), outOfStockSkus);
                })
                .collect(Collectors.toList());

        // Print the products with out-of-stock SKUs
        productsWithOutOfStockSKUs.forEach(System.out::println);
    }

    static class Product {
        private String name;
        private List<Sku> skus;

        public Product(String name, List<Sku> skus) {
            this.name = name;
            this.skus = skus;
        }

        public String getName() {
            return name;
        }

        public List<Sku> getSkus() {
            return skus;
        }

        @Override
        public String toString() {
            return "Product{" +
                    "name='" + name + '\'' +
                    ", skus=" + skus +
                    '}';
        }
    }

    static class Sku {
        private String skuId;
        private int inventory;

        public Sku(String skuId, int inventory) {
            this.skuId = skuId;
            this.inventory = inventory;
        }

        public String getSkuId() {
            return skuId;
        }

        public int getInventory() {
            return inventory;
        }

        @Override
        public String toString() {
            return "Sku{" +
                    "skuId='" + skuId + '\'' +
                    ", inventory=" + inventory +
                    '}';
        }
    }
}
```

In this example:
- The `Product` class represents a product with a name and a list of SKUs.
- The `Sku` class represents a SKU with an SKU ID and an inventory count.
- For each product, we filter its list of SKUs to include only SKUs with an inventory count of 0 (out of stock).
- We then create a new `Product` object for each product with the filtered list of out-of-stock SKUs.
- The result is a list of products, each containing only the out-of-stock SKUs.

The output would be:
```
Product{name='Product1', skus=[Sku{skuId='SKU002', inventory=0}]}
Product{name='Product2', skus=[Sku{skuId='SKU004', inventory=0}, Sku{skuId='SKU006', inventory=0}]}
```

## Given an Employee list, Return the Employee who is having max and min salary
To get the employee with the maximum and minimum salary from a list of employees, you can use the `Collections.max()` and `Collections.min()` methods, passing in a custom comparator to compare employees based on their salaries. Here's an example:

```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MaxMinSalaryEmployee {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = Arrays.asList(
                new Employee("Alice", 50000),
                new Employee("Bob", 60000),
                new Employee("Charlie", 45000),
                new Employee("David", 70000)
        );

        // Get the employee with the maximum salary
        Employee maxSalaryEmployee = employees.stream()
                .max(Comparator.comparing(Employee::getSalary))
                .orElse(null);

        // Get the employee with the minimum salary
        Employee minSalaryEmployee = employees.stream()
                .min(Comparator.comparing(Employee::getSalary))
                .orElse(null);

        // Print the results
        System.out.println("Employee with max salary: " + maxSalaryEmployee);
        System.out.println("Employee with min salary: " + minSalaryEmployee);
    }

    static class Employee {
        private String name;
        private int salary;

        public Employee(String name, int salary) {
            this.name = name;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public int getSalary() {
            return salary;
        }

        @Override
        public String toString() {
            return "Employee{" +
                    "name='" + name + '\'' +
                    ", salary=" + salary +
                    '}';
        }
    }
}
```

In this example:
- The `Employee` class represents an employee with a name and a salary.
- We use `Comparator.comparing(Employee::getSalary)` to create a comparator based on the employee's salary.
- We then use `max()` and `min()` to find the employee with the maximum and minimum salary, respectively.
- Finally, we print the employees with the maximum and minimum salaries.

The output would be:
```
Employee with max salary: Employee{name='David', salary=70000}
Employee with min salary: Employee{name='Charlie', salary=45000}
```

## Given an Employee List, Return the maximum salary of an employee from each department
To return the maximum salary of an employee from each department from a list of employees, you can use Java streams and group the employees by department, then find the maximum salary in each group. Here's an example:

```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MaxSalaryByDepartment {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = Arrays.asList(
                new Employee("Alice", "HR", 50000),
                new Employee("Bob", "IT", 60000),
                new Employee("Charlie", "HR", 45000),
                new Employee("David", "IT", 70000),
                new Employee("Eve", "IT", 55000)
        );

        // Return the maximum salary of an employee from each department
        Map<String, Employee> maxEmployeeByDepartment = employees.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,
                        Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparing(Employee::getSalary)), Optional::get)));

        // Print the maximum salary of an employee from each department
        maxEmployeeByDepartment.forEach((department, maxSalary) -> System.out.println("Department: " + department + ", Max Salary: " + maxSalary));

        // Return the maximum salary of an employee from each department
        Map<String, Integer> maxSalaryByDepartment = employees.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,
                        Collectors.mapping(Employee::getSalary, Collectors.collectingAndThen(Collectors.maxBy(Comparator.naturalOrder()), Optional::get))));

        // Print the maximum salary of an employee from each department
        maxSalaryByDepartment.forEach((department, maxSalary) -> System.out.println("Department: " + department + ", Max Salary: " + maxSalary));
    }

    static class Employee {
        private String name;
        private String department;
        private int salary;

        public Employee(String name, String department, int salary) {
            this.name = name;
            this.department = department;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public String getDepartment() {
            return department;
        }

        public int getSalary() {
            return salary;
        }
    }
}
```

In this example:
- The `Employee` class represents an employee with a name, department, and salary.
- We use `Collectors.groupingBy` to group the employees by department.
- For each group, we use `Collectors.mapping` to extract the salaries of the employees in that department.
- We then use `Collectors.collectingAndThen` to find the maximum salary in each group using `Collectors.maxBy(Comparator.naturalOrder())`.
- Finally, we print the maximum salary of an employee from each department.

The output would be:
```
Department: IT, Max Salary: 70000
Department: HR, Max Salary: 50000
```

You can also achieve the same result using `Collectors.reducing` to find the maximum salary for each department. Here's how you can do it:

```java
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class MaxSalaryByDepartmentReducing {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = Arrays.asList(
                new Employee("Alice", "HR", 50000),
                new Employee("Bob", "IT", 60000),
                new Employee("Charlie", "HR", 45000),
                new Employee("David", "IT", 70000),
                new Employee("Eve", "IT", 55000)
        );

        // Return the maximum salary of an employee from each department using reducing
        Map<String, Integer> maxSalaryByDepartment = employees.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,
employee                        Collectors.reducing(0, Employee::getSalary, Integer::max)));

       //Approach-2 to get employee with maximum salary from each department
        Map<String, Optional<Employee>> employeeMap = employees.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,
                        Collectors.reducing(BinaryOperator.maxBy(Comparator.comparing(Employee::getSalary)))));

        // Print the maximum salary of an employee from each department
        maxSalaryByDepartment.forEach((department, maxSalary) -> System.out.println("Department: " + department + ", Max Salary: " + maxSalary));
    }

    static class Employee {
        private String name;
        private String department;
        private int salary;

        public Employee(String name, String department, int salary) {
            this.name = name;
            this.department = department;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public String getDepartment() {
            return department;
        }

        public int getSalary() {
            return salary;
        }
    }
}
```

In this example, we use `Collectors.reducing` to find the maximum salary for each department. The `reducing` collector takes three arguments:

1. The identity value (`0` in this case) which is the initial value when no elements are present in a group.
2. The mapping function (`Employee::getSalary`) to extract the salary of an employee.
3. The accumulator function (`Integer::max`) to find the maximum salary.

The result is a map containing the maximum salary of an employee from each department.

## Given a Product list, Return the maximum price of a product from each category
To return the maximum price of a product from each category from a list of products, you can use Java streams to group the products by category and then find the product with the maximum price in each group. Here's how you can do it:

```java
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MaxPriceProductByCategory {
    public static void main(String[] args) {
        // Sample list of products
        List<Product> products = Arrays.asList(
                new Product("Apple", "Fruit", 2.5),
                new Product("Banana", "Fruit", 1.5),
                new Product("Carrot", "Vegetable", 1.0),
                new Product("Broccoli", "Vegetable", 1.2),
                new Product("Milk", "Dairy", 3.0),
                new Product("Cheese", "Dairy", 5.0)
        );

        // Return the product with the maximum price from each category
        Map<String, Product> maxPriceProductByCategory = products.stream()
                .collect(Collectors.groupingBy(Product::getCategory,
                        Collectors.collectingAndThen(
                                Collectors.maxBy(Comparator.comparingDouble(Product::getPrice)),
                                product -> product.orElse(null)
                        )));

        // Print the product with the maximum price from each category
        maxPriceProductByCategory.forEach((category, product) ->
                System.out.println("Category: " + category + ", Max Price Product: " + product.getName() +
                        " (Price: " + product.getPrice() + ")"));
    }

    static class Product {
        private String name;
        private String category;
        private double price;

        public Product(String name, String category, double price) {
            this.name = name;
            this.category = category;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public String getCategory() {
            return category;
        }

        public double getPrice() {
            return price;
        }
    }
}
```

In this example:
- The `Product` class represents a product with a name, category, and price.
- We use `Collectors.groupingBy` to group the products by category.
- For each group, we use `Collectors.maxBy` to find the product with the maximum price.
- We then use `Collectors.collectingAndThen` to unwrap the `Optional<Product>` returned by `maxBy` and get the product directly.
- Finally, we print the product with the maximum price from each category.

The output would be:
```
Category: Dairy, Max Price Product: Cheese (Price: 5.0)
Category: Fruit, Max Price Product: Apple (Price: 2.5)
Category: Vegetable, Max Price Product: Broccoli (Price: 1.2)
```

## Sort the Map by key and value using java8
To sort a `Map` by its keys or values using Java 8, you can use the `entrySet()` method along with the `stream()` method to create a stream of entries, and then use the `sorted()` method with a comparator to sort the entries. Here's an example of sorting a map by its keys and values:

```java
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SortMap {
    public static void main(String[] args) {
        // Sample map
        Map<String, Integer> map = new HashMap<>();
        map.put("John", 25);
        map.put("Alice", 30);
        map.put("Bob", 20);

        // Sort the map by keys
        Map<String, Integer> sortedByKey = map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        System.out.println("Sorted by key:");
        sortedByKey.forEach((key, value) -> System.out.println(key + ": " + value));

        // Sort the map by values
        Map<String, Integer> sortedByValue = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        System.out.println("\nSorted by value:");
        sortedByValue.forEach((key, value) -> System.out.println(key + ": " + value));
    }
}
```

In this example:
- We first create a sample `HashMap` called `map`.
- To sort by keys, we use `sorted(Map.Entry.comparingByKey())` which sorts the entries by keys in natural order (alphabetical order for strings).
- To sort by values, we use `sorted(Map.Entry.comparingByValue())` which sorts the entries by values in natural order (ascending order for numbers).
- We collect the sorted entries back into a map using `Collectors.toMap()` with a `LinkedHashMap` to maintain the order.
- Finally, we print the sorted maps to see the result.

## Given an Employee List, Find the Nth Highest Salary from an Employee whose salary may be duplicated
To find the Nth highest salary from a list of employees, you can use Java streams to sort the employees by salary in descending order and then skip the first N-1 employees to get the Nth highest salary. Here's how you can do it:

```java
package com.java.stream;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeesWithNthHighestSalary {

    public static void main(String[] args) {

        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Alice", 50000));
        employees.add(new Employee("Bob", 60000));
        employees.add(new Employee("Charlie", 45000));
        employees.add(new Employee("David", 70000));
        employees.add(new Employee("Eve", 55000));
        employees.add(new Employee("Frank", 55000));
        employees.add(new Employee("George", 60000));
        employees.add(new Employee("Henry", 45000));

        // Pass nth highest salary below
        int n = 3;
        Map.Entry<Integer, List<String>> salaryResult = employees.stream().collect(Collectors.groupingBy(Employee::getSalary,
                        Collectors.mapping(Employee::getName, Collectors.toList())))
                .entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByKey())).collect(Collectors.toList()).get(n-1);
        System.out.println("Third Highest Salary is: " + salaryResult);

    }

    static class Employee {
        private String name;
        private int salary;

        public Employee(String name, int salary) {
            this.name = name;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public int getSalary() {
            return salary;
        }
    }
}

```
### Output
```
Third Highest Salary is: 55000=[Eve, Frank]
```

## Given an Employee List, Sort employees based on descending order
To sort employees based on descending order of their salaries in a given list using Java 8 streams, you can use the `sorted` method with a comparator. Here's how you can do it:

```java
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortEmployeesDescending {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Alice", 50000));
        employees.add(new Employee("Bob", 60000));
        employees.add(new Employee("Charlie", 45000));
        employees.add(new Employee("David", 70000));
        employees.add(new Employee("Eve", 55000));

        // Sort employees in descending order of salary
        List<Employee> sortedEmployees = employees.stream()
                .sorted(Comparator.comparingInt(Employee::getSalary).reversed())
                .collect(Collectors.toList());

        // Print the sorted employees
        sortedEmployees.forEach(System.out::println);
    }

    static class Employee {
        private String name;
        private int salary;

        public Employee(String name, int salary) {
            this.name = name;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public int getSalary() {
            return salary;
        }

        @Override
        public String toString() {
            return "Employee{name='" + name + "', salary=" + salary + "}";
        }
    }
}
```

### Explanation:
1. **Create a List of Employees**: We start by creating a sample list of `Employee` objects.
2. **Stream Operations**:
   - `employees.stream()` creates a stream from the list of employees.
   - `sorted(Comparator.comparingInt(Employee::getSalary).reversed())` sorts the employees by salary in descending order.
   - `collect(Collectors.toList())` collects the sorted stream into a list.
3. **Print the Sorted Employees**: Finally, we print the sorted employees using `forEach(System.out::println)`.

This will output the employees sorted in descending order based on their salaries.

## Given an Employee List, Get Top 3 salaried employees
To get the top 3 salaried employees from a given list using Java 8 streams, you can sort the employees by their salary in descending order and then limit the stream to the first 3 elements. Here's how you can do it:

```java
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TopSalariedEmployees {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Alice", 50000));
        employees.add(new Employee("Bob", 60000));
        employees.add(new Employee("Charlie", 45000));
        employees.add(new Employee("David", 70000));
        employees.add(new Employee("Eve", 55000));

        // Get the top 3 salaried employees
        List<Employee> top3SalariedEmployees = employees.stream()
                .sorted(Comparator.comparingInt(Employee::getSalary).reversed())
                .limit(3)
                .collect(Collectors.toList());

        // Print the top 3 salaried employees
        top3SalariedEmployees.forEach(System.out::println);
    }

    static class Employee {
        private String name;
        private int salary;

        public Employee(String name, int salary) {
            this.name = name;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public int getSalary() {
            return salary;
        }

        @Override
        public String toString() {
            return "Employee{name='" + name + "', salary=" + salary + "}";
        }
    }
}
```

In this example:

1. **Create a List of Employees**: We start by creating a sample list of `Employee` objects.
2. **Stream Operations**:
   - `employees.stream()` creates a stream from the list of employees.
   - `sorted(Comparator.comparingInt(Employee::getSalary).reversed())` sorts the employees by salary in descending order.
   - `limit(3)` limits the stream to the first 3 elements.
   - `collect(Collectors.toList())` collects the top 3 employees into a list.
3. **Print the Top 3 Employees**: Finally, we print the top 3 salaried employees using `forEach(System.out::println)`.

## Given an Employee List, Fetch all employees having salary less than 3rd highest salary
To fetch all employees having a salary less than the 3rd highest salary in a given list using Java 8 streams, you can follow these steps:

1. Find the 3rd highest salary.
2. Filter employees with a salary less than this 3rd highest salary.

Here's the implementation:

```java
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EmployeesLessThanThirdHighestSalary {
    public static void main(String[] args) {
        // Sample list of employees
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Alice", 50000));
        employees.add(new Employee("Bob", 60000));
        employees.add(new Employee("Charlie", 45000));
        employees.add(new Employee("David", 70000));
        employees.add(new Employee("Eve", 55000));
        employees.add(new Employee("Frank", 30000));

        // Find the 3rd highest salary
        Optional<Integer> thirdHighestSalaryOpt = employees.stream()
                .map(Employee::getSalary)
                .sorted(Comparator.reverseOrder())
                .distinct()
                .skip(2) // Skip the first two highest salaries to get the third highest
                .findFirst();

        if (thirdHighestSalaryOpt.isPresent()) {
            int thirdHighestSalary = thirdHighestSalaryOpt.get();
            
            // Fetch all employees with salary less than the 3rd highest salary
            List<Employee> employeesWithLessThanThirdHighestSalary = employees.stream()
                    .filter(e -> e.getSalary() < thirdHighestSalary)
                    .collect(Collectors.toList());

            // Print the employees
            employeesWithLessThanThirdHighestSalary.forEach(System.out::println);
        } else {
            System.out.println("Not enough unique salaries to determine the 3rd highest.");
        }
    }

    static class Employee {
        private String name;
        private int salary;

        public Employee(String name, int salary) {
            this.name = name;
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public int getSalary() {
            return salary;
        }

        @Override
        public String toString() {
            return "Employee{name='" + name + "', salary=" + salary + "}";
        }
    }
}
```

### Explanation:
1. **Find the 3rd Highest Salary**:
    - We first map the employees to their salaries and sort them in descending order.
    - `distinct()` is used to ensure that we consider unique salaries.
    - We skip the first two highest salaries using `skip(2)` and get the third highest salary using `findFirst()`. 

2. **Filter Employees**:
    - If the third highest salary is found, we filter the employees whose salary is less than the third highest salary.
    - We then collect these employees into a list.

3. **Print Employees**:
    - We print the employees whose salary is less than the third highest salary.
    - If there are not enough unique salaries to determine the third highest salary, a message is printed indicating this.

## Replace all the occurrences of the given character from the given String 
Here's how you can replace all occurrences of a given character in a string using Java 8:

```java
import java.util.stream.Collectors;

public class ReplaceCharacter {
    public static void main(String[] args) {
        String input = "java programming";
        char target = 'a';
        char replacement = 'x';

        String result = input.chars()
                .mapToObj(c -> (char) c)
                .map(c -> c == target ? replacement : c)
                .map(String::valueOf)
                .collect(Collectors.joining());

        System.out.println("Original string: " + input);
        System.out.println("Modified string: " + result);
    }
}
```

### Explanation:
1. **`input.chars()`**: Converts the string into an `IntStream` of Unicode values of the characters.
2. **`mapToObj(c -> (char) c)`**: Converts the Unicode values back to characters.
3. **`map(c -> c == target ? replacement : c)`**: Maps each character to the replacement character if it matches the target character; otherwise, it maps to the original character.
4. **`map(String::valueOf)`**: Converts each character to a string.
5. **`collect(Collectors.joining())`**: Joins the strings back into a single string.

This approach leverages Java 8's stream API to replace characters in a functional programming style.

## Given Each Product have List of Sku, Get the product having highest price in the sku
To find the product with the highest price in its SKUs using Java 8, you can follow this approach. The idea is to find the SKU with the highest price for each product and then determine the product that has the highest SKU price.

Here's a code example:

```java
import java.util.*;
import java.util.stream.Collectors;

public class ProductHighestSkuPrice {

    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product("Product1", Arrays.asList(new Sku("Sku1", 10.0), new Sku("Sku2", 20.0))),
                new Product("Product2", Arrays.asList(new Sku("Sku3", 15.0), new Sku("Sku4", 25.0))),
                new Product("Product3", Arrays.asList(new Sku("Sku5", 30.0), new Sku("Sku6", 5.0)))
        );

        Optional<Product> productWithHighestSkuPrice = products.stream()
                .max(Comparator.comparingDouble(ProductHighestSkuPrice::getHighestSkuPrice));

        productWithHighestSkuPrice.ifPresent(product -> 
                System.out.println("Product with highest SKU price: " + product.getName()));
    }

    private static double getHighestSkuPrice(Product product) {
        return product.getSkus().stream()
                .mapToDouble(Sku::getPrice)
                .max()
                .orElse(0.0);
    }

    static class Product {
        private String name;
        private List<Sku> skus;

        public Product(String name, List<Sku> skus) {
            this.name = name;
            this.skus = skus;
        }

        public String getName() {
            return name;
        }

        public List<Sku> getSkus() {
            return skus;
        }
    }

    static class Sku {
        private String name;
        private double price;

        public Sku(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public double getPrice() {
            return price;
        }
    }
}
```

### Explanation:
1. **Product and Sku Classes**:
    - `Product`: Represents a product with a name and a list of SKUs.
    - `Sku`: Represents an SKU with a name and a price.

2. **Creating Products**:
    - A list of products is created, each with a list of SKUs having prices.

3. **Finding the Product with the Highest SKU Price**:
    - The stream of products is processed to find the one with the highest SKU price.
    - `getHighestSkuPrice(Product product)`: Helper method to find the maximum price among the SKUs for a given product.
    - `max(Comparator.comparingDouble(ProductHighestSkuPrice::getHighestSkuPrice))`: Finds the product with the highest SKU price.

4. **Output**:
    - If a product with the highest SKU price is found, its name is printed.

This code demonstrates how to use Java 8 streams to process collections, find maximum values, and work with nested objects.

## Given Each Product have List of Sku, Sort the product having highest price in the sku whose product is available
To sort the products based on the highest price of a SKU, considering only products that are available, you can use Java 8 streams with sorting operations. Here's an example:

```java
import java.util.*;
import java.util.stream.Collectors;

public class SortProductsByHighestSkuPrice {

    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product("Product1", Arrays.asList(new Sku("Sku1", 10.0, true), new Sku("Sku2", 20.0, false))),
                new Product("Product2", Arrays.asList(new Sku("Sku3", 15.0, true), new Sku("Sku4", 25.0, true))),
                new Product("Product3", Arrays.asList(new Sku("Sku5", 30.0, true), new Sku("Sku6", 5.0, true)))
        );

        List<Product> sortedProducts = products.stream()
                .filter(product -> product.getSkus().stream().anyMatch(Sku::isAvailable)) // Filter only available products
                .sorted(Comparator.comparingDouble(SortProductsByHighestSkuPrice::getHighestSkuPrice).reversed()) // Sort by highest SKU price
                .collect(Collectors.toList());

        sortedProducts.forEach(product -> System.out.println(product.getName() + " - " + getHighestSkuPrice(product)));
    }

    private static double getHighestSkuPrice(Product product) {
        return product.getSkus().stream()
                .filter(Sku::isAvailable)
                .mapToDouble(Sku::getPrice)
                .max()
                .orElse(0.0);
    }

    static class Product {
        private String name;
        private List<Sku> skus;

        public Product(String name, List<Sku> skus) {
            this.name = name;
            this.skus = skus;
        }

        public String getName() {
            return name;
        }

        public List<Sku> getSkus() {
            return skus;
        }
    }

    static class Sku {
        private String name;
        private double price;
        private boolean available;

        public Sku(String name, double price, boolean available) {
            this.name = name;
            this.price = price;
            this.available = available;
        }

        public double getPrice() {
            return price;
        }

        public boolean isAvailable() {
            return available;
        }
    }
}
```

### Explanation:
1. **Product and Sku Classes**:
    - `Product`: Represents a product with a name and a list of SKUs.
    - `Sku`: Represents an SKU with a name, price, and availability status.

2. **Creating Products**:
    - A list of products is created, each with a list of SKUs having prices and availability statuses.

3. **Filtering and Sorting**:
    - Products are filtered to include only those with at least one available SKU.
    - The filtered products are then sorted in descending order based on the highest price of an available SKU.

4. **Output**:
    - The sorted products are printed, along with the highest price of an available SKU for each product.

This example demonstrates how to use Java 8 streams to filter, map, and sort collections based on specific criteria.

The output of the provided code will be:

```
Product3 - 30.0
Product2 - 25.0
Product1 - 10.0
```

This output represents the products sorted in descending order based on the highest price of an available SKU.