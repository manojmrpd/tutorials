# Java Coding Interview Concepts

## Table of Contents
1. Java Data Structure & Algorithms
    - Searching
      - Linear Search
      - Binary Search
    - Sorting
      - Bubble Sort
      - Selection Sort
      - Insertion Sort
      - Merge Sort
      - Quick Sort
      - Heap Sort
    - LinkedList
      - ArrayList vs LinkedList
      - Singly Linked List
      - Doubly Linked List
      - Circular Linked List
    - Queues
      - Stack
      - Queue
      - Priority Queue
      - Heap Data Strcture
      - Deque
    - Trees
      - Binary Tree
      - Self balancing Binary Search Tree (AVL Tree)
      - Segment Tree
      - Inorder, Preorder, Postorder Traversal
    - Graphs
      - Adjacency Matrix
      - Adjacency List
      - Depth-First Search (DFS) 
      - Breadth-First Search (BFS)
2. Java Arrays
   - Find Nth Largest element in a given array 
      - Using array 
      - Using streams 