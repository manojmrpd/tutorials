
# Java Data Structures & Algorithms (DSA)

This repository contains a comprehensive guide to data structures and algorithms implemented in Java. It covers various fundamental concepts, algorithms, and their applications, along with code examples and explanations.

## Table of Contents

1. [Introduction](#1-introduction)
2. [Searching](#2-searching)
   - [Linear Search](#linear-search)
   - [Binary Search](#binary-search)
3. [Sorting](#3-sorting)
   - [Bubble Sort](#bubble-sort)
   - [Selection Sort](#selection-sort)
   - [Insertion Sort](#insertion-sort)
   - [Merge Sort](#merge-sort)
   - [Quick Sort](#quick-sort)
   - [Heap Sort](#heap-sort)
4. [LinkedList](#4-linkedlist)
   - [ArrayList vs LinkedList](#arraylist-vs-linkedlist)
   - [Singly Linked List](#singly-linked-list)
   - [Doubly Linked List](#doubly-linked-list)
   - [Circular Linked List](#circular-linked-list)
5. [Queues](#5-queues)
   - [Stack](#stack)
   - [Queue](#queue)
   - [Priority Queue](#priority-queue)
   - [Heap Data Structure](#heap-data-structure)
   - [Deque](#deque)
6. [Trees](#6-trees)
   - [Binary Tree](#binary-tree)
   - [Binary Search Tree (BST)](#binary-search-tree-bst)
   - [Self-balancing Binary Search Tree (AVL Tree)](#self-balancing-binary-search-tree-avl-tree)
   - [Segment Tree](#segment-tree)
   - [Tree Traversals](#tree-traversals)
7. [Graphs](#7-graphs)
   - [Adjacency Matrix](#adjacency-matrix)
   - [Adjacency List](#adjacency-list)
   - [Depth-First Search (DFS)](#depth-first-search-dfs)
   - [Breadth-First Search (BFS)](#breadth-first-search-bfs)


## 1. Introduction

### Overview of Data Structures
Data structures are fundamental concepts in computer science that involve the organization, management, and storage of data. They provide a way to manage large amounts of data efficiently for various operations such as retrieval, manipulation, and storage. Common data structures include arrays, linked lists, stacks, queues, trees, and graphs. Each data structure has its strengths and weaknesses, and the choice of data structure can significantly affect the performance of a program.

### Importance of Algorithms
Algorithms are step-by-step procedures or formulas for solving problems. They are essential in computer science because they provide a clear set of instructions to achieve a particular goal, such as sorting a list of numbers, searching for an item in a dataset, or finding the shortest path between two points. The efficiency of an algorithm can be analyzed in terms of time complexity and space complexity, and choosing the right algorithm for a specific problem is crucial for developing efficient and effective software.

### Complexity Analysis (Big O Notation)
Complexity analysis is a technique used to evaluate the efficiency of an algorithm. Big O Notation is a mathematical notation that describes the upper limit of an algorithm's running time or space requirements in terms of the input size (n). It provides a high-level understanding of the algorithm's performance and helps in comparing different algorithms. Common Big O complexities include:

- **O(1):** Constant time – the algorithm's performance is independent of the input size.
- **O(n):** Linear time – the algorithm's performance grows linearly with the input size.
- **O(log n):** Logarithmic time – the algorithm's performance grows logarithmically with the input size.
- **O(n log n):** Linearithmic time – the algorithm's performance grows linearly with a logarithmic factor.
- **O(n^2):** Quadratic time – the algorithm's performance grows quadratically with the input size.
- **O(2^n):** Exponential time – the algorithm's performance grows exponentially with the input size.

Understanding these complexities helps in making informed decisions about the choice of algorithms and data structures for a given problem, ensuring optimal performance of the software.


## 2. Searching


## Linear Search

### Introduction
Linear Search is one of the simplest searching algorithms used to find an element in a list. It is also known as sequential search. The idea is to start at the beginning of the list and check each element one by one until the desired element is found or the end of the list is reached. This algorithm works on both sorted and unsorted lists.

### Algorithm
The Linear Search algorithm follows these steps:

1. Start from the first element of the list.
2. Compare the current element with the target element.
3. If the current element matches the target element, return the index of the current element.
4. If the current element does not match the target element, move to the next element in the list.
5. Repeat steps 2-4 until the target element is found or the end of the list is reached.
6. If the end of the list is reached and the target element is not found, return -1 indicating that the element is not present in the list.

Here's a Java implementation of the Linear Search algorithm:

```java
public class LinearSearch {
    // Method to perform linear search
    public static int linearSearch(int[] arr, int target) {
        // Iterate through the array
        for (int i = 0; i < arr.length; i++) {
            // Check if the current element is the target element
            if (arr[i] == target) {
                return i; // Return the index of the target element
            }
        }
        return -1; // Return -1 if the target element is not found
    }

    public static void main(String[] args) {
        int[] array = {10, 20, 30, 40, 50};
        int target = 30;
        int index = linearSearch(array, target);

        if (index != -1) {
            System.out.println("Element found at index: " + index);
        } else {
            System.out.println("Element not found in the array");
        }
    }
}
```

### Time Complexity
The time complexity of Linear Search depends on the position of the target element in the list:

- **Best Case:** O(1) - The target element is the first element in the list.
- **Average Case:** O(n) - The target element is somewhere in the middle of the list.
- **Worst Case:** O(n) - The target element is the last element in the list or not present in the list at all.

In all cases, `n` represents the number of elements in the list. Since Linear Search examines each element sequentially, its time complexity in the worst and average cases is linear, making it less efficient for large lists compared to more advanced searching algorithms like Binary Search. However, its simplicity and the fact that it does not require the list to be sorted make it a useful algorithm for small or unsorted datasets.

## Binary Search

### Introduction
Binary Search is a highly efficient algorithm used to find the position of a target element within a sorted array. Unlike Linear Search, which checks each element sequentially, Binary Search repeatedly divides the search interval in half, significantly reducing the number of comparisons needed to locate the target element. This algorithm works only on sorted arrays.

### Algorithm
The Binary Search algorithm follows these steps:

1. Initialize two pointers: `low` to the first element of the array and `high` to the last element.
2. Calculate the middle index `mid` as the average of `low` and `high`.
3. Compare the target element with the element at the `mid` index:
   - If the target element is equal to the `mid` element, return the `mid` index.
   - If the target element is less than the `mid` element, set `high` to `mid - 1` and repeat steps 2-3.
   - If the target element is greater than the `mid` element, set `low` to `mid + 1` and repeat steps 2-3.
4. If `low` exceeds `high`, the target element is not in the array, return -1.

Here's a Java implementation of the Binary Search algorithm:

```java
public class BinarySearch {
    // Method to perform binary search
    public static int binarySearch(int[] arr, int target) {
        int low = 0;
        int high = arr.length - 1;

        while (low <= high) {
            int mid = low + (high - low) / 2;

            // Check if the target is present at mid
            if (arr[mid] == target) {
                return mid;
            }
            // If target is greater, ignore the left half
            if (arr[mid] < target) {
                low = mid + 1;
            }
            // If target is smaller, ignore the right half
            else {
                high = mid - 1;
            }
        }

        // Target element is not present in the array
        return -1;
    }

    public static void main(String[] args) {
        int[] array = {10, 20, 30, 40, 50};
        int target = 30;
        int index = binarySearch(array, target);

        if (index != -1) {
            System.out.println("Element found at index: " + index);
        } else {
            System.out.println("Element not found in the array");
        }
    }
}
```

### Time Complexity
The time complexity of Binary Search is as follows:

- **Best Case:** O(1) - The target element is found at the first comparison (middle of the array).
- **Average Case:** O(log n) - The algorithm repeatedly divides the search interval in half.
- **Worst Case:** O(log n) - The target element is found after the maximum number of divisions.

Here, `n` represents the number of elements in the array. The logarithmic time complexity makes Binary Search significantly more efficient than Linear Search for large datasets.

### Applications
Binary Search is widely used in various applications due to its efficiency:

1. **Searching in Sorted Arrays:** Directly finding elements in large sorted arrays.
2. **Database Indexing:** Quickly locating records in a database.
3. **Libraries and APIs:** Utilized in functions like `Arrays.binarySearch` in Java.
4. **Gaming:** Efficiently searching for optimal moves or positions.
5. **Problem Solving:** Employed in various algorithmic problems, such as finding the square root of a number, finding the closest pair of points, and more.

Binary Search is a fundamental algorithm in computer science, providing a powerful tool for efficient searching in sorted datasets.

## 3. Sorting

## Bubble Sort

### Introduction
Bubble Sort is a simple comparison-based sorting algorithm that repeatedly steps through the list, compares adjacent elements, and swaps them if they are in the wrong order. This process is repeated until the list is sorted. The name "Bubble Sort" comes from the way smaller elements "bubble" to the top of the list (i.e., towards the beginning) with each pass. While Bubble Sort is easy to understand and implement, it is not suitable for large datasets as its average and worst-case time complexities are quite high compared to more advanced algorithms.

### Algorithm
The Bubble Sort algorithm follows these steps:

1. Start at the beginning of the list.
2. Compare the first two adjacent elements.
3. If the first element is greater than the second, swap them.
4. Move to the next pair of adjacent elements and repeat steps 2-3 until the end of the list is reached.
5. Repeat the entire process for the rest of the list, excluding the last sorted elements.
6. Continue this process until no swaps are needed in a complete pass through the list, indicating that the list is sorted.

Here's a Java implementation of the Bubble Sort algorithm:

```java
public class BubbleSort {
    // Method to perform bubble sort
    public static void bubbleSort(int[] arr) {
        int n = arr.length;
        boolean swapped;
        
        // Traverse through all array elements
        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            
            // Last i elements are already sorted
            for (int j = 0; j < n - 1 - i; j++) {
                // Compare and swap if the current element is greater than the next element
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
            }
            
            // If no two elements were swapped, the array is already sorted
            if (!swapped) break;
        }
    }

    public static void main(String[] args) {
        int[] array = {64, 34, 25, 12, 22, 11, 90};
        bubbleSort(array);
        
        System.out.println("Sorted array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
```

### Time Complexity
The time complexity of Bubble Sort can be analyzed as follows:

- **Best Case:** O(n) - Occurs when the list is already sorted. The algorithm will only need one pass through the list to confirm that it is sorted.
- **Average Case:** O(n^2) - Occurs when the list elements are in random order. The algorithm will need multiple passes through the list to sort it.
- **Worst Case:** O(n^2) - Occurs when the list is sorted in reverse order. The algorithm will need the maximum number of passes to sort the list.

Here, `n` represents the number of elements in the list.

### Optimizations
While Bubble Sort is inherently inefficient for large datasets, some optimizations can improve its performance:

1. **Early Termination:** As shown in the Java implementation above, if no swaps are made during a pass, the algorithm can terminate early, indicating that the list is already sorted.
2. **Reducing the Range:** After each pass, the largest element is guaranteed to be in its correct position. Therefore, the range of the inner loop can be reduced by one for each subsequent pass.

These optimizations can help reduce the number of comparisons and swaps needed, especially if the list is partially sorted.

Bubble Sort is a fundamental algorithm that serves as an educational tool for understanding sorting concepts and algorithm design. However, for practical purposes, more efficient algorithms like Quick Sort or Merge Sort are preferred for large datasets.

## Selection Sort

### Introduction
Selection Sort is a simple comparison-based sorting algorithm that divides the list into two parts: a sorted part at the beginning and an unsorted part at the end. Initially, the sorted part is empty, and the unsorted part is the entire list. The algorithm repeatedly selects the smallest (or largest, depending on sorting order) element from the unsorted part and moves it to the end of the sorted part. This process continues until all elements are sorted.

### Algorithm
The Selection Sort algorithm follows these steps:

1. Start with the first element as the current minimum (or maximum) and mark its position.
2. Traverse the unsorted part of the list to find the smallest (or largest) element.
3. Swap the smallest (or largest) element with the first element of the unsorted part.
4. Move the boundary between the sorted and unsorted parts one element to the right.
5. Repeat steps 1-4 for the rest of the list until the entire list is sorted.

Here's a Java implementation of the Selection Sort algorithm:

```java
public class SelectionSort {
    // Method to perform selection sort
    public static void selectionSort(int[] arr) {
        int n = arr.length;

        // Traverse through all array elements
        for (int i = 0; i < n - 1; i++) {
            // Find the minimum element in the unsorted part
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }

            // Swap the found minimum element with the first element of the unsorted part
            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }
    }

    public static void main(String[] args) {
        int[] array = {64, 25, 12, 22, 11};
        selectionSort(array);
        
        System.out.println("Sorted array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
```

### Time Complexity
The time complexity of Selection Sort can be analyzed as follows:

- **Best Case:** O(n^2) - The algorithm always performs n*(n-1)/2 comparisons, regardless of the initial order of elements.
- **Average Case:** O(n^2) - The average number of comparisons and swaps remains the same as the best case.
- **Worst Case:** O(n^2) - The worst case occurs when the list is in reverse order, but the number of comparisons and swaps is still the same as the best case.

Here, `n` represents the number of elements in the list. Selection Sort's time complexity is quadratic, making it inefficient for large datasets.

### Applications
Selection Sort is typically not used in practice for large datasets due to its inefficiency compared to more advanced algorithms. However, it has some applications and educational value:

1. **Small Datasets:** Suitable for small datasets where its simplicity outweighs the performance drawbacks.
2. **Educational Purposes:** Used for teaching sorting concepts and algorithm design due to its simple implementation and clear logic.
3. **Memory Efficiency:** Selection Sort has a space complexity of O(1) as it sorts in place, making it useful in scenarios where memory usage is a concern.
4. **Minimum Comparisons:** While Selection Sort performs more swaps than other algorithms, it makes the minimum number of comparisons needed to sort the list, which can be useful in specific scenarios where comparisons are expensive.

In summary, while Selection Sort is not the most efficient sorting algorithm for large datasets, its simplicity and specific advantages make it a useful tool in certain situations and an excellent educational example of a basic sorting algorithm.

## Insertion Sort

### Introduction
Insertion Sort is a simple and intuitive comparison-based sorting algorithm that builds the final sorted array one element at a time. It works similarly to how one might sort playing cards in their hands. Starting with an empty left hand and the cards face down on the table, you pick up the cards one at a time and insert them into the correct position among the cards in your left hand. It is efficient for small datasets and partially sorted arrays but less efficient for large datasets.

### Algorithm
The Insertion Sort algorithm follows these steps:

1. Start with the second element (consider the first element as already sorted).
2. Compare the current element with the elements in the sorted part of the array (left side).
3. Shift the elements in the sorted part to the right until you find the correct position for the current element.
4. Insert the current element into the correct position.
5. Repeat steps 2-4 for all elements until the entire array is sorted.

Here's a Java implementation of the Insertion Sort algorithm:

```java
public class InsertionSort {
    // Method to perform insertion sort
    public static void insertionSort(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; i++) {
            int key = arr[i];
            int j = i - 1;

            // Move elements of arr[0..i-1], that are greater than key, to one position ahead of their current position
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }

    public static void main(String[] args) {
        int[] array = {12, 11, 13, 5, 6};
        insertionSort(array);

        System.out.println("Sorted array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
```

### Time Complexity
The time complexity of Insertion Sort can be analyzed as follows:

- **Best Case:** O(n) - Occurs when the array is already sorted. The algorithm only makes n-1 comparisons and no swaps.
- **Average Case:** O(n^2) - The algorithm performs n*(n-1)/4 comparisons and n*(n-1)/4 swaps on average.
- **Worst Case:** O(n^2) - Occurs when the array is sorted in reverse order. The algorithm performs n*(n-1)/2 comparisons and n*(n-1)/2 swaps.

Here, `n` represents the number of elements in the array. The quadratic time complexity in the average and worst cases makes Insertion Sort less efficient for large datasets.

### Comparisons with Other Sorts
Insertion Sort has its unique characteristics and compares differently with other sorting algorithms:

1. **Bubble Sort:**
   - Both Bubble Sort and Insertion Sort have a worst-case time complexity of O(n^2).
   - Insertion Sort generally performs fewer comparisons and swaps, making it more efficient than Bubble Sort in practice.
   - Bubble Sort is simpler but less efficient due to its repeated swapping.

2. **Selection Sort:**
   - Both Selection Sort and Insertion Sort have a worst-case time complexity of O(n^2).
   - Insertion Sort is generally more efficient than Selection Sort because it reduces the number of comparisons and swaps.
   - Selection Sort performs a fixed number of comparisons, while Insertion Sort adapts based on the initial order of the elements.

3. **Merge Sort and Quick Sort:**
   - Merge Sort and Quick Sort have better average and worst-case time complexities of O(n log n).
   - Insertion Sort is more efficient for small datasets and nearly sorted arrays.
   - Merge Sort requires additional memory, while Quick Sort can be unstable without modifications.

4. **Heap Sort:**
   - Heap Sort has a time complexity of O(n log n) and is more efficient for larger datasets.
   - Insertion Sort is simpler and more intuitive, making it suitable for educational purposes and small datasets.

### Applications
Insertion Sort is used in various scenarios due to its simplicity and efficiency for small datasets:

1. **Small Arrays:** Ideal for sorting small datasets due to its low overhead.
2. **Partially Sorted Arrays:** Efficient for nearly sorted arrays where it can take advantage of its best-case time complexity.
3. **Educational Purposes:** Used to teach basic sorting concepts and algorithm design.
4. **Online Sorting:** Suitable for sorting data as it arrives, one element at a time (online algorithms).

In conclusion, Insertion Sort is a straightforward and efficient sorting algorithm for small and partially sorted datasets, offering simplicity and ease of implementation. However, for larger datasets, more advanced algorithms like Merge Sort, Quick Sort, and Heap Sort are preferred due to their better time complexities.

## Merge Sort

### Introduction
Merge Sort is an efficient, stable, comparison-based sorting algorithm that uses the divide-and-conquer paradigm. It works by dividing the array into smaller subarrays, sorting those subarrays, and then merging them back together to form a sorted array. Due to its predictable O(n log n) time complexity, it is one of the most widely used general-purpose sorting algorithms. Merge Sort is particularly useful for sorting linked lists and large datasets due to its stable nature and consistent performance.

### Algorithm
The Merge Sort algorithm can be broken down into the following steps:

1. **Divide:** Recursively split the array into two halves until each subarray contains a single element or is empty.
2. **Conquer:** Recursively sort each half.
3. **Combine:** Merge the two sorted halves to produce a single sorted array.

Here's a detailed explanation and Java implementation of the Merge Sort algorithm:

1. **Divide:** Split the array into two halves.
2. **Conquer:** Recursively sort each half.
3. **Combine:** Merge the two sorted halves to produce a single sorted array.

### Detailed Steps
1. **Recursive Splitting:** 
   - If the array has more than one element, split the array into two halves.
   - Recursively apply Merge Sort to both halves.

2. **Merging:**
   - Compare the elements of both halves and merge them into a single sorted array.
   - Continue this process until all elements are merged back into a sorted array.

### Java Implementation

```java
public class MergeSort {
    // Method to merge two sorted subarrays
    private static void merge(int[] arr, int left, int mid, int right) {
        // Calculate the sizes of the subarrays to be merged
        int n1 = mid - left + 1;
        int n2 = right - mid;

        // Create temporary arrays
        int[] L = new int[n1];
        int[] R = new int[n2];

        // Copy data to temporary arrays
        for (int i = 0; i < n1; ++i)
            L[i] = arr[left + i];
        for (int j = 0; j < n2; ++j)
            R[j] = arr[mid + 1 + j];

        // Merge the temporary arrays back into the original array
        int i = 0, j = 0;
        int k = left;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        // Copy remaining elements of L[] if any
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        // Copy remaining elements of R[] if any
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    // Method to implement merge sort
    private static void mergeSort(int[] arr, int left, int right) {
        if (left < right) {
            // Find the middle point
            int mid = (left + right) / 2;

            // Recursively sort the two halves
            mergeSort(arr, left, mid);
            mergeSort(arr, mid + 1, right);

            // Merge the sorted halves
            merge(arr, left, mid, right);
        }
    }

    public static void main(String[] args) {
        int[] array = {12, 11, 13, 5, 6, 7};
        mergeSort(array, 0, array.length - 1);

        System.out.println("Sorted array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
```

### Time Complexity
The time complexity of Merge Sort can be analyzed as follows:

- **Best Case:** O(n log n) - Occurs when the array is already sorted, but the algorithm still performs the same number of comparisons.
- **Average Case:** O(n log n) - The algorithm consistently divides the array and merges them, resulting in a logarithmic number of divisions and linear number of comparisons per division.
- **Worst Case:** O(n log n) - Similar to the average case, the algorithm performs consistently regardless of the initial order of elements.

Here, `n` represents the number of elements in the array. The logarithmic depth of the recursive calls and linear merging process contribute to the overall time complexity of O(n log n).

### Space Complexity
The space complexity of Merge Sort is O(n) due to the additional temporary arrays used for merging. These arrays require extra space proportional to the size of the input array. Unlike in-place sorting algorithms like Quick Sort or Heap Sort, Merge Sort requires significant additional memory, which can be a disadvantage for very large datasets.

### Applications
Merge Sort is widely used in various applications due to its efficiency and stability:

1. **Large Datasets:** Suitable for sorting large datasets where consistent performance is crucial.
2. **Linked Lists:** Particularly efficient for linked lists since merging linked lists does not require additional space, unlike array-based merging.
3. **External Sorting:** Useful in external sorting algorithms, where data that cannot fit into memory is sorted.
4. **Stable Sort Requirements:** Preferred in scenarios where stability (preserving the relative order of equal elements) is necessary.
5. **Parallel Processing:** Can be parallelized easily due to its divide-and-conquer nature, making it suitable for multi-threaded and distributed environments.

In conclusion, Merge Sort is a robust and reliable sorting algorithm that offers predictable performance and stability, making it suitable for a wide range of applications despite its higher space complexity.

## Quick Sort

### Introduction
Quick Sort is a highly efficient, comparison-based sorting algorithm that uses the divide-and-conquer paradigm. It is known for its average-case time complexity of O(n log n) and is often faster in practice than other O(n log n) algorithms like Merge Sort and Heap Sort. Quick Sort works by selecting a 'pivot' element from the array and partitioning the other elements into two sub-arrays, according to whether they are less than or greater than the pivot. The sub-arrays are then sorted recursively.

### Algorithm
The Quick Sort algorithm follows these steps:

1. **Divide:** Choose a pivot element from the array.
2. **Partition:** Rearrange the elements of the array so that all elements less than the pivot come before it, and all elements greater than the pivot come after it. The pivot is now in its final sorted position.
3. **Conquer:** Recursively apply the above steps to the sub-arrays of elements with smaller and larger values.

The process continues until the base case of an array with a single element is reached, which is already sorted.

Here's a Java implementation of the Quick Sort algorithm:

```java
public class QuickSort {
    // Method to partition the array
    private static int partition(int[] arr, int low, int high) {
        int pivot = arr[high]; // Choose the last element as pivot
        int i = (low - 1); // Index of smaller element

        for (int j = low; j < high; j++) {
            // If the current element is smaller than or equal to the pivot
            if (arr[j] <= pivot) {
                i++;

                // Swap arr[i] and arr[j]
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        // Swap arr[i+1] and arr[high] (or pivot)
        int temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;

        return i + 1;
    }

    // Method to perform Quick Sort
    private static void quickSort(int[] arr, int low, int high) {
        if (low < high) {
            // Partition the array
            int pi = partition(arr, low, high);

            // Recursively sort the sub-arrays
            quickSort(arr, low, pi - 1);
            quickSort(arr, pi + 1, high);
        }
    }

    public static void main(String[] args) {
        int[] array = {10, 7, 8, 9, 1, 5};
        quickSort(array, 0, array.length - 1);

        System.out.println("Sorted array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
```

### Time Complexity
The time complexity of Quick Sort can be analyzed as follows:

- **Best Case:** O(n log n) - Occurs when the pivot divides the array into two nearly equal halves, resulting in a balanced recursion tree.
- **Average Case:** O(n log n) - On average, the pivot tends to divide the array into two halves, leading to a balanced recursion tree.
- **Worst Case:** O(n^2) - Occurs when the pivot is always the smallest or largest element, leading to an unbalanced recursion tree. This can be mitigated by choosing a good pivot strategy (e.g., random pivot or median-of-three).

Here, `n` represents the number of elements in the array. While the worst-case time complexity is quadratic, good pivot strategies typically ensure that Quick Sort performs closer to the average case.

### Partitioning Techniques
The efficiency of Quick Sort heavily depends on the partitioning technique. Different strategies for choosing the pivot and partitioning the array include:

1. **Lomuto Partition Scheme:**
   - The last element is chosen as the pivot.
   - The array is partitioned such that all elements less than the pivot are on the left, and all elements greater than the pivot are on the right.
   - Simple but can result in poor performance for certain input distributions.

   Example implementation (used in the Java code above).

2. **Hoare Partition Scheme:**
   - The first element is chosen as the pivot.
   - Two indices are used to scan the array from left to right and right to left, respectively.
   - Elements are swapped when the left index encounters an element greater than the pivot and the right index encounters an element less than the pivot.
   - More efficient than Lomuto for certain input distributions but more complex to implement.

   Example implementation:

   ```java
   private static int hoarePartition(int[] arr, int low, int high) {
       int pivot = arr[low];
       int i = low - 1;
       int j = high + 1;
       while (true) {
           do {
               i++;
           } while (arr[i] < pivot);

           do {
               j--;
           } while (arr[j] > pivot);

           if (i >= j)
               return j;

           int temp = arr[i];
           arr[i] = arr[j];
           arr[j] = temp;
       }
   }
   ```

3. **Median-of-Three Partitioning:**
   - The pivot is chosen as the median of the first, middle, and last elements.
   - Helps to avoid worst-case scenarios and improves performance on average.

   Example implementation:

   ```java
   private static int medianOfThree(int[] arr, int low, int high) {
       int mid = low + (high - low) / 2;
       if (arr[mid] < arr[low])
           swap(arr, low, mid);
       if (arr[high] < arr[low])
           swap(arr, low, high);
       if (arr[high] < arr[mid])
           swap(arr, mid, high);
       return arr[mid];
   }

   private static void swap(int[] arr, int i, int j) {
       int temp = arr[i];
       arr[i] = arr[j];
       arr[j] = temp;
   }
   ```

4. **Random Pivot:**
   - The pivot is chosen randomly from the array.
   - Reduces the probability of encountering worst-case scenarios.

   Example implementation:

   ```java
   private static int randomPartition(int[] arr, int low, int high) {
       int pivotIndex = low + (int) (Math.random() * (high - low + 1));
       swap(arr, pivotIndex, high);
       return partition(arr, low, high);
   }
   ```

### Applications
Quick Sort is widely used in various applications due to its efficiency and versatility:

1. **General-Purpose Sorting:** Used in many libraries and frameworks for general-purpose sorting.
2. **Large Datasets:** Suitable for large datasets due to its average-case efficiency.
3. **In-Place Sorting:** Does not require additional memory for sorting (aside from the recursion stack), making it memory efficient.
4. **Parallel Sorting:** Can be parallelized easily due to its divide-and-conquer nature, making it suitable for multi-threaded and distributed environments.
5. **Real-Time Systems:** Efficient for real-time systems where predictable performance is crucial.

In conclusion, Quick Sort is a robust and efficient sorting algorithm with a strong theoretical foundation and practical performance. Its ability to handle large datasets and in-place sorting makes it a popular choice for many applications, despite its potential quadratic worst-case time complexity. Proper pivot selection strategies can mitigate this downside, ensuring Quick Sort's efficiency in most cases.

## Heap Sort

### Introduction
Heap Sort is a comparison-based sorting algorithm that uses a binary heap data structure to sort elements. It is an in-place algorithm with a time complexity of O(n log n) in the worst case, making it efficient for large datasets. Heap Sort is not a stable sort, meaning it does not preserve the relative order of equal elements. It is often used in applications where auxiliary memory usage must be minimized, as it only requires a constant amount of additional space.

### Algorithm
The Heap Sort algorithm follows these steps:

1. **Build a Max Heap:** Rearrange the array into a max heap, where the largest element is at the root.
2. **Extract the Maximum Element:** Swap the root element with the last element in the array and reduce the heap size by one. Then, heapify the root element to maintain the max heap property.
3. **Repeat Step 2:** Continue extracting the maximum element and heapifying the root until the heap size is reduced to one.

Here's a Java implementation of the Heap Sort algorithm:

```java
public class HeapSort {
    // Method to heapify a subtree rooted at node i
    private static void heapify(int[] arr, int n, int i) {
        int largest = i; // Initialize largest as root
        int left = 2 * i + 1; // Left child
        int right = 2 * i + 2; // Right child

        // If left child is larger than root
        if (left < n && arr[left] > arr[largest])
            largest = left;

        // If right child is larger than largest so far
        if (right < n && arr[right] > arr[largest])
            largest = right;

        // If largest is not root
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            // Recursively heapify the affected subtree
            heapify(arr, n, largest);
        }
    }

    // Method to perform heap sort
    private static void heapSort(int[] arr) {
        int n = arr.length;

        // Build a max heap
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(arr, n, i);

        // Extract elements from the heap one by one
        for (int i = n - 1; i > 0; i--) {
            // Move current root to end
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;

            // Call heapify on the reduced heap
            heapify(arr, i, 0);
        }
    }

    public static void main(String[] args) {
        int[] array = {12, 11, 13, 5, 6, 7};
        heapSort(array);

        System.out.println("Sorted array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
```

### Time Complexity
The time complexity of Heap Sort can be analyzed as follows:

- **Best Case:** O(n log n) - The algorithm always divides the array into a balanced binary heap and performs heap operations.
- **Average Case:** O(n log n) - Similar to the best case, the average time complexity is logarithmic due to the balanced nature of heap operations.
- **Worst Case:** O(n log n) - Even in the worst-case scenario, the algorithm maintains a logarithmic time complexity due to the heap property.

Here, `n` represents the number of elements in the array. The logarithmic time complexity arises from the height of the binary heap, which is log(n), and each heapify operation takes O(log n) time.

### Heap Operations
Heap Sort relies on two main heap operations: building the heap and heapifying.

1. **Building the Heap:**
   - To build a max heap, we start from the last non-leaf node and call the heapify function for each node.
   - This process ensures that all nodes satisfy the heap property, resulting in a complete binary heap.
   - The time complexity of building the heap is O(n).

2. **Heapifying:**
   - The heapify operation ensures that a subtree rooted at a given node satisfies the heap property.
   - If the root node is smaller than its children, we swap it with the larger child and recursively heapify the affected subtree.
   - The time complexity of heapifying a node is O(log n) due to the height of the tree.

Here's a breakdown of the heap operations:

1. **Heapify Function:**
   - Given an array and an index `i`, heapify the subtree rooted at `i`.
   - Compare the root with its left and right children.
   - If the root is smaller than one of its children, swap it with the larger child.
   - Recursively apply heapify to the affected subtree.

2. **Build Max Heap:**
   - Start from the last non-leaf node (index `n/2 - 1`) and move to the root (index `0`).
   - Apply heapify to each node to ensure the heap property is maintained.

3. **Extract Max Element:**
   - Swap the root (maximum element) with the last element of the array.
   - Reduce the heap size by one.
   - Apply heapify to the root to restore the heap property.
   - Repeat until the heap size is reduced to one.

### Applications
Heap Sort is used in various applications due to its efficiency and in-place sorting:

1. **Sorting Large Datasets:** Efficient for sorting large datasets where memory usage must be minimized.
2. **Priority Queues:** Used to implement priority queues where the highest (or lowest) priority element is always at the root.
3. **Heapsort Variants:** Utilized in variants like external sorting algorithms for handling large amounts of data that do not fit into memory.
4. **Order Statistics:** Useful in finding the k-th largest (or smallest) element in an array.

In conclusion, Heap Sort is a robust and efficient sorting algorithm that leverages the properties of binary heaps to achieve a logarithmic time complexity. Its in-place sorting capability and consistent performance make it suitable for various applications, especially where auxiliary memory usage must be minimized.

## 4. LinkedList

## ArrayList vs LinkedList

### Introduction
Both `ArrayList` and `LinkedList` are implementations of the `List` interface in Java's Collections Framework. They provide ordered collections of elements, but they have different underlying structures and performance characteristics, making each suitable for different use cases.

### ArrayList

An `ArrayList` is a resizable array implementation of the `List` interface. It provides fast random access and is generally preferred when the list is primarily used for accessing elements.

### LinkedList

A `LinkedList` is a doubly-linked list implementation of the `List` interface. Each element is stored in a node that contains a reference to the next and previous nodes. This structure makes certain operations more efficient compared to `ArrayList`.

### Comparison

1. **Underlying Data Structure**
   - **ArrayList:** Backed by a dynamic array.
   - **LinkedList:** Composed of nodes, each containing a reference to the next and previous node (doubly linked list).

2. **Access Time**
   - **ArrayList:** O(1) for random access. Fast due to direct indexing.
   - **LinkedList:** O(n) for random access. Slow due to sequential traversal.

3. **Insertion Time**
   - **ArrayList:**
     - O(n) for inserting at the beginning or in the middle (due to shifting elements).
     - O(1) for inserting at the end (amortized time, as resizing may be required).
   - **LinkedList:**
     - O(1) for inserting at the beginning or in the middle (if the position is known).
     - O(n) for inserting if the position is not known (due to traversal).

4. **Deletion Time**
   - **ArrayList:**
     - O(n) for deleting from the beginning or middle (due to shifting elements).
     - O(1) for deleting from the end.
   - **LinkedList:**
     - O(1) for deleting if the position is known (no shifting required).
     - O(n) for deleting if the position is not known (due to traversal).

5. **Memory Usage**
   - **ArrayList:** Less memory overhead as it stores elements in a contiguous array.
   - **LinkedList:** More memory overhead as it stores extra references (to next and previous nodes) for each element.

6. **Iteration Performance**
   - **ArrayList:** Better cache performance due to contiguous memory allocation.
   - **LinkedList:** Poorer cache performance due to scattered memory allocation.

7. **Resizing**
   - **ArrayList:** Resizing involves creating a new array and copying elements, which can be costly.
   - **LinkedList:** No resizing required, as nodes are dynamically allocated.

### Use Cases

**ArrayList:**
1. **Frequent Random Access:** When the application requires frequent access to elements by index.
   - Example: Storing a list of student records where access by index is common.
2. **Read-Intensive Applications:** When read operations outnumber write operations.
   - Example: Displaying data on a user interface where updates are infrequent.
3. **Appending Data:** When frequently adding elements at the end.
   - Example: Logging events or collecting user inputs.

**LinkedList:**
1. **Frequent Insertions/Deletions:** When the application requires frequent insertions and deletions in the middle of the list.
   - Example: Implementing a job queue where jobs can be added or removed from both ends.
2. **Queue or Deque Implementations:** When used as a stack, queue, or deque.
   - Example: Browser history (back and forward navigation), or an undo mechanism in a text editor.
3. **Data of Unknown Size:** When the size of the data set is unknown and dynamic resizing is required.
   - Example: Real-time data streams or online algorithms where data is continuously added.

### Summary Table

| Feature                        | ArrayList                | LinkedList                |
|------------------------------- |------------------------- |-------------------------- |
| Underlying Data Structure      | Dynamic Array            | Doubly Linked List        |
| Access Time                    | O(1) (fast)              | O(n) (slow)               |
| Insertion Time (beginning/middle) | O(n) (slow)              | O(1) (fast)               |
| Insertion Time (end)           | O(1) (amortized)         | O(1)                      |
| Deletion Time (beginning/middle) | O(n) (slow)              | O(1) (fast)               |
| Deletion Time (end)            | O(1)                     | O(n) (if traversal needed)|
| Memory Usage                   | Less overhead            | More overhead (due to node references) |
| Iteration Performance          | Better (cache friendly)  | Poorer (cache unfriendly) |
| Resizing                       | Expensive (copying array)| Cheap (dynamic allocation)|

In conclusion, choosing between `ArrayList` and `LinkedList` depends on the specific use case and the types of operations that are most frequent in the application. For random access and read-heavy scenarios, `ArrayList` is generally more efficient. For frequent insertions and deletions, especially in the middle of the list, `LinkedList` is more suitable.

## Singly Linked List

### Introduction
A Singly Linked List is a linear data structure consisting of nodes where each node contains a data part and a reference (or link) to the next node in the sequence. The list starts with a "head" node and ends with a node pointing to `null`. Unlike arrays, linked lists do not store elements in contiguous memory locations, allowing for efficient insertions and deletions.

### Operations

1. **Insertion**

   **At the Beginning:**
   - Create a new node with the given data.
   - Point the new node's `next` to the current head.
   - Update the head to point to the new node.

   **At the End:**
   - Create a new node with the given data.
   - If the list is empty, make the new node the head.
   - Otherwise, traverse to the end of the list.
   - Point the last node's `next` to the new node.

   **At a Specific Position:**
   - Create a new node with the given data.
   - Traverse to the node just before the specified position.
   - Point the new node's `next` to the current node at that position.
   - Update the previous node's `next` to point to the new node.

   Example Java implementation:
   ```java
   class Node {
       int data;
       Node next;

       Node(int data) {
           this.data = data;
           this.next = null;
       }
   }

   class SinglyLinkedList {
       Node head;

       // Insert at the beginning
       void insertAtBeginning(int data) {
           Node newNode = new Node(data);
           newNode.next = head;
           head = newNode;
       }

       // Insert at the end
       void insertAtEnd(int data) {
           Node newNode = new Node(data);
           if (head == null) {
               head = newNode;
               return;
           }
           Node temp = head;
           while (temp.next != null) {
               temp = temp.next;
           }
           temp.next = newNode;
       }

       // Insert at a specific position
       void insertAtPosition(int data, int position) {
           Node newNode = new Node(data);
           if (position == 0) {
               newNode.next = head;
               head = newNode;
               return;
           }
           Node temp = head;
           for (int i = 0; i < position - 1; i++) {
               if (temp == null) throw new IllegalArgumentException("Position out of bounds");
               temp = temp.next;
           }
           newNode.next = temp.next;
           temp.next = newNode;
       }
   }
   ```

2. **Deletion**

   **From the Beginning:**
   - Update the head to point to the next node.

   **From the End:**
   - Traverse to the second last node.
   - Update its `next` to `null`.

   **From a Specific Position:**
   - Traverse to the node just before the specified position.
   - Update its `next` to point to the node after the one to be deleted.

   Example Java implementation:
   ```java
   class SinglyLinkedList {
       Node head;

       // Delete from the beginning
       void deleteFromBeginning() {
           if (head != null) {
               head = head.next;
           }
       }

       // Delete from the end
       void deleteFromEnd() {
           if (head == null) return;
           if (head.next == null) {
               head = null;
               return;
           }
           Node temp = head;
           while (temp.next.next != null) {
               temp = temp.next;
           }
           temp.next = null;
       }

       // Delete from a specific position
       void deleteFromPosition(int position) {
           if (head == null) return;
           if (position == 0) {
               head = head.next;
               return;
           }
           Node temp = head;
           for (int i = 0; i < position - 1; i++) {
               if (temp == null || temp.next == null) throw new IllegalArgumentException("Position out of bounds");
               temp = temp.next;
           }
           if (temp.next == null) throw new IllegalArgumentException("Position out of bounds");
           temp.next = temp.next.next;
       }
   }
   ```

3. **Traversal**
   - Start from the head node.
   - Visit each node, process its data, and move to the next node until the end of the list is reached.

   Example Java implementation:
   ```java
   class SinglyLinkedList {
       Node head;

       // Traverse the list
       void traverse() {
           Node temp = head;
           while (temp != null) {
               System.out.print(temp.data + " ");
               temp = temp.next;
           }
           System.out.println();
       }
   }
   ```

### Time Complexity

1. **Insertion:**
   - **At the Beginning:** O(1) - Directly inserts the new node at the head.
   - **At the End:** O(n) - Requires traversal to the end of the list.
   - **At a Specific Position:** O(n) - Requires traversal to the specific position.

2. **Deletion:**
   - **From the Beginning:** O(1) - Directly updates the head.
   - **From the End:** O(n) - Requires traversal to the second last node.
   - **From a Specific Position:** O(n) - Requires traversal to the specific position.

3. **Traversal:**
   - O(n) - Visits each node exactly once.

In summary, a Singly Linked List is a versatile data structure that allows for efficient insertions and deletions, especially at the beginning of the list. It is suitable for applications where these operations are frequent and random access is not required. The simplicity of its operations and predictable time complexity make it a foundational structure in computer science.

## Doubly Linked List

### Introduction
A Doubly Linked List is a linear data structure consisting of nodes where each node contains three parts: a data part, a reference to the next node, and a reference to the previous node. This allows traversal of the list in both directions (forward and backward). The list starts with a "head" node and ends with a "tail" node, with the `next` pointer of the tail node being `null` and the `prev` pointer of the head node being `null`.

### Operations

1. **Insertion**

   **At the Beginning:**
   - Create a new node with the given data.
   - Set the new node's `next` to the current head.
   - If the list is not empty, set the current head's `prev` to the new node.
   - Update the head to point to the new node.
   - If the list was empty, also set the tail to the new node.

   **At the End:**
   - Create a new node with the given data.
   - Set the new node's `prev` to the current tail.
   - If the list is not empty, set the current tail's `next` to the new node.
   - Update the tail to point to the new node.
   - If the list was empty, also set the head to the new node.

   **At a Specific Position:**
   - Create a new node with the given data.
   - Traverse to the node just before the specified position.
   - Set the new node's `next` to the current node at that position.
   - Set the new node's `prev` to the previous node.
   - Update the previous node's `next` to the new node.
   - Update the current node's `prev` to the new node.

   Example Java implementation:
   ```java
   class Node {
       int data;
       Node next;
       Node prev;

       Node(int data) {
           this.data = data;
           this.next = null;
           this.prev = null;
       }
   }

   class DoublyLinkedList {
       Node head;
       Node tail;

       // Insert at the beginning
       void insertAtBeginning(int data) {
           Node newNode = new Node(data);
           if (head == null) {
               head = newNode;
               tail = newNode;
           } else {
               newNode.next = head;
               head.prev = newNode;
               head = newNode;
           }
       }

       // Insert at the end
       void insertAtEnd(int data) {
           Node newNode = new Node(data);
           if (tail == null) {
               head = newNode;
               tail = newNode;
           } else {
               newNode.prev = tail;
               tail.next = newNode;
               tail = newNode;
           }
       }

       // Insert at a specific position
       void insertAtPosition(int data, int position) {
           if (position == 0) {
               insertAtBeginning(data);
               return;
           }
           Node newNode = new Node(data);
           Node temp = head;
           for (int i = 0; i < position - 1; i++) {
               if (temp == null) throw new IllegalArgumentException("Position out of bounds");
               temp = temp.next;
           }
           if (temp.next == null) {
               insertAtEnd(data);
           } else {
               newNode.next = temp.next;
               newNode.prev = temp;
               temp.next.prev = newNode;
               temp.next = newNode;
           }
       }
   }
   ```

2. **Deletion**

   **From the Beginning:**
   - Update the head to point to the next node.
   - If the list is not empty, set the new head's `prev` to `null`.
   - If the list becomes empty, set the tail to `null`.

   **From the End:**
   - Update the tail to point to the previous node.
   - If the list is not empty, set the new tail's `next` to `null`.
   - If the list becomes empty, set the head to `null`.

   **From a Specific Position:**
   - Traverse to the node at the specified position.
   - Update the previous node's `next` to point to the next node.
   - Update the next node's `prev` to point to the previous node.

   Example Java implementation:
   ```java
   class DoublyLinkedList {
       Node head;
       Node tail;

       // Delete from the beginning
       void deleteFromBeginning() {
           if (head != null) {
               head = head.next;
               if (head != null) {
                   head.prev = null;
               } else {
                   tail = null;
               }
           }
       }

       // Delete from the end
       void deleteFromEnd() {
           if (tail != null) {
               tail = tail.prev;
               if (tail != null) {
                   tail.next = null;
               } else {
                   head = null;
               }
           }
       }

       // Delete from a specific position
       void deleteFromPosition(int position) {
           if (position == 0) {
               deleteFromBeginning();
               return;
           }
           Node temp = head;
           for (int i = 0; i < position; i++) {
               if (temp == null) throw new IllegalArgumentException("Position out of bounds");
               temp = temp.next;
           }
           if (temp.next == null) {
               deleteFromEnd();
           } else {
               temp.prev.next = temp.next;
               temp.next.prev = temp.prev;
           }
       }
   }
   ```

3. **Traversal**
   - **Forward Traversal:**
     - Start from the head node.
     - Visit each node, process its data, and move to the next node until the end of the list is reached.

   - **Backward Traversal:**
     - Start from the tail node.
     - Visit each node, process its data, and move to the previous node until the start of the list is reached.

   Example Java implementation:
   ```java
   class DoublyLinkedList {
       Node head;
       Node tail;

       // Forward traversal
       void traverseForward() {
           Node temp = head;
           while (temp != null) {
               System.out.print(temp.data + " ");
               temp = temp.next;
           }
           System.out.println();
       }

       // Backward traversal
       void traverseBackward() {
           Node temp = tail;
           while (temp != null) {
               System.out.print(temp.data + " ");
               temp = temp.prev;
           }
           System.out.println();
       }
   }
   ```

### Time Complexity

1. **Insertion:**
   - **At the Beginning:** O(1) - Directly inserts the new node at the head.
   - **At the End:** O(1) - Directly inserts the new node at the tail.
   - **At a Specific Position:** O(n) - Requires traversal to the specific position.

2. **Deletion:**
   - **From the Beginning:** O(1) - Directly updates the head.
   - **From the End:** O(1) - Directly updates the tail.
   - **From a Specific Position:** O(n) - Requires traversal to the specific position.

3. **Traversal:**
   - **Forward Traversal:** O(n) - Visits each node exactly once.
   - **Backward Traversal:** O(n) - Visits each node exactly once.

In summary, a Doubly Linked List provides efficient insertions and deletions at both ends of the list and allows for bidirectional traversal. This makes it suitable for applications where these operations are frequent, and bidirectional access is required. However, it uses more memory than a Singly Linked List due to the extra references to previous nodes.

## Circular Linked List

### Introduction
A Circular Linked List is a variation of a linked list where the last node points back to the first node, forming a circular structure. This allows for continuous traversal of the list without a defined end. Circular linked lists can be singly or doubly linked, where singly linked lists have nodes with references to the next node, and doubly linked lists have nodes with references to both the next and previous nodes.

### Operations

1. **Insertion**

   **At the Beginning:**
   - Create a new node with the given data.
   - If the list is empty, set the new node's `next` to point to itself.
   - Otherwise, set the new node's `next` to the current head.
   - Traverse to the last node and update its `next` to the new node.
   - Update the head to point to the new node.

   **At the End:**
   - Create a new node with the given data.
   - If the list is empty, set the new node's `next` to point to itself and update the head to the new node.
   - Otherwise, traverse to the last node.
   - Set the last node's `next` to the new node.
   - Update the new node's `next` to point to the head.

   **At a Specific Position:**
   - Create a new node with the given data.
   - Traverse to the node just before the specified position.
   - Set the new node's `next` to the current node at that position.
   - Update the previous node's `next` to the new node.

   Example Java implementation:
   ```java
   class Node {
       int data;
       Node next;

       Node(int data) {
           this.data = data;
           this.next = null;
       }
   }

   class CircularLinkedList {
       Node head;

       // Insert at the beginning
       void insertAtBeginning(int data) {
           Node newNode = new Node(data);
           if (head == null) {
               head = newNode;
               newNode.next = newNode;
           } else {
               Node temp = head;
               while (temp.next != head) {
                   temp = temp.next;
               }
               newNode.next = head;
               temp.next = newNode;
               head = newNode;
           }
       }

       // Insert at the end
       void insertAtEnd(int data) {
           Node newNode = new Node(data);
           if (head == null) {
               head = newNode;
               newNode.next = newNode;
           } else {
               Node temp = head;
               while (temp.next != head) {
                   temp = temp.next;
               }
               temp.next = newNode;
               newNode.next = head;
           }
       }

       // Insert at a specific position
       void insertAtPosition(int data, int position) {
           if (position == 0) {
               insertAtBeginning(data);
               return;
           }
           Node newNode = new Node(data);
           Node temp = head;
           for (int i = 0; i < position - 1; i++) {
               if (temp.next == head) throw new IllegalArgumentException("Position out of bounds");
               temp = temp.next;
           }
           newNode.next = temp.next;
           temp.next = newNode;
       }
   }
   ```

2. **Deletion**

   **From the Beginning:**
   - If the list is empty, do nothing.
   - If the list has only one node, set the head to `null`.
   - Otherwise, traverse to the last node.
   - Update the last node's `next` to the current head's `next`.
   - Update the head to the current head's `next`.

   **From the End:**
   - If the list is empty, do nothing.
   - If the list has only one node, set the head to `null`.
   - Otherwise, traverse to the second last node.
   - Update its `next` to point to the head.

   **From a Specific Position:**
   - If the list is empty, do nothing.
   - If the position is 0, delete from the beginning.
   - Otherwise, traverse to the node just before the specified position.
   - Update its `next` to skip the node at the specified position.

   Example Java implementation:
   ```java
   class CircularLinkedList {
       Node head;

       // Delete from the beginning
       void deleteFromBeginning() {
           if (head == null) return;
           if (head.next == head) {
               head = null;
               return;
           }
           Node temp = head;
           while (temp.next != head) {
               temp = temp.next;
           }
           temp.next = head.next;
           head = head.next;
       }

       // Delete from the end
       void deleteFromEnd() {
           if (head == null) return;
           if (head.next == head) {
               head = null;
               return;
           }
           Node temp = head;
           while (temp.next.next != head) {
               temp = temp.next;
           }
           temp.next = head;
       }

       // Delete from a specific position
       void deleteFromPosition(int position) {
           if (head == null) return;
           if (position == 0) {
               deleteFromBeginning();
               return;
           }
           Node temp = head;
           for (int i = 0; i < position - 1; i++) {
               if (temp.next == head) throw new IllegalArgumentException("Position out of bounds");
               temp = temp.next;
           }
           if (temp.next == head) throw new IllegalArgumentException("Position out of bounds");
           temp.next = temp.next.next;
       }
   }
   ```

3. **Traversal**
   - **Forward Traversal:**
     - Start from the head node.
     - Visit each node, process its data, and move to the next node until returning to the head.

   Example Java implementation:
   ```java
   class CircularLinkedList {
       Node head;

       // Traverse the list
       void traverse() {
           if (head == null) return;
           Node temp = head;
           do {
               System.out.print(temp.data + " ");
               temp = temp.next;
           } while (temp != head);
           System.out.println();
       }
   }
   ```

### Use Cases

1. **Circular Buffers:**
   - Useful for buffering data streams where the buffer needs to be reused in a circular fashion.
   - Example: Multimedia streaming, where the buffer continuously overwrites old data with new incoming data.

2. **Round-Robin Scheduling:**
   - Ideal for implementing round-robin scheduling algorithms where each process gets an equal share of CPU time.
   - Example: Operating system process scheduling.

3. **Fifo (First In, First Out) Data Structures:**
   - Useful for implementing FIFO queues with wrap-around capabilities.
   - Example: Network packet buffering where the packets need to be processed in a circular manner.

4. **Continuous Monitoring:**
   - Effective for applications requiring continuous monitoring and logging.
   - Example: Logging systems where old logs are overwritten by new ones once the buffer is full.

### Summary Table

| Operation        | Time Complexity |
|------------------|-----------------|
| Insertion (Beginning) | O(n) - (due to traversal to the last node) |
| Insertion (End)       | O(n) - (due to traversal to the last node) |
| Insertion (Position)  | O(n) - (due to traversal to the position)  |
| Deletion (Beginning)  | O(n) - (due to traversal to the last node) |
| Deletion (End)        | O(n) - (due to traversal to the second last node) |
| Deletion (Position)   | O(n) - (due to traversal to the position)  |
| Traversal             | O(n) - (visits each node exactly once)     |

In summary, a Circular Linked List is useful for scenarios requiring circular traversal, such as round-robin scheduling, buffering, and continuous monitoring. While it offers benefits for these specific use cases, it has similar time complexities to singly and doubly linked lists for insertion, deletion, and traversal operations.

## 5. Queues

## Stack

### Introduction
A Stack is a linear data structure that follows the Last In, First Out (LIFO) principle. This means that the last element added to the stack will be the first one to be removed. It is similar to a stack of plates: you can only take the top plate off the stack first, and you can only add a new plate on the top. Stacks are widely used in various applications, such as expression evaluation, function call management, and backtracking algorithms.

### Operations

1. **Push**
   - Adds an element to the top of the stack.
   - If the stack is implemented using an array, it involves placing the element at the next available position and increasing the stack pointer.
   - If the stack is implemented using a linked list, it involves creating a new node and updating the top pointer.

2. **Pop**
   - Removes the top element from the stack.
   - If the stack is implemented using an array, it involves decreasing the stack pointer.
   - If the stack is implemented using a linked list, it involves updating the top pointer to the next node.
   - This operation usually returns the removed element.

3. **Peek**
   - Returns the top element of the stack without removing it.
   - It allows us to see the top element without modifying the stack.

Example Java implementation using an array:
```java
class Stack {
    private int maxSize;
    private int top;
    private int[] stackArray;

    // Constructor
    public Stack(int size) {
        maxSize = size;
        stackArray = new int[maxSize];
        top = -1;
    }

    // Push operation
    public void push(int value) {
        if (top == maxSize - 1) {
            throw new StackOverflowError("Stack is full");
        }
        stackArray[++top] = value;
    }

    // Pop operation
    public int pop() {
        if (top == -1) {
            throw new IllegalStateException("Stack is empty");
        }
        return stackArray[top--];
    }

    // Peek operation
    public int peek() {
        if (top == -1) {
            throw new IllegalStateException("Stack is empty");
        }
        return stackArray[top];
    }

    // Check if the stack is empty
    public boolean isEmpty() {
        return top == -1;
    }

    // Check if the stack is full
    public boolean isFull() {
        return top == maxSize - 1;
    }
}
```

Example Java implementation using a linked list:
```java
class Node {
    int data;
    Node next;

    Node(int data) {
        this.data = data;
        this.next = null;
    }
}

class Stack {
    private Node top;

    // Constructor
    public Stack() {
        top = null;
    }

    // Push operation
    public void push(int value) {
        Node newNode = new Node(value);
        newNode.next = top;
        top = newNode;
    }

    // Pop operation
    public int pop() {
        if (top == null) {
            throw new IllegalStateException("Stack is empty");
        }
        int value = top.data;
        top = top.next;
        return value;
    }

    // Peek operation
    public int peek() {
        if (top == null) {
            throw new IllegalStateException("Stack is empty");
        }
        return top.data;
    }

    // Check if the stack is empty
    public boolean isEmpty() {
        return top == null;
    }
}
```

### Applications

1. **Expression Evaluation:**
   - Stacks are used to evaluate expressions written in postfix or prefix notation. They help in parsing and evaluating expressions by temporarily holding operators and operands.

2. **Function Call Management:**
   - Stacks are used in managing function calls and recursion. The call stack keeps track of active subroutines or functions, maintaining their local variables and return addresses.

3. **Undo Mechanism in Software:**
   - Stacks are used to implement undo functionality in text editors, drawing applications, and other software. Each action is pushed onto the stack, and undo operations pop actions from the stack to revert to the previous state.

4. **Backtracking Algorithms:**
   - Stacks are employed in backtracking algorithms such as depth-first search (DFS), maze solving, and puzzle solving. They help in exploring possible solutions by pushing choices onto the stack and backtracking by popping them off.

5. **Parenthesis Matching:**
   - Stacks are used to check balanced parentheses in expressions. Each opening parenthesis is pushed onto the stack, and each closing parenthesis pops from the stack. If the stack is empty at the end, the parentheses are balanced.

6. **Browser History:**
   - Stacks are used to implement the browser's back functionality. Each visited page is pushed onto the stack, and the back operation pops the current page from the stack to go to the previous page.

### Summary Table

| Operation | Time Complexity |
|-----------|-----------------|
| Push      | O(1)            |
| Pop       | O(1)            |
| Peek      | O(1)            |

In summary, stacks are simple yet powerful data structures with various applications in computer science and software development. They are particularly useful for scenarios requiring LIFO access, such as expression evaluation, function call management, and backtracking algorithms. The main operations—push, pop, and peek—are all performed in constant time, making stacks efficient for these use cases.

## Queue

### Introduction
A Queue is a linear data structure that follows the First In, First Out (FIFO) principle. This means that the first element added to the queue will be the first one to be removed. It is similar to a line of people waiting for a service: the person who gets in line first is the first to be served. Queues are widely used in various applications, such as task scheduling, handling requests in web servers, and breadth-first search in graphs.

### Operations

1. **Enqueue**
   - Adds an element to the end of the queue.
   - If the queue is implemented using an array, it involves placing the element at the next available position.
   - If the queue is implemented using a linked list, it involves creating a new node and updating the rear pointer.

2. **Dequeue**
   - Removes the front element from the queue.
   - If the queue is implemented using an array, it involves removing the element at the front and shifting all other elements one position to the left.
   - If the queue is implemented using a linked list, it involves updating the front pointer to the next node.
   - This operation usually returns the removed element.

3. **Peek**
   - Returns the front element of the queue without removing it.
   - It allows us to see the front element without modifying the queue.

Example Java implementation using an array:
```java
class Queue {
    private int maxSize;
    private int front;
    private int rear;
    private int[] queueArray;

    // Constructor
    public Queue(int size) {
        maxSize = size;
        queueArray = new int[maxSize];
        front = 0;
        rear = -1;
    }

    // Enqueue operation
    public void enqueue(int value) {
        if (rear == maxSize - 1) {
            throw new IllegalStateException("Queue is full");
        }
        queueArray[++rear] = value;
    }

    // Dequeue operation
    public int dequeue() {
        if (front > rear) {
            throw new IllegalStateException("Queue is empty");
        }
        return queueArray[front++];
    }

    // Peek operation
    public int peek() {
        if (front > rear) {
            throw new IllegalStateException("Queue is empty");
        }
        return queueArray[front];
    }

    // Check if the queue is empty
    public boolean isEmpty() {
        return front > rear;
    }

    // Check if the queue is full
    public boolean isFull() {
        return rear == maxSize - 1;
    }
}
```

Example Java implementation using a linked list:
```java
class Node {
    int data;
    Node next;

    Node(int data) {
        this.data = data;
        this.next = null;
    }
}

class Queue {
    private Node front;
    private Node rear;

    // Constructor
    public Queue() {
        front = null;
        rear = null;
    }

    // Enqueue operation
    public void enqueue(int value) {
        Node newNode = new Node(value);
        if (rear == null) {
            front = newNode;
            rear = newNode;
        } else {
            rear.next = newNode;
            rear = newNode;
        }
    }

    // Dequeue operation
    public int dequeue() {
        if (front == null) {
            throw new IllegalStateException("Queue is empty");
        }
        int value = front.data;
        front = front.next;
        if (front == null) {
            rear = null;
        }
        return value;
    }

    // Peek operation
    public int peek() {
        if (front == null) {
            throw new IllegalStateException("Queue is empty");
        }
        return front.data;
    }

    // Check if the queue is empty
    public boolean isEmpty() {
        return front == null;
    }
}
```

### Applications

1. **Task Scheduling:**
   - Queues are used in task scheduling algorithms to manage tasks in a fair order.
   - Example: Operating system task scheduling where processes are executed in the order they arrive.

2. **Handling Requests in Web Servers:**
   - Queues are used to manage incoming requests in web servers.
   - Example: HTTP request handling where requests are processed in the order they are received.

3. **Breadth-First Search (BFS):**
   - Queues are used in BFS algorithms to explore nodes level by level.
   - Example: Graph traversal where all nodes at the present level are processed before moving to the next level.

4. **Print Spooling:**
   - Queues are used in print spooling to manage print jobs in the order they are submitted.
   - Example: Print queues in operating systems where print jobs are processed sequentially.

5. **Message Queuing:**
   - Queues are used in message queuing systems to manage the delivery of messages between distributed systems.
   - Example: Message brokers like RabbitMQ and Apache Kafka use queues to ensure message delivery.

6. **Simulation:**
   - Queues are used in simulations to model real-world systems.
   - Example: Simulating a line at a bank where customers are served in the order they arrive.

### Summary Table

| Operation | Time Complexity |
|-----------|-----------------|
| Enqueue   | O(1)            |
| Dequeue   | O(1)            |
| Peek      | O(1)            |

In summary, queues are fundamental data structures with various applications in computer science and software development. They are particularly useful for scenarios requiring FIFO access, such as task scheduling, request handling, and breadth-first search algorithms. The main operations—enqueue, dequeue, and peek—are all performed in constant time, making queues efficient for these use cases.

## Priority Queue

### Introduction
A Priority Queue is an abstract data type similar to a regular queue or stack, but with an added priority associated with each element. Elements are dequeued based on their priority rather than their order in the queue. Higher priority elements are dequeued before lower priority ones, regardless of when they were added to the queue. Priority queues are widely used in scenarios such as task scheduling, Dijkstra's algorithm for shortest paths, and event-driven simulation systems.

### Operations

1. **Insert (Enqueue)**
   - Adds an element to the priority queue with a given priority.
   - Elements are added in such a way that the queue maintains its priority order.

2. **Remove (Dequeue)**
   - Removes and returns the element with the highest priority from the queue.
   - The queue is reordered if necessary to maintain the priority order after the removal.

3. **Peek**
   - Returns the element with the highest priority without removing it from the queue.
   - This allows us to view the highest-priority element without modifying the queue.

4. **Change Priority**
   - Changes the priority of a specific element in the queue.
   - The queue is reordered if necessary to maintain the priority order after the priority change.

### Implementations

**Heap-based Priority Queue:**

A heap is a complete binary tree that satisfies the heap property, which can be either:
- Max-Heap: Each parent node is greater than or equal to its child nodes.
- Min-Heap: Each parent node is less than or equal to its child nodes.

A priority queue is often implemented using a binary heap due to its efficient insertion and removal operations.

1. **Insert (Enqueue)**
   - Insert the new element at the end of the heap.
   - Heapify up: Compare the new element with its parent and swap them if necessary.
   - Repeat until the heap property is restored.

2. **Remove (Dequeue)**
   - Remove the root element (the highest-priority element in a max-heap or the lowest-priority element in a min-heap).
   - Replace the root with the last element in the heap.
   - Heapify down: Compare the new root with its children and swap them if necessary.
   - Repeat until the heap property is restored.

3. **Peek**
   - Return the root element of the heap without removing it.

Example Java implementation of a min-heap-based priority queue:
```java
import java.util.Arrays;

class MinHeap {
    private int[] heap;
    private int size;
    private int maxSize;

    // Constructor
    public MinHeap(int maxSize) {
        this.maxSize = maxSize;
        this.size = 0;
        heap = new int[this.maxSize + 1];
        heap[0] = Integer.MIN_VALUE;
    }

    // Returns position of parent
    private int parent(int pos) {
        return pos / 2;
    }

    // Returns position of left child
    private int leftChild(int pos) {
        return (2 * pos);
    }

    // Returns position of right child
    private int rightChild(int pos) {
        return (2 * pos) + 1;
    }

    // Swap two nodes
    private void swap(int fpos, int spos) {
        int tmp;
        tmp = heap[fpos];
        heap[fpos] = heap[spos];
        heap[spos] = tmp;
    }

    // Heapify up
    private void heapifyUp(int pos) {
        int temp = heap[pos];
        while (pos > 0 && temp < heap[parent(pos)]) {
            heap[pos] = heap[parent(pos)];
            pos = parent(pos);
        }
        heap[pos] = temp;
    }

    // Heapify down
    private void heapifyDown(int pos) {
        int smallest = pos;
        int left = leftChild(pos);
        int right = rightChild(pos);

        if (left <= size && heap[left] < heap[smallest]) {
            smallest = left;
        }
        if (right <= size && heap[right] < heap[smallest]) {
            smallest = right;
        }
        if (smallest != pos) {
            swap(pos, smallest);
            heapifyDown(smallest);
        }
    }

    // Insert an element into the heap
    public void insert(int element) {
        if (size >= maxSize) {
            throw new IllegalStateException("Heap is full");
        }
        heap[++size] = element;
        heapifyUp(size);
    }

    // Remove and return the minimum element from the heap
    public int remove() {
        int popped = heap[1];
        heap[1] = heap[size--];
        heapifyDown(1);
        return popped;
    }

    // Return the minimum element from the heap without removing it
    public int peek() {
        return heap[1];
    }

    // Display the heap
    public void displayHeap() {
        System.out.println("Heap: " + Arrays.toString(Arrays.copyOfRange(heap, 1, size + 1)));
    }
}

public class PriorityQueueExample {
    public static void main(String[] args) {
        MinHeap minHeap = new MinHeap(10);
        minHeap.insert(3);
        minHeap.insert(5);
        minHeap.insert(1);
        minHeap.insert(10);
        minHeap.insert(2);

        minHeap.displayHeap();

        System.out.println("Peek: " + minHeap.peek());
        System.out.println("Remove: " + minHeap.remove());
        minHeap.displayHeap();
    }
}
```

### Applications

1. **Task Scheduling:**
   - Priority queues are used in task scheduling algorithms to manage tasks based on their priority.
   - Example: Operating system process scheduling where high-priority tasks are executed before low-priority ones.

2. **Dijkstra's Algorithm:**
   - Priority queues are used in Dijkstra's algorithm to find the shortest path in a weighted graph.
   - Example: Network routing protocols to find the shortest path between nodes.

3. **Event-Driven Simulation:**
   - Priority queues are used to manage events in event-driven simulation systems.
   - Example: Simulating a system where events are processed based on their occurrence time.

4. **Huffman Coding:**
   - Priority queues are used in Huffman coding to build an optimal prefix code.
   - Example: Compression algorithms to encode data efficiently.

5. **A* Search Algorithm:**
   - Priority queues are used in the A* search algorithm to find the shortest path in a graph.
   - Example: Pathfinding algorithms in games and navigation systems.

6. **Load Balancing:**
   - Priority queues are used in load balancing to manage tasks based on their priority and resource requirements.
   - Example: Distributing workloads in distributed computing systems.

### Summary Table

| Operation | Time Complexity (Heap-based) |
|-----------|------------------------------|
| Insert    | O(log n)                     |
| Remove    | O(log n)                     |
| Peek      | O(1)                         |
| Change Priority | O(log n)               |

In summary, priority queues are powerful data structures that provide efficient ways to manage elements based on their priority. They are particularly useful in applications requiring prioritized task scheduling, shortest path algorithms, and event-driven simulations. The heap-based implementation ensures efficient insertion, removal, and priority change operations, making priority queues suitable for performance-critical applications.

## Heap Data Structure

### Introduction
A Heap is a specialized tree-based data structure that satisfies the heap property. It is commonly used to implement priority queues, but it also finds applications in various other algorithms. In a heap, the parent node is always ordered with respect to its children according to a comparison criterion, typically the value of the node.

### Types

1. **Min-Heap**
   - In a Min-Heap, the value of the parent node is always less than or equal to the values of its children.
   - The smallest element is at the root of the tree.

2. **Max-Heap**
   - In a Max-Heap, the value of the parent node is always greater than or equal to the values of its children.
   - The largest element is at the root of the tree.

### Operations

1. **Insert**
   - Adds a new element to the heap.
   - **Algorithm:**
     - Add the new element at the end of the heap.
     - Perform "heapify up" (or "bubble up") to maintain the heap property.
       - Compare the added element with its parent; if they are in the correct order (Min-Heap: parent <= child, Max-Heap: parent >= child), the process is complete.
       - Otherwise, swap the elements and repeat until the heap property is restored.

   Example of "heapify up" in a Min-Heap:
   ```java
   public void insert(int value) {
       heap[++size] = value;
       int current = size;
       while (heap[current] < heap[parent(current)]) {
           swap(current, parent(current));
           current = parent(current);
       }
   }
   ```

2. **Delete**
   - Removes the root element from the heap (the smallest element in a Min-Heap or the largest element in a Max-Heap).
   - **Algorithm:**
     - Replace the root element with the last element in the heap.
     - Perform "heapify down" (or "bubble down") to maintain the heap property.
       - Compare the new root with its children; if they are in the correct order, the process is complete.
       - Otherwise, swap the element with the smaller (Min-Heap) or larger (Max-Heap) child and repeat until the heap property is restored.

   Example of "heapify down" in a Min-Heap:
   ```java
   public int delete() {
       int popped = heap[FRONT];
       heap[FRONT] = heap[size--];
       heapifyDown(FRONT);
       return popped;
   }

   private void heapifyDown(int pos) {
       while (!isLeaf(pos)) {
           int leftChild = leftChild(pos);
           int rightChild = rightChild(pos);
           int smallest = pos;

           if (leftChild <= size && heap[leftChild] < heap[smallest]) {
               smallest = leftChild;
           }
           if (rightChild <= size && heap[rightChild] < heap[smallest]) {
               smallest = rightChild;
           }

           if (smallest != pos) {
               swap(pos, smallest);
               pos = smallest;
           } else {
               break;
           }
       }
   }
   ```

3. **Peek**
   - Returns the root element without removing it.
   - **Algorithm:**
     - Simply return the element at the root of the heap.

   Example:
   ```java
   public int peek() {
       return heap[FRONT];
   }
   ```

### Applications

1. **Priority Queue:**
   - Heaps are used to implement priority queues, where elements are added with priority and the highest (Max-Heap) or lowest (Min-Heap) priority element is removed first.
   - Example: Task scheduling in operating systems.

2. **Heap Sort:**
   - A comparison-based sorting technique that uses a binary heap.
   - Example: Sorting an array in ascending or descending order.

3. **Graph Algorithms:**
   - Heaps are used in various graph algorithms such as Dijkstra's shortest path algorithm and Prim's minimum spanning tree algorithm.
   - Example: Finding the shortest path in a network.

4. **Event Simulation:**
   - Heaps are used to manage events in event-driven simulation systems.
   - Example: Simulating a system where events are processed based on their occurrence time.

5. **Median Maintenance:**
   - Heaps are used to maintain the median of a stream of numbers by using a combination of Min-Heap and Max-Heap.
   - Example: Real-time data analysis.

6. **Kth Largest/Smallest Element:**
   - Heaps are used to find the kth largest or smallest element in an array.
   - Example: Finding the 3rd largest element in an unsorted array.

### Summary Table

| Operation | Time Complexity |
|-----------|-----------------|
| Insert    | O(log n)        |
| Delete    | O(log n)        |
| Peek      | O(1)            |

In summary, heaps are versatile data structures that provide efficient ways to manage and access data based on priority. They are particularly useful in applications requiring priority-based task management, efficient sorting, and graph algorithms. The main operations—insert, delete, and peek—are performed efficiently, making heaps suitable for performance-critical applications.

## Deque (Double-ended Queue)

### Introduction
A Deque (short for "double-ended queue") is an abstract data type that generalizes a queue, for which elements can be added to or removed from either the front (head) or back (tail). Deques support all operations that queues do, but also allow insertion and deletion at both ends, providing greater flexibility in data handling.

### Operations

1. **Add to Front (addFirst)**
   - Inserts an element at the front of the deque.
   - Example in Java:
     ```java
     Deque<Integer> deque = new LinkedList<>();
     deque.addFirst(10);
     ```

2. **Add to Back (addLast)**
   - Inserts an element at the back of the deque.
   - Example in Java:
     ```java
     deque.addLast(20);
     ```

3. **Remove from Front (removeFirst)**
   - Removes and returns the element at the front of the deque.
   - Example in Java:
     ```java
     int front = deque.removeFirst();
     ```

4. **Remove from Back (removeLast)**
   - Removes and returns the element at the back of the deque.
   - Example in Java:
     ```java
     int back = deque.removeLast();
     ```

5. **Peek Front (peekFirst)**
   - Returns the element at the front of the deque without removing it.
   - Example in Java:
     ```java
     int front = deque.peekFirst();
     ```

6. **Peek Back (peekLast)**
   - Returns the element at the back of the deque without removing it.
   - Example in Java:
     ```java
     int back = deque.peekLast();
     ```

7. **Check if Deque is Empty (isEmpty)**
   - Checks if the deque is empty.
   - Example in Java:
     ```java
     boolean isEmpty = deque.isEmpty();
     ```

8. **Size (size)**
   - Returns the number of elements in the deque.
   - Example in Java:
     ```java
     int size = deque.size();
     ```

Example Java implementation using `ArrayDeque`:
```java
import java.util.ArrayDeque;
import java.util.Deque;

public class DequeExample {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();

        // Add elements to the deque
        deque.addFirst(10);
        deque.addLast(20);
        deque.addFirst(5);

        // Print deque
        System.out.println("Deque: " + deque);

        // Remove elements from the deque
        int front = deque.removeFirst();
        int back = deque.removeLast();

        // Print removed elements
        System.out.println("Removed from front: " + front);
        System.out.println("Removed from back: " + back);

        // Peek elements
        System.out.println("Front element: " + deque.peekFirst());
        System.out.println("Back element: " + deque.peekLast());

        // Check if deque is empty
        System.out.println("Is deque empty? " + deque.isEmpty());

        // Size of deque
        System.out.println("Size of deque: " + deque.size());
    }
}
```

### Use Cases

1. **Palindrome Checker:**
   - Deques are useful for checking if a sequence of characters is a palindrome.
   - Example: Verifying if the string "radar" reads the same backward and forward.

2. **Sliding Window Problems:**
   - Deques are used to efficiently solve sliding window problems where elements within a specific range need to be accessed or modified.
   - Example: Finding the maximum or minimum in a sliding window of an array.

3. **Task Scheduling:**
   - Deques can be used in task scheduling where tasks need to be processed from both ends.
   - Example: Scheduling processes in a way that allows both immediate and deferred task execution.

4. **Undo Operations:**
   - Deques can be used to implement undo functionality in applications by maintaining a history of operations.
   - Example: Undo feature in text editors.

5. **Deque-based Queue and Stack Implementations:**
   - Deques can be used to implement both queue and stack data structures, providing more flexible operation handling.
   - Example: Using a deque to implement a stack that allows pushing and popping from both ends.

6. **Graph Algorithms:**
   - Deques are used in certain graph traversal algorithms like BFS (Breadth-First Search) and DFS (Depth-First Search) to efficiently manage nodes.
   - Example: Maintaining the list of nodes to visit next in a graph traversal algorithm.

### Summary Table

| Operation        | Description                             |
|------------------|-----------------------------------------|
| addFirst         | Inserts an element at the front         |
| addLast          | Inserts an element at the back          |
| removeFirst      | Removes and returns the front element   |
| removeLast       | Removes and returns the back element    |
| peekFirst        | Returns the front element without removing |
| peekLast         | Returns the back element without removing |
| isEmpty          | Checks if the deque is empty            |
| size             | Returns the number of elements in the deque |

In summary, a deque is a versatile data structure that allows efficient insertion and deletion at both ends. Its flexibility makes it suitable for a wide range of applications, including palindromes checking, sliding window problems, task scheduling, and implementing undo operations. The ability to perform operations from both ends makes deques a powerful tool in various algorithms and real-world scenarios.

## 6. Trees

## Binary Tree

### Introduction
A binary tree is a hierarchical data structure in which each node has at most two children, referred to as the left child and the right child. The topmost node is called the root, and nodes without children are called leaves. Binary trees are widely used in various applications such as searching, sorting, and hierarchical data representation.

### Types

1. **Full Binary Tree**
   - A binary tree is full if every node has either 0 or 2 children.
   - Example:
     ```
         1
        / \
       2   3
      / \ / \
     4  5 6  7
     ```

2. **Complete Binary Tree**
   - A binary tree is complete if all levels are fully filled except possibly the last level, which is filled from left to right.
   - Example:
     ```
         1
        / \
       2   3
      / \ /
     4  5 6
     ```

3. **Perfect Binary Tree**
   - A binary tree is perfect if all internal nodes have exactly two children and all leaf nodes are at the same level.
   - Example:
     ```
         1
        / \
       2   3
      / \ / \
     4  5 6  7
     ```

4. **Balanced Binary Tree**
   - A binary tree is balanced if the height of the left and right subtrees of every node differs by at most one.
   - Example (Balanced):
     ```
         1
        / \
       2   3
      / \
     4   5
     ```
   - Example (Unbalanced):
     ```
         1
        /
       2
      /
     3
     ```

### Basic Operations

1. **Insertion**
   - Adds a new node to the tree at the appropriate position to maintain the binary tree properties.
   - Example in Java:
     ```java
     class Node {
         int key;
         Node left, right;

         public Node(int item) {
             key = item;
             left = right = null;
         }
     }

     class BinaryTree {
         Node root;

         BinaryTree() {
             root = null;
         }

         void insert(int key) {
             root = insertRec(root, key);
         }

         Node insertRec(Node root, int key) {
             if (root == null) {
                 root = new Node(key);
                 return root;
             }

             if (key < root.key)
                 root.left = insertRec(root.left, key);
             else if (key > root.key)
                 root.right = insertRec(root.right, key);

             return root;
         }
     }
     ```

2. **Deletion**
   - Removes a node from the tree and rearranges the structure to maintain the binary tree properties.
   - Example in Java:
     ```java
     void delete(int key) {
         root = deleteRec(root, key);
     }

     Node deleteRec(Node root, int key) {
         if (root == null) return root;

         if (key < root.key)
             root.left = deleteRec(root.left, key);
         else if (key > root.key)
             root.right = deleteRec(root.right, key);
         else {
             if (root.left == null)
                 return root.right;
             else if (root.right == null)
                 return root.left;

             root.key = minValue(root.right);
             root.right = deleteRec(root.right, root.key);
         }

         return root;
     }

     int minValue(Node root) {
         int minValue = root.key;
         while (root.left != null) {
             minValue = root.left.key;
             root = root.left;
         }
         return minValue;
     }
     ```

3. **Traversal**
   - **Inorder Traversal (Left, Root, Right)**
     - Visits the nodes in a non-decreasing order.
     - Example in Java:
       ```java
       void inorder() {
           inorderRec(root);
       }

       void inorderRec(Node root) {
           if (root != null) {
               inorderRec(root.left);
               System.out.print(root.key + " ");
               inorderRec(root.right);
           }
       }
       ```

   - **Preorder Traversal (Root, Left, Right)**
     - Visits the root node before the left and right subtrees.
     - Example in Java:
       ```java
       void preorder() {
           preorderRec(root);
       }

       void preorderRec(Node root) {
           if (root != null) {
               System.out.print(root.key + " ");
               preorderRec(root.left);
               preorderRec(root.right);
           }
       }
       ```

   - **Postorder Traversal (Left, Right, Root)**
     - Visits the root node after the left and right subtrees.
     - Example in Java:
       ```java
       void postorder() {
           postorderRec(root);
       }

       void postorderRec(Node root) {
           if (root != null) {
               postorderRec(root.left);
               postorderRec(root.right);
               System.out.print(root.key + " ");
           }
       }
       ```

4. **Searching**
   - Finds a node in the tree with a given key.
   - Example in Java:
     ```java
     Node search(int key) {
         return searchRec(root, key);
     }

     Node searchRec(Node root, int key) {
         if (root == null || root.key == key)
             return root;

         if (root.key > key)
             return searchRec(root.left, key);

         return searchRec(root.right, key);
     }
     ```

5. **Height Calculation**
   - Determines the height of the tree, which is the number of edges on the longest path from the root to a leaf.
   - Example in Java:
     ```java
     int height() {
         return heightRec(root);
     }

     int heightRec(Node root) {
         if (root == null)
             return 0;

         int leftHeight = heightRec(root.left);
         int rightHeight = heightRec(root.right);

         return Math.max(leftHeight, rightHeight) + 1;
     }
     ```

### Summary Table

| Operation   | Description                                                      |
|-------------|------------------------------------------------------------------|
| Insertion   | Adds a new node while maintaining binary tree properties         |
| Deletion    | Removes a node and maintains binary tree properties              |
| Inorder     | Traverses nodes in non-decreasing order                          |
| Preorder    | Traverses nodes starting from the root                           |
| Postorder   | Traverses nodes starting from the leaves                         |
| Searching   | Finds a node with a specified key                                |
| Height      | Calculates the height of the tree                                |

In summary, binary trees are fundamental data structures that provide efficient ways to organize and access data. They come in various forms, each with specific properties that make them suitable for different applications. The basic operations—such as insertion, deletion, traversal, searching, and height calculation—are essential for working with binary trees in computer science and software development.

## Binary Search Tree (BST)

### Introduction
A Binary Search Tree (BST) is a binary tree in which each node has at most two children (left and right), and the key (value) of each node is greater than the keys in its left subtree and less than the keys in its right subtree. This property allows for efficient searching, insertion, and deletion operations.

### Operations

1. **Insertion**
   - To insert a new key into a BST, we start at the root and compare the key with the root's key.
   - If the key is less than the root's key, we move to the left subtree. If it is greater, we move to the right subtree.
   - We repeat this process recursively until we reach a null subtree, where we insert the new key as a leaf node.

2. **Deletion**
   - Deleting a node in a BST can be a bit more complex.
   - If the node to be deleted is a leaf node (has no children), we simply remove it from the tree.
   - If the node has only one child, we replace the node with its child.
   - If the node has two children, we can either find the maximum node in its left subtree (predecessor) or the minimum node in its right subtree (successor) to replace it, and then delete the predecessor or successor recursively.

3. **Searching**
   - Searching in a BST is similar to insertion.
   - We start at the root and compare the key with the root's key.
   - If the key is less than the root's key, we move to the left subtree. If it is greater, we move to the right subtree.
   - We repeat this process recursively until we find the key or reach a null subtree (indicating the key is not in the tree).

### Time Complexity

- **Average Case:**
  - The average time complexity of insertion, deletion, and searching in a BST is O(log n), where n is the number of nodes in the tree.
  - This is because in a balanced BST, each comparison reduces the search space by half, similar to binary search in a sorted array.

- **Worst Case:**
  - The worst-case time complexity of insertion, deletion, and searching in a BST is O(n), which occurs when the tree is unbalanced.
  - An unbalanced tree can degenerate into a linked list, where each node only has one child, leading to linear time complexity for operations.

- **Balanced BST:**
  - To ensure efficient operations, it is important to keep the BST balanced.
  - Various self-balancing BSTs, such as AVL trees and Red-Black trees, automatically adjust their structure to maintain balance, ensuring O(log n) time complexity for operations even in the worst case.

### Example Implementation in Java

```java
class Node {
    int key;
    Node left, right;

    public Node(int item) {
        key = item;
        left = right = null;
    }
}

class BinarySearchTree {
    Node root;

    BinarySearchTree() {
        root = null;
    }

    void insert(int key) {
        root = insertRec(root, key);
    }

    Node insertRec(Node root, int key) {
        if (root == null) {
            root = new Node(key);
            return root;
        }

        if (key < root.key)
            root.left = insertRec(root.left, key);
        else if (key > root.key)
            root.right = insertRec(root.right, key);

        return root;
    }

    void inorder() {
        inorderRec(root);
    }

    void inorderRec(Node root) {
        if (root != null) {
            inorderRec(root.left);
            System.out.print(root.key + " ");
            inorderRec(root.right);
        }
    }

    void delete(int key) {
        root = deleteRec(root, key);
    }

    Node deleteRec(Node root, int key) {
        if (root == null) return root;

        if (key < root.key)
            root.left = deleteRec(root.left, key);
        else if (key > root.key)
            root.right = deleteRec(root.right, key);
        else {
            if (root.left == null)
                return root.right;
            else if (root.right == null)
                return root.left;

            root.key = minValue(root.right);
            root.right = deleteRec(root.right, root.key);
        }

        return root;
    }

    int minValue(Node root) {
        int minValue = root.key;
        while (root.left != null) {
            minValue = root.left.key;
            root = root.left;
        }
        return minValue;
    }

    boolean search(int key) {
        return searchRec(root, key);
    }

    boolean searchRec(Node root, int key) {
        if (root == null)
            return false;

        if (root.key == key)
            return true;

        if (root.key < key)
            return searchRec(root.right, key);

        return searchRec(root.left, key);
    }
}

public class Main {
    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();

        bst.insert(50);
        bst.insert(30);
        bst.insert(20);
        bst.insert(40);
        bst.insert(70);
        bst.insert(60);
        bst.insert(80);

        System.out.println("Inorder traversal:");
        bst.inorder(); // Output: 20 30 40 50 60 70 80

        System.out.println("\nAfter deleting 20:");
        bst.delete(20);
        bst.inorder(); // Output: 30 40 50 60 70 80

        System.out.println("\nSearch for 40: " + bst.search(40)); // Output: true
        System.out.println("Search for 100: " + bst.search(100)); // Output: false
    }
}
```

This example demonstrates basic operations on a BST, including insertion, deletion, and searching. The tree is balanced in this example, resulting in efficient O(log n

## Self-balancing Binary Search Tree (AVL Tree)

### Introduction
An AVL tree is a self-balancing binary search tree in which the heights of the two child subtrees of any node differ by at most one. It is named after its inventors, Adelson-Velsky and Landis. AVL trees maintain their balance through rotations, which are operations that preserve the binary search tree properties while adjusting the tree's balance factor.

### Rotations

1. **Left Rotation**
   - **Condition:** Imbalance occurs when the right subtree of a node is higher by 2 levels or more than the left subtree.
   - **Action:** Perform a left rotation to rebalance the tree.
     ```
         A
          \
           B
            \
             C
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

2. **Right Rotation**
   - **Condition:** Imbalance occurs when the left subtree of a node is higher by 2 levels or more than the right subtree.
   - **Action:** Perform a right rotation to rebalance the tree.
     ```
             C
            /
           B
          /
         A
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

3. **Left-Right Rotation (Double Rotation)**
   - **Condition:** Imbalance occurs when a node's right child has a left child.
   - **Action:** Perform a right rotation on the right child followed by a left rotation on the node to rebalance the tree.
     ```
           A
            \
             C
            /
           B
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

4. **Right-Left Rotation (Double Rotation)**
   - **Condition:** Imbalance occurs when a node's left child has a right child.
   - **Action:** Perform a left rotation on the left child followed by a right rotation on the node to rebalance the tree.
     ```
           C
          /
         A
          \
           B
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

### Time Complexity

- **Insertion:** O(log n)
- **Deletion:** O(log n)
- **Searching:** O(log n)

In AVL trees, the height of the tree is always O(log n), where n is the number of nodes in the tree. This guarantees the efficient time complexity for insertion, deletion, and searching operations. The rotations ensure that the tree remains balanced, providing optimal performance even for dynamic datasets.

## Self-balancing Binary Search Tree (AVL Tree)

### Introduction
An AVL tree is a self-balancing binary search tree in which the heights of the two child subtrees of any node differ by at most one. It is named after its inventors, Adelson-Velsky and Landis. AVL trees maintain their balance through rotations, which are operations that preserve the binary search tree properties while adjusting the tree's balance factor.

### Rotations

1. **Left Rotation**
   - **Condition:** Imbalance occurs when the right subtree of a node is higher by 2 levels or more than the left subtree.
   - **Action:** Perform a left rotation to rebalance the tree.
     ```
         A
          \
           B
            \
             C
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

2. **Right Rotation**
   - **Condition:** Imbalance occurs when the left subtree of a node is higher by 2 levels or more than the right subtree.
   - **Action:** Perform a right rotation to rebalance the tree.
     ```
             C
            /
           B
          /
         A
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

3. **Left-Right Rotation (Double Rotation)**
   - **Condition:** Imbalance occurs when a node's right child has a left child.
   - **Action:** Perform a right rotation on the right child followed by a left rotation on the node to rebalance the tree.
     ```
           A
            \
             C
            /
           B
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

4. **Right-Left Rotation (Double Rotation)**
   - **Condition:** Imbalance occurs when a node's left child has a right child.
   - **Action:** Perform a left rotation on the left child followed by a right rotation on the node to rebalance the tree.
     ```
           C
          /
         A
          \
           B
     ```
     becomes
     ```
           B
         /   \
        A     C
     ```

### Time Complexity

- **Insertion:** O(log n)
- **Deletion:** O(log n)
- **Searching:** O(log n)

In AVL trees, the height of the tree is always O(log n), where n is the number of nodes in the tree. This guarantees the efficient time complexity for insertion, deletion, and searching operations. The rotations ensure that the tree remains balanced, providing optimal performance even for dynamic datasets.

## Segment Tree

### Introduction
Segment Tree is a versatile data structure that allows querying and updating elements in an array efficiently. It is particularly useful for handling range queries (queries that ask for information about a specific range of elements) in an array.

### Construction
1. **Array Representation:**
   - The segment tree is typically represented as an array.
   - For an array of size `n`, the segment tree would have a total of `2n-1` nodes.

2. **Construction Process:**
   - Start with an array of size `n`.
   - Each leaf node in the segment tree represents an element from the input array.
   - The parent nodes in the segment tree represent segments of the input array. Each parent node's value is computed based on the values of its children.

3. **Building the Tree:**
   - The segment tree is built recursively in a top-down manner.
   - At each step, the current node is assigned the sum (or any other aggregate function) of its children's values.
   - This process continues until each segment represents a single element from the input array.

### Queries and Updates
1. **Query Operation:**
   - To perform a query for a given range `[l, r]`, start at the root of the segment tree.
   - If the current segment `[start, end]` is completely within `[l, r]`, return the value of the current node.
   - If the current segment is completely outside `[l, r]`, return a neutral value (e.g., 0 for sum queries).
   - If the current segment partially overlaps with `[l, r]`, recurse on both children and combine the results.

2. **Update Operation:**
   - To update a value at index `idx` in the input array, start at the root of the segment tree.
   - If the current segment contains `idx`, update the current node and recurse on its children.
   - Otherwise, do not update the current node and continue to recurse on the appropriate child.

### Applications
1. **Range Queries:**
   - Segment trees are used for efficient querying of range-based problems, such as finding the sum, minimum, maximum, or other aggregate functions over a range of elements in an array.

2. **Lazy Propagation:**
   - Segment trees can be extended with lazy propagation to efficiently handle updates (e.g., incrementing all elements in a range by a value) in addition to queries.

3. **Interval Scheduling:**
   - Segment trees can be used to efficiently schedule and manage intervals of time or resources, such as in scheduling tasks or allocating resources.

4. **Geometric Problems:**
   - In geometric problems, segment trees can be used to efficiently handle queries related to points, lines, or shapes in a plane.

5. **String Processing:**
   - Segment trees can be applied in string processing algorithms, such as substring queries or palindrome checks.

Segment trees are a powerful data structure that enables efficient handling of range queries and updates in arrays. Their recursive nature and ability to represent array segments make them well-suited for a variety of applications where such operations are frequently required.

Here's a basic implementation of a segment tree in Java for range sum queries:

```java
class SegmentTree {
    int[] tree;
    int n;

    SegmentTree(int[] nums) {
        n = nums.length;
        tree = new int[2 * n];

        // Fill the leaf nodes with the input array values
        for (int i = 0; i < n; i++) {
            tree[n + i] = nums[i];
        }

        // Build the segment tree by calculating the sum of child nodes
        for (int i = n - 1; i > 0; i--) {
            tree[i] = tree[2 * i] + tree[2 * i + 1];
        }
    }

    void update(int index, int val) {
        index += n;
        tree[index] = val;
        // Update the parent nodes iteratively
        while (index > 0) {
            int left = index;
            int right = index;
            if (index % 2 == 0) {
                right = index + 1;
            } else {
                left = index - 1;
            }
            // Parent is the sum of left and right child nodes
            tree[index / 2] = tree[left] + tree[right];
            index /= 2;
        }
    }

    int sumRange(int left, int right) {
        left += n;
        right += n;
        int sum = 0;
        while (left <= right) {
            if ((left % 2) == 1) {
                sum += tree[left];
                left++;
            }
            if ((right % 2) == 0) {
                sum += tree[right];
                right--;
            }
            left /= 2;
            right /= 2;
        }
        return sum;
    }
}

public class Main {
    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 7, 9, 11};
        SegmentTree tree = new SegmentTree(nums);
        System.out.println(tree.sumRange(0, 2)); // Output: 9
        tree.update(1, 2);
        System.out.println(tree.sumRange(0, 2)); // Output: 8
    }
}
```

This implementation demonstrates the basic operations of a segment tree, including construction, updating values, and querying the sum of a range. You can modify this implementation to suit other types of range queries or to handle other aggregate functions besides the sum.

Tree traversals are algorithms used to visit each node in a tree data structure exactly once. There are three main types of tree traversals: inorder, preorder, and postorder. These traversals differ in the order in which they visit nodes.

## Inorder Traversal

In inorder traversal, nodes are visited in the following order:
1. Visit the left subtree.
2. Visit the current node (root).
3. Visit the right subtree.

Inorder traversal is commonly used for binary search trees (BSTs) to visit nodes in ascending order. It is also used to get the sorted sequence of elements in a BST.

## Preorder Traversal

In preorder traversal, nodes are visited in the following order:
1. Visit the current node (root).
2. Visit the left subtree.
3. Visit the right subtree.

Preorder traversal is used to create a copy of the tree, and it is also useful for prefix expressions (Polish notation).

## Postorder Traversal

In postorder traversal, nodes are visited in the following order:
1. Visit the left subtree.
2. Visit the right subtree.
3. Visit the current node (root).

Postorder traversal is used to delete the tree and to evaluate postfix expressions.

## Implementation (Java)

Here's a simple implementation of the three tree traversal algorithms:

```java
class Node {
    int data;
    Node left, right;

    public Node(int item) {
        data = item;
        left = right = null;
    }
}

class BinaryTree {
    Node root;

    BinaryTree() {
        root = null;
    }

    void inorder(Node node) {
        if (node != null) {
            inorder(node.left);
            System.out.print(node.data + " ");
            inorder(node.right);
        }
    }

    void preorder(Node node) {
        if (node != null) {
            System.out.print(node.data + " ");
            preorder(node.left);
            preorder(node.right);
        }
    }

    void postorder(Node node) {
        if (node != null) {
            postorder(node.left);
            postorder(node.right);
            System.out.print(node.data + " ");
        }
    }

    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);

        System.out.println("Inorder traversal:");
        tree.inorder(tree.root); // Output: 4 2 5 1 3
        System.out.println("\nPreorder traversal:");
        tree.preorder(tree.root); // Output: 1 2 4 5 3
        System.out.println("\nPostorder traversal:");
        tree.postorder(tree.root); // Output: 4 5 2 3 1
    }
}
```

In this implementation, we create a simple binary tree and traverse it using the inorder, preorder, and postorder traversal methods. Each method recursively traverses the left subtree, visits the current node, and then recursively traverses the right subtree.

## 7. Graphs

In Java, a graph is a data structure that consists of a set of vertices (nodes) and a set of edges that connect these vertices. Graphs are used to model relationships between objects or entities. There are two common ways to represent graphs in Java: adjacency matrix and adjacency list.

### Adjacency Matrix
- **Representation:** An adjacency matrix is a 2D array of size \(V \times V\), where \(V\) is the number of vertices in the graph. If there is an edge from vertex \(i\) to vertex \(j\), the matrix element \(A[i][j]\) is set to 1 (or some weight if the graph is weighted). Otherwise, it is set to 0.
- **Space Complexity:** The space complexity of an adjacency matrix is \(O(V^2)\), which can be inefficient for sparse graphs (graphs with few edges).
- **Time Complexity:** Querying an edge between two vertices takes \(O(1)\) time. Adding or removing an edge takes \(O(1)\) time. However, iterating over all edges adjacent to a vertex takes \(O(V)\) time, which can be inefficient for dense graphs.

### Adjacency List
- **Representation:** An adjacency list is a collection of lists or arrays, where each vertex \(v\) has a list of vertices that are adjacent to it. In an undirected graph, if there is an edge between vertex \(u\) and vertex \(v\), then \(v\) appears in \(u\)'s list and \(u\) appears in \(v\)'s list.
- **Space Complexity:** The space complexity of an adjacency list is \(O(V + E)\), where \(V\) is the number of vertices and \(E\) is the number of edges in the graph. This representation is more space-efficient for sparse graphs.
- **Time Complexity:** Querying an edge between two vertices takes \(O(deg(v))\) time, where \(deg(v)\) is the degree of vertex \(v\). Adding or removing an edge takes \(O(1)\) time.

### Graph Class in Java
In Java, a graph can be represented using custom classes. Here's a basic implementation of a graph using an adjacency list:

```java
import java.util.*;

class Graph {
    private int V;
    private LinkedList<Integer>[] adj;

    Graph(int v) {
        V = v;
        adj = new LinkedList[v];
        for (int i = 0; i < v; ++i)
            adj[i] = new LinkedList();
    }

    void addEdge(int v, int w) {
        adj[v].add(w);
        adj[w].add(v);
    }

    LinkedList<Integer>[] getAdjacencyList() {
        return adj;
    }

    public static void main(String[] args) {
        Graph g = new Graph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 3);

        LinkedList<Integer>[] adjList = g.getAdjacencyList();
        for (int i = 0; i < adjList.length; i++) {
            System.out.print("Vertex " + i + " is connected to: ");
            for (Integer neighbor : adjList[i]) {
                System.out.print(neighbor + " ");
            }
            System.out.println();
        }
    }
}
```

In this implementation, the `Graph` class uses an array of `LinkedList`s to represent the adjacency list. The `addEdge` method adds an edge between two vertices, and the `getAdjacencyList` method returns the adjacency list of the graph.

## Adjacency Matrix

### Representation
An adjacency matrix is a square matrix used to represent a finite graph. The rows and columns of the matrix represent the vertices of the graph, and the presence or absence of edges between vertices is indicated by the values in the matrix. 

For an undirected graph, the adjacency matrix is symmetric. For a directed graph, it is not necessarily symmetric.

If there are \(n\) vertices in the graph, the adjacency matrix is an \(n \times n\) matrix.

### Space and Time Complexity
- **Space Complexity:** The space complexity of an adjacency matrix is \(O(V^2)\), where \(V\) is the number of vertices in the graph. This is because every vertex is connected to every other vertex (or itself in the case of loops) in the worst case.
  
- **Time Complexity for Querying an Edge:** Given two vertices \(u\) and \(v\), determining if there is an edge between them in an adjacency matrix takes \(O(1)\) time. This is because you can directly access the entry in the matrix corresponding to \(u\) and \(v\) to check for the presence of an edge.

- **Time Complexity for Adding or Removing an Edge:** Adding or removing an edge in an adjacency matrix takes \(O(1)\) time. This is because you can directly update the entry in the matrix corresponding to the edge.

### Applications
- **Graph Representation:** Adjacency matrices are commonly used to represent graphs in graph theory and computer science. They provide a simple and intuitive way to represent graphs and are easy to understand and implement.

- **Path Finding Algorithms:** Adjacency matrices are used in path-finding algorithms such as Floyd-Warshall algorithm for finding the shortest path between all pairs of vertices in a graph.

- **Network Analysis:** In network analysis, adjacency matrices are used to represent connections between nodes in a network, such as in social networks, transportation networks, and communication networks.

- **Clustering Algorithms:** Adjacency matrices are used in clustering algorithms to represent the similarity or distance between data points in a dataset.

- **Sparse Graphs:** While adjacency matrices are not space-efficient for sparse graphs (graphs with few edges), they are useful for dense graphs where most pairs of vertices are connected.

Here's a basic implementation of an adjacency matrix in Java:

```java
class Graph {
    private int[][] adjMatrix;
    private int numVertices;

    public Graph(int numVertices) {
        this.numVertices = numVertices;
        adjMatrix = new int[numVertices][numVertices];
    }

    public void addEdge(int source, int destination) {
        adjMatrix[source][destination] = 1;
        // For undirected graph, add the following line
        // adjMatrix[destination][source] = 1;
    }

    public void removeEdge(int source, int destination) {
        adjMatrix[source][destination] = 0;
        // For undirected graph, add the following line
        // adjMatrix[destination][source] = 0;
    }

    public boolean hasEdge(int source, int destination) {
        return adjMatrix[source][destination] == 1;
    }

    public void printGraph() {
        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                System.out.print(adjMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(4);

        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);

        System.out.println("Adjacency Matrix:");
        graph.printGraph();
    }
}
```

In this implementation, we create a `Graph` class with an adjacency matrix representation. We can add edges, remove edges, check if an edge exists between two vertices, and print the adjacency matrix.

## Adjacency List

### Representation
An adjacency list is a collection of lists or arrays used to represent the edges in a graph. Each vertex in the graph has a list of adjacent vertices, which are the vertices that can be reached from the current vertex by following an edge.

In an undirected graph, each edge is represented twice (once in the adjacency list of each endpoint). In a directed graph, each edge is represented only once in the adjacency list of its source vertex.

### Space and Time Complexity
- **Space Complexity:** The space complexity of an adjacency list is \(O(V + E)\), where \(V\) is the number of vertices and \(E\) is the number of edges in the graph. This is because each vertex has an associated list of adjacent vertices, and the total number of entries in all adjacency lists is \(E\).

- **Time Complexity for Querying an Edge:** Given two vertices \(u\) and \(v\), determining if there is an edge between them in an adjacency list takes \(O(deg(u))\) time, where \(deg(u)\) is the degree of vertex \(u\) (the number of edges incident to \(u\)). This is because you need to scan through the adjacency list of \(u\) to check for the presence of \(v\).

- **Time Complexity for Adding or Removing an Edge:** Adding or removing an edge in an adjacency list takes \(O(1)\) time on average. This is because you can add or remove an element from the adjacency list of a vertex in constant time.

### Applications
- **Graph Representation:** Adjacency lists are commonly used to represent graphs in graph theory and computer science. They provide a more space-efficient way to represent sparse graphs (graphs with few edges) compared to adjacency matrices.

- **Traversal Algorithms:** Adjacency lists are used in traversal algorithms such as breadth-first search (BFS) and depth-first search (DFS) to efficiently visit all vertices in a graph.

- **Shortest Path Algorithms:** Adjacency lists are used in shortest path algorithms such as Dijkstra's algorithm and Bellman-Ford algorithm to find the shortest path between two vertices in a graph.

- **Network Routing:** Adjacency lists are used in network routing algorithms to represent the connections between routers or nodes in a network.

- **Social Networks:** Adjacency lists are used in social network analysis to represent connections between users in a social network.

- **Recommendation Systems:** Adjacency lists are used in recommendation systems to represent the relationships between users and items in a dataset.

Here's a basic implementation of an adjacency list in Java using a HashMap:

```java
import java.util.*;

class Graph {
    private Map<Integer, List<Integer>> adjacencyList;

    public Graph() {
        adjacencyList = new HashMap<>();
    }

    public void addEdge(int source, int destination) {
        if (!adjacencyList.containsKey(source)) {
            adjacencyList.put(source, new ArrayList<>());
        }
        if (!adjacencyList.containsKey(destination)) {
            adjacencyList.put(destination, new ArrayList<>());
        }
        adjacencyList.get(source).add(destination);
        // For undirected graph, add the following line
        // adjacencyList.get(destination).add(source);
    }

    public void removeEdge(int source, int destination) {
        if (adjacencyList.containsKey(source)) {
            adjacencyList.get(source).remove(Integer.valueOf(destination));
        }
        // For undirected graph, add the following line
        // adjacencyList.get(destination).remove(Integer.valueOf(source));
    }

    public boolean hasEdge(int source, int destination) {
        return adjacencyList.containsKey(source) && adjacencyList.get(source).contains(destination);
    }

    public void printGraph() {
        for (Map.Entry<Integer, List<Integer>> entry : adjacencyList.entrySet()) {
            System.out.print(entry.getKey() + " -> ");
            for (Integer vertex : entry.getValue()) {
                System.out.print(vertex + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph();

        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);

        System.out.println("Adjacency List:");
        graph.printGraph();
    }
}
```

In this implementation, we use a HashMap to represent the adjacency list. Each key in the HashMap corresponds to a vertex, and the value is a list of adjacent vertices. We provide methods to add edges, remove edges, check for the presence of an edge, and print the adjacency list.

## Depth-First Search (DFS)

### Algorithm
Depth-First Search (DFS) is a traversal algorithm used to explore nodes and edges in a graph. It starts at a selected node (often called the "root" in a tree) and explores as far as possible along each branch before backtracking.

1. **Recursive Approach:**
   - Visit the current node.
   - Recursively visit all unvisited neighbors of the current node.

2. **Stack-Based Iterative Approach:**
   - Start with an empty stack and push the root node onto the stack.
   - While the stack is not empty, pop a node from the stack.
     - If the node has not been visited, mark it as visited and push all its unvisited neighbors onto the stack.

### Applications
1. **Traversal:** DFS is used to traverse trees and graphs. In trees, DFS can be used to do a pre-order, in-order, or post-order traversal depending on the requirement.
2. **Connected Components:** DFS can be used to find the connected components in an undirected graph.
3. **Cycle Detection:** DFS can be used to detect cycles in a graph.
4. **Pathfinding:** DFS can be used to find a path between two vertices in a graph.
5. **Topological Sorting:** DFS can be used to perform a topological sort on a directed acyclic graph (DAG).

### Time Complexity
The time complexity of DFS is \(O(V + E)\), where \(V\) is the number of vertices and \(E\) is the number of edges in the graph. This is because each vertex and each edge is visited once. In the worst-case scenario, DFS may visit every vertex and edge in the graph.

Here's a simple implementation of Depth-First Search (DFS) in Java for an undirected graph represented using an adjacency list:

```java
import java.util.*;

class Graph {
    private int V;
    private LinkedList<Integer>[] adj;

    Graph(int v) {
        V = v;
        adj = new LinkedList[v];
        for (int i = 0; i < v; ++i)
            adj[i] = new LinkedList();
    }

    void addEdge(int v, int w) {
        adj[v].add(w);
        adj[w].add(v);
    }

    void DFSUtil(int v, boolean[] visited) {
        visited[v] = true;
        System.out.print(v + " ");
        for (Integer n : adj[v]) {
            if (!visited[n])
                DFSUtil(n, visited);
        }
    }

    void DFS(int v) {
        boolean[] visited = new boolean[V];
        DFSUtil(v, visited);
    }

    public static void main(String[] args) {
        Graph g = new Graph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 3);

        System.out.println("Depth First Traversal (starting from vertex 0):");
        g.DFS(0);
    }
}
```

In this implementation, we first create a `Graph` class with methods to add edges and perform Depth-First Search traversal. The `DFSUtil` method is a recursive helper function that performs the actual traversal. We then create a graph, add some edges, and perform DFS starting from a specified vertex.

## Breadth-First Search (BFS)

### Algorithm
Breadth-First Search (BFS) is a traversal algorithm used to explore nodes and edges in a graph. It starts at a selected node (often called the "root" in a tree) and explores all of the neighbor nodes at the present depth prior to moving on to the nodes at the next depth level.

1. **Queue-Based Iterative Approach:**
   - Start with an empty queue and enqueue the root node.
   - While the queue is not empty, dequeue a node from the queue.
     - Visit the node.
     - Enqueue all unvisited neighbors of the node.

### Applications
1. **Shortest Path and Minimum Spanning Tree for unweighted graphs:** BFS can be used to find the shortest path from a start node to an end node in an unweighted graph, as well as to find the minimum spanning tree of the graph.
2. **Connected Components:** BFS can be used to find all the connected components in an undirected graph.
3. **Cycles:** BFS can be used to detect cycles in a graph.
4. **Network Broadcasting:** BFS is used in network algorithms for broadcasting packets from one source to all other nodes in the network.
5. **Finding Path:** BFS can be used to find the path between two nodes in a graph.

### Time Complexity
The time complexity of BFS is \(O(V + E)\), where \(V\) is the number of vertices and \(E\) is the number of edges in the graph. This is because each vertex and each edge is visited once. In the worst-case scenario, BFS may visit every vertex and edge in the graph.

Here's a simple implementation of Breadth-First Search (BFS) in Java for an undirected graph represented using an adjacency list:

```java
import java.util.*;

class Graph {
    private int V;
    private LinkedList<Integer>[] adj;

    Graph(int v) {
        V = v;
        adj = new LinkedList[v];
        for (int i = 0; i < v; ++i)
            adj[i] = new LinkedList();
    }

    void addEdge(int v, int w) {
        adj[v].add(w);
        adj[w].add(v);
    }

    void BFS(int s) {
        boolean[] visited = new boolean[V];
        LinkedList<Integer> queue = new LinkedList<Integer>();

        visited[s] = true;
        queue.add(s);

        while (queue.size() != 0) {
            s = queue.poll();
            System.out.print(s + " ");

            Iterator<Integer> i = adj[s].listIterator();
            while (i.hasNext()) {
                int n = i.next();
                if (!visited[n]) {
                    visited[n] = true;
                    queue.add(n);
                }
            }
        }
    }

    public static void main(String[] args) {
        Graph g = new Graph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 3);

        System.out.println("Breadth First Traversal (starting from vertex 0):");
        g.BFS(0);
    }
}
```

In this implementation, we create a `Graph` class with methods to add edges and perform Breadth-First Search traversal. The `BFS` method starts by initializing a boolean array `visited` to keep track of visited vertices and a queue to store vertices to be processed. It then performs the BFS traversal by repeatedly dequeuing a vertex from the queue, visiting it, and enqueuing its unvisited neighbors.

## Contributing
If you'd like to contribute, please fork the repository and use a feature branch. Pull requests are warmly welcome.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.