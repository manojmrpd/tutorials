# Java Array Coding Interview

## Table of Contents

- [Filter Even Numbers in a Given Array in Java](#filter-even-numbers-in-a-given-array-in-java)
- [Find If a Number is Even or Odd Without Using the Modulus Operator](#find-if-a-number-is-even-or-odd-without-using-the-modulus-operator)
- [Print the Fibonacci Numbers Till 100](#print-the-fibonacci-numbers-till-100)
- [Check if a Given Number is a Palindrome](#check-if-a-given-number-is-a-palindrome)
- [Check if a Given Number is an Armstrong Number](#check-if-a-given-number-is-an-armstrong-number)
- [Find the Second Largest Number in a Given Array in Java](#find-the-second-largest-number-in-a-given-array-in-java)
- [Find the Third Largest Number in a Given Array in Java](#find-the-third-largest-number-in-a-given-array-in-java)
- [Remove Duplicate Elements in a Given Array in Java](#remove-duplicate-elements-in-a-given-array-in-java)
- [Find Duplicate Elements in a Given Array in Java](#find-duplicate-elements-in-a-given-array-in-java)
- [Count Duplicate Occurrences in a Given Integer Array in Java](#count-duplicate-occurrences-in-a-given-integer-array-in-java)
- [Find the Second Non-repeated Occurrence in an Integer Array in Java](#find-the-second-non-repeated-occurrence-in-an-integer-array-in-java)
- [Find the Second Repeated Occurrence in an Integer Array in Java](#find-the-second-repeated-occurrence-in-an-integer-array-in-java)
- [Place All Occurrences of Even Numbers Before Odd Numbers in an Array in Java](#place-all-occurrences-of-even-numbers-before-odd-numbers-in-an-array-in-java)
- [Place Zeros to the Left and Ones to the Right in an Integer Array](#place-zeros-to-the-left-and-ones-to-the-right-in-an-integer-array)
- [Sort the Elements in Descending Order in an Integer Array](#sort-the-elements-in-descending-order-in-an-integer-array)
- [Check Input in an Integer Array](#check-input-in-an-integer-array)


## Filter even numbers in a given array in java

To filter even numbers from an array in Java, you can use a variety of methods, such as using a simple loop, the `Stream` API (introduced in Java 8), or other utility classes. Below are examples of both approaches:

### Using a Simple Loop
```java
import java.util.ArrayList;
import java.util.List;

public class FilterEvenNumbers {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> evenNumbers = new ArrayList<>();

        for (int number : numbers) {
            if (number % 2 == 0) {
                evenNumbers.add(number);
            }
        }

        System.out.println("Even numbers: " + evenNumbers);
    }
}
``````

### Explanation

1. **Simple Loop Approach**:
    - Create an `ArrayList` to store the even numbers.
    - Iterate through the array and check if each number is even using the modulus operator (`%`).
    - If the number is even, add it to the `ArrayList`.


## Find a number is even or add without using modules operator
To determine if a number is even or odd without using the modulus operator (`%`), you can use bitwise operations. Specifically, you can use the bitwise AND operator (`&`) to check the least significant bit of the number. If the least significant bit is 0, the number is even; if it is 1, the number is odd.

Here's how you can do it in Java:

### Using Bitwise AND Operator

```java
public class EvenOrOdd {
    public static void main(String[] args) {
        int number = 42; // Example number to check

        if (isEven(number)) {
            System.out.println(number + " is even.");
        } else {
            System.out.println(number + " is odd.");
        }
    }

    public static boolean isEven(int number) {
        return (number & 1) == 0;
    }
}
```

### Explanation

1. **Input Number**:
    - The example number to check is `42`.

2. **isEven Method**:
    - The method takes an integer as input and returns a boolean indicating whether the number is even.
    - It uses the bitwise AND operator (`&`) to check the least significant bit of the number.
    - `number & 1` evaluates to `0` if the number is even and `1` if the number is odd.
    - The method returns `true` if the result is `0` (even), otherwise, it returns `false` (odd).

This approach is efficient and works for any integer, allowing you to determine if a number is even or odd without using the modulus operator.

## Print the Fibanocci number till 100
To print Fibonacci numbers up to 100 in Java, you can use a loop to generate the Fibonacci sequence and stop when the next Fibonacci number exceeds 100. Here's how you can do it:

```java
public class Fibonacci {
    public static void main(String[] args) {
        int maxNumber = 100;
        int previousNumber = 0;
        int currentNumber = 1;

        System.out.println("Fibonacci Series up to " + maxNumber + ":");
        
        while (previousNumber <= maxNumber) {
            System.out.print(previousNumber + " ");

            int nextNumber = previousNumber + currentNumber;
            previousNumber = currentNumber;
            currentNumber = nextNumber;
        }
    }
}
```

### Explanation

1. **Initialization**:
    - `maxNumber` is set to 100, which determines when to stop printing Fibonacci numbers.
    - `previousNumber` is initialized to 0 (the first Fibonacci number).
    - `currentNumber` is initialized to 1 (the second Fibonacci number).

2. **Printing Fibonacci Series**:
    - The program enters a `while` loop that continues as long as `previousNumber` is less than or equal to `maxNumber`.
    - Within the loop, it prints the `previousNumber`.
    - It then calculates the `nextNumber` in the sequence by adding `previousNumber` and `currentNumber`.
    - It updates `previousNumber` to `currentNumber` and `currentNumber` to `nextNumber` to prepare for the next iteration.

3. **Output**:
    - The program prints the Fibonacci series up to the maximum number specified (in this case, up to 100).

When you run this program, it will print the Fibonacci series up to 100:

```
Fibonacci Series up to 100:
0 1 1 2 3 5 8 13 21 34 55 89
```

## Find the given number is a palindrome number
To determine if a given number is a palindrome in Java, you can use the following approach. A number is considered a palindrome if it reads the same backward as forward. Here's a simple method to check for a palindrome number:

### Using a Simple Loop
```java
public class PalindromeNumber {
    public static void main(String[] args) {
        int number = 12321; // Example number to check
        boolean isPalindrome = isPalindrome(number);

        if (isPalindrome) {
            System.out.println(number + " is a palindrome.");
        } else {
            System.out.println(number + " is not a palindrome.");
        }
    }

    public static boolean isPalindrome(int number) {
        int originalNumber = number;
        int reversedNumber = 0;

        while (number != 0) {
            int digit = number % 10;
            reversedNumber = reversedNumber * 10 + digit;
            number /= 10;
        }

        return originalNumber == reversedNumber;
    }
}
```

### Explanation

1. **Input Number**: 
    - The example number to check is `12321`.

2. **isPalindrome Method**:
    - The method takes an integer as input and returns a boolean indicating whether the number is a palindrome.
    - Store the original number in `originalNumber`.
    - Initialize `reversedNumber` to 0.
    - Use a while loop to reverse the digits of the input number:
        - Extract the last digit of the number using `number % 10`.
        - Append the digit to `reversedNumber` by multiplying `reversedNumber` by 10 and adding the digit.
        - Remove the last digit from the number using integer division (`number /= 10`).
    - After the loop, compare `originalNumber` with `reversedNumber`.
    - Return `true` if they are equal, otherwise return `false`.

This method efficiently checks if a given number is a palindrome by reversing its digits and comparing it to the original number.

## Find the given number is a amstrong number
An Armstrong number (also known as a narcissistic number or pluperfect number) is a number that is equal to the sum of its own digits each raised to the power of the number of digits. For example, 153 is an Armstrong number because \(1^3 + 5^3 + 3^3 = 153\).

Here's a Java program to check if a given number is an Armstrong number:

```java
public class ArmstrongNumber {
    public static void main(String[] args) {
        int number = 153; // Example number to check
        boolean isArmstrong = isArmstrong(number);

        if (isArmstrong) {
            System.out.println(number + " is an Armstrong number.");
        } else {
            System.out.println(number + " is not an Armstrong number.");
        }
    }

    public static boolean isArmstrong(int number) {
        int originalNumber = number;
        int sum = 0;
        int numberOfDigits = String.valueOf(number).length();

        while (number != 0) {
            int digit = number % 10;
            sum += Math.pow(digit, numberOfDigits);
            number /= 10;
        }

        return sum == originalNumber;
    }
}
```

### Explanation

1. **Input Number**:
    - The example number to check is `153`.

2. **isArmstrong Method**:
    - The method takes an integer as input and returns a boolean indicating whether the number is an Armstrong number.
    - Store the original number in `originalNumber`.
    - Initialize `sum` to 0.
    - Determine the number of digits in the input number using `String.valueOf(number).length()`.
    - Use a while loop to iterate through each digit of the number:
        - Extract the last digit of the number using `number % 10`.
        - Raise the digit to the power of `numberOfDigits` and add it to `sum`.
        - Remove the last digit from the number using integer division (`number /= 10`).
    - After the loop, compare `sum` with `originalNumber`.
    - Return `true` if they are equal, otherwise return `false`.

This method checks if a given number is an Armstrong number by calculating the sum of its digits each raised to the power of the number of digits and comparing it to the original number.

## Find the second largest number in a given array in java
To find the second largest number in a given array in Java, you can use the following approach. The idea is to iterate through the array and keep track of the largest and second largest numbers.

### Using a Single Pass
This method traverses the array once and keeps track of the largest and second largest numbers:

```java
public class SecondLargestNumber {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 4, 45, 99, 2, 8};

        int secondLargest = findSecondLargest(numbers);

        if (secondLargest != Integer.MIN_VALUE) {
            System.out.println("The second largest number is: " + secondLargest);
        } else {
            System.out.println("The array does not have a second largest number.");
        }
    }

    public static int findSecondLargest(int[] numbers) {
        if (numbers == null || numbers.length < 2) {
            throw new IllegalArgumentException("Array must have at least two elements");
        }

        int largest = Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;

        for (int number : numbers) {
            if (number > largest) {
                secondLargest = largest;
                largest = number;
            } else if (number > secondLargest && number < largest) {
                secondLargest = number;
            }
        }

        return secondLargest;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 4, 45, 99, 2, 8}`.

2. **findSecondLargest Method**:
    - The method takes an integer array as input and returns the second largest number.
    - It checks if the array is null or has less than two elements and throws an `IllegalArgumentException` if so.
    - Initialize `largest` and `secondLargest` to `Integer.MIN_VALUE` to handle all possible integer values.
    - Iterate through each number in the array:
        - If the current number is greater than `largest`, update `secondLargest` to `largest` and then update `largest` to the current number.
        - If the current number is greater than `secondLargest` but less than `largest`, update `secondLargest` to the current number.
    - After the loop, return `secondLargest`.

This method efficiently finds the second largest number in a single pass through the array. If the array has fewer than two elements, it throws an exception to indicate invalid input.

## Find the third largest number in a given array in java
To find the third largest number in a given array in Java, you can use a simple algorithm that iterates through the array once, keeping track of the three largest numbers found so far. Here's how you can implement this:

### Using a Single Pass
This method traverses the array once and keeps track of the largest, second largest, and third largest numbers:

```java
public class ThirdLargestNumber {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 4, 45, 99, 2, 8};

        int thirdLargest = findThirdLargest(numbers);

        if (thirdLargest != Integer.MIN_VALUE) {
            System.out.println("The third largest number is: " + thirdLargest);
        } else {
            System.out.println("The array does not have a third largest number.");
        }
    }

    public static int findThirdLargest(int[] numbers) {
        if (numbers == null || numbers.length < 3) {
            throw new IllegalArgumentException("Array must have at least three elements");
        }

        int first = Integer.MIN_VALUE;
        int second = Integer.MIN_VALUE;
        int third = Integer.MIN_VALUE;

        for (int number : numbers) {
            if (number > first) {
                third = second;
                second = first;
                first = number;
            } else if (number > second && number < first) {
                third = second;
                second = number;
            } else if (number > third && number < second) {
                third = number;
            }
        }

        return third;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 4, 45, 99, 2, 8}`.

2. **findThirdLargest Method**:
    - The method takes an integer array as input and returns the third largest number.
    - It checks if the array is null or has fewer than three elements and throws an `IllegalArgumentException` if so.
    - Initialize `first`, `second`, and `third` to `Integer.MIN_VALUE` to handle all possible integer values.
    - Iterate through each number in the array:
        - If the current number is greater than `first`, update `third` to `second`, `second` to `first`, and then `first` to the current number.
        - If the current number is greater than `second` but less than `first`, update `third` to `second` and `second` to the current number.
        - If the current number is greater than `third` but less than `second`, update `third` to the current number.
    - After the loop, return `third`.

This method efficiently finds the third largest number in a single pass through the array. If the array has fewer than three elements, it throws an exception to indicate invalid input.


## Remove the duplicate elements in a given array in java
To remove duplicate elements from a given array in Java, you can use a `HashSet` to store unique elements. A `HashSet` does not allow duplicate values, making it ideal for this task. Here's how you can do it:

### Using a HashSet

```java
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        int[] uniqueNumbers = removeDuplicates(numbers);

        System.out.println("Array without duplicates: " + Arrays.toString(uniqueNumbers));
    }

    public static int[] removeDuplicates(int[] numbers) {
        Set<Integer> uniqueSet = new HashSet<>();
        
        for (int number : numbers) {
            uniqueSet.add(number);
        }

        int[] uniqueArray = new int[uniqueSet.size()];
        int index = 0;
        
        for (int number : uniqueSet) {
            uniqueArray[index++] = number;
        }

        return uniqueArray;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **removeDuplicates Method**:
    - The method takes an integer array as input and returns a new array containing only the unique elements.
    - Initialize a `HashSet` called `uniqueSet` to store unique elements.
    - Iterate through each number in the input array and add it to the `HashSet`. Adding elements to a `HashSet` automatically handles duplicates by only storing unique elements.
    - Create a new array called `uniqueArray` with a size equal to the number of unique elements (i.e., the size of the `HashSet`).
    - Iterate through the `HashSet` and copy the elements to the `uniqueArray`.
    - Return the `uniqueArray`.

3. **Output**:
    - The program prints the array without duplicates: `[50, 20, 40, 10, 30]`.

Note: The order of elements in the resulting array may not be the same as the input array because a `HashSet` does not maintain insertion order. If maintaining the order is important, you can use a `LinkedHashSet` instead:

### Using a LinkedHashSet

```java
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        int[] uniqueNumbers = removeDuplicates(numbers);

        System.out.println("Array without duplicates: " + Arrays.toString(uniqueNumbers));
    }

    public static int[] removeDuplicates(int[] numbers) {
        Set<Integer> uniqueSet = new LinkedHashSet<>();
        
        for (int number : numbers) {
            uniqueSet.add(number);
        }

        int[] uniqueArray = new int[uniqueSet.size()];
        int index = 0;
        
        for (int number : uniqueSet) {
            uniqueArray[index++] = number;
        }

        return uniqueArray;
    }
}
```

Using `LinkedHashSet` maintains the insertion order of elements, ensuring that the output array has the same order as the original array, minus the duplicates.

If you want to remove duplicate elements from an array without using a `HashSet`, you can use a sorting-based approach or a nested loop to check for duplicates. Here, I'll show you both methods:

### Method 1: Using Sorting

1. Sort the array.
2. Traverse the sorted array and keep track of unique elements.
3. Copy unique elements to a new array.

```java
import java.util.Arrays;

public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        int[] uniqueNumbers = removeDuplicates(numbers);

        System.out.println("Array without duplicates: " + Arrays.toString(uniqueNumbers));
    }

    public static int[] removeDuplicates(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            return numbers;
        }

        Arrays.sort(numbers);
        int uniqueCount = 1;

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] != numbers[i - 1]) {
                uniqueCount++;
            }
        }

        int[] uniqueArray = new int[uniqueCount];
        uniqueArray[0] = numbers[0];
        int index = 1;

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] != numbers[i - 1]) {
                uniqueArray[index++] = numbers[i];
            }
        }

        return uniqueArray;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **removeDuplicates Method**:
    - The method sorts the array first.
    - It then counts the number of unique elements.
    - A new array `uniqueArray` is created to hold these unique elements.
    - Iterate through the sorted array to populate the `uniqueArray`.

3. **Output**:
    - The program prints the array without duplicates: `[10, 20, 30, 40, 50]`.

### Method 2: Using Nested Loops

1. Use a nested loop to check each element against all previous elements.
2. If the element is unique, add it to a new array.

```java
public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        int[] uniqueNumbers = removeDuplicates(numbers);

        System.out.println("Array without duplicates: " + java.util.Arrays.toString(uniqueNumbers));
    }

    public static int[] removeDuplicates(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            return numbers;
        }

        int[] tempArray = new int[numbers.length];
        int uniqueCount = 0;

        for (int i = 0; i < numbers.length; i++) {
            boolean isDuplicate = false;
            for (int j = 0; j < uniqueCount; j++) {
                if (numbers[i] == tempArray[j]) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                tempArray[uniqueCount++] = numbers[i];
            }
        }

        int[] uniqueArray = new int[uniqueCount];
        System.arraycopy(tempArray, 0, uniqueArray, 0, uniqueCount);

        return uniqueArray;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **removeDuplicates Method**:
    - The method initializes a temporary array `tempArray` to store unique elements and a counter `uniqueCount`.
    - It uses nested loops to check each element against all previous elements.
    - If an element is found to be unique, it is added to `tempArray`.
    - After processing all elements, the method creates a final array `uniqueArray` with the size of `uniqueCount` and copies unique elements from `tempArray`.

3. **Output**:
    - The program prints the array without duplicates: `[10, 20, 30, 40, 50]`.

Both methods remove duplicates from the given array without using a `HashSet`. The sorting-based approach is generally more efficient for large arrays, while the nested loop approach is straightforward but less efficient for larger arrays.

## Find the duplicate elements in a given array in java
To find duplicate elements in a given array in Java, you can use a `HashMap` or a nested loop approach. Here, I'll show you both methods:

### Using HashMap

```java
import java.util.HashMap;
import java.util.Map;

public class FindDuplicates {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        Map<Integer, Integer> duplicateCounts = findDuplicateOccurrences(numbers);

        for (Map.Entry<Integer, Integer> entry : duplicateCounts.entrySet()) {
            if (entry.getValue() > 1) {
                System.out.println("Number " + entry.getKey() + " occurs " + entry.getValue() + " times");
            }
        }
    }

    public static Map<Integer, Integer> findDuplicateOccurrences(int[] numbers) {
        Map<Integer, Integer> counts = new HashMap<>();

        for (int number : numbers) {
            counts.put(number, counts.getOrDefault(number, 0) + 1);
        }

        return counts;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **findDuplicateOccurrences Method**:
    - The method takes an integer array as input and returns a `Map<Integer, Integer>` containing the count of each duplicate element.
    - Initialize a `HashMap` called `counts` to store the frequency of each element.
    - Iterate through each number in the array and add it to the `HashMap`. If the number is already in the `HashMap`, increment its count.

3. **Output**:
    - The program prints each duplicate number along with its count.

### Using Nested Loop

```java
public class FindDuplicates {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        findDuplicateOccurrences(numbers);
    }

    public static void findDuplicateOccurrences(int[] numbers) {
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[i] == numbers[j]) {
                    System.out.println("Duplicate number found: " + numbers[i]);
                }
            }
        }
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **findDuplicateOccurrences Method**:
    - The method uses a nested loop to compare each element in the array with every other element.
    - If a duplicate is found (i.e., if `numbers[i] == numbers[j]`), it prints the duplicate number.

3. **Output**:
    - The program prints each duplicate number found in the array.

Both methods find duplicate elements in the given array. The HashMap approach is more efficient for larger arrays, while the nested loop approach is simpler but less efficient for larger arrays.

## Find the duplicate occurrences  count in a given integer array in java
To find and count the duplicate occurrences in a given array in Java, you can use a `HashMap` to keep track of the frequency of each element. This approach ensures that you count the occurrences efficiently.

Here's an example of how you can achieve this:

### Using a HashMap

```java
import java.util.HashMap;
import java.util.Map;

public class DuplicateOccurrences {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        Map<Integer, Integer> duplicateCounts = findDuplicateOccurrences(numbers);

        for (Map.Entry<Integer, Integer> entry : duplicateCounts.entrySet()) {
            System.out.println("Number " + entry.getKey() + " occurs " + entry.getValue() + " times");
        }
    }

    public static Map<Integer, Integer> findDuplicateOccurrences(int[] numbers) {
        Map<Integer, Integer> counts = new HashMap<>();

        for (int number : numbers) {
            counts.put(number, counts.getOrDefault(number, 0) + 1);
        }

        // Remove non-duplicates from the map
        counts.entrySet().removeIf(entry -> entry.getValue() == 1);

        return counts;
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **findDuplicateOccurrences Method**:
    - The method takes an integer array as input and returns a `Map<Integer, Integer>` containing the count of each duplicate element.
    - Initialize a `HashMap` called `counts` to store the frequency of each element.
    - Iterate through each number in the array:
        - Use `counts.getOrDefault(number, 0)` to get the current count of the number (defaulting to 0 if the number is not yet in the map) and increment it by 1.
        - Put the updated count back into the map using `counts.put(number, updatedCount)`.
    - After populating the map with frequencies, remove entries that have a count of 1 (i.e., non-duplicates) using `counts.entrySet().removeIf(entry -> entry.getValue() == 1)`.
    - Return the map containing only duplicate elements and their counts.

3. **Output**:
    - The program prints each number that occurs more than once along with its count.

This method efficiently finds and counts the duplicate occurrences in the array using a single pass to build the frequency map and another pass to filter out non-duplicates.

### Using a Simple Loop

To find the duplicate occurrences count in a given array in Java without using a `HashMap`, you can sort the array and then iterate through it to count the duplicates. Here's how you can do it:

```java
import java.util.Arrays;

public class DuplicateOccurrences {
    public static void main(String[] args) {
        int[] numbers = {10, 20, 10, 30, 20, 40, 50, 30, 30};

        findDuplicateOccurrences(numbers);
    }

    public static void findDuplicateOccurrences(int[] numbers) {
        Arrays.sort(numbers);

        int count = 1;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] == numbers[i - 1]) {
                count++;
            } else {
                if (count > 1) {
                    System.out.println("Number " + numbers[i - 1] + " occurs " + count + " times");
                }
                count = 1;
            }
        }

        // Check for duplicates in the last element
        if (count > 1) {
            System.out.println("Number " + numbers[numbers.length - 1] + " occurs " + count + " times");
        }
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{10, 20, 10, 30, 20, 40, 50, 30, 30}`.

2. **findDuplicateOccurrences Method**:
    - The method sorts the array first.
    - It then iterates through the sorted array and counts the occurrences of each number.
    - If the current number is the same as the previous number, it increments the count.
    - If the current number is different from the previous number, it checks if the count is greater than 1 (indicating duplicates) and prints the count.
    - The method also checks for duplicates in the last element of the array.

3. **Output**:
    - The program prints each duplicate number along with its count.

This method efficiently finds and counts the duplicate occurrences in the array without using a `HashMap`.

## Find the Second non-repeated occurrences in an integer array in java
To find the second non-repeated occurrence in an integer array in Java, you can use a combination of frequency counting and an array traversal. Here’s how you can achieve this without using `HashMap`.

### Steps:

1. **Count Frequencies**: Traverse the array to count the frequency of each element.
2. **Find Non-Repeated Elements**: Traverse the array again to identify the non-repeated elements.
3. **Get Second Non-Repeated Element**: Track the non-repeated elements and print the second one.

Here's the implementation:

```java
public class SecondNonRepeated {
    public static void main(String[] args) {
        int[] numbers = {4, 5, 4, 5, 3, 2, 3, 7, 8};

        int secondNonRepeated = findSecondNonRepeated(numbers);

        if (secondNonRepeated != -1) {
            System.out.println("The second non-repeated number is: " + secondNonRepeated);
        } else {
            System.out.println("There is no second non-repeated number.");
        }
    }

    public static int findSecondNonRepeated(int[] numbers) {
        int[] frequency = new int[1001]; // Adjust size based on input constraints

        // Count frequencies of each element
        for (int number : numbers) {
            frequency[number]++;
        }

        // Find the second non-repeated element
        int nonRepeatedCount = 0;
        for (int number : numbers) {
            if (frequency[number] == 1) {
                nonRepeatedCount++;
                if (nonRepeatedCount == 2) {
                    return number;
                }
            }
        }

        return -1; // If there is no second non-repeated number
    }
}
```

### Explanation

1. **Counting Frequencies**:
    - The method uses an array `frequency` to count the occurrences of each element.
    - The size of the frequency array is set to `1001`, assuming that the input array values are within the range `0-1000`. You can adjust this size based on your constraints.

2. **Finding Non-Repeated Elements**:
    - The method traverses the input array and uses the `frequency` array to count how many times each number appears.
    - It then traverses the input array again to find the second non-repeated element. It maintains a counter `nonRepeatedCount` to track the number of non-repeated elements encountered.

3. **Output**:
    - If the second non-repeated element is found, it returns that element.
    - If no second non-repeated element exists, it returns `-1`.

### Example

For the array `{4, 5, 4, 5, 3, 2, 3, 7, 8}`, the frequency count will be:
- `4`: 2 times
- `5`: 2 times
- `3`: 2 times
- `2`: 1 time
- `7`: 1 time
- `8`: 1 time

The non-repeated elements are `2`, `7`, and `8`. The second non-repeated element is `7`. Therefore, the output will be `The second non-repeated number is: 7`.

## Find the Second repeated occurrences in an integer array in java
To find the second repeated occurrence in an integer array in Java, you can use a combination of frequency counting and traversal. Here's how you can achieve this without using `HashMap`.

### Steps:

1. **Count Frequencies**: Traverse the array to count the frequency of each element.
2. **Identify Repeated Elements**: Traverse the array again to identify the repeated elements.
3. **Get Second Repeated Element**: Track the repeated elements and print the second one.

Here's the implementation:

```java
public class SecondRepeated {
    public static void main(String[] args) {
        int[] numbers = {4, 5, 4, 5, 3, 2, 3, 7, 8, 2};

        int secondRepeated = findSecondRepeated(numbers);

        if (secondRepeated != -1) {
            System.out.println("The second repeated number is: " + secondRepeated);
        } else {
            System.out.println("There is no second repeated number.");
        }
    }

    public static int findSecondRepeated(int[] numbers) {
        int max = findMax(numbers);
        int[] frequency = new int[max + 1]; // Adjust size based on input constraints

        // Count frequencies of each element
        for (int number : numbers) {
            frequency[number]++;
        }

        // Find the second repeated element
        int repeatedCount = 0;
        for (int number : numbers) {
            if (frequency[number] > 1) {
                repeatedCount++;
                frequency[number] = 0; // To avoid counting the same element multiple times
                if (repeatedCount == 2) {
                    return number;
                }
            }
        }

        return -1; // If there is no second repeated number
    }

    private static int findMax(int[] numbers) {
        int max = numbers[0];
        for (int number : numbers) {
            if (number > max) {
                max = number;
            }
        }
        return max;
    }
}
```

### Explanation

1. **Counting Frequencies**:
    - The method uses an array `frequency` to count the occurrences of each element.
    - The size of the `frequency` array is set to the maximum value found in the input array plus one. This ensures that all possible values in the input array have corresponding indices in the `frequency` array.

2. **Finding Repeated Elements**:
    - The method traverses the input array and uses the `frequency` array to count how many times each number appears.
    - It then traverses the input array again to find the repeated elements. It maintains a counter `repeatedCount` to track the number of repeated elements encountered.
    - Once a repeated element is found, it sets its frequency to `0` to avoid counting it multiple times.

3. **Output**:
    - If the second repeated element is found, it returns that element.
    - If no second repeated element exists, it returns `-1`.

### Example

For the array `{4, 5, 4, 5, 3, 2, 3, 7, 8, 2}`, the frequency count will be:
- `4`: 2 times
- `5`: 2 times
- `3`: 2 times
- `2`: 2 times
- `7`: 1 time
- `8`: 1 time

The first repeated element is `4` and the second repeated element is `5`. Therefore, the output will be `The second repeated number is: 5`.

## Place all the occurences of even number in an array before odd numbers in java
To place all occurrences of even numbers in an array before odd numbers in Java, you can use a two-pointer approach. Here's how you can do it:

```java
public class EvenOddSort {
    public static void main(String[] args) {
        int[] numbers = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};

        sortEvenOdd(numbers);

        System.out.println("Array with even numbers before odd numbers: " + java.util.Arrays.toString(numbers));
    }

    public static void sortEvenOdd(int[] numbers) {
        int left = 0;
        int right = numbers.length - 1;

        while (left < right) {
            if (numbers[left] % 2 != 0 && numbers[right] % 2 == 0) {
                // Swap odd number from left and even number from right
                int temp = numbers[left];
                numbers[left] = numbers[right];
                numbers[right] = temp;
                left++;
                right--;
            } else {
                if (numbers[left] % 2 == 0) {
                    left++;
                }
                if (numbers[right] % 2 != 0) {
                    right--;
                }
            }
        }
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5}`.

2. **sortEvenOdd Method**:
    - The method uses two pointers, `left` and `right`, initialized to the start and end of the array respectively.
    - It iterates through the array from both ends:
        - If the number at the `left` pointer is odd and the number at the `right` pointer is even, it swaps them and moves the pointers.
        - If the number at the `left` pointer is even, it moves the `left` pointer to the right.
        - If the number at the `right` pointer is odd, it moves the `right` pointer to the left.
    - The process continues until the `left` pointer is no longer less than the `right` pointer.

3. **Output**:
    - The program prints the array with even numbers occurring before odd numbers: `[4, 2, 6, 3, 1, 5, 9, 5, 3, 1, 5]`.

This method rearranges the elements in the array such that all even numbers appear before odd numbers, without changing the relative order of even or odd numbers.

## Given an integer array, place 0s to left and 1s to right
To place all 0s to the left and all 1s to the right in an array, you can use a two-pointer approach. Here's how you can do it:

```java
public class SortZeroesAndOnes {
    public static void main(String[] args) {
        int[] nums = {0, 1, 0, 1, 1, 0, 0, 1};

        sortZeroesAndOnes(nums);

        System.out.println("Array after sorting: " + java.util.Arrays.toString(nums));
    }

    public static void sortZeroesAndOnes(int[] nums) {
        int left = 0;
        int right = nums.length - 1;

        while (left < right) {
            while (nums[left] == 0 && left < right) {
                left++;
            }

            while (nums[right] == 1 && left < right) {
                right--;
            }

            if (left < right) {
                nums[left] = 0;
                nums[right] = 1;
                left++;
                right--;
            }
        }
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{0, 1, 0, 1, 1, 0, 0, 1}`.

2. **sortZeroesAndOnes Method**:
    - The method uses two pointers, `left` and `right`, initialized to the start and end of the array respectively.
    - It iterates through the array from both ends:
        - While `nums[left]` is `0` and `left` is less than `right`, increment `left`.
        - While `nums[right]` is `1` and `left` is less than `right`, decrement `right`.
        - If `left` is still less than `right`, swap `nums[left]` with `0` and `nums[right]` with `1`, and then increment `left` and decrement `right`.

3. **Output**:
    - The program prints the array after sorting: `[0, 0, 0, 0, 1, 1, 1, 1]`.

This method rearranges the elements in the array such that all `0`s appear before all `1`s.

## Given an integer array, sort the elements in descending order
To sort an integer array in descending order without using `Arrays.sort`, you can implement a sorting algorithm like Bubble Sort, Selection Sort, or Insertion Sort. Here, I will demonstrate how to implement Bubble Sort to sort the array in descending order:

### Bubble Sort Implementation in Descending Order

```java
public class SortDescending {
    public static void main(String[] args) {
        int[] numbers = {4, 5, 4, 5, 3, 2, 3, 7, 8, 2};

        bubbleSortDescending(numbers);

        System.out.println("Array sorted in descending order: " + java.util.Arrays.toString(numbers));
    }

    public static void bubbleSortDescending(int[] arr) {
        int n = arr.length;
        boolean swapped;
        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            for (int j = 0; j < n - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    // Swap arr[j] and arr[j+1]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
            }
            // If no two elements were swapped in the inner loop, break
            if (!swapped) break;
        }
    }
}
```

### Explanation

1. **Input Array**:
    - The example array is `{4, 5, 4, 5, 3, 2, 3, 7, 8, 2}`.

2. **Bubble Sort in Descending Order**:
    - The `bubbleSortDescending` method implements the Bubble Sort algorithm to sort the array in descending order.
    - It iterates over the array multiple times. In each pass, it compares adjacent elements and swaps them if they are in the wrong order (i.e., the first element is less than the second element).
    - This process is repeated until the array is sorted in descending order. The outer loop ensures that the largest elements bubble to the end of the array in each iteration.

3. **Output**:
    - The program prints the array sorted in descending order.

### Output

The output will be:
```
Array sorted in descending order: [8, 7, 5, 5, 4, 4, 3, 3, 2, 2]
```

This implementation sorts the array in descending order without using the built-in `Arrays.sort` method.

## Given an integer array of Input check-1
- Input:  {1, 2, 3, 4, '@', 5, 9, 'a', 6, 7, 8, 4, -5, -7, -3, -2, -1};
- Expected Output: 1 2 3 6 7 8 -3 -2 -1

```java
package com.java.stream.number;

public class InputOutputArray1 {
    public static void main(String[] args) {
        Object[] arr = {1, 2, 3, 4, '@', 5, 9, 'a', 6, 7, 8, 4, -5, -7, -3, -2, -1};
        StringBuilder line = new StringBuilder();
        for (Object obj : arr) {
            if (obj instanceof Integer) {
                int num = (Integer) obj;
                if (num >= -3 && num <= 3 || num >= 6 && num <= 8) {
                    line.append(num).append(" ");
                }
            }
        }
        System.out.println(line.toString());
    }
}
```

### Output
```
1 2 3 6 7 8 -3 -2 -1
```